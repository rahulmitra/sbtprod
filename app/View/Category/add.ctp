<?php $this->Html->addCrumb('Advertisers', $this->Html->url( null, true )); ?>
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Add Category
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				
				<form action="/category/add" id="submit_form_line_item" method="POST" class="form-horizontal">
					<div class="form-body">
						<div class="alert alert-danger display-hide">
							<button class="close" data-close="alert"></button>
							You have some form errors. Please check below.
						</div>
						<div class="alert alert-success display-hide">
							<button class="close" data-close="alert"></button>
							Your form validation is successful!
						</div>
					
			
						<h3 class="form-section">Add Category</h3>
						
						
						<div class="form-group">
							<label class="control-label col-md-2">Category Name<span class="required">
							* </span></label>
							<div class="col-md-5">
								<div class="input-icon right">
									<input type="text" class="form-control" name="data[tblCategory][category_name]" id="txtSubjectLine" placeholder="Enter the Subject Line here" />
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-2">Status<span class="required">
							* </span></label>
							<div class="col-md-2">
								<div class="input-group">
							       	<label>
												<input type="radio" onclick="checkInternalExternal(this)" checked name="data[tblCategory][status]" value="1" checked  class="icheck">Active 
									</label>
									<label>
												<input type="radio" onclick="checkInternalExternal(this)" name="data[tblAdunit][status]" value="0" class="icheck"> Inactive
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-offset-6 col-md-6">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->	
<style>
	.select2-container-multi .select2-choices .select2-search-choice {
	margin : 6px 0 3px 5px !important;
	}
	.select2-container--default .select2-selection--multiple .select2-selection__choice
	{
	padding: 5px 5px;
	}
</style>		
																