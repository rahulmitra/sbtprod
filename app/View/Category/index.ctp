<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Dashboard</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/company/">All Categories</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Categories</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<a href="/category/add" id="sample_editable_1_new" class="btn green">
									Add New <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						<div class="col-md-6">
						</div>
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
						<tr>
							<th>
								Company Name
							</th>
							<th>
								Email
							</th>
							<th>
								Joined
							</th>
							<th>
								Status
							</th>
							<th class="center" style="text-align:center;">
								Action
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							$sr = 1;
							foreach ($group as $user):
							$class = '';
							if ($i++ % 2 == 0) {
								$class = 'altrow';
							}
							
							// check if user account enables
							$exp = $user['User']['expire_account'];
							
							if ($user['User']['disable'] or ($exp != '0000-00-00' and $this->Time->fromString($exp) < time()))
							$class = " class=\"{$class} disabled\"";
							else
							$class = " class=\"{$class}\"";
							
						?>
						<tr class="odd gradeX">
							<td>
								<?php echo $user['tblProfile']['CompanyName']; ?>
							</td>
							<td>
								<?php $email = $user['User']['email']; echo "<a href=\"mailto:{$email}\">{$email}</a>"; ?>
							</td>
							<td class="center">
								<?php echo $this->Time->format('d/m/Y', $user['User']['created']); ?>&nbsp;
							</td>
							<td>
								<?php if ($user['User']['disable'])
								{
							echo '<span class="label label-important">Disabled</span>';
								} else {		
									$exp = $user['User']['expire_account'];
									if ($exp != '0000-00-00' && $this->Time->fromString($exp) < time()) echo '<span class="label label-success">Approved</span>';
								}
								?>
							</td>
							<td align="center">
										<a style="margin-right:5px;" href="<?php echo $this->Html->url(array('action'=>'edit', $user['User']['id'])); ?>">
											<i class="icon-pencil">
											</i>Edit </a>
										<a href="<?php echo $this->Html->url( array('action'=>'delete', $user['User']['id']))?>" data-confirm="WARNING: This will also delete all data related to userss.
										This cannot be undone.
										Are you sure you want to delete <?php echo $user['User']['login']; ?>?" data-disable-with="Deleting..." data-method="delete" rel="nofollow">
											<i class="icon-trash"> </i>
											Delete
										</a>
							</td>
						</tr>
						<?php $sr++; endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
