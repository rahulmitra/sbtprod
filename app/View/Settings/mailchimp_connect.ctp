<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/settings/">Settings</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Mailchimp Connect</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a> 
				</div>
			</div>
			<div class="portlet-body">
				<form role="form"  action="/settings/mailchimpConnect" id="submit_form_lyrisConnect" method="POST" class="form-horizontal">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Please check the MailChimp details. Please check below.
					</div>
					<?php if($isSuccess == 1) { ?>
					<div class="alert alert-danger">
						<button class="close" data-close="alert"></button>
						Not able to connect with MailChimp. Please try again.
					</div>
					<?php } ?>
					<?php if($isSuccess == 2) { ?>
					<div class="alert alert-success">
						<button class="close" data-close="alert"></button>
						You have successfully connected with MailChimp
					</div>
					<?php } ?>
					<?php if($isSuccess == 3) { ?>
					<div class="alert alert-success">
						<button class="close" data-close="alert"></button>
						You have already connected with MailChimp
					</div>
					<div class="form-group">
						<label for="inputEmail1" class="col-md-2 control-label">MailChimp API Key</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="data[tblUserSettings][ApiKey]" value="" id="inputApiKey" placeholder="API Key">
						</div>
					</div>
				
				
					<?php } else { ?>
					<div class="form-group">
						<label for="inputEmail1" class="col-md-2 control-label">MailChimp API Key</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="data[tblUserSettings][ApiKey]" id="inputApiKey" placeholder="API Key">
						</div>
					</div>
				
				
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<button type="submit" class="btn blue">Connect</button>
						</div>
					</div>
					<?php } ?>
				</form>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->