<?php $this->Html->addCrumb('Advertisers', $this->Html->url( null, true )); ?>
<!-- BEGIN PAGE CONTENT INNER -->
<style>
	.pricing-footer {
		background:#fff;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-2">
						<ul class="ver-inline-menu tabbable margin-bottom-10">
							<li class="active">
								<a data-toggle="tab" href="#tab_1">
								<i class="fa fa-group"></i> USERS </a>
								<span class="after">
								</span>
							</li>
							<li>
								<a data-toggle="tab" href="#tab_2">
								<i class="fa fa-check "></i> INTEGRATIONS </a>
							</li>
							<li>
								<a data-toggle="tab" href="#tab_3">
								<i class="fa fa-bank"></i> COMPANIES </a>
							</li>
							<li>
								<a data-toggle="tab" href="#tab_4">
								<i class="fa fa-dollar"></i> BILLING </a>
							</li>
						</ul>
					</div>
					<div class="col-md-10">
						<div class="tab-content">
							<div id="tab_1" class="tab-pane active">
								<!-- BEGIN SAMPLE TABLE PORTLET-->
								<div class="table-scrollable">
									<table class="table table-hover">
										<thead>
											<tr>
												<th>
													#
												</th>
												<th>
													First Name
												</th>
												<th>
													Last Name
												</th>
												<th>
													Username
												</th>
												<th>
													Status
												</th>
											</tr>
										</thead>
										<tbody>
										
										</tbody>
									</table>
								</div>								
								<!-- END SAMPLE TABLE PORTLET-->
							</div>
							<div id="tab_2" class="tab-pane">
								<div class="panel-group accordion" id="accordion3">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
												Email Service Providers</a>
											</h4>
										</div>
										<div id="collapse_3_1" style="background:#eee;" class="panel-collapse in">
											<div class="panel-body">	
												<div class="row">
													<div class="col-md-12">
														
														<?php foreach($esps as $esp) { ?>
														<div class="col-md-4">
															<div class="pricing hover-effect">
																<div class="pricing-head">
																	<h3><img  height="40" src="<?php echo $esp['tblEmailServiceProvider']['esp_logourl']; ?>" />  <span style="color:#000;">
																	1 Account Connected.</span>
																	</h3>
																	
																</div>
																
																<div class="pricing-footer" style="padding: 0 16px 8px !important;">
																	<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																	<?php if($isSuccessLyris == 1) { ?>
																		<div class="row">
																		<div class="col-md-12" style="margin-top: 10px;margin-bottom: 11px;">
																			<a href="/settings/lyrisConnect/" class="btn green">
																				Manage Integrations<i class="m-icon-swapright m-icon-white"></i>
																			</a>
																		</div>
																		<div class="col-md-12">
																			<a href="#" class="btn red">
																				Connect More <i class="m-icon-swapright m-icon-white"></i>
																			</a>
																		</div>
																		</div>
																		<?php } else { ?>
																		<?php if( $esp['tblEmailServiceProvider']['esp_id'] == '4') {?>
																		<a href="/settings/mailchimpConnect/"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																		<?php } else { ?>
																			<a href="/settings/lyrisConnect/"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																		<?php } ?>
																	<?php } ?>
																</div>
																</div>
															</div>
														<?php } ?>
															
														</div>
														<!--/span-->
													</div>
													
													
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2">
													Social Networks </a>
												</h4>
											</div>
											<div id="collapse_3_2" class="panel-collapse collapse">
												<div class="panel-body" style="background:#eee;">
													<div class="row">
														<div class="col-md-12">
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/Twitter_logo_blue.png" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href='javascript:;' onclick="javascript:window.open('http://services.digitaladvertising.systems/twitterconfirm.aspx?d_profid=5', 'socialwindow','location=1,status=1,scrollbars=1, width=500,height=600');"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/Facebook_logo_(square).png" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;" onclick="javascript:window.open('http://services.digitaladvertising.systems/fbconfirm.aspx?d_profid=5', 'socialwindow','location=1,status=1,scrollbars=1, width=500,height=600');"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img src="/img/instagram.png" height="40"  />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;" onclick="javascript:window.open('http://services.digitaladvertising.systems/instagram-confirm.aspx?d_profid=5', 'socialwindow','location=1,status=1,scrollbars=1, width=500,height=600');" class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img src="/img/pinterest-logo.png" height="40"  />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
														</div>
														<!--/span-->
													</div>
												</div>
											</div>
										</div>
										<!--code by faiz-->
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3">
													Ad Servers  <span style="margin-left:20px;" id="total"></span> </a>
												</h4>
											</div>
											<div id="collapse_3_3" class="panel-collapse collapse">
												<div class="panel-body"  style="background:#eee;">
													<div class="row">
														<div class="col-md-12">
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/dclk-logo-dfp-small-business.png" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img  height="40"  src="/img/dclk-logo-dfp.png" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
														</div>
														<!--/span-->
													</div>
												</div>
											</div>
										</div>
										<!--end code-->
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4">
													Ad Exchanges, DSPs & SSPs  <span style="margin-left:20px;" id="total"></span> </a>
												</h4>
											</div>
											<div id="collapse_3_4" class="panel-collapse collapse">
												<div class="panel-body"  style="background:#eee;">
													<div class="row">
														<div class="col-md-12">
															
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/Logo_Rubicon_Project_2014_Official.svg.png" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/Pubmatic-Logo.png" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/DataXu-logo.jpg" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/LiveIntent.png" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/powerinbox-logo.jpg" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
											</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_5">
													Data Management Partners  <span style="margin-left:20px;" id="total"></span> </a>
												</h4>
											</div>
											<div id="collapse_3_5" class="panel-collapse collapse">
												<div class="panel-body">
													<div class="row">
														<div class="col-md-12">
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/acxiom-logo-ret.png" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/edq-logo.png" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
															<div class="col-md-4">
																<div class="pricing hover-effect">
																	<div class="pricing-head">
																		<h3><img height="40"  src="/img/KMA-logo.png" />  <span>
																		Connect With Google DFP </span>
																		</h3>
																	</div>
																	
																	<div class="pricing-footer">
																		<p style="visibility:hidden;">
																			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .
																		</p>
																		<a href="javascript:;"  class="btn red">
																			Connect Now <i class="m-icon-swapright m-icon-white"></i>
																		</a>
																	</div>
																</div>
															</div>
														</div>
														<!--/span-->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="tab_3" class="tab-pane">
									<!-- BEGIN EXAMPLE TABLE PORTLET-->
									<div class="table-toolbar">
										<div class="row">
											<div class="col-md-6">
												<div class="btn-group">
													<a href="/company/add" id="sample_editable_1_new" class="btn green">
														Add New <i class="fa fa-plus"></i>
													</a>
												</div>
											</div>
											<div class="col-md-6">
											</div>
										</div>
									</div>
									<table class="table table-striped table-bordered table-hover" id="sample_1">
										<thead>
											<tr>
												<th>
													Company Name
												</th>
												<th>
													Email
												</th>
												<th>
													Joined
												</th>
												<th>
													Status
												</th>
												<th class="center" style="text-align:center;">
													Action
												</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$i = 0;
												$sr = 1;
												foreach ($group as $user):
												$class = '';
												if ($i++ % 2 == 0) {
													$class = 'altrow';
												}
												
												// check if user account enables
												$exp = $user['User']['expire_account'];
												
												if ($user['User']['disable'] or ($exp != '0000-00-00' and $this->Time->fromString($exp) < time()))
												$class = " class=\"{$class} disabled\"";
												else
												$class = " class=\"{$class}\"";
												
											?>
											<tr class="odd gradeX">
												<td>
													<?php echo $user['tblProfile']['CompanyName']; ?>
												</td>
												<td>
													<?php $email = $user['User']['email']; echo "<a href=\"mailto:{$email}\">{$email}</a>"; ?>
												</td>
												<td class="center">
													<?php echo $this->Time->format('d/m/Y', $user['User']['created']); ?>&nbsp;
												</td>
												<td>
													<?php if ($user['User']['disable'])
														{
															echo '<span class="label label-important">Disabled</span>';
															} else {		
															$exp = $user['User']['expire_account'];
															if ($exp != '0000-00-00' && $this->Time->fromString($exp) < time()) echo '<span class="label label-success">Approved</span>';
														}
													?>
												</td>
												<td align="center">
													<div class="btn-group">
														<a data-toggle="dropdown" href="javascript:;" class="btn btn-circle btn-default" aria-expanded="true">
															<i class="fa fa-share"></i> Action <i class="fa fa-angle-down"></i>
														</a>
														<ul class="dropdown-menu pull-right">
															<li>
															<a style="margin-right:5px;" href="<?php echo $this->Html->url(array('controller'=>'insertionorder','action'=>'add/company_id:'. $user['tblProfile']['dfp_company_id'])); ?>">
																<i class="fa fa-codepen">
																</i>Create Order </a>
															</li>	
															<li>
																<a style="margin-right:5px;" href="<?php echo $this->Html->url(array('controller'=>'orders','action'=>'index', $user['User']['id'])); ?>">
																	<i class="fa fa-codepen">
																	</i>View Order</a>
															</li>
															<li>
																<a style="margin-right:5px;" href="<?php echo $this->Html->url(array('controller'=>'inventory_management','action'=>'index', $user['User']['id'])); ?>">
																<i class="fa fa-database">
																	</i>Manage Ad-Units</a>
															</li>
															<li>
																<a style="margin-right:5px;" href="<?php echo $this->Html->url(array('controller'=>'company','action'=>'edit', $user['User']['id'])); ?>">
																<i class="icon-pencil">
																	</i>Edit</a>
															</li>
															 
												 
														</ul>
													</div>
													 
												</td>
											</tr>
											<?php $sr++; endforeach; ?>
										</tbody>
									</table>
									
									<!-- END EXAMPLE TABLE PORTLET-->
								</div>
								<div id="tab_4" class="tab-pane">
									Coming Soon
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->			
<style>
	.panel-default>.panel-heading {
	background-color : #73716e !important;
	color : #fff;
	}
	
	.panel-default>.panel-heading > .panel-title > a:hover {
	color : #fff;
	text-decoration:underline;
	}
	
	.panel-title{
	font-weight:200 !important;
	}
</style>											