		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" valign="top" bgcolor="#e5e3ce" style="background-color:#e5e3ce;"><br>
					<br>
					<table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td align="center" valign="middle"><img src="http://ads-demo.siliconbiztech.com/email_template/9/images/top.jpg" width="730" height="27" style="display:block;"></td>
						</tr>
						<tr>
							<td align="center" valign="middle" bgcolor="#2c817c" style="background-color:#2c817c; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:20px;">
								<tr>
									<td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif;">
										<div style="font-size:48px; color:#f9e4ad;"><b>My Investor News & Reports</b></div>
										<div style="font-size:18px; color:#ffffff;">Free daily newsletter that offers investment reports and trading strategies</div>
										
									</td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td align="center" valign="middle" height="20"></td>
						</tr>
						<tr>
							<td align="center" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="left" valign="top" bgcolor="#dda51c" style="background-color:#dda51c; padding:8px; font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#ffffff;"><b>Here are your updates for today...</b></td>
										</tr>
										<?php echo $fullhtml; ?>
									</table>
									</td>
								</tr>
							</table>
							<br>
							<br>
							<center>
								<p align="center" style="margin-top:0em;margin-bottom:0em;font-size:8px">
									<font face="Verdana, Arial, Helvetica, sans-serif"><span style="font-size:8pt;font-family:Arial">DISCLAIMER:<br>
										The content on this email is a combination of paid sponsorships and promoted articles. MyInvestorNewsandReports.com is not affiliated with nor does it endorse any trading system, newsletter or other similar service. MyInvestorNewsandReports.com does not guarantee or verify any performance claims made by such systems, newsletters or services. Trading involves a significant and substantial risk of loss and may not be suitable for everyone. You should only trade with money you can afford to lose. There is no guarantee that you will profit from your trading activity and it is possible that you may lose all of, or if trading on margin more than, your investment. Some of the results shown may be based on simulated performance. SIMULATED OR HYPOTHETICAL PERFORMANCE RESULTS HAVE CERTAIN INHERENT LIMITATIONS. UNLIKE THE RESULTS SHOWN IN AN ACTUAL PERFORMANCE RECORD, SUCH RESULTS DO NOT REPRESENT ACTUAL TRADING. ALSO, BECAUSE THE TRADES HAVE NOT ACTUALLY BEEN EXECUTED, THE RESULTS MY HAVE UNDER OR OVER-COMPENSATED FOR THE IMPACT, IF ANY, OF CERTAIN MARKET FACTORS, SUCH AS LACK OF LIQUIDITY. SIMULATED OR HYPOTHETICAL PROGRAMS IN GENERAL ARE ALSO SUBJECT TO THE FACT THAT THEY ARE DESIGNED WITH THE BENEFIT OF HINDSIGHT. NO REPRESENTATION IS BEING MADE THAT ANY ACCOUNT WILL OR IS LIKELY TO ACHIEVE PROFITS OR LOSSES SIMILAR TO THOSE SHOWN. Past performance is not necessarily indicative of future performance. This brief statement cannot disclose all the risks and other significant aspects of trading. You should carefully study trading and consider whether such activity is suitable for you in light of your circumstances and financial resources before you trade. 
										<br><br>
										This message is considered by regulation to be a commercial and advertising message. This is a permission-based message. You are receiving this email either because you opted-in to this subscription or because you have a prior existing relationship with MyInvestorNewsandReports.com or one of its subsidiaries, and previously provided your email address to us. This email fully complies with all laws and regulations. If you do not wish to receive this email, then we apologize for the inconvenience. You can immediately discontinue receiving this email by clicking on the un-subscribe or profile link and you will no longer receive this email. We will immediately redress any complaints you may have. If you have any questions, please send an email with your questions to newsletters@MyInvestorNewsandReports.com
										<br><br>
										MyInvestorNewsandReports.com does not recommend or promote any stock tickers, symbols or any such identifier.<br><br>
									<?php echo $physical_address; ?><br/><br><?php echo $unsubscribe_link; ?>.</span></font></p>
									
							</center>
							</td>
						</tr>
					</table>
					<br>
					<br>
				</td>
			</tr>
		</table>
