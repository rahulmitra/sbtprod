<table border='0' style='border: none !important;width: 100%;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;' bgcolor='#FFFFFF'>
			<tr>
				<td></td>
				<td class='header container' style='display:block!important;width:750px!important;margin:0 auto!important;clear:both!important;' >
					
					<div class='content' style='width:750px;margin:0 auto;display:block;border-bottom:3px solid #000;'>
						<table  border='0' style='width: 100%;height:70px;' bgcolor='#FFFFFF'>
							<tr>
								<td><img src="http://quote.stockquotes.finance/assets/images/otclogo.png" />
								</td>
								<td style='padding-top:25px'>
								</td>
								<td  style='padding-top:25px' align='right'><h6 class='collapse' style='font-family: HelveticaNeue-Light, Helvetica Neue Light, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;margin:0;padding:0;'><?php echo date("F j, Y");  ?></h6></td>
							</tr>
						</table>
						
					</div>
					
				</td>
				<td></td>
			</tr>
		</table><!-- /HEADER -->
		
		<table  border='0' style='width: 100%;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;' bgcolor='#FFFFFF'>
			<tr>
				<td></td>
				<td class='header container' style='display:block!important;width:750px!important;margin:0 auto!important;margin-bottom:25px;clear:both!important;' >
					
					<div class='content' style='width:750px;margin:0 auto;display:block; '>
						<table  style='width: 100%;height:110px;' bgcolor='#FFFFFF'>
							<tr>
								<td>
									<b style='font-size:12px;margin-left:10px;'>TODAY'S NEWSLETTER BROUGHT TO YOU BY</b>
								</td>
							</tr>
							<tr>
								<td align='center'>
									<div class='content' style='width: 750px; display: block; text-align: left; margin: 8px 0 5px; font-size: 16px;'>
										<span style='font-size:20px;font-weight:bold;'><?php echo $sponsoredTitle; ?></span><br />
										<div><?php echo $sponsoredBody ?> <a href="<?php echo $sponsoredClickURL ?>"><?php echo $sponsoredClickCTA ?></a>
										</div>
									</div><br><br>
									<img width="1" height="1" src="<?php echo $sponsoredImpression ?>">
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td  align='center'>
					<div class='content' style='width: 750px; display: block; text-align: left; margin: 8px 0 5px; font-size: 16px;'>
						<span style='font-size:22px;font-weight:bold;'>WE DO THE RESEARCH ON 
						PENNY STOCKS</span><br />
						OTC Rockstar has made it our business to pin point the best possible hot penny stocks on the market.When you win, we win. Because we consider your win, our win, it can be said that we work for you. 
					</div>
				</td>
			</tr>
		</table>
		
		<table style='width: 100%;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;margin-bottom:10px;margin-top:10px;' bgcolor='#FFFFFF'>
			<tr>
				<td></td>
				<td class='header container' style='display:block!important;width:750px!important;margin:0 auto!important;clear:both!important;' >
					
					<div class='content' style='width:750px;margin:0 auto;display:block; '>
						<table  style='width: 750px;border: solid 1px #e9e9e9;height:110px;' bgcolor='#FFFFFF'>
							<tr>
								<td>
									<div class='bar' style='border-bottom: solid 2px #000;font-size:14px;'><h2 style='margin:0px;'>Top 10 Gainer Today</h2></div>
									<div class='mpbox'>
										<div class='xscroll'>
											<table class='datatable js' id='dt2' width='100%' border='0' cellpadding='1' cellspacing='1'><thead>  <tr style='font-size:14px;' class='datatable_header' id='dt2_column'>  
												<th rel='text' id='dt12_column_name' width="10%" align='left' class='ds_name sort_none'>Symbol</th>
												<th rel='text' id='dt12_column_name' width="20%"  align='left' class='ds_name sort_none'>Company</th>
												<th rel='text' id='dt12_column_name' width="10%" align='left' class='ds_name sort_none'>Sector</th>
												<th rel='text' id='dt12_column_name' width="20%" align='left' class='ds_name sort_none'>Industry</th>
												<th rel='price' id='dt12_column_last' width="10%" align='center' class='ds_last sort_none'><abbr title='Last Price: The last trade price.'>Last</abbr></th>
												<th rel='change' id='dt12_column_change' width="10%" align='center' class='ds_change sort_none'><abbr title='Change: The difference between the current price and the previous day's settlement price.'>Change</abbr></th>
												<th rel='price' id='dt12_column_high' width="10%" align='center' class='ds_high sort_none'><abbr title='High Price: The highest trade price for the day.'>High</abbr></th>
												<th rel='price' id='dt12_column_low' width="10%"	q1``aw align='center' class='ds_low sort_none'><abbr title='Low Price: The lowest trade price for the day.'>Low</abbr></th>
											</tr>
											</thead><tbody> 
											<?php echo  $content['gainers']; ?>
											</tbody></table>
										<div class='loading' id='ldt2'></div></div>
									</div>	
									
								</td>
							</tr>
						</table>
						
					</div>
					
				</td>
				<td></td>
			</tr>
		</table><!-- /HEADER -->
		
		<table border='0' style='width: 100%;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;' bgcolor='#FFFFFF'>
			<tr>
				<td></td>
				<td class='header container' style='display:block!important;width:750px!important;margin:0 auto!important;clear:both!important;' >
					
					<div class='content' style='width:750px;margin:0 auto;display:block; '>
						<table  style='width: 100%;' bgcolor='#FFFFFF'>
							<tr>
								<td style='background-color:#ffffff;'>
									<div style='font-family:Arial, sans-serif;font-size:15px; line-height:135%; border:2px solid #0B6BBB; padding:15px 20px;'>
										<div style='font-size:18px; font-weight:600; text-align:center; line-height:22px; margin-bottom:10px;'><?php echo $sponsoredTitle1; ?></div>
										<?php echo $sponsoredBody1; ?><br><br> <center><a target="_blank" href="<?php echo $sponsoredClickURL1 ?>"><?php echo $sponsoredClickCTA1 ?></a></center><br><br>
									</div>
									<img src="<?php echo $sponsoredImpression1; ?>" width="1" height="1" style="display:none;" />
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
		<!-- BODY -->
		<table style='width: 100%;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;margin-bottom:10px;margin-top:10px;' bgcolor='#FFFFFF'>
			<tr>
				<td></td>
				<td class='header container' style='display:block!important;width:750px!important;margin:0 auto!important;clear:both!important;' >
					
					<div class='content' style='width:750px;margin:0 auto;display:block; '>
						<table  style='width: 750px;border: solid 1px #e9e9e9;height:110px;' bgcolor='#FFFFFF'>
							<tr>
								<td>
									<div class='bar' style='border-bottom: solid 2px #000;font-size:14px;'><h2 style='margin:0px;'>
									Top10 Losers Today	</h2></div>
									<div class='mpbox'>
										
										<div class='xscroll'>
											<table class='datatable js' id='dt1' width='100%' border='0' cellpadding='1' cellspacing='1'><thead>  
												<tr class='datatable_header' id='dt1_column'  style='font-size:14px;'>    
													<th rel='text' id='dt12_column_name' width="10%" align='left' class='ds_name sort_none'>Symbol</th>
													<th rel='text' id='dt12_column_name' width="20%"  align='left' class='ds_name sort_none'>Company</th>
													<th rel='text' id='dt12_column_name' width="10%" align='left' class='ds_name sort_none'>Sector</th>
													<th rel='text' id='dt12_column_name' width="20%" align='left' class='ds_name sort_none'>Industry</th>
													<th rel='price' id='dt12_column_last' width="10%" align='center' class='ds_last sort_none'><abbr title='Last Price: The last trade price.'>Last</abbr></th>
													<th rel='change' id='dt12_column_change' width="10%" align='center' class='ds_change sort_none'><abbr title='Change: The difference between the current price and the previous day's settlement price.'>Change</abbr></th>
													<th rel='price' id='dt12_column_high' width="10%" align='center' class='ds_high sort_none'><abbr title='High Price: The highest trade price for the day.'>High</abbr></th>
													<th rel='price' id='dt12_column_low' width="10%"	q1``aw align='center' class='ds_low sort_none'><abbr title='Low Price: The lowest trade price for the day.'>Low</abbr></th>
												</tr>
												</thead><tbody>  
												<?php echo  $content['losers']; ?> 
											</tbody></table>
										</div>
									</div>	
									
								</td>
							</tr>
						</table><br /><br /><br /><br />
						<center>
							<p align="center" style="margin-top:0em;margin-bottom:0em;font-size:8px">
								<font face="Verdana, Arial, Helvetica, sans-serif"><span style="font-size:8pt;font-family:Arial">DISCLAIMER:<br>
									The content on this email is a combination of paid sponsorships and promoted articles. OTCRockStar.com is not affiliated with nor does it endorse any trading system, newsletter or other similar service. OTCRockStar.com does not guarantee or verify any performance claims made by such systems, newsletters or services. Trading involves a significant and substantial risk of loss and may not be suitable for everyone. You should only trade with money you can afford to lose. There is no guarantee that you will profit from your trading activity and it is possible that you may lose all of, or if trading on margin more than, your investment. Some of the results shown may be based on simulated performance. SIMULATED OR HYPOTHETICAL PERFORMANCE RESULTS HAVE CERTAIN INHERENT LIMITATIONS. UNLIKE THE RESULTS SHOWN IN AN ACTUAL PERFORMANCE RECORD, SUCH RESULTS DO NOT REPRESENT ACTUAL TRADING. ALSO, BECAUSE THE TRADES HAVE NOT ACTUALLY BEEN EXECUTED, THE RESULTS MY HAVE UNDER OR OVER-COMPENSATED FOR THE IMPACT, IF ANY, OF CERTAIN MARKET FACTORS, SUCH AS LACK OF LIQUIDITY. SIMULATED OR HYPOTHETICAL PROGRAMS IN GENERAL ARE ALSO SUBJECT TO THE FACT THAT THEY ARE DESIGNED WITH THE BENEFIT OF HINDSIGHT. NO REPRESENTATION IS BEING MADE THAT ANY ACCOUNT WILL OR IS LIKELY TO ACHIEVE PROFITS OR LOSSES SIMILAR TO THOSE SHOWN. Past performance is not necessarily indicative of future performance. This brief statement cannot disclose all the risks and other significant aspects of trading. You should carefully study trading and consider whether such activity is suitable for you in light of your circumstances and financial resources before you trade. 
									<br><br>
									This message is considered by regulation to be a commercial and advertising message. This is a permission-based message. You are receiving this email either because you opted-in to this subscription or because you have a prior existing relationship with OTCRockStar.com or one of its subsidiaries, and previously provided your email address to us. This email fully complies with all laws and regulations. If you do not wish to receive this email, then we apologize for the inconvenience. You can immediately discontinue receiving this email by clicking on the unsubscribe or profile link and you will no longer receive this email. We will immediately redress any complaints you may have. If you have any questions, please send an email with your questions to newsletters@OTCRockStar.com
									<br><br>
									OTCRockStar.com does not recommend or promote any stock tickers, symbols or any such identifier.<br><br>
									<?php echo $physical_address; ?>
									<br/><br><?php echo $unsubscribe_link; ?>.
								</span></font></p>
						</center>
					</div>
					
				</td>
				<td></td>
			</tr>
		</table>
<?php echo $nlpixel; ?>