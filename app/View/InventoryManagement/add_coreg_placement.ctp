<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/inventory_management/">Inventory Management</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/inventory_management/all_placements">All Placements</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet light" id="form_wizard_1">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
					<i class="fa fa-gift"></i> Build a New Co-Registration placement </span>
				</span>
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse">
				</a>
				<a href="#portlet-config" data-toggle="modal" class="config">
				</a>
				<a href="javascript:;" class="reload">
				</a>
				<a href="javascript:;" class="remove">
				</a>
			</div>
		</div>
		<div class="portlet-body form">
			<form action="javascript:;" class="form-horizontal" id="submit_form" method="POST">
				<div class="form-wizard">
					<div class="form-body">
						<ul class="nav nav-pills nav-justified steps">
							<li>
								<a href="#tab1" data-toggle="tab" class="step">
									<span class="number">
									1 </span>
									<span class="desc">
									<i class="fa fa-check"></i> Layout </span>
								</a>
							</li>
							<li>
								<a href="#tab2" data-toggle="tab" class="step">
									<span class="number">
									2 </span>
									<span class="desc">
									<i class="fa fa-check"></i> Field Mapping </span>
								</a>
							</li>
							<li>
								<a href="#tab3" data-toggle="tab" class="step active">
									<span class="number">
									3 </span>
									<span class="desc">
									<i class="fa fa-check"></i>Audience Profiling </span>
								</a>
							</li>
							<li>
								<a href="#tab4" data-toggle="tab" class="step">
									<span class="number">
									4 </span>
									<span class="desc">
									<i class="fa fa-check"></i> Script </span>
								</a>
							</li>
						</ul>
						<div id="bar" class="progress progress-striped" role="progressbar">
							<div class="progress-bar progress-bar-success">
							</div>
						</div>
						<div class="tab-content">
							<div class="alert alert-danger display-none">
								<button class="close" data-dismiss="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-none">
								<button class="close" data-dismiss="alert"></button>
								Your form validation is successful!
							</div>
							
							
							<div class="tab-pane active" id="tab1">
								<div class="row">
									<div class="col-sm-12">
										<div  class="row">
											<div class="col-sm-5">
												<h4>Publisher/placement</h4>
												<p><input type="text" value="" class="form-control demo"></p>
												<p>Start Typing to Make a Selection</p>
											</div>
											<div class="col-sm-6">
												<h4>Select Style</h4>
												<div class="style-section">
													<p><input type="radio" name="radio" id="radio"> <label for="radio">In-Page (In-Line, Post-Reg etc.)</label></p>
													<p><input type="radio" name="radio" id="radio2"> <label for="radio2">Out of Page (Pop-Up, Interlay, FlyGuy etc.)</label></p>
													<p><input type="radio" name="radio" id="radio3"> <label for="radio3">In Email</label></p>
												</div>
											</div>
										</div>
										
										<br>
										
										<h2>Template</h2>
										<div class="tabs-sectoin">
											<ul class="nav nav-tabs">
												<li class="active"><a data-toggle="tab" href="#content-1"><img src="/img/images/1.png" alt=""></a></li>
												<li><a data-toggle="tab" href="#content-2"><img src="/img/images/1.png" alt=""></a></li>
												<li><a data-toggle="tab" href="#content-3"><img src="/img/images/2.png" alt=""></a></li>
												<li><a data-toggle="tab" href="#content-4"><img src="/img/images/3.png" alt=""></a></li>
												<li><a data-toggle="tab" href="#content-5"><img src="/img/images/1.png" alt=""></a></li>
												<li><a data-toggle="tab" href="#content-6"><img src="/img/images/2.png" alt=""></a></li>
											</ul>
										</div>
										
										<div class="box-info">
											
											<div class="row">
												<div class="col-sm-4">
													<div class="row">
														<div class="col-xs-4"><label class="mt-5">Width</label></div>
														<div class="col-xs-8">
															<p><input type="text" value="" class="form-control demo"><p>
															</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="row">
																<div class="col-xs-3"><label class="mt-5">Height</label></div>
																<div class="col-xs-9">
																	<p><input type="text" value="" class="form-control demo"><p>
																	</div>
																	</div>
																</div>
																<div class="col-sm-2"><label class="mt-5">Pixel</label></div>
															</div>
															
															<div class="row">
																<div class="col-sm-4">
																	<div class="row">
																		<div class="col-xs-4"><label class="mt-5"># of Offers</label></div>
																		<div class="col-xs-8">
																			<p><input type="text" value="" class="form-control demo"><p>
																			</div>
																			</div>
																		</div>
																	</div>
																	
																	
																	<div class="row">
																		<div class="col-sm-6">
																			<br><br>
																			<div class="row">
																				<div class="col-xs-4"><label class="mt-5">Submit Button Text</label></div>
																				<div class="col-xs-6">
																					<input type="text" value="" class="form-control demo">
																				</div>
																			</div>
																			<br>
																			<div class="row">
																				<div class="col-xs-4"></div>
																				<div class="col-xs-6">
																					<p>
																						<input type="submit" value="Choose from a pre-designed style" class="btn btn-success">
																					</p>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<h4>Button Size</h4>
																			<p>
																				<input type="radio" name="size" id="size1">
																				<label for="size1">S</label>
																				&nbsp;&nbsp;&nbsp;
																				<input type="radio" name="size" id="size2">
																				<label for="size2">M</label>
																				&nbsp;&nbsp;&nbsp;
																				<input type="radio" name="size" id="size3">
																				<label for="size3">L</label>
																				&nbsp;&nbsp;&nbsp;
																				<input type="radio" name="size" id="size4">
																				<label for="size4">XL</label>
																			</p>
																			<div class="form-group row">
																				<label class="control-label col-md-3">Button Color</label>
																				<div class="col-md-5">
																					<div class="input-group color colorpicker-default" data-color="#3865a8" data-color-format="rgba">
																						<input type="text" class="form-control" value="#3865a8" readonly>
																						<span class="input-group-btn">
																							<button class="btn default" type="button"><i style="background-color: #3865a8;"></i>&nbsp;</button>
																						</span>
																					</div>
																					<!-- /input-group -->
																				</div>
																			</div>
																			<div class="form-group row">
																				<label class="control-label col-md-3">Text Color</label>
																				<div class="col-md-5">
																					<div class="input-group color colorpicker-default" data-color="#3865a8" data-color-format="rgba">
																						<input type="text" class="form-control" value="#3865a8" readonly>
																						<span class="input-group-btn">
																							<button class="btn default" type="button"><i style="background-color: #3865a8;"></i>&nbsp;</button>
																						</span>
																					</div>
																					<!-- /input-group -->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																
															</div>
															
															<div class="col-sm-12">
																<h2>Preview Window</h2>
																
																<div class="tab-content">
																	<div class="tab-pane active" id="content-1">
																		<img src="/img/images/1.png" alt="">
																	</div>
																	<div class="tab-pane fade" id="content-2">
																		<img src="/img/images/2.png" alt="">
																	</div>
																	<div class="tab-pane fade" id="content-3">
																		<img src="/img/images/3.png" alt="">
																	</div>
																	<div class="tab-pane fade" id="content-4">
																		<img src="/img/images/1.png" alt="">
																	</div>
																	<div class="tab-pane fade" id="content-5">
																		<img src="/img/images/2.png" alt="">
																	</div>
																	<div class="tab-pane fade" id="content-6">
																		<img src="/img/images/3.png" alt="">
																	</div>
																</div>
																
															</div>
														</div>
													</div><!--tab1 -->
													
													
													<div class="tab-pane" id="tab2">
														<div class="portlet grey-cascade box">
															<div class="portlet-title">
																<div class="caption">Add/Modify Field Mapping</div>
															</div>
															<div class="portlet-body">
																<div class="table-responsive">
																	<table class="table table-hover table-bordered table-striped">
																		<thead>
																			<tr>
																				<th>Field</th>
																				<th>Available in placement</th>
																				<th>Field Lable in Publisher  Placement</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td>First Name</td>
																				<td><input type="checkbox" id="checkbox"> <label for="checkbox">Yes</label></td>
																				<td><input type="text" value="" class="form-control demo auto-w"> Save</td>
																			</tr>
																			<tr>
																				<td>Last Name</td>
																				<td><input type="checkbox" id="checkbox"> <label for="checkbox">Yes</label></td>
																				<td><input type="text" value="" class="form-control demo auto-w"> Save</td>
																			</tr>
																			<tr>
																				<td>Email</td>
																				<td><input type="checkbox" id="checkbox"> <label for="checkbox">Yes</label></td>
																				<td><input type="text" value="" class="form-control demo auto-w"> Save</td>
																			</tr>
																			<tr>
																				<td>Address 1</td>
																				<td><input type="checkbox" id="checkbox"> <label for="checkbox">Yes</label></td>
																				<td><input type="text" value="" class="form-control demo auto-w"> Save</td>
																			</tr>
																			<tr>
																				<td>Address 2</td>
																				<td><input type="checkbox" id="checkbox"> <label for="checkbox">Yes</label></td>
																				<td><input type="text" value="" class="form-control demo auto-w"> Save</td>
																			</tr>
																			<tr>
																				<td>First Name</td>
																				<td><input type="checkbox" id="checkbox"> <label for="checkbox">Yes</label></td>
																				<td><input type="text" value="" class="form-control demo auto-w"> Save</td>
																			</tr>
																			<tr>
																				<td>Last Name</td>
																				<td><input type="checkbox" id="checkbox"> <label for="checkbox">Yes</label></td>
																				<td><input type="text" value="" class="form-control demo auto-w"> Save</td>
																			</tr>
																			<tr>
																				<td>Email</td>
																				<td><input type="checkbox" id="checkbox"> <label for="checkbox">Yes</label></td>
																				<td><input type="text" value="" class="form-control demo auto-w"> Save</td>
																			</tr>
																			<tr>
																				<td>Address 1</td>
																				<td><input type="checkbox" id="checkbox"> <label for="checkbox">Yes</label></td>
																				<td><input type="text" value="" class="form-control demo auto-w"> Save</td>
																			</tr>
																			<tr>
																				<td>Address 2</td>
																				<td><input type="checkbox" id="checkbox"> <label for="checkbox">Yes</label></td>
																				<td><input type="text" value="" class="form-control demo auto-w"> Save</td>
																			</tr>
																			
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div><!--tab2 -->
													
													<div class="tab-pane" id="tab3">
														<div class="panel panel-default">
															<div class="panel-heading">
																<h4 class="panel-title">
																	<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3">
																	Data Centers  <span style="margin-left:20px;" id="total"></span> </a>
																</h4>
															</div>
															<div id="collapse_3_3" class="panel-collapse collapse">
																<div class="panel-body">
																	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group">
																				<div class="col-md-12">
																					<select multiple="multiple" id="audience-select"  class="form-control" name="audience-select[]" size="10">
		<option value='Acxiom'>Acxiom</option>
		<option value='Experian'>Experian</option>
		<option value='Exelate'>Exelate</option>
		<option value='DataXU'>DataXU</option>
		</select>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																</div>
															</div>
														</div>
													</div><!--tab3 -->
													
													<div class="tab-pane" id="tab4">
														<div class="row">
															<div class="col-sm-6">
																<p>
																	<h4>Iframe Code</h4>
																	<textarea rows="5" cols="5" class="form-control demo"></textarea>
																</p>
															</div>
															<div class="col-sm-6">
																<p>
																	<h4>Javascript Code</h4>
																	<textarea rows="5" cols="5" class="form-control demo"></textarea>
																</p>
															</div>
														</div>
													</div><!--tab4 -->
													
													
													
												</div>
											</div>
											<div class="form-actions">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<a href="javascript:;" class="btn default button-previous">
														<i class="m-icon-swapleft"></i> Back </a>
														<a href="javascript:;" class="btn blue button-next">
															Continue <i class="m-icon-swapright m-icon-white"></i>
														</a>
														<a href="javascript:;" class="btn green button-submit">
															Submit <i class="m-icon-swapright m-icon-white"></i>
														</a>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- END PAGE CONTENT INNER -->
					<style>
						.style-section label{
						font-size:12px;
						}
						.style-section p{
						margin:0 0 5px;
						}
						.tabs-sectoin{
						}
						.tabs-sectoin .nav-tabs img{
						max-height:55px;
						}
						.box-info{
						padding:30px;
						}
						.box-info label,
						.box-info .field{
						display:inline-block;
						vertical-align:middle;
						}
						.box-info .field{
						border:1px solid #e5e5e5;
						margin:0 5px;
						}
						.tab-content img{
						max-width:100%;
						}
						.auto-w{
						width:auto;
						display:inline-block;
						}
						.mt-5{
						margin-top:5px !important;
						}
					</style>			<style>
	.panel-default>.panel-heading {
	background-color : #73716e !important;
	color : #fff;
	}
	
	.panel-default>.panel-heading > .panel-title > a:hover {
	color : #fff;
	text-decoration:underline;
	}
	
	.panel-title{
	font-weight:200 !important;
	}
	</style>																					