<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/inventory_management/">Inventory Management</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/inventory_management/all_placements">All Placements</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Placement</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<a href="/inventory_management/addAdPlacement" id="sample_editable_1_new" class="btn green">
									Add New <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						<div class="col-md-6">
						</div>
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
						<tr>
							<th class="table-checkbox">
								PID
							</th>
							<th>
								Publisher Name
							</th>
							<th>
								Placement Name
							</th>
							<th>
								Mapped AdUnits
							</th>
							<th>
								Placement DFP ID
							</th>
							<th>
								Create Date
							</th>
							<th class="center" style="text-align:center;">
								Action
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($group as $user):
							$class = '';
							if ($i++ % 2 == 0) {
								$class = 'altrow';
							}
							if($user['tblPlacement']['p_dfp_id'] != "") {
							
						?>
						<tr class="odd gradeX">
							<td>
								<?php echo $user['tblPlacement']['p_id']; ?>
							</td>
							<td>
								<?php echo $user['tblp']['CompanyName']; ?>
							</td>
							<td>
								<?php echo $user['tblPlacement']['p_name']; ?>
							</td>
							<td>
								<?php echo $user['tblPlacement']['p_adunits']; ?>
							</td>
							<td>
								<?php echo $user['tblPlacement']['p_dfp_id']; ?>
							</td>
							<td class="center">
								<?php echo $this->Time->format('d/m/Y', $user['tblPlacement']['p_created_at']); ?>&nbsp;
							</td>
							<td align="center">
										<a target="_blank" style="margin-right:5px;" href="https://www.google.com/dfp/1575221?gsessionid=wgGIdIIG2YJ0x3WyOj1VLg#inventory/placement/placementId=<?php echo $user['tblPlacement']['p_dfp_id']; ?>">
											<i class="icon-eye">
											</i> View </a>
							</td>
						</tr>
						<?php } endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
