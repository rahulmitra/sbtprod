<?php
	/**
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Pages
		* @since         CakePHP(tm) v 0.10.0.1076
	*/
//if (!Configure::read('debug')):
    //throw new NotFoundException();
//endif;

App::uses('Debugger', 'Utility');
?>
<style>
	.font-hg {
	color:#000 !important;
	font-size:48px !important;
	font-weight:bold !important;
	}
	.font-grey-mint {
	color:#000 !important;
	}
	.font-sm {
	font-size:16px !important;
	}
</style>
<!-- BEGIN PAGE BREADCRUMB -->
<div class="row">
	<div class="col-md-4">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="/">Home</a><i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="/inventory_management/">Ad Units</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				<a href="#"><?php echo $adunit_name; ?></a>
			</li>
		</ul>
	</div>
	<div class="col-md-4">
		<div align="center">
		<a href="http://digitaladvertising.systems/inventory_management/view_user_profile/b0eb1a6574e211e5858d3417ebe5d44c/">View Audience Data</a></div>
	</div>
	<div class="col-md-4">
		<div class="portlet-title" style="float:right;">
			<div class="actions">
				<div class="btn-group btn-group-devided" data-toggle="buttons">
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "day" ? "active" : "" ?>">
					<input type="radio" name="options" onchange="chart1()" class="toggle" id="option1">Today</label>
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "week" ? "active" : "" ?>">
			<input type="radio" name="options"  onchange="chart2()" class="toggle" id="option2">Last 7 Days</label>
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "month" ? "active" : "" ?>">
			<input type="radio" name="options"  onchange="chart3()" class="toggle" id="option3">MTD</label>
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "year" ? "active" : "" ?>">
			<input type="radio" name="options"  onchange="chart4()" class="toggle" id="option2">YTD</label>
		</div>
	</div>
</div>
</div>
</div>

<div class="portlet-title hide">
	<div class="actions">
		<div class="btn-group btn-group-devided" data-toggle="buttons">
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "day" ? "active" : "" ?>">
			<input type="radio" name="options" onchange="chart1()" class="toggle" id="option1">Today</label>
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "week" ? "active" : "" ?>">
			<input type="radio" name="options"  onchange="chart2()" class="toggle" id="option2">Last 7 Days</label>
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "month" ? "active" : "" ?>">
			<input type="radio" name="options"  onchange="chart3()" class="toggle" id="option3">MTD</label>
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "year" ? "active" : "" ?>">
			<input type="radio" name="options"  onchange="chart4()" class="toggle" id="option2">YTD</label>
		</div>
	</div>
</div>

<!-- END PAGE BREADCRUMB -->


<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-5">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Business</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-3 col-sm-3 col-xs-3">
						<div class="font-grey-mint font-sm">
							Line Items
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo number_format($activeLines); ?> <span class="font-lg font-grey-mint"></span>
						</div>
						<div class="font-grey-mint font-sm">
							Active
						</div>
					</div>
					<div class="col-md-5 col-sm-5 col-xs-5">
						<div id="barchart_plain" width="100%" height="150"></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									CPM
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalCPM); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									CPC
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalCPC); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									CPL
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalCPL); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									SCR
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalSCR); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row  list-separated">
					<div class="col-md-7 col-sm-7 col-xs-7">
						<div class="font-grey-mint font-sm">
							Revenue
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							$<?php echo number_format($totalRevenue,2); ?> <i style="font-size:20px;color:green;margin-left:1px;" class="fa fa-arrow-up"></i><span class="font-lg font-grey-mint">6.5%</span>
						</div>
					</div>
					<div class="col-md-5 col-sm-5 col-xs-5">
						<div class="font-grey-mint font-sm">
							eCPM
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							$<?php echo $imp != 0 ? number_format((($totalRevenue * 1000) / $imp),2) : "0"; ?> <i style="font-size:20px;color:green;margin-left:1px;" class="fa fa-arrow-up"></i><span class="font-lg font-grey-mint">2.5%</span>
						</div>
					</div>
				</div>
				<div class="row list-separated">
					<div class="col-md-5 col-sm-5 col-xs-12">
						<div id="piechart" style="width: 100%; height: 100%;"></div>
					</div>
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="view_subscribers">
								<thead>
									<tr>
										<th class="table-checkbox">
											Price Format
										</th>
										<th>
											Revenue
										</th>
										<th>
											Share
										</th>
										<th>
											eCPM
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>CPM</td>
										<td>$<?php echo number_format($revenueCPM,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueCPM * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
									<tr>
										<td>CPC</td>
										<td>$<?php echo number_format($revenueCPC,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueCPC * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
									<tr>
										<td>CPL</td>
										<td>$<?php echo number_format($revenueCPL,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueCPL * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
									<tr>
										<td>SCR</td>
										<td>$<?php echo number_format($revenueSCR,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueSCR * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">TOP PERFORMING LINE-ITEMS</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="uppercase font-hg font-red-flamingo">
							<div id="top_performing_line_items" width="100%" height="250"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Users</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12">
						<div class="font-grey-mint font-sm">
							# of Subscribers
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo "0" ?> <span class="font-lg font-grey-mint"></span>
						</div>
						<div id="regions_div" style="width: 100%;"></div>
					</div>
					<div class="col-md-12 col-sm-12">
						<div class="font-grey-mint font-sm">
							DAILY SUBSCRIPTION TRENDS
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart-o font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Engagement</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div id="g1_sent_delivered" class="gauge"></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div id="g1_opens" class="gauge"></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div id="g1_clicks" class="gauge"></div>
					</div>
				</div>
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="clearfix">
							<div id="legendContainer_email_message" class="table-responsive"></div> 
						</div>
						<div id="chart_email_message" class="chart"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row list-separated">
			<div class="col-md-9">
				<div class="portlet box grey-gallery">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-check  font-grey-silver"></i>
							<span class="caption-subject font-grey-silver bold uppercase">Quality</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row list-separated">
							<div class="col-md-4 col-sm-4 col-xs-4">
								<div class="font-grey-mint font-sm">
									3rd Party Rejects
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($third_party_rejects); ?> <span class="font-lg font-grey-mint"></span>
								</div>
								<div class="font-grey-mint font-sm">
									<?php echo number_format((($third_party_rejects / $imp) * 100),2) ?>%
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<div class="font-grey-mint font-sm">
									Client Rejects
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($clients_rejects); ?> <span class="font-lg font-grey-mint"></span>
								</div>
								<div class="font-grey-mint font-sm">
									<?php echo number_format((($clients_rejects / $imp) * 100),2) ?>%
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<div class="font-grey-mint font-sm">
									AR Rejects
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($ar_rejects); ?> <span class="font-lg font-grey-mint"></span>
								</div>
								<div class="font-grey-mint font-sm">
									<?php echo number_format((($ar_rejects / $imp) * 100),2) ?>%
								</div>
							</div>
						</div>
						<div class="row list-separated">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="clearfix">
									<div id="legendContainer_email_quality" class="table-responsive"></div> 
								</div>
								<div id="chart_email_quality" class="chart"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="portlet box grey-gallery">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-desktop font-grey-silver"></i>
							<span class="caption-subject font-grey-silver bold uppercase">Technology</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row list-separated">
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="font-grey-mint font-sm">
									Device Type
								</div>
								<div id="piechart_desktop" style="width: 100%;"></div>
							</div>
							<div class="col-md-4 col-sm-4  col-xs-12">
								<div class="font-grey-mint font-sm">
									Browsers
								</div>
								<div id="piechart_windows" style="width: 100%;"></div>
							</div>
							<div class="col-md-4 col-sm-4  col-xs-12">
								<div class="font-grey-mint font-sm">
									OS Family
								</div>
								<div id="piechart_ios" style="width: 100%;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<!-- END PAGE CONTENT INNER -->

<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>

<!-- END PAGE CONTENT INNER -->
<script>
	
	var ChartsFlotcharts1 = function() {
		
		return {
			//main function to initiate the module
			initCharts: function() {
				
				if (!jQuery.plot) {
					return;
				}
				
				var data = [];
				var totalPoints = 250;
				
				//Interactive Chart
				
				function chart_email_message() {
					if ($('#chart_email_message').size() != 1) {
						return;
					}
					
					function randValue() {
						return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
					}
					var imp = [
					<?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_imp']; ?>],
					<?php } ?>
					];
					var leads = [
					<?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date'])  - 1; ?>,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_leads']; ?>],
					<?php } ?>
					];
					var delivered = [
					<?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?>,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_delivered']; ?>],
					<?php } ?>
					];
					var accepted = [
					<?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?>,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_accepted']; ?>],
					<?php } ?>
					];
					
					var plot = $.plot($("#chart_email_message"), [{
						data: imp,
						label: "Impression",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:0
						}, {
						data: leads,
						label: "Leads",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:1
						}, {
						data: delivered,
						label: "Delivered",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:2
						} , {
						data: accepted,
						label: "Accepted",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:3
						}], {
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
								fillColor: {
									colors: [{
										opacity: 0.05
										}, {
										opacity: 0.01
									}]
								}
							},
							points: {
								show: true,
								radius: 3,
								lineWidth: 1
							},
							shadowSize: 2
						},
						grid: {
							hoverable: true,
							clickable: true,
							tickColor: "#eee",
							borderColor: "#eee",
							borderWidth: 1
						},
						colors: ["#d12610", "#37b7f3", "#52e136"],
						xaxis: {
							mode: "time",
							tickSize: [1, "day"],    
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						yaxis: {
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						legend:{         
							placement: 'outsideGrid',
							container:$("#legendContainer_email_message"),    
							labelFormatter: function(label, series){
								return '<button data-toggle="button" onclick="togglePlot('+series.idx+');" class="btn btn-default ' + ((series.lines.show) ? "active" : "") + '" type="button" aria-pressed="false"> '+label + ((series.lines.show) ? " <span style=\"margin-left: 5px; font-size: 10px;\">&#10006;</span>" : "")+'</button>';
							},
							noColumns: 0
						}
					});
					
					function gd(date_c) {
						var date = Date.parse(date_c);
						return date.toString('dd-MMM-yyyy');
					}
					
					function showTooltip(x, y, contents) {
						$('<div id="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 15,
							border: '1px solid #333',
							padding: '4px',
							color: '#fff',
							'border-radius': '3px',
							'background-color': '#333',
							opacity: 0.80
						}).appendTo("body").fadeIn(200);
					}
					
					var previousPoint = null;
					$("#chart_2").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x);
						$("#y").text(pos.y);
						
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;
								
								$("#tooltip").remove();
								var x = item.datapoint[0],
								y = item.datapoint[1];
								showTooltip(item.pageX, item.pageY, item.series.label + " on " + convertTimestamp(x) + " is " + y);
							}
							} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
					
					togglePlot = function(seriesIdx)
					{
						var someData = plot.getData();  
						someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;  
						if (!someData[seriesIdx].lines.show){
							someData[seriesIdx].tempData = someData[seriesIdx].data;
							someData[seriesIdx].data = [];
						}
						else
						{
							someData[seriesIdx].data = someData[seriesIdx].tempData;
						}
						plot.setData(someData);
						plot.setupGrid();
						plot.draw();
					}
					
					
					function convertTimestamp(timestamp) {
						var nextDay = new Date(timestamp); // 86400000 - one day in ms 
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
						'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						return months[nextDay.getUTCMonth()] + ' ' + nextDay.getUTCDate();
					}
				}
				
				function chart_email_quality() {
					if ($('#chart_email_quality').size() != 1) {
						return;
					}
					
					function randValue() {
						return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
					}
					var thirdparty = [
					<?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_3party']; ?>],
					<?php } ?>
					];
					var clients_rejects = [
					<?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date'])  - 1; ?>,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_client_rejects']; ?>],
					<?php } ?>
					];
					var ar_rejects = [
					<?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?>,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_ar_rejects']; ?>],
					<?php } ?>
					];
					
					var plot = $.plot($("#chart_email_quality"), [{
						data: thirdparty,
						label: "3rd Party Rejects",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:0
						}, {
						data: clients_rejects,
						label: "Client Rejects",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:1
						} , {
						data: ar_rejects,
						label: "AR Rejects",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:3
						}], {
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
								fillColor: {
									colors: [{
										opacity: 0.05
										}, {
										opacity: 0.01
									}]
								}
							},
							points: {
								show: true,
								radius: 3,
								lineWidth: 1
							},
							shadowSize: 2
						},
						grid: {
							hoverable: true,
							clickable: true,
							tickColor: "#eee",
							borderColor: "#eee",
							borderWidth: 1
						},
						colors: ["#d12610", "#37b7f3", "#52e136"],
						xaxis: {
							mode: "time",
							tickSize: [1, "day"],    
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						yaxis: {
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						legend:{         
							placement: 'outsideGrid',
							container:$("#legendContainer_email_quality"),    
							labelFormatter: function(label, series){
								return '<button data-toggle="button" onclick="togglePlot1('+series.idx+');" class="btn btn-default ' + ((series.lines.show) ? "active" : "") + '" type="button" aria-pressed="false"> '+label + ((series.lines.show) ? " <span style=\"margin-left: 5px; font-size: 10px;\">&#10006;</span>" : "")+'</button>';
							},
							noColumns: 0
						}
					});
					
					function gd(date_c) {
						var date = Date.parse(date_c);
						return date.toString('dd-MMM-yyyy');
					}
					
					function showTooltip(x, y, contents) {
						$('<div id="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 15,
							border: '1px solid #333',
							padding: '4px',
							color: '#fff',
							'border-radius': '3px',
							'background-color': '#333',
							opacity: 0.80
						}).appendTo("body").fadeIn(200);
					}
					
					var previousPoint = null;
					$("#chart_2").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x);
						$("#y").text(pos.y);
						
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;
								
								$("#tooltip").remove();
								var x = item.datapoint[0],
								y = item.datapoint[1];
								showTooltip(item.pageX, item.pageY, item.series.label + " on " + convertTimestamp(x) + " is " + y);
							}
							} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
					
					togglePlot1 = function(seriesIdx)
					{
						var someData = plot.getData();  
						someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;  
						if (!someData[seriesIdx].lines.show){
							someData[seriesIdx].tempData = someData[seriesIdx].data;
							someData[seriesIdx].data = [];
						}
						else
						{
							someData[seriesIdx].data = someData[seriesIdx].tempData;
						}
						plot.setData(someData);
						plot.setupGrid();
						plot.draw();
					}
					
					
					function convertTimestamp(timestamp) {
						var nextDay = new Date(timestamp); // 86400000 - one day in ms 
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
						'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						return months[nextDay.getUTCMonth()] + ' ' + nextDay.getUTCDate();
					}
				}
				
				
				
				//graph
				chart_email_message();
				chart_email_quality();
				
			}
		};
		
	}();
	
	
	function chart1(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "day/";
			} else {
			window.location = "/messages/index/all/day";
		}
	}
	
	function chart2(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "week/";
			} else {
			window.location = "/messages/index/all/week";
		}
	}
	
	function chart3(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "month/";
			} else {
			window.location = "/messages/index/all/month";
		}
	}
	
	function chart4(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "year/";
			} else {
			window.location = "/messages/index/all/year";
		}
	}
</script>
<!-- CSS -->
<style type="text/css">
	#legendContainer {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
	
	#legendContainer_email_message {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer_email_message .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
	
	#legendContainer_email_quality {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer_email_quality .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
</style>	
<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualization);
	
	function drawVisualization() {
		var data = google.visualization.arrayToDataTable([
		['Type', 'Count'],
		['CPM', <?php echo $totalCPM; ?>],
		['CPC', <?php echo $totalCPC; ?>],
		['CPL', <?php echo $totalCPL; ?>],
		['SCR', <?php echo $totalSCR; ?>]
		]);
		var options = {
			height: 150,
			'chartArea': {'width': '70%', 'height': '80%'},
			bar: {groupWidth: '75%'},
		};
		var chart = new google.visualization.BarChart(document.getElementById('barchart_plain'));
		chart.draw(data, options);
	}
</script>
<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualization);
	
	function drawVisualization() {
		var data = google.visualization.arrayToDataTable([
		['Line Item', 'Revenue'],
		<?php $count=0; foreach($revenueByTypes as $revenueByType) {	?>
			['<?php echo str_replace("'", "", $revenueByType['name']); ?>', <?php echo $revenueByType['revenue']; ?>],
		<?php $count++; if($count == 9) break; } ?>
		]);
		var options = {
			height: 250,
			bar: {groupWidth: '75%'},
			'chartArea': {top:20,'width': '70%', 'height': '80%'}
		};
		var chart = new google.visualization.BarChart(document.getElementById('top_performing_line_items'));
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Price Format', 'Revenue'],
		['CPM',     <?php echo $revenueCPM ?>],
		['CPC',      <?php echo $revenueCPC ?>],
		['CPL',   <?php echo $revenueCPL ?>],
		['SCR',  <?php echo $revenueSCR ?>]
		]);
		
		var options = {
			'chartArea': {top:20,'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Device', 'Type'],
		<?php foreach ($DeviceType as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_desktop'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Browser', 'Name'],
		<?php foreach ($browsers as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_windows'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['OS', 'Family'],
		<?php foreach ($osfamily as $key => $value) {?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_ios'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["geochart"]});
	google.setOnLoadCallback(drawRegionsMap);
	
	
	function drawRegionsMap() {
		
		var data = google.visualization.arrayToDataTable([
		['Country', 'Popularity'],
		<?php foreach ($country as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			
		};
		
		var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
		
		chart.draw(data, options);
	}
</script>	
<script>
    var g1;
    var g2;
    var g3;
    document.addEventListener("DOMContentLoaded", function(event) {
        g1 = new JustGage({
            id: "g1_sent_delivered",
            value: <?php echo $imp; ?>,
            min: 0,
            max: <?php echo $imp; ?>,
            gaugeWidthScale: 0.8,
			title: "Impressions",
            counter: true,
			formatNumber: true,
            hideInnerShadow: true,
			levelColors: [
			"#00fff6",
			"#ff00fc",
			"#FC3B00"
			]
		});
		g2 = new JustGage({
            id: "g1_opens",
            value: <?php echo $delivered; ?>,
            min: 0,
            max: <?php echo $leads; ?>,
            gaugeWidthScale: 0.6,
			title: "Delivered",
            counter: true,
			formatNumber: true,
            hideInnerShadow: true,
    		pointer: true,
			pointerOptions: {
				toplength: -15,
				bottomlength: 10,
				bottomwidth: 12,
				color: '#8e8e93',
				stroke: '#ffffff',
				stroke_width: 3,
				stroke_linecap: 'round'
			},
			levelColors: [
			"#00fff6",
			"#FA9C00",
			"#FA9C00"
			]
		}); 
		g3 = new JustGage({
            id: "g1_clicks",
            value: <?php echo $accepted; ?>,
            min: 0,
            max: <?php echo $delivered; ?>,
            gaugeWidthScale: 0.6,
			title: "Accepted",
            counter: true,
			formatNumber: true,
            hideInnerShadow: true,
    		pointer: true,
			pointerOptions: {
				toplength: -15,
				bottomlength: 10,
				bottomwidth: 12,
				color: '#8e8e93',
				stroke: '#ffffff',
				stroke_width: 3,
				stroke_linecap: 'round'
			},
			levelColors: [
			"#00fff6",
			"#F8C700",
			"#F8C700"
			]
		});
	});
	</script>			