
<div style="clear:both;"></div>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <?php echo $this->Html->link('Home', '/'); ?>
        <i class="fa fa-circle"></i>
    </li>
    <li  class="active">
        <?php echo $this->Html->link("Inventory Management", array('controller' => 'inventory_management', 'action' => 'index'), array('escape' => false)); ?> 
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">AdUnits</span>
				</div>
				
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
					<?php if(!empty($optt) && $optt == 1) {  ?>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
						<input type="radio" name="options" class="toggle" id="option1" value="1">Created By Self</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
						<input type="radio" name="options" class="toggle" id="option2" value="2">Created By Others</label>
						<?php } else if(!empty($optt) && $optt == 2) { ?>
							<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
						<input type="radio" name="options" class="toggle" id="option1" value="1">Created By Self</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
						<input type="radio" name="options" class="toggle" id="option2" value="2">Created By Others</label>
						<?php } else { ?>
					
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
						<input type="radio" name="options" class="toggle" id="option1" value="1">Created By Self</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
						<input type="radio" name="options" class="toggle" id="option2" value="2">Created By Others</label>
					<?php }	?>
					
					</div>
				</div>
				
			</div>
            <div class="portlet-body">
                <?php
                if ($adunit != 0 && $ad_ptype_id == 6) {
                    ?>
                    <div class="row">
                        <!--<div class="col-sm-6">
                                <p>
                                        <h4>Javascript Code (With Logo Template)</h4>
                                        <textarea rows="5" cols="5" class="form-control demo"><script type='text/javascript' src='http://services.digitaladvertising.systems/coreg/das_coregscript.aspx?t=1&auid=<?php echo $ad_uid; ?>&<?php echo $field; ?>&redirecturl={VALUE}'></script></textarea>
                                </p>
                        </div>
                        <div class="col-sm-6">
                                <p>
                                        <h4>Javascript Code (Without Logo Template)</h4>
                                        <textarea rows="5" cols="5" class="form-control demo"><script type='text/javascript' src='http://services.digitaladvertising.systems/coreg/das_coregscript.aspx?t=2&auid=<?php echo $ad_uid; ?>&<?php echo $field; ?>&redirecturl={VALUE}'></script></textarea>
                                </p>
                        </div>-->
                        <div class="col-sm-12">
                            <h4>Javascript Code </h4>
                            <div>
                                <h4 style="font-weight: bold;font-size:14px;">Step 1 : Put the following html code where you want to show the offers</h4>
                                <div class="margin-top-10 margin-bottom-10"><code>&lt;div id="das_inline_offers">&lt;div></code></div>
                                <h4  style="font-weight: bold;font-size:14px;padding-top:10px;">Step 2 : Put the following javascript code before the end of the body tag of your html page</h4>
                                <div><code>
                                        &lt;script>
                                            (function (i, s, o, g, r, a, m) {
                                                a = s.createElement(o),
                                                        m = s.getElementsByTagName(o)[0];
                                                a.src = g;
                                                a.id = 'das_addunit_offers';
                                                m.parentNode.insertBefore(a, m)
                                            })(window, document, 'script', 'http://api.digitaladvertising.systems/js/offers.js?auid=<?php echo $ad_uid; ?>&redirecturl={value}');
                                        &lt;/script>

                                    </code>
                                </div>
                                 <h4 style="font-weight: bold;font-size:14px;padding-top:10px;">Step 3: Add the following javascript function "submitLead();" on your registration or submit button </h4>
                                 <div class="margin-top-10 margin-bottom-10"><code>Example : &lt;button class="btn btn-primary" id="das_post_lead" onclick="submitLead();">Register&lt;/button></code></div>
                            </div>
                            <div>&nbsp;</div>
                            <?php
                        }
                        ?>
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <?php echo $this->Html->link('Add New <i class="fa fa-plus"></i>', array('controller' => 'inventory_management', 'action' => 'addAdUnit'), array('escape' => false, 'class' => 'btn green', 'id' => 'sample_editable_1_new')); ?>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="adunitlist">
                                <thead>
                                    <tr>
                                        <th class="table-checkbox">
                                            Sr. No
                                        </th>
                                        <th>
                                            Publisher Name
										</th>
										<?php if(!empty($optt) && $optt == 2) {  ?>
										<th>
										Created By
										</th>
										<?php } ?>
                                        <th>
                                            AdUnit Name
                                        </th>
                                        <th>
                                            Media Type
                                        </th>
                                        <th>
                                            Product Type
                                        </th>
                                        <th>
                                            AdUnit Size
                                        </th>
                                        <th>
                                            Create Date
										</th>
										<?php if(!empty($optt) && $optt == 1 ) {  ?>
                                        <th>
                                            Edit
										</th>
										<?php } else if(!empty($optt) && $optt == 2) {  ?>
											
											<?php } else { ?>
											<th>
                                            Edit
										</th>
										<?php }  ?>
									</tr>
								</thead>
                                <tbody>
                                    <?php
										//pr($group);die;
										$i = 0;
										$count = 1;
										foreach ($group as $user):
                                        $class = '';
                                        if ($i++ % 2 == 0) {
                                            $class = 'altrow';
										}
                                        if ($user['tblAdunit']['ad_dfp_id'] != "") {
										?>
										<tr class="odd gradeX">
											<td>
												<?php echo $count; ?>
											</td>
											<td>
												<?php echo $user['tblp']['CompanyName']; ?>
											</td>
											<?php if(!empty($optt) && $optt == 2) {  ?>
											<td>
												<?php echo $user['OwnerCompanyName']; ?>
											</td>
											<?php } ?>
											
											<td>
												<?php
                                                    if ($user['tbmt']['mtype_id'] == "2") {
                                                        echo $this->Html->link($user['tblAdunit']['adunit_name'], array('controller' => 'inventory_management', 'action' => 'view_stats_email', $user['tblAdunit']['ad_uid']), array('escape' => false));
                                                    } else if ($user['tbmt']['mtype_id'] == "5") {
                                                        echo $this->Html->link($user['tblAdunit']['adunit_name'], array('controller' => 'inventory_management', 'action' => 'view_stats_leadgen', $user['tblAdunit']['ad_uid']), array('escape' => false));
                                                    } else {
                                                        echo $user['tblAdunit']['adunit_name'];
                                                    }
                                                    ?> 
                                                    <?php
                                                    if (!empty($nlchild)) {
                                                        foreach ($nlchilds[$user['tblAdunit']['ad_uid']] as $nlchild) {
                                                            ?>
                                                            <br />- <?php echo $nlchild['tblAdunit']['adunit_name']; ?>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo $user['tbmt']['mtype_name']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $user['tbpt']['ptype_name']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $user['tbmt']['mtype_name'] == "Email" ? "N/A" : $user['tblAdunit']['ad_unit_sizes']; ?>
                                                </td>
                                                <td class="center">

                                                    <?php $dateformte = Configure::read('Php.dateformte'); ?>
                                                    <?php echo $this->Time->format($dateformte, $user['tblAdunit']['ad_created_at']); ?>&nbsp;
                                                </td>
											<?php if(!empty($optt) && $optt == 1) {  ?>
											<td class="center">
												<?php echo $this->Html->link('Edit', array('controller' => 'inventory_management', 'action' => 'editAdUnit', $user['tblAdunit']['adunit_id']), array('escape' => false, 'class' => 'editlink' )); ?>
											</td>
											<?php } else if(!empty($optt) && $optt == 2) {  ?>
											
											<?php } else { ?>
											<td class="center">
												<?php echo $this->Html->link('Edit', array('controller' => 'inventory_management', 'action' => 'editAdUnit', $user['tblAdunit']['adunit_id']), array('escape' => false, 'class' => 'editlink' )); ?>
											</td>
											<?php } ?>
										</tr>
										<?php
                                            $count++;
										} endforeach;
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
                <!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
        <!-- END PAGE CONTENT INNER -->
        <style>
            #sample_4_filter { 
			float:right;
            }
		</style>
		<script>
		
		$("input[name='options']").change(function(){
		//alert($(this).val());
		var opt = $(this).val();
		window.location.href = "/inventory_management?options=" + opt;
    // Do something interesting here
});
		</script>	

