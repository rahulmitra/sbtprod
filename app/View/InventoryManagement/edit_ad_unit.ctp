<!-- BEGIN PAGE BREADCRUMB -->
<style>
div.radio, div.radio span,
div.radio input {
    width: 20px!important;
    height: 20px!important;
}
.uniform-size{margin-left:-10px!important;
				}
				
.box-info label{margin-right: 10px!important;}
</style>

<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/inventory_management/">Inventory Management</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/inventory_management/editAdUnit">Edit Ad Units</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Edit Inventory -Ad Unit
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				<?php echo $this->Form->create('InventoryManagement',array('url'=>array('controller'=>'inventory_management','action'=>'editAdUnit/',$adunit_id)))?>
					<div class="form-body">
						<h3 class="form-section">Edit Ad Unit</h3>
						<div class="row">
						<div class="col-md-12">
						<div class="col-md-3">&nbsp;</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Ad Unit Name <span class="required">
									* </span></label>
									<div class="col-md-9">
										<?php echo $this->Form->hidden('tblAdunit.adunit_id',array('value'=>$adunit_id));?> 
										<?php echo $this->Form->hidden('tblAdunit.ad_uid',array('value'=>$tblAdunits['tblAdunit']['ad_uid']));?> 
										<?php echo $this->Form->input('tblAdunit.adunit_name',array('id'=>'title','placeholder'=>'Enter Ad Unit Name Here','div'=>false,'label'=>false,'class'=>'form-control', 'value'=>$tblAdunits['tblAdunit']['adunit_name']));?> 
										
									</div>
								</div>
							</div>
							<div class="col-md-3">&nbsp;</div>
							</div>
							<!--/span-->
							<div class="col-md-6 hide">
								<div class="form-group">
									<label class="control-label col-md-3">Ad Unit DFP Name</label>
									<div class="col-md-9">
										<?php echo $this->Form->input('tblAdunit.adunit_dfp_name',array('id'=>'title','placeholder'=>'Ad Unit DFP Name','div'=>false,'label'=>false,'class'=>'form-control', 'value'=>$tblAdunits['tblAdunit']['adunit_dfp_name']));?>  
									</div>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-12" style="margin-top:15px;">
							<div class="col-md-3">&nbsp;</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Description</label>
									<div class="col-md-9">
										<?php echo $this->Form->textarea('tblAdunit.ad_desc',array('id'=>'title', 'div'=>false,'label'=>false,'rows'=>1,'class'=>'form-control', 'value'=>$tblAdunits['tblAdunit']['ad_desc']));?>  										 
										<span class="help-block">
										Enter Ad Unit Description.</span>
									</div>
								</div>
							</div>
							<div class="col-md-3">&nbsp;</div>
							</div>
							<!--/span-->
							<div class="col-md-6 hide">
								<div class="form-group">
									<label class="control-label col-md-3">Ad Unit Size</label>
									<div class="col-md-9">
										<?php echo $this->Form->input('tblAdunit.ad_unit_sizes',array('type'=>'select','div'=>false,'label'=>false,'class'=>'form-control', 'options'=>$adUnit_size,'default'=>$tblAdunits['tblAdunit']['ad_unit_sizes']));?> 
										<span class="help-block">
										Select Ad Unit Size. 1X1 for Out of Page </span>
									</div>
								</div>
							</div>
							<!--/span-->
							<!--/span-->
							<div class="col-md-12">
							<div class="col-md-3">&nbsp;</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Audience Profiling</label>
									<div class="col-md-9">
										<div class="clearfix">
											<div class="icheck-inline">
												<label>
												<input type="radio" id="ad_AudienceProfileYes" onclick="checkAudienceProfile(this)" checked name="data[tblAdunit][ad_audience_profile]" value="1" <?php echo ($tblAdunits['tblAdunit']['ad_audience_profile']==1)?'checked':''?>  class="icheck">Yes </label>
												<label>
												<input type="radio" id="ad_AudienceProfileNo" 
												onclick="checkAudienceProfile(this)" name="data[tblAdunit][ad_audience_profile]" value="0" <?php echo ($tblAdunits['tblAdunit']['ad_audience_profile']==0)?'checked':''?> class="icheck"> No</label>
												 
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">&nbsp;</div>
							</div>
							<!--/span-->
							<!--/span-->
							<div class="col-md-6 hide">
								<div class="form-group">
									<label class="control-label col-md-3">Parent Ad Unit</label>
									<div class="col-md-9">
										<?php echo $this->Form->input('tblAdunit.ad_parent_id',array('type'=>'select','div'=>false,'label'=>false,'class'=>'form-control', 'options'=>$ParentAdUnit,'default'=>$tblAdunits['tblAdunit']['ad_parent_id'],'onChange'=>'checkParent(this);'));?> 
										<span class="help-block">
										Select Ad Unit Size. 1X1 for Out of Page </span>
									</div>
								</div>
							</div>
							<div class="col-md-12">
							<div class="col-md-3">&nbsp;</div>
							<div class="col-md-6" id="adAutoOptimize" style="display:<?php 
							echo ($tblAdunits['tblAdunit']['ad_audience_profile']==1)?'block':'none'?>;">
								<div class="form-group">
									<label class="control-label 
									col-md-6"> Auto Optimize Offers with Data Profiles</label>
									<div class="col-md-6">
										<div class="clearfix">
											<div class="icheck-inline">
												<label>
												<input type="radio"   name="data[tblAdunit][ad_auto_optimize]" value="1" <?php echo ($tblAdunits['tblAdunit']['ad_auto_optimize']==1)?'checked':''?>  class="icheck">Yes </label>
												<label>
												<input type="radio"  name="data[tblAdunit][ad_auto_optimize]" value="0" <?php echo ($tblAdunits['tblAdunit']['ad_auto_optimize']==0)?'checked':''?> class="icheck"> No</label>
												 <span class="help-block"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-3">&nbsp;</div>
							</div>
							<!--/span-->
							<div class="col-md-12">
							<div class="col-md-3">&nbsp;</div>
							<div class="col-md-6" id="publisher_id">
								<div class="form-group">
									<label class="control-label col-md-3" >Company</label>
									<div class="col-md-9">
										
										<?php echo $this->Form->input('tblAdunit.publisher_id',array('type'=>'select','div'=>false,'label'=>false,'class'=>'form-control js-comapny-array', 'options'=>$publisher,'default'=>$tblAdunits['tblAdunit']['publisher_id']));?> 
										<span class="help-block">Select The Company Name. </span>
									</div>
								</div>
							</div>
							<div class="col-md-3">&nbsp;</div>
							</div>
							<!--/span-->
							<div class="col-md-12">
							<div class="col-md-3">&nbsp;</div>
							<div class="col-md-6" id="ad_ptype_id">
								<div class="form-group">
									<label class="control-label col-md-3" >Product Type</label>
									<div class="col-md-9" >
										<?php echo $this->Form->input('tblAdunit.ad_ptype_id',array('type'=>'select','div'=>false,'label'=>false,'class'=>'form-control js-product-array', 'options'=>$ProductTypeList,'default'=>$tblAdunits['tblAdunit']['ad_ptype_id'],'empty'=>'--Select Product Type--','onChange'=>'checkMoreInfo(this);'));?> 
										 
										<span class="help-block">
										Select Type of the Product</span>
									</div>
								</div>
							</div>
							<div class="col-md-3">&nbsp;</div>
							</div>
							<!--/span-->
							<div class="col-md-12">
							<div class="col-md-3">&nbsp;</div>
							<div class="col-md-6" id="placement" style="display:<?php echo 
						($tblAdunits['tblAdunit']['ad_ptype_id']==6 
						and $tblAdunits['tblAdunit']['ad_parent_id']==0)?'block':'none';?>;">
								<div class="form-group">
									<label class="control-label col-md-3" >Placement</label>
									<div class="col-md-6" >
										<?php echo $this->Form->input('tblAdunit.ad_placement_id',array('type'=>'select','div'=>false,'label'=>false,'class'=>'form-control js-placement-array', 'options'=>$adUnitPlacements,'default'=>$tblAdunits['tblAdunit']['ad_placement_id']  ));?> 
										
										 
										<span class="help-block">
										Select Type of the Product</span>
									</div>
								</div>
							</div>
							<div class="col-md-3">&nbsp;</div>
							</div>
							<div class="col-md-12">
							<div class="col-md-3">&nbsp;</div>
							<div class="col-md-6" id="ad_adtype_id" style="display:<?php echo 
						($tblAdunits['tblAdunit']['ad_ptype_id']==6 
						and $tblAdunits['tblAdunit']['ad_parent_id']==0)?'block':'none';?>;">
								<div class="form-group">
									<label class="control-label col-md-3" >Ad-Unit Type</label>
									<div class="col-md-9" >
										
										
										<?php echo $this->Form->input('tblAdunit.ad_adtype_id',array('type'=>'select','div'=>false,'label'=>false,'class'=>'form-control js-unit_type-array', 'options'=>$adUnitTypes,'default'=>$tblAdunits['tblAdunit']['ad_adtype_id']  ));?> 
										
									 
										<span class="help-block">
										Select Type of the Product</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">&nbsp;</div>
						</div>
						
						<h3 class="form-section" id="coregHDiv" style="display:<?php echo 
						($tblAdunits['tblAdunit']['ad_ptype_id']==6 
						and $tblAdunits['tblAdunit']['ad_parent_id']==0)?'block':'none';?>;">Layout</h3>
						<div class="row" id="coregDiv" style="display:<?php echo 
						($tblAdunits['tblAdunit']['ad_ptype_id']==6)?'block':'none';?>;">
							<div class="col-md-12">
		<div class="portlet light" id="form_wizard_1">
		<div class="portlet-body form">
				<div class="form-wizard">
					<div class="form-body">
						
						<div class="tab-content">
							<div class="alert alert-danger display-none">
								<button class="close" data-dismiss="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-none">
								<button class="close" data-dismiss="alert"></button>
								Your form validation is successful!
							</div>
							<div class="tab-pane active" id="tab1" >
								<div class="row">
									<div class="col-sm-12">
										<h2>Template</h2>
										<div class="tabs-sectoin">
											<ul class="nav nav-tabs">
												<?php
												$tab_ids=configure::read('tab_ids');
												$current_tab_id 
												=(!empty($tblAdunitsLayout['AdunitLayout']['tab_id']))?$tblAdunitsLayout['AdunitLayout']['tab_id']:1;
												 if(!empty($tab_ids)){
													 foreach($tab_ids as $key=>$tab_id){
														 $class = ($key==$current_tab_id)?'class="active"':'';
														?>
														<li <?php echo $class;?>onclick="current_tab(<?php echo $key;?>)" ><a data-toggle="tab" href="#content-<?php echo $key;?>"><?php echo $this->Html->image($tab_id,array('escape' => false));?></a></li>
														<?php 
													 }
												 }
												 ?> 
											</ul>
										</div>
										
										<div class="box-info">
											<div class="row"  style="margin-bottom:15px;">
												<div class="col-sm-4">
													<div class="row">
														<div class="col-xs-4">
															<label class="control-label">Layout</label></div>
																<div class="col-xs-8">
																<div class="clearfix">
																<div class="icheck-inline">
																<label>
																	<?php $ad_gen_layout=(!empty($tblAdunitsLayout['AdunitLayout']['ad_gen_layout']))?$tblAdunitsLayout['AdunitLayout']['ad_gen_layout']:'';?>
																	<input type="radio" id="gen_layoutFixed"  onclick="checkGenLayout(this)" name="data[AdunitLayout][ad_gen_layout]" value="1"  <?php echo ($ad_gen_layout == '1')?'checked':'' ?> >Fixed </label>
																<label>
																	<input type="radio" id="gen_layoutFluid"  onclick="checkGenLayout(this)" name="data[AdunitLayout][ad_gen_layout]" value="0" <?php echo ($ad_gen_layout == '0')?'checked':'' ?> >Fluid</label>
																</div>
															</div>
														</div>
													</div>
												</div>
												
													<!-- Script button -->
												<?php 
												$this->request->data['AdunitLayout']['script_included_button']= $button_script = (!empty($tblAdunitsLayout['AdunitLayout']['script_included_button']))?$tblAdunitsLayout['AdunitLayout']['script_included_button']:'';
												?>
												<div class="col-sm-8">
													<div class="row">
														<div class="col-xs-12">
															<?php 
															 
															echo $this->Form->input('AdunitLayout.script_included_button', array('type'=>'checkbox','id'=>'scriptButton','label' => false, 'div' => false, 'onClick' => 'hideColorButton(this)', 'class'=>false,'after'=>__('<label for="scriptButton">Button included as part of script?</label>')));?>
														</div>
													</div>
												</div>
												<!--/End of Script button-->
											</div>	
											
											<div class="row" id="layoutWidth" style="display:none;">
												<div class="col-sm-4">
													<div class="row">
														<div class="col-xs-4"><label class="mt-5">Width</label></div>
														<div class="col-xs-8">
															<p>
																<?php echo $this->Form->hidden('AdunitLayout.id',array('value'=>(!empty($tblAdunitsLayout['AdunitLayout']['id']))?$tblAdunitsLayout['AdunitLayout']['id']:'')); ?>
																<?php echo $this->Form->hidden('AdunitLayout.tab_id',array('class'=> 'form-control demo','value'=>$current_tab_id,'div'=>false,'label'=>false)); ?>
																	<?php echo 
																	$this->Form->input('AdunitLayout.ad_width',array('class'=> 'form-control demo','div'=>false,'label'=>false,
																	'value'=>(!empty($tblAdunitsLayout['AdunitLayout']['ad_width']))?$tblAdunitsLayout['AdunitLayout']['ad_width']:''
																	)); ?>
															</div>
															</div>
														</div>
														<div class="col-sm-4">
															<div class="row">
																<div class="col-xs-9">
																	<div class="clearfix">
																	<div class="icheck-inline">
																	<label>
																		<input type="radio" id="gen_layoutPercentage"  name="data[AdunitLayout][ad_gen_layout_width]" value="percentage">% </label>
																	<label>
																		<input type="radio" id="gen_layoutPixel" name="data[AdunitLayout][ad_gen_layout_width]" value="px"  > Pixel</label>
																	</div>
																</div>
															</div>
																<div class="col-xs-3"></div>
															</div>
														</div>
													</div>
															
															<div class="row">
																<div class="col-sm-4">
																	<div class="row">
																		<div class="col-xs-4"><label class="mt-5"># of Offers</label></div>
																		<div class="col-xs-8">
																			<p>
																				<?php echo $this->Form->input('AdunitLayout.ad_offers',array('class'=> 'form-control demo','div'=>false,'label'=>false
																				,'value'=>(!empty($tblAdunitsLayout['AdunitLayout']['ad_offers']))?$tblAdunitsLayout['AdunitLayout']['ad_offers']:'')); ?>
																				 <p>
																			</div>
																			</div>
																		</div>
																	</div>
																	
																	
																	<div class="row" id="subButton"  style="display:<?php echo ($button_script== '1')?'none':'block';?>">
																		<div class="col-sm-6">
																			<br><br>
																			<div class="row">
																				<div class="col-xs-4"><label class="mt-5">Submit Button Text</label></div>
																				<div class="col-xs-6">
																					<?php echo $this->Form->input('AdunitLayout.ad_button_label',array('class'=> 'form-control demo','div'=>false,'onkeyup'=>'changeButtonText(this);','label'=>false,'value'=>(!empty($tblAdunitsLayout['AdunitLayout']['ad_button_label']))?$tblAdunitsLayout['AdunitLayout']['ad_button_label']:'')); ?>
																				</div>
																			</div>
																			<br>
																			<div class="row">
																				<div class="col-xs-4"></div>
																				<div class="col-xs-6 hide">
																					<p>
																						<input type="submit" value="Choose from a pre-designed style" class="btn btn-success">
																					</p>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<h4>Button Size</h4>
																			<p>
																				<?php 
																				 $button_option=configure::read('button_option');
																				$attributes = array(
																					'class' => 'uniform-size',
																					'legend' => false,
																					'value' => (!empty($tblAdunitsLayout['AdunitLayout']['button_size']))?$tblAdunitsLayout['AdunitLayout']['button_size']:'',
																					'onClick' => 'changeButtonSize(this)'
																				);
																				 
																				echo $this->Form->radio('AdunitLayout.button_size', $button_option, $attributes);
																				?>
																				
																			 
																			</p>
																			<div class="form-group row">
																				<label class="control-label col-md-3">Button Color</label>
																				<div class="col-md-5">
																					<div id="cp3" class="input-group color colorpicker-default" data-color="#3865a8" data-color-format="hex">
																						<?php echo 
																						$this->Form->input('AdunitLayout.button_color',array('class'=> 'form-control color2','onkeyup'=>'changeButtonBgColor(this)','div'=>false,'label'=>false
																						,'value'=>(!empty($tblAdunitsLayout['AdunitLayout']['button_color']))?$tblAdunitsLayout['AdunitLayout']['button_color']:'#3865a8')); ?>
																						<span class="input-group-btn">
																							<button class="btn default" type="button"><i id="btc" style="background-color: #3865a8;"></i>&nbsp;</button>
																						</span>
																					</div>
																					<!-- /input-group -->
																				</div>
																			</div>
																			<div class="form-group row">
																				<label class="control-label col-md-3">Text Color</label>
																				<div class="col-md-5">
																					<div id="cp4" class="input-group color colorpicker-default" data-color="#3865a8" data-color-format="hex">
																						<?php echo 
																						$this->Form->input('AdunitLayout.text_color',array('class'=> 'form-control color3','onkeyup'=>'changeButtonTextColor(this)','value'=>(!empty($tblAdunitsLayout['AdunitLayout']['text_color']))?$tblAdunitsLayout['AdunitLayout']['text_color']:'#3865a8','div'=>false,'label'=>false)); ?>
																						<span class="input-group-btn">
																							<button class="btn default" type="button"><i id="btt" style="background-color: #3865a8;"></i>&nbsp;</button>
																						</span>
																					</div>
																					<!-- /input-group -->
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6" style=""> 
																				<div class="row">
																				<div class="col-xs-4"><label class="mt-5">Preview Button</label></div>
																				<div class="col-xs-6">
																				<?php  
																				$button_size = (!empty($tblAdunitsLayout['AdunitLayout']['button_size']))?$tblAdunitsLayout['AdunitLayout']['button_size']:'';
																				$button_width = 0;
																				if($button_size == 'S'){
																				 $button_width = 100;
																				}else if($button_size == 'M'){
																				  $button_width = 150;
																				}else if($button_size == 'L'){
																				 $button_width = 200;
																				}else if($button_size == 'XL'){
																				 $button_width = 250;
																				}else{
																				 $button_width = 75;
																				}
																				?>
																				<input class="btn default" type="button" id="Buttontext"
																				style="width:<?php echo $button_width?>px;background-color:<?php echo (!empty($tblAdunitsLayout['AdunitLayout']['button_color']))?$tblAdunitsLayout['AdunitLayout']['button_color']:'#3865a8'; ?>;
																				color:<?php echo (!empty($tblAdunitsLayout['AdunitLayout']['text_color']))?$tblAdunitsLayout['AdunitLayout']['text_color']:'#3865a8'; ?>; " 
																				value="<?php echo (!empty($tblAdunitsLayout['AdunitLayout']['ad_button_label']))?$tblAdunitsLayout['AdunitLayout']['ad_button_label']:'Submit'; ?>" />
																				</div>
																				</div>
																				</div>
																				<div class="col-sm-6" style=""> 
																					<div class="form-group row">
																					<label class="control-label col-md-3">Button hover Color</label>
																					<div class="col-md-5">
																					<div id="cp5" class="input-group color colorpicker-default" data-color="#3865a8" data-color-format="hex">
																					<?php echo $this->Form->input('AdunitLayout.button_hover_color',array('class'=> 'form-control color4','onkeyup'=>'changeButtonHover()','value'=>'#3865a8','div'=>false,'label'=>false)); ?>
																					<span class="input-group-btn">
																					<button class="btn default" type="button"><i id="btch" style="background-color: #3865a8;"></i>&nbsp;</button>
																					</span>
																					</div>
																					<!-- /input-group -->
																					</div>
																					</div>
																					<div class="form-group row">
																					<label class="control-label col-md-3">Text Hover Color</label>
																					<div class="col-md-5">
																					<div id="cp6" class="input-group color colorpicker-default" data-color="#3865a8" data-color-format="hex">
																					<?php echo $this->Form->input('AdunitLayout.text_hover_color',array('class'=> 'form-control color5','onkeyup'=>'changeButtonTextHover()','value'=>'#3865a8','div'=>false,'label'=>false)); ?>
																					<span class="input-group-btn">
																					<button class="btn default" type="button"><i id="btth" style="background-color: #3865a8;"></i>&nbsp;</button>
																					</span>
																					</div>
																					<!-- /input-group -->
																					</div>
																					</div>
																					
																					
																		</div>
																																									
																																							
																	</div>
																</div>
																
															</div>
															
															<div class="col-sm-12 hide">
																<h2>Preview Window</h2>
																
																<div class="tab-content">
																	<div class="tab-pane active" id="content-1">
																		<?php echo $this->Html->image('images/1.png',array('escape' => false));?>
																	</div>
																	<div class="tab-pane fade" id="content-2">
																		<?php echo $this->Html->image('images/2.png',array('escape' => false));?>
																	
																	</div>
																	<div class="tab-pane fade" id="content-3">
																		<?php echo $this->Html->image('images/1.png',array('escape' => false));?>
																	</div>
																	<div class="tab-pane fade" id="content-4">
																		<?php echo $this->Html->image('images/1.png',array('escape' => false));?>
																	
																	</div>
																	<div class="tab-pane fade" id="content-5">
																			<?php echo $this->Html->image('images/2.png',array('escape' => false));?>
																	</div>
																	<div class="tab-pane fade" id="content-6">
																		<?php echo $this->Html->image('images/3.png',array('escape' => false));?>
																	
																	</div>
																</div>
																
															</div>
														</div>
													</div><!--tab1 -->
													<div class="col-md-3 trigger_action">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                            <?php echo $this->Form->input('tblAdunit.tbl_select_all', array('type' => 'checkbox', 'id' => 'select_all', 'label' => false, 'div' => false, 'data-checkbox' => 'icheckbox_square-grey', 'class' => false,'checked'=> isset($tblAdunits['tblAdunit']['tbl_select_all']) && !empty($tblAdunits['tblAdunit']['tbl_select_all']) ? 'checked' :'', 'after' => __('<label for="fl_trigger_action">Select All Option </label>'))); ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php echo $this->Form->input('tblAdunit.tbl_select_one', array('type' => 'checkbox', 'id' => 'select_one', 'label' => false, 'div' => false, 'data-checkbox' => 'icheckbox_square-grey', 'class' => false,'checked'=> isset($tblAdunits['tblAdunit']['tbl_select_one']) && !empty($tblAdunits['tblAdunit']['tbl_select_one']) ? 'checked' :'',  'after' => __('<label for="fl_trigger_action">Select 1 Option </label>'))); ?>
                                                        </div>
                                                    </div>
                                                </div>
													<h3 class="form-section" >Field Mapping</h3>
													<div>
														<div class="portlet grey-cascade box">
															<div class="portlet-title">
																<div class="caption">Add/Modify Field Mapping</div>
															</div>
															<div class="portlet-body">
																<div class="table-responsive">
																	<table class="table table-hover table-bordered table-striped" id= "addmapfield">
																		<thead>
																			<tr>
																				<th width="15%">Available in placement</th>
																				<th width="15%" >Field Description</th>
																				
																				<th width="10%" >Field Format</th>
																				<th width="10%" >Field Type</th>
																				<th width="15%" >Value</th>
																				<th width="35%">Field Label in Publisher  Placement</th>
																			</tr>
																		</thead>
																		<tbody>
																			 <?php
																			 // these configure form app/config/config.php
																			$addfields=configure::read('addfields');
																			 
																			$totalfields = count($addfields)-1;
																			if(!empty($addfields)){ 
																				$counter =0;
																				foreach($addfields as $key=>$value){?>
																				<tr>
																					<?php $fieldValue= arrayfind($tblAdunitsFieldMapping, 'field_description', $value['description']);
																					?>
																					<td>
																						<?php echo $this->Form->input('FieldMapping.status.'.$counter, array('type'=>'checkbox','id'=>'status'.$counter,'label' => false,(!empty($fieldValue))?'checked':'',
																						 'hiddenField' => false,'div' => false,'class'=>false,'after'=>__('<label for="status'.$counter.'">Yes</label>')));?>
																						
																					</td>
																					<td>
																						
																						<?php echo $value['description'];?> <?php echo $this->Form->hidden('FieldMapping.custom.'.$counter,array('value'=>0,'div'=>false,'label'=>false)); ?><?php echo $this->Form->hidden('FieldMapping.field_description.',array('class'=> 'form-control demo auto-w','value'=>$value['description'],'div'=>false,'label'=>false)); ?>
																						<?php echo $this->Form->hidden('FieldMapping.field_name.'.$counter,array('class'=> 'form-control demo auto-w','value'=>$key,'div'=>false,'label'=>false)); ?>
																					</td>
																					<td>
																						<?php /// this is used for identify custom field
																						 
																						if(in_array($key,array("primary_phone_number","dob"))){
																							echo $this->Form->input('FieldMapping.field_format.'.$counter,array(
																							'label' => false,
																							'value'=>(!empty($fieldValue['field_format']))?$fieldValue['field_format']:'',
																							'placeholder'=>$value['placeholder'],
																							'class' => 'form-control'));} ?>
																						  
																						</td>
																					<td>
																						<?php 
																						$CollectFieldType =configure::read('CollectFieldType');
																						echo (!empty($CollectFieldType[$value['type']]))?$CollectFieldType[$value['type']]:'N/A'?>
																						<div style="display:none">
																							<?php
																							echo $this->Form->input('FieldMapping.field_type.'.$counter, 
																							array(
																								'label' => false,
																								'type' => 'select',
																								'class' => 'js-creative-field-type-array form-control',
																								'onchange'=>'ShowFieldType(this.value,'.$counter.')',
																								'options' => $CollectFieldType,
																								'selected' => $value['type'],
																								
																								));
																							?>
																						</div>
																					</td>
																					
																					<td>
																						<?php /// this is used for identify custom field
																						if(in_array($key,array("primary_phone_number","gender"))){
																							 echo $this->Form->input('FieldMapping.field_custom_text.'.$counter,array(
																							'label' => false,
																							'value'=>(!empty($fieldValue['field_custom_text']))?$fieldValue['field_custom_text']:'',
																							'placeholder'=>(!empty($value['placeholder']))?$value['placeholder']:array(),
																							'class' => 'form-control'));
																						} else if(in_array($key,array("dob"))){
																							echo $this->Form->input('FieldMapping.field_custom_text.'.$counter,array(
																							'label' => false,
																							'type' => 'select',
																							'onchange'=>'ShowDatePlaceholder(this.value,'.$counter.')',
																							'default'=>(!empty($fieldValue['field_custom_text']))?$fieldValue['field_custom_text']:'',
																							'options'=>(!empty($value['format']))?$value['format']:array(),
																							'class' => 'form-control dobformat'));
																						} 
																						 ?>
																						<div id="showFieldTypeValue<?php echo $counter ?>"></div> 
																					</td>	
																					
																					<td>
																						<?php echo $this->Form->input('FieldMapping.field_lable.'.$counter, array('label' => false, 'div' => false,'class'=>'form-control demo auto-w','value'=>(
																						!empty($fieldValue))?$fieldValue['field_lable']:''));
																						if($counter==$totalfields){
																							echo '<button id="b1" class="btn add-more" type="button">+</button>';
																						}
																						?>
																					 	
																				</td>
																			</tr>
																				<?php 
																				$counter++;
																				} // end of foreach of addfileds
																				
																			} // end of if condition of addfileds
																			
																			 ?> 
																			
																			
																			 
																			</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div><!--tab2 -->
													
													<div class="tab-pane hide" id="tab3"  >
														<div class="panel panel-default">
															<div class="panel-heading">
																<h4 class="panel-title">
																	<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3">
																	Data Centers  <span style="margin-left:20px;" id="total"></span> </a>
																</h4>
															</div>
															<div id="collapse_3_3" class="panel-collapse collapse in">
																<div class="panel-body">
																	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group">
																				<div class="col-md-12">
																					<select multiple="multiple" id="datacenter-select"  class="form-control" name="datacenter-select[]">
		<option value='Acxiom'>Acxiom</option>
		<option value='Experian'>Experian</option>
		<option value='Exelate'>Exelate</option>
		<option value='DataXU'>DataXU</option>
		</select>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																</div>
															</div>
														</div>
													</div><!--tab3 -->
												</div>
											</div>
											
										</div>
								</div>
							</div>
						</div>
						</div>
						 <h3 class="form-section" id="emailHDiv" style="display:<?php echo ($tblAdunits['tblAdunit']['ad_parent_id']==0 and ($tblAdunits['tblAdunit']['ad_ptype_id']==2))?'block':'none';?>;">Email Ad-Unit Details</h3>
						<div class="row" id="emailDiv" style="display:<?php echo ($tblAdunits['tblAdunit']['ad_parent_id']==0 and ($tblAdunits['tblAdunit']['ad_ptype_id']==2))?'block':'none';?>;">
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Internal / External</label>
									<div class="col-md-9">
										<div class="clearfix">
											<div class="icheck-inline">
												<label>
												<input type="radio" onclick="checkInternalExternal(this)" checked name="data[tblAdunit][ad_isExternal]" value="1"  <?php echo ($tblAdunits['tblAdunit']['ad_isExternal']==1)?'checked':''?> class="icheck">External </label>
												<label>
												<input type="radio" onclick="checkInternalExternal(this)" 
												name="data[tblAdunit][ad_isExternal]" <?php echo ($tblAdunits['tblAdunit']['ad_isExternal']==0)?'checked':''?> value="0" class="icheck"> Internal</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6" id="listSizeDiv"style="display:<?php echo ($tblAdunits['tblAdunit']['ad_parent_id']==0 and ($tblAdunits['tblAdunit']['ad_isExternal']==1))?'block':'none';?>;">
								<div class="form-group">
									<label class="control-label col-md-3">Email List Size</label>
									<div class="col-md-9">
										<?php echo 
										$this->Form->input('tblAdunit.ad_list_size',array('div'=>false,'label'=>false,'placeholder'=>'List Size','class'=>'form-control slug1','id'=>'slug1', 'value'=>$tblAdunits['tblAdunit']['ad_list_size']));?> 
										 
									</div>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6" id="espDiv" style="display:<?php 
							echo ($tblAdunits['tblAdunit']['ad_parent_id']==0 and ($tblAdunits['tblAdunit']['ad_ptype_id']==2  ))?'block':'none';?>;">
								<div class="form-group">
									<label class="control-label col-md-3">Select ESP Service</label>
									<div class="col-md-9">
										<?php 
										if($isSuccessLyris == 1) {
											$options[1] ='Lyris';
										}
										$options[2] ='Socketlabs';
										if($isSuccessMailchimp == 1) {
										$options[3] ='Mailchimp';
										}
										echo $this->Form->input('tblAdunit.ad_esp',array('type'=>'select','div'=>false,'label'=>false,'class'=>'form-control  js-example-basic-single','empty'=>'--Start typing or select ESP Name--', 'options'=>$options,'default'=>$tblAdunits['tblAdunit']['ad_esp']));?> 
										 
										<span class="help-block">
										Select your connected ESP with this list</span>
									</div>
								</div>
							</div>
							<div class="col-md-6" id="lyrisDiv"  style="display:<?php 
							echo ($tblAdunits['tblAdunit']['ad_parent_id']==0 and ($tblAdunits['tblAdunit']['ad_ptype_id']==2  ))?'block':'none';?>;">
								<div class="form-group">
									<label class="control-label col-md-3">Select List</label>
									<div class="col-md-9">
										
										<?php echo $this->Form->input('tblAdunit.ad_esp_list_id',array('type'=>'select','div'=>false,'label'=>false,'class'=>'form-control  js-esp-list-array','empty'=>'--Start typing or select ESP Name--', 'options'=>$adesplist,'default'=>$tblAdunits['tblAdunit']['ad_esp_list_id']));?> 
										<span class="help-block">
										Select your ESP associated with this email list</span>
									</div>
								</div>
							</div>
						</div>
						<!--/row-->
						<!-- start of email new letter  -->
						<div id="email-newletter" style="display:<?php 
							echo 
							($tblAdunits['tblAdunit']['ad_parent_id']==0 
							and ($tblAdunits['tblAdunit']['ad_ptype_id']==12 ||  $tblAdunits['tblAdunit']['ad_ptype_id']==2 ||
							$tblAdunits['tblAdunit']['ad_ptype_id']==13))?'block':'none';?>;">
							<h3 class="form-section" id="display-title" ><?php echo 
							($tblAdunits['tblAdunit']['ad_ptype_id']==12 )?'Email Auto Responder':($tblAdunits['tblAdunit']['ad_ptype_id']==12 )?'Dedicated Email':'Email News Letters';?></h3>
							<div class="row"  >
							<!--/span-->
								<div class="col-md-12 newsletters">
									<div class="form-group"> 
										<label class="control-label col-md-2"># of offers</label>
										<div class="col-md-5">
											<?php echo 
											$this->Form->input('tblAdunit.ad_offers',array( 'div'=>false,'label'=>false,'placeholder'=>'No Of Adds on a NewsLetter','class'=>'form-control',  'value'=>$tblAdunits['tblAdunit']['ad_offers']));?> 
											 
										</div>
									</div> 
								</div>
                                <div class="col-md-12 newsletters">
									<div class="form-group"> 
										<label class="control-label col-md-2">Sender Name</label>
										<div class="col-md-5">
											<?php echo 
											$this->Form->input('tblAdunit.sender_email',array( 'div'=>false,'label'=>false,'placeholder'=>'Sender Name','class'=>'form-control',  'value'=>$tblAdunits['tblAdunit']['sender_email']));?> 
											 
										</div>
									</div> 
								</div>
								<div class="col-md-12 newsletters">
									<div class="form-group"> 
										<label class="control-label col-md-2">Mailing Address Footer</label>
										<div class="col-md-8">
											<?php echo 
											$this->Form->textarea('tblAdunit.mailling_address_footer',array( 'div'=>false,'label'=>false,'placeholder'=>'Mailing Address Footer','rows'=>'4','class'=>'form-control',  'value'=>$tblAdunits['tblAdunit']['mailling_address_footer']));?> 
										 
										
										</div>
									</div> 
								</div> 
								<div class="col-md-12 newsletters">
									<div class="form-group"> 
										<label class="control-label col-md-2">Automatic Address Footer</label>
										<div class="col-md-8">
											<?php echo 
											$this->Form->textarea('tblAdunit.automatic_address_footer',array( 'div'=>false,'label'=>false,'placeholder'=>'Automatic Address Footer','rows'=>'4','class'=>'form-control',  'value'=>$tblAdunits['tblAdunit']['automatic_address_footer']));?> 
										 
										
										</div>
									</div> 
								</div> 
							</div>
					 	</div>
						<!-- end of new letter-->
						
						
						
						
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-offset-6 col-md-6">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>
				<?php echo $this->Form->end();?>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script>
	function  current_tab(tab_id){
		 $("#AdunitLayoutTabId").val(tab_id);

	}
	
$(document).ready(function(){
	$('.color2').colorpicker();
	$('.color2').colorpicker().on('changeColor', function(e) {
	$('#btc')[0].style.backgroundColor = e.color.toHex();
	 });
	$('.color3').colorpicker();
	$('.color3').colorpicker().on('changeColor', function(e) {
	$('#btt')[0].style.backgroundColor = e.color.toHex();
	 });
	$('.color4').colorpicker();
	$('.color4').colorpicker().on('changeColor', function(e) {
	$('#btch')[0].style.backgroundColor = e.color.toHex();
	 });
	$('.color5').colorpicker();
	$('.color5').colorpicker().on('changeColor', function(e) {
	$('#btth')[0].style.backgroundColor = e.color.toHex();
	 });

$('#cp3').colorpicker().on('changeColor', function(e) {
	
			$('#Buttontext')[0].style.backgroundColor = e.color.toHex();
	
        });

$('#cp4').colorpicker().on('changeColor', function(e) {
	
			$('#Buttontext')[0].style.color = e.color.toHex();
	
        });

	});
	
	function changeButtonText(option){
var button_text = option.value;
   //alert(button_text);
   document.getElementById("Buttontext").value = button_text;
}

function changeButtonSize(option){
var button_size = option.value ;
//alert(button_size);
if(button_size == "S"){
  document.getElementById("Buttontext").style.width = "100px";
}else if(button_size == "M"){
 document.getElementById("Buttontext").style.width = "150px";
}else if(button_size == "L"){
 document.getElementById("Buttontext").style.width = "200px";
}else if(button_size == "XL"){
 document.getElementById("Buttontext").style.width = "250px";
}else{
}
}

function changeButtonBgColor(option){
var Bg_color = option.value;

var bg = document.getElementById("AdunitLayoutButtonColor").value ;
//alert(bg);
document.getElementById("Buttontext").style.backgroundColor = bg;
document.getElementById("btc").style.backgroundColor = bg;
}
  
function changeButtonTextColor(){
var text = document.getElementById("AdunitLayoutTextColor").value ;
//alert(bg);
document.getElementById("Buttontext").style.color = text;
document.getElementById("btt").style.backgroundColor = text;
}

   window.onload = function() {
	   $(".dobformat").change(function(){
			var selectval = $(this).val();
			if(selectval ==3 || selectval ==2 ){
				$('#FieldMappingFieldLable10').attr("placeholder", "eg:- month;day;year");
			}else{
				$('#FieldMappingFieldLable10').removeAttr("placeholder");
			}
		});
          document.getElementById("Buttontext").onmouseover = function()
          {
		     var bghover = document.getElementById("AdunitLayoutButtonHoverColor").value ;
              this.style.backgroundColor = bghover;
			    var bgtexthover = document.getElementById("AdunitLayoutTextHoverColor").value ;
				this.style.color = bgtexthover;
				
          }
 
          document.getElementById("Buttontext").onmouseout = function()
          {
		      var bgcolor = document.getElementById("AdunitLayoutButtonColor").value ;
              this.style.backgroundColor = bgcolor;
			  var bgtextcolor = document.getElementById("AdunitLayoutTextColor").value ;
			  this.style.color = bgtextcolor;
          }
      }
  
	
	
	function checkMoreInfo(option){
		 
		switch(option.value){
			case "2" :
				document.getElementById("emailDiv").style.display = "block";
				document.getElementById("emailHDiv").style.display = "block";
				
				document.getElementById("coregDiv").style.display = "none";
				document.getElementById("coregHDiv").style.display = "none";
				document.getElementById("email-newletter").style.display = "block";
				document.getElementById("placement").style.display = "none";
				document.getElementById("ad_adtype_id").style.display = "none";
				document.getElementById("display-title").innerHTML = "Dedicated Email";
				$("#AdunitLayoutTabId").val('');
				break;
			 
			case "0" :
				document.getElementById("productType").style.display = "block";
				document.getElementById("emailHDiv").style.display = "none";
				document.getElementById("emailDiv").style.display = "none";
				document.getElementById("coregDiv").style.display = "none";
				document.getElementById("coregHDiv").style.display = "none";
				document.getElementById("email-newletter").style.display = "none";
				
				$("#AdunitLayoutTabId").val('');
				break;
			case "6" :
				document.getElementById("coregDiv").style.display = "block";
				document.getElementById("coregHDiv").style.display = "block";
				document.getElementById("emailDiv").style.display = "none";
				document.getElementById("emailHDiv").style.display = "none";
				document.getElementById("email-newletter").style.display = "none";
			
				$("#AdunitLayoutTabId").val(1);
				break;
			case "12" :
				document.getElementById("emailDiv").style.display = "none";
				document.getElementById("emailHDiv").style.display = "none";
				document.getElementById("coregDiv").style.display = "none";
				document.getElementById("coregHDiv").style.display = "none";
				document.getElementById("email-newletter").style.display = "block";
		
				document.getElementById("display-title").innerHTML = "Email Auto Responder";
				$("#AdunitLayoutTabId").val('');
				break;
			case "13" :
				document.getElementById("emailDiv").style.display = "none";
				document.getElementById("emailHDiv").style.display = "none";
				document.getElementById("coregDiv").style.display = "none";
				document.getElementById("coregHDiv").style.display = "none";
				document.getElementById("email-newletter").style.display = "block";
			
				document.getElementById("display-title").innerHTML = "Email News Letter";
				$("#AdunitLayoutTabId").val('');
				break;
			default :
				document.getElementById("emailDiv").style.display = "none";
				document.getElementById("emailHDiv").style.display = "none";
				document.getElementById("coregDiv").style.display = "none";
				document.getElementById("coregHDiv").style.display = "none";
				document.getElementById("email-newletter").style.display = "none";
			
				$("#AdunitLayoutTabId").val('');
				break;
		}
	}
	
	function checkParent(option) {
		switch(option.value){
			case "0" :
				document.getElementById("ad_ptype_id").style.display = "block";
				document.getElementById("placement").style.display = "block";
				document.getElementById("ad_adtype_id").style.display = "block";
				document.getElementById("publisher_id").style.display = "block";
				document.getElementById("email-newletter").style.display = "block";
				document.getElementById("emailDiv").style.display = "block";
				document.getElementById("emailHDiv").style.display = "block";
				document.getElementById("coregDiv").style.display = "block";
				document.getElementById("coregHDiv").style.display = "block";
			 
				break;
			default :
				document.getElementById("ad_ptype_id").style.display = "none";
				document.getElementById("placement").style.display = "none";
				document.getElementById("publisher_id").style.display = "none";
				document.getElementById("ad_adtype_id").style.display = "none";
				document.getElementById("email-newletter").style.display = "none";
				document.getElementById("emailDiv").style.display = "none";
				document.getElementById("emailHDiv").style.display = "none";
				document.getElementById("coregHDiv").style.display = "none";
				document.getElementById("coregDiv").style.display = "none";
				break;
		}
	}
	
	
	function checkInternalExternal(option){
		 
		switch(option.value){
			case "0" :
			 	document.getElementById("espDiv").style.display = "block";
				document.getElementById("lyrisDiv").style.display = "block";
				document.getElementById("listSizeDiv").style.display = "none";
				break;
			default :
				document.getElementById("espDiv").style.display = "none";
				document.getElementById("lyrisDiv").style.display = "none";
				document.getElementById("listSizeDiv").style.display = "block";
				break;
		}
	}
	function checkAudienceProfile(option){
		 
		switch(option.value){
			case "0" :
				document.getElementById("adAutoOptimize").style.display = "none";
				break;
			case "1" :
				document.getElementById("adAutoOptimize").style.display = "block";
			break;	
			default :
				document.getElementById("adAutoOptimize").style.display = "none";
				break;
		}
	}
	
	function checkGenLayout(option){
			switch(option.value){
			case "0" :
			document.getElementById("layoutWidth").style.display = "none";
			break;
			case "1" :
			document.getElementById("layoutWidth").style.display = "block";
			break;	
			default :
			document.getElementById("layoutWidth").style.display = "block";
			break;
		}
	}
	
	function changeButtonHover(){
  var bghover = document.getElementById("AdunitLayoutButtonHoverColor").value ;
  document.getElementById("btch").style.backgroundColor = bghover;
  }
 function changeButtonTextHover(){
 var bgtexthover = document.getElementById("AdunitLayoutTextHoverColor").value ;
  document.getElementById("btth").style.backgroundColor = bgtexthover;
  }
  
	function hideColorButton(cb){
		if(cb.checked){
			document.getElementById("subButton").style.display = "none";
		}else{
			document.getElementById("subButton").style.display = "block";
		}
	}
	
	/*jQuery(document).ready(function(){
    var next = 1;
    jQuery(".add-more").click(function(e){
		  e.preventDefault();
        alert('dfdsfd');
    });   
});*/
		function ShowFieldType(fieldtype,key){
			 var id ="showFieldTypeValue"+key;
			  var date_type ="ShowDatetype"+key;
			 $('#'+date_type).html('');
			 if(fieldtype ==2 || fieldtype ==3|| fieldtype ==5){
				document.getElementById(id).innerHTML = "<input name='data[FieldMapping][field_custom_text]["+key+"]' value='' class='form-control' type='text' id='FieldMappingCustomValues' placeholder='Put values as semicolon seprated eg:- emp1;emp2'> ";
			}else if(fieldtype ==4){
				document.getElementById(id).innerHTML = "<input name='data[FieldMapping][field_custom_text]["+key+"]' value='' class='form-control' type='text' id='FieldMappingCustomValues' placeholder='Default value here'> ";
			}
			else if(fieldtype ==6){
				 <?php 
					$DateFormats = configure::read('DateFormat'); 
					$DateFormatOption = "";
					if(!empty($DateFormats)){
						foreach($DateFormats as $key=>$value){
									$DateFormatOption = $DateFormatOption. "<option value='".$key."'>".$value."</option>";
						}
					}
				 ?>
				 $('#'+date_type).html("<input name='data[FieldMapping][field_format]["+key+"]' value='' class='form-control' type='text' id='field_format"+key+"' placeholder='eg:-mm/dd/yyyy'>");
				$('#'+id).html( "<select  class='js-creative-field-type-array form-control' name='data[FieldMapping][field_custom_text]["+key+"]' onchange ='ShowDatePlaceholder(this.value,"+key+");'id='fld_custom_texts"+key+"'><?php echo preg_replace( "/\r|\n/", "",$DateFormatOption);?></select>");
			}
			else{
				document.getElementById(id).innerHTML = "";
			}
		} 
		function ShowDatePlaceholder(value,key) {
			if(value ==3 || value ==2 ){
					$('#FieldMappingFieldLable'+key).attr("placeholder", "eg:- month;day;year");
				}else{
					$('#FieldMappingFieldLable'+key).removeAttr("placeholder");
				}
		}
</script>
<style>
	.panel-default>.panel-heading {
	background-color : #73716e !important;
	color : #fff;
	}
	
	.panel-default>.panel-heading > .panel-title > a:hover {
	color : #fff;
	text-decoration:underline;
	}
	
	.panel-title{
	font-weight:200 !important;
	}
	</style>
	<style>
						.style-section label{
						font-size:12px;
						}
						.style-section p{
						margin:0 0 5px;
						}
						.tabs-sectoin{
						}
						.tabs-sectoin .nav-tabs img{
						max-height:55px;
						}
						.box-info{
						padding:30px;
						}
						.box-info label,
						.box-info .field{
						display:inline-block;
						vertical-align:middle;
						}
						.box-info .field{
						border:1px solid #e5e5e5;
						margin:0 5px;
						}
						.tab-content img{
						max-width:100%;
						}
						.auto-w{
						width:auto;
						display:inline-block;
						}
						.mt-5{
						margin-top:5px !important;
						}
					</style>
<?php 
function arrayfind($multidimensionals, $field, $value){
	foreach($multidimensionals as $key => $multidimensional){
	     
      if ( $multidimensional['FieldMapping'][$field] === $value )
          return $multidimensional['FieldMapping'];
   }
   return array();
   
}
?>