<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/inventory_management/">Inventory Management</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/inventory_management/addAdPlacement">Ad Placement</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i> Inventory - Ad Placement
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				<form action="/inventory_management/addAdPlacement" id="submit_form" method="POST" class="form-horizontal">
					<div class="form-body">
						<h3 class="form-section">Add Ad Placement</h3>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Name <span class="required">
									* </span></label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="data[tblPlacement][p_name]" placeholder="Enter Placement Name Here">
										<span class="help-block">
										Enter Placement Name </span>
									</div>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group has-error">
									<label class="control-label col-md-3">Publisher</label>
									<div class="col-md-9">
										<select name="data[tblPlacement][p_publisher_id]" id="ddlPublishers" class="select2me form-control">
											<?php foreach ($publishers as $option){?>
												<option value="">--Select--</option>
												<option value="<?php echo $option['tblProfile']['user_id'] ?>"><?php echo $option['tblProfile']['CompanyName'] ?></option>
											<?php } ?>
										</select>
										<span class="help-block">
										Select The Publisher. </span>
									</div>
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Description</label>
									<div class="col-md-9">
										<textarea rows="3" name="data[tblPlacement][p_desc]" class="form-control"></textarea>
										<span class="help-block">
										Enter Placement Description.</span>
									</div>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Ad Unit Size</label>
									<div class="col-md-9">
										<select name="data[tblPlacement][p_adunits]" id="ddlAduints" class="select2me form-control">
												<option value="">--Select--</option>
										</select>
										<span class="help-block">
										Select Ad Unit Size. </span>
									</div>
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-offset-6 col-md-6">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<?php
$this->Js->get('#ddlPublishers')->event('change', 
$this->Js->request(array(
'controller'=>'InventoryManagement',
'action'=>'ajax_getAdUnitByPublisher'
), array(
'update'=>'#ddlAduints',
'async' => true,
'method' => 'post',
'dataExpression'=>true,
'data'=> $this->Js->serializeForm(array(
'isForm' => true,
'inline' => true
))
))
);
?>