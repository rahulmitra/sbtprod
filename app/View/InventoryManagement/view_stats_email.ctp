<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */
if (!Configure::read('debug')):
    throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>
<style>
    .font-hg {
        color:#000 !important;
        font-size:48px !important;
        font-weight:bold !important;
    }
    .font-grey-mint {
        color:#000 !important;
    }
    .font-sm {
        font-size:16px !important;
    }
    .emailtab{background-color: #fff;margin-top: 30px;}
    .emailtab  li {padding:15px !important;margin-right: 0px;background-color: #fff;border-bottom: 2px solid #e3e3e3;}
    .emailtab  li.active {padding:15px !important;background-color: #fff;border:2px solid #e3e3e3;border-width: 2px 2px 0px 2px;}
    .emailtab  li.dropdown {border:2px solid #e3e3e3;border-width: 0px 0px 2px 0px !important;}

</style>
<!-- BEGIN PAGE BREADCRUMB -->

<!-- END PAGE BREADCRUMB -->
<div class="portlet-body">
    <a class="btn green">Add New <i class="fa fa-plus"></i></a>
    <div class="row">
        <div class="col-sm-9">
            <div class="tabbable-line">
                <ul class="emailtab">
                    <li class="<?php echo (empty($type) || ($tab == 'tab1')) ? 'active dropdown' : ''; ?>">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown"><?php echo $adunit_name; ?>
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                <?php
                                if (!empty($adunit_list)) {
                                    foreach ($adunit_list as $key => $adunit) {
                                        ?>
                                        <li role="presentation"><?php echo $this->Html->link($adunit, array('controller' => 'inventory_management', 'action' => 'view_stats_email', $key, $tab, $duration), array('escape' => false, 'role' => 'menuitem', 'tabindex' => '-1', 'class' => 'step', 'title' => $adunit)); ?></li>
                                        <?php
                                    } // end of foreach conditions
                                } // end of if conditions
                                ?>

                            </ul>
                        </div>
                        <?php //echo $this->Html->link($adunit_name, array('controller' => 'inventory_management', 'action' => 'view_stats_email',$nl_adunit_uuid, 'tab1',$duration), array('escape' => false,'class'=>'step','title'=> __('QH.com Ad-Unit-Dedicated Emails List1')))  ?>
                    </li>

                    <li class="<?php echo ($tab == 'tab2') ? 'active' : ''; ?>">
                        <?php echo $this->Html->link('<i class="fa fa-dollar"></i>&nbsp;REVENUE', array('controller' => 'inventory_management', 'action' => 'view_stats_email', $nl_adunit_uuid, 'tab2', $duration), array('escape' => false, 'class' => 'step', 'title' => __('REVENUE'))) ?>
                    </li>
                    <li class="<?php echo ($tab == 'tab3') ? 'active' : ''; ?>">
                        <?php echo $this->Html->link('<i class="fa fa-area-chart"></i>&nbsp;IMPRESSION REPORT', array('controller' => 'inventory_management', 'action' => 'view_stats_email', $nl_adunit_uuid, 'tab3', $duration), array('escape' => false, 'class' => 'step', 'title' => __('IMPRESSION REPORT'))) ?>
                    </li>
                    <li class="<?php echo ($tab == 'tab4') ? 'active' : ''; ?>">
                        <?php echo $this->Html->link('<i class="fa fa-users"></i>&nbsp;AUDIENCE PROFILE', array('controller' => 'inventory_management', 'action' => 'view_stats_email', $nl_adunit_uuid, 'tab4', $duration), array('escape' => false, 'class' => 'step', 'title' => __('AUDIENCE PROFILE'))) ?>
                    </li>

                    <li class="<?php echo ($tab == 'tab5') ? 'active' : ''; ?>">
                        <?php echo $this->Html->link('<i class="fa fa-calculator"></i>&nbsp;INVENTORY', array('controller' => 'inventory_management', 'action' => 'view_stats_email', $nl_adunit_uuid, 'tab5', $duration), array('escape' => false, 'class' => 'step', 'title' => __('INVENTORY'))) ?>
                    </li>
                    <?php /*<li class="<?php echo ($tab == 'tab6') ? 'active' : ''; ?>">
                        <?php echo $this->Html->link('<i class="fa fa-cogs"></i>&nbsp;SETTING', array('controller' => 'inventory_management', 'action' => 'view_stats_email', $nl_adunit_uuid, 'tab6', $duration), array('escape' => false, 'class' => 'step', 'title' => __('SETTING'))) ?>
                    </li> */?>


                </ul>
            
            </div>
        </div>
        <div class="col-sm-3">
                <div class="portlet-title" style="float:right;">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo $duration == "day" ? "active" : "" ?>">
                                <input type="radio" name="options" onchange="chart('day')" class="toggle" id="option1">Today</label>
                            <label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo $duration == "week" ? "active" : "" ?>">
                                <input type="radio" name="options"  onchange="chart('week')" class="toggle" id="option2">Last 7 Days</label>
                            <label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo $duration == "month" ? "active" : "" ?>">
                                <input type="radio" name="options"  onchange="chart('month')" class="toggle" id="option3">MTD</label>
                            <label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo $duration == "year" ? "active" : "" ?>">
                                <input type="radio" name="options"  onchange="chart('year')" class="toggle" id="option2">YTD</label>
                        </div>
                    </div>
                </div>
        </div>
    </div>

</div>
<div class="tab-content">
	<?php
		// Show tab show between tab1 to tab6
		if(in_array($tab,array('tab1','tab2','tab3','tab4','tab5','tab6'))){
			echo $this->element('start_email/'.$tab);
		}
    ?>
</div>
</div>
<style>
    #sample_4_filter { 
        float:right;
    }
</style>	
<script>					
	function chart(arg){
	var href= '<?php echo Router::url(array('controller' => 'inventory_management', 'action' => 'view_stats_email',$nl_adunit_uuid,$tab));?>/';
	if(href)
	{
		window.location = href  + arg+"/";
		} else {
		window.location = "/messages/index/all/day";
	}
}
</script>					
