<?php
	/**
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Pages
		* @since         CakePHP(tm) v 0.10.0.1076
	*/
	
	if (!Configure::read('debug')):
	throw new NotFoundException();
	endif;
	
	App::uses('Debugger', 'Utility');
?>
<div class="portlet-body">
	<div class="tabbable-line">
		<ul class="nav nav-tabs ">
			<li class="active">
				<a href="#tab_15_1" data-toggle="tab">
				REVENUE SUMMARY </a>
			</li>
			<li  style="display:none;">
				<a href="#tab_15_2" data-toggle="tab">
				SPEND SUMMARY </a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_15_1">
				<!-- BEGIN PAGE CONTENT INNER -->
				<div class="row">
					<div class="col-md-12">
						<?php echo $this->element('revenue_widget'); ?>
					</div>
					<div class="col-md-6">
						<?php echo $this->element('monthly_revenue_breakdown_by_source'); ?>
						<?php echo $this->element('lead_generation_revenue_breakdown_placement'); ?>
						<?php echo $this->element('email_list_revenue'); ?>
					</div>
					<div class="col-md-6">
						<!-- BEGIN PORTLET-->
						<?php echo $this->element('daily_revenue_trend'); ?>
						<?php echo $this->element('lead_generation_daily_revenue_trends'); ?>
						<!-- BEGIN PORTLET-->
						<div class="portlet light tasks-widget">
							<div class="portlet-title"> 
								<div class="caption caption-md">
									<i class="icon-bar-chart theme-font hide"></i>
									<span class="caption-subject theme-font bold uppercase">Live Feed</span>
									<span class="caption-helper">Latest 3 Articles</span>
								</div>
								<div class="inputs">
									<div class="portlet-input input-small input-inline">
										<div class="input-icon right">
											<i class="icon-magnifier"></i>
											<input type="text" class="form-control form-control-solid" placeholder="search...">
										</div>
									</div>
								</div>
							</div>
							<div class="portlet-body">
								<div class="task-content">
									<div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
										<!-- START TASK LIST -->
										<div id="widgetIframe"><iframe width="100%" height="700" src="https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=SiliconRssWidget&actionToWidgetize=rssPiwik&idSite=3&period=range&date=2015-07-22,2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>
										<!-- END START TASK LIST -->
									</div>
								</div>
								<div class="task-footer">
									<div class="btn-arrow-link pull-right">
										
									</div>
								</div>
							</div>
						</div>
						<!-- END PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT INNER -->
			</div>
			<div class="tab-pane" id="tab_15_2">
				<div class="row">
					<div class="col-md-12">
						<?php echo $this->element('expenditure_widget'); ?>
					</div>
					<div class="col-md-6">
						<?php //echo $this->element('monthly_revenue_breakdown_by_source'); ?>
						<?php //echo $this->element('lead_generation_revenue_breakdown_placement'); ?>
						<?php //echo $this->element('email_list_revenue'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function viewSummary(period){
		if(period == "year")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=3&period=year&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "week")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=3&period=week&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "month")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=3&period=month&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "day")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=3&period=day&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		
		document.getElementById('viewSummaryiframe').src = loc;
		
	}
	
	
	function viewSummarygraph(period)
	{
		
		if(period == "year")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=year&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "week")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=week&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "month")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=month&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "day")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=day&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		
		document.getElementById('viewSummarygraphiframe').src = loc;
	}
	
	function pageviewSummarygraph(period)
	{
		
		if(period == "year")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_pageviews&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=year&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "week")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_pageviews&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=week&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "month")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_pageviews&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=month&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "day")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_pageviews&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=day&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		
		document.getElementById('pageviewSummarygraphiframe').src = loc;
	}
</script>