<div class="portlet light ">
							<div class="portlet-title">
								<div class="caption caption-md">
									<i class="icon-bar-chart theme-font hide"></i>
									<span class="caption-subject theme-font bold uppercase">Visits Over Time</span>
									<span class="caption-helper hide">weekly stats...</span>
								</div>
								<div class="actions">
									<div class="btn-group btn-group-devided" data-toggle="buttons">
										<label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
										<input type="radio" name="options" class="toggle" onchange="viewSummarygraph('day')">Day</label>
										<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
										<input type="radio" name="options" class="toggle" onchange="viewSummarygraph('week')">Week</label>
										<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
										<input type="radio" name="options" class="toggle" onchange="viewSummarygraph('month')">Month</label>
										<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
										<input type="radio" name="options" class="toggle" onchange="viewSummarygraph('year')">Year</label>
									</div>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-scrollable table-scrollable-borderless">
									<div id="widgetIframe"><iframe width="100%" id="viewSummarygraphiframe" height="240" src="https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=day&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>
								</div>
							</div>
						</div>
						<!-- END PORTLET-->

