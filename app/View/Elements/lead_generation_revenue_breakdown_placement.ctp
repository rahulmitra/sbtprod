<div class="portlet light ">
	<div class="portlet-title">
		<div class="caption caption-md">
			<i class="icon-bar-chart theme-font hide"></i>
			<span class="caption-subject theme-font bold uppercase">Lead-Generation Revenue Breakdown by Placement</span>
		</div>
		<div class="actions">
			<span class="caption-subject theme-font bold uppercase">$<?php echo $totalLeadGenMonth = $monthlyCoreg +  $monthlyHostPost + $monthlyLandingPage; ?></span>
		</div>
	</div>
	<div class="portlet-body">
		<?php if($totalLeadGenMonth == 0) { ?>
			No Revenue from Lead-Generation this Month
		<?php } ?>
		<div id="piechart_3d_lead_gen_pla" style="width: 100%; height: 300px;"></div>
	</div>
</div>
<!-- END PORTLET-->
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
        var data = google.visualization.arrayToDataTable([
		['Task', 'Hours per Day'],
		['Co Registration',    <?php echo $monthlyCoreg; ?>],
		['Host & Post',      <?php echo $monthlyHostPost; ?>],
		['Landing Page',  <?php echo $monthlyLandingPage; ?>]
        ]);
		
		var formatter = new google.visualization.NumberFormat({
			prefix: '$'
		});
		formatter.format(data, 1);
		
        var options = {
			'width':'100%',
			'height':'300',
			chartArea:{left:45,top:20,width:"100%",height:"90%"},
			pieHole: 0.4,
			pieSliceText: 'value'
		};
		
        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d_lead_gen_pla'));
        chart.draw(data, options);
	}
</script>