<div class="hor-menu ">
	<ul class="nav navbar-nav">
		<li>
		<?php echo $this->Html->link('Dashboard','/');?>
	</li>
	<li class="menu-dropdown classic-menu-dropdown">
		<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
			Order Management <i class="fa fa-angle-down"></i>
		</a>
		<ul class="dropdown-menu pull-left">
			<li class="">
				<a href="">
					<i class="icon-plus"></i>
				Create Insertion Orders </a>
			</li>
			<li class="">
				<a href="/company/">
					<i class="icon-list"></i>
				View Insertion Orders </a>
			</li>
			<li class="">
					<a href="/insertionorder/add">
						<i class="icon-plus"></i>
					Add New Orders </a>
				</li>
				<li class="">
					<a href="/insertionorder/add_line_item">
						<i class="icon-plus"></i>
					Create New Line Items </a>
				</li>
				<li class="">
					<a href="/orders/">
						<i class="icon-list"></i>
					View All Orders </a>
				</li>
			
		</ul>
	</li>

		<li class="menu-dropdown classic-menu-dropdown ">
			<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
				Lead Management <i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu pull-left">
				<li class="">
					<a href="">Create Lead Source </a>
				</li>
				<li class="">
					<a href="">View Lead Sources </a>
				</li>
				<li class="">
					<a href="">View Source Leads </a>
				</li>
				<li class="">
					<a href="">Manage Source Leads</a>
				</li>
				<li class="">
					<a href="">Advertiser Lead Management </a>
				</li>
				<li class="">
					<a href="">Validation Management </a>
				</li>
				<li class="">
					<a href="">Lead Reconciliation</a>
				</li>
			</ul>
		</li>
		<li class="menu-dropdown classic-menu-dropdown ">
			<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
				Lead Gen Inventory Management<i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu pull-left">
				<li class="">
					<a href="/inventory_management/addAdUnit">
						<i class="icon-plus"></i>
					Add Ad Unit </a>
				</li>
			
				<li class="">
					<a href="/inventory_management/">
						<i class="icon-list"></i>
					View Ad Units </a>
				</li>
				<li class="">
					<a href="">Lead Gen Offer Management</a>
						
					
				</li>
			</ul>
		</li>
		<li class="menu-dropdown classic-menu-dropdown ">
			<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
				Native Inventory Management<i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu pull-left">
				<li class="">
					<a href="/inventory_management/addAdUnit">
						<i class="icon-plus"></i>
					Add Ad Unit </a>
				</li>
			
				<li class="">
					<a href="/inventory_management/">
						<i class="icon-list"></i>
					View Ad Units </a>
				</li>
		
			</ul>
		</li>
		
		<li class="menu-dropdown classic-menu-dropdown ">
			<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
				Email Management<i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu pull-left">
			<li class="">
					<a href="">
						<i class="icon-plus"></i>
					Add Email Lists </a>
				</li>
				<li class="">
					<a href="/messages/view_email_lists">
						<i class="icon-list"></i>
					View Email Lists </a>
				</li>
				<li class="">
					<a href="">
						<i class="icon-plus"></i>
					Create Email </a>
				</li>
				<li class="">
					<a href="">
						<i class="icon-list"></i>
					View Emails </a>
				</li>
				<li class="">
					<a href="/messages/create/">
						<i class="icon-list"></i>
					Create Newsletter </a>
				</li>
				<li class="">
					<a href="">
						<i class="icon-list"></i>
					View Newsletter </a>
				</li>
				<li class="">
					<a href="">
						<i class="icon-list"></i>
					View Calender </a>
				</li>
				<li class="">
					<a href="">
						<i class="icon-plus"></i>
					Create Drip Series </a>
				</li>
				<li class="">
					<a href="">
						<i class="icon-list"></i>
					View Drip Series </a>
				</li>
				<li class="">
					<a href="">
						<i class="icon-plus"></i>
					Create Trigger Email </a>
				</li>
				<li class="">
					<a href="">
						<i class="icon-list"></i>
					View Trigger Emails </a>
				</li>
			</ul>
		</li>
		
		<li class="menu-dropdown classic-menu-dropdown ">
			<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
				Reporting <i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu pull-left">
				<li class=""><a href="/">Offer Performance Estimates </a></li>
				<li class=""><a href="/">Email Performance Estimates </a></li>
				<li class=""><a href="/">Lead Gen Performance Estimates </a></li>
				<li class=""><a href="/">Native Unit Performance Estimates </a></li>
				<li class=""><a href="/">Source Performance Estimates </a></li>
				<li class=""><a href="/">Offer Reporting </a></li>
				<li class=""><a href="/">Email Compaign Reporting </a></li>
				<li class=""><a href="/">Lead Gen Reporting </a></li>
				<li class=""><a href="/">Native Reporting </a></li>
			</ul>
		</li>

</ul>
</div>
