 

// count allocation 


$('#event_period').datepicker({
	inputs: $('.actual_range'),
	autoclose: true,
	todayHighlight: true,
	startDate: '<?php echo $lineItem_details['tblLineItem']['li_start_date']; ?>',
	endDate: '<?php echo $lineItem_details['tblLineItem']['li_end_date']; ?>'
}); 

$('#geography-select').multiSelect({
	selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search location'>",
	selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search location'>",
	afterInit: function(ms){
		var that = this,
		$selectableSearch = that.$selectableUl.prev(),
		$selectionSearch = that.$selectionUl.prev(),
		selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
		selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
		
		that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
		.on('keydown', function(e){
			if (e.which === 40){
				that.$selectableUl.focus();
				return false;
			}
		});
		
		that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
		.on('keydown', function(e){
			if (e.which == 40){
				that.$selectionUl.focus();
				return false;
			}
		});
	},
	afterSelect: function(){
		this.qs1.cache();
		this.qs2.cache();
	},
	afterDeselect: function(){
		this.qs1.cache();
		this.qs2.cache();
	}
});

$("#behaviour").select2({
	maximumSelectionLength: 1,
	placeholder: "Start Typing to Select ONLY ONE "
});
$("#audience1").select2({
	
	placeholder: "Start Typing to Select"
});
$("#audience2").select2({
	
	placeholder: "Start Typing to Select"
});
$("#audience3").select2({
	
	placeholder: "Start Typing to Select"
});
$("#audience4").select2({
	
	placeholder: "Start Typing to Select"
});
$("#audience5").select2({
	
	placeholder: "Start Typing to Select"
});
$("#audience6").select2({
	
	placeholder: "Start Typing to Select"
});
$("#audience7").select2({
	
	placeholder: "Start Typing to Select"
});
$("#selectMediaType").select2({
	maximumSelectionLength: 1,
	placeholder: "Start Typing to Select Media Type "
});

$("#selectAdUnit").select2({
	maximumSelectionLength: 1,
	placeholder: "Start Typing to Select Ad Unit "
});

$('#selectMediaType').change(function(){
	var favorite = [];
	var selectedItem = [];
	$.each($("#selectMediaType :selected"), function(){
		favorite.push($(this).val());
	});
	$('#selectAdUnit :selected').each(function(i, selected){ 
		selectedItem[i] = $(selected).val(); 
	});
	var selecid = favorite.join(",");
	
	if(selecid ==6){
	 	$('.trigger_action').hide();
	}else{
		$('.trigger_action').show();
	}
	$("#adunit-Creative-type").empty();
	$('#adunit-Creative-type').multiSelect('refresh');
	//alert("My favourite sports are: " + favorite.join(", "));
	$.ajax({
		type: "POST",
		url: "/insertionorder/getCreativeByMediaType",
		data:'media_id='+favorite.join(",")+'&li_dfp_id=<?php echo $lineItem_details['tblLineItem']['li_dfp_id'];  ?>',
		dataType: "json",
		success: function(data){
			var count = 0;
			$.each(data, function(index) {
				$('#adunit-Creative-type').multiSelect('addOption', { value: data[index].id, text: data[index].text, index: count });
				count++;
			});
		 <?php if($this->params['action'] == "edit_line_item_flight"){
				$selected='';
				 if(!empty($creative_select))
					 $selected = "'".implode("','",$creative_select)."'";
				?> 
				$('#adunit-Creative-type').multiSelect('select', [<?php echo $selected;?>]);
			<?php }?>
			$('#adunit-Creative-type').multiSelect('refresh');
		}
	});
	if(selecid == 2 || selecid == 12 || selecid == 13){
		$("#behaviourHeader").slideDown();
		$("#behaviourSettings").slideDown();
		$("#deliveryHeader").slideUp();
		$("#deliverySettings").slideUp();						
		} else {
		$("#behaviourSettings").slideUp();
		$("#behaviourHeader").slideUp();
		$("#deliveryHeader").slideDown();
		$("#deliverySettings").slideDown();	
	}
	
});

//  04-05-16
$('#selectMediaType').change(function(){
	
	// Remove all Manage Lead Allocation
	$('#adunitcontent tr.ManageLead').remove();
	
	var favorite = [];
	var selectedItem = [];
	$.each($("#selectMediaType :selected"), function(){
		favorite.push($(this).val());
	});
	$('#selectAdUnit :selected').each(function(i, selected){ 
		selectedItem[i] = $(selected).val(); 
	});
	var selecid = favorite.join(",");
	$("#adunit-select").empty();
	$('#adunit-select').multiSelect('refresh');
	//alert("My favourite sports are: " + favorite.join(", "));
	$.ajax({
		type: "POST",
		url: "/insertionorder/getAdUnitsByMediaType",
		data:'media_id='+favorite.join(","),
		dataType: "json",
		success: function(data){
			var count = 0;
			$.each(data, function(index) {
				$('#adunit-select').multiSelect('addOption', { value: data[index].id, text: data[index].text, index: count });
				count++;
			});
			
			<?php if($this->params['action'] == "edit_line_item_flight"){
				$selected='';
				 if(!empty($adunit_select))
					 $selected = "'".implode("','",$adunit_select)."'";
				?> 
				$('#adunit-select').multiSelect('select', [<?php echo $selected;?>]);
			<?php }?>
			$('#adunit-select').multiSelect('refresh');
		}
	});
	if(selecid == 2 || selecid == 12 || selecid == 13){
		$("#behaviourHeader").slideDown();
		$("#behaviourSettings").slideDown();
		$("#deliveryHeader").slideUp();
		$("#deliverySettings").slideUp();						
		} else {
		$("#behaviourSettings").slideUp();
		$("#behaviourHeader").slideUp();
		$("#deliveryHeader").slideDown();
		$("#deliverySettings").slideDown();	
	}
	
});
$('#adunit-Creative-type').multiSelect({
	selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search creative type'>",
	selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search creative type'>",
	afterInit: function(ms){
		var that = this,
		$selectableSearch = that.$selectableUl.prev(),
		$selectionSearch = that.$selectionUl.prev(),
		selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
		selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
		
		that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
		.on('keydown', function(e){
			if (e.which === 40){
				that.$selectableUl.focus();
				return false;
			}
		});
		
		that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
		.on('keydown', function(e){
			if (e.which == 40){
				that.$selectionUl.focus();
				return false;
			}
		});
	},
	afterSelect: function(){
		this.qs1.cache();
		this.qs2.cache();
	},
	afterDeselect: function(){
		this.qs1.cache();
		this.qs2.cache();
	}
});
//end of 04-05-16


$('#adunit-select').multiSelect({
	selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search publisher or ad unit'>",
	selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search publisher or ad unit'>",
	afterInit: function(ms){
		var that = this,
		$selectableSearch = that.$selectableUl.prev(),
		$selectionSearch = that.$selectionUl.prev(),
		selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
		selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
		
		that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
		.on('keydown', function(e){
			if (e.which === 40){
				that.$selectableUl.focus();
				return false;
			}
		});
		
		that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
		.on('keydown', function(e){
			if (e.which == 40){
				that.$selectionUl.focus();
				return false;
			}
		});
	},
	afterSelect: function(){
		this.qs1.cache();
		this.qs2.cache();
	},
	afterDeselect: function(){
		this.qs1.cache();
		this.qs2.cache();
	}
});

$('input[name="data[tblDeliverySetting][DeliveryMethod]"]').on('ifChanged', function(event){
	var selectedVal = "";
	var selected = $("input[type='radio'][name='data[tblDeliverySetting][DeliveryMethod]']:checked");
	 
	if (selected.length > 0) {
		selectedVal = selected.val();
	}
	deliveryMethod(selectedVal);
	
});
function deliveryMethod(selectedVal){
	if(selectedVal == 1){
		$("#realTimeDelivery").slideDown();
		$("#batchPost").slideUp();
		$("#httpGetPost").slideUp();
		$("#ftpUpload").slideUp();
		} else if(selectedVal == 2){
		$("#batchPost").slideDown();
		$("#realTimeDelivery").slideUp();
		$("#httpGetPost").slideUp();
		$("#ftpUpload").slideUp();
		} else if(selectedVal == 3){
		$("#ftpUpload").slideDown();
		$("#batchPost").slideUp();
		$("#httpGetPost").slideUp();
		$("#realTimeDelivery").slideUp();
		} else if(selectedVal == 4){
		$("#httpGetPost").slideDown();
		$("#batchPost").slideUp();
		$("#ftpUpload").slideUp();
		$("#realTimeDelivery").slideUp();
	}
}
<?php if($this->params['action'] == "edit_line_item_flight"){?> 
	$("#selectMediaType").change();
	<?php if(!empty($this->request->data['tblDeliverySetting']['DeliveryMethod'])){ ?>
		var selectedVal =parseInt('<?php echo $this->request->data['tblDeliverySetting']['DeliveryMethod'];?>');
		deliveryMethod(selectedVal);
	<?php }?> 
	function adunit_apend_edit(id,textlabal,dailyAllocation,monthlyAllocation,allocation,rev_share){
		
		document.getElementById("changeAdUnitDiv").style.display = "block";
		$('#adunitcontent tr:last').after('<tr class="ManageLead '+id+'"><td>'+textlabal+'</td> <td><input name="dailyAllocation[' + id + ']" value="'+dailyAllocation+'" data-inputmask="\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true" placeholder="Daily Allocation"   class="form-control allocation onlynumber"><br><input name="monthlyAllocation[' + id + ']" value="'+monthlyAllocation+'" data-inputmask="\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true" placeholder="Monthly Allocation"   class="form-control allocation onlynumber"><br><input name="allocation['+id+']" value="'+allocation+'" data-inputmask="\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true" placeholder="Total Allocation"  class="form-control allocation onlynumber"></td><td><input type ="number" data-inputmask="\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true, \'placeholder\': \'0\'"  max=100 min=0  name="rev_share['+id+']" value="'+rev_share+'" class="form-control onlynumber"></td></tr>');
	}


<?php }?>
FormValidation.init();
$('#builder').queryBuilder({
	sortable: true,
	filters: [{
		id: 'gender',
		label: 'Gender',
		type: 'integer',
		input: 'radio',
		values: {
			M: 'Male',
			F: 'Female'
		},
		operators: ['equal']
		},{
		id: 'states',
		label: 'State',
		type: 'integer',
		input: 'select',
		values: {
			'AL' :'Alabama',
                        'AK': 'Alaska',
                        'AZ': 'Arizona',
                        'AR': 'Arkansas',
                        'CA': 'California',
                        'CO': 'Colorado',
                        'CT': 'Connecticut',
                        'DE': 'Delaware',
                        'DC': 'District Of Columbia',
                        'FL': 'Florida',
                        'GA': 'Georgia',
                        'HI': 'Hawaii',
                        'ID': 'Idaho',
                        'IL': 'Illinois',
                        'IN': 'Indiana',
                        'IA': 'Iowa',
                        'KS': 'Kansas',
                        'KY': 'Kentucky',
                        'LA': 'Louisiana',
                        'ME': 'Maine',
                        'MD': 'Maryland',
                        'MA': 'Massachusetts',
                        'MI': 'Michigan',
                        'MN': 'Minnesota',
                        'MS': 'Mississippi',
                        'MO': 'Missouri',
                        'MT': 'Montana',
                        'NE': 'Nebraska',
                        'NV': 'Nevada',
                        'NH': 'New Hampshire',
                        'NJ': 'New Jersey',
                        'NM': 'New Mexico',
                        'NY': 'New York',
                        'NC': 'North Carolina',
                        'ND': 'North Dakota',
                        'OH': 'Ohio',
                        'OK': 'Oklahoma',
                        'OR': 'Oregon',
                        'PA': 'Pennsylvania',
                        'RI': 'Rhode Island',
                        'SC': 'South Carolina',
                        'SD': 'South Dakota',
                        'TN': 'Tennessee',
                        'TX': 'Texas',
                        'UT': 'Utah',
                        'VT': 'Vermont',
                        'VA': 'Virginia',
                        'WA': 'Washington',
                        'WV': 'West Virginia',
                        'WI': 'Wisconsin',
                        'WY': 'Wyoming'
		},
		operators: ['contains','not_contains']
		},{
		id: 'KMA_EDUC',
		label: 'Education',
		type: 'integer',
		input: 'select',
		values: {
			'A': 'Some High School',
			'B': 'High School Grad',
			'C': 'Some College',
			'D': 'College Graduate'
		},
		operators: ['equal']
		},{
		id: 'KMA_HHINC',
		label: 'Income',
		type: 'integer',
		input: 'select',
		values: {
			'A':'Under 30 k',
			'B':'30k to 50k',
			'C':'50k to 75k',
			'D':'75k to 100k',
			'E':'100k to 150k',
			'F':'150k+'
		},
		operators: ['equal']
		},{
		id: 'KMA_OCCUP',
		label: 'Occupation',
		type: 'integer',
		input: 'select',
		values: {
			'A': 'Prof/Tech',
			'B': 'Admin / Manager',
			'C': 'Blue Collar',
			'D': 'Clerical/Service',
			'E': 'Homemaker',
			'F': 'Retired',
			'G': 'Business Owner',
			'H': 'Sales/Marketing'
		},
		operators: ['equal']
		},{
		id: 'age',
		label: 'Age',
		type: 'integer',
		input: 'text',
		operators: ['less','less_or_equal','greater','greater_or_equal']
		},{
		id: 'KMA_NETWORTH',
		label: 'Home Owner',
		type: 'integer',
		input: 'select',
		values: {
			'O': 'Owner',
			'R': 'Rent',
		},
		operators: ['equal']
		},{
		id: 'browser',
		label: 'Browser',
		type: 'integer',
		input: 'select',
		values: {
			'mobile': 'Mobile',
			'desktop': 'Desktop',
		},
		operators: ['equal']
		},{
		id: 'weekday',
		label: 'Day',
		type: 'integer',
		input: 'select',
		values: {
			1: 'Monday',
			2: 'Tuesday',
			3: 'Wednesday',
			4: 'Thusrday',
			5: 'Friday',
			6: 'Saturday',
			0: 'Sunday',
		},
		operators: ['equal']
		},{
		id: 'email',
		label: 'Email Domain',
		type: 'string',
		input: 'text',
		operators: ['contains','not_contains']
		},{
		id: 'source',
		label: 'Traffic Source',
		type: 'string',
		input: 'text',
		operators: ['contains','not_contains']
		},{
		id: 'hour',
		label: 'Hour',
		type: 'integer',
		input: 'select',
		values: {
			'1-2': '1 AM - 2 AM',
			'2-3': '2 AM - 3 AM',
			'3-4': '3 AM - 4 AM',
			'4-5': '4 AM - 5 AM',
			'5-6': '5 AM - 6 AM',
			'6-7': '6 AM - 7 AM',
			'7-8': '7 AM - 8 AM',
			'8-9': '8 AM - 9 AM',
			'9-10': '9 AM - 10 AM',
			'10-11': '10 AM - 11 AM',
			'11-12': '11 AM - 12 PM',
			'12-13': '12 PM - 1 PM',
			'13-14': '1 PM - 1 PM',
			'14-15': '2 PM - 3 PM',
			'15-16': '3 PM - 4 PM',
			'16-17': '4 PM - 5 PM',
			'17-18': '5 PM - 6 PM',
			'18-19': '6 PM - 7 PM',
			'19-20': '7 PM - 8 PM',
			'20-21': '8 PM - 9 PM',
			'21-22': '9 PM - 10 PM',
			'22-23': '10 PM - 11 PM',
			'23-24': '11 PM - 12 AM',
		},
		operators: ['equal']
		},{
		id: 'interest',
		label: 'Interest',
		type: 'integer',
		input: 'select',
		values: {
			'KMA_HOMEOFFICE': 'Business - Home Office',
			'KMA_SOHO': 'Business - Small Office/Home Office',
			'KMA_CONTED': 'Contiuing Education',
			'KMA_CIGAR_SMOKER': 'Smokers - Cigar',
			'KMA_SMOKER': 'Smokers',
			'KMA_WTLOSS': 'Health Conscious (Diet &amp; Weight Loss)',
			'KMA_HEALTH': 'Health Conscious (Healthy Living)',
			'KMA_EXERCISE': 'Health Conscious (Exercise) ',
			'KMA_INVESTMENTS': 'Investors (Investing &amp; Personal Finance) ',
			'KMA_OPSEEK': 'Business - Opportunity Seekers ',
			'KMA_INVEST_HIGHEND': 'Investors (High Net Worth) ',
			'KMA_INVEST_LOWEND': 'Investors (Active Investors)',
			'KMA_PETS_CATS': 'Pet Lovers - Cats ',
			'KMA_PETS_DOGS': 'Pet Lovers - Dogs ',
			'KMA_PETS': 'Pet Lovers',
			'KMA_SRPRODS': 'Products - Seniors',
			'KMA_TRAVEL': 'Travelers',
			'KMA_TRAVEL_HIGHEND': 'Travelers - Luxury',
			'KMA_TRAVEL_LOWEND': 'Travelers - Deal Hunters ',
			'KMA_WOMFASH': 'Fashion &amp; Apparel (Women\'s) ',
			'KMA_WOMFASH_HIGHEND': 'Fashion &amp; Apparel (Women\'s Brand Names) ',
			'KMA_WOMFASH_LOWEND': 'Fashion &amp; Apparel (Women\'s Deal Hunters)',
			'KMA_MENFASH': 'Fashion &amp; Apparel (Men\'s) ',
			'KMA_TECHNOLOGY': 'Electronics &amp; Technology',
			'KMA_DEALS': 'Deals &amp; Discounts',
			'KMA_FOODWINE': 'Food &amp; Wine',
			'KMA_GAMERS': 'Electronics &amp; Technology (Gamers) ',
			'KMA_DECOR': 'Home D?cor',
			'KMA_HOMEGARD': 'Home &amp; Garden',
			'KMA_KIDSPROD': 'Products - Children',
			'KMA_MOTORCYCLE': 'Auto - Motorcycle Enthusiasts',
			'KMA_CURREVENT': 'Magazines &amp; Books (Current Events) ',
			'KMA_POL_LEFT': 'Political (Left) ',
			'KMA_POL_RIGHT': 'Political (Right) ',
			'KMA_POL_IND': 'Political (Independent)',
			'KMA_SWEEPS': 'Sweepstakes Enthusiasts ',
			'KMA_DONORS': 'Donors',
			'KMA_CRAFTS_COLL': 'Arts, Crafts &amp; Collectibles',
			'KMA_AUTOMOTIVE': 'Auto - Enthusiasts',
			'KMA_OUTDOORS': 'Outdoors',
		},
		operators: ['in']
	}]
});
// set rules
$('.parse-sql').on('click', function () {
	var resJson = $('#builder').queryBuilder('getRules');
	$(".json-parsed").html(JSON.stringify(resJson, null, 2));
});
// reset builder
/* get rules & SQL
	$('.reset').on('click', function () {
	$('#builder').queryBuilder('reset');
	$(".json-parsed").empty();
	$(".sql-parsed").empty();
});	*/
 
<?php 
if(!empty($adunit_select_div)){
	foreach($adunit_select_div as $lead){
	?>
	 $('#changeAdUnitDiv').show();
	adunit_apend_edit(<?php echo $lead['tblMappedAdunit']['ma_ad_id'];?>,'<?php echo $lead['tblp']['CompanyName']."=>".$lead['tblAdunit']['adunit_name'];?>',<?php echo (!empty($lead['tblMappedAdunit']['dailyAllocation']))?$lead['tblMappedAdunit']['dailyAllocation']:0?>,<?php echo (!empty($lead['tblMappedAdunit']['monthlyAllocation']))?$lead['tblMappedAdunit']['monthlyAllocation']:0?>,<?php echo (!empty($lead['tblMappedAdunit']['allocation']))?$lead['tblMappedAdunit']['allocation']:0?>,<?php echo (!empty($lead['tblMappedAdunit']['rev_share']))?$lead['tblMappedAdunit']['rev_share']:0;?>);
	<?php 
	} //end of foreach loop
}
?>


 <?php if($this->params['action'] == "edit_line_item_flight" and !empty($this->request->data['tblFlight']['fl_audience_segment'] and $this->request->data['tblFlight']['fl_audience_segment']!='{}')){?> 
// set rules
 $('#builder').queryBuilder('setRules',  
  <?php echo $this->request->data['tblFlight']['fl_audience_segment']; ?> 
   );
<?php } ?>


