
<div class="portlet light ">
	<div class="portlet-body">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover" id="sample_4">
				<thead>
					<tr>
						<th class="table-checkbox">
							<b>Gross Spend MTD</b>
						</th>
						<th>
							<b>Today's Total Spend</b>
						</th>
						<th>
							<b>Lead-Gen Spend Today</b>	
						</th>
						<th>
							<b>Email Spend Today</b>
						</th>
						<th>
							<b>Display Spend Today</b>
						</th>
						<th>
							<b>Native Spend Today</b>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="odd gradeX">
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$8,351</span>
						</td>
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$1,026</span><i style="font-size:35px;color:red;margin-left:5px;" class="fa fa-arrow-down"></i>
						</td>
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$350</span><i style="font-size:35px;color:green;margin-left:5px;" class="fa fa-arrow-up"></i>
						</td>
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$450</span><i style="font-size:35px;color:green;margin-left:5px;" class="fa fa-arrow-up"></i>
						</td>
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$126</span><i style="font-size:35px;color:red;margin-left:5px;" class="fa fa-arrow-down"></i>
						</td>
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$100</span><i style="font-size:35px;color:green;margin-left:5px;" class="fa fa-arrow-up"></i>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- END PORTLET-->

