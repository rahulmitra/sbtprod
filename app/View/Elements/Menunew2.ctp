<div class="hor-menu ">
	<ul class="nav navbar-nav">
		<li>
			<?php echo $this->Html->link('Dashboard','/');?>
		</li>
		<li class="menu-dropdown classic-menu-dropdown hide">
			<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
				Company <i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu pull-left">
				<li class="">
					 <?php echo $this->Html->link("<i class='icon-plus'></i> Add New Company",array('controller'=>'company','action'=>'add'),array('escape'=>false));?>
				 </li>
				<li class="">
					 <?php echo $this->Html->link("<i class='icon-list'></i>All Companies",array('controller'=>'company','action'=>'index'),array('escape'=>false));?>
				</li>
			</ul>
		</li>
		<?php  $groupId = $this->Authake->getGroupIds(); 
			if(!empty($groupId)){
				if($groupId[0] == 1) { ?>
					<li>
						<?php echo $this->Html->link('ESP Reporting',array('controller' => 'esp', 'action' => 'report'));?>
					</li>
			<?php }
			}
			if($groupId[0] == "5") { ?>
			<li class="menu-dropdown classic-menu-dropdown ">
			<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">Delivery <i class="fa fa-angle-down"></i></a>
				<ul class="dropdown-menu pull-left">
				<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>View Reports  ",array('controller'=>'delivery','action'=>'view_reports' ),array('escape'=>false));?>
					</li>
				</ul>
			</li>
			<li class="menu-dropdown classic-menu-dropdown ">
				<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
					Inventory Management<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-left">
					<li class="">
						 <?php echo $this->Html->link("<i class='icon-plus'></i>
						Add Ad Unit",array('controller'=>'inventory_management','action'=>'addAdUnit'),array('escape'=>false));?>
					</li>
					<li style="display:none;" class="">
						<?php echo $this->Html->link('<i class="icon-plus"></i>
						Add Ad Placement',array('controller'=>'inventory_management','action'=>'addAdPlacement'),array('escape'=>false));?>
					 
					</li>
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i> View Adunits  ",array('controller'=>'inventory_management','action'=>'index' ),array('escape'=>false));?>
					</li>	
                                        <li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i> Adunit Reports  ",array('controller'=>'orders','action'=>'adunit_reports' ),array('escape'=>false));?>
					</li>	
					<li  style="display:none;"  class="">
						<?php echo $this->Html->link('<i class="icon-list"></i>
						View All Placements ',array('controller'=>'inventory_management','action'=>'all_placements'),array('escape'=>false));?>
						 
					</li>
				</ul>
			</li>
			<li class="menu-dropdown classic-menu-dropdown <?php echo $isNL; ?>">
				<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
					Newsletter Product<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-left">
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>
						View Products ",array('controller'=>'newsletter','action'=>'index'),array('escape'=>false));?>
					</li>
				</ul>
			</li>
			<li class="menu-dropdown classic-menu-dropdown <?php echo $isEmail; ?>">
				<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
					Email Management<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-left">
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>
					View Email Lists ",array('controller'=>'messages','action'=>'view_email_lists'),array('escape'=>false));?>
					 
					</li>
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>
					View Messages ",array('controller'=>'messages','action'=>'index','all'),array('escape'=>false));?>
						 
					</li>
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>
					Create Messages ",array('controller'=>'messages','action'=>'create' ),array('escape'=>false));?>
						 
					</li>
				</ul>
			</li>
			<li class="menu-dropdown classic-menu-dropdown hide">
				<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
					Reports<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-left">
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>
						Strategic Report  ",array('controller'=>'report','action'=>'strategic_report' ),array('escape'=>false));?>
					 
					</li>
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>
						Tactical Report  ",array('controller'=>'report','action'=>'tactical_report' ),array('escape'=>false));?>
						 
					</li>
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>
						Operational Report ",array('controller'=>'report','action'=>'operational_report' ),array('escape'=>false));?>
						
					 
					</li>
				</ul>
			</li>
			<li class="menu-dropdown classic-menu-dropdown ">
				<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">Lead Management <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-left">
					<li class="">
						<?php echo $this->Html->link("<i class='icon-plus'></i> Leads Reconciliation ",array('controller'=>'leads','action'=>'reConciliation'),array('escape'=>false));?>
					</li>
					<li class=""> <?php echo $this->Html->link("<i class='icon-list'></i> View Leads ",array('controller'=>'leads','action'=>'index'),array('escape'=>false));?>
					</li>
					<li class=""> <?php echo $this->Html->link("<i class='icon-list'></i> Real Time Stats ",array('controller'=>'leads','action'=>'realtime_stats'),array('escape'=>false));?>
					</li>			 
				</ul>
			</li>						
			<li class="menu-dropdown classic-menu-dropdown ">
				<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;">
					Orders <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-left">
					<li class="">
						<?php echo $this->Html->link("<i class='icon-plus'></i>
						Add New Orders ",array('controller'=>'insertionorder','action'=>'add' ),array('escape'=>false));?>
					</li>
					<li class="">
						<?php echo $this->Html->link("<i class='icon-plus'></i>
						Create Line Items ",array('controller'=>'insertionorder','action'=>'add_line_item' ),array('escape'=>false));?>
					</li>
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>All Orders  ",array('controller'=>'orders','action'=>'index' ),array('escape'=>false));?>
					</li>
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>View Flight Report  ",array('controller'=>'orders','action'=>'flight_reports' ),array('escape'=>false));?>
					</li>					
									
					<li class="">
						<?php echo $this->Html->link("<i class='icon-list'></i>View Line Items  ",array('controller'=>'orders','action'=>'reports' ),array('escape'=>false));?>
					</li>					
				</ul>
			</li>
		<?php } ?>
	</ul>
</div>
					
