<div class="portlet light ">
	<div class="portlet-title">
		<div class="caption caption-md">
			<i class="icon-bar-chart theme-font hide"></i>
			<span class="caption-subject theme-font bold uppercase">Daily Revenue Trends</span>
		</div>
	</div>
	<div class="portlet-body">
		<div id="chart_div_revenue_trend" style="width: 100%; height: 300px;"></div>
	</div>
</div>
<!-- END PORTLET-->
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualization);
	
	function drawVisualization() {
		// Some raw data (not necessarily accurate)
		var data = google.visualization.arrayToDataTable([
		['Date', 'Total', 'Lead Gen', 'Email', 'Display', 'Social'],
		<?php 
		for ($i=9; $i>=0; $i--)
			{?>
		['<?php echo $dailyRevenueTrend[$i]['date']; ?>',  <?php echo $dailyRevenueTrend[$i]['ods_daily_total']; ?>,      <?php echo $dailyRevenueTrend[$i]['ods_daily_leadgen']; ?>,         <?php echo $dailyRevenueTrend[$i]['ods_daily_email']; ?>,  <?php echo $dailyRevenueTrend[$i]['ods_daily_desktop']; ?>, <?php echo $dailyRevenueTrend[$i]['ods_daily_social']; ?>],
			<?php } ?>
		]);
		
		var options = {
			'width':'100%',
			'height':'300',
			chartArea:{left:45,top:20,width:"100%",height:"70%"},
			vAxis: {title: 'List Size'},
			legend: {
				position: 'top'
			},
			pointSize: 8,
			lineWidth:4,
			hAxis: {title: 'Date'},
			seriesType: 'line',
			series: {0: {type: 'bars'}}
		};
		
		var chart = new google.visualization.ComboChart(document.getElementById('chart_div_revenue_trend'));
		chart.draw(data, options);
	}
</script>