
	<script type="text/javascript">
            google.load('visualization', '1', {'packages':['columnchart','piechart']});
            //set callback
            google.setOnLoadCallback (createChart);
            //callback function
            function createChart() {
               //create data table object
                var dataTableMulticolumn = new google.visualization.DataTable();
                //define columns for second example
                dataTableMulticolumn.addColumn('string','Quarters');
                 <?php  
				$contentp_revenuebyisp = $bounceReport_Explode = array(); 
				if(!empty($PerformanceRevenueByISP)){
					 
					foreach($PerformanceRevenueByISP as $data){
					?>
					dataTableMulticolumn.addColumn('number', '<?php echo $data['tblIspList']['isp_name']?>');
					<?php 
						 $bounceReport_Explode['OPENS'][] =$data['tblIspReport']['total_opens'];
						 $bounceReport_Explode['CLICKS'][] =$data['tblIspReport']['total_clicks'];
						 $bounceReport_Explode['BOUNCES'][] =$data['tblIspReport']['total_bounces'];
					}
				}
				if(!empty($bounceReport_Explode)){
						foreach($bounceReport_Explode as $key=>$value){
							if(!empty($value)){
								$contentp_revenuebyisp[] ="['".$key."',".implode(",",$value)."]";
							}
						}
				} 
				if(!empty($contentp_revenuebyisp)){ ?>
						dataTableMulticolumn.addRows([<?php echo implode(",",$contentp_revenuebyisp);?>]);
				<?php }?>
                
                var multiColumnChart = new google.visualization.ColumnChart (document.getElementById('Performance_report'));
                var options = {width: 600, height: 200,   title: 'PERFORMANCE REPORT BY ESP'
					 ,vAxis: {minValue: 0,format: '#,###%'}};
                multiColumnChart.draw(dataTableMulticolumn, options);
        
            }

             

        </script>	
		 
 <div id="Performance_report" style="width: 450px; height: 400px;"></div>
