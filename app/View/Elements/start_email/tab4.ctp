
<table class="table table-striped table-bordered table-hover bg_white" >
			<thead>
				<tr>
					<th class="table-checkbox">
						Total Subscribers
					</th>
					<th class="table-checkbox">
						Average Monthly Subcribers
					</th>
					<th class="table-checkbox">
						Average Daily Subscribers
					</th>
					<th class="table-checkbox">
						Opt-Outs % age
					</th>
					<th class="table-checkbox">
						Dropped / Bounced Subscribers
					</th>
					<th class="table-checkbox">
						Active Engagement
					</th>
				</tr>
				<tr class="odd gradeX">
					<td ><?php echo $AudienceProfile['TotalSubcribers'];?></td>
					<td><?php echo $AudienceProfile['TotalAvgMonth'];?></td>
					<td><?php echo $AudienceProfile['TotalAvgDaily'];?></td>
					<td> <?php echo number_format($AudienceProfile['opt_out_age'],2);?>%</td>
					<td><?php echo number_format($AudienceProfile['dropped_bounced_subscribers'],2);?>%</td>
					<td><?php echo number_format($AudienceProfile['active_engagement'],2);?>%</td>
				</tr>
			</thead>
		</table>	
<div class="row1">
	<div class="col-md-6">
		 <div class="portlet box grey-gallery">
			 <div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Subscribers by Source</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12">
						<div id="SubcribersBySource"> </div>
					</div>
					<!--<div class="col-md-12 col-sm-12">
						<h3> Data Download</h3>
						 <table class="table table-striped table-bordered table-hover bg_white" >
							<tr>
								<th>Non Openers</th>
								<td>11125252</td>
							</tr>
							
							<tr>
								<th>Non Openers</th>
								<td>11125252</td>
							</tr>
							
							<tr>
								<th>Non Openers</th>
								<td>11125252</td>
							</tr>
							
							<tr>
								<th>Non Openers</th>
								<td>11125252</td>
							</tr>
							
						 
						 </table>
					</div> -->
				</div>
			</div>
		 </div>
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Users</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12">
						<div class="font-grey-mint font-sm">
							# of Subscribers
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo number_format($totalSubscribers); ?> <span class="font-lg font-grey-mint"></span>
						</div>
						<div id="tab4regions_div" style="width: 100%;"></div>
					</div>
					<div class="col-md-12 col-sm-12">
						<div class="font-grey-mint font-sm">
							DAILY SUBSCRIPTION TRENDS
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-check  font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Quality</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="font-grey-mint font-sm">
							Unsubscribes
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo number_format($group['tblMailing']['total_unsubscribe']); ?> <span class="font-lg font-grey-mint"></span>
						</div>
						<div class="font-grey-mint font-sm">
							<?php echo number_format((($group['tblMailing']['total_unsubscribe'] / $totalQuality) * 100),2) ?>%
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="font-grey-mint font-sm">
							Hard Bounces
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo number_format($group['tblMailing']['total_bounce']); ?> <span class="font-lg font-grey-mint"></span>
						</div>
						<div class="font-grey-mint font-sm">
							<?php echo number_format((($group['tblMailing']['total_bounce'] / $totalQuality) * 100),2) ?>%
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="font-grey-mint font-sm">
							Complaints
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo number_format($group['tblMailing']['total_spam']); ?> <span class="font-lg font-grey-mint"></span>
						</div>
						<div class="font-grey-mint font-sm">
							<?php echo number_format((($group['tblMailing']['total_spam'] / $totalQuality) * 100),2) ?>%
						</div>
					</div>
				</div>
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="clearfix">
							<div id="legendContainer_email_quality" class="table-responsive"></div> 
						</div>
						<div id="chart_email_quality" class="chart"></div>
					</div>
				</div>
			</div>
		</div>
		
		
	</div>
	<div class="col-md-6">
		
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-check font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Daily Subscriber Trends</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12">
						<div id ="daily_subscribe"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-check font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Revenue By Source</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12">
						<div id="RevenueBySource"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-check font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Opens & Clicks By Source</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12">
						<div id="Open_clickBySource" ></div>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-desktop font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Technology</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="font-grey-mint font-sm">
							Device Type
						</div>
						<div id="piechart_desktop" style="width: 100%;"></div>
					</div>
					<div class="col-md-4 col-sm-4  col-xs-12">
						<div class="font-grey-mint font-sm">
							Browsers
						</div>
						<div id="piechart_windows" style="width: 100%;"></div>
					</div>
					<div class="col-md-4 col-sm-4  col-xs-12">
						<div class="font-grey-mint font-sm">
							OS Family
						</div>
						<div id="piechart_ios" style="width: 100%;"></div>
					</div>
				</div>
			</div>
			
		</div>
		
		
	</div>			
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>

<script>
	
	var ChartsFlotcharts1 = function() {
		
		return {
			//main function to initiate the module
			initCharts: function() {
				
				if (!jQuery.plot) {
					return;
				}
				
				var data = [];
				var totalPoints = 250;
				
				//Interactive Chart
				
				function chart_email_message() {
					if ($('#chart_email_message').size() != 1) {
						return;
					}
					
					function randValue() {
						return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
					}
					var sent = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date']) - 1; ?> ,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_sent']; ?>],
					<?php } ?>
					];
					var delivered = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date'])  - 1; ?>,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_delivered']; ?>],
					<?php } ?>
					];
					var uniqueOpens = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date']) - 1; ?>,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_u_open']; ?>],
					<?php } ?>
					];
					var uniqueClicks = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date']) - 1; ?>,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_u_clicks']; ?>],
					<?php } ?>
					];
					
					var plot = $.plot($("#chart_email_message"), [{
						data: sent,
						label: "Sent",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:0
						}, {
						data: delivered,
						label: "Delivered",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:1
						}, {
						data: uniqueClicks,
						label: "Unique Clicks",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:2
						} , {
						data: uniqueOpens,
						label: "Unique Opens",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:3
						}], {
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
								fillColor: {
									colors: [{
										opacity: 0.05
										}, {
										opacity: 0.01
									}]
								}
							},
							points: {
								show: true,
								radius: 3,
								lineWidth: 1
							},
							shadowSize: 2
						},
						grid: {
							hoverable: true,
							clickable: true,
							tickColor: "#eee",
							borderColor: "#eee",
							borderWidth: 1
						},
						colors: ["#d12610", "#37b7f3", "#52e136"],
						xaxis: {
							mode: "time",
							tickSize: [1, "day"],    
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						yaxis: {
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						legend:{         
							placement: 'outsideGrid',
							container:$("#legendContainer_email_message"),    
							labelFormatter: function(label, series){
								return '<button data-toggle="button" onclick="togglePlot('+series.idx+');" class="btn btn-default ' + ((series.lines.show) ? "active" : "") + '" type="button" aria-pressed="false"> '+label + ((series.lines.show) ? " <span style=\"margin-left: 5px; font-size: 10px;\">&#10006;</span>" : "")+'</button>';
							},
							noColumns: 0
						}
					});
					
					function gd(date_c) {
						var date = Date.parse(date_c);
						return date.toString('dd-MMM-yyyy');
					}
					
					function showTooltip(x, y, contents) {
						$('<div id="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 15,
							border: '1px solid #333',
							padding: '4px',
							color: '#fff',
							'border-radius': '3px',
							'background-color': '#333',
							opacity: 0.80
						}).appendTo("body").fadeIn(200);
					}
					
					var previousPoint = null;
					$("#chart_2").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x);
						$("#y").text(pos.y);
						
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;
								
								$("#tooltip").remove();
								var x = item.datapoint[0],
								y = item.datapoint[1];
								showTooltip(item.pageX, item.pageY, item.series.label + " on " + convertTimestamp(x) + " is " + y);
							}
							} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
					
					togglePlot = function(seriesIdx)
					{
						var someData = plot.getData();  
						someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;  
						if (!someData[seriesIdx].lines.show){
							someData[seriesIdx].tempData = someData[seriesIdx].data;
							someData[seriesIdx].data = [];
						}
						else
						{
							someData[seriesIdx].data = someData[seriesIdx].tempData;
						}
						plot.setData(someData);
						plot.setupGrid();
						plot.draw();
					}
					
					
					function convertTimestamp(timestamp) {
						var nextDay = new Date(timestamp); // 86400000 - one day in ms 
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
						'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						return months[nextDay.getUTCMonth()] + ' ' + nextDay.getUTCDate();
					}
				}
				
				function chart_email_quality() {
					if ($('#chart_email_quality').size() != 1) {
						return;
					}
					
					function randValue() {
						return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
					}
					var unsubscribe = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date']) - 1; ?> ,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_unsubscribe']; ?>],
					<?php } ?>
					];
					var bounce = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date'])  - 1; ?>,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_bounce']; ?>],
					<?php } ?>
					];
					var spam = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date']) - 1; ?>,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_spam']; ?>],
					<?php } ?>
					];
					
					var plot = $.plot($("#chart_email_quality"), [{
						data: unsubscribe,
						label: "Unsubscribe",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:0
						}, {
						data: bounce,
						label: "Bounce",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:1
						} , {
						data: spam,
						label: "Spam",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:3
						}], {
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
								fillColor: {
									colors: [{
										opacity: 0.05
										}, {
										opacity: 0.01
									}]
								}
							},
							points: {
								show: true,
								radius: 3,
								lineWidth: 1
							},
							shadowSize: 2
						},
						grid: {
							hoverable: true,
							clickable: true,
							tickColor: "#eee",
							borderColor: "#eee",
							borderWidth: 1
						},
						colors: ["#d12610", "#37b7f3", "#52e136"],
						xaxis: {
							mode: "time",
							tickSize: [1, "day"],    
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						yaxis: {
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						legend:{         
							placement: 'outsideGrid',
							container:$("#legendContainer_email_quality"),    
							labelFormatter: function(label, series){
								return '<button data-toggle="button" onclick="togglePlot1('+series.idx+');" class="btn btn-default ' + ((series.lines.show) ? "active" : "") + '" type="button" aria-pressed="false"> '+label + ((series.lines.show) ? " <span style=\"margin-left: 5px; font-size: 10px;\">&#10006;</span>" : "")+'</button>';
							},
							noColumns: 0
						}
					});
					
					function gd(date_c) {
						var date = Date.parse(date_c);
						return date.toString('dd-MMM-yyyy');
					}
					
					function showTooltip(x, y, contents) {
						$('<div id="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 15,
							border: '1px solid #333',
							padding: '4px',
							color: '#fff',
							'border-radius': '3px',
							'background-color': '#333',
							opacity: 0.80
						}).appendTo("body").fadeIn(200);
					}
					
					var previousPoint = null;
					$("#chart_2").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x);
						$("#y").text(pos.y);
						
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;
								
								$("#tooltip").remove();
								var x = item.datapoint[0],
								y = item.datapoint[1];
								showTooltip(item.pageX, item.pageY, item.series.label + " on " + convertTimestamp(x) + " is " + y);
							}
							} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
					
					togglePlot1 = function(seriesIdx)
					{
						var someData = plot.getData();  
						someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;  
						if (!someData[seriesIdx].lines.show){
							someData[seriesIdx].tempData = someData[seriesIdx].data;
							someData[seriesIdx].data = [];
						}
						else
						{
							someData[seriesIdx].data = someData[seriesIdx].tempData;
						}
						plot.setData(someData);
						plot.setupGrid();
						plot.draw();
					}
					
					
					function convertTimestamp(timestamp) {
						var nextDay = new Date(timestamp); // 86400000 - one day in ms 
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
						'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						return months[nextDay.getUTCMonth()] + ' ' + nextDay.getUTCDate();
					}
				}
				
				chart_email_quality();
				
			}
		};
		
	}();
	
	
	 
</script>
<!-- CSS -->
<style type="text/css">
	#legendContainer {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
	
	#legendContainer_email_message {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer_email_message .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
	
	#legendContainer_email_quality {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer_email_quality .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
</style>	

<script type="text/javascript">
	google.load("visualization", "1", {packages:["geochart"]});
	google.setOnLoadCallback(drawRegionsMap);
	
	
	function drawRegionsMap() {
		
		var data = google.visualization.arrayToDataTable([
		['Country', 'Popularity'],
		<?php foreach ($country as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
		};
		
		var chart = new google.visualization.GeoChart(document.getElementById('tab4regions_div'));
		
		chart.draw(data, options);
	}
</script>	

<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualizationSBS);
	
	function drawVisualizationSBS() {
		var data = google.visualization.arrayToDataTable([
		['Year', 'Sales' ] <?php  
			$revenueExplode = array();
			$revenueReport = '';
			if(!empty($SubscribersBySource)){
				echo ','; // for first data 
				foreach($SubscribersBySource as $Subscrier){
					$revenueExplode[]= "['".$Subscrier['Dynamic']['nl_source']."',  ". $Subscrier['Dynamic']['totalsource']."]";
				}
			}
			if(!empty($revenueExplode)){
				echo implode(",",$revenueExplode);	
			}
			
		?>
		]);
		
		var options = {'title':'Subcribers by Source',
			'pieSliceText': 'value',
			'width':400,
		'height':350};
		
		var chart = new google.visualization.PieChart(document.getElementById('SubcribersBySource'));
		chart.draw(data, options);
	}
	
</script>


<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualizationRBS);
	
	function drawVisualizationRBS() {
		
        var data4 = google.visualization.arrayToDataTable([
		['Element', 'Density', { role: 'style' }],
		['Source1', 8.94, '#e5e4e2'],            // RGB value
		['Source2', 10.49, 'gold'],            // English color name
		['Source3', 19.30, 'silver'],
		
		['Source4', 21.45, 'color: #b87333' ],
		
		
        ]);
		
		var options4 = {'title':'Revenue By Source','width':500,'height':350};
		
		var chart = new google.visualization.ColumnChart(document.getElementById('RevenueBySource'));
		chart.draw(data4, options4);
	}
	
</script>

<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualizationOCS);
	
	function drawVisualizationOCS() {
		
        var data5 = google.visualization.arrayToDataTable([
		['Element', 'Density', { role: 'style' }],
		['Source1', 8.94, '#b87333'],            // RGB value
		['Source2', 10.49, 'silver'],            // English color name
		['Source3', 19.30, 'gold'],
		
		['Source4', 21.45, 'color: #e5e4e2' ], // CSS-style declaration
		]);
		
		var options5 = {'title':'Opens & Clicks By Source','width':500,'height':350};
		
		var chart = new google.visualization.ColumnChart(document.getElementById('Open_clickBySource'));
		chart.draw(data5, options5);
	}
	
</script>


<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualizationOCS);
	
	function drawVisualizationOCS() {
		
       var data_daily_subscribe = google.visualization.arrayToDataTable([
		['Month', 'Total', 'Source1', 'Source2', 'Source3', 'Source4', 'Average'],
		['1-Sep ',  500,      938,         522,             998,           450,      614.6],
		['2-Sep',  2000,      1120,        599,             1268,          288,      682],
		['3-Sep',  1500,      1167,        587,             807,           397,      623],
		['4-Sep',  1800,      1110,        615,             968,           215,      609.4],
		['5-Sep',  1400,      691,         629,             1026,          366,      569.6],
		['6-Sep ',  500,      938,         522,             998,           450,      614.6]
		
		]); 
	
		
			var options_daily_subscribe = {
			title : 'Daily Subscriber Trends',
			'width':600,
			'height':350,
			seriesType: 'bars',
			series: {5: {type: 'line'}}
		};      
		
		
		var chart = new google.visualization.ComboChart(document.getElementById('daily_subscribe'));
		chart.draw(data_daily_subscribe, options_daily_subscribe);
	}
	
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Device', 'Type'],
		<?php foreach ($DeviceType as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_desktop'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Browser', 'Name'],
		<?php foreach ($browsers as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_windows'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['OS', 'Family'],
		<?php foreach ($osfamily as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_ios'));
		
		chart.draw(data, options);
	}
</script>
