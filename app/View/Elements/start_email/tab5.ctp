 	
<div class="row">
	<div class="col-md-5">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Business</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div> 
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-3 col-sm-3 col-xs-3">
						<div class="font-grey-mint font-sm">
							Line Items
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo number_format($activeLines); ?> <span class="font-lg font-grey-mint"></span>
						</div>
						<div class="font-grey-mint font-sm">
							Active
						</div>
					</div>
					<div class="col-md-5 col-sm-5 col-xs-5">
						<div id="tab5barchart_plain" width="100%" height="150"></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									CPM
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format(!empty($totalCPM)?$totalCPM:0); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									CPC
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalCPC); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									CPL
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalCPL); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									SCR
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalSCR); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row  list-separated">
					
					<div class="col-md-7 col-sm-7 col-xs-7">
						<div class="font-grey-mint font-sm">
							Revenue
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							$<?php echo number_format($totalRevenue,2); ?> <i style="font-size:20px;color:green;margin-left:5px;" class="fa fa-arrow-up"></i><span class="font-lg font-grey-mint">4.5%</span>
						</div>
					</div>
					<div class="col-md-5 col-sm-5 col-xs-5">
						<div class="font-grey-mint font-sm">
							eCPM
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							$<?php echo $group['tblMailing']['total_sent'] != 0 ? number_format((($totalRevenue * 1000) / $group['tblMailing']['total_sent']),2) : "0"; ?> <i style="font-size:20px;color:green;margin-left:5px;" class="fa fa-arrow-up"></i><span class="font-lg font-grey-mint">2.5%</span>
						</div>
					</div>
				</div>
				<div class="row list-separated">
					<div class="col-md-5 col-sm-5 col-xs-12">
						<div id="piechart" style="width: 100%; height: 100%;"></div>
					</div>
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="view_subscribers">
								<thead>
									<tr>
										<th class="table-checkbox">
											Price Format
										</th>
										<th>
											Revenue
										</th>
										<th>
											Share
										</th>
										<th>
											eCPM
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>CPM</td>
										<td>$<?php echo number_format($revenueCPM,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueCPM * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
									<tr>
										<td>CPC</td>
										<td>$<?php echo number_format($revenueCPC,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueCPC * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
									<tr>
										<td>CPL</td>
										<td>$<?php echo number_format($revenueCPL,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueCPL * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
									<tr>
										<td>SCR</td>
										<td>$<?php echo number_format($revenueSCR,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueSCR * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		 
 <!-- END EXAMPLE TABLE PORTLET-->
</div>

<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualization);
	
	function drawVisualization() {
		var data = google.visualization.arrayToDataTable([
		['Type', 'Count'],
		['CPM', <?php echo $totalCPM; ?>],
		['CPC', <?php echo $totalCPC; ?>],
		['CPL', <?php echo $totalCPL; ?>],
		['SCR', <?php echo $totalSCR; ?>]
		]);
		var options = {
			height: 150,
			'chartArea': {'width': '70%', 'height': '80%'},
			bar: {groupWidth: '75%'},
		};
		var chart = new google.visualization.BarChart(document.getElementById('tab5barchart_plain'));
		chart.draw(data, options);
	}
</script>
