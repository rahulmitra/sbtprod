<table class="table table-striped table-bordered table-hover" >
			<thead>
				<tr>
					<th class="table-checkbox">
						Gross Revenue MTD
					</th>
					<th class="table-checkbox">
						Today's Total Earnings
					</th>
					<th class="table-checkbox">
						Live Campaign this month
					</th>
					<th class="table-checkbox">
						MTD Sign-Ups
					</th>
					<th class="table-checkbox">
						eCPM
					</th>
					<th class="table-checkbox">
					</th>
				</tr>
				<tr class="odd gradeX">
					<td >$6,353</td>
					<td>$1,026</td>
					<td>23</td>
					<td> 32,656</td>
					<td>$0.19</td>
					<td></td>
				</tr>
			</thead>
		</table>	
<div class="row">
	<div class="col-md-5">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Business</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-3 col-sm-3 col-xs-3">
						<div class="font-grey-mint font-sm">
							Line Items
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo number_format($activeLines); ?> <span class="font-lg font-grey-mint"></span>
						</div>
						<div class="font-grey-mint font-sm">
							Active
						</div>
					</div>
					<div class="col-md-5 col-sm-5 col-xs-5">
						<div id="barchart_plain" width="100%" height="150"></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									CPM
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalCPM); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									CPC
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalCPC); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									CPL
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalCPL); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6">
								<div class="font-grey-mint font-sm">
									SCR
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($totalSCR); ?> <span class="font-lg font-grey-mint"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row  list-separated">
					
					<div class="col-md-7 col-sm-7 col-xs-7">
						<div class="font-grey-mint font-sm">
							Revenue
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							$<?php echo number_format($totalRevenue,2); ?> <i style="font-size:20px;color:green;margin-left:5px;" class="fa fa-arrow-up"></i><span class="font-lg font-grey-mint">4.5%</span>
						</div>
					</div>
					<div class="col-md-5 col-sm-5 col-xs-5">
						<div class="font-grey-mint font-sm">
							eCPM
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							$<?php echo $sent != 0 ? number_format((($totalRevenue * 1000) / $sent),2) : "0"; ?> <i style="font-size:20px;color:green;margin-left:5px;" class="fa fa-arrow-up"></i><span class="font-lg font-grey-mint">2.5%</span>
						</div>
					</div>
				</div>
				<div class="row list-separated">
					<div class="col-md-5 col-sm-5 col-xs-12">
						<div id="piechart" style="width: 100%; height: 100%;"></div>
					</div>
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="view_subscribers">
								<thead>
									<tr>
										<th class="table-checkbox">
											Price Format
										</th>
										<th>
											Revenue
										</th>
										<th>
											Share
										</th>
										<th>
											eCPM
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>CPM</td>
										<td>$<?php echo number_format($revenueCPM,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueCPM * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
									<tr>
										<td>CPC</td>
										<td>$<?php echo number_format($revenueCPC,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueCPC * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
									<tr>
										<td>CPL</td>
										<td>$<?php echo number_format($revenueCPL,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueCPL * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
									<tr>
										<td>SCR</td>
										<td>$<?php echo number_format($revenueSCR,2); ?></td>
										<td><?php
											if ($totalRevenue == 0) {
												echo "0";
												} else { 
												echo round(($revenueSCR * 100) / $totalRevenue);
											}?>%</td>
											<td>$8</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">TOP PERFORMING LINE-ITEMS</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="uppercase font-hg font-red-flamingo">
							<div id="top_performing_line_items" width="100%" height="250"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php if(count($nlchilds) > 0) { ?>
			<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-user font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">TOP PERFORMING Unit</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="uppercase font-hg font-red-flamingo">
								<div id="top_performing_unit" width="100%" height="250"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		
		
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Users</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12">
						<div class="font-grey-mint font-sm">
							# of Subscribers
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo number_format(count($subscribers)); ?> <span class="font-lg font-grey-mint"></span>
						</div>
						<div id="regions_div" style="width: 100%;"></div>
					</div>
					<div class="col-md-12 col-sm-12">
						<div class="font-grey-mint font-sm">
							DAILY SUBSCRIPTION TRENDS
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-7">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart-o font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Engagement</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div id="g1_sent_delivered" class="gauge"></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div id="g1_opens" class="gauge"></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div id="g1_clicks" class="gauge"></div>
					</div>
				</div>
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="clearfix">
							<div id="legendContainer_email_message" class="table-responsive"></div> 
						</div>
						<div id="chart_email_message" class="chart"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row list-separated">
			<div class="col-md-9">
				<div class="portlet box grey-gallery">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-check  font-grey-silver"></i>
							<span class="caption-subject font-grey-silver bold uppercase">Quality</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row list-separated">
							<div class="col-md-4 col-sm-4 col-xs-4">
								<div class="font-grey-mint font-sm">
									Unsubscribes
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($unsubscribe); ?> <span class="font-lg font-grey-mint"></span>
								</div>
								<div class="font-grey-mint font-sm">
									<?php echo number_format((($unsubscribe / $totalQuality) * 100),2) ?>%
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<div class="font-grey-mint font-sm">
									Hard Bounces
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($bounce); ?> <span class="font-lg font-grey-mint"></span>
								</div>
								<div class="font-grey-mint font-sm">
									<?php echo number_format((($bounce / $totalQuality) * 100),2) ?>%
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<div class="font-grey-mint font-sm">
									Complaints
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo number_format($spam); ?> <span class="font-lg font-grey-mint"></span>
								</div>
								<div class="font-grey-mint font-sm">
									<?php echo number_format((($spam / $totalQuality) * 100),2) ?>%
								</div>
							</div>
						</div>
						<div class="row list-separated">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="clearfix">
									<div id="legendContainer_email_quality" class="table-responsive"></div> 
								</div>
								<div id="chart_email_quality" class="chart"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="portlet box grey-gallery">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-desktop font-grey-silver"></i>
							<span class="caption-subject font-grey-silver bold uppercase">Technology</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row list-separated">
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="font-grey-mint font-sm">
									Device Type
								</div>
								<div id="piechart_desktop" style="width: 100%;"></div>
							</div>
							<div class="col-md-4 col-sm-4  col-xs-12">
								<div class="font-grey-mint font-sm">
									Browsers
								</div>
								<div id="piechart_windows" style="width: 100%;"></div>
							</div>
							<div class="col-md-4 col-sm-4  col-xs-12">
								<div class="font-grey-mint font-sm">
									OS Family
								</div>
								<div id="piechart_ios" style="width: 100%;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END EXAMPLE TABLE PORTLET-->
</div>
