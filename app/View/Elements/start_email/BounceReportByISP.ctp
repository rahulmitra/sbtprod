
   <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
		var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day']<?php  
			$bounceReport_Explode = array(); 
		 
			if(!empty($BounceReportByISP)){
				echo ','; // for first data 
				foreach($BounceReportByISP as $totalbounce){
					$bounceReport_Explode[]= "['".$totalbounce['tblIspList']['isp_name']."',  ". $totalbounce['tblIspReport']['total_bounces']."]";
				}
			}
			if(!empty($bounceReport_Explode)){
				echo implode(",",$bounceReport_Explode);	
			}
			 ?>
        ]);
        var options = {
          title: 'Bounces Report By ISP'
        };

        var chart = new google.visualization.PieChart(document.getElementById('BounceReportByISP'));

        chart.draw(data, options);
      }
    </script>
 <div id="BounceReportByISP" style="width: 500px; height: 350px;"></div>
