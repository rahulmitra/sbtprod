<table class="table table-striped table-bordered table-hover bg_white" >
    <thead>
        <tr>
            <th class="table-checkbox">
                Gross Revenue MTD
            </th>
            <th class="table-checkbox">
                Today's Total Earnings
            </th>
            <th class="table-checkbox">
                Live Campaign this month
            </th>
            <th class="table-checkbox">
                MTD Sign-Ups
            </th>
            <th class="table-checkbox">
                eCPM
            </th>
        </tr>
        <tr class="odd gradeX">
            <td >$<?php echo $RevenueTable['total_rd_revenue']; ?></td>
            <td>$<?php echo $RevenueTable['today_total_earnings']; ?></td>
            <td><?php echo $RevenueTable['TotalLiveCampaignThisMonth']; ?></td>
            <td> <?php echo $RevenueTable['TotalMTD']; ?></td>
            <td >$<?php echo number_format($RevenueTable['TotaleCPM'], 2); ?></td>
        </tr>
    </thead>
</table>	
<div class="row">
    <div class="col-md-6">

        <div class="portlet box grey-gallery">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-grey-silver"></i>
                    <span class="caption-subject font-grey-silver bold uppercase">Revenue by Pricing Format</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row list-separated">
                    <div class="col-md-12 col-sm-12">
                        <div id="Revenue_P_Format" style="width: 550px; height: 400px;"></div>
                        <?php  
                        $total_rd_revenueExplode = array(); 
						$datevalue= '';
						if(!empty($RevenueByPriceFormat)){
							foreach($RevenueByPriceFormat as $priceformat){
								$total_rd_revenueExplode[]= 
								"['".$priceformat[0]."',  
								".$priceformat[1]."]";
							}
						}//Here $datevalue formate ['name',value1,value2,..]
						if(!empty($total_rd_revenueExplode)){
							 $datevalue = implode(",",$total_rd_revenueExplode);	
						}
						$header=array(
						'header'=>array('Task', 'Hours per Day'),
						'options'=>array('title'=>'Revenue by Pricing Format','width'=>'550','height'=>'350'),
						'hAxis'=>array('title'=>'test','titleTextStyle'=>array('color'=>'#333')),'vAxis'=>array('minValue'=> '0'));
						echo $this->Gchart->create('PieChart',$header, 
						$datevalue,'Revenue_P_Format');?>
                    </div>

                </div>
            </div>
        </div>


        <div class="portlet box grey-gallery">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-grey-silver"></i>
                    <span class="caption-subject font-grey-silver bold uppercase">Revenue by Vertical</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row list-separated">
                    <div class="col-md-12 col-sm-12">
                        <div id="Revenue_Vetical" style="width: 550px; height: 400px;">
                        <?php  
                        $ExplodeData = array(); 
						$datevalue= '';
						if(!empty($RevenueByVertical)){
							foreach($RevenueByVertical as $priceformat){
								$ExplodeData[]= "['".$priceformat['tblCategory']['category_name']."',  ".$priceformat['tblReportDay']['total_rd_revenue']."]";
							}
						}//Here $datevalue formate ['name',value1,value2,..]
						if(!empty($ExplodeData)){
							 $datevalue = implode(",",$ExplodeData);	
						}
						$header=array(
							'header'=>array('Task', 'Hours per Day'),
							'options'=>array('title'=>'Revenue by Vertical','width'=>'550','height'=>'350')
						);
						echo $this->Gchart->create('PieChart',$header, 
						$datevalue,'Revenue_Vetical');?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="portlet box grey-gallery">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-grey-silver"></i>
                    <span class="caption-subject font-grey-silver bold uppercase">Revenue by ISP</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row list-separated">
                    <div class="col-md-12 col-sm-12">
                        <div id="Revenue_ISp" style="width: 550px; height: 400px;">
							<?php  
							$ExplodeData = array(); 
							//$datevalue= "['Auto Warranty', 5],['Health Insurance', 4],['Auto Insurance',8],['Life Insurance',2],['Daiting & Personals ',    10],['Home Improvement',7]";
							if(!empty($RevenueByISP)){
								foreach($RevenueByISP as $data){
									$ExplodeData[]= "['".$data['tblIspList']['isp_name']."',  ".$data['tblReportDay']['total_rd_revenue']."]";
								}
							}
							//Here $datevalue formate ['name',value1,value2,..]
							if(!empty($ExplodeData)){
								 $datevalue = implode(",",$ExplodeData);	
							}
							$header=array(
							'header'=>array('Task', 'Hours per Day'),
							'options'=>array('title'=>'Revenue by Vertical','width'=>'550','height'=>'350'),
							'hAxis'=>array(),
							'vAxis'=>array(),
							'curveType'=>array(),
							'legend'=>array());
							echo $this->Gchart->create('PieChart',$header,$datevalue,'Revenue_ISp');?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="portlet box grey-gallery">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-grey-silver"></i>
                    <span class="caption-subject font-grey-silver bold uppercase">Opens & Click % age Trends by ISP</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row list-separated">
                    <div class="col-md-12 col-sm-12">
                        <div id="Revenue_open_click" style="width: 550px; height: 400px;">
                        <?php  
							$datevalue='';
							$ExplodeData = array(); 
							if(!empty($OpenClickTrendByIsp)){
								foreach($OpenClickTrendByIsp as $data){
									$ExplodeData[]= "['".$data['tblIspReport']['dateformate']."',  ".$data['tblIspReport']['OpenPercentage'].",  ".$data['tblIspReport']['ClickPercentage']."]";
								}
							}
							 //Here $datevalue formate ['name',value1,value2,..]
							if(!empty($ExplodeData)){
								 $datevalue = implode(",",$ExplodeData);	
							}
							$header=array(
							'header'=>array('Month', 'Opens', 'Click'),
							'options'=>array('title'=>'Opens & Click % age Trends by ISP','width'=>'550','height'=>'350'),
							'curveType'=>array('curveType'=>'function'));
							echo $this->Gchart->create('LineChart',$header, 
							$datevalue,'Revenue_open_click');?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>	  
    <div class="col-md-6">
        <div class="portlet box grey-gallery">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-grey-silver"></i>
                    <span class="caption-subject font-grey-silver bold uppercase">Revenue Trends</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row list-separated">
                    <div class="col-md-12 col-sm-12">
                        <div id="chart_div_Revenue" style="width: 550px; height: 400px;"></div>
                       <?php  
                        $ExplodeData = array(); 
						$datevalue= '';
						if(!empty($revenueTrends)){
							foreach($revenueTrends as $data){
								$ExplodeData[]= "['".$data[0]."',  ". $data[1]."]";
							}
						}
						//Here $datevalue formate ['name',value1,value2,..]
						if(!empty($ExplodeData)){
							 $datevalue = implode(",",$ExplodeData);	
						}
						$header=array(
						'header'=>array('Year', 'Sales' ),
						'options'=>array('title'=>'Revenue Trends'),
						'hAxis'=>array('title'=>'Overall Revenue Trends','titleTextStyle'=>array('color'=>'#333')),
						'vAxis'=>array('minValue'=> '0'));
						echo $this->Gchart->create('AreaChart',$header, 
						$datevalue,'chart_div_Revenue');?>
                        
                    </div>

                </div>
            </div>
        </div>

        <div class="portlet box grey-gallery">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-grey-silver"></i>
                    <span class="caption-subject font-grey-silver bold uppercase">eCPM Trends</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row list-separated">
                    <div class="col-md-12 col-sm-12">
                        <div id="chart_div_eCpm" style="width: 550px; height: 400px;">
                        <?php  
                        $ExplodeData = array(); 
						$datevalue= '';
						if(!empty($eCPMTrade)){
							foreach($eCPMTrade as $eCPMT){
								$ExplodeData[]= "['".$eCPMT['tblReportDay']['dateformate']."',  ". $eCPMT['tblReportDay']['total_rd_revenue']."]";
							}
						}
						//Here $datevalue formate ['name',value1,value2,..]
						if(!empty($ExplodeData)){
							 $datevalue = implode(",",$ExplodeData);	
						}
						$header=array(
						'header'=>array('Year', 'Sales' ),
						'options'=>array('title'=>'eCPM Trends'),
						'hAxis'=>array('title'=>'ECPM Trends- Network Wide','titleTextStyle'=>array('color'=>'#333')),
						'vAxis'=>array('minValue'=> '0'));
						echo $this->Gchart->create('AreaChart',$header, 
						$datevalue,'chart_div_eCpm');?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!--        <div class="portlet box grey-gallery">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-user font-grey-silver"></i>
                            <span class="caption-subject font-grey-silver bold uppercase">eCPC Trends</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row list-separated">
                            <div class="col-md-12 col-sm-12">
                                <div id="chart_diveCPC" style="width: 550px; height: 400px;"></div>
                            </div>
        
                        </div>
                    </div>
                </div>-->

        <div class="portlet box grey-gallery">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-grey-silver"></i>
                    <span class="caption-subject font-grey-silver bold uppercase">eCPM by ISP
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row list-separated">
                    <div class="col-md-12 col-sm-12">
                        <div id="chart_div_ecmp_isp" style="width: 550px; height: 400px;">
                        
                        <?php  
                        $ExplodeData = array(); 
						$datevalue = "['May 03', 1, 4],['May 04', 2, 5],['May 05', 13, 15],['May 06', 3, 20]";
						/*if(!empty($eCPMTrade)){
							foreach($eCPMTrade as $eCPMT){
								$ExplodeData[]= "['".$eCPMT['tblReportDay']['dateformate']."',  ". $eCPMT['tblReportDay']['total_rd_revenue']."]";
							}
						}
						//Here $datevalue formate ['name',value1,value2,..]
						if(!empty($ExplodeData)){
							 $datevalue = implode(",",$ExplodeData);	
						}*/
						$header=array(
						'header'=>array('Year', 'Sales', 'Expenses' ),
						'options'=>array('title'=>'eCPM by ISP'),
						'hAxis'=>array('title'=>'','titleTextStyle'=>array('color'=>'#333')),
						'vAxis'=>array('minValue'=> '0'));
						echo $this->Gchart->create('AreaChart',$header, 
						$datevalue,'chart_div_ecmp_isp');?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="portlet box grey-gallery">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user font-grey-silver"></i>
                    <span class="caption-subject font-grey-silver bold uppercase">Earning by ISP</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row list-separated">
                    <div class="col-md-12 col-sm-12">
                        <div id="chart_div_earning_isp" style="width: 550px; height: 400px;">
                        <?php  
                        $ExplodeData = array(); 
                        $datevalue='';
						if(!empty($EaringISP)){
							foreach($EaringISP as $data){
								$ExplodeData[]= "['".$data['tblReportDay']['dateformate']."',  ". $data['tblReportDay']['total_revenue']."]";
							}
						}
						//Here $datevalue formate ['name',value1,value2,..]
						if(!empty($ExplodeData)){
							 $datevalue = implode(",",$ExplodeData);	
						}
						$header=array(
						'header'=>array('Year', 'Sales'),
						'options'=>array('title'=>'Earning by ISP'),
						'hAxis'=>array('title'=>'','titleTextStyle'=>array('color'=>'#333')),
						'vAxis'=>array('minValue'=> '0'));
						echo $this->Gchart->create('AreaChart',$header, 
						$datevalue,'chart_div_earning_isp');?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
