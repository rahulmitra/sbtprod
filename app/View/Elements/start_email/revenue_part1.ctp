
   <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
		google.setOnLoadCallback(drawChart);
      function drawChart() {
		 
        
        
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Cost Per Impression',     5],
          ['Cost Per Click',      4],
          ['Cost Per Load ',  8],
          ['Single Click Registration', 2],
          ['Cost Per Send',    10],
          ['Cost Per Acquisition',    7]
        ]);
        
        var data2 = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Auto Warranty',     5],
          ['Health Insurance',      4],
          ['Life Insurance ',  8],
          ['Dating & Personals', 2],
          ['Home Improvement',    7]
           
        ]);
         var data3 = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Auto Warranty',     5],
          ['Health Insurance',      4],
          ['Auto Insurance',  8],
          ['Life Insurance', 2],
          ['Daiting & Personals ',    10],
          ['Home Improvement',    7]
        ]);
          var data4 = google.visualization.arrayToDataTable([
          ['Month', 'Opens', 'Click'],
          ['Jan',  1000,      400],
          ['Feb',  1170,      460],
          ['Mar',  660,       1030],
          ['Apr',  1030,      540],
          ['May',  460,      660],
          ['Jun',  400,      1170]
        ]);

            
	  // Set chart options
            var options = {'title':'Revenue by Pricing Format',
                           'width':550,
                           'height':350};
            // Set chart options
            var options2 = {'title':'Revenue by Vertical',
                           'width':550,
                           'height':350};
            // Set chart options
            var options3 = {'title':'Revenue by ISP',
                           'width':550,
                           'height':350};	
                           
             var options4 = {
				  title: 'Opens & Click % age Trends by ISP','width':550,
                           'height':350,
				  curveType: 'function',
				  legend: { position: 'bottom' }
				};               
       var chart = new google.visualization.PieChart(document.getElementById('Revenue_P_Format'));
            chart.draw(data, options);
		var chart2 = new google.visualization.PieChart(document.getElementById('Revenue_Vetical'));
		chart2.draw(data2, options2);
		var chart3 = new google.visualization.PieChart(document.getElementById('Revenue_ISp'));
		chart3.draw(data3, options3);
		var chart4 = new google.visualization.LineChart(document.getElementById('Revenue_open_click'));
		chart4.draw(data4, options4);
      }
    </script>
    
    
 <div id="Revenue_P_Format"></div>
 <div id="Revenue_Vetical"> </div>
 <div id="Revenue_ISp"> </div>
 <div id="Revenue_open_click"> </div>
