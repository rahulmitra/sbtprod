 <table class="table table-striped table-bordered table-hover bg_white" >
			<thead>
				<tr>
					<th class="table-checkbox">
						Gross Revenue MTD
					</th>
					<th class="table-checkbox">
						Today's Total Earnings
					</th>
					<th class="table-checkbox">
						Live Campaign this month
					</th>
					<th class="table-checkbox">
						MTD Sign-Ups
					</th>
					<th class="table-checkbox">
						eCPM
					</th>
					<th class="table-checkbox">
					</th>
				</tr>
				<tr class="odd gradeX">
					<td >$<?php echo $RevenueTable['total_rd_revenue']; ?></td>
					<td>$<?php echo  $RevenueTable['today_total_earnings']; ?></td>
					<td><?php echo  $RevenueTable['TotalLiveCampaignThisMonth']; ?></td>
					<td> <?php echo  $RevenueTable['TotalMTD']; ?></td>
					<td >$<?php echo  number_format($RevenueTable['TotaleCPM'],2); ?></td>
					<td></td>
				</tr>
			</thead>
		</table>	
<div class="row">
	<div class="col-md-6">
		 <div class="portlet box grey-gallery">
			 <div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Revenue Trends</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12">
						<div id="chart_div_Revenue" style="width: 550px; height: 400px;"></div>
					</div>
					 
				</div>
			</div>
		 </div>
		 <div class="portlet box grey-gallery">
			 <div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">eCPM Trends</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12">
						<div id="chart_div_eCpm" style="width: 550px; height: 400px;"></div>
					</div>
					 
				</div>
			</div>
		 </div>
		 <div class="portlet box grey-gallery">
			 <div class="portlet-title">
				<div class="caption">
					<i class="fa fa-user font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Daily New Subscribers</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12">
						<div id="chart_div_sub" style="width: 550px; height: 400px;"></div>
					</div>
					 
				</div>
			</div>
		 </div>
		
		 
	 </div>
	<div class="col-md-6">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart-o font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Engagement</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div id="g1_sent_delivered" class="gauge"></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div id="g1_opens" class="gauge"></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<div id="g1_clicks" class="gauge"></div>
					</div>
				</div>
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="clearfix">
							<div id="legendContainer_email_message" class="table-responsive"></div> 
						</div>
						<div id="chart_email_message" class="chart"></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-bar-chart-o font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">PERFORMANCE REPORT BY ISP</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div id="Performance_report" style="width: 450px; height: 400px;">
					<?php  
							$header=array(
							'header'=>array('string','Quarters'),
							'options'=>array('title'=>'PERFORMANCE REPORT BY ISP','width'=>'600','height'=>'200'),
							'hAxis'=>array(),
							'vAxis'=>array('minValue'=>'0','format'=>'#,###%'),
							'curveType'=>array(),
							'legend'=>array());
							echo $this->Gchart->Multichart('PieChart',$header,$PerformanceRevenueByISP,'Performance_report');?>
					</div>
				</div>
			</div>
		</div> 
	 </div>
	<!-- END EXAMPLE TABLE PORTLET-->
</div>
<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualizationD);
	
	function drawVisualizationD() {
		var data = google.visualization.arrayToDataTable([
		['Year', 'New Subscribers' ]<?php  
			$dialy_sub_Explode = array(); 
			$revenueReport = '';
			if(!empty($Daily_New_Subscribers)){
				echo ','; // for first data 
				foreach($Daily_New_Subscribers as $subscriber){
					$dialy_sub_Explode[]= "['".$subscriber['dateformate']."',  ". $subscriber['totalemail']."]";
				}
			}
			if(!empty($dialy_sub_Explode)){
				echo implode(",",$dialy_sub_Explode);	
			}
		?>
		]);
		
		var sub_options = {
          title: 'Daily New Subscribers',
          hAxis: {title: 'Month',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };
		
		var chart = new google.visualization.AreaChart(document.getElementById('chart_div_sub'));
		chart.draw(data, sub_options);
	}
</script>
<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualizationR);
	
	function drawVisualizationR() {
		var Revenuedata = google.visualization.arrayToDataTable([
          ['Year', 'Sales' ]<?php  
			$revenueExplode = array();
			$revenueReport = '';
			if(!empty($revenueTrends)){
				echo ','; // for first data 
				foreach($revenueTrends as $revenue){
					$revenueExplode[]= "['".$revenue['tblReportDay']['dateformate']."',  ". $revenue['tblReportDay']['total_rd_revenue']."]";
				}
			}
			if(!empty($revenueExplode)){
				echo implode(",",$revenueExplode);	
				}
					 
			?>
        ]);
		
		var Revenueoptions = {
          title: 'Revenue Trends',
          hAxis: {title: 'Overall revenue Trends',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };
		
		var chart = new google.visualization.AreaChart(document.getElementById('chart_div_Revenue'));
		chart.draw(Revenuedata, Revenueoptions);
	}
</script>
<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualizationE);
	
	function drawVisualizationE() {
		var eCpmtdata = google.visualization.arrayToDataTable([
          ['Year', 'Sales' ]<?php  
			$revenuecpmExplode = array();
			$revenueReport = '';
			if(!empty($eCPMTrade)){
				echo ','; // for first data 
				foreach($eCPMTrade as $eCPMT){
					$revenuecpmExplode[]= "['".$eCPMT['tblReportDay']['dateformate']."',  ". $eCPMT['tblReportDay']['total_rd_revenue']."]";
				}
			}
			if(!empty($revenuecpmExplode)){
				echo implode(",",$revenuecpmExplode);	
			}
			 ?>
        ]);
		
		 var eCpmoptions = {
          title: 'eCPM Trends',
          hAxis: {title: 'ECPM Trends- Network Wide',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0,format: '$#,###'}
        };
		
		var chart = new google.visualization.AreaChart(document.getElementById('chart_div_eCpm'));
		chart.draw(eCpmtdata, eCpmoptions);
	}
</script>
<script>
    var g1;
    var g2;
    var g3;
    document.addEventListener("DOMContentLoaded", function(event) {
        g1 = new JustGage({
            id: "g1_sent_delivered",
            value: <?php echo $group['tblMailing']['total_delivered']; ?>,
            min: 0,
            max: <?php echo $group['tblMailing']['total_sent']; ?>,
            gaugeWidthScale: 0.8,
			title: "Delivered",
            counter: true,
			formatNumber: true,
            hideInnerShadow: true,
			levelColors: [
			"#00fff6",
			"#ff00fc",
			"#FC3B00"
			]
		});
		g2 = new JustGage({
            id: "g1_opens",
            value: <?php echo $group['tblMailing']['total_opens']; ?>,
            min: 0,
            max: <?php echo $group['tblMailing']['totalopens']; ?>,
            gaugeWidthScale: 0.6,
			title: "Opens",
            counter: true,
			formatNumber: true,
            hideInnerShadow: true,
    		pointer: true,
			pointerOptions: {
				toplength: -15,
				bottomlength: 10,
				bottomwidth: 12,
				color: '#8e8e93',
				stroke: '#ffffff',
				stroke_width: 3,
				stroke_linecap: 'round'
			},
			levelColors: [
			"#00fff6",
			"#FA9C00",
			"#FA9C00"
			]
		}); 
		g3 = new JustGage({
            id: "g1_clicks",
            value: <?php echo $group['tblMailing']['clicks']; ?>,
            min: 0,
            max: <?php echo $group['tblMailing']['clicks']; ?>,
            gaugeWidthScale: 0.6,
			title: "Clicks",
            counter: true,
			formatNumber: true,
            hideInnerShadow: true,
    		pointer: true,
			pointerOptions: {
				toplength: -15,
				bottomlength: 10,
				bottomwidth: 12,
				color: '#8e8e93',
				stroke: '#ffffff',
				stroke_width: 3,
				stroke_linecap: 'round'
			},
			levelColors: [
			"#00fff6",
			"#F8C700",
			"#F8C700"
			]
		});
	});
    </script>				
	<script>
	
	var ChartsFlotcharts1 = function() {
		
		return {
			//main function to initiate the module
			initCharts: function() {
				
				if (!jQuery.plot) {
					return;
				}
				
				var data = [];
				var totalPoints = 250;
				
				//Interactive Chart
				
				function chart_email_message() {
					if ($('#chart_email_message').size() != 1) {
						return;
					}
					
					function randValue() {
						return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
					}
					var sent = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date']) - 1; ?> ,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_sent']; ?>],
					<?php } ?>
					];
					var delivered = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date'])  - 1; ?>,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_delivered']; ?>],
					<?php } ?>
					];
					var uniqueOpens = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date']) - 1; ?>,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_u_open']; ?>],
					<?php } ?>
					];
					var uniqueClicks = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date']) - 1; ?>,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_u_clicks']; ?>],
					<?php } ?>
					];
					
					var plot = $.plot($("#chart_email_message"), [{
						data: sent,
						label: "Sent",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:0
						}, {
						data: delivered,
						label: "Delivered",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:1
						}, {
						data: uniqueClicks,
						label: "Unique Clicks",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:2
						} , {
						data: uniqueOpens,
						label: "Unique Opens",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:3
						}], {
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
								fillColor: {
									colors: [{
										opacity: 0.05
										}, {
										opacity: 0.01
									}]
								}
							},
							points: {
								show: true,
								radius: 3,
								lineWidth: 1
							},
							shadowSize: 2
						},
						grid: {
							hoverable: true,
							clickable: true,
							tickColor: "#eee",
							borderColor: "#eee",
							borderWidth: 1
						},
						colors: ["#d12610", "#37b7f3", "#52e136"],
						xaxis: {
							mode: "time",
							tickSize: [1, "day"],    
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						yaxis: {
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						legend:{         
							placement: 'outsideGrid',
							container:$("#legendContainer_email_message"),    
							labelFormatter: function(label, series){
								return '<button data-toggle="button" onclick="togglePlot('+series.idx+');" class="btn btn-default ' + ((series.lines.show) ? "active" : "") + '" type="button" aria-pressed="false"> '+label + ((series.lines.show) ? " <span style=\"margin-left: 5px; font-size: 10px;\">&#10006;</span>" : "")+'</button>';
							},
							noColumns: 0
						}
					});
					
					function gd(date_c) {
						var date = Date.parse(date_c);
						return date.toString('dd-MMM-yyyy');
					}
					
					function showTooltip(x, y, contents) {
						$('<div id="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 15,
							border: '1px solid #333',
							padding: '4px',
							color: '#fff',
							'border-radius': '3px',
							'background-color': '#333',
							opacity: 0.80
						}).appendTo("body").fadeIn(200);
					}
					
					var previousPoint = null;
					$("#chart_2").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x);
						$("#y").text(pos.y);
						
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;
								
								$("#tooltip").remove();
								var x = item.datapoint[0],
								y = item.datapoint[1];
								showTooltip(item.pageX, item.pageY, item.series.label + " on " + convertTimestamp(x) + " is " + y);
							}
							} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
					
					togglePlot = function(seriesIdx)
					{
						var someData = plot.getData();  
						someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;  
						if (!someData[seriesIdx].lines.show){
							someData[seriesIdx].tempData = someData[seriesIdx].data;
							someData[seriesIdx].data = [];
						}
						else
						{
							someData[seriesIdx].data = someData[seriesIdx].tempData;
						}
						plot.setData(someData);
						plot.setupGrid();
						plot.draw();
					}
					
					
					function convertTimestamp(timestamp) {
						var nextDay = new Date(timestamp); // 86400000 - one day in ms 
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
						'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						return months[nextDay.getUTCMonth()] + ' ' + nextDay.getUTCDate();
					}
				}
				
				function chart_email_quality() {
					if ($('#chart_email_quality').size() != 1) {
						return;
					}
					
					function randValue() {
						return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
					}
					var unsubscribe = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date']) - 1; ?> ,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_unsubscribe']; ?>],
					<?php } ?>
					];
					var bounce = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date'])  - 1; ?>,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_bounce']; ?>],
					<?php } ?>
					];
					var spam = [
					<?php if(!empty($group['tblMailing']['m_sent_date'])) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$group['tblMailing']['m_sent_date']); ?>,<?php echo $this->Time->format('m',$group['tblMailing']['m_sent_date']) - 1; ?>,<?php echo $this->Time->format('d',$group['tblMailing']['m_sent_date']); ?>)).getTime(), <?php echo $group['tblMailing']['m_spam']; ?>],
					<?php } ?>
					];
					
					var plot = $.plot($("#chart_email_quality"), [{
						data: unsubscribe,
						label: "Unsubscribe",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:0
						}, {
						data: bounce,
						label: "Bounce",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:1
						} , {
						data: spam,
						label: "Spam",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:3
						}], {
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
								fillColor: {
									colors: [{
										opacity: 0.05
										}, {
										opacity: 0.01
									}]
								}
							},
							points: {
								show: true,
								radius: 3,
								lineWidth: 1
							},
							shadowSize: 2
						},
						grid: {
							hoverable: true,
							clickable: true,
							tickColor: "#eee",
							borderColor: "#eee",
							borderWidth: 1
						},
						colors: ["#d12610", "#37b7f3", "#52e136"],
						xaxis: {
							mode: "time",
							tickSize: [1, "day"],    
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						yaxis: {
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						legend:{         
							placement: 'outsideGrid',
							container:$("#legendContainer_email_quality"),    
							labelFormatter: function(label, series){
								return '<button data-toggle="button" onclick="togglePlot1('+series.idx+');" class="btn btn-default ' + ((series.lines.show) ? "active" : "") + '" type="button" aria-pressed="false"> '+label + ((series.lines.show) ? " <span style=\"margin-left: 5px; font-size: 10px;\">&#10006;</span>" : "")+'</button>';
							},
							noColumns: 0
						}
					});
					
					function gd(date_c) {
						var date = Date.parse(date_c);
						return date.toString('dd-MMM-yyyy');
					}
					
					function showTooltip(x, y, contents) {
						$('<div id="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 15,
							border: '1px solid #333',
							padding: '4px',
							color: '#fff',
							'border-radius': '3px',
							'background-color': '#333',
							opacity: 0.80
						}).appendTo("body").fadeIn(200);
					}
					
					var previousPoint = null;
					$("#chart_2").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x);
						$("#y").text(pos.y);
						
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;
								
								$("#tooltip").remove();
								var x = item.datapoint[0],
								y = item.datapoint[1];
								showTooltip(item.pageX, item.pageY, item.series.label + " on " + convertTimestamp(x) + " is " + y);
							}
							} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
					
					togglePlot1 = function(seriesIdx)
					{
						var someData = plot.getData();  
						someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;  
						if (!someData[seriesIdx].lines.show){
							someData[seriesIdx].tempData = someData[seriesIdx].data;
							someData[seriesIdx].data = [];
						}
						else
						{
							someData[seriesIdx].data = someData[seriesIdx].tempData;
						}
						plot.setData(someData);
						plot.setupGrid();
						plot.draw();
					}
					
					
					function convertTimestamp(timestamp) {
						var nextDay = new Date(timestamp); // 86400000 - one day in ms 
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
						'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						return months[nextDay.getUTCMonth()] + ' ' + nextDay.getUTCDate();
					}
				}
				
				
				
				//graph
				chart_email_message();
				chart_email_quality();
				
			}
		};
		
	}();
</script>
<!-- CSS -->
<style type="text/css">
	#legendContainer {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
	
	#legendContainer_email_message {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer_email_message .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
	
	#legendContainer_email_quality {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer_email_quality .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
</style>	