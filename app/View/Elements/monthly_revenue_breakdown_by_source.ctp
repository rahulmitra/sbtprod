<div class="portlet light ">
	<div class="portlet-title">
		<div class="caption caption-md">
			<i class="icon-bar-chart theme-font hide"></i>
			<span class="caption-subject theme-font bold uppercase">Monthly Revenue Breakdown by Source</span>
		</div>
		<div class="actions">
			<span class="caption-subject theme-font bold uppercase">$<?php echo $totalMonthlySourceRevenue = $monthlydesktop + $monthlyLeadGen +  $monthlyEmail + $monthlySocial + $monthlyMobile; ?></span>
		</div>
	</div>
	<div class="portlet-body">
		<?php if($totalMonthlySourceRevenue == 0) { ?>
			No Revenue from any Source this Month
		<?php } ?>
		<div id="piechart_3d" style="width: 100%; height: 300px;"></div>
	</div>
</div>
<!-- END PORTLET-->
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
			var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Desktop',     <?php echo $monthlydesktop; ?>],
          ['Email',      <?php echo $monthlyEmail; ?>],
          ['Lead Gen',  <?php echo $monthlyLeadGen; ?>],
          ['Social', <?php echo $monthlySocial; ?>],
          ['Mobile',    <?php echo $monthlyMobile; ?>]
        ]);
			
			var formatter = new google.visualization.NumberFormat({
				prefix: '$'
			});
			formatter.format(data, 1);
			
			var options = {
				'width':'100%',
				'height':'300',
				chartArea:{left:45,top:20,width:"100%",height:"90%"},
				pieHole: 0.4,
				pieSliceText: 'value'
			};
			
			var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
			chart.draw(data, options);
	}
</script>