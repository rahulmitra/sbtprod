<div class="portlet light ">
	<div class="portlet-title">
		<div class="caption caption-md">
			<i class="icon-bar-chart theme-font hide"></i>
			<span class="caption-subject theme-font bold uppercase">Email List Revenue MTD</span>
		</div>
		<div class="actions">
		<span class="caption-subject theme-font bold uppercase">$<?php echo $totalEmailMonth = $monthlyDedicatedEmail +  $monthlyAutoResponder + $monthlyNewsletters; ?></span>
		</div>
	</div>
	<div class="portlet-body">
		<?php if($totalEmailMonth == 0) { ?>
			No Revenue from Email this Month
		<?php } ?>
		<div id="donutchart" style="width: 100%; height: 300px;"></div>
	</div>
</div>
<!-- END PORTLET-->
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
        var data = google.visualization.arrayToDataTable([
		['Task', 'Hours per Day'],
		['Dedicated Email',     <?php echo $monthlyDedicatedEmail; ?>],
		['Auto Responder',      <?php echo $monthlyAutoResponder; ?>],
		['Newsletters',  <?php echo $monthlyNewsletters; ?>]
        ]);
		
		var formatter = new google.visualization.NumberFormat({
			prefix: '$'
		});
		formatter.format(data, 1);
		
        var options = {
			'width':'100%',
			'height':'300',
			chartArea:{left:45,top:20,width:"100%",height:"80%"},
			pieHole: 0.4,
			pieSliceText: 'value'
		};
		
        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
	}
</script>
