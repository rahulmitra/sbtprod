<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<script src="/plugins/jquery.min.js" type="text/javascript"></script>
<?php

/** conditional CSS files / page basis  */
$action = $this->name . '_' . $this->action;

switch ($action) {
    case 'Company_index':
        echo $this->Html->css(array(''));
        break;
    default:

        break;
}
?>

		<?php if($this->params['action'] == "add_line_item" || $this->params['action'] == "edit_line_item") {
				echo $this->Html->script('/plugins/ckeditor/vendor/ckeditor/ckeditor.js');
			}?>
<!-- END GLOBAL MANDATORY STYLES -->
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->Html->css('components-rounded');
			echo $this->Html->css('plugins');
			echo $this->Html->css('layout');
			echo $this->Html->css('default');
			echo $this->Html->css('custom');
			
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
		<?php if(($this->params['controller'] == "company" && $this->params['action'] == "index") || ($this->params['controller'] == "settings" && $this->params['action'] == "index")  || ($this->params['controller'] == "publishers" && $this->params['action'] == "index") || ($this->params['controller'] == "publishers" && $this->params['action'] == "index") || ($this->params['controller'] == "newsletter" && $this->params['action'] == "view_subscribers") || ($this->params['controller'] == "orders" && ($this->params['action'] == "index" ||$this->params['action'] == "orders" )))
			{ ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
		<?php } ?>

		<?php if(($this->params['controller'] == "inventory_management" && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit"))|| ($this->params['controller'] == "esp" && $this->params['action'] == "report")) { ?>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="/plugins/jquery-multi-select/css/multi-select.css"/>
		<?php } ?>

		<?php if($this->params['controller'] == "settings" && $this->params['action'] == "index") { ?>
<link href="/css/pricing-table.css" rel="stylesheet" type="text/css"/>
		<?php } ?>

		<?php if(($this->params['controller'] == "company" && ($this->params['action'] == "add" || $this->params['action'] == "edit" )) || ($this->params['controller'] == "publishers" && $this->params['action'] == "add"))
			{ ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css" href="/plugins/icheck/skins/all.css"/>
<link rel="stylesheet" type="text/css" href="/plugins/select2/select2country.css"/>
<link rel="stylesheet" type="text/css" href="/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="/plugins/typeahead/typeahead.css"/>
<!-- END PAGE LEVEL STYLES -->
		<?php } ?>
		<?php if(in_array($this->params['controller'],array("insertionorder","leads")) && (
			in_array($this->params['action'],array("add_line_item","edit_line_item" ,"add" ,"add_line_item_flight" ,"edit_line_item_flight","reConciliation","upload_creative_text_html","edit_upload_creative_text_html","index" ))));
			{ ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css" href="/plugins/icheck/skins/all.css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
			<?php 
				$datepickercss= "/plugins/bootstrap-datepicker/css/datepicker.css";
				//if($this->params['controller'] =="leads") $datepickercss= "/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css";?>

<link rel="stylesheet" type="text/css" href="<?php echo $datepickercss;?>"/>
<!-- END PAGE LEVEL STYLES -->
		<?php } ?>

		<?php if($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item_flight" || $this->params['action'] == "edit_line_item_flight" )) { ?>

<link rel="stylesheet" type="text/css" href="/plugins/querybuilder/query-builder.default.min.css"/>
		<?php } ?>


		<?php if($this->params['controller'] == "newsletter" && $this->params['action'] == "upload_subscribers") { ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet"/>
<link href="/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet"/>
<link href="/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
<!-- END PAGE LEVEL STYLES -->	
		<?php } ?>

		<?php if(in_array($this->params['controller'],array("insertionorder","leads","newsletter")) && in_array($this->params['action'],array("upload_creative","upload_subscribers","reConciliation","upload_creative_text_html","edit_upload_creative_text_html"))) { ?>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet"/>
<link href="/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet"/>
<link href="/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
<!-- END PAGE LEVEL STYLES -->	
		<?php } ?>

		<?php if($this->params['controller'] == "wall" && $this->params['action'] == "index") { ?>
<link rel="stylesheet" type="text/css" href="/plugins/jquery-multi-select/css/multi-select.css"/>
<link href="/plugins/dropzone/css/dropzone.css" rel="stylesheet"/>
<link href="/css/timeline.css" rel="stylesheet" type="text/css"/>
<link href="/css/timeline2.css" rel="stylesheet" type="text/css"/>
<link href="/css/fb-buttons.css" rel="stylesheet" type="text/css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
		<?php } ?>

		<?php if(($this->params['controller'] == "messages" ||$this->params['controller'] == "leads" || $this->params['controller'] == "insertionorder" || $this->params['controller'] == "orders") && ($this->params['action'] == "create" || $this->params['action'] == "edit_upload_creative_text_html"||$this->params['action'] == "upload_creative_text_html"|| $this->params['action'] == "view_line_creatives"|| $this->params['action'] == "reConciliation")) { ?>
<link rel="stylesheet" type="text/css" href="/plugins/jquery-multi-select/css/multi-select.css"/>
<link href="/plugins/dropzone/css/dropzone.css" rel="stylesheet"/>
<link href="/css/timeline.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link href="/css/timeline2.css" rel="stylesheet" type="text/css"/>
<link href="/css/fb-buttons.css" rel="stylesheet" type="text/css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
		<?php } ?>

		<?php if(($this->params['controller'] == "inventory_management" && $this->params['action'] == "add_coreg_placement") || ($this->params['controller'] == "inventory_management" && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit"))) { ?>
<link rel="stylesheet" type="text/css" href="/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
		<?php } ?>