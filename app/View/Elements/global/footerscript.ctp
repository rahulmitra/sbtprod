<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/plugins/respond.min.js"></script>
<script src="/plugins/excanvas.min.js"></script> 
<![endif]-->	 
<?php
$css = $js = array();
array_push($js, '/plugins/jquery-migrate.min.js', '/plugins/jquery-ui/jquery-ui.min.js', '/plugins/bootstrap/js/bootstrap.min.js', '/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js', '/plugins/jquery-slimscroll/jquery.slimscroll.min.js', '/plugins/jquery.blockui.min.js', '/plugins/jquery.cokie.min.js', '/plugins/uniform/jquery.uniform.min.js');
echo $this->Html->script($js);
$js = array();
// clear js 
echo $this->System->load_css('foot');
echo $this->System->load_js('foot');
echo $this->System->load_scripts_block('foot');
echo $this->System->load_css_block('foot');
?>
<?php
if (($this->params['controller'] == "company" && $this->params['action'] == "index") || ($this->params['controller'] == "settings" && $this->params['action'] == "index") || ($this->params['controller'] == "publishers" && $this->params['action'] == "index") || ($this->params['controller'] == "insertionorder" && $this->params['action'] == "index") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "index") || ($this->params['controller'] == "newsletter" && $this->params['action'] == "view_subscribers")) {
    array_push($js, '/plugins/select2/select2country.min.js', '/plugins/datatables/media/js/jquery.dataTables.min.js', '/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js', '/scripts/table-managed.js');
}
?>
<?php
if (($this->params['controller'] == "delivery" && $this->params['action'] == "index") || ($this->params['controller'] == "messages" && $this->params['action'] == "view_email_lists")) {
    array_push($js, '/plugins/select2/select2country.min.js', '/plugins/datatables/media/js/jquery.dataTables.min.js', '/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js', '/scripts/table-managed.js');
}
?>
<?php
if (($this->params['controller'] == "company" && ($this->params['action'] == "add" || $this->params['action'] == "edit" )) || ($this->params['controller'] == "publishers" && $this->params['action'] == "add")) {
    array_push($js, '/plugins/jquery-validation/js/jquery.validate.min.js', '/plugins/jquery-validation/js/additional-methods.min.js', '/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js', '/plugins/typeahead/typeahead.bundle.js', '/plugins/select2/select2country.min.js', '/scripts/form-wizard.js');
}
?>
<?php
if ($this->params['controller'] == "inventory_management" && $this->params['action'] == "add_coreg_placement") {
    array_push($js, '/plugins/jquery-validation/js/jquery.validate.min.js', '/plugins/jquery-validation/js/additional-methods.min.js', '/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js', '/plugins/typeahead/typeahead.bundle.js', '/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', '/plugins/select2/select2country.min.js', '/scripts/form_wizard_coreg.js');
}
?>
<?php
if (($this->params['controller'] == "newsletter" && $this->params['action'] == "view_stats") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_email") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_stats") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_lead_gen_stats") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_cpm_stats") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_leadgen") || ($this->params['controller'] == "messages" && $this->params['action'] == "index")) {
    array_push($js, '/plugins/flot/jquery.flot.min.js', '/plugins/flot/jquery.flot.resize.min.js', '/plugins/flot/jquery.flot.time.min.js', '/plugins/flot/jquery.flot.stack.min.js', '/plugins/flot/jquery.flot.crosshair.min.js', '/plugins/flot/jquery.flot.categories.min.js');
}
?>
<?php
if (($this->params['controller'] == "newsletter" && $this->params['action'] == "upload_subscribers") || ($this->params['controller'] == "insertionorder" && $this->params['action'] == "upload_creative")) {
    array_push($js, '/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js', '/plugins/jquery-file-upload/js/vendor/tmpl.min.js', '/plugins/jquery-file-upload/js/vendor/load-image.min.js', '/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js', '/plugins/jquery-file-upload/js/jquery.iframe-transport.js', '/plugins/jquery-file-upload/js/jquery.fileupload.js', '/plugins/jquery-file-upload/js/jquery.fileupload-process.js', '/plugins/jquery-file-upload/js/jquery.fileupload-image.js', '/plugins/jquery-file-upload/js/jquery.fileupload-validate.js', '/plugins/jquery-file-upload/js/jquery.fileupload-ui.js', '/scripts/form-fileupload.js');
}
?>
<?php
if (($this->params['controller'] == "inventory_management" || $this->params['controller'] == "esp") && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit" || $this->params['action'] == "report")) {
    array_push($js, '/plugins/jquery.slug.js', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js', '/plugins/jquery-multi-select/js/jquery.multi-select.js', '/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js');
    echo $this->Html->script($js);
    $js = array();
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#title").slug();
        });
        $(window).load(function () {
            $('#ad_isExternalNo').click();
        });
    </script>	 
<?php } ?>
<?php
if ((in_array($this->params['controller'], array("insertionorder", "leads")) && in_array($this->params['action'], array("add_line_item", "edit_line_item", "add", "add_line_item_flight", "edit_line_item_flight", "reConciliation", "index")))) {
    array_push($js, '/plugins/jquery-validation/js/jquery.validate.min.js', '/plugins/jquery-validation/js/additional-methods.min.js', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js', '/plugins/jquery-multi-select/js/jquery.multi-select.js');
    ?>
    <?php
    if (!in_array($this->params['controller'], array("leads", "orders"))) {
        array_push($js, '/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
    } else {
        array_push($js, '/plugins/bootstrap-daterangepicker/moment.min.js');
    }
    array_push($js, '/plugins/icheck/icheck.min.js', '/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', '/plugins/quicksearch-master/jquery.quicksearch.js', '/scripts/form-validation.js');
}
?>
<?php
if ($this->params['controller'] == "wall" && $this->params['action'] == "index") {
    array_push($js, '/plugins/wall/wall.js', '/plugins/dropzone/dropzone.js', '/plugins/justgage/justgage.js', '/plugins/justgage/raphael-2.1.4.min.js', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js', '/plugins/jquery-multi-select/js/jquery.multi-select.js', '/scripts/form-dropzone.js');
}
?>
<?php
if (($this->params['controller'] == "messages" || $this->params['controller'] == "insertionorder" || $this->params['controller'] == "orders") && ($this->params['action'] == "create" || $this->params['action'] == "upload_creative_text_html" || $this->params['action'] == "edit_upload_creative_text_html" || $this->params['action'] == "view_line_creatives")) {
    array_push($js, '/plugins/wall/wall.js', '/plugins/dropzone/dropzone.js', '/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js', '/plugins/jquery-multi-select/js/jquery.multi-select.js', '/scripts/form-dropzone-message.js');
}
?>		
<?php
if (($this->params['controller'] == "orders" && $this->params['action'] == "index") || ($this->params['controller'] == "leads" && $this->params['action'] == "index")) {
    array_push($js, '/plugins/datatables/media/js/jquery.dataTables.min.js', '/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js', '/scripts/table-managed.js', '/plugins/justgage/justgage.js', '/plugins/justgage/raphael-2.1.4.min.js');
}
?>
<?php
if (($this->params['controller'] == "messages" && $this->params['action'] == "index") || ($this->params['controller'] == "newsletter" && $this->params['action'] == "view_stats") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_email") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_stats") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_cpm_stats") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_lead_gen_stats") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_leadgen")) {
    array_push($js, '/plugins/justgage/justgage.js', '/plugins/justgage/raphael-2.1.4.min.js');
}
?>
<?php
if (($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_user_profile") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_user_profile")) {
    array_push($js, '/plugins/amcharts/amcharts/amcharts.js', '/plugins/amcharts/amcharts/serial.js', '/plugins/amcharts/amcharts/pie.js', '/plugins/amcharts/amcharts/radar.js', '/plugins/amcharts/amcharts/themes/light.js', '/plugins/amcharts/amcharts/themes/patterns.js', '/plugins/amcharts/amcharts/themes/chalk.js', '/plugins/amcharts/ammap/ammap.js', '/plugins/amcharts/ammap/maps/js/worldLow.js', '/plugins/amcharts/amstockcharts/amstock.js', '/scripts/charts-user.js');
}
if ($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item_flight" || $this->params['action'] == "edit_line_item_flight")) {
    array_push($js, '/plugins/querybuilder/query-builder.js');
}
array_push($js, '/scripts/metronic.js', '/scripts/layout.js', '/scripts/demo.js');
echo $this->Html->script($js);
$js = array();
?>		
<script>
<?php if ($this->params['controller'] == "insertionorder" && ($this->params['action'] == "upload_creative_text_html" || $this->params['action'] == "edit_upload_creative_text_html")) { ?>
        jQuery(document).ready(function () {
            checkMoreInfo('<?php echo $creatives_type_id; ?>');
        });
<?php } ?>
    jQuery(document).ready(function () {

<?php
if (in_array($this->params['controller'], array("insertionorder")) && in_array($this->params['action'], array("edit_upload_creative_text_html", "upload_creative_text_html"))) {

    $links = (!empty($creatives['tblCreative']['link'])) ? json_decode($creatives['tblCreative']['link'], true) : array();
    ?>
            var next = '<?php echo count($links) ?>';<?php
    if (!empty($links)) {

        foreach ($links as $key => $link) {
            if ($key == 0) {
                continue;
            }
            ?>
                    next++;
                    $(".addmorelink").append('<div class=""><input name="data[tblCreative][link][' + next + ']" class="form-control" style="float:left;width:83%;margin-right:10px;margin-bottom:10px;" value="<?php echo $link ?>" placeholder="Enter the link" type="text" id="tblCreativeLink"></div><a href="javascript:void(0);"  class="remCF"><button  class="btn"   type="button" style="margin-bottom:10px;">-</button></a></div>');
                    $(".remCF").on('click', function () {

                        $(this).prev().remove();
                        $(this).remove();
                    });
            <?php
        }
    }
    ?>
            $(".add-field-more").click(function () {
                next++;
                $(".addmorelink").append('<div class=""><input style="float:left;width:83%;margin-right:10px;margin-bottom:10px;" name="data[tblCreative][link][' + next + ']" class="form-control" placeholder="Enter the link" type="text" id="tblCreativeLink"></div> <a href="javascript:void(0);" class="remCF"><button  style="margin-bottom:10px;" class="btn" type="button">-</button></a></div> ');
                $(".remCF").on('click', function () {
                    $(this).prev().remove();
                    $(this).remove();

                });
            });

<?php } ?>



<?php $addfields = configure::read('addfields');
?>
        var next = '<?php echo count($addfields) ?>';
        // edit addunit page 
<?php
if (!empty($tblAdunitsFieldMapping)) {
    $counter = count($addfields);
    $DateFormats = configure::read('DateFormat');
    foreach ($tblAdunitsFieldMapping as $value) {
        if ($value['FieldMapping']['custom'] == 1) {
            $counter++;
            ?>
                    next++;
            <?php
            $selectType = (!empty($value['FieldMapping']['field_type'])) ? $value['FieldMapping']['field_type'] : '';
            $selectbox = preg_replace("/\r|\n/", "", $this->Form->input('FieldMapping.field_type.' . $counter, array(
                        'label' => false,
                        'type' => 'select',
                        'class' => 'js-creative-field-type-array form-control',
                        'onchange' => 'ShowFieldType(this.value,' . $counter . ')',
                        'empty' => 'Please Select Field type',
                        'selected' => $selectType,
                        'options' => configure::read('CollectFieldType'))));
            $fld_custom_texts = '';
            $ShowDatetype = '';
            if (in_array($selectType, array(2, 3, 5, 4))) {
                $fld_custom_texts = preg_replace("/\r|\n/", "", $this->Form->input('FieldMapping.field_custom_text.' . $counter, array('label' => false, 'value' => (!empty($value)) ? $value['FieldMapping']['field_custom_text'] : '', 'div' => false, 'class' => 'form-control demo auto-w')));
            }
            if (in_array($selectType, array(6))) {
                $fld_custom_texts = preg_replace("/\r|\n/", "", $this->Form->input('FieldMapping.field_custom_text.' . $counter, array('label' => false,
                            'onchange' => 'ShowDatePlaceholder(this.value,' . $counter . ')',
                            'type' => 'select',
                            'selected' => (!empty($value)) ? $value['FieldMapping']['field_custom_text'] : '',
                            'options' => (!empty($DateFormats)) ? $DateFormats : array(), 'div' => false, 'class' => 'form-control')));

                $ShowDatetype = preg_replace("/\r|\n/", "", $this->Form->input('FieldMapping.field_format.' . $counter, array('label' => false, 'value' => (!empty($value)) ? $value['FieldMapping']['field_format'] : '', 'div' => false, 'class' => 'form-control')));
            }
            ?>
                    $("#addmapfield").append('<tr> </td> <td> <div ><input type="checkbox" name="data[FieldMapping][status][' + next + ']" checked value="1" id="check' + next + '"> <label for="check' + next + '">Yes</label></div></td><td><input type="hidden" name="data[FieldMapping][custom][' + next + ']" value="1" ><input name="data[FieldMapping][field_description][' + next + ']" class="form-control demo auto-w" type="text" value="<?php echo $value['FieldMapping']['field_description']; ?>"</td><td><div id="ShowDatetype' + next + '"><?php echo $ShowDatetype ?></div></td>	<td><?php echo $selectbox; ?></td><td id="FieldMappingFieldLable' + next + '"><?php echo $fld_custom_texts; ?></td><td>  <input name="data[FieldMapping][field_lable][' + next + ']" value="<?php echo $value['FieldMapping']['field_lable']; ?>" class="form-control demo auto-w" type="text" id="FieldMappingFieldLable' + next + '">&nbsp; <a href="javascript:void(0);" class="remCF"><button class="btn" type="button">-</button></a> </td></tr>');
                    $(".remCF").on('click', function () {
                        $(this).parent().parent().remove();
                    });
            <?php
        }
    }
}
?>

        $(".add-more").click(function () {
            next++;
<?php
$options = '';
$values = configure::read('CollectFieldType');
$options = '<option value="">Please Select Field type</option>';
if (!empty($values)) {
    foreach ($values as $key => $value) {
        $options = $options . '<option value="' . $key . '">' . $value . '</option>';
    }
}
?>
            $("#addmapfield").append('<tr><td> <div class="ajaxcheckbox"><input type="checkbox" name="data[FieldMapping][status][' + next + ']" checked value="1" id="check' + next + '"> <label for="check' + next + '">Yes</label></div></td><td><input type="hidden" name="data[FieldMapping][custom][' + next + ']" value="1" ><input name="data[FieldMapping][field_description][' + next + ']" class="form-control demo auto-w" type="text"> </td> <td><div id="ShowDatetype' + next + '"><input type ="hidden"  name="data[FieldMapping][field_format][' + next + ']" class="form-control demo auto-w" type="text" value=""></div></td><td><select onchange="ShowFieldType(this.value,' + next + ')" class="js-creative-field-type-array form-control" name="data[FieldMapping][field_type][' + next + ']" id="FieldMappingFieldLable"><?php echo $options; ?></select></td> <td id="showFieldTypeValue' + next + '"></td><td><input name="data[FieldMapping][field_lable][' + next + ']" class="form-control demo auto-w" type="text" id="FieldMappingFieldLable' + next + '">&nbsp; <a href="javascript:void(0);" class="remCF"><button  class="btn" type="button">-</button></a> </td></tr>');
            $(".remCF").on('click', function () {
                $(this).parent().parent().remove();
            });
        });

<?php if ($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item" || $this->params['action'] == "edit_line_item")) { ?>

            calculate();

            var index = '<?php echo count(configure::read('CollectedCollect')); ?>';
    <?php
    if (!empty($lineItemFields)) {
        $DateFormats = configure::read('DateFormat');
        $counter = count(configure::read('CollectedCollect'));
        foreach ($lineItemFields as $value) {
            if ($value['LineItemField']['custom'] == 1) {
                $counter++;
                ?>
                        index++;
                <?php
                $selectType = (!empty($value['LineItemField']['fld_type'])) ? $value['LineItemField']['fld_type'] : '';
                $selectbox = preg_replace("/\r|\n/", "", $this->Form->input('LineItemField.fld_type.' . $counter, array(
                            'label' => false,
                            'type' => 'select',
                            'class' => 'js-creative-field-type-array form-control',
                            'onchange' => 'ShowFieldType(this.value,' . $counter . ')',
                            'empty' => 'Please Select Field type',
                            'selected' => $selectType,
                            'options' => configure::read('CollectFieldType'))));
                $fld_custom_texts = '';
                $ShowDatetype = '';
                if (in_array($selectType, array(2, 3, 5, 4))) {
                    $fld_custom_texts = preg_replace("/\r|\n/", "", $this->Form->input('LineItemField.fld_custom_texts.' . $counter, array('label' => false, 'value' => (!empty($value)) ? $value['LineItemField']['fld_custom_texts'] : '', 'div' => false, 'class' => 'form-control demo auto-w')));
                }

                if (in_array($selectType, array(6))) {
                    $fld_custom_texts = preg_replace("/\r|\n/", "", $this->Form->input('LineItemField.fld_custom_texts.' . $counter, array('label' => false,
                                'onchange' => 'ShowDatePlaceholder(this.value,' . $counter . ')',
                                'selected' => (!empty($value)) ? $value['LineItemField']['fld_custom_texts'] : '',
                                'type' => 'select', 'options' => (!empty($DateFormats)) ? $DateFormats : array(), 'div' => false, 'class' => 'form-control')));

                    $ShowDatetype = preg_replace("/\r|\n/", "", $this->Form->input('LineItemField.field_format.' . $counter, array('label' => false, 'value' => (!empty($value)) ? $value['LineItemField']['field_format'] : '', 'div' => false, 'class' => 'form-control')));
                }
                ?>
                        $("#addmapfield tr:last").before('<tr></td> <td><div ><input type="checkbox" name="data[LineItemField][status][' + index + ']" checked value="1" id="check' + index + '"> <label for="check' + index + '">Yes</label></div></td><td><input type="hidden" name="data[LineItemField][custom][' + index + ']" value="1" ><input name="data[LineItemField][fld_label][' + index + ']" class="form-control demo auto-w" type="text" value="<?php echo $value['LineItemField']['fld_label']; ?>"> <td><?php echo $selectbox; ?><div id="ShowDatetype' + index + '"><?php echo $ShowDatetype; ?></div></td><td id="showFieldTypeValue' + index + '"><?php echo $fld_custom_texts; ?> </td><td>  <input name="data[LineItemField][adv_fld_name][' + index + ']" value="<?php echo $value['LineItemField']['adv_fld_name']; ?>" class="form-control demo auto-w" type="text" id="LineItemFieldLable"></td><td><div ><input type="checkbox" name="data[LineItemField][required][' + index + ']"  <?php if(!empty($value['LineItemField']['required'])){ echo "checked"; } ?> value="1" id="check' + index + '"> &nbsp; <a href="javascript:void(0);" class="remCF"><button class="btn" type="button">-</button></a> </div></td></tr>');
                        $(".remCF").on('click', function () {
                            $(this).parent().parent().parent().remove();
                        });
                <?php
            }
        }
    }
    ?>


            $(".add-field-more").click(function () {
                index++;
    <?php
    $values = configure::read('CollectFieldType');
    $options = '<option value="">Please Select Field type</option>';
    if (!empty($values)) {
        foreach ($values as $key => $value) {
            $options = $options . '<option value="' . $key . '">' . $value . '</option>';
        }
    }
    ?>
                $(this).parents('tr:last').before('<tr><td> <div class="ajaxcheckbox"><input type="checkbox" name="data[LineItemField][status][' + index + ']" checked value="1" id="check' + index + '"> <label for="check' + index + '">Yes</label></div></td><td><input type="hidden" name="data[LineItemField][custom][' + index + ']" value="1" ><input name="data[LineItemField][fld_label][' + index + ']" class="form-control demo auto-w" type="text"> </td> <td><select onchange="ShowFieldType(this.value,' + index + ')" class="js-creative-field-type-array form-control" name="data[LineItemField][fld_type][' + index + ']" id="LineItemFieldFldType"><?php echo $options; ?></select><div id="ShowDatetype' + index + '"></div></td><td id="showFieldTypeValue' + index + '"></td><td><input name="data[LineItemField][adv_fld_name][' + index + ']" class="form-control demo auto-w" type="text" id="LineItemFieldAdvFldName' + index + '"></td><td><div class="ajaxcheckbox"><input type="checkbox" name="data[LineItemField][required][' + index + ']"  value="1" id="required' + index + '"> <label for="required' + index + '">&nbsp;</label> &nbsp; <a href="javascript:void(0);" class="remCF"><button class="btn" type="button">-</button></a> </div></td></tr>');
                $(".remCF").on('click', function () {
                    $(this).parent().parent().parent().remove();
                });
            });
<?php } ?>
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout 
        Demo.init(); // init demo features
<?php if (($this->params['controller'] == "company" && $this->params['action'] == "index") || ($this->params['controller'] == "settings" && $this->params['action'] == "index") || ($this->params['controller'] == "publishers" && $this->params['action'] == "index") || ($this->params['controller'] == "insertionorder" && $this->params['action'] == "index") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "index") || ($this->params['controller'] == "newsletter" && $this->params['action'] == "view_subscribers") || ($this->params['controller'] == "orders" && $this->params['action'] == "index") || ($this->params['controller'] == "leads" && $this->params['action'] == "index") || ($this->params['controller'] == "delivery" && $this->params['action'] == "index") || ($this->params['controller'] == "messages" && $this->params['action'] == "view_email_lists"))  {
    ?>
            TableManaged.init();
<?php } ?>
<?php if (($this->params['controller'] == "company" && ($this->params['action'] == "add" || $this->params['action'] == "edit" )) || ($this->params['controller'] == "publishers" && $this->params['action'] == "add")) {
    ?>
            FormWizard.init();
            var myTypeahead = $('input.typeahead-devs').typeahead({
                name: 'data[tblProfile][CompanyName]',
                remote: '/company/getCompanyNameByQuery?q=%QUERY'
            });
<?php } ?>
<?php if (($this->params['controller'] == "newsletter" && $this->params['action'] == "view_stats") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_email") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_stats") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_cpm_stats") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_leadgen") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_lead_gen_stats")) { ?>
            //ChartsFlotcharts.initCharts();
            ChartsFlotcharts1.initCharts();
<?php } ?>
<?php if ($this->params['controller'] == "messages" && $this->params['action'] == "index") { ?>
            ChartsFlotcharts.initCharts();
<?php } ?>
<?php if ($this->params['controller'] == "newsletter" && $this->params['action'] == "upload_subscribers") { ?>
            FormFileUpload.init();
<?php } ?>
<?php if ($this->params['controller'] == "insertionorder" && $this->params['action'] == "upload_creative") { ?>
            FormFileUploadCreative.init();
<?php } ?>
<?php if ($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add")) { ?>

            // start of  add compnay pop.
            $('.AddCompanyiframe').click(function () {
                $.ajax({
                    type: "POST",
                    url: "/insertionorder/get_company_ajax",
                    dataType: "text",
                    success: function (data) {
                        $(".select2me").empty();
                        $(".select2me").append(data);

                    }
                });
            });
            // end of add compnay pop					
            $(".js-example-basic-single").select2({
                placeholder: "Start typing to select Advertiser Name ",
                allowClear: true
            });
            $(".js-example-basic-single").on("change", function (e) {
                //alert($(this).val());
                $.ajax({
                    type: "POST",
                    url: "/insertionorder/getContactInfoCompany",
                    data: 'company_id=' + $(this).val(),
                    dataType: "json",
                    success: function (data) {
                        var name = data.name.replace(/\s/g, "\t");
                        $('input[name="data[User][ContactName]"]').val(name);
                        $('input[name="data[User][email]"]').val(data.email);
                    }
                });
            });

    <?php if (!empty($company_id)) { ?>
                $('.js-example-basic-single').trigger("change");
    <?php } ?>

            FormValidation.init();
<?php } ?>
<?php if ($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item" || $this->params['action'] == "edit_line_item")) { ?>

            $("#costStructure").select2({
                maximumSelectionLength: 1,
                placeholder: "Start Typing to Select ONLY ONE "
            });
            $("#costStructure").on("change", function (e) {
                calculate();
            });
            $("#renFrequency").select2({
                maximumSelectionLength: 1,
                placeholder: "Select One "
            });
            $(".js-example-data-array").select2({
                placeholder: "Select (or start typing) the order name for the above selected advertiser"
            });
            $(".js-example-data-array-categories").select2({
                placeholder: "Select (or start typing) the category for the line item"
            });
            $("#txtRate").inputmask();
            $("#txtQuantity").inputmask();
            $("#calquantity").inputmask();
            $("#caltxtCalculate").inputmask();
            $("#txtCalculate").inputmask();
            $("#txtDaily").inputmask();
            $("#txtweekly").inputmask();
            $("#txtMonthly").inputmask();
            $('.event_period').datepicker({
                inputs: $('.actual_range'),
                autoclose: true,
                todayHighlight: true,
                startDate: '+0d'


            });
            $(".input-group-btn").on("click", function () {
                $(this).prev("input").focus();
            });
            /*$(".event_period_button").click(function() {
             $('.event_period').datepicker("show");
             });*/

            $('#adunit-select').multiSelect({
                selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search publisher or ad unit'>",
                selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search publisher or ad unit'>",
                afterInit: function (ms) {
                    var that = this,
                            $selectableSearch = that.$selectableUl.prev(),
                            $selectionSearch = that.$selectionUl.prev(),
                            selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.$selectableUl.focus();
                                    return false;
                                }
                            });

                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.$selectionUl.focus();
                                    return false;
                                }
                            });
                },
                afterSelect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                },
                afterDeselect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                }
            });
            // 04-05-16
            $('#adunit-Creative-type').multiSelect({
                selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search creative'>",
                selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search creative'>",
                afterInit: function (ms) {
                    var that = this,
                            $selectableSearch = that.$selectableUl.prev(),
                            $selectionSearch = that.$selectionUl.prev(),
                            selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.$selectableUl.focus();
                                    return false;
                                }
                            });

                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.$selectionUl.focus();
                                    return false;
                                }
                            });
                },
                afterSelect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                },
                afterDeselect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                }
            });
            //04-05-16	
            $('#geography-select').multiSelect({
                selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search location'>",
                selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search location'>",
                afterInit: function (ms) {
                    var that = this,
                            $selectableSearch = that.$selectableUl.prev(),
                            $selectionSearch = that.$selectionUl.prev(),
                            selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.$selectableUl.focus();
                                    return false;
                                }
                            });

                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.$selectionUl.focus();
                                    return false;
                                }
                            });
                },
                afterSelect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                },
                afterDeselect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                }
            });
            $('#audience-select').multiSelect({
                selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search location'>",
                selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search location'>",
                afterInit: function (ms) {
                    var that = this,
                            $selectableSearch = that.$selectableUl.prev(),
                            $selectionSearch = that.$selectionUl.prev(),
                            selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.$selectableUl.focus();
                                    return false;
                                }
                            });

                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.$selectionUl.focus();
                                    return false;
                                }
                            });
                },
                afterSelect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                },
                afterDeselect: function () {
                    this.qs1.cache();
                    this.qs2.cache();
                }
            });

            $('#audience-select').change(function () {
                var count = 0;
                var to = 0;
                $('#audience-select > option:selected').each(function () {
                    var text = $(this).text();
                    var splitted = text.split(" (");
                    //var splitted = text.substring(text.lastIndexOf("("));
                    var value = splitted[1].replace(")", "");
                    count = +count + +value;
                    to++;
                });
                var result = (count * 40) / 100;
                if (to == 1)
                    $('#total').html("(Available Emails : " + (count).toFixed() + ")");
                else
                    $('#total').html("(Available Emails : " + (count - result).toFixed() + ")");
            });

            $('input[name="productTypes[]"]').on('ifChanged', function (event) {
                var favorite = [];
                var selectedItem = [];
                $.each($("input[name='productTypes[]']:checked"), function () {
                    favorite.push($(this).val());
                });
                $('#adunit-select :selected').each(function (i, selected) {
                    selectedItem[i] = $(selected).val();
                });
                //alert("My favourite sports are: " + selectedItem.join(", "));
                $("#adunit-select").empty();
                $('#adunit-select').multiSelect('refresh');
                $.ajax({
                    type: "POST",
                    url: "/insertionorder/getAdUnitsByMediaType",
                    data: 'media_id=' + favorite.join(","),
                    dataType: "json",
                    success: function (data) {
                        var count = 0;
                        $.each(data, function (index) {
                            $('#adunit-select').multiSelect('addOption', {value: data[index].id, text: data[index].text, index: count});
                            count++;
                        });
                        $('#adunit-select').multiSelect('refresh');
                        $('#adunit-select').multiSelect('select', selectedItem);
                    }
                });
            });
            FormValidation.init();
<?php } ?>
<?php if ($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item_flight" || $this->params['action'] == "edit_line_item_flight")) { ?>
    <?php echo $this->element('default_line_item_flight_js'); ?>
<?php } ?>
<?php if ($this->params['controller'] == "insertionorder" && $this->params['action'] == "edit_line_item") { ?>
            var favorite = [];
            $.each($("input[name='productTypes[]']:checked"), function () {
                favorite.push($(this).val());
            });
            $('#adunit-select').multiSelect('refresh');
            //alert("My favourite sports are: " + favorite.join(", "));
            $.ajax({
                type: "POST",
                url: "/insertionorder/getAdUnitsByMediaType",
                data: 'media_id=' + favorite.join(","),
                dataType: "json",
                success: function (data) {
                    var count = 0;
                    $.each(data, function (index) {
                        $('#adunit-select').multiSelect('addOption', {value: data[index].id, text: data[index].text, index: count});
                        count++;
                    });
                    $('#adunit-select').multiSelect('refresh');
                }
            });
<?php } ?>

<?php if ($this->params['controller'] == "wall" && $this->params['action'] == "index") {
    ?>

            $("#socialShareWith").select2({
                placeholder: "Who should see this?",
                allowClear: true
            });

            $(".js-advertiser-array").select2({
                placeholder: "Start typing to select Advertiser Name ",
                allowClear: true
            });

            $(".js-line-item-array").select2({
                placeholder: "Start typing to select Line Item",
                allowClear: true
            });

            $(".js-io-array").select2({
                placeholder: "Select (or start typing) the order name for the above selected advertiser",
                allowClear: true
            });

            $(".js-adunit-array").select2({
                placeholder: "Start typing to select Ad Unit",
                allowClear: true
            });

            $(".js-advertiser-array").on("change", function (e) {
                //alert($(this).val());
                $('.js-io-array').empty();
                $.ajax({
                    type: "POST",
                    url: "/wall/getOrderByAdvertiser",
                    data: 'advertiser_id=' + $(this).val(),
                    dataType: "json",
                    success: function (data) {
                        $(".js-io-array").select2({
                            data: data
                        });
                    }
                });
            });

            $(".js-io-array").on("change", function (e) {
                //alert($(this).val());
                $('.js-line-item-array').empty();
                $.ajax({
                    type: "POST",
                    url: "/wall/getLineItems",
                    data: 'order_id=' + $(this).val(),
                    dataType: "json",
                    success: function (data) {
                        $(".js-line-item-array").select2({
                            data: data
                        });
                    }
                });
            });

            $(".js-line-item-array").on("change", function (e) {
                //alert($(this).val());
                $('.js-adunit-array').empty();
                $.ajax({
                    type: "POST",
                    url: "/wall/view_mapped_adunits",
                    data: 'line_item_id=' + $(this).val(),
                    dataType: "json",
                    success: function (data) {
                        $(".js-adunit-array").select2({
                            data: data
                        });
                    }
                });
            });

            $(".js-adunit-array").on("change", function (e) {
                $("#hdnMappedId").val($(this).val());
            });

            $("#LineItemWithCreative").select2({
                placeholder: "Select Line Item"
            });

            $('#tabs div:first').show();
            $('#tabs ul li:first').addClass('active');
            $('#tabs ul li a').click(function () {
                $('#tabs ul li').removeClass('active');
                $(this).parent().addClass('active');
                var currentTab = $(this).attr('href');
                $('#tabs div').hide();
                $('.dz-default').show();
                $(currentTab).show();
                return false;
            });
            FormDropzone.init();
<?php } ?>

<?php if (($this->params['controller'] == "orders" || $this->params['controller'] == "insertionorder" ) && ($this->params['action'] == "upload_creative_text_html" || $this->params['action'] == "edit_upload_creative_text_html" || $this->params['action'] == "view_line_creatives")) { ?>
            $(".js-creative-array").select2({
                placeholder: "Start typing to select creative type ",
                allowClear: true
            });
            $(".js-creative-image-array").select2({
                placeholder: "Start typing to select image type ",
                allowClear: true
            });
            $(".js-creative-status-array").select2({
                placeholder: "Start typing to select status ",
                allowClear: true
            });

            $(window).load(function () {
                $('.select2-container--default').css("width", "100%");
            })
            FormDropzone.init();
<?php } ?>

<?php if ($this->params['controller'] == "leads" && ($this->params['action'] == "reConciliation" || $this->params['action'] == "index")) { ?>
    <?php if ($this->params['action'] == "index") { ?>
                $(".costStructure").select2({
                    maximumSelectionLength: 1,
                    placeholder: "Start Typing to Select ONLY ONE "
                });
                $('input[name="data[Lead][daterange]"]').daterangepicker();

                $(function () {

                    function cb(start, end) {
                        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    }
                    cb(moment().subtract(29, 'days'), moment());

                    $('input[name="data[Lead][daterange]"]').daterangepicker({
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        }
                    }, cb);

                });




    <?php } ?>

            $(".onlynumber").inputmask();
            $(".js-order-array").select2({
                placeholder: "Select (or start typing) the order name",
                //allowClear: true
            });
            $(".js-company-array").select2({
                placeholder: "Select (or start typing) the company name",
                //allowClear: true
            });
            $(".js-line-array").select2({
                placeholder: "Select (or start typing) the line item name",
                //allowClear: true
            });
            $('#LeadCompanyId').change(function () {
                
                //alert(JSON.stringify($("#LeadCompanyId :selected")));
                // Remove all Manage Lead Allocation
                var favorite = [];
                var selectedItem = [];
                $.each($("#LeadCompanyId :selected"), function () {
                    favorite.push($(this).val());
                     $("#comp_id").val($(this).val());
                });
                $("#LeadOrderId").empty();
                $("#LeadLineItem").empty();
                $(".js-order-array").select2({
                    placeholder: "Select (or start typing) the order name",
                    allowClear: true
                });
                $(".js-line-array").select2({
                    placeholder: "Select (or start typing) the line item name ",
                    //allowClear: true
                });

                var selecid = favorite.join(",");

                //alert("My favourite sports are: " + favorite.join(", "));
                $.ajax({
                    type: "POST",
                    url: "/leads/getAllOrderList",
                    data: 'user_id=' + favorite.join(","),
                    dataType: "json",
                    success: function (data) {
                        var count = 0;
                        //alert(JSON.stringify(data));
                        LoadLineItems(data[0].id);

                        $("#LeadOrderId").select2({
                            data: data
                        });
                        $(".js-order-array").select2({
                            placeholder: "Select (or start typing) the order name",
                            //allowClear: true
                        });

                    }
                });

            });
        
           function LoadLineItems(id) {
                // Remove all Manage Lead Allocation
                var favorite = [];
                var selectedItem = [];
                favorite.push(id);
                $("#order_id").val(id);
                $("#LeadLineItem").empty();
                $(".js-line-array").select2({
                    placeholder: "Select (or start typing) the line item name ",
                    //allowClear: true
                });
                var selecid = favorite.join(",");

                //alert("My favourite sports are: " + favorite.join(", "));
                $.ajax({
                    type: "POST",
                    url: "/leads/getAllLineItemByOrder",
                    data: 'order_id=' + favorite.join(","),
                    dataType: "json",
                    success: function (data) {
                        var count = 0;
                        $("#LeadLineItem").select2({
                            data: data
                        });
                        $(".js-line-array").select2({
                            placeholder: "Select (or start typing) the line item name ",
                            allowClear: true
                        });


                    }
                });

            }
        
      $('#LeadOrderId').change(function () {
                    //alert();
                // Remove all Manage Lead Allocation
                var favorite = [];
                var selectedItem = [];
                $.each($("#LeadOrderId :selected"), function () {
                    favorite.push($(this).val());
                    $("#order_id").val($(this).val());
                });
                $("#LeadLineItem").empty();
                $(".js-line-array").select2({
                    placeholder: "Select (or start typing) the line item name ",
                    //allowClear: true
                });
                var selecid = favorite.join(",");

                //alert("My favourite sports are: " + favorite.join(", "));
                $.ajax({
                    type: "POST",
                    url: "/leads/getAllLineItemByOrder",
                    data: 'order_id=' + favorite.join(","),
                    dataType: "json",
                    success: function (data) {
                        var count = 0;

                        $("#LeadLineItem").select2({
                            data: data
                        });
                        $(".js-line-array").select2({
                            placeholder: "Select (or start typing) the line item name ",
                            allowClear: true
                        });


                    }
                });

            });


<?php } ?>
<?php if ($this->params['controller'] == "messages" && $this->params['action'] == "create") { ?>
            $(".js-advertiser-array").select2({
                placeholder: "Start typing to select Advertiser Name ",
                allowClear: true
            });

            $(".js-line-item-array").select2({
                placeholder: "Start typing to select Line Item",
                allowClear: true
            });

            $(".js-io-array").select2({
                placeholder: "Select (or start typing) the order name for the above selected advertiser",
                allowClear: true
            });

            $(".js-adunit-array").select2({
                placeholder: "Start typing to select Ad Unit",
                allowClear: true
            });

            $(".js-advertiser-array").on("change", function (e) {
                //alert($(this).val());
                $('.js-io-array').empty();
                $.ajax({
                    type: "POST",
                    url: "/wall/getOrderByAdvertiser",
                    data: 'advertiser_id=' + $(this).val(),
                    dataType: "json",
                    success: function (data) {
                        $(".js-io-array").select2({
                            data: data
                        });
                    }
                });
            });

            $(".js-io-array").on("change", function (e) {
                //alert($(this).val());
                $('.js-line-item-array').empty();
                $.ajax({
                    type: "POST",
                    url: "/wall/getLineItems",
                    data: 'order_id=' + $(this).val(),
                    dataType: "json",
                    success: function (data) {
                        $(".js-line-item-array").select2({
                            data: data
                        });
                    }
                });
            });

            $(".js-line-item-array").on("change", function (e) {
                //alert($(this).val());
                $('.js-adunit-array').empty();
                $.ajax({
                    type: "POST",
                    url: "/wall/view_mapped_adunits",
                    data: 'line_item_id=' + $(this).val(),
                    dataType: "json",
                    success: function (data) {
                        $(".js-adunit-array").select2({
                            data: data
                        });
                    }
                });
            });


            FormDropzone.init();
            $('#dt1').datepicker({
            });
            $('#dt2').datepicker({
            });
<?php } ?>
<?php if (($this->params['controller'] == "inventory_management" && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit")) || ($this->params['controller'] == "esp" && $this->params['action'] == "report")) { ?>
            // for report
            $(".js-profile-array").select2({
                placeholder: "Select the publisher name"
            });
            $(".js-adunit-array").select2({
                placeholder: "Select adunit name"
            });
            $(".js-mail-array").select2({
                placeholder: "Select mail name "
            });

            $(".js-profile-array").on("change", function (e) {
                $('.js-mail-array').empty();
                $('.js-adunit-array').empty();

                $.ajax({
                    type: "POST",
                    url: "/esp/getAdunits",
                    data: 'publisher_id=' + $(this).val(),
                    dataType: "json",
                    success: function (data) {
                        $(".js-adunit-array").select2({
                            data: data
                        });
                    }
                });
            });
            $(".js-adunit-array").on("change", function (e) {
                //alert($(this).val());

                $('.js-mail-array').empty();
                $.ajax({
                    type: "POST",
                    url: "/esp/getMails",
                    data: 'adunit_id=' + $(this).val(),
                    dataType: "json",
                    success: function (data) {
                        $(".js-mail-array").select2({
                            data: data
                        });
                    }
                });
            });
            // close for report 
            $(".js-example-basic-single").select2({
                placeholder: "Start typing to select ESP Name ",
                allowClear: true
            });

            $(".js-esp-list-array").select2({
                placeholder: "Select the ESP provider first"
            });

            $(".js-comapny-array").select2({
                placeholder: "Select the ESP provider first"
            });
            $(".js-product-array").select2({
                placeholder: "Select the product type"
            });
            $(".js-placement-array").select2({
                placeholder: "Select the placement type"
            });
            $(".js-unit_type-array").select2({
                placeholder: "Select the unit type"
            });
            $('#cp3').colorpicker();
            $('#cp4').colorpicker();
            $('#cp5').colorpicker();
            $('#cp6').colorpicker();
            $(".js-example-basic-single").on("change", function (e) {
                //alert($(this).val());
                $('.js-esp-list-array').empty();
                if ($(this).val() == 1)
                {
                    $.ajax({
                        type: "POST",
                        url: "/inventory_management/getLyrisList",
                        dataType: "json",
                        success: function (data) {
                            $(".js-esp-list-array").select2({
                                data: data
                            });
                        }

                    });
                } else if ($(this).val() == 2)
                {
                    $.ajax({
                        type: "POST",
                        url: "/inventory_management/getSocketLabList",
                        dataType: "json",
                        success: function (data) {
                            $(".js-esp-list-array").select2({
                                data: data
                            });

                        }
                    });
                } else if ($(this).val() == 3)
                {
                    $.ajax({
                        type: "POST",
                        url: "/inventory_management/getMailchimpList",
                        dataType: "json",
                        success: function (data) {
                            $(".js-esp-list-array").select2({
                                data: data
                            });
                        }
                        //$('js-esp-list-array').val('225791');
                    });

                } else {
                    $('.js-esp-list-array').empty();
                }
                $(".js-esp-list-array").select2().select2('val', '225791');

            });

            $(".js-product-array").on("change", function (e) {
                //alert($(this).val());
                $('.js-placement-array').empty();
                if ($(this).val() == 6)
                {
                    $.ajax({
                        type: "POST",
                        url: "/inventory_management/getPlacementList",
                        dataType: "json",
                        success: function (data) {
                            $(".js-placement-array").select2({
                                data: data
                            });
                        }

                    });

                    $('.js-unit_type-array').empty();

                    $.ajax({
                        type: "POST",
                        url: "/inventory_management/getUnitList",
                        dataType: "json",
                        success: function (data) {
                            $(".js-unit_type-array").select2({
                                data: data
                            });
                        }

                    });
                } else {
                    $('.js-placement-array').empty();
                    $('.js-unit_type-array').empty();
                }
                $(".js-placement-array").select2().select2('val', '225791');
                $(".js-unit_type-array").select2().select2('val', '225792');

            });

            $('#productType').change(function () {
                if ($("#productType").val() != "6") {

                    jQuery('#placement').hide("slow");
                    jQuery('#ad_adtype_id').hide("slow");
                }

                if ($("#productType").val() === "6") {

                    jQuery('#placement').show("slow");
                    jQuery('#ad_adtype_id').show("slow");
                }

            });

            $('#tblAdunitAdPtypeId').change(function () {
                if ($("#tblAdunitAdPtypeId").val() != "6") {

                    jQuery('#placement').hide("slow");
                    jQuery('#ad_adtype_id').hide("slow");
                }

                if ($("#tblAdunitAdPtypeId").val() === "6") {

                    jQuery('#placement').show("slow");
                    jQuery('#ad_adtype_id').show("slow");
                }

            });


<?php } ?>
<?php if (($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_user_profile") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_user_profile")) { ?>
            ChartsAmcharts.init();
<?php } ?>

<?php if ($this->params['controller'] == "inventory_management" && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit")) { ?>
            $('#datacenter-select').multiSelect();
<?php } ?>
        // edit upload  creative

<?php
if ($this->params['controller'] == "insertionorder" && ( $this->params['action'] == "edit_upload_creative_text_html")) {
    echo 'checkMoreInfo("' . $creatives['tblCreative']['creatives_type'] . '")';
}
?>


    });

<?php if ($this->params['controller'] == "leads" && $this->params['action'] == "reConciliation") { ?>
        $(".LeadLineId").select2({
            placeholder: "Start typing to select order ",
            allowClear: true
        });
        function ShowOrderItem(option) {
            if (!option) {
                alert('Please select order ');
                return false;
            }
            $('#LeadLineId').empty();

            $.ajax({
                type: "GET",
                url: "/leads/order_line_item/" + option,
                dataType: "json",
                success: function (data) {
                    $("#LeadLineId").select2({
                        data: data
                    });
                }
            });
        }

<?php } ?>
<?php if ($this->params['controller'] == "insertionorder" && $this->params['action'] == "add") { ?>
        function checkRevenueType(option) {
            $('#adAutoAllCompany').empty();
            $('#adAutoAllCompany').select2({
                placeholder: "Start typing to select Advertiser Name ",
                allowClear: true
            });
            $.ajax({
                type: "GET",
                url: "/insertionorder/inhousecompany/" + option,
                dataType: "json",
                success: function (data) {
                    $("#adAutoAllCompany").select2({
                        data: data
                    });
                }
            });
        }

<?php } ?>
<?php if ($this->params['action'] == "add_line_item" || $this->params['action'] == "edit_line_item") { ?>
        jQuery(document).ready(function () {

            jQuery('input[name="data[tblLineItem][li_is_scrub]"]').on('ifUnchecked', function (event) {
                jQuery('#ScrubAcceptanceDiv').hide("slow");


            });
            jQuery('input[name="data[tblLineItem][li_is_scrub]"]').on('ifChecked', function (event) {
                jQuery('#ScrubAcceptanceDiv').show("slow");
                jQuery('#txtScrubRate').focus();

            });

            jQuery('input[name="data[AutoResponder][is_autoresp]"]').on('ifUnchecked', function (event) {
                jQuery('#AutoResponder').hide("slow");
            });
            jQuery('input[name="data[AutoResponder][is_autoresp]"]').on('ifChecked', function (event) {
                jQuery('#AutoResponder').show("slow");
            });


        });

<?php } ?>
</script>

<?php echo $scripts_for_layout; ?>
<!-- Js writeBuffer -->
<?php
if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer'))
    echo $this->Js->writeBuffer();
// Writes cached scripts
echo $this->Html->script('jquery.csv-0.71.min.js');
?>
<script>
    function switchPanel() {
        $.ajax({
            url: '/Cron/switchPanel',
            type: 'POST',
            success: function (response) {
                document.location.href = "/";
            }
        });
    }
</script>
<!-- END JAVASCRIPTS -->
<?php
if ($this->params['controller'] == "leads" && $this->params['action'] == "index") {
    echo $this->Html->script('/plugins/bootstrap-daterangepicker/daterangepicker.js');
}
?>

<script>
    // this condition is not working when select multiple
<?php if (($this->params['controller'] != "orders" && $this->params['action'] != "reports")) { ?>
        $(document).ready(function () {
            $(".select2-selection").on("focus", function () {
                $(this).parent().parent().prev().select2("open");
            });
        });
<?php } ?>
</script>
