
<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<?php $css =$js=array();
			array_push($css,'https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all','/plugins/font-awesome/css/font-awesome.min.css','/plugins/simple-line-icons/simple-line-icons.min.css','/plugins/bootstrap/css/bootstrap.min.css','/plugins/bootstrap/css/bootstrap.min.css','/plugins/uniform/css/uniform.default.css');
			array_push($js,'/plugins/jquery.min.js');
			if($this->params['action'] == "add_line_item" || $this->params['action'] == "edit_line_item") {
				array_push($js,	'/plugins/ckeditor/vendor/ckeditor/ckeditor.js');				
				//echo $this->Html->script('');
			}?>
		<!-- END GLOBAL MANDATORY STYLES -->
		<?php
			echo $this->Html->css($css);
			echo $this->Html->script($js);
			$css =$js=array();
			// clear array here 
		
			echo $this->Html->meta('icon');			
			echo $this->Html->css('components-rounded');
			echo $this->Html->css('plugins');
			echo $this->Html->css('layout');
			//echo $this->Html->css('default');
			echo $this->Html->css('custom');			
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
			
			// these script added from controllers
			echo $this->System->load_css('head');
			echo $this->System->load_js('head');
			echo $this->System->load_scripts_block('head');
			echo $this->System->load_scripts_document_block();
			echo $this->System->load_css_block('head'); ?>
			
			<?php 
			/*echo $action = strtolower($this->name) . '_' . strtolower($this->action);

			switch ($action) {
				case 'company_index':
					array_push($css,'/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
					break;
				case 'settings_index':
					array_push($css,'/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
					break;
				case 'publishers_index':
					array_push($css,'/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
					break;
				case 'newsletter_view_subscribers':
					array_push($css,'/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
					break;
				case 'orders_index':
					array_push($css,'/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
					break;
				case 'orders_orders':
					array_push($css,'/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
					break;
				case 'inventory_management_addadunit':
					array_push($css,'//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css','/plugins/jquery-multi-select/css/multi-select.css');
					break;
				case 'esp_report':
					array_push($css,'//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css','/plugins/jquery-multi-select/css/multi-select.css');
					break;
				
				
				default:

						break;
				}*/
			
			?>
			
		<?php if(  ($this->params['controller'] == "publishers" && $this->params['action'] == "index") || ($this->params['controller'] == "publishers" && $this->params['action'] == "index") || ($this->params['controller'] == "newsletter" && $this->params['action'] == "view_subscribers") || ($this->params['controller'] == "orders" && ($this->params['action'] == "index" ||$this->params['action'] == "orders" )) || ($this->params['controller'] == "leads" && $this->params['action'] == "index") || ($this->params['controller'] == "delivery" && $this->params['action'] == "index") || ($this->params['controller'] == "messages" && $this->params['action'] == "view_email_lists"))
			{ 
				array_push($css,'/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');	
			} ?>
			<?php if(($this->params['controller'] == "inventory_management" && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit"))|| ($this->params['controller'] == "esp" && $this->params['action'] == "report")) {
			array_push($css,'/plugins/select2/select2.min.css','/plugins/jquery-multi-select/css/multi-select.css');
		 } ?>
		 <?php if($this->params['controller'] == "settings" && $this->params['action'] == "index") { 
			array_push($css,'pricing-table.css');	
		} ?>
		<?php if(($this->params['controller'] == "company" && ($this->params['action'] == "add" || $this->params['action'] == "edit" )) || ($this->params['controller'] == "publishers" && $this->params['action'] == "add")){
			array_push($css,'/plugins/jquery-multi-select/css/multi-select.css','/plugins/icheck/skins/all.css','/plugins/select2/select2country.css','/plugins/bootstrap-datepicker/css/datepicker.css','/plugins/typeahead/typeahead.css');		
			}  
		 
			array_push($css,'/plugins/jquery-multi-select/css/multi-select.css','/plugins/icheck/skins/all.css','/plugins/select2/select2.min.css','/plugins/bootstrap-datepicker/css/datepicker.css','/plugins/bootstrap-daterangepicker/daterangepicker.css');?>
			  
		 
		<?php if($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item_flight" || $this->params['action'] == "edit_line_item_flight" )) {
				array_push($css,'/plugins/querybuilder/query-builder.default.min.css');
			} ?>
		 <?php if($this->params['controller'] == "newsletter" && $this->params['action'] == "upload_subscribers") { 
			  array_push($css,'/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css','/plugins/jquery-file-upload/css/jquery.fileupload.css','/plugins/jquery-file-upload/css/jquery.fileupload-ui.css');
			} ?>
		<?php if(in_array($this->params['controller'],array("insertionorder","leads","newsletter")) && in_array($this->params['action'],array("upload_creative","upload_subscribers","reConciliation","upload_creative_text_html","edit_upload_creative_text_html"))) { 
			 array_push($css,'/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css','/plugins/jquery-file-upload/css/jquery.fileupload.css','/plugins/jquery-file-upload/css/jquery.fileupload-ui.css');
		 } ?>
		<?php if($this->params['controller'] == "wall" && $this->params['action'] == "index") { 
			  array_push($css,'/plugins/jquery-multi-select/css/multi-select.css','/plugins/dropzone/css/dropzone.css','timeline.css','timeline2.css','fb-buttons.css','/plugins/select2/select2.min.css');
		} ?>
		<?php if(($this->params['controller'] == "messages" ||$this->params['controller'] == "leads" || $this->params['controller'] == "insertionorder" || $this->params['controller'] == "orders") && ($this->params['action'] == "create" || $this->params['action'] == "edit_upload_creative_text_html"||$this->params['action'] == "upload_creative_text_html"|| $this->params['action'] == "view_line_creatives"|| $this->params['action'] == "reConciliation")) { 
			 array_push($css,'/plugins/jquery-multi-select/css/multi-select.css','/plugins/dropzone/css/dropzone.css','timeline.css','/plugins/bootstrap-datepicker/css/datepicker.css','timeline2.css','fb-buttons.css','/plugins/select2/select2.min.css');
			?>
		<?php } ?>
		<?php if(($this->params['controller'] == "inventory_management" && $this->params['action'] == "add_coreg_placement") || ($this->params['controller'] == "inventory_management" && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit"))) { 
			array_push($css,'/plugins/bootstrap-colorpicker/css/colorpicker.css');	
		} ?>
		<?php 
			echo $this->Html->css($css);
			echo $this->Html->script($js);
			$css =$js=array(); // clear css and js array
		?>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
