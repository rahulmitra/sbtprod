<div class="top-menu">
	<ul class="nav navbar-nav">
		<!-- BEGIN NOTIFICATION DROPDOWN -->
		<li class="dropdown dropdown-extended dropdown-dark dropdown-notification hide" id="header_notification_bar">
			<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<i class="icon-bell"></i>
				<span class="badge badge-default">1</span>
			</a>
			<ul class="dropdown-menu">
				<li class="external">
					<h3>You have <strong>1 pending</strong> notification</h3>
					<a href="javascript:;">view all</a>
				</li>
				<li>
					<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
						<li>
							<a href="javascript:;">
								<span class="time">just now</span>
								<span class="details">
									<span class="label label-sm label-icon label-success">
										<i class="fa fa-plus"></i>
									</span>
								New user registered. </span>
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</li>
		<!-- END NOTIFICATION DROPDOWN -->
		<!-- BEGIN USER LOGIN DROPDOWN -->
		<li class="dropdown dropdown-user dropdown-dark">
			<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
				<img alt="" class="img-circle" src="<?php echo $this->Gravatar->get_gravatar($this->Authake->getUserEmail(),41,'','',false) ?>" />
				<span class="username username-hide-mobile"><?php echo  $this->Authake->getLogin(); ?></span>
			</a>
			<ul class="dropdown-menu dropdown-menu-default">
				<li>
					<?php echo $this->Html->link('<i class="icon-user"></i>Home</a>','/',array('escape'=>false));?>					
				</li>
				<li class="divider">
				</li>
				<li>
					<?php echo $this->Html->link('<i class="icon-key"></i>Settings',array('controller'=>'settings','action'=>'index'),array('escape'=>false));?>
				</li>
				<li class="divider">
				</li>
				<li>
					<?php echo $this->Html->link('<i class="icon-key"></i>Log Out',array('controller'=>'logout'),array('escape'=>false));?>
				</li>
			</ul>
		</li>
		<!-- END USER LOGIN DROPDOWN -->
	</ul>
</div>
<!--
	<?php $groupId = $this->Authake->getGroupIds();
		if($groupId[0] == "4") { ?>
		<div class="btn-group pull-right"  style="margin-top:10px;margin-left:10px;">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-ellipsis-horizontal"></i> Buying Activity <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu">
		<li>
		<a href="javascript:switchPanel();">
		Switch to Selling Activity </a>
		</li>
		</ul>
		</div>
		<?php }  if ($groupId[0] == "3") { ?>
		<div class="btn-group pull-right"  style="margin-top:10px;margin-left:10px;">
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		<i class="fa fa-ellipsis-horizontal"></i> Selling Activity <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu">
		<li>
		<a href="javascript:switchPanel();">
		Switch to Buying Activity </a>
		</li>
		</ul>
		</div>
	<?php } ?>-->
	<!-- END TOP NAVIGATION MENU -->
