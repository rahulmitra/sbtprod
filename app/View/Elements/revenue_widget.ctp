
<div class="portlet light ">
	<div class="portlet-body">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover" id="sample_4">
				<thead>
					<tr>
						<th class="table-checkbox">
							<b>Gross Revenue MTD</b>
						</th>
						<th>
							<b>Today's Total Earnings</b>
						</th>
						<th>
							<b>Lead-Gen Revenue Today</b>	
						</th>
						<th>
							<b>Email Revenue Today</b>
						</th>
						<th>
							<b>Display Revenue Today</b>
						</th>
						<th>
							<b>Social Revenue Today</b>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr class="odd gradeX">
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$<?php echo number_format($grossrevenue,2); ?></span>
						</td>
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$<?php echo $todaytotal; ?></span><i style="font-size:35px;color:red;margin-left:5px;" class="fa fa-arrow-down"></i>
						</td>
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$<?php echo $todayLeadGentotal; ?></span><i style="font-size:35px;color:green;margin-left:5px;" class="fa fa-arrow-up"></i>
						</td>
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$<?php echo $todayEmailtotal; ?></span><i style="font-size:35px;color:green;margin-left:5px;" class="fa fa-arrow-up"></i>
						</td>
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$<?php echo $todayDisplaytotal; ?></span><i style="font-size:35px;color:red;margin-left:5px;" class="fa fa-arrow-down"></i>
						</td>
						<td align="center">
							<span style="font-size: 40px;font-weight: bold;">$<?php echo $todaySocialtotal; ?></span><i style="font-size:35px;color:green;margin-left:5px;" class="fa fa-arrow-up"></i>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- END PORTLET-->

