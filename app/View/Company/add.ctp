<?php $this->Html->addCrumb('Advertisers', $this->Html->url( null, true ));
?>
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet light" id="form_wizard_1">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-green-sharp bold uppercase">
						<i class="fa fa-gift"></i><span class="step-title"> Step 1 of 4 </span>
					</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				<?php  if(empty($pop))
				{
					$popvalue="";
				}else{
					$popvalue='/'.$pop;
				}  ?>
				<form action="/company/add<?php echo $popvalue;?>" class="form-horizontal" id="submit_form" method="POST">
					<div class="form-wizard">
						<div class="form-body">
							<ul class="nav nav-pills nav-justified steps">
								<li>
									<a href="#tab1" data-toggle="tab" class="step">
										<span class="number">
										1 </span>
										<span class="desc">
										<i class="fa fa-check"></i> Search & Connect </span>
									</a>
								</li>
								<li>
									<a href="#tab2" data-toggle="tab" class="step">
										<span class="number">
										2 </span>
										<span class="desc">
										<i class="fa fa-check"></i> Company Primary Contact </span>
									</a>
								</li>
								<li>
									<a href="#tab3" data-toggle="tab" class="step">
										<span class="number">
										3 </span>
										<span class="desc">
										<i class="fa fa-check"></i> Company Profile Setup </span>
									</a>
								</li>
								<li>
									<a href="#tab4" data-toggle="tab" class="step active">
										<span class="number">
										4 </span>
										<span class="desc">
										<i class="fa fa-check"></i>  Confirm </span>
									</a>
								</li>
							</ul>
							<div id="bar" class="progress progress-striped" role="progressbar">
								<div class="progress-bar progress-bar-success">
								</div>
							</div>
							<div class="tab-content">
								<div class="alert alert-danger display-none">
									<button class="close" data-dismiss="alert"></button>
									You have some form errors. Please check below.
								</div>
								<div class="alert alert-success display-none">
									<button class="close" data-dismiss="alert"></button>
									<img src="/img/ajax-loade.gif"> Creating..
								</div>
								<div class="tab-pane active" id="tab1">
									<h3 class="block">Company</h3>
									<div class="form-group">
										<label class="control-label col-md-3">Company Name <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<?php echo $this->Form->input('tblProfile.CompanyName', array(
													  'class' => 'typeahead-devs form-control',
													  'placeholder'=>'Start typing company name here',
													  'label'=>false,
													  'div'=>false,
													  'id'=>'rep_name',
													  
													   ));
													   
													   ?>
											
											
											<span class="help-block">
											</span>
										</div>
									</div>
									
									<div class="form-group" style="display:none;">
										<label class="control-label col-md-3">Status <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<?php echo $this->Form->input('User.disable', array(
													  'class' => 'form-control',
													  'label'=>false,
													  'type'=>'select',
													  'options' =>array('0'=>'Enable','1'=>'Disable'),
													   
											   ));?>
											 <span class="help-block">
											</span>
										</div>
									</div>
									
								</div>
								<div class="tab-pane" id="tab2">
									<h3 class="block">Primary Company Contact</h3>
									<div style="display:none;" class="form-group">
										<label class="control-label col-md-3">Username <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="data[User][login]"/>
											<span class="help-block">
											Provide your username </span>
										</div>
									</div>
									<div class="form-group" style="display:none;">
										<label class="control-label col-md-3">Password <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input type="password" class="form-control" name="data[User][password]" id="submit_form_password"/>
											<span class="help-block">
											Provide your password. </span>
										</div>
									</div>
									<div class="form-group" style="display:none;">
										<label class="control-label col-md-3">Confirm Password <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input type="password" class="form-control" name="rpassword"/>
											<span class="help-block">
											Confirm your password </span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Contact's Full Name <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											
											
											<?php echo $this->Form->input('User.ContactName', array(
													  'class' => 'typeahead-devs form-control',
													   'label'=>false,
													   'div'=>false,
													   ));
													   
										   ?> 
											<span class="help-block">
											Provide representative full name </span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Email <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<?php echo $this->Form->input('User.email', array(
													  'class' => 'form-control',
													   'type'=>'text',
													   'label'=>false,
													   'id'=>"rep_email",
													   'div'=>false,
													   ));
													   
										   ?>
											<span class="help-block">
											Provide representative email address </span>
										</div>
									</div>
									
								</div>
								<div class="tab-pane" id="tab3">
									<h3 class="block">Company Profile</h3>
									<div class="form-group">
										<label class="control-label col-md-3">Company URL 
										</label>
										<div class="col-md-4">
											<?php echo $this->Form->hidden('tblProfile.profileID'); ?>
											<?php echo $this->Form->hidden('User.id');
												$disabled ='';
											 ?>
											<?php echo $this->Form->input('tblProfile.CompanyURL', array(
													  'class' => 'typeahead-devs form-control',
													   'label'=>false,
													   'div'=>false,
													   'disabled'=>$disabled,
													   ));
												echo $this->Form->hidden('Group.Group', array(
													  'value'=>5,
													   ));	   
										   ?> 
											<span class="help-block">
											</span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Company Description</label>
										<div class="col-md-4">
											<?php echo $this->Form->textarea('tblProfile.CompanyDesc', array(
													   'rows' => 3,
													   'class' => 'form-control',
													   'disabled'=>$disabled,
													   'label'=>false,
													   'div'=>false,
													   ));
												 	   
										   ?> 
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Default Revenue Share<span class="required">
										* </span>
										</label>
										<div class="col-md-4"> 
											
											<?php echo $this->Form->number('tblProfile.rev_share', array(
													    'class' => 'form-control',
													   'label'=>false,
													   'disabled'=>$disabled,
													   'style'=>'width:70px;',
													   'placeholder'=>'in %',
													   'min'=>"1", 
													   'max'=>"100",
													   'value'=>"50",
													   'data-inputmask'=>"'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'placeholder': '0'",
													   'div'=>false,
													   ));
												 	   
										   ?> <div style="position: absolute;left: 103px;top: 8px;"> %</div>
											<span class="help-block">
											This information is used when you purchase media from this company.  You can change this for each placement & line item at a later date.</span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Phone Number  
										</label>
										<div class="col-md-4">
											<?php echo $this->Form->input('tblProfile.PrimaryPhone', array(
													    'class' => 'form-control',
													   'label'=>false,
													   'disabled'=>$disabled,
													   'div'=>false,
													   ));
												 	   
										   ?> 
											<span class="help-block">  </span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Primary Office Address <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<?php echo $this->Form->input('tblProfile.Address1', array(
													  'class'=>'form-control',
													   'label'=>false,
													   'disabled'=>$disabled,
													   'div'=>false,
													   ));
										   ?>
											<span class="help-block">
											  </span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Address 2
										</label>
										<div class="col-md-4">
											<?php echo $this->Form->input('tblProfile.Address2', array(
														 'class'=>'form-control',
													   'label'=>false,
													   'disabled'=>$disabled,
													   'div'=>false,
													   ));
										   ?> 
											<span class="help-block">  </span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">City/Town <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<?php echo $this->Form->input('tblProfile.City', array(
														'class'=>'form-control',
													    'label'=>false,
													    'disabled'=>$disabled,
													    'div'=>false,
													   ));
										   ?> 
											<span class="help-block">  </span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">State <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<?php echo $this->Form->input('tblProfile.State_Province', array(
														'class'=>'form-control',
													   'label'=>false,
													   'disabled'=>$disabled,
													   'div'=>false,
													   
													   ));
										   ?>
											<span class="help-block"> </span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Zip/Postal Code <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<?php echo $this->Form->input('tblProfile.ZIP_Postal', array(
													   'class'=>'form-control',
													   'disabled'=>$disabled,
													   'label'=>false,
													   'div'=>false,
													   ));
										   ?>
											<span class="help-block">  </span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Country</label>
										<div class="col-md-4">
											<?php $country =array("AF"=>"Afghanistan","AL"=>"Albania","DZ"=>"Algeria","AS"=>"American Samoa","AD"=>"Andorra","AO"=>"Angola","AI"=>"Anguilla","AR"=>"Argentina","AM"=>"Armenia","AW"=>"Aruba","AU"=>"Australia","AT"=>"Austria","AZ"=>"Azerbaijan","BS"=>"Bahamas","BH"=>"Bahrain","BD"=>"Bangladesh","BB"=>"Barbados","BY"=>"Belarus","BE"=>"Belgium","BZ"=>"Belize","BJ"=>"Benin","BM"=>"Bermuda","BT"=>"Bhutan","BO"=>"Bolivia","BA"=>"Bosnia and Herzegowina","BW"=>"Botswana","BV"=>"Bouvet Island","BR"=>"Brazil","IO"=>"British Indian Ocean Territory","BN"=>"Brunei Darussalam","BG"=>"Bulgaria","BF"=>"Burkina Faso","BI"=>"Burundi","KH"=>"Cambodia","CM"=>"Cameroon","CA"=>"Canada","CV"=>"Cape Verde","KY"=>"Cayman Islands","CF"=>"Central African Republic","TD"=>"Chad","CL"=>"Chile","CN"=>"China","CX"=>"Christmas Island","CC"=>"Cocos (Keeling) Islands","CO"=>"Colombia","KM"=>"Comoros","CG"=>"Congo","CD"=>"Congo, the Democratic Republic of the","CK"=>"Cook Islands","CR"=>"Costa Rica","CI"=>"Cote d'Ivoire","HR"=>"Croatia (Hrvatska)","CU"=>"Cuba","CY"=>"Cyprus","CZ"=>"Czech Republic","DK"=>"Denmark","DJ"=>"Djibouti","DM"=>"Dominica","DO"=>"Dominican Republic","EC"=>"Ecuador","EG"=>"Egypt","SV"=>"El Salvador","GQ"=>"Equatorial Guinea","ER"=>"Eritrea","EE"=>"Estonia","ET"=>"Ethiopia","FK"=>"Falkland Islands (Malvinas)","FO"=>"Faroe Islands","FJ"=>"Fiji","FI"=>"Finland","FR"=>"France","GF"=>"French Guiana","PF"=>"French Polynesia","TF"=>"French Southern Territories","GA"=>"Gabon","GM"=>"Gambia","GE"=>"Georgia","DE"=>"Germany","GH"=>"Ghana","GI"=>"Gibraltar","GR"=>"Greece","GL"=>"Greenland","GD"=>"Grenada","GP"=>"Guadeloupe","GU"=>"Guam","GT"=>"Guatemala","GN"=>"Guinea","GW"=>"Guinea-Bissau","GY"=>"Guyana","HT"=>"Haiti","HM"=>"Heard and Mc Donald Islands","VA"=>"Holy See (Vatican City State)","HN"=>"Honduras","HK"=>"Hong Kong","HU"=>"Hungary","IS"=>"Iceland","IN"=>"India","ID"=>"Indonesia","IR"=>"Iran (Islamic Republic of)","IQ"=>"Iraq","IE"=>"Ireland","IL"=>"Israel","IT"=>"Italy","JM"=>"Jamaica","JP"=>"Japan","JO"=>"Jordan","KZ"=>"Kazakhstan","KE"=>"Kenya","KI"=>"Kiribati","KP"=>"Korea, Democratic People's Republic of","KR"=>"Korea, Republic of","KW"=>"Kuwait","KG"=>"Kyrgyzstan","LA"=>"Lao People's Democratic Republic","LV"=>"Latvia","LB"=>"Lebanon","LS"=>"Lesotho","LR"=>"Liberia","LY"=>"Libyan Arab Jamahiriya","LI"=>"Liechtenstein","LT"=>"Lithuania","LU"=>"Luxembourg","MO"=>"Macau","MK"=>"Macedonia, The Former Yugoslav Republic of","MG"=>"Madagascar","MW"=>"Malawi","MY"=>"Malaysia","MV"=>"Maldives","ML"=>"Mali","MT"=>"Malta","MH"=>"Marshall Islands","MQ"=>"Martinique","MR"=>"Mauritania","MU"=>"Mauritius","YT"=>"Mayotte","MX"=>"Mexico","FM"=>"Micronesia, Federated States of","MD"=>"Moldova, Republic of","MC"=>"Monaco","MN"=>"Mongolia","MS"=>"Montserrat","MA"=>"Morocco","MZ"=>"Mozambique","MM"=>"Myanmar","NA"=>"Namibia","NR"=>"Nauru","NP"=>"Nepal","NL"=>"Netherlands","AN"=>"Netherlands Antilles","NC"=>"New Caledonia","NZ"=>"New Zealand","NI"=>"Nicaragua","NE"=>"Niger","NG"=>"Nigeria","NU"=>"Niue","NF"=>"Norfolk Island","MP"=>"Northern Mariana Islands","NO"=>"Norway","OM"=>"Oman","PK"=>"Pakistan","PW"=>"Palau","PA"=>"Panama","PG"=>"Papua New Guinea","PY"=>"Paraguay","PE"=>"Peru","PH"=>"Philippines","PN"=>"Pitcairn","PL"=>"Poland","PT"=>"Portugal","PR"=>"Puerto Rico","QA"=>"Qatar","RE"=>"Reunion","RO"=>"Romania","RU"=>"Russian Federation","RW"=>"Rwanda","KN"=>"Saint Kitts and Nevis","LC"=>"Saint LUCIA","VC"=>"Saint Vincent and the Grenadines","WS"=>"Samoa","SM"=>"San Marino","ST"=>"Sao Tome and Principe","SA"=>"Saudi Arabia","SN"=>"Senegal","SC"=>"Seychelles","SL"=>"Sierra Leone","SG"=>"Singapore","SK"=>"Slovakia (Slovak Republic)","SI"=>"Slovenia","SB"=>"Solomon Islands","SO"=>"Somalia","ZA"=>"South Africa","GS"=>"South Georgia and the South Sandwich Islands","ES"=>"Spain","LK"=>"Sri Lanka","SH"=>"St. Helena","PM"=>"St. Pierre and Miquelon","SD"=>"Sudan","SR"=>"Suriname","SJ"=>"Svalbard and Jan Mayen Islands","SZ"=>"Swaziland","SE"=>"Sweden","CH"=>"Switzerland","SY"=>"Syrian Arab Republic","TW"=>"Taiwan, Province of China","TJ"=>"Tajikistan","TZ"=>"Tanzania, United Republic of","TH"=>"Thailand","TG"=>"Togo","TK"=>"Tokelau","TO"=>"Tonga","TT"=>"Trinidad and Tobago","TN"=>"Tunisia","TR"=>"Turkey","TM"=>"Turkmenistan","TC"=>"Turks and Caicos Islands","TV"=>"Tuvalu","UG"=>"Uganda","UA"=>"Ukraine","AE"=>"United Arab Emirates","GB"=>"United Kingdom","US" =>"United States","UM"=>"United States Minor Outlying Islands","UY"=>"Uruguay","UZ"=>"Uzbekistan","VU"=>"Vanuatu","VE"=>"Venezuela","VN"=>"Viet Nam","VG"=>"Virgin Islands (British)","VI"=>"Virgin Islands (U.S.)","WF"=>"Wallis and Futuna Islands","EH"=>"Western Sahara","YE"=>"Yemen","ZM"=>"Zambia","ZW" =>"Zimbabwe"); ?>
											  
											 <?php echo $this->Form->input('tblProfile.Country', array(
													  'class' => 'form-control',
													  'label'=>false,
													  'id'=>'country_list',
													  'type'=>'select',
													  'empty' => 'Please Select Country',
													  'selected' =>'US',
													  'options' =>$country,
													   
											   ));?>
											 
											<?php echo $this->Form->hidden('tblUserConnect.tblProfileId'); ?>
											<?php echo $this->Form->hidden('tblUserConnect.userId'); ?> 
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab4">
									<h3 class="block">Confirm Company Details</h3>
									<h4 class="form-section">Primary Contact</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Representative Name:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[User][ContactName]">
											</p>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Email:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[User][email]">
											</p>
										</div>
									</div>
									<h4 class="form-section">Company Profile</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Company Name:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][CompanyName]">
											</p>
										</div>
									</div>
									<div class="form-group" id="company_url">
										<label class="control-label col-md-3">Company URL:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][CompanyURL]">
											</p>
										</div>
									</div>
									<div class="form-group"  id="company_desc">
										<label class="control-label col-md-3">Company Description:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][CompanyDesc]">
											</p>
										</div>
									</div>
									
									<div class="form-group"  id="company_phone">
										<label class="control-label col-md-3">Default Revenue Sharing:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][rev_share]">
											</p>
										</div>
									</div>
									<div class="form-group"  id="company_phone">
										<label class="control-label col-md-3">Phone:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][PrimaryPhone]">
											</p>
										</div>
									</div>
									<div class="form-group"  id="company_address1">
										<label class="control-label col-md-3">Primary Office Address:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][Address1]">
											</p>
										</div>
									</div>
									<div class="form-group"  id="company_address2">
										<label class="control-label col-md-3">Address 2:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][Address2]">
											</p>
										</div>
									</div>
									<div class="form-group"  id="company_city">
										<label class="control-label col-md-3">City/Town:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][City]">
											</p>
										</div>
									</div>
									<div class="form-group"  id="company_state">
										<label class="control-label col-md-3">State:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][State_Province]">
											</p>
										</div>
									</div>
									<div class="form-group"  id="company_zip">
										<label class="control-label col-md-3">Zip/Postal Code:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][ZIP_Postal]">
											</p>
										</div>
									</div>
									<div class="form-group"  id="company_country">
										<label class="control-label col-md-3">Country:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][Country]">
											</p>
										</div>
									</div>
									<div style="display:none;" class="form-group">
										<label class="control-label col-md-3">Company Category:</label>
										<div class="col-md-4">
											<p class="form-control-static" data-display="data[tblProfile][CompanyCategroy]">
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<a href="javascript:;" class="btn default button-previous">
									<i class="m-icon-swapleft"></i> Back </a>
									<a href="javascript:;" class="btn blue button-next">
										Continue <i class="m-icon-swapright m-icon-white"></i>
									</a>
									<a href="javascript:;" class="btn green button-submit">
										Submit <i class="m-icon-swapright m-icon-white"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->