<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Dashboard</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/company/">All Companies</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Companies</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<a href="/company/add" id="sample_editable_1_new" class="btn green">
									Add New <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						<div class="col-md-6">
						</div>
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover" id="companydata" >
					<thead>
						<tr>
							<th>
								Company Name
							</th>
							<th>
								Email
							</th>
							<th>
								Joined
							</th>
							<th>
								Status
							</th>
							<th class="center" style="text-align:center;">
								Action
							</th>
						</tr>
					</thead>
					  
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<script>

$(document).ready(function() {
	 
    $('#companydata').DataTable({
         "bProcessing": true,
         "serverSide": true,
         "ajax":{
            url :"/company/company_ajax_list", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#companydata_processing").css("display","none");
            }
          }
        });   
});
</script>
