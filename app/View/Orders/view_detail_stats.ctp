<?php
	/**
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Pages
		* @since         CakePHP(tm) v 0.10.0.1076
	*/
	
	if (!Configure::read('debug')):
	throw new NotFoundException();
	endif;
	
	App::uses('Debugger', 'Utility');
?>
<style>
	.font-hg {
	color:#000 !important;
	font-size:48px !important;
	font-weight:bold !important;
	}
	.font-grey-mint {
	color:#000 !important;
	}
	.font-sm {
	font-size:16px !important;
	}
</style>
<div class="row">
	<div class="col-md-4">
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="/">Home</a><i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="/orders/view_line_items/<?php echo $li_order_id; ?>/">Line Items</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				<a href="#"><?php echo $lineItem_name; ?></a>
			</li>
		</ul>
	</div>
	<div class="col-md-4">
		<div align="center">
		<a href="http://digitaladvertising.systems/orders/view_user_profile/<?php echo $line_item_id; ?>/">View Audience Data</a></div>
	</div>
	<div class="col-md-4">
		<div class="portlet-title" style="float:right;">
			<div class="actions">
				<div class="btn-group btn-group-devided" data-toggle="buttons">
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "day" ? "active" : "" ?>">
					<input type="radio" name="options" onchange="chart1()" class="toggle" id="option1">Today</label>
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "week" ? "active" : "" ?>">
					<input type="radio" name="options"  onchange="chart2()" class="toggle" id="option2">Last 7 Days</label>
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "month" ? "active" : "" ?>">
					<input type="radio" name="options"  onchange="chart3()" class="toggle" id="option3">MTD</label>
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "year" ? "active" : "" ?>">
					<input type="radio" name="options"  onchange="chart4()" class="toggle" id="option2">YTD</label>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE BREADCRUMB -->


<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-5">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Performance</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class="font-grey-mint font-sm">
								Generated
							</div>
							<div class="uppercase font-hg font-red-flamingo">
								<?php echo number_format($leads); ?><span class="font-lg font-grey-mint"></span>
							</div>
							<div class="font-grey-mint font-sm">
								Leads
							</div>
						</div>					
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class="font-grey-mint font-sm">
								Delivered
							</div>
							<div class="uppercase font-hg font-red-flamingo">
								<?php echo number_format($delivered); ?> <span class="font-lg font-grey-mint"></span>
							</div>
							<div class="font-grey-mint font-sm">
								Leads
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4">
							<div class="font-grey-mint font-sm">
								Accepted
							</div>
							<div class="uppercase font-hg font-red-flamingo">
								<?php echo number_format($accepted); ?> <span class="font-lg font-grey-mint"></span>
							</div>
							<div class="font-grey-mint font-sm">
								Leads
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-6">
							<img src="http://www.jlion.com:80//tools/Thermometer.aspx?MIN=0&MAX=<?php echo $totalGoalLead; ?>&VT=4&T=Trending&IV=<?php echo $accepted; ?>&M=0&SC=0&CS=2&CI=en-US&TH=1" style="border: solid 1px black;" alt="" title="" width="130" height="191"/>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-6">
							<div class="font-grey-mint font-sm">
								Trending
							</div>
							<div class="uppercase font-hg font-red-flamingo">
								<?php echo (number_format($totalTrending) - 100); ?>%
							</div>
							<div class="font-grey-mint font-sm">
								Below Goal
							</div>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
							<div id="chart_allocation"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption caption-md">
						<i class="fa fa-dollar font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">Business</span>
						<span class="caption-helper hide">weekly stats...</span>
					</div>
				</div>
				<div class="portlet-body">
					
					<div class="row  list-separated">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="font-grey-mint font-sm">
								Revenue
							</div>
							<div class="uppercase font-hg font-red-flamingo">
								$<?php echo number_format($totalAchivedRev); ?> <span class="font-lg font-grey-mint"></span>
							</div>
							<div class="font-grey-mint font-sm">
								<?php echo number_format($totalTrending); ?>% of Total $<?php echo number_format($totalGoalRev); ?>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="font-grey-mint font-sm">
								Average CPL
							</div>
							<div class="uppercase font-hg font-red-flamingo">
								$3.55 <span class="font-lg font-grey-mint"></span>
							</div>
							<div class="font-grey-mint font-sm">
								Revenue Per Lead $<?php echo $li_rate; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption caption-md">
						<i class="fa fa-dollar font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">TOP PERFORMING SOURCES</span>
						<span class="caption-helper hide">weekly stats...</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="uppercase font-hg font-red-flamingo">
								<div id="top_performing_line_items" width="100%" height="250"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-user font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">Users</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="col-md-12 col-sm-12">
							<div class="font-grey-mint font-sm">
								# of Subscribers
							</div>
							<div class="uppercase font-hg font-red-flamingo">
								<?php echo "0" ?> <span class="font-lg font-grey-mint"></span>
							</div>
							<div id="regions_div" style="width: 100%; height: 200px;"></div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="font-grey-mint font-sm">
								DAILY SUBSCRIPTION TRENDS
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-bar-chart-o font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">Trending By Channel</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div id="piechart_trending" class="gauge"></div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="clearfix">
								<div id="legendContainer_email_message" class="table-responsive"></div> 
							</div>
							<div id="chart_email_message" class="chart"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-bar-chart-o font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">A/B Creative Trending</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="row list-separated">
						<div class="col-md-1 col-sm-1 col-xs-12">
						</div>
							<div class="col-md-3 col-sm-3 col-xs-12">
								<div class="font-grey-mint font-sm">
									Email
								</div>
								<div id="piechart_cemail" style="width: 100%;"></div>
							</div>
							<div class="col-md-3 col-sm-3  col-xs-12">
								<div class="font-grey-mint font-sm">
									Coreg
								</div>
								<div id="piechart_ccoreg" style="width: 100%;"></div>
							</div>
							<div class="col-md-3 col-sm-3  col-xs-12">
								<div class="font-grey-mint font-sm">
									Creatives
								</div>
								<div id="piechart_creative" style="width: 100%;"></div>
							</div>
							<div class="col-md-1 col-sm-1 col-xs-12">
						</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row list-separated">
				<div class="col-md-9">
					<div class="portlet box grey-gallery">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-check  font-grey-silver"></i>
								<span class="caption-subject font-grey-silver bold uppercase">Quality</span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row list-separated">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<div class="font-grey-mint font-sm">
										3rd Party Rejects
									</div>
									<div class="uppercase font-hg font-red-flamingo">
										<?php echo $third_party_rejects; ?> <span class="font-lg font-grey-mint"></span>
									</div>
									<div class="font-grey-mint font-sm">
										<?php echo number_format((($third_party_rejects / $totalRejects) * 100),2) ?>%
									</div>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<div class="font-grey-mint font-sm">
										Client Rejects
									</div>
									<div class="uppercase font-hg font-red-flamingo">
									<?php echo $clients_rejects; ?> </span>
								</div>
								<div class="font-grey-mint font-sm">
									<?php echo number_format((($clients_rejects / $totalRejects) * 100),2) ?>%
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<div class="font-grey-mint font-sm">
									AR Rejects
								</div>
								<div class="uppercase font-hg font-red-flamingo">
									<?php echo $ar_rejects; ?><span class="font-lg font-grey-mint"></span>
								</div>
								<div class="font-grey-mint font-sm">
									<?php echo number_format((($ar_rejects / $totalRejects) * 100),2) ?>%
								</div>
							</div>
						</div>
						<div class="row list-separated">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="clearfix">
									<div id="legendContainer_email_quality" class="table-responsive"></div> 
								</div>
								<div id="chart_email_quality" class="chart"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="portlet box grey-gallery">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-desktop font-grey-silver"></i>
							<span class="caption-subject font-grey-silver bold uppercase">Technology</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row list-separated">
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="font-grey-mint font-sm">
									Device Type
								</div>
								<div id="piechart_desktop" style="width: 100%;"></div>
							</div>
							<div class="col-md-4 col-sm-4  col-xs-12">
								<div class="font-grey-mint font-sm">
									Browsers
								</div>
								<div id="piechart_windows" style="width: 100%;"></div>
							</div>
							<div class="col-md-4 col-sm-4  col-xs-12">
								<div class="font-grey-mint font-sm">
									OS Family
								</div>
								<div id="piechart_ios" style="width: 100%;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				
			</div>
		</div>
	</div>
	<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<!-- END PAGE CONTENT INNER -->

<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>

<!-- END PAGE CONTENT INNER -->
<script>
	
	var ChartsFlotcharts1 = function() {
		
		return {
			//main function to initiate the module
			initCharts: function() {
				
				if (!jQuery.plot) {
					return;
				}
				
				var data = [];
				var totalPoints = 250;
				
				//Interactive Chart
				
				function chart_email_message() {
					if ($('#chart_email_message').size() != 1) {
						return;
					}
					
					function randValue() {
						return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
					}
					var email = [
					<?php foreach($tblReportDayTrendingByChannel as $data) { 
						if($data['tbmt']['mtype_name'] == "Email") {
						?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_accepted']; ?>],
					<?php } } ?>
					];
					var desktop = [
					<?php foreach($tblReportDayTrendingByChannel as $data) { 
						if($data['tbmt']['mtype_name'] == "Desktop") {
						?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_accepted']; ?>],
					<?php } } ?>
					];
					var mobile = [
					<?php foreach($tblReportDayTrendingByChannel as $data) { 
						if($data['tbmt']['mtype_name'] == "Mobile") {
						?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_accepted']; ?>],
					<?php } } ?>
					];
					var coreg = [
					<?php foreach($tblReportDayTrendingByChannel as $data) { 
						if($data['tbmt']['mtype_name'] == "Lead-Gen") {
						?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_accepted']; ?>],
					<?php } } ?>
					];
					
					
					var plot = $.plot($("#chart_email_message"), [{
						data: email,
						label: "Email",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:0
						}, {
						data: desktop,
						label: "Desktop",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:1
						}, {
						data: mobile,
						label: "Mobile",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:2
						} , {
						data: coreg,
						label: "Co-Reg",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:3
						}], {
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
								fillColor: {
									colors: [{
										opacity: 0.05
										}, {
										opacity: 0.01
									}]
								}
							},
							points: {
								show: true,
								radius: 3,
								lineWidth: 1
							},
							shadowSize: 2
						},
						grid: {
							hoverable: true,
							clickable: true,
							tickColor: "#eee",
							borderColor: "#eee",
							borderWidth: 1
						},
						colors: ["#3366CC", "#DC3912", "#FF9900", "#109618"],
						xaxis: {
							mode: "time",
							tickSize: [1, "day"],    
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						yaxis: {
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						legend:{         
							placement: 'outsideGrid',
							container:$("#legendContainer_email_message"),    
							labelFormatter: function(label, series){
								return '<button data-toggle="button" onclick="togglePlot('+series.idx+');" class="btn btn-default ' + ((series.lines.show) ? "active" : "") + '" type="button" aria-pressed="false"> '+label + ((series.lines.show) ? " <span style=\"margin-left: 5px; font-size: 10px;\">&#10006;</span>" : "")+'</button>';
							},
							noColumns: 0
						}
					});
					
					function gd(date_c) {
						var date = Date.parse(date_c);
						return date.toString('dd-MMM-yyyy');
					}
					
					function showTooltip(x, y, contents) {
						$('<div id="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 15,
							border: '1px solid #333',
							padding: '4px',
							color: '#fff',
							'border-radius': '3px',
							'background-color': '#333',
							opacity: 0.80
						}).appendTo("body").fadeIn(200);
					}
					
					var previousPoint = null;
					$("#chart_2").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x);
						$("#y").text(pos.y);
						
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;
								
								$("#tooltip").remove();
								var x = item.datapoint[0],
								y = item.datapoint[1];
								showTooltip(item.pageX, item.pageY, item.series.label + " on " + convertTimestamp(x) + " is " + y);
							}
							} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
					
					togglePlot = function(seriesIdx)
					{
						var someData = plot.getData();  
						someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;  
						if (!someData[seriesIdx].lines.show){
							someData[seriesIdx].tempData = someData[seriesIdx].data;
							someData[seriesIdx].data = [];
						}
						else
						{
							someData[seriesIdx].data = someData[seriesIdx].tempData;
						}
						plot.setData(someData);
						plot.setupGrid();
						plot.draw();
					}
					
					
					function convertTimestamp(timestamp) {
						var nextDay = new Date(timestamp); // 86400000 - one day in ms 
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
						'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						return months[nextDay.getUTCMonth()] + ' ' + nextDay.getUTCDate();
					}
				}
				
				function chart_email_quality() {
					if ($('#chart_email_quality').size() != 1) {
						return;
					}
					
					function randValue() {
						return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
					}
					var thirdparty = [
					<?php foreach($tblReportDayQuality as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_3party']; ?>],
					<?php } ?>
					];
					var clients_rejects = [
					<?php foreach($tblReportDayQuality as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date'])  - 1; ?>,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_client_rejects']; ?>],
					<?php } ?>
					];
					var ar_rejects = [
					<?php foreach($tblReportDayQuality as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?>,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_ar_rejects']; ?>],
					<?php } ?>
					];
					
					var plot = $.plot($("#chart_email_quality"), [{
						data: thirdparty,
						label: "3rd Party Rejects",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:0
						}, {
						data: clients_rejects,
						label: "Client Rejects",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:1
						} , {
						data: ar_rejects,
						label: "AR Rejects",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:3
						}], {
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
								fillColor: {
									colors: [{
										opacity: 0.05
										}, {
										opacity: 0.01
									}]
								}
							},
							points: {
								show: true,
								radius: 3,
								lineWidth: 1
							},
							shadowSize: 2
						},
						grid: {
							hoverable: true,
							clickable: true,
							tickColor: "#eee",
							borderColor: "#eee",
							borderWidth: 1
						},
						colors: ["#d12610", "#37b7f3", "#52e136"],
						xaxis: {
							mode: "time",
							tickSize: [1, "day"],    
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						yaxis: {
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						legend:{         
							placement: 'outsideGrid',
							container:$("#legendContainer_email_quality"),    
							labelFormatter: function(label, series){
								return '<button data-toggle="button" onclick="togglePlot1('+series.idx+');" class="btn btn-default ' + ((series.lines.show) ? "active" : "") + '" type="button" aria-pressed="false"> '+label + ((series.lines.show) ? " <span style=\"margin-left: 5px; font-size: 10px;\">&#10006;</span>" : "")+'</button>';
							},
							noColumns: 0
						}
					});
					
					function gd(date_c) {
						var date = Date.parse(date_c);
						return date.toString('dd-MMM-yyyy');
					}
					
					function showTooltip(x, y, contents) {
						$('<div id="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 15,
							border: '1px solid #333',
							padding: '4px',
							color: '#fff',
							'border-radius': '3px',
							'background-color': '#333',
							opacity: 0.80
						}).appendTo("body").fadeIn(200);
					}
					
					var previousPoint = null;
					$("#chart_2").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x);
						$("#y").text(pos.y);
						
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;
								
								$("#tooltip").remove();
								var x = item.datapoint[0],
								y = item.datapoint[1];
								showTooltip(item.pageX, item.pageY, item.series.label + " on " + convertTimestamp(x) + " is " + y);
							}
							} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
					
					togglePlot1 = function(seriesIdx)
					{
						var someData = plot.getData();  
						someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;  
						if (!someData[seriesIdx].lines.show){
							someData[seriesIdx].tempData = someData[seriesIdx].data;
							someData[seriesIdx].data = [];
						}
						else
						{
							someData[seriesIdx].data = someData[seriesIdx].tempData;
						}
						plot.setData(someData);
						plot.setupGrid();
						plot.draw();
					}
					
					
					function convertTimestamp(timestamp) {
						var nextDay = new Date(timestamp); // 86400000 - one day in ms 
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
						'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						return months[nextDay.getUTCMonth()] + ' ' + nextDay.getUTCDate();
					}
				}
				
				
				
				//graph
				chart_email_message();
				chart_email_quality();
				
			}
		};
		
	}();
	
	
	function chart1(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "day/";
			} else {
			window.location = "/messages/index/all/day";
		}
	}
	
	function chart2(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "week/";
			} else {
			window.location = "/messages/index/all/week";
		}
	}
	
	function chart3(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "month/";
			} else {
			window.location = "/messages/index/all/month";
		}
	}
	
	function chart4(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "year/";
			} else {
			window.location = "/messages/index/all/year";
		}
	}
</script>
<!-- CSS -->
<style type="text/css">
	#legendContainer {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
	
	#legendContainer_email_message {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer_email_message .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
	
	#legendContainer_email_quality {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer_email_quality .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
</style>	
<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualization);
	
	function drawVisualization() {
		var data = google.visualization.arrayToDataTable([
		['Unit', 'Revenue'],
		<?php foreach($tblReportBySources as $data) { ?>
			['<?php echo $data['tba']['adunit_name']; ?>', <?php echo ($data[0]['rd_accepted'] * $li_rate); ?>],
		<?php } ?>
		]);
		
		var options = {
			height: 250,
			bar: {groupWidth: '75%'},
			'chartArea': {top:20,'width': '70%', 'height': '80%'}
		};
		var chart = new google.visualization.BarChart(document.getElementById('top_performing_line_items'));
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Device', 'Type'],
		<?php foreach ($DeviceType as $key => $value) {?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_desktop'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Browser', 'Name'],
		<?php foreach ($browsers as $key => $value) {?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_windows'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['','Email', 'Desktop', 'Mobile', 'Co-Reg'],
		['',<?php echo $emailTotal; ?>,<?php echo $desktopTotal; ?>,<?php echo $mobileTotal; ?>,<?php echo $coregTotal; ?>]
		]);
		
		var options = {
			legend: { position: 'top', maxLines: 3 },
			colors: ["#3366CC", "#DC3912", "#FF9900", "#109618"],
			isStacked: true,
			'chartArea': {'width': '90%', 'height': '60%'}
		};
		
		var chart = new google.visualization.BarChart(document.getElementById('piechart_trending'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['OS', 'Family'],
		<?php foreach ($osfamily as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_ios'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["geochart"]});
	google.setOnLoadCallback(drawRegionsMap);
	
	
	function drawRegionsMap() {
		
		var data = google.visualization.arrayToDataTable([
		['Country', 'Popularity'],
		<?php foreach ($country as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			height: 200,
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
		
		chart.draw(data, options);
	}
</script>	
<script>
	var g1;
	var g2;
	var g3;
	document.addEventListener("DOMContentLoaded", function(event) {
		g1 = new JustGage({
			id: "chart_allocation",
			value:  <?php echo $accepted; ?>,
			min: 0,
			max: <?php echo $totalGoalLead; ?>,
			gaugeWidthScale: 0.8,
			title: "Total Allocation",
			counter: true,
			formatNumber: true,
			hideInnerShadow: true,
			levelColors: [
			"#00fff6",
			"#ff00fc",
			"#FC3B00"
			]
		});
	});
	</script>	
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Creative', 'Leads'],
		['N/A', 1]
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'},
			sliceVisibilityThreshold: 0,
			slices: {
            0: { color: 'grey' }
          }
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_cemail'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Creative', 'Leads'],
			['A', 35],
			['B', 65]
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_ccoreg'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Creative', 'Leads'],
			['A', 35],
			['B', 65]
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_creative'));
		
		chart.draw(data, options);
	}
</script>