<?php
	/**
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Pages
		* @since         CakePHP(tm) v 0.10.0.1076
	*/
	
	if (!Configure::read('debug')):
	throw new NotFoundException();
	endif;
	
	App::uses('Debugger', 'Utility');
?>
<style>
	.font-hg {
	color:#000 !important;
	font-size:48px !important;
	font-weight:bold !important;
	}
	.font-grey-mint {
	color:#000 !important;
	}
	.font-sm {
	font-size:16px !important;
	}
</style>
<div class="row">
	<div class="col-md-4">
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="/">Home</a><i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="/orders/view_line_items/<?php echo $li_order_id; ?>/">Line Items</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				<a href="#"><?php echo $lineItem_name; ?></a>
			</li>
		</ul>
	</div>
	
	 
	<div class="col-md-4">
		<div align="center">
		<a href="http://digitaladvertising.systems/orders/view_user_profile/78/">View Audience Data</a></div>
	</div>
	<div class="col-md-4">
		<div class="portlet-title" style="float:right;" >
			<div class="actions">
				<div class="btn-group btn-group-devided" data-toggle="buttons">
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "day" ? "active" : "" ?>">
					<input type="radio" name="options" onchange="chart('day')" class="toggle" id="option1">Today</label>
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "week" ? "active" : "" ?>">
					<input type="radio" name="options"  onchange="chart('week')" class="toggle" id="option2">Last 7 Days</label>
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "month" ? "active" : "" ?>">
					<input type="radio" name="options"  onchange="chart('month')" class="toggle" id="option3">MTD</label>
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "year" ? "active" : "" ?>">
					<input type="radio" name="options"  onchange="chart('year')" class="toggle" id="option2">YTD</label>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE BREADCRUMB -->

<div class="clearfix"> </div>
	<div class="caption"style="margin-bottom:20px;">
		<span class="caption-subject font-green-sharp bold uppercase">Offer  Name : <?php echo $lineItem_name;?></span>
	</div>
<div class="clearfix"> </div>	
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<div class="col-sm-3" >
			<div class="list-group" style="font-size:13px;">
			  <a href="#" class="list-group-item disabled">
				Lead Totals
			  </a>
			  <a href="#" class="list-group-item">Today: <span class="textblue"><?php echo $LeadTotal['tblReportDay']['TotalLead']?> </span></a>
			  <a href="#" class="list-group-item">Last 7 Days: <span class="textblue"><?php echo $LeadTotal['tblReportDay']['TotalWeek']?></span></a>
			  <a href="#" class="list-group-item">This Month: <span class="textblue"><?php echo $LeadTotal['tblReportDay']['TotalMonth']?></span> </a>
			  <a href="#" class="list-group-item">This Year: <span class="textblue"><?php echo $LeadTotal['tblReportDay']['TotalYear']?></span></a>
			</div>
		</div>	
		<div class="col-sm-3" >
			<div class="list-group"  style="font-size:13px;">
			  <a href="#" class="list-group-item disabled">
				Performance Vs. Budgets
			  </a>
			 <a href="#" class="list-group-item">Today: <span class="textblue"><?php echo round($Performance['day_p_budget_a']);?> </span>(<?php  $css =($Performance['day_p_budget'] < 0)?'textred':'textgreen'; echo '<span class="'.$css.'">'.round($Performance['day_p_budget']).'</span>';?>)</a> 
			 
			 <a href="#" class="list-group-item">Week: <span class="textblue"><?php echo round($Performance['week_p_budget_a']);?> </span>(<?php  $css =($Performance['week_p_budget'] < 0)?'textred':'textgreen'; echo '<span class="'.$css.'">'.round($Performance['week_p_budget']).'</span>';?>)</a> 
			 
			 <a href="#" class="list-group-item">Month: <span class="textblue"><?php echo round($Performance['month_p_budget_a']);?> </span>(<?php  $css =($Performance['month_p_budget'] < 0)?'textred':'textgreen'; echo '<span class="'.$css.'">'.round($Performance['month_p_budget']).'</span>';?>)</a> 
			 
			 <a href="#" class="list-group-item">Year: <span class="textblue"><?php echo round($Performance['year_p_budget_a']);?> </span>(<?php  $css =($Performance['year_p_budget'] < 0)?'textred':'textgreen'; echo '<span class="'.$css.'">'.round($Performance['year_p_budget']).'</span>';?>)</a> 
			  
			 
			</div>
		</div>
		 
		<div class="col-sm-6">	
			 	 
				<div id="chart_email_message" class="chart" style="height:200px;" ></div>
			 
		</div>
		
						
	
		<div class="col-md-12" style="margin-bottom:20px;"> 
			
			 <?php echo $this->Form->create('Order',array('url'=>array('controller'=>'orders','action'=>'view_detail_lead_gen_stats',$line_item_id),'type' => 'file','class'=>'form-inline'));?>
				<label for="exampleInputName2" style="margin-right:30px;">Filter By:   </label>
				<div class="form-group" style="margin-right:30px;">
					<label for="exampleInputName2" >ISP</label>
					<?php echo $this->Form->input('isp', array(
					  'class' => 'js-example-data-array-categories js-isp form-control',
					  'label'=>false,
					  'div'=>false,
					  'type'=>'select',
					  'onchange'=>'submitisp(this.value)',
					  'empty' => 'Select ISP',
					  'default' => $isp_request,
					  
					  'options' => $isp													   
				  ));?>
				</div>
				<div class="form-group" style="margin-right:30px;">
					<label for="exampleInputEmail2">Demographic Data </label>
					<?php echo $this->Form->input('demographic_data', array(
					  'class' => 'js-example-data-array-categories js-isp form-control',
					  'label'=>false,
					  'type'=>'select',
					  'div'=>false,
					  'onchange'=>'submitdemographic(this.value)',
					  'empty' => 'Select Demographic Data ',
					   'default' => $demograpic_request,
					   'options' => $demographic_data													   
				  ));?>
				</div>
				<div class="form-group" style="margin-right:30px;">
					<label for="exampleInputEmail2">Source</label>
					<?php echo $this->Form->input('source', array(
					  'class' => 'js-example-data-array-source js-isp form-control',
					  'label'=>false,
					  'div'=>false,
					  'type'=>'select',
					  'onchange'=>'submitsource(this.value)',
					  'empty' => 'Select Source Data ',
					  'default' => $source_request,
					  'options' => $source_data													   
				  ));?>
				</div>
			<?php echo $this->Form->end(); ?>
			
			 
		 
		<div class="clearfix"> </div>
	</div>
	<?php echo $this->Form->end(); ?>
	<div class="col-md-6">
		 	<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption caption-md">
						<i class="fa fa-dollar font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">TOP PERFORMING SOURCES</span>
						<span class="caption-helper hide">weekly stats...</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="uppercase font-hg font-red-flamingo">
								<div id="piechart_trending" class="gauge"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-user font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">Users</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="col-md-12 col-sm-12">
							<div class="font-grey-mint font-sm">
								# of Subscribers
							</div>
							<div class="uppercase font-hg font-red-flamingo">
								<?php echo "0" ?> <span class="font-lg font-grey-mint"></span>
							</div>
							<div id="regions_div" style="width: 100%; height: 200px;"></div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="font-grey-mint font-sm">
								DAILY SUBSCRIPTION TRENDS
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-user font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">ENGAGEMENT BY DEVICE</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="col-md-12 col-sm-12">
							 
							<div id="engagement_device" style="width: 100%; height: 200px;"></div>
						</div>
						<div class="col-md-12 col-sm-12">
							 
							
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="col-md-6">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			 <div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-bar-chart-o font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">TOP PERFORMIMING AD UNITS</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="uppercase font-hg font-red-flamingo">
								<div id="top_performing_line_items" width="100%" height="250"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-bar-chart-o font-grey-silver"></i>
						<span class="caption-subject font-grey-silver bold uppercase">A/B Creative Trending</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row list-separated">
						<div class="row list-separated">
						<div class="col-md-1 col-sm-1 col-xs-12">
						</div>
							<div class="col-md-3 col-sm-3 col-xs-12">
								<div class="font-grey-mint font-sm">
									Email
								</div>
								<div id="piechart_cemail" style="width: 100%;"></div>
							</div>
							<div class="col-md-3 col-sm-3  col-xs-12">
								<div class="font-grey-mint font-sm">
									Coreg
								</div>
								<div id="piechart_ccoreg" style="width: 100%;"></div>
							</div>
							<div class="col-md-3 col-sm-3  col-xs-12">
								<div class="font-grey-mint font-sm">
									Creatives
								</div>
								<div id="piechart_creative" style="width: 100%;"></div>
							</div>
							<div class="col-md-1 col-sm-1 col-xs-12">
						</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row list-separated">
				 <div class="portlet box grey-gallery">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-desktop font-grey-silver"></i>
							<span class="caption-subject font-grey-silver bold uppercase">Technology</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row list-separated">
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="font-grey-mint font-sm">
									Device Type
								</div>
								<div id="piechart_desktop" style="width: 100%;"></div>
							</div>
							<div class="col-md-4 col-sm-4  col-xs-12">
								<div class="font-grey-mint font-sm">
									Browsers
								</div>
								<div id="piechart_windows" style="width: 100%;"></div>
							</div>
							<div class="col-md-4 col-sm-4  col-xs-12">
								<div class="font-grey-mint font-sm">
									OS Family
								</div>
								<div id="piechart_ios" style="width: 100%;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				
			</div>
		</div>
	</div>
	
	<!-- END EXAMPLE TABLE PORTLET-->
	
	<div class="col-md-12"> 
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-desktop font-grey-silver"></i>
							<span class="caption-subject font-grey-silver bold uppercase">AUTO-RESPONDER STATISTICS</span>
						</div>
					</div>
			 
			<div class="portlet-body">
				 
				 <table class="table table-hover">
					 <tr>
						 <td>Total sent</td>
						 <td>Total Delivered</td>
						 <td>Total Opens</td>
						 <td>Total Unique Opens</td>
						 <td>Total Clicks</td>
						 <td>Total Unique Clicks</td>
						 <td>Complaints</td>
					 </tr>
					 <tr> 
						<td><?php echo $auto_responder_statistics['tblMailing']['TotalSent']; ?></td>
						<td><?php echo $auto_responder_statistics['tblMailing']['TotalDelivered']; ?>(<?php percent($auto_responder_statistics['tblMailing']['TotalDelivered'],$auto_responder_statistics['tblMailing']['TotalSent']); ?>%)</td>
						<td><?php echo $auto_responder_statistics['tblMailing']['TotalOpen']; ?>(<?php percent($auto_responder_statistics['tblMailing']['TotalOpen'],$auto_responder_statistics['tblMailing']['TotalSent']); ?>%)</td>
						<td><?php echo $auto_responder_statistics['tblMailing']['TotalUOpen']; ?>(<?php percent($auto_responder_statistics['tblMailing']['TotalUOpen'],$auto_responder_statistics['tblMailing']['TotalSent']); ?>%)</td>
						<td><?php echo $auto_responder_statistics['tblMailing']['TotalClick']; ?></td>
						<td><?php echo $auto_responder_statistics['tblMailing']['TotalUClick']; ?>(<?php percent($auto_responder_statistics['tblMailing']['TotalUClick'],$auto_responder_statistics['tblMailing']['TotalSent']); ?>%)</td>
						<td>$<?php echo $auto_responder_statistics['tblMailing']['TotalComplaint']; ?></td>
					 </tr>	
					</table>
						 
			</div>
		</div>
	</div>	
	
	<div class="col-md-6">
		<?php 
		if(!empty($email_performace_report)){?>
		<h3 id="hide-header-table">Email domain performance </h3>
		<table class="table table-hover">
		 <tr>
			 <th>Domain</th><th> Email<th>Bounces</th><th>Opens</th><th>Clicks</th><th>Unsubs</th>
		 </tr>
			<?php
			 // get total email	
			 $total_email =0;
			 foreach($email_performace_report as $key=>$value)
					$total_email +=$value['tblIspReport']['TotalEmail'];
			?>
	
			
			<?php foreach($email_performace_report as $key=>$value){?>
				<tr>
					<td><?php echo  $value['tblIspReport']['Domain']; ?></td><td> <?php echo  $value['tblIspReport']['TotalEmail']; ?>(<?php percent($value['tblIspReport']['TotalEmail'],$total_email); ?>%)<td><?php echo  $value['tblIspReport']['TotalBounce']; ?>(<?php percent($value['tblIspReport']['TotalBounce'],$value['tblIspReport']['TotalEmail']); ?>%)</td><td><?php echo  $value['tblIspReport']['TotalOpen']; ?>(<?php percent($value['tblIspReport']['TotalOpen'],$value['tblIspReport']['TotalEmail']); ?>%)</td><td><?php echo  $value['tblIspReport']['TotalClick']; ?>(<?php percent($value['tblIspReport']['TotalClick'],$value['tblIspReport']['TotalEmail']); ?>0%)</td><td><?php echo  $value['tblIspReport']['TotalUnsub']; ?>(<?php percent($value['tblIspReport']['TotalUnsub'],$value['tblIspReport']['TotalEmail']); ?>%)</td>
				 </tr>	
			<?php }?>	
				
		
		  
		</table>
		<?php }?>	
			
	</div>
	
	<div class="col-md-6">
		 <div>

		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#clickPerformance" aria-controls="home" role="tab" data-toggle="tab">Click Performance</a></li>
			<li role="presentation"><a href="#clickMap" aria-controls="profile" role="tab" data-toggle="tab">Click Map</a></li>
		  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="clickPerformance">
				<table class="table table-hover">
				 <tr>
					 <th>URL</th><th> Total Click</th>
				 </tr>
				 <tr>	
					 <td> www.google.com</td><td>1</td>
				 </tr>
				 <tr>
					 <td> www.facebook.com</td><td>10</td>
				 </tr>
				 <tr>
					 <td> www.yahoo.com</td><td>10</td>
				 </tr>		
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="clickMap">
				<table class="table table-hover">
				  <tr>
					 <th>URL</th><th> Total Click</th>
				 </tr>
				 <tr>	
					 <td> www.facebook.com</td><td>1</td>
				 </tr>
				 <tr>
					 <td> www.google.com</td><td>10</td>
				  </tr>
				 <tr>	
					<td> www.savemoneyindia.com</td><td>10</td>
				 </tr>		
				</table>
			</div>
		  </div>

		</div>
 
	</div>
</div>
</div>
<!-- END PAGE CONTENT INNER -->

<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>

<!-- END PAGE CONTENT INNER -->
<script>
	
	var ChartsFlotcharts1 = function() {
		
		return {
			//main function to initiate the module
			initCharts: function() {
				
				if (!jQuery.plot) {
					return;
				}
				
				var data = [];
				var totalPoints = 250;
				
				//Interactive Chart
				
				function chart_email_message() {
					if ($('#chart_email_message').size() != 1) {
						return;
					}
					
					function randValue() {
						return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
					}
					var email = [
					<?php foreach($tblReportDayTrendingByChannel as $data) { 
						if($data['tbmt']['mtype_name'] == "Email") {
						?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_accepted']; ?>],
					<?php } } ?>
					];
					var desktop = [
					<?php foreach($tblReportDayTrendingByChannel as $data) { 
						if($data['tbmt']['mtype_name'] == "Desktop") {
						?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_accepted']; ?>],
					<?php } } ?>
					];
					var mobile = [
					<?php foreach($tblReportDayTrendingByChannel as $data) { 
						if($data['tbmt']['mtype_name'] == "Mobile") {
						?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_accepted']; ?>],
					<?php } } ?>
					];
					var coreg = [
					<?php foreach($tblReportDayTrendingByChannel as $data) { 
						if($data['tbmt']['mtype_name'] == "Lead-Gen") {
						?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblReportDay']['rd_date']); ?>,<?php echo $this->Time->format('m',$data['tblReportDay']['rd_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblReportDay']['rd_date']); ?>)).getTime(), <?php echo $data[0]['rd_accepted']; ?>],
					<?php } } ?>
					];
					
					
					var plot = $.plot($("#chart_email_message"), [{
						data: email,
						label: "Email",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:0
						}, {
						data: desktop,
						label: "Desktop",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:1
						}, {
						data: mobile,
						label: "Mobile",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:2
						} , {
						data: coreg,
						label: "Co-Reg",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:3
						}], {
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
								fillColor: {
									colors: [{
										opacity: 0.05
										}, {
										opacity: 0.01
									}]
								}
							},
							points: {
								show: true,
								radius: 3,
								lineWidth: 1
							},
							shadowSize: 2
						},
						grid: {
							hoverable: true,
							clickable: true,
							tickColor: "#eee",
							borderColor: "#eee",
							borderWidth: 1
						},
						colors: ["#3366CC", "#DC3912", "#FF9900", "#109618"],
						xaxis: {
							mode: "time",
							tickSize: [1, "day"],    
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						yaxis: {
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						legend:{         
							placement: 'outsideGrid',
							container:$("#legendContainer_email_message"),    
							labelFormatter: function(label, series){
								return '<button data-toggle="button" onclick="togglePlot('+series.idx+');" class="btn btn-default ' + ((series.lines.show) ? "active" : "") + '" type="button" aria-pressed="false"> '+label + ((series.lines.show) ? " <span style=\"margin-left: 5px; font-size: 10px;\">&#10006;</span>" : "")+'</button>';
							},
							noColumns: 0
						}
					});
					
					function gd(date_c) {
						var date = Date.parse(date_c);
						return date.toString('dd-MMM-yyyy');
					}
					
					function showTooltip(x, y, contents) {
						$('<div id="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 15,
							border: '1px solid #333',
							padding: '4px',
							color: '#fff',
							'border-radius': '3px',
							'background-color': '#333',
							opacity: 0.80
						}).appendTo("body").fadeIn(200);
					}
					
					var previousPoint = null;
					$("#chart_2").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x);
						$("#y").text(pos.y);
						
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;
								
								$("#tooltip").remove();
								var x = item.datapoint[0],
								y = item.datapoint[1];
								showTooltip(item.pageX, item.pageY, item.series.label + " on " + convertTimestamp(x) + " is " + y);
							}
							} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
					
					togglePlot = function(seriesIdx)
					{
						var someData = plot.getData();  
						someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;  
						if (!someData[seriesIdx].lines.show){
							someData[seriesIdx].tempData = someData[seriesIdx].data;
							someData[seriesIdx].data = [];
						}
						else
						{
							someData[seriesIdx].data = someData[seriesIdx].tempData;
						}
						plot.setData(someData);
						plot.setupGrid();
						plot.draw();
					}
					
					
					function convertTimestamp(timestamp) {
						var nextDay = new Date(timestamp); // 86400000 - one day in ms 
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
						'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						return months[nextDay.getUTCMonth()] + ' ' + nextDay.getUTCDate();
					}
				}
				
				 //graph
				chart_email_message();
				///chart_email_quality();
				
			}
		};
		
	}();
	
	function chart(arg){ 
		window.location = '<?php echo Router::url(array('controller' => 'orders', 'action' => 'view_detail_lead_gen_stats',$line_item_id));?>/'+ arg+'/<?php echo (!empty($isp_request))?$isp_request:'null';?>'+'/<?php echo (!empty($demograpic_request))?$demograpic_request:'null';?>'+'/<?php echo (!empty($source_request))?$source_request:'null';?>';
		 
	}
	
	 
</script>
<!-- CSS -->
<style type="text/css">
	#legendContainer {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
	
	#legendContainer_email_message {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer_email_message .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
	
	#legendContainer_email_quality {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer_email_quality .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
</style>	
<script>
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualization);
	
	function drawVisualization() {
		// TOP PERFORMIMING AD UNITS		
		 var data = google.visualization.arrayToDataTable([
		['Unit', 'Revenue'],
		<?php foreach($tblReportBySources as $data) { ?>
			['<?php echo $data['tba']['adunit_name']; ?>', <?php echo ($data[0]['rd_accepted'] * $li_rate); ?>],
		<?php } ?>
		]);
		
		var options = {
			height: 250,
			bar: {groupWidth: '75%'},
			'chartArea': {top:20,'width': '70%', 'height': '80%'}
		};
		var chart = new google.visualization.BarChart(document.getElementById('top_performing_line_items'));
		chart.draw(data, options);
		
		//engagement device
	
		
		 var engagement_device_data = google.visualization.arrayToDataTable([
			['Source', 'Records']	
			<?php 
			$device_explode =array();
			if(!empty($DeviceType)){
				echo ',';  
				foreach($DeviceType as $key=>$value){
					$device_explode[]= "['".$key."',  ".$value."]";
				}
			}
			if(!empty($device_explode)){
				echo implode(",",$device_explode);	
			}?>
		  ]);

		  var engagement_device_options = {
			chartArea: {width: '50%'},
			 
			
		  };

		  var engagement_device_chart = new google.visualization.BarChart(document.getElementById('engagement_device'));

		 engagement_device_chart.draw(engagement_device_data, engagement_device_options);	
		
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Device', 'Type'],
		<?php foreach ($DeviceType as $key => $value) {?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_desktop'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Browser', 'Name'],
		<?php foreach ($browsers as $key => $value) {?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_windows'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['','Email', 'Desktop', 'Mobile', 'Co-Reg'],
		['',<?php echo $emailTotal; ?>,<?php echo $desktopTotal; ?>,<?php echo $mobileTotal; ?>,<?php echo $coregTotal; ?>]
		]);
		
		var options = {
			legend: { position: 'top', maxLines: 3 },
			colors: ["#3366CC", "#DC3912", "#FF9900", "#109618"],
			isStacked: true,
			'chartArea': {'width': '90%', 'height': '60%'}
		};
		
		var chart = new google.visualization.BarChart(document.getElementById('piechart_trending'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['OS', 'Family'],
		<?php foreach ($osfamily as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_ios'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["geochart"]});
	google.setOnLoadCallback(drawRegionsMap);
	
	
	function drawRegionsMap() {
		
		var data = google.visualization.arrayToDataTable([
		['Country', 'Popularity'],
		<?php foreach ($country as $key => $value) { ?>
			['<?php echo $key; ?>', <?php echo $value; ?>],
		<?php } ?>
		]);
		
		var options = {
			height: 200,
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
		
		chart.draw(data, options);
	}
</script>	 
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Creative', 'Leads'],
		['N/A', 1]
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'},
			sliceVisibilityThreshold: 0,
			slices: {
            0: { color: 'grey' }
          }
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_cemail'));
		
		chart.draw(data, options);
	}percent
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Creative', 'Leads'],
			['A', 35],
			['B', 65]
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_ccoreg'));
		
		chart.draw(data, options);
	}
</script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		
		var data = google.visualization.arrayToDataTable([
		['Creative', 'Leads'],
			['A', 35],
			['B', 65]
		]);
		
		var options = {
			'chartArea': {'width': '100%', 'height': '80%'}
		};
		
		var chart = new google.visualization.PieChart(document.getElementById('piechart_creative'));
		
		chart.draw(data, options);
	}
	// CHANGE  drop down
	
	function submitisp(val){
		window.location =  '<?php echo  Router::url(array('controller'=>'orders','action' => 'view_detail_lead_gen_stats',$line_item_id,$duration));?>/'+val+'/null'+'/null';
		 
	} 
	function submitdemographic(val){
		window.location = '<?php echo  Router::url(array('controller'=>'orders','action' => 'view_detail_lead_gen_stats',$line_item_id,$duration));?>'+'/null/'+val+'/null';
		 
	} 
	function submitsource(val){
		window.location = '<?php echo  Router::url(array('controller'=>'orders','action' => 'view_detail_lead_gen_stats',$line_item_id,$duration));?>'+'/null'+'/null/'+val;
		 
	} 
	
</script>
<?php 
	function percent($num_amount, $num_total) {
		if(empty($num_total)) {echo  "0"; return true;};
		$count1 = $num_amount / $num_total;
		$count2 = $count1 * 100;
		echo  number_format($count2, 2);
		return true;
	}

?>
