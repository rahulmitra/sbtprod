<!-- BEGIN PAGE CONTENT INNER -->
<style>
th, td{
    text-align: center;
}
</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Adunit Reports</span>
                </div>                
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <?php echo $this->Form->create(false, array('url' => array('controller' => 'orders', 'action' => 'adunit_reports'), 'class' => 'form-horizontal', 'id' => 'reportsForm')); ?>
                    <?php echo $this->Form->hidden('submit_value', array('value' => 'submit')); ?>
                    <div class="row">

                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('company_id', array(
                                    'class' => 'js-company-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Select Company',
                                    'options' => $publishers
                                ));
                                ?>
                            </div>
                        </div>
						 <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('media_id', array(
                                    'class' => 'js-media-array form-control',
                                    'label' => false,
                                    'type' => 'select',
									'multiple' => true,
									'onchange' => 'ShowProductType(this.value)',
                                    'empty' => 'Select Media Name',
                                    'options' => $mediatypelist
                                ));
                                ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('product_id', array(
                                    'class' => 'js-producttype-array form-control',
                                    'label' => false,
                                    'type' => 'select',
									'multiple' => true,
                                    'id' => 'producttype_content',
									'empty' => 'Select Product type'
									));
                                ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('cost_id', array(
                                    'class' => 'js-coststructure-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => 'Select Cost Structure',
                                    'options' => $costStructure
                                ));
                                ?>
                            </div>
							
							
                        </div>
                       
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php echo $this->Form->input('daterange', array('label' => false, 'div' => false, 'class' => 'form-control', 'style'=>'padding-right: 0;','placeholder' => 'Date Range')); ?> 	<span></span>


                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="btn-group">
                                <button type="submit" class="btn blue btn-secondary">Filter</button> 
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
				
				 <div class="table-responsive1">
				 
				 <?php 
						if (!empty($tblAdunits)) {
						foreach ($tblAdunits as $tblAdunit) { 
						if ($tblAdunit['tbmt']['mtype_id'] == "2") {
						?>
					<table class="table table-striped table-bordered table-hover" id="reports" style="margin-bottom:40px;">
						<thead style="background-color: rgb(102, 102, 102);color:#fff;text-align:center;">
                            <tr>
								<th>Ad Unit Details</th>
								<th>Sent</th>
								<th>Delivered</th>
								<th>Opens</th>
								<th>Clicks/CTR</th>
								<th>Conversions</th>
								<th>Revenue</th>
								<th>eCPM<br><span style="font-size: 11px;">Sends</span></th>
								<th>eCPM<br><span style="font-size: 11px;">Opens</span></th>
								<th>eCPC</th>
								<th>Actions</th>
							</tr>
						</thead>
						
							<tr>
								<td>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th colspan="2">
											<?php
											
                                            echo $this->Html->link($tblAdunit['tblAdunit']['adunit_name'], array('controller' => 'inventory_management', 'action' => 'view_stats_email', $tblAdunit['tblAdunit']['ad_uid']), array('escape' => false));
                                        
										?>
											</th>
										</tr>
									</thead>
										<tr>
											<td> Total List Size</td>
											<td><?php echo empty($tblAdunit['tblAdunit']['ad_list_size']) ? "N/A" : $tblAdunit['tblAdunit']['ad_list_size'] ?></td>
										</tr>
										<tr>
											<td> Frequency</td>
											<td><span style="color:green;font-weight:600;">M T W T F</span> <span style="color:red;font-weight:600;">S S</span></td>
										</tr>
										<tr>
											<td> Ad Unit Desc</td>
											<td><?php echo empty($tblAdunit['tblAdunit']['ad_desc']) ? "N/A" : $tblAdunit['tblAdunit']['ad_desc'] ?></td>
										</tr>
										<tr>
											<td>Active Flights</td>
											<td>5</td>
										</tr>
								</table>
								
								</td>
								<td> 
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>
											
											<?php echo empty($tblAdunit[0]['totalsent']) ? "0" : $tblAdunit[0]['totalsent'] ?>
											</th>
										</tr>
									</thead>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
								
								</table>	
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
												<?php echo empty($tblAdunit[0]['delivered']) ? "0" : $tblAdunit[0]['delivered'] ?>
												</th>
											</tr>
										</thead>
											<tr><td>
											<?php 
											if(!empty($tblAdunit[0]['totalsent'])){
											$totalsent = $tblAdunit[0]['totalsent'];
											$delivered = $tblAdunit[0]['delivered'];
											$delper = ($delivered *100)/$totalsent;
											echo number_format($delper,2)."%";
											}else{
											echo "0%";
											}
											?>
											</td></tr>
											<tr><td><i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>2.50%</td></tr>
											<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:90,129,102,48,87,117,111,132,90,99,45,63,87,102,120,120,69,48,63,18"></td></tr>
											<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>
											<?php echo empty($tblAdunit[0]['opens']) ? "0" : $tblAdunit[0]['opens'] ?>
											</th>
										</tr>
									</thead>
										<tr><td>
										<?php
										if(!empty($tblAdunit[0]['totalsent'])){
											$totalsent = $tblAdunit[0]['totalsent'];
											$opens = $tblAdunit[0]['opens'];
											$opensper = ($opens *100)/$totalsent;
											echo number_format($opensper,2)."%";
											}else{
											echo "0%";
											}
											?>
										</td></tr>
										<tr><td><i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>1.19%</td></tr>
										<tr><td><img src="http://chart.apis.google.com/chart?chs=90x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:90,129,102,48,87,117,111,132,90,99,45,63,87,102,120,120,69,48,63,18"></td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
												<?php echo empty($tblAdunit[0]['clicks']) ? "0" : $tblAdunit[0]['clicks'] ?>
												</th>
											</tr>
										</thead>
											<tr><td>
											<?php
											if(!empty($tblAdunit[0]['totalsent'])){
											$totalsent = $tblAdunit[0]['totalsent'];
											$clicks = $tblAdunit[0]['clicks'];
											$clicksper = ($clicks *100)/$totalsent;
											echo number_format($clicksper,2)."%";
											}else{
											echo "0%";
											}
											?>
											</td></tr>
											<tr><td><i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>14.90%</td></tr>
											<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:90,129,102,48,87,117,111,132,90,99,45,63,87,102,120,120,69,48,63,18"></td></tr>
											<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>
											No Pixel
											</th>
										</tr>
									</thead>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>
											<?php echo empty($tblAdunit['tblReportDay']['rd_revenue']) ? "N/A" : "$".$tblAdunit['tblReportDay']['rd_revenue'] ?>
											</th>
										</tr>
									</thead>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>
											<?php
											if(!empty($tblAdunit[0]['totalsent'])&& !empty($tblAdunit['tblReportDay']['rd_revenue'])){
											$totalsent = $tblAdunit[0]['totalsent'];
									$revenue = $tblAdunit['tblReportDay']['rd_revenue'];
											$revenuesent = ($revenue *1000)/$totalsent;
											echo "$".number_format($revenuesent,2);
											}else{
											echo "$0";
											}
											?>
											</th>
										</tr>
									</thead>
										<tr><td>$0.15</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>
												<?php
											if(!empty($tblAdunit[0]['opens'])&& !empty($tblAdunit['tblReportDay']['rd_revenue'])){
											$opens = $tblAdunit[0]['opens'];
									$revenue = $tblAdunit['tblReportDay']['rd_revenue'];
											$revenueopen = ($revenue *1000)/$opens;
											echo "$".number_format($revenueopen,2);
											}else{
											echo "$0";
											}
											?>
											</th>
										</tr>
									</thead>
										<tr><td><i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>0.12%</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>
												<?php
											if(!empty($tblAdunit[0]['clicks'])&& !empty($tblAdunit['tblReportDay']['rd_revenue'])){
											$clicks = $tblAdunit[0]['clicks'];
									$revenue = $tblAdunit['tblReportDay']['rd_revenue'];
											$revenueclick = $revenue/$clicks;
											echo "$".number_format($revenueclick,2);
											}else{
											echo "$0";
											}
											?>
											</th>
										</tr>
									</thead>
										<tr><td>$0.02</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
								<table class="table table-striped table-bordered table-hover">
									
										<tr>
											<td>
											
											<?php echo $this->Html->link('Edit Ad Unit', array('controller' => 'inventory_management', 'action' => 'editAdUnit', $tblAdunit['tblAdunit']['adunit_id'])); ?>
											</td>
										</tr>
								
										<tr><td><a href="#">View Flights</a></td></tr>
										<tr><td><a href="#">Add Flights</a></td></tr>
										<tr><td><a href="#">View Audience Profile</a></td></tr>
										<tr><td><a href="#">View Avails & STR Report</a></td></tr>
									</table>
								</td>
							</tr>
							
					</table>
					<?php } }}?>
				</div>	

                <div class="table-responsive1">
                    <table class="table table-striped table-bordered table-hover" id="reports">
                        <thead>
                            <tr>

                                <th>
                                    <?php echo $this->Paginator->sort('adunit_name', 'Adunit Name/  Line item Pricing Model/ Line item Status 
									 <span class="sortingreport"> </span>', array('escape' => false)); ?>  

                                </th>
                              
                               
                                <th>
                                    <?php echo $this->Paginator->sort('li_rate', 'Click/Takes<span class="sortingreport"> </span>', array('escape' => false)); ?>

                                </th>
                                <th>Takes & Take Rate</th>
                                <th>Imp & eCPM</th>
                              
                                <th>Accepted / Pending </th>


                                <th width="29%">AUTO-RESPONDERS <br/>
                                    <span class="col-lg-2" style="font-size: 10px;">Opens</span>
                                    <span class="col-lg-2" style="font-size: 10px;">Clicks</span>
                                    <span class="col-lg-2" style="font-size: 10px;">Canceled</span>
                                    <span class="col-lg-2" style="font-size: 10px;">Bounced</span>
                                    <span class="col-lg-2" style="font-size: 10px;">Complaints</span>
                                    <span class="col-lg-2" style="font-size: 10px;">Rejected</span>

                                </th>
                            </tr>
                        </thead>
                        <?php
                        if (!empty($tblAdunits)) {
                            $csnameArray = array(1 => '<span style="color:red;font-weight:bold;">{CSNAME}</span>', 2 => '<span style="color:red;font-weight:bold;">{CSNAME}</span>', 3 => '<span style="color:#FF9934;font-weight:bold;">{CSNAME}</span>', 4 => '<span style="color:green;font-weight:bold;">{CSNAME}</span>', 5 => '<span style="color:#FF9934;font-weight:bold;">{CSNAME}</span>', 6 => '<span style="color:#BB99AA;font-weight:bold;">{CSNAME}</span>');
                            $statusArray = array(1 => '<span style="color:red;font-weight:bold;">{STATUS}</span>', 2 => '<a style="margin-right:5px;text-decoration:underline;font-weight:bold;" href="{LINK}">Upload Creative</a>', 3 => '<span style="color:#FF9934;font-weight:bold;">{STATUS}</span>', 4 => '<span style="color:green;font-weight:bold;">{STATUS}</span>', 5 => '<span style="color:#FF9934;font-weight:bold;">{STATUS}</span>', 6 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 7 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 8 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 9 => '<a style="margin-right:5px;text-decoration:underline;font-weight:bold;" href="{LINK}">Create Flight</a>');
                            $dateformte = Configure::read('Php.dateformte');
                            foreach ($tblAdunits as $tblAdunit) {
                                ?>
                                <tr>

                                    <td><?php
                                        $lineitemLink = '';
                                        if ($tblAdunit['tbmt']['mtype_id'] == "2" || $tblAdunit['tbmt']['mtype_id'] == "5") {
                                            echo $this->Html->link($tblAdunit['tblAdunit']['adunit_name'], array('controller' => 'inventory_management', 'action' => 'view_stats_email', $tblAdunit['tblAdunit']['ad_uid']), array('escape' => false));
                                        } 
                                        //else if ($tblAdunit['tbmt']['mtype_id'] == "5") {
                                          //  echo $this->Html->link($tblAdunit['tblAdunit']['adunit_name'], array('controller' => 'inventory_management', 'action' => 'view_stats_leadgen', $tblAdunit['tblAdunit']['ad_uid']), array('escape' => false));
                                        //} 
                                        else {
                                            echo $tblAdunit['tblAdunit']['adunit_name'];
                                        }
                                        ?> 
                                        <br><?php echo $tblAdunit['tbmt']['mtype_name'].' -> '.  $tblAdunit['tbpt']['ptype_name'];  ?><br> 
                                    </td>
                                    <td>
                                        <?php echo '$' . $tblAdunit['0']['li_rate']; ?>
                                    </td>
                                    <td>
                                        <?php echo $tblAdunit['tblReportDay']['totaltake']; ?>
                                        <div><?php
                                            echo($tblAdunit['tblReportDay']['totaltake'] == 0) ? 0 : (
                                                    $tblAdunit['tblReportDay']['totaltake'] / ($tblAdunit['0']['ma_imp'] == 0) ? 1 : $tblAdunit['0']['ma_imp']);
                                            ?>%<img src="http://chart.apis.google.com/chart?chs=100x50&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($tblAdunit['tblReportDay']['takegraph']) ? implode(',', array_values($tblAdunit['tblReportDay']['takegraph'])) : 0); ?>"></div> 
                                    </td>
                                     <td>
                                        <div>
                                            <?php echo $tblAdunit['0']['ma_imp']; ?><br>

                                            <img src="http://chart.apis.google.com/chart?chs=100x50&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($tblAdunit['tblReportDay']['impgraph']) ? implode(',', array_values($tblAdunit['tblReportDay']['impgraph'])) : 0); ?>"></div>
											$<?php echo $ecpm = sprintf('%0.2f', ($tblAdunit['0']['li_revenue'] * 1000) / ($tblAdunit['0']['ma_imp'] == 0 ? 1 : $tblAdunit['0']['ma_imp'])); ?>
                                        <div>
											<img src="http://chart.apis.google.com/chart?chs=100x50&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($tblAdunit['tblReportDay']['eCPMgraph']) ? implode(',', array_values($tblAdunit['tblReportDay']['eCPMgraph'])) : 0); ?>">
										</div>

                                    </td>
                                    <td>
                                        <?php echo $tblAdunit['tblReportDay']['totalAccepted'];
                                        ?><br><span style="color:green;"> (<?php
                                        echo
                                        getPercent($tblAdunit['tblReportDay']['totaltake'], $tblAdunit['tblReportDay']['totalAccepted']);
                                        ?>%)</span><br><?php echo ($tblAdunit['tblReportDay']['totalPending']); ?><br><span style="color:green;"> (<?php
                                            echo
                                            getPercent($tblAdunit['tblReportDay']['totaltake'], $tblAdunit['tblReportDay']['totalPending']);
                                            ?>%)</span>
                                    </td>

                                    <td>

                                        <span class="col-lg-2" style="text-align: center;"><?php echo $tblAdunit[0]['opens']; ?><span style="display:block;"><?php echo getPercent($tblAdunit[0]['totalsent'], $tblAdunit[0]['opens']); ?>%</span></span> <span class="col-lg-2"  style="text-align: center;"><?php echo $tblAdunit[0]['clicks']; ?><span style="display:block;"><?php echo getPercent($tblAdunit[0]['totalsent'], $tblAdunit[0]['clicks']); ?>%</span></span> <span class="col-lg-2" style="text-align: center;"><?php echo $tblAdunit[0]['delivered']; ?><span style="display:block;"><?php echo getPercent($tblAdunit[0]['totalsent'], $tblAdunit[0]['delivered']); ?>%</span></span> <span class="col-lg-2"  style="text-align: center;"><?php echo $tblAdunit[0]['bounce']; ?><span style="display:block;"><?php echo getPercent($tblAdunit[0]['totalsent'], $tblAdunit[0]['bounce']); ?>%</span></span> <span class="col-lg-2"  style="text-align: center;"><?php echo $tblAdunit[0]['compain']; ?><span style="display:block;"><?php echo getPercent($tblAdunit[0]['totalsent'], $tblAdunit[0]['compain']); ?>%</span></span> 
                                        <span class="col-lg-2"  style="text-align: center;"><?php echo $tblAdunit['tblReportDay']['totalRejected']; ?><span style="display:block;"><?php echo getPercent($tblAdunit[0]['totalsent'], $tblAdunit['tblReportDay']['totalRejected']); ?>%</span></span> 
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tfoot>
                            <tr>
                                <td colspan="9">
                                    <?php if (!$tblAdunits) { ?>
                                        <div style='color:#FF0000'>No Record Found</div>
                                    <?php } else { ?>

                                        <ul class="pagination">

                                            <?php if ($this->Paginator->first()) { ?><li><?php echo $this->Paginator->first('« First', array('class' => '')); ?></li>
                                            <?php } ?>										
                                            <?php if ($this->Paginator->hasPrev()) { ?>
                                                <li><?php echo $this->Paginator->prev('< Previous', array('class' => ''), null, array('class' => 'disabled')); ?>&nbsp;  &nbsp;</li>
                                            <?php } ?>

                                            <?= $this->Paginator->numbers(array('modulus' => 6, 'tag' => 'li', 'class' => '', 'separator' => '')); ?>
                                            <?php if ($this->Paginator->hasNext()) { ?>

                                                <li><?php echo $this->Paginator->next('Next >', array('class' => '')); ?></li>
                                            <?php } ?>
                                            <?php if ($this->Paginator->last()) { ?>
                                                <li><?php echo $this->Paginator->last('Last »', array('class' => '')); ?></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>


                                </td>
                            </tr>
                        </tfoot>

                    </table>
                </div>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
    #sample_4_filter { 
        float:right;
    }
</style>
<script>

    jQuery(document).ready(function () {
        $('input[name="data[daterange]"]').daterangepicker();
        $(function () {
            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            cb(moment().subtract(29, 'days'), moment());
            $('input[name="data[daterange]"]').daterangepicker({
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

        });
        $(".js-company-array").select2({
            placeholder: "Select Company",
            maximumSelectionLength: 1,
            //allowClear: true
        });
        $(".js-producttype-array").select2({
            placeholder: "Select Product Type ",
			maximumSelectionLength: 1,
            //allowClear: true
        });
        $(".js-coststructure-array").select2({
            placeholder: "Select Price Format",
			maximumSelectionLength: 1,
            //allowClear: true
        });
        $(".js-media-array").select2({
            placeholder: "Select Media Type",
			maximumSelectionLength: 1,
            //allowClear: true
        });

    });
    function actionPlay(action, fl_id) {
        var id = '';
        if (action == 2) { // hide here play and show stop button
            var play_id = "play" + fl_id;
            var stop_id = "stop" + fl_id;
        } else {
            var play_id = "stop" + fl_id;
            var stop_id = "play" + fl_id;
        }

        $.ajax({
            type: "POST",
            url: "<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'update_flight_play_action'), true); ?>/" + action + "/" + fl_id,
            dataType: "json",
            success: function (data) {
                if (data == 1) {
                    $("#" + play_id).hide();
                    $("#" + stop_id).show();
                }
            }, error: function (data) {
                alert("There was an error. Try again please!");
            }
        });

    }

    $('#company_id').change(function () {
        // Remove all Manage Lead Allocation
        var favorite = [];
        var selectedItem = [];
        $.each($("#company_id :selected"), function () {
            favorite.push($(this).val());
        });
        $(".js-order-array").empty();
        $(".js-order-array").select2({
            placeholder: "Select Orders",
            allowClear: true
        });

        var selecid = favorite.join(",");

        //alert("My favourite sports are: " + favorite.join(", "));
        $.ajax({
            type: "POST",
            url: "<?php echo Router::url(array('controller' => 'orders', 'action' => 'getAllOrderList')); ?>",
            data: 'user_id=' + favorite.join(","),
            dataType: "json",
            success: function (data) {
                var count = 0;
                $(".js-order-array").select2({
                    data: data
                });
                $(".js-order-array").select2({
                    placeholder: "Select Orders",
                });

            }
        });

    });
	
	 function ShowProductType(value) {
	
		if (!value) {
            alert('Please select Media Tpye');
            return false;
        }
        $.ajax({
            type: "POST",
            url: "/orders/getproducttype_ajax/" + value,
            dataType: "json",
            success: function (data) {
				
                $(".js-producttype-array").empty();
                $(".js-producttype-array").select2({
                    data: data
                });
                
				$(".js-producttype-array").select2({
					placeholder: "Select Product Type ",
					maximumSelectionLength: 1,
				});
				
			$(".js-producttype-array").select2("open");
            }
        });
    }

</script>

<?php

function getprocessbar($process) {
    $color = 'red';
    if ($process >= 0 && $process < 50) {
        $color = 'red';
    } else if ($process >= 50 && $process < 75) {
        $color = '#EB661E';
    } else if ($process >= 75 && $process < 90) {
        $color = 'orange';
    } else if ($process >= 90 && $process <= 100) {
        $color = 'green';
    }

    return $color;
}

function getPercent($total, $goal) {

    if ($total == 0)
        return 0;
    return sprintf('%0.2f', ($goal / $total ) * 100);
}
?>


