<!-- BEGIN PAGE CONTENT INNER -->
<style>
	th, td{
    text-align: center;
	}
</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
					<?php if (!empty($adunit_id)){ ?>
                    <span class="caption-subject font-green-sharp bold uppercase">Flight Reports Of "<?php echo trim($adunit_id) ?>"</span>
					<?php }else{ ?>
					 <span class="caption-subject font-green-sharp bold uppercase">Flight Reports</span>
                <?php } ?>
				</div>

            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <?php echo $this->Form->create(false, array('url' => array('controller' => 'orders', 'action' => 'flight_reports'), 'class' => 'form-horizontal', 'id' => 'reportsForm')); ?>
                    <?php echo $this->Form->hidden('submit_value', array('value' => 'submit')); ?>
                    <div class="row">

                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('company_id', array(
                                    'class' => 'js-company-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Select Advertiser',
                                    'options' => $companylist
                                ));
                                ?>



                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('order_id', array(
                                    'class' => 'js-order-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Select Orders',
                                    'options' => $orderlist
                                ));
                                ?>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('status_id', array(
                                    'class' => 'js-satus-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Please Select Status',
                                    'options' => $status
                                ));
                                ?>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('media_id', array(
                                    'class' => 'js-media-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Please Select Media Name ',
                                    'options' => $medialist
                                ));
                                ?>

                            </div>


                        </div>
                        <div class="col-md-2">


                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php echo $this->Form->input('daterange', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Date Range', 'style' => 'padding-right: 0;')); ?> 	<span></span>


                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="btn-group">
                                <button type="submit" class="btn blue btn-secondary">Filter</button> 
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
				</div>
				
				<div class="table-responsive1">
				    <?php
                        if (!empty($flights)) {
                            $csnameArray = array(1 => '<span style="color:red;font-weight:bold;">{CSNAME}</span>', 2 => '<span style="color:red;font-weight:bold;">{CSNAME}</span>', 3 => '<span style="color:#FF9934;font-weight:bold;">{CSNAME}</span>', 4 => '<span style="color:green;font-weight:bold;">{CSNAME}</span>', 5 => '<span style="color:#FF9934;font-weight:bold;">{CSNAME}</span>', 6 => '<span style="color:#BB99AA;font-weight:bold;">{CSNAME}</span>');
                            $statusArray = array(1 => '<span style="color:red;font-weight:bold;">{STATUS}</span>', 2 => '<a style="margin-right:5px;text-decoration:underline;font-weight:bold;" href="{LINK}">Upload Creative</a>', 3 => '<span style="color:#FF9934;font-weight:bold;">{STATUS}</span>', 4 => '<span style="color:green;font-weight:bold;">{STATUS}</span>', 5 => '<span style="color:#FF9934;font-weight:bold;">{STATUS}</span>', 6 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 7 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 8 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 9 => '<a style="margin-right:5px;text-decoration:underline;font-weight:bold;" href="{LINK}">Create Flight</a>');
                            $dateformte = Configure::read('Php.dateformte');
                            foreach ($flights as $report) { 
							
								if ($report['tbmt']['mtype_id'] == "1" || $report['tbmt']['mtype_id'] == "4") {  ?>
								
								<table class="table table-striped table-bordered table-hover" id="reports" style="margin-bottom:40px;">
									<thead style="background-color: rgb(102, 102, 102);color:#fff;text-align:center;">
										<tr>
										<th></th>
											<th>Flight Details 
											<?php if ($report['tbmt']['mtype_id'] == "1") {  ?>
												(Desktop)
												<?php } else if ($report['tbmt']['mtype_id'] == "4") {  ?>
												(Social)
												<?php } else { } ?>
												</th>
											<th>Page Views</th>
											<th>Ad Impressions</th>
											<th>Viewable Impressions</th>
											<th>Clicks</th>
											<th>Conversions</th>
											<th>Ad Unit CTR</th>
											<th>Revenue</th>
											<th>Page eRPM</th>
											<th>Ad Unit eRPM</th>
											<th>Ad Unit eCPC</th>
											<th>Actions</th>
										</tr>
									</thead>
									
									<tr>
									
									<td>
											
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															
															<?php
																$stopstatus = $playstatus = "display:none;";
																
																if ($report['tblFlight']['status'] == 1) {
																	$stopstatus = "display:block;";
																	} else {
																	$playstatus = "display:block;";
																}
															?>
															<button style="<?php echo $stopstatus; ?>" id="play<?php echo $report['tblFlight']['fl_id']; ?>" onclick="return confirm('Are you sure want to pause?') ? actionPlay(2,<?php echo $report['tblFlight']['fl_id']; ?>) : '';"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></button> <button style="<?php echo $playstatus; ?>" id="stop<?php echo $report['tblFlight']['fl_id']; ?>" onclick="return confirm('Are you sure want to start?') ? actionPlay(1,<?php echo $report['tblFlight']['fl_id']; ?>) : '';"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover" >
												<thead>
												<tr>
													<th style="vertical-align: middle;">Flight</th>
													<th  style="text-align: left;"><?php echo $report['tblFlight']['fl_name']; ?></th></tr></thead>
												<tr>
												<td>Ad unit</td>
													<td  style="text-align: left;"><?php echo $report['tblAdunit']['adunit_name']; ?></td>
													</tr>
												<tr>
													<td>Line item</td>
													<td style="text-align: left;"><?php echo $report['tblp']['CompanyName'] . ' >> ' . $report['tblLineItem']['li_name']; ?></td></tr>
												<tr>
													<td style="text-align: left;">Date</td>
													<td style="text-align: left;"><?php echo (!empty($report['tblFlight']['fl_start'])) ? $this->Time->format($dateformte, $report['tblFlight']['fl_start']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?> / <?php echo (!empty($report['tblFlight']['fl_end'])) ? $this->Time->format($dateformte, $report['tblFlight']['fl_end']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?>
														
													</td>
												</tr>
												
											</table>
										</td>
										<td> 
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												
											</table>	
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>
													N/A
												</td></tr>
												<tr><td>N/A</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:90,129,102,48,87,117,111,132,90,99,45,63,87,102,120,120,69,48,63,18"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															
															<?php echo empty($report['tblReportDay']['rd_imp']) ? "0" : $report['tblReportDay']['rd_imp'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>
													N/A	
												</td></tr>
												<tr><td>
													<?php
														$open = 0;
														if(empty($report['tblReportDay']['rd_imp'])){
															$open = 0;
															}else{
															$open = $report['tblReportDay']['rd_imp'];		
														}
														$openlast = 0;	
														if(empty($report['tblReportDayLast']['rd_imp'])){
															$openlast = 0;
															}else{
															$openlast = $report['tblReportDayLast']['rd_imp']	;	
														}
														$openPer2 = 0;
														if($open == 0 && $openlast == 0){
															$openPer2 = 0;
															}else if ($openlast == 0){
															$openPer2 = 100;
															}else if($open == 0 ){
															$openPer2 = -100;
															}else{
															$V2 =  $open;
															$V1 = $openlast;
															$openPer2 = ((($V2 - $V1)/$V1)*100);
															
															$openPer = ((($open - $openlast)/(($open + $openlast)/2))*100);
															
														}
														echo $openlast."<br/>";
													?>
													<?php if($openPer2 > 0){ ?>
														<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
														<?php }else{ ?>
														<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
													<?php } ?>
													<?php  echo number_format($openPer2,2)."%"; ?>		
													
													
												</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=90x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:<?php echo (!empty($report['tblReportDay']['impgraph']) ? implode(',', array_values($report['tblReportDay']['impgraph'])) : 0); ?>"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($report['tblReportDay']['rd_accepted']) ? "0" : $report['tblReportDay']['rd_accepted'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>
													N/A
												</td></tr>
												<tr><td>
													
													<?php
														$take = 0;
														if(empty($report['tblReportDay']['rd_accepted'])){
															$take = 0;
															}else{
															$take = $report['tblReportDay']['rd_accepted'];		
														}
														$takelast = 0;	
														if(empty($report['tblReportDayLast']['rd_accepted'])){
															$takelast = 0;
															}else{
															$takelast = $report['tblReportDayLast']['rd_accepted']	;	
														}
														$takePer2 = 0;
														if($take == 0 && $takelast == 0){
															$takePer2 = 0;
															}else if ($takelast == 0){
															$takePer2 = 100;
															}else if($take == 0 ){
															$takePer2 = -100;
															}else{
															$V2 =  $take;
															$V1 = $takelast;
															$takePer2 = ((($V2 - $V1)/$V1)*100);
															
															$takePer = ((($take - $takelast)/(($take + $takelast)/2))*100);
															
														}
														echo $takelast."<br/>";
													?>
													<?php if($takePer2 > 0){ ?>
														<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
														<?php }else{ ?>
														<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
													<?php } ?>
													<?php  echo number_format($takePer2,2)."%"; ?>
													
												</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($report['tblReportDay']['takegraph']) ? implode(',', array_values($report['tblReportDay']['takegraph'])) : 0); ?>"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															No Pixel
															
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
														N/A
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
																<?php echo empty($report['tblReportDay']['rd_revenue']) ? "$0" : "$".$report['tblReportDay']['rd_revenue'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															&nbsp;
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										
										<td>
											<table class="table table-striped table-bordered table-hover">
												
												<thead>
													<tr>
														<th>
														<a style="margin-right:5px;" href="<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'edit_line_item_flight', $report['tblFlight']['fl_id'], $report['tblLineItem']['li_id'])); ?>"></i>Edit</a>
													</th>
												</tr>
												</thead>
												
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
									</tr>
								</table>
							
							<?php } else if ($report['tbmt']['mtype_id'] == "2") {  ?>
								
								<table class="table table-striped table-bordered table-hover" id="reports">
									<thead style="background-color: rgb(102, 102, 102);color:#fff;text-align:center;">
										<tr>
											<th></th>
											<th>Flight Details (Email)</th>
											<th>Sent</th>
											<th>Delivered</th>
											<th>Opens</th>
											<th>Clicks/CTR</th>
											<th>Conversions</th>
											<th>Revenue</th>
											<th>eCPM<br><span style="font-size: 11px;">Sends</span></th>
											<th>eCPM<br><span style="font-size: 11px;">Opens</span></th>
											<th>eCPC</th>
											<th>Actions</th>
										</tr>
									</thead>
									
									<tr>
										<td>
											
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															
															<?php
																$stopstatus = $playstatus = "display:none;";
																
																if ($report['tblFlight']['status'] == 1) {
																	$stopstatus = "display:block;";
																	} else {
																	$playstatus = "display:block;";
																}
															?>
															<button style="<?php echo $stopstatus; ?>" id="play<?php echo $report['tblFlight']['fl_id']; ?>" onclick="return confirm('Are you sure want to pause?') ? actionPlay(2,<?php echo $report['tblFlight']['fl_id']; ?>) : '';"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></button> <button style="<?php echo $playstatus; ?>" id="stop<?php echo $report['tblFlight']['fl_id']; ?>" onclick="return confirm('Are you sure want to start?') ? actionPlay(1,<?php echo $report['tblFlight']['fl_id']; ?>) : '';"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover" >
												<thead>
												<tr>
													<th style="vertical-align: middle;">Flight</th>
													<th  style="text-align: left;"><?php echo $report['tblFlight']['fl_name']; ?></th></tr></thead>
												<tr>
												<td>Ad unit</td>
													<td  style="text-align: left;"><?php echo $report['tblAdunit']['adunit_name']; ?></td>
													</tr>
												<tr>
													<td>Line item</td>
													<td style="text-align: left;"><?php echo $report['tblp']['CompanyName'] . ' >> ' . $report['tblLineItem']['li_name']; ?></td></tr>
												<tr>
													<td style="text-align: left;">Date</td>
													<td style="text-align: left;"><?php echo (!empty($report['tblFlight']['fl_start'])) ? $this->Time->format($dateformte, $report['tblFlight']['fl_start']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?> / <?php echo (!empty($report['tblFlight']['fl_end'])) ? $this->Time->format($dateformte, $report['tblFlight']['fl_end']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?>
														
													</td>
												</tr>
												
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($report[0]['totalsent'])? "0" : $report[0]['totalsent'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
											
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($report[0]['delivered']) ? "0" : $report[0]['delivered'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>
													<?php 
														if(!empty($report[0]['totalsent'])){
															$totalsent = $report[0]['totalsent'];
															$delivered = $report[0]['delivered'];
															$delper = ($delivered *100)/$totalsent;
															echo number_format($delper,2)."%";
															}else{
															echo "0%";
														}
													?>
												</td></tr>
												<tr><td>N/A</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:90,129,102,48,87,117,111,132,90,99,45,63,87,102,120,120,69,48,63,18"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($report['tblReportDay']['rd_imp']) ? "0" : $report['tblReportDay']['rd_imp'] ?>
															
														</th>
													</tr>
												</thead>
												<tr><td>
													<?php
														if(!empty($report[0]['totalsent'])){
															$totalsent = $report[0]['totalsent'];
															$opens = $report['tblReportDay']['rd_imp'];
															$opensper = ($opens *100)/$totalsent;
															echo number_format($opensper,2)."%";
															}else{
															echo "0%";
														}
													?>
												</td></tr>
												<tr><td>
													
													<?php
														$open = 0;
														if(empty($report['tblReportDay']['rd_imp'])){
															$open = 0;
															}else{
															$open = $report['tblReportDay']['rd_imp'];		
														}
														$openlast = 0;	
														if(empty($report['tblReportDayLast']['rd_imp'])){
															$openlast = 0;
															}else{
															$openlast = $report['tblReportDayLast']['rd_imp']	;	
														}
														$openPer2 = 0;
														if($open == 0 && $openlast == 0){
															$openPer2 = 0;
															}else if ($openlast == 0){
															$openPer2 = 100;
															}else if($open == 0 ){
															$openPer2 = -100;
															}else{
															$V2 =  $open;
															$V1 = $openlast;
															$openPer2 = ((($V2 - $V1)/$V1)*100);
															
															$openPer = ((($open - $openlast)/(($open + $openlast)/2))*100);
															
														}
														echo $openlast."<br/>";
													?>
													<?php if($openPer2 > 0){ ?>
														<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
														<?php }else{ ?>
														<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
													<?php } ?>
													<?php  echo number_format($openPer2,2)."%"; ?>		
													
												</td></tr>		
												
												<tr><td><img src="http://chart.apis.google.com/chart?chs=90x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:<?php echo (!empty($report['tblReportDay']['impgraph']) ? implode(',', array_values($report['tblReportDay']['impgraph'])) : 0); ?>"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
											
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($report['tblReportDay']['rd_accepted']) ? "0" : $report['tblReportDay']['rd_accepted'] ?>
														</th>
													</tr>
												</thead>
												<tr>
													<td>
														<?php
															if(!empty($report[0]['totalsent'])){
																$totalsent = $report[0]['totalsent'];
																$clicks = $report['tblReportDay']['rd_accepted'];
																$clicksper = ($clicks *100)/$totalsent;
																echo number_format($clicksper,2)."%";
																}else{
																echo "0%";
															}
														?></td></tr>
														
														<tr><td>
															<?php
																$accepted = 0;
																if(empty($report['tblReportDay']['rd_accepted'])){
																	$accepted = 0;
																	}else{
																	$accepted = $report['tblReportDay']['rd_accepted'];		
																}
																$acceptedlast = 0;	
																if(empty($report['tblReportDayLast']['rd_accepted'])){
																	$acceptedlast = 0;
																	}else{
																	$acceptedlast = $report['tblReportDayLast']['rd_accepted']	;	
																}
																$acceptedPer2 = 0;
																if($accepted == 0 && $acceptedlast == 0){
																	$acceptedPer2 = 0;
																	}else if ($acceptedlast == 0){
																	$acceptedPer2 = 100;
																	}else if($accepted == 0 ){
																	$acceptedPer2 = -100;
																	}else{
																	$V2 =  $accepted;
																	$V1 = $acceptedlast;
																	$acceptedPer2 = ((($V2 - $V1)/$V1)*100);
																	
																	$acceptedPer = ((($accepted - $acceptedlast)/(($accepted + $acceptedlast)/2))*100);
																	
																}
																echo $acceptedlast."<br/>";
															?>
															<?php if($acceptedPer2 > 0){ ?>
																<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
																<?php }else{ ?>
																<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
															<?php } ?>
															<?php  echo number_format($acceptedPer2,2)."%"; ?>
														</td></tr>
														<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($report['tblReportDay']['takegraph']) ? implode(',', array_values($report['tblReportDay']['takegraph'])) : 0); ?>"></td></tr>
														<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															No Pixel
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($report['tblReportDay']['rd_revenue']) ? "$0" : "$".$report['tblReportDay']['rd_revenue'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php
																if(!empty($report[0]['totalsent'])&& !empty($report['tblReportDay']['rd_revenue'])){
																	$totalsent = $report[0]['totalsent'];
																	$revenue = $report['tblReportDay']['rd_revenue'];
																	$revenuesent = ($revenue *1000)/$totalsent;
																	echo "$".number_format($revenuesent,2);
																	}else{
																	echo "$0";
																}
															?>
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php
																if(!empty($report[0]['opens'])&& !empty($report['tblReportDay']['rd_revenue'])){
																	$opens = $report[0]['opens'];
																	$revenue = $report['tblReportDay']['rd_revenue'];
																	$revenueopen = ($revenue *1000)/$opens;
																	echo "$".number_format($revenueopen,2);
																	}else{
																	echo "$0";
																}
															?>
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php
																if(!empty($report[0]['clicks'])&& !empty($report['tblReportDay']['rd_revenue'])){
																	$clicks = $report[0]['clicks'];
																	$revenue = $report['tblReportDay']['rd_revenue'];
																	$revenueclick = $revenue/$clicks;
																	echo "$".number_format($revenueclick,2);
																	}else{
																	echo "$0";
																}
															?>
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
														<a style="margin-right:5px;" href="<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'edit_line_item_flight', $report['tblFlight']['fl_id'], $report['tblLineItem']['li_id'])); ?>"></i>Edit</a>
													</th>
												</tr>
											</thead>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									
								</tr>
							</table>
							<?php } else if ($report['tbmt']['mtype_id'] == "5") {  ?>
							
							<table class="table table-striped table-bordered table-hover" id="reports">
								<thead style="background-color: rgb(102, 102, 102);color:#fff;text-align:center;">
									<tr>
										<th></th>
										<th>Flight Details (Lead-Gen)</th>
										<th>Page Views</th>
										<th>Registrations</th>
										<th>Offer Views</th>
										<th>Takes</th>
										<th>Leads<br><span style="font-size: 11px;">Accepted Rejected </span></th>
										<th>Revenue</th>
										<th>eRPM<br><span style="font-size: 11px;">Page Views</span></th>
										<th>eRPM<br><span style="font-size: 11px;">Offerviews</span></th>
										<th>RPR</th>
										<th>Actions</th>
									</tr>
								</thead>
								
								<tr>
									<td>
										
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
														
														<?php
															$stopstatus = $playstatus = "display:none;";
															
															if ($report['tblFlight']['status'] == 1) {
																$stopstatus = "display:block;";
																} else {
																$playstatus = "display:block;";
															}
														?>
														<button style="<?php echo $stopstatus; ?>" id="play<?php echo $report['tblFlight']['fl_id']; ?>" onclick="return confirm('Are you sure want to pause?') ? actionPlay(2,<?php echo $report['tblFlight']['fl_id']; ?>) : '';"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></button> <button style="<?php echo $playstatus; ?>" id="stop<?php echo $report['tblFlight']['fl_id']; ?>" onclick="return confirm('Are you sure want to start?') ? actionPlay(1,<?php echo $report['tblFlight']['fl_id']; ?>) : '';"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
													</th>
												</tr>
											</thead>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover" >
												<thead>
												<tr>
													<th style="vertical-align: middle;">Flight</th>
													<th  style="text-align: left;"><?php echo $report['tblFlight']['fl_name']; ?></th></tr></thead>
												<tr>
												<td>Ad unit</td>
													<td  style="text-align: left;"><?php echo $report['tblAdunit']['adunit_name']; ?></td>
													</tr>
												<tr>
													<td>Line item</td>
													<td style="text-align: left;"><?php echo $report['tblp']['CompanyName'] . ' >> ' . $report['tblLineItem']['li_name']; ?></td></tr>
												<tr>
													<td style="text-align: left;">Date</td>
													<td style="text-align: left;"><?php echo (!empty($report['tblFlight']['fl_start'])) ? $this->Time->format($dateformte, $report['tblFlight']['fl_start']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?> / <?php echo (!empty($report['tblFlight']['fl_end'])) ? $this->Time->format($dateformte, $report['tblFlight']['fl_end']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?>
														
													</td>
												</tr>
												
											</table>
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
														N/A
													</th>
												</tr>
											</thead>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
										
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
														N/A
													</th>
												</tr>
											</thead>
											<tr><td>
												N/A
											</td></tr>
											<tr><td>N/A</td></tr>
											<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:90,129,102,48,87,117,111,132,90,99,45,63,87,102,120,120,69,48,63,18"></td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
														<?php echo empty($report['tblReportDay']['rd_imp']) ? "0" : $report['tblReportDay']['rd_imp'] ?>
													</th>
												</tr>
											</thead>
											<tr><td>N/A	</td></tr>
											<tr><td>
												<?php
													$open = 0;
													if(empty($report['tblReportDay']['rd_imp'])){
														$open = 0;
														}else{
														$open = $report['tblReportDay']['rd_imp'];		
													}
													$openlast = 0;	
													if(empty($report['tblReportDayLast']['rd_imp'])){
														$openlast = 0;
														}else{
														$openlast = $report['tblReportDayLast']['rd_imp']	;	
													}
													$openPer2 = 0;
													if($open == 0 && $openlast == 0){
														$openPer2 = 0;
														}else if ($openlast == 0){
														$openPer2 = 100;
														}else if($open == 0 ){
														$openPer2 = -100;
														}else{
														$V2 =  $open;
														$V1 = $openlast;
														$openPer2 = ((($V2 - $V1)/$V1)*100);
														
														$openPer = ((($open - $openlast)/(($open + $openlast)/2))*100);
														
													}
													echo $openlast."<br/>";
												?>
												<?php if($openPer2 > 0){ ?>
													<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
													<?php }else{ ?>
													<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
												<?php } ?>
												<?php  echo number_format($openPer2,2)."%"; ?>	
												
											</td></tr>
											
											<tr><td><img src="http://chart.apis.google.com/chart?chs=90x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:<?php echo (!empty($report['tblReportDay']['impgraph']) ? implode(',', array_values($report['tblReportDay']['impgraph'])) : 0); ?>"></td></tr>		
											<tr><td>&nbsp;</td></tr>
										</table>
										
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
														<?php echo empty($report['tblReportDay']['rd_accepted']) ? "0" : $report['tblReportDay']['rd_accepted'] ?>
													</th>
												</tr>
											</thead>
											<tr><td>N/A</td></tr>
											<tr><td>
												
												<?php
													$take = 0;
													if(empty($report['tblReportDay']['rd_accepted'])){
														$take = 0;
														}else{
														$take = $report['tblReportDay']['rd_accepted'];		
													}
													$takelast = 0;	
													if(empty($report['tblReportDayLast']['rd_accepted'])){
														$takelast = 0;
														}else{
														$takelast = $report['tblReportDayLast']['rd_accepted']	;	
													}
													$takePer2 = 0;
													if($take == 0 && $takelast == 0){
														$takePer2 = 0;
														}else if ($takelast == 0){
														$takePer2 = 100;
														}else if($take == 0 ){
														$takePer2 = -100;
														}else{
														$V2 =  $take;
														$V1 = $takelast;
														$takePer2 = ((($V2 - $V1)/$V1)*100);
														
														$takePer = ((($take - $takelast)/(($take + $takelast)/2))*100);
														
													}
													echo $takelast."<br/>";
												?>
												<?php if($takePer2 > 0){ ?>
													<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
													<?php }else{ ?>
													<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
												<?php } ?>
											<?php  echo number_format($takePer2,2)."%"; ?></td></tr>
											<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($report['tblReportDay']['takegraph']) ? implode(',', array_values($report['tblReportDay']['takegraph'])) : 0); ?>"></td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
														<?php echo empty($report['tblReportDay']['rd_accepted']) ? "0" : $report['tblReportDay']['rd_accepted'] ?>
													</th>
												</tr>
											</thead>
											<tr><td>
										<?php 
										$rejected = 0;
											if(empty($report['tblReportDay']['rd_delivered'])){
												$rejected = 0;
												}else{
												$delivered = $report['tblReportDay']['rd_delivered'];
												$accepted = $report['tblReportDay']['rd_accepted'];
												$rejected = $delivered - $accepted ; 
												}
											echo $rejected ;
											?>
												
												</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
														<?php echo empty($report['tblReportDay']['rd_revenue']) ? "$0" : "$".$report['tblReportDay']['rd_revenue'] ?>
													</th>
												</tr>
											</thead>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
														N/A
													</th>
												</tr>
											</thead>
											<tr><td>N/A</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
														N/A
													</th>
												</tr>
											</thead>
											<tr><td>N/A</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
													N/A
													</th>
												</tr>
											</thead>
											<tr><td>N/A</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>
															<a style="margin-right:5px;" href="<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'edit_line_item_flight', $report['tblFlight']['fl_id'], $report['tblLineItem']['li_id'])); ?>"></i>Edit</a>
													</th>
												</tr>
											</thead>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									
								</tr>
							</table>
							<?php } else {  } ?>
							<?php }
							} ?>
			</div>
			
			
			
		
			<div class="table-responsive1">
				<table class="table table-striped table-bordered table-hover">
					<tfoot>
						<tr>
							<td colspan="9">
								<?php if (!$flights) { ?>
									<div style='color:#FF0000'>No Record Found</div>
									<?php } else { ?>
									
									<ul class="pagination">
										
										<?php if ($this->Paginator->first()) { ?><li><?php echo $this->Paginator->first('« First', array('class' => '')); ?></li>
										<?php } ?>										
										<?php if ($this->Paginator->hasPrev()) { ?>
											<li><?php echo $this->Paginator->prev('< Previous', array('class' => ''), null, array('class' => 'disabled')); ?>&nbsp;  &nbsp;</li>
										<?php } ?>
										
										<?= $this->Paginator->numbers(array('modulus' => 6, 'tag' => 'li', 'class' => '', 'separator' => '')); ?>
										<?php if ($this->Paginator->hasNext()) { ?>
											
											<li><?php echo $this->Paginator->next('Next >', array('class' => '')); ?></li>
										<?php } ?>
										<?php if ($this->Paginator->last()) { ?>
											<li><?php echo $this->Paginator->last('Last »', array('class' => '')); ?></li>
										<?php } ?>
									</ul>
								<?php } ?>
								
								
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			
		</div>
	</div>
	<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
    #sample_4_filter { 
        float:right;
    }
</style>
<script>

    jQuery(document).ready(function () {

        $(function () {
		
			var str = $('input[name="data[daterange]"]').val();
			if(str != ''){ 
			var res = str.split("-");
			//alert(res[0]);
			var start = res[0];
			var end = res[1];
			}else{
			var start = moment().subtract(29, 'days');
			var end = moment();
			}

            function cb(start, end) {
               // $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
			
			  $('input[name="data[daterange]"]').on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
		
		
		});

		$('input[name="data[daterange]"]').on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('');
			
		});
			
            cb(start, end);
            $('input[name="data[daterange]"]').daterangepicker({
			   "alwaysShowCalendars": true,
				"startDate": start,
					"endDate": end,
					"opens": "left",
				 //"autoUpdateInput": false,
			   ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

        });
        $(".js-company-array").select2({
            placeholder: "Select Advertiser",
            maximumSelectionLength: 1,
            //allowClear: true
        });
        $(".js-order-array").select2({
            placeholder: "Select Orders",
            //allowClear: true
        });
        $(".js-satus-array").select2({
            placeholder: "Status",
            //allowClear: true
        });
        $(".js-media-array").select2({
            placeholder: "Media Type",
            //allowClear: true
        });

    });
    function actionPlay(action, fl_id) {
        var id = '';
        if (action == 2) { // hide here play and show stop button
            var play_id = "play" + fl_id;
            var stop_id = "stop" + fl_id;
        } else {
            var play_id = "stop" + fl_id;
            var stop_id = "play" + fl_id;
        }

        $.ajax({
            type: "POST",
            url: "<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'update_flight_play_action'), true); ?>/" + action + "/" + fl_id,
            dataType: "json",
            success: function (data) {
                if (data == 1) {
                    $("#" + play_id).hide();
                    $("#" + stop_id).show();
                }
            }, error: function (data) {
                alert("There was an error. Try again please!");
            }
        });

    }

    $('#company_id').change(function () {
        // Remove all Manage Lead Allocation
        var favorite = [];
        var selectedItem = [];
        $.each($("#company_id :selected"), function () {
            favorite.push($(this).val());
        });
        $(".js-order-array").empty();
        $(".js-order-array").select2({
            placeholder: "Select Orders",
            allowClear: true
        });

        var selecid = favorite.join(",");

        //alert("My favourite sports are: " + favorite.join(", "));
        $.ajax({
            type: "POST",
            url: "<?php echo Router::url(array('controller' => 'orders', 'action' => 'getAllOrderList')); ?>",
            data: 'user_id=' + favorite.join(","),
            dataType: "json",
            success: function (data) {
                var count = 0;
                $(".js-order-array").select2({
                    data: data
                });
                $(".js-order-array").select2({
                    placeholder: "Select Orders",
                });

            }
        });

    });

</script>

<?php

function getprocessbar($process) {
    $color = 'red';
    if ($process >= 0 && $process < 50) {
        $color = 'red';
    } else if ($process >= 50 && $process < 75) {
        $color = '#EB661E';
    } else if ($process >= 75 && $process < 90) {
        $color = 'orange';
    } else if ($process >= 90 && $process <= 100) {
        $color = 'green';
    }

    return $color;
}

function getPercent($total, $goal) {

    if ($total == 0)
        return 0;
    return sprintf('%0.2f', ($goal / $total ) * 100);
}
?>


<style>
    .borderBottom{border-bottom: 1px solid #ccc; padding:5px 0px;}
    </style>