<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="/">Home</a><i class="fa fa-circle"></i>
    </li>
    <li >
        <a href="/orders/">Orders</a><i class="fa fa-circle"></i>
    </li>
    <li  class="active">
        <a href="#">Line Items</a>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Flights  of "<?php echo (!empty($lineItem_data['tblLineItem']['li_name'])) ? $lineItem_data['tblLineItem']['li_name'] : '' ?>"</span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">

                                <?php echo $this->html->link('Add a Flight  <i class="fa fa-plus"></i>', array('controller' => 'insertionorder', 'action' => 'add_line_item_flight', $line_item_id, $lineItem_data['tcs']['cs_id']), array('class' => 'btn green', 'escape' => false)); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_4">
                        <thead>
                            <tr>
                                <td width="5%;"></td>
                                <td width="25%;">
                                    <b>Ad Unit Name</b><br>
                                    Publisher
                                </td>

                                <th width="15%;">
                                    Flight Name
                                </th>

                                <th width="23%;">
                                    Preview
                                </th>
                                <th width="13%;">
                                    Delivery Mode
                                </th>
                                <th width="7%;">
                                    Start Date
                                </th>
                                <th width="7%;">
                                    End Date
                                </th>
                                <th width="5%;">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            if (!empty($mappedUnits)) {
                                foreach ($mappedUnits as $flights) {
                                    $br = (count($flights) > 1) ? '<br>' : '';
                                    ?>

                                    <tr class="odd gradeX">
                                        <td>

                                            <?php
                                            foreach ($flights as $user)
                                                ;
                                            $stopstatus = $playstatus = "display:none;";
                                            if ($user['tblFlight']['status'] == 1) {
                                                $stopstatus = "display:block;";
                                            } else {
                                                $playstatus = "display:block;";
                                            }
                                            ?>
                                            <button style ="<?php echo $stopstatus; ?>" id ="<?php echo "play" . $user['tblFlight']['fl_id']; ?>" onClick="return confirm('Are you sure want to pause?') ? actionPlay(2,<?php echo $user['tblFlight']['fl_id']; ?>) : '';"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></button>
                                            <button style ="<?php echo $playstatus; ?>" id ="<?php echo "stop" . $user['tblFlight']['fl_id'] ?>" onClick="return confirm('Are you sure want to start?') ? actionPlay(1,<?php echo $user['tblFlight']['fl_id']; ?>) : '';"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>


                                        </td>
                                        <td>
                                            <?php foreach ($flights as $user) { ?>
                                                <b><?php echo $user['tblAdunit']['adunit_name']; ?></b><br>
                                                <?php
                                                echo $user['tblp']['CompanyName'];
                                                echo $br;
                                                echo $br;
                                                ?>

                                            <?php } ?>
                                        </td>
                                        <td>

                                            <?php echo $user['tblFlight']['fl_name']; ?>
                                            <br/>

                                        </td>
                                        <td><?php
                                            // creative list
                                            foreach ($flights as $user) {

                                                if (!empty($user['tblCreative']) and $user['tblFlight']['fl_ptype_id'] == 2) {
                                                    $keys=array_keys($user['tblCreative']);
                                                    //echo $keys[0];
                                                    ?>
                                                    <a class="btn btn-xs blue"  href='/orders/preview_creative/<?php echo $user['tblMappedAdunit']['ma_uid']; ?>/<?php echo $keys[0];?>' target='_blank'>Preview Creative</a>
                                                    <?php 
                                                        echo $br;
                                                        echo $br;
                                                        echo $br;
                                                    //echo "<ul class='list-group'>";
                                                    //echo "<li class='list-group-item' style='width:150px;'>" . $this->Html->link('<i class="fa fa-users"></i>Preview', array('controller' => 'messages', 'action' => 'view_message_stats_views', $user['tblMappedAdunit']['ma_uid']), array('escape' => false, 'target' => '_blank')) . $this->html->link('', array('controller' => 'messages', 'action' => 'view_message_stats_download/' . $user['tblMappedAdunit']['ma_uid'], $user['tblFlight']['fl_id']), array('class' => 'glyphicon glyphicon-download-alt', 'style' => 'float:right;')) . "</li>";
                                                    //echo "</ul>";
                                                } elseif (in_array($user['tblFlight']['fl_ptype_id'], array(1, 15))) {
                                                    foreach ($user['tblCreative'] as $key => $value) { //echo $value;
                                                        
                                                        ?>
                                                         <li class='list-group-item'>
                                                             <?php echo $value;?>
                                                        <a class="btn btn-xs blue"  href='/orders/preview_creative/<?php echo $user['tblMappedAdunit']['ma_uid']; ?>/<?php echo $key;?>' target='_blank'>Preview Creative</a>
                                                         <?php echo '<br>';
                                                        echo '<br>';
                                                        echo '<br>'; ?>
                                                         </li>
                                                    <?php }
                                                } elseif ($user['tblFlight']['fl_ptype_id'] == 13||$user['tblFlight']['fl_ptype_id'] == 20) {

                                                    foreach ($user['tblCreative'] as $key => $value) {
                                                        
                                                        ?>
                                                        <a class="btn btn-xs blue"  href='/orders/preview_creative/<?php echo $user['tblMappedAdunit']['ma_uid']; ?>/<?php echo $key;?>' target='_blank'>Preview Creative</a>
                                                        <?php
                                                        echo '<br>';
                                                        echo '<br>';
                                                        echo '<br>';
                                                    }
                                                } else {
                                                    echo "<ul class='list-group'>";
                                                    foreach ($user['tblCreative'] as $key => $value) {
                                                        ?>
                                            <li class='list-group-item'>
                                                <?php echo $value . '&nbsp;(' . Configure::read('data.flight.lead_post_status.' . $user['tblMappedAdunit']['lead_post_status']) . ')'; ?><br />
                                                <?php if ($user['tblMappedAdunit']['lead_post_status'] == 1) { ?>
                                                    <?php echo $this->Html->link('Preview & Submit Test', array('controller' => 'orders', 'action' => 'coreg_detail', $user['tblAdunit']['ad_uid'], $line_item_id, $user['tblFlight']['fl_id']), array('escape' => false, 'class' => "btn btn-xs blue")); ?>
                                                <?php } elseif ($user['tblMappedAdunit']['lead_post_status'] == 2) { ?>
                                                    <a href="javascript:;" class="btn btn-xs blue" onclick="change_leadpost_status(<?php echo $user['tblAdunit']['ad_dfp_id']; ?>,<?php echo $line_item_id; ?>,<?php echo $user['tblFlight']['fl_id']; ?>, 3);">Approve Flight</a>
                                                    <?php echo '&nbsp;' . $this->Html->link('Send another Test Lead', array('controller' => 'orders', 'action' => 'coreg_detail', $user['tblAdunit']['ad_uid'], $line_item_id, $user['tblFlight']['fl_id']), array('escape' => false, 'class' => "btn btn-xs blue")); ?>
                                                <?php } elseif ($user['tblMappedAdunit']['lead_post_status'] == 3) { ?>
                                                    <a href="javascript:;"  class="btn btn-xs blue" onclick="change_leadpost_status(<?php echo $user['tblAdunit']['ad_dfp_id']; ?>,<?php echo $line_item_id; ?>,<?php echo $user['tblFlight']['fl_id']; ?>, 4);">Stop & Close Flight</a>
                                                <?php } else { ?>
                                                    <a href="javascript:;"  class="btn btn-xs blue" onclick="change_leadpost_status(<?php echo $user['tblAdunit']['ad_dfp_id']; ?>,<?php echo $line_item_id; ?>,<?php echo $user['tblFlight']['fl_id']; ?>, 1);">Activate Again</a>
                                                <?php }
                                                ?>

                                            </li>
                                            <?php
                                        }
                                        echo "</ul>";
                                    }
                                    //echo $br;
                                }
                                ?>
                                </td>
                                <td>
                                    <?php
                                    // creative list
                                    foreach ($flights as $user) {
                                        //if (!empty($user['tblCreative']) and $user['tblFlight']['fl_ptype_id'] != 2) {
                                        if (!empty($user['tblCreative'])) {
                                            echo "<ul class='list-group'>";
                                            foreach ($user['tblCreative'] as $key => $value) {
                                                ?>
                                            <li class='list-group-item'>
                                                <?php if ($user['tblMappedAdunit']['is_lead_post'] == 1) { ?>
                                                    Live Mode <a onclick="change_lead_delivery_status(<?php echo $user['tblMappedAdunit']['ma_id']; ?>, 0);" class="btn btn-xs btn-success">Make Test</a>
                                                <?php } else { ?>
                                                    Test Mode <a  onclick="change_lead_delivery_status(<?php echo $user['tblMappedAdunit']['ma_id']; ?>, 1);" class="btn btn-xs btn-danger">Make Live</a>
                                                <?php } ?>
                                            </li>
                                            <?php
                                        }
                                        echo "</ul>";
                                    }
                                    //echo $br;
                                }
                                ?>
                                </td>
                                <td><?php
                                    $dateformte = Configure::read('Php.dateformte');
                                    echo!empty($user['tblLineItem']['li_start_date']) ? date($dateformte, strtotime($user['tblLineItem']['li_start_date'])) : "<span style='font-weight:bold;color:red;'>N/A</span>";
                                    ?>
                                </td>
                                <td><?php
                                    echo!empty($user['tblLineItem']['li_end_date']) ? date($dateformte, strtotime($user['tblLineItem']['li_end_date'])) : "<span style='font-weight:bold;color:red;'>N/A</span>";
                                    ?> </td>
                                <td class="center">
                                    <?php echo $this->Html->link('Edit', array('controller' => 'insertionorder', 'action' => 'edit_line_item_flight', $user['tblFlight']['fl_id'], $line_item_id)); ?>
                                </td>
                                </tr> 
                                <?php
                                // }
                            }
                        }// end of if conditions 
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
    <style>
        #sample_4_filter { 
            float:right;
        }
    </style>

    <script>
        function change_leadpost_status(ad_dfp_id, line_item_id, flight_id, status) {
            jQuery.ajax({
                type: "POST",
                url: '/orders/change_leadpost_status',
                // data: 'flight_id=' + flight_id + '&status=' + status,
                data: 'ad_dfp_id=' + ad_dfp_id + '&li_id=' + line_item_id + '&flight_id=' + flight_id + '&status=' + status,
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }
        function change_lead_delivery_status(ma_id, status) {
            jQuery.ajax({
                type: "POST",
                url: '/orders/change_flight_delivery_status',
                data: 'ma_id=' + ma_id + '&status=' + status,
                dataType: "json",
                success: function (data) {
                    location.reload();
                }
            });
        }
        function actionPlay(action, fl_id) {
            var id = '';
            if (action == 2) { // hide here play and show stop button
                var play_id = "play" + fl_id;
                var stop_id = "stop" + fl_id;
            } else {
                var play_id = "stop" + fl_id;
                var stop_id = "play" + fl_id;
            }

            $.ajax({
                type: "POST",
                url: "<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'update_flight_play_action'), true); ?>/" + action + "/" + fl_id,
                dataType: "json",
                success: function (data) {
                    if (data == 1) {
                        $("#" + play_id).hide();
                        $("#" + stop_id).show();
                    }
                }, error: function (data) {
                    alert("There was an error. Try again please!");
                }
            });

        }
    </script>
    <!--
            <a class="btn default" data-toggle="modal" href="#draggable_<?php echo $user['tblMappedAdunit']['ma_uid']; ?>">
            View Pixel </a>
            <div class="modal fade draggable-modal" id="draggable_<?php echo $user['tblMappedAdunit']['ma_uid']; ?>" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Here's The Pixels</h4>
            </div>
            <div class="modal-body">
            <b>Impresion Pixel</b><br />    http://tracking.digitaladvertising.systems/piwik.php?idsite=<?php echo $user['tblp']['tracksite_id']; ?>&rec=1&c_n=<?php echo $user['tblMappedAdunit']['ma_l_id']; ?>&c_p=<?php echo $user['tblMappedAdunit']['ma_uid']; ?>
            <br />
            <br />
            <b>Lead Pixel</b><br />
            http://tracking.digitaladvertising.systems/piwik.php?idsite=<?php echo $user['tblp']['tracksite_id']; ?>&rec=1&c_n=<?php echo $user['tblMappedAdunit']['ma_l_id']; ?>&c_p=<?php echo $user['tblMappedAdunit']['ma_uid']; ?>&c_i=lead
            
            </div>
            <div class="modal-footer">
            <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
            </div>
            </div>
            </div>
    -->