<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Creative Details
                </div>
            </div>
            <div class="portlet-body form">
                <div class='wrapper'>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">creative</h3>
                        </div>

                        <div class="panel-body">
                            <?php
                            //pr($mappedAdunitDetails);
                            if(isset($mailings[0]['tbl_mailings']['m_id']))
                                $mid = $mailings[0]['tbl_mailings']['m_id'];
                            else
                                $mid = "";
                           // echo $mid;
                            //$cs = "";
                            if((($mappedAdunitDetails['LineItem']['li_cs_id']) == 1) && (($mappedAdunitDetails['LineItem']['li_cs_id']) == 5))
                                   $cs = "getclick";
                                elseif(($mappedAdunitDetails['LineItem']['li_cs_id']) == 3)
                                   $cs = "get";
                                elseif(($mappedAdunitDetails['LineItem']['li_cs_id']) == 7)
                                   $cs = "getscr";
                                else
                                    $cs = "getemail";
                            
                            $impression_url = "https://tracking.digitaladvertising.systems/piwik.php?idsite=" . $mappedAdunitDetails['tblp']['tracksite_id'] . "&rec=1&c_n=" . $mappedAdunitDetails['Creative']['cr_id'] . "&c_p=" . $mappedAdunitDetails['tblMappedAdunit']['ma_uid'];
                            $impression_pixel = "<img src='" . $impression_url . "' style='display:none;' height=1 width=1 />";
                            $url = "https://api.digitaladvertising.systems/leads/".$cs."?";
                            $link = "adid=" . $mappedAdunitDetails['tblMappedAdunit']['ma_ad_id'] . '&lid=' . $mappedAdunitDetails['LineItem']['li_dfp_id'];
                            $link.='&cid=' . $mappedAdunitDetails['Creative']['cr_id'] . '&orid=' . $mappedAdunitDetails['Order']['dfp_order_id'] . '&flid=' . $mappedAdunitDetails['Flight']['fl_id'];
                            $link.='&map_id=' . $mappedAdunitDetails['tblMappedAdunit']['ma_uid'];
                            $link.='&mailing_id='.$mid;
                            //$link.='&od_fname=&od_lname=&od_site=&device_type=&od_phone=&od_source=&od_country_name=&fp_browser=&fp_connection=&fp_display=&fp_flash=&fp_language=&fp_os=&fp_timezone=&fp_useragent=&od_ipaddress=';
                            $link.='&email=%%EMAIL_ADDRESS%%';
                            //$link.='&redirect=http://zacks.com';
                           
                            $href = $url . $link;
                            $body = $mappedAdunitDetails['Creative']['cr_body'];
                            $a2=array();
                            preg_match_all("/href=\"(.*?)\"/i", $body, $matches);
                            for($i = 0; $i<count($matches[1]); $i++)
                            {
                                $a2[].= $href;
                            }
                          //  print_r($matches[1]);
                            $body = str_replace($matches[1], $a2, $body);
                            $rss_url = 'http://api.digitaladvertising.systems/nloffers/index.xml?auid='.$mappedAdunitDetails['tblAdunit']['ad_uid'];
                            if (!empty($mappedAdunitDetails['Creative']['cr_cta'])) {
                                $body.='<div><a href="' . $href . '">' . $mappedAdunitDetails['Creative']['cr_cta'] . '</a></div>';
                            }
                            ?>
                            <div><b>Impression Pixel :</b> <br /><br /><code><?php echo htmlspecialchars($impression_pixel); ?></code></div>
                            <div class="margin-top-20"><b>Posting Url :</b> <br /><br /><code><?php echo $href; ?></code></div>
                            <div class="margin-top-20"><b>Redirect Url :</b> <br /><br /><code><?php echo $mappedAdunitDetails['Creative']['cr_redirect_url']; ?></code></div>
                            <div class="margin-top-20"><b>RSS Feed Url :</b> <br /><br /><code><?php echo $rss_url; ?></code></div>
                            <div class="margin-top-20"><b>Creative Body :</b> <br /><br />
                                <textarea class="form-control" rows="10"><?php echo $body . $impression_pixel; ?></textarea>
                            </div>
                            <div class="margin-top-20"><b>Email Tag :</b>&nbsp;&nbsp;<?php echo '%%EMAIL_ADDRESS%%'; ?></div>
                        </div>
                    </div>
                </div>
                <div style="background-color: #eeeeee;margin: 20px 0px;padding:20px 0px;">
                    <a href="/orders/view_flights/<?php echo $mappedAdunitDetails['LineItem']['li_id']; ?>" class="btn green" style="padding:10px;margin-left: 20px;">Return</a>
                </div>
            </div>
        </div>
    </div>
</div>