<?php
	/**
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Pages
		* @since         CakePHP(tm) v 0.10.0.1076
	*/
	
	if (!Configure::read('debug')):
	throw new NotFoundException();
	endif;
	
	App::uses('Debugger', 'Utility');
?>
<div class="row">
	<div class="col-md-4">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="/">Home</a><i class="fa fa-circle"></i>
			</li>
			<li>
				<a href="/orders/view_line_items/<?php echo $li_order_id; ?>/">Line Item</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				<a href="#"><?php echo $line_name; ?></a>
			</li>
		</ul>
	</div>
	<div class="col-md-4">
		<div align="center">
		<a href="http://digitaladvertising.systems/orders/view_detail_stats/<?php echo $line_id; ?>/">View Performance Data</a></div>
	</div>
	<div class="col-md-4">
		<div class="portlet-title" style="float:right;">
			<div class="actions">
				<div class="btn-group btn-group-devided" data-toggle="buttons">
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "day" ? "active" : "" ?>">
					<input type="radio" name="options" onchange="chart1()" class="toggle" id="option1">Today</label>
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "week" ? "active" : "" ?>">
					<input type="radio" name="options"  onchange="chart2()" class="toggle" id="option2">Last 7 Days</label>
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "month" ? "active" : "" ?>">
					<input type="radio" name="options"  onchange="chart3()" class="toggle" id="option3">MTD</label>
					<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "year" ? "active" : "" ?>">
					<input type="radio" name="options"  onchange="chart4()" class="toggle" id="option2">YTD</label>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-8">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-male font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Age Group</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_age" class="chart" style="height: 325px;">
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa  fa-bank font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Education</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_education" class="chart" style="height: 325px;">
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-smile-o  font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">MARITAL STATUS</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_marital" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-suitcase font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Occupation</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_occup" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-home  font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Household income</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_hhinc" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Net Worth</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_networth" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-home font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Home owner status</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_ownrent" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Dwelling Type</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_hsg" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-8">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-info font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Length of Residence</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_lmos" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa  fa-chain-broken font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Smokers</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_smoker" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-6">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-building font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Business</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_business" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-6">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-hospital-o font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Health Conscious</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_health" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-6">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Investing</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_investing" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-6">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-paw font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Pet Lovers</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_pet" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
		<div class="col-md-6">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-truck font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Travelers</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_traveler" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
		<div class="col-md-6">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Fashion & Apparel</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_fashion" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Products</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_product" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	

	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Electronics & Technology</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_electronic" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>		
	<div class="col-md-4">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Auto</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_auto" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-6">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Political</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_political" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="col-md-12">
		<div class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="fa fa-dollar font-grey-silver"></i>
					<span class="caption-subject font-grey-silver bold uppercase">Others</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div id="chart_rest" class="chart" style="height: 325px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<script type="text/javascript">
	<?php 
		$tblKmaAgeArray = array();
		$i = 0;
		foreach($tblKmaAgeGroup as $key => $value){
			$tblKmaAgeArray[$i]['group'] = $key;
			$tblKmaAgeArray[$i]['count'] = $value;
			$i++;
		}
	?>
    ageVars = new Array();
	ageVars = <?php echo json_encode($tblKmaAgeArray); ?>;
	
	<?php 
		$tblKmaEducationArray = array();
		$i = 0;
		foreach($tblKmaEducation as $key => $value){
				/*
			switch($key){
				case "A":
				$tblKmaEducationArray[$i]['group'] = "Some High School";
				break;
				case "B":
				$tblKmaEducationArray[$i]['group'] = "High School Grad";
				break;
				case "C":
				$tblKmaEducationArray[$i]['group'] = "Some College";
				break;
				case "D":
				$tblKmaEducationArray[$i]['group'] = "College Graduate";
				break;
				Default:
				$tblKmaEducationArray[$i]['group'] = "Unknown";
				break;
			} */
			$tblKmaEducationArray[$i]['group'] = $key;
			$tblKmaEducationArray[$i]['count'] = $value;
			$i++;
		}
	?>
    educationVars = new Array();
	educationVars = <?php echo json_encode($tblKmaEducationArray); ?>;
	
	<?php 
		$tblKmaMaritalArray = array();
		$i = 0;
		foreach($tblKmaMaritalStats as $key => $value){
			$tblKmaMaritalArray[$i]['group'] = $key;
			$tblKmaMaritalArray[$i]['count'] = $value;
			$i++;
		}
	?>
    maritalVars = new Array();
	maritalVars = <?php echo json_encode($tblKmaMaritalArray); ?>;
	
	<?php 
		$tblKmaOccupArray = array();
		$i = 0;
		foreach($tblKmaOccup as $key => $value){
			$tblKmaOccupArray[$i]['group'] = $key;
			$tblKmaOccupArray[$i]['count'] = $value;
			$i++;
		}
	?>
    occupVars = new Array();
	occupVars = <?php echo json_encode($tblKmaOccupArray); ?>;
	
	
	<?php 
		$tblKmahhincArray = array();
		$i = 0;
		foreach($tblKmahhinc as $key => $value){
			$tblKmahhincArray[$i]['group'] = $key;
			$tblKmahhincArray[$i]['count'] = $value;
			$i++;
		}
	?>
    hhincVars = new Array();
	hhincVars = <?php echo json_encode($tblKmahhincArray); ?>;
	
	
	<?php 
		$tblKmaNetWorthArray = array();
		$i = 0;
		foreach($tblKmaNetWorth as $key => $value){
			$tblKmaNetWorthArray[$i]['group'] = $key;
			$tblKmaNetWorthArray[$i]['count'] = $value;
			$i++;
		}
	?>
    networthVars = new Array();
	networthVars = <?php echo json_encode($tblKmaNetWorthArray); ?>;
	
	
	<?php 
		$tblKmaOwnRentArray = array();
		$i = 0;
		foreach($tblKmaOwnRent as $key => $value){
			$tblKmaOwnRentArray[$i]['group'] = $key;
			$tblKmaOwnRentArray[$i]['count'] = $value;
			$i++;
		}
	?>
    ownrentVars = new Array();
	ownrentVars = <?php echo json_encode($tblKmaOwnRentArray); ?>;
	
	
	
	<?php 
		$tblKmahsgArray = array();
		$i = 0;
		foreach($tblKmaHsg as $key => $value){
			$tblKmahsgArray[$i]['group'] = $key;
			$tblKmahsgArray[$i]['count'] = $value;
			$i++;
		}
	?>
    hsgVars = new Array();
	hsgVars = <?php echo json_encode($tblKmahsgArray); ?>;
	
	
	
	<?php 
		$tblKmahLmosArray = array();
		$i = 0;
		foreach($tblKmaLmos as $key => $value){
			$tblKmahLmosArray[$i]['group'] = $key;
			$tblKmahLmosArray[$i]['count'] = $value;
			$i++;
		}
	?>
    lmosVars = new Array();
	lmosVars = <?php echo json_encode($tblKmahLmosArray); ?>;
	
	<?php 
		$tblKmaBusinessArray = array();
		$tblKmaBusinessArray[0]['group'] = 'Business - Home Office';
		$tblKmaBusinessArray[0]['count'] = $KMA_HOMEOFFICE;
		$tblKmaBusinessArray[0]['color'] = '#FF0F00';
		$tblKmaBusinessArray[1]['group'] = 'Business - SOHO';
		$tblKmaBusinessArray[1]['count'] = $KMA_SOHO;
		$tblKmaBusinessArray[1]['color'] = "#FF6600";
		$tblKmaBusinessArray[2]['group'] = 'Business - Opportunity Seekers';
		$tblKmaBusinessArray[2]['count'] = $KMA_OPSEEK;
		$tblKmaBusinessArray[2]['color'] = "#FF9E01";
	?>
	businessVars = new Array();
	businessVars = <?php echo json_encode($tblKmaBusinessArray); ?>;
	
	
	<?php 
		$tblKmaSmokerArray = array();
		$tblKmaSmokerArray[0]['group'] = 'Smokers';
		$tblKmaSmokerArray[0]['count'] = $KMA_SMOKER;
		$tblKmaSmokerArray[0]['color'] = "#FCD202";
		$tblKmaSmokerArray[1]['group'] = 'Smokers(Cigars)';
		$tblKmaSmokerArray[1]['count'] = $KMA_CIGAR_SMOKER;
		$tblKmaSmokerArray[1]['color'] = "#F8FF01";
	?>
	smokersVars = new Array();
	smokersVars = <?php echo json_encode($tblKmaSmokerArray); ?>;
	
	
	<?php 
		$tblKmaHealthArray = array();
		$tblKmaHealthArray[0]['group'] = 'Health Conscious (Diet & Weight Loss)';
		$tblKmaHealthArray[0]['count'] = $KMA_WTLOSS;
		$tblKmaHealthArray[0]['color'] = "#FCD202";
		$tblKmaHealthArray[1]['group'] = 'Health Conscious (Healthy Living) ';
		$tblKmaHealthArray[1]['count'] = $KMA_HEALTH;
		$tblKmaHealthArray[1]['color'] = "#F8FF01";
		$tblKmaHealthArray[2]['group'] = 'Health Conscious (Exercise Enthusiasts)';
		$tblKmaHealthArray[2]['count'] = $KMA_EXERCISE;
		$tblKmaHealthArray[2]['color'] = "#FF9E01";
	?>
	healthVars = new Array();
	healthVars = <?php echo json_encode($tblKmaHealthArray); ?>;
	
	
	<?php 
		$tblKmaInvestingArray = array();
		$tblKmaInvestingArray[0]['group'] = 'Investing';
		$tblKmaInvestingArray[0]['count'] = $KMA_INVESTMENTS;
		$tblKmaInvestingArray[0]['color'] = "#FCD202";
		$tblKmaInvestingArray[1]['group'] = 'Investing (Traders)';
		$tblKmaInvestingArray[1]['count'] = $KMA_INVEST_LOWEND;
		$tblKmaInvestingArray[1]['color'] = "#F8FF01";
		$tblKmaInvestingArray[2]['group'] = 'Investors (High Net Worth)';
		$tblKmaInvestingArray[2]['count'] = $KMA_INVEST_HIGHEND;
		$tblKmaInvestingArray[2]['color'] = "#FF9E01";
	?>
	investingVars = new Array();
	investingVars = <?php echo json_encode($tblKmaInvestingArray); ?>;
	
	<?php 
		$tblKmaPetArray = array();
		$tblKmaPetArray[0]['group'] = 'Pet Lovers';
		$tblKmaPetArray[0]['count'] = $KMA_PETS;
		$tblKmaPetArray[0]['color'] = "#FCD202";
		$tblKmaPetArray[1]['group'] = 'Pet Lovers - Dogs';
		$tblKmaPetArray[1]['count'] = $KMA_PETS_DOGS;
		$tblKmaPetArray[1]['color'] = "#F8FF01";
		$tblKmaPetArray[2]['group'] = 'Pet Lovers - Cats';
		$tblKmaPetArray[2]['count'] = $KMA_PETS_CATS;
		$tblKmaPetArray[2]['color'] = "#FF9E01";
	?>
	petVars = new Array();
	petVars = <?php echo json_encode($tblKmaPetArray); ?>;
	
	<?php 
		$tblKmaProductsArray = array();
		$tblKmaProductsArray[0]['group'] = 'Products - Seniors';
		$tblKmaProductsArray[0]['count'] = $KMA_SRPRODS;
		$tblKmaProductsArray[0]['color'] = "#FCD202";
		$tblKmaProductsArray[1]['group'] = 'Products - Children\'s';
		$tblKmaProductsArray[1]['count'] = $KMA_KIDSPROD;
		$tblKmaProductsArray[1]['color'] = "#F8FF01";
	?>
	productVars = new Array();
	productVars = <?php echo json_encode($tblKmaProductsArray); ?>;
	
	
	<?php 
		$tblKmaTravelersArray = array();
		$tblKmaTravelersArray[0]['group'] = 'Travelers';
		$tblKmaTravelersArray[0]['count'] = $KMA_TRAVEL;
		$tblKmaTravelersArray[0]['color'] = "#FCD202";
		$tblKmaTravelersArray[1]['group'] = 'Travelers (Luxury) ';
		$tblKmaTravelersArray[1]['count'] = $KMA_TRAVEL_HIGHEND;
		$tblKmaTravelersArray[1]['color'] = "#F8FF01";
		$tblKmaTravelersArray[2]['group'] = 'Travelers (Deal Hunters)';
		$tblKmaTravelersArray[2]['count'] = $KMA_TRAVEL_LOWEND;
		$tblKmaTravelersArray[2]['color'] = "#FF9E01";
	?>
	travelersVars = new Array();
	travelersVars = <?php echo json_encode($tblKmaTravelersArray); ?>;
	
	
	<?php 
		$tblKmaFashionsArray = array();
		$tblKmaFashionsArray[0]['group'] = 'Fashion & Apparel (Women\'s) ';
		$tblKmaFashionsArray[0]['count'] = $KMA_WOMFASH;
		$tblKmaFashionsArray[0]['color'] = "#FCD202";
		$tblKmaFashionsArray[1]['group'] = 'Fashion & Apparel (Women\'s Deal Hunters)';
		$tblKmaFashionsArray[1]['count'] = $KMA_WOMFASH_HIGHEND;
		$tblKmaFashionsArray[1]['color'] = "#F8FF01";
		$tblKmaFashionsArray[2]['group'] = 'Fashion & Apparel (Women\'s Brand Names)';
		$tblKmaFashionsArray[2]['count'] = $KMA_WOMFASH_LOWEND;
		$tblKmaFashionsArray[2]['color'] = "#0D8ECF";
		$tblKmaFashionsArray[3]['group'] = 'Fashion & Apparel (Men\'s)';
		$tblKmaFashionsArray[3]['count'] = $KMA_MENFASH;
		$tblKmaFashionsArray[3]['color'] = "#FF9E01";
	?>
	fashionsVars = new Array();
	fashionsVars = <?php echo json_encode($tblKmaFashionsArray); ?>;
	
	
	<?php 
		$tblKmaElectronicsArray = array();
		$tblKmaElectronicsArray[0]['group'] = 'Electronics & Technology';
		$tblKmaElectronicsArray[0]['count'] = $KMA_TECHNOLOGY;
		$tblKmaElectronicsArray[0]['color'] = "#FCD202";
		$tblKmaElectronicsArray[1]['group'] = 'Electronics & Technology (Gamers)';
		$tblKmaElectronicsArray[1]['count'] = $KMA_GAMERS;
		$tblKmaElectronicsArray[1]['color'] = "#F8FF01";
	?>
	electronicsVars = new Array();
	electronicsVars = <?php echo json_encode($tblKmaElectronicsArray); ?>;
	
	<?php 
		$tblKmaPoliticalArray = array();
		$tblKmaPoliticalArray[0]['group'] = 'Political(Left)';
		$tblKmaPoliticalArray[0]['count'] = $KMA_POL_LEFT;
		$tblKmaPoliticalArray[0]['color'] = "#FCD202";
		$tblKmaPoliticalArray[1]['group'] = 'Political(Right)';
		$tblKmaPoliticalArray[1]['count'] = $KMA_POL_RIGHT;
		$tblKmaPoliticalArray[1]['color'] = "#F8FF01";
		$tblKmaPoliticalArray[2]['group'] = 'Political(Independents)';
		$tblKmaPoliticalArray[2]['count'] = $KMA_POL_IND;
		$tblKmaPoliticalArray[2]['color'] = "#FF9E01";
	?>
	politicalVars = new Array();
	politicalVars = <?php echo json_encode($tblKmaPoliticalArray); ?>;
	
	
	<?php 
		$tblKmaAutoArray = array();
		$tblKmaAutoArray[0]['group'] = 'Auto - Motorcycle Enthusiasts';
		$tblKmaAutoArray[0]['count'] = $KMA_MOTORCYCLE;
		$tblKmaAutoArray[0]['color'] = "#FCD202";
		$tblKmaAutoArray[1]['group'] = 'Auto Enthusiasts';
		$tblKmaAutoArray[1]['count'] = $KMA_AUTOMOTIVE;
		$tblKmaAutoArray[1]['color'] = "#F8FF01";
	?>
	autoVars = new Array();
	autoVars = <?php echo json_encode($tblKmaAutoArray); ?>;
	
	
	<?php 
		$tblKmaRestArray = array();
		$tblKmaRestArray[0]['group'] = 'Continuing Education';
		$tblKmaRestArray[0]['count'] = $KMA_CONTED;
		$tblKmaRestArray[0]['color'] = "#FF0F00";
		$tblKmaRestArray[1]['group'] = 'Magazines & Books (Current Events)';
		$tblKmaRestArray[1]['count'] = $KMA_CURREVENT;
		$tblKmaRestArray[1]['color'] = "#FF6600";
		$tblKmaRestArray[2]['group'] = 'Sweepstakes Enthusiasts';
		$tblKmaRestArray[2]['count'] = $KMA_SWEEPS;
		$tblKmaRestArray[2]['color'] = "#FF9E01";
		$tblKmaRestArray[3]['group'] = 'Donors';
		$tblKmaRestArray[3]['count'] = $KMA_DONORS;
		$tblKmaRestArray[3]['color'] = "#FCD202";
		$tblKmaRestArray[4]['group'] = 'Arts, Crafts & Collectors';
		$tblKmaRestArray[4]['count'] = $KMA_CRAFTS_COLL;
		$tblKmaRestArray[4]['color'] = "#F8FF01";
		$tblKmaRestArray[5]['group'] = 'Outdoors';
		$tblKmaRestArray[5]['count'] = $KMA_OUTDOORS;
		$tblKmaRestArray[5]['color'] = "#B0DE09";
		$tblKmaRestArray[6]['group'] = 'Home D�cor';
		$tblKmaRestArray[6]['count'] = $KMA_DECOR;
		$tblKmaRestArray[6]['color'] = "#04D215";
		$tblKmaRestArray[7]['group'] = 'Home & Garden';
		$tblKmaRestArray[7]['count'] = $KMA_HOMEGARD;
		$tblKmaRestArray[7]['color'] = "#0D8ECF";
		$tblKmaRestArray[8]['group'] = 'Deals & Discounts';
		$tblKmaRestArray[8]['count'] = $KMA_DEALS;
		$tblKmaRestArray[8]['color'] = "#0D52D1";
		$tblKmaRestArray[9]['group'] = 'Food & Wine';
		$tblKmaRestArray[9]['count'] = $KMA_FOODWINE;
		$tblKmaRestArray[9]['color'] = "#2A0CD0";
	?>
	restVars = new Array();
	restVars = '<?php echo json_encode($tblKmaRestArray); ?>';
</script>
<script type="text/javascript">
	  google.load("visualization", "1", {packages:["bar"]});
	  google.setOnLoadCallback(drawVisualization);
      function drawVisualization() {
        var data = google.visualization.arrayToDataTable([
          ['Age', 'Male', 'Female', 'N/A'],
          ['18-34', <?php echo $tblKmaAgeGroup['A']['M'] ?>, <?php echo $tblKmaAgeGroup['A']['F'] ?>, <?php echo $tblKmaAgeGroup['A'][''] ?>],
          ['35-44', <?php echo $tblKmaAgeGroup['B']['M'] ?>, <?php echo $tblKmaAgeGroup['B']['F'] ?>, <?php echo $tblKmaAgeGroup['B'][''] ?>],
          ['45-54', <?php echo $tblKmaAgeGroup['C']['M'] ?>, <?php echo $tblKmaAgeGroup['C']['F'] ?>, <?php echo $tblKmaAgeGroup['C'][''] ?>],
          ['55-64', <?php echo $tblKmaAgeGroup['D']['M'] ?>, <?php echo $tblKmaAgeGroup['D']['F'] ?>, <?php echo $tblKmaAgeGroup['D'][''] ?>],
          ['65-74', <?php echo $tblKmaAgeGroup['E']['M'] ?>, <?php echo $tblKmaAgeGroup['E']['F'] ?>, <?php echo $tblKmaAgeGroup['E'][''] ?>],
          ['75-plus',<?php echo $tblKmaAgeGroup['F']['M'] ?>, <?php echo $tblKmaAgeGroup['F']['F'] ?>, <?php echo $tblKmaAgeGroup['F'][''] ?>],
          ['N/A',<?php echo $tblKmaAgeGroup['']['M'] ?>, <?php echo $tblKmaAgeGroup['']['F'] ?>, <?php echo $tblKmaAgeGroup[''][''] ?>]
        ]);

        var options = {
          'width':'100%',
		 'height':'325',
			chartArea:{left:45,top:20,width:"100%",height:"70%"}
        };

        var chart = new google.charts.Bar(document.getElementById('chart_age'));

        chart.draw(data,options);
      }
    </script>