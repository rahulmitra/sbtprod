<style>
	.inactive-button{font-weight:bold;}
    .ready-button{font-weight:bold;}
    .uploadcreative-button{margin-right:-2px;text-decoration:underline;font-weight:bold;}
    .delivery-button{font-weight:bold;}
    .createflight-button{margin-right:5px;text-decoration:underline;font-weight:bold;}
    .extra-button{color:#BB99AA;font-weight:bold;}
    .action-button{width:100%;border: 2px solid #333;}
	.label-success {
    background-color: #5cb85c;
	}
	.label-primary {
    background-color: #337ab7;
	}
	.label-primary a{color:#fff;}
	.csname{font-weight:bold;}
	.progress { margin-bottom: 1px; width: 140px;}
	
</style>	
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Line items Reports</span>
				</div>
				
			</div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <?php echo $this->Form->create(false, array('url' => array('controller' => 'orders', 'action' => 'reports'), 'class' => 'form-horizontal')); ?>
                    <?php echo $this->Form->hidden('submit_value', array('value' => 'submit')); ?>
                    <div class="row">
						
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
									echo $this->Form->input('company_id', array(
                                    'class' => 'js-company-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Select Advertiser',
                                    'options' => $companylist
									));
								?>
								
								
								
							</div>
						</div>
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
									echo $this->Form->input('order_id', array(
                                    'class' => 'js-order-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Select Orders',
                                    'options' => $orderlist
                                ));
                                ?>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('status_id', array(
                                    'class' => 'js-satus-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Please Select Status',
                                    'options' => $status
                                ));
                                ?>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('media_id', array(
                                    'class' => 'js-media-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Please Select Media Name ',
                                    'options' => $medialist
                                ));
                                ?>

                            </div>


                        </div>
						<div class="col-md-2">
                            <div class="btn-group">
                                <button type="submit" class="btn blue btn-secondary">Filter</button> 
							</div>
						</div>
                        <div class="col-md-2">


                            <div class="input-icon right" style="display:none;">
                                <i class="fa"></i> 
                                <?php echo $this->Form->input('daterange', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Date Range', 'style' => 'padding-right: 0;')); ?> 	<span></span>
								
								
							</div>
						</div>
                   
					</div>
                    <?php echo $this->Form->end(); ?>
				</div>
                <div class="table-responsive1">
				    <?php
                        if (!empty($lineitems)) {
							$csnameArray = array(1 => '<span class="csname">CPC</span>', 2 => '<span class="csname">CPD</span>', 3 => '<span class="csname">CPL</span>', 4 => '<span class="csname">CPS</span>', 5 => '<span class="csname">CPTI</span>', 6 => '<span class="csname">CPV</span>');
                            $statusArray = array(1 => '<label class="label label-default"><span class="inactive-button">{STATUS}</span></label>', 2 => '<label class="label label-primary"><a class="uploadcreative-button" href="{LINK}">Upload Creative</a></label>', 3 => '<label class="label label-warning"><span class="ready-button">{STATUS}</span></label>', 4 => '<label class="label label-success"><span class="delivery-button">{STATUS}</span></label>', 5 => '<label class="label label-warning"><span class="ready-button">{STATUS}</span></label>', 6 => '<label class="label label-default"><span class="extra-button">{STATUS}</span></label>', 7 => '<label class="label label-default"><span class="extra-button">{STATUS}</span></label>', 8 => '<label class="label label-default"><span class="extra-button">{STATUS}</span></label>', 9 => '<label class="label label-primary"><a class="createflight-button" href="{LINK}">Create Flight</a></label>');
                            $dateformte = Configure::read('Php.dateformte');
                            //pr($lineitems);
                            foreach ($lineitems as $report) {
								
							?>
							
							<table class="table table-striped table-bordered table-hover" id="reports" style="margin-bottom:40px;">
								<thead style="background-color: rgb(102, 102, 102);color:#fff;text-align:center;">
									<tr>
										
										<th colspan="2" style="text-align:center;">Line item Details </th>
										<th>Page Views</th>
										<th>Registrations</th>
										<th>Offer Views</th>
										<th>Takes</th>
										<!--<th>Leads<br><span style="font-size: 11px;">Accepted Rejected </span></th> -->
										<th>Revenue</th>
										<th>eCPM</th>
										<th>eRPM<br><span style="font-size: 11px;">Offerviews</span></th>
										<th>RPR</th>
										<th>Actions</th>
									</tr>
								</thead>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<?php
											$priceModel = '';
											if (array_key_exists($report['tcs']['cs_id'], $csnameArray)) {
												$priceModel = str_replace('{CSNAME}', $report['tcs']['cs_name'], $csnameArray[$report['tcs']['cs_id']]);
												} else {
												$priceModel = $report['tcs']['cs_name'];
											}
										?> 
										
										<?php
											$linkstatus = '';
											if ($report['tls']['ls_id'] == 2) {
												$linkstatus = Router::url(array('controller' => 'orders', 'action' => 'view_line_creatives', $report['tblLineItem']['li_id']));
												} else if ($report['tls']['ls_id'] == 9) {
												$linkstatus = Router::url(array('controller' => 'insertionorder', 'action' => 'add_line_item_flight', $report['tblLineItem']['li_id'], $report['tls']['ls_id']));
											}
											$stautsData = '';
											if (array_key_exists($report['tls']['ls_id'], $statusArray)) {
												$report['tls']['ls_id'];
												$status_span = str_replace('{STATUS}', $report['tls']['ls_status'], $statusArray[$report['tls']['ls_id']]);
												$stautsData = str_replace('{LINK}', $linkstatus, $status_span);
												// line
											}
											$TestLead = '';
											if (in_array($report['tcs']['cs_id'], array('3', '7'))) {
												$TestLead = '<a class="createflight-button"   href="' . Router::url(array('controller' => 'insertionorder', 'action' => 'test_lead_posting', $report['tblLineItem']['li_id'], $report['tls']['ls_id'])) . '" class="" data-toggle="modal" data-target="#myModal' . $report['tblLineItem']['li_id'] . '">Test Lead Posting</a><div id="myModal' . $report['tblLineItem']['li_id'] . '" class="modal fade"><div class="modal-dialog"><div class="modal-content"></div></div></div>';
											}
											$stopstatus = $playstatus = "display:none;";
											if ($report['tblLineItem']['tbl_lineitem_status'] == 1) {
												$stopstatus = "display:block;";
												} else {
												$playstatus = "display:block;";
											}
										?> 
										
										<thead>
											<tr>
												<th>
													<button style="<?php echo $stopstatus; ?>" id="play<?php echo $report['tblLineItem']['li_id']; ?>" onclick="return confirm('Are you sure want to pause?') ? actionPlay(2,<?php echo $report['tblLineItem']['li_id']; ?>) : '';"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></button> <button style="<?php echo $playstatus; ?>" id="stop<?php echo $report['tblLineItem']['li_id']; ?>" onclick="return confirm('Are you sure want to start?') ? actionPlay(1,<?php echo $report['tblLineItem']['li_id']; ?>) : '';"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
												</th>
											</tr>
										</thead>
										<tr><td><?php echo $priceModel; ?></td></tr>
										<tr><td><?php echo $stautsData; ?></td></tr>
										<tr><td>Active Flights&nbsp;&nbsp;<?php echo $report['total_active_flight']; ?></td></tr>
										<tr><td>Completed Flights &nbsp;&nbsp;<?php echo $report['total_active_completed_flight']; ?></td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th colspan="2">
													<?php echo $report['tblLineItem']['li_name']; ?>
												</th>
											</tr>
										</thead>
										<tr><td colspan="2"> <?php echo $report['tblp']['CompanyName']; ?></td></tr>
										<tr><td>Goal Vs Booked</td>
											<td><?php $percentage = getPercent($report['tblLineItem']['li_total'],$report['tblLineItem']['li_revenue']); ?>
												<div class="progress">
													<div style="border: 2px solid <?php echo getprocessbar($percentage); ?>;
													height: 20px;">
														<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage; ?>%;color:black;box-shadow: none;background-color:<?php echo getprocessbar($percentage); ?>"> &nbsp;&nbsp;<?php echo $percentage; ?>%</div>
													</div>
												</div>
											Booked $<?php echo ($report['tblLineItem']['li_revenue']) . " out of $" . $report['tblLineItem']['li_total']; ?></td></tr>
											<tr><td>Goal Vs Delivered</td>
												<td>                <div class="progress">
													<?php $percentage = getPercent($report['tblLineItem']['li_total'], $report['tblLineItem']['li_revenue']); ?>
													<div style="border: 2px solid <?php echo getprocessbar($percentage); ?>;
													height: 20px; ">
														<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage; ?>%;color:black;box-shadow: none;background-color:<?php echo getprocessbar($percentage); ?>"> &nbsp;&nbsp;<?php echo $percentage; ?>%</div>
													</div>
												</div>
												Delevered $<?php echo ($report['tblLineItem']['li_revenue']) . " out of $" . $report['tblLineItem']['li_total']; ?> </td></tr>
												<tr><td>Date</td><td>
												<?php echo (!empty($report['tblLineItem']['li_start_date'])) ? $this->Time->format($dateformte, $report['tblLineItem']['li_start_date']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?> / <?php echo (!empty($report['tblLineItem']['li_end_date'])) ? $this->Time->format($dateformte, $report['tblLineItem']['li_end_date']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?></td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													N/A
												</th>
											</tr>
										</thead>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													N/A
												</th>
											</tr>
										</thead>
										<tr><td>N/A</td></tr>
										<tr><td>N/A</td></tr>
										<tr><td>N/A</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													<?php echo $report['tblLineItem']['li_imp']; ?>
												</th>
											</tr>
										</thead>
										<tr><td>N/A</td></tr>
										<tr><td>N/A</td></tr>
										<tr><td> 
											<?php if (empty($report['tblReportDay']['impgraph'])) { ?>
												
												<ul class="noactivity"><li>No Acitivity Yet</li>
												</ul>
												<?php } else { ?>
												<img src="http://chart.apis.google.com/chart?chs=90x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($report['tblReportDay']['impgraph']) ? implode(',', array_values($report['tblReportDay']['impgraph'])) : 0); ?>">
											<?php } ?></td></tr>
											<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													<?php echo $report['tblLineItem']['li_accepted']; ?>
												</th>
											</tr>
										</thead>
										<tr><td>N/A</td></tr>
										<tr><td>N/A</td></tr>
										<tr><td>    <?php if (empty($report['tblReportDay']['takegraph'])) { ?>
											<ul class="noactivity"><li>No Acitivity Yet</li>
											</ul>
											<?php } else { ?>
											<img src="http://chart.apis.google.com/chart?chs=90x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($report['tblReportDay']['eCPMgraph']) ? implode(',', array_values($report['tblReportDay']['eCPMgraph'])) : 0); ?>">
										<?php } ?></td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													$<?php echo $report['tblLineItem']['li_revenue'] ?>
												</th>
											</tr>
										</thead>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													$<?php echo $ecpm = sprintf('%0.2f', ($report['tblLineItem']['li_revenue'] * 1000) / ($report['tblLineItem']['li_imp'] == 0 ? 1 : $report['tblLineItem']['li_imp'])); ?>
												</th>
											</tr>
										</thead>
										<tr><td>    
											<?php if (empty($report['tblReportDay']['eCPMgraph'])) { ?>
                                                <ul class="noactivity">
													<li>No Acitivity Yet</li>
												</ul>
												<?php } else { ?>
												<img src="http://chart.apis.google.com/chart?chs=90x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($report['tblReportDay']['eCPMgraph']) ? implode(',', array_values($report['tblReportDay']['eCPMgraph'])) : 0); ?>">
											<?php } ?>
										</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													N/A
												</th>
											</tr>
										</thead>
										<tr><td>N/A</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													N/A
												</th>
											</tr>
										</thead>
										<tr><td>N/A</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								<td>
									<table class="table table-striped table-bordered table-hover">
										
										<tr>
											<td>
												<a href="<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'edit_line_item', $report['tblLineItem']['li_id'])); ?>">Edit</a>
											</td>
										</tr>
										
										<tr><td><a href="<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'add_line_item_flight', $report['tblLineItem']['li_id'], $report['tls']['ls_id'])); ?>">Create Flight</a></td></tr>
										<tr><td><a href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'view_flights', $report['tblLineItem']['li_id'])); ?>">View Flights</a></td></tr>
										<tr><td><a href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'view_line_creatives', $report['tblLineItem']['li_id'])); ?>">View / Upload Creatives</a></td></tr>
										<tr><td>&nbsp;</td></tr>
									</table>
								</td>
								
							</table>
							<?php } 
						}
						
					?>
					
				</div>
				
				<div class="table-responsive1">
					<table class="table table-striped table-bordered table-hover">
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <?php if (!$lineitems) { ?>
                                        <div style='color:#FF0000'>No Record Found</div>
                                    <?php } else { ?>

                                        <ul class="pagination">

                                            <?php if ($this->Paginator->first()) { ?><li><?php echo $this->Paginator->first('« First', array('class' => '')); ?></li>
                                            <?php } ?>										
                                            <?php if ($this->Paginator->hasPrev()) { ?>
                                                <li><?php echo $this->Paginator->prev('< Previous', array('class' => ''), null, array('class' => 'disabled')); ?>&nbsp;  &nbsp;</li>
                                            <?php } ?>

                                            <?= $this->Paginator->numbers(array('modulus' => 6, 'tag' => 'li', 'class' => '', 'separator' => '')); ?>
                                            <?php if ($this->Paginator->hasNext()) { ?>

                                                <li><?php echo $this->Paginator->next('Next >', array('class' => '')); ?></li>
                                            <?php } ?>
                                            <?php if ($this->Paginator->last()) { ?>
                                                <li><?php echo $this->Paginator->last('Last »', array('class' => '')); ?></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>


                                </td>
                            </tr>
                        </tfoot>

                    </table>
                </div>

            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>
<script>
	
	jQuery(document).ready(function () {
		
		
		//$('input[name="data[daterange]"]').daterangepicker();
		
		$(function () {
			
			var str = $('input[name="data[daterange]"]').val();
			if(str != ''){
				var res = str.split("-");
				//alert(res[0]);
				var start = res[0];
				var end = res[1];
				}else{
				var start = moment().subtract(29, 'days');
				var end = moment();
			}
            function cb(start, end) {
                //$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			}
			
			$('input[name="data[daterange]"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
				
				
			});
			
			$('input[name="data[daterange]"]').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
				
			});
			cb(start, end);
			
			$('input[name="data[daterange]"]').daterangepicker({
				"alwaysShowCalendars": true,
				//startDate": start,
				//"endDate": end,
				"opens": "left",
				"autoUpdateInput": false,
				ranges: {
					'Today': [moment(), moment()],
					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			}, cb);
			
		});
		$(".js-company-array").select2({
			placeholder: "Select Advertiser",
			maximumSelectionLength: 1,
			//allowClear: true
		});
		$(".js-order-array").select2({
			placeholder: "Select Orders",
			//allowClear: true
		});
		$(".js-satus-array").select2({
			placeholder: "Status",
			//allowClear: true
		});
		$(".js-media-array").select2({
			placeholder: "Media Type",
			//allowClear: true
		});
		
	});
	function actionPlay(action, li_id) {
		var id = '';
		if (action == 2) { // hide here play and show stop button
			var play_id = "play" + li_id;
			var stop_id = "stop" + li_id;
			} else {
			var play_id = "stop" + li_id;
			var stop_id = "play" + li_id;
		}
		
		$.ajax({
			type: "POST",
			url: "<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'update_play_action'), true); ?>/" + action + "/" + li_id,
			dataType: "json",
			success: function (data) {
				if (data == 1) {
					$("#" + play_id).hide();
					$("#" + stop_id).show();
					} else {
					alert("There was an error. Try again please!");
				}
				}, error: function (data) {
				alert("There was an error. Try again please!");
			}
		});
		
	}
	
	$('#company_id').change(function () {
		// Remove all Manage Lead Allocation
		var favorite = [];
		var selectedItem = [];
		$.each($("#company_id :selected"), function () {
			favorite.push($(this).val());
		});
		$(".js-order-array").empty();
		$(".js-order-array").select2({
			placeholder: "Select Orders",
			allowClear: true
		});
		
		var selecid = favorite.join(",");
		
		//alert("My favourite sports are: " + favorite.join(", "));
		$.ajax({
			type: "POST",
			url: "<?php echo Router::url(array('controller' => 'orders', 'action' => 'getAllOrderList')); ?>",
			data: 'user_id=' + favorite.join(","),
			dataType: "json",
			success: function (data) {
				var count = 0;
				$(".js-order-array").select2({
					data: data
				});
				$(".js-order-array").select2({
					placeholder: "Select Orders",
				});
				
			}
		});
		
	});
	
</script>

<?php

function getprocessbar($process) {
    $color = 'red';
    if ($process >= 0 && $process < 50) {
        $color = 'red';
    } else if ($process >= 50 && $process < 75) {
        $color = '#EB661E';
    } else if ($process >= 75 && $process < 90) {
        $color = 'orange';
    } else if ($process >= 90 && $process <= 100) {
        $color = 'green';
    }

    return $color;
}

function getPercent($total, $goal) {

    if ($total == 0)
        return 0;
    return sprintf('%0.2f', ($goal / $total ) * 100);
}
?>


