
<!-- BEGIN PAGE CONTENT INNER -->
<style>
th, td{
    text-align: center;
}
</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Adunit Reports</span>
                </div>
				<?php $optt = $this->Session->read('optt');
					//echo $optt;
					?>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
					<?php if(!empty($optt) && $optt == 1) {  ?>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
						<input type="radio" name="options" class="toggle" id="option1" value="1">Created By Self</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
						<input type="radio" name="options" class="toggle" id="option2" value="2">Created By Others</label>
						<?php } else if(!empty($optt) && $optt == 2) { ?>
							<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
						<input type="radio" name="options" class="toggle" id="option1" value="1">Created By Self</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
						<input type="radio" name="options" class="toggle" id="option2" value="2">Created By Others</label>
						<?php } else { ?>
					
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
						<input type="radio" name="options" class="toggle" id="option1" value="1">Created By Self</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
						<input type="radio" name="options" class="toggle" id="option2" value="2">Created By Others</label>
					<?php }	?>
					
					</div>
				</div>
				
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <?php echo $this->Form->create(false, array('url' => array('controller' => 'orders', 'action' => 'adunit_reports'), 'class' => 'form-horizontal', 'id' => 'reportsForm')); ?>
                    <?php echo $this->Form->hidden('submit_value', array('value' => 'submit')); ?>
                    <div class="row">

                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('company_id', array(
                                    'class' => 'js-company-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Select Company',
                                    'options' => $publishers
                                ));
                                ?>
                            </div>
                        </div>
						 <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('media_id', array(
                                    'class' => 'js-media-array form-control',
                                    'label' => false,
                                    'type' => 'select',
									'multiple' => true,
									'onchange' => 'ShowProductType(this.value)',
                                    'empty' => 'Select Media Name',
                                    'options' => $mediatypelist
                                ));
                                ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('product_id', array(
                                    'class' => 'js-producttype-array form-control',
                                    'label' => false,
                                    'type' => 'select',
									'multiple' => true,
                                    'id' => 'producttype_content',
									'empty' => 'Select Product type'
									));
                                ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
                                echo $this->Form->input('cost_id', array(
                                    'class' => 'js-coststructure-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => 'Select Cost Structure',
                                    'options' => $costStructure
                                ));
                                ?>
                            </div>
							
							
                        </div>
                       
                        <div class="col-md-2">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php echo $this->Form->input('daterange', array('label' => false, 'div' => false, 'class' => 'form-control', 'style'=>'padding-right: 0;','placeholder' => 'Date Range')); ?> 	<span></span>


                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="btn-group">
                                <button type="submit" class="btn blue btn-secondary">Filter</button> 
                            </div>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
				
				 <div class="table-responsive1">
				 
				 <?php 
						if (!empty($tblAdunits)) {
							foreach ($tblAdunits as $tblAdunit) { 
								if ($tblAdunit['tbmt']['mtype_id'] == "1" || $tblAdunit['tbmt']['mtype_id'] == "4") {  ?>
								
								<table class="table table-striped table-bordered table-hover" id="reports" style="margin-bottom:40px;">
									<thead style="background-color: rgb(102, 102, 102);color:#fff;text-align:center;">
										<tr>
											<th>Ad Unit Details 
												<?php if ($tblAdunit['tbmt']['mtype_id'] == "1") {  ?>
												(Desktop)
												<?php } else if ($tblAdunit['tbmt']['mtype_id'] == "4") {  ?>
												(Social)
												<?php } else { } ?>
												</th>
											<th>Page Views</th>
											<th>Viewable Impressions</th>
											<th>Ad Impressions</th>
											<th>Clicks</th>
											<th>Conversions</th>
											<th>Ad Unit CTR</th>
											<th>Revenue</th>
											<th>Page eRPM</th>
											<th>Ad Unit eRPM</th>
											<th>Ad Unit eCPC</th>
											<?php if(!empty($optt) && $optt == 1){ ?>
											<th>Actions</th>
											<?php } else if(!empty($optt) && $optt == 2){ ?>
											<?php } else { ?>
											<th>Actions</th>
											<?php } ?>
										</tr>
									</thead>
									
									<tr>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th colspan="2">
															<?php
																
																echo $tblAdunit['tblAdunit']['adunit_name'];
																
															?>
														</th>
													</tr>
												</thead>
												<tr>
												<?php if(!empty($optt) && $optt == 2){ ?>
												<td> <b>Created By</b> </td>
												<td><b><?php echo $tblAdunit['OwnerCompanyName']; ?></b></td>
												<?php } else { ?>
												<td> Impressions</td>
													<td>N/A</td>
													<?php } ?>
												</tr>
												<tr>
													<td> CTR</td>
													<td>N/A</td>
												</tr>
												<tr>
													<td>Viewability</td>
													<td><?php echo empty($tblAdunit['tblAdunit']['ad_desc']) ? "N/A" : $tblAdunit['tblAdunit']['ad_desc'] ?></td>
												</tr>
												<tr>
													<td>Active Flights</td>
													<td><?php echo empty($tblAdunit[0]['li_id']) ? 0 : $tblAdunit[0]['li_id'] ?></td>
												</tr>
											</table>
											
										</td>
										<td> 
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												
											</table>	
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>
													N/A
												</td></tr>
												<tr><td>N/A</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:90,129,102,48,87,117,111,132,90,99,45,63,87,102,120,120,69,48,63,18"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															
															<?php echo empty($tblAdunit['tblReportDay']['rd_imp']) ? "0" : $tblAdunit['tblReportDay']['rd_imp'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>
													N/A	
												</td></tr>
												<tr><td>
													<?php
														$open = 0;
														if(empty($tblAdunit['tblReportDay']['rd_imp'])){
															$open = 0;
															}else{
															$open = $tblAdunit['tblReportDay']['rd_imp'];		
														}
														$openlast = 0;	
														if(empty($tblAdunit['tblReportDayLast']['rd_imp'])){
															$openlast = 0;
															}else{
															$openlast = $tblAdunit['tblReportDayLast']['rd_imp']	;	
														}
														$openPer2 = 0;
														if($open == 0 && $openlast == 0){
															$openPer2 = 0;
															}else if ($openlast == 0){
															$openPer2 = 100;
															}else if($open == 0 ){
															$openPer2 = -100;
															}else{
															$V2 =  $open;
															$V1 = $openlast;
															$openPer2 = ((($V2 - $V1)/$V1)*100);
															
															$openPer = ((($open - $openlast)/(($open + $openlast)/2))*100);
															
														}
														echo $openlast."<br/>";
													?>
													<?php if($openPer2 > 0){ ?>
														<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
														<?php }else{ ?>
														<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
													<?php } ?>
													<?php  echo number_format($openPer2,2)."%"; ?>		
													
													
												</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=90x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:<?php echo (!empty($tblAdunit['tblReportDay']['impgraph']) ? implode(',', array_values($tblAdunit['tblReportDay']['impgraph'])) : 0); ?>"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($tblAdunit['tblReportDay']['rd_accepted']) ? "0" : $tblAdunit['tblReportDay']['rd_accepted'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>
													N/A
												</td></tr>
												<tr><td>
													
													<?php
														$take = 0;
														if(empty($tblAdunit['tblReportDay']['rd_accepted'])){
															$take = 0;
															}else{
															$take = $tblAdunit['tblReportDay']['rd_accepted'];		
														}
														$takelast = 0;	
														if(empty($tblAdunit['tblReportDayLast']['rd_accepted'])){
															$takelast = 0;
															}else{
															$takelast = $tblAdunit['tblReportDayLast']['rd_accepted']	;	
														}
														$takePer2 = 0;
														if($take == 0 && $takelast == 0){
															$takePer2 = 0;
															}else if ($takelast == 0){
															$takePer2 = 100;
															}else if($take == 0 ){
															$takePer2 = -100;
															}else{
															$V2 =  $take;
															$V1 = $takelast;
															$takePer2 = ((($V2 - $V1)/$V1)*100);
															
															$takePer = ((($take - $takelast)/(($take + $takelast)/2))*100);
															
														}
														echo $takelast."<br/>";
													?>
													<?php if($takePer2 > 0){ ?>
														<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
														<?php }else{ ?>
														<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
													<?php } ?>
													<?php  echo number_format($takePer2,2)."%"; ?>
													
												</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($tblAdunit['tblReportDay']['takegraph']) ? implode(',', array_values($tblAdunit['tblReportDay']['takegraph'])) : 0); ?>"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															No Pixel
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($tblAdunit['tblReportDay']['rd_revenue']) ? "$0" : "$".$tblAdunit['tblReportDay']['rd_revenue'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															&nbsp;
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<?php if((!empty($optt) && $optt == 1) || ($optt == 3)){ ?>
										<td>
											<table class="table table-striped table-bordered table-hover">
												
												<tr>
													<td>
														
														<?php echo $this->Html->link('Edit Ad Unit', array('controller' => 'inventory_management', 'action' => 'editAdUnit', $tblAdunit['tblAdunit']['adunit_id'])); ?>
													</td>
												</tr>
												
												<tr><td>
													
													<?php echo $this->Html->link('View Flights', array('controller' => 'orders', 'action' => 'flight_reports', $tblAdunit['tblAdunit']['adunit_id'])); ?>
													
													
												</td></tr>
												<tr><td><a href="#" data-toggle="modal" data-target="#myModal">Add Flights</a></td></tr>
												<tr><td><a href="#">View Audience Profile</a></td></tr>
												<tr><td><a href="#">View Avails & STR Report</a></td></tr>
											</table>
										</td>
										<?php } ?>
									</tr>
								</table>
								
								
								<?php } else if ($tblAdunit['tbmt']['mtype_id'] == "2") {
								?>
								<table class="table table-striped table-bordered table-hover" id="reports" style="margin-bottom:40px;">
									<thead style="background-color: rgb(102, 102, 102);color:#fff;text-align:center;">
										<tr>
											<th>Ad Unit Details (Email)</th>
											<th>Sent</th>
											<th>Delivered</th>
											<th>Opens</th>
											<th>Clicks/CTR</th>
											<th>Conversions</th>
											<th>Revenue</th>
											<th>eCPM<br><span style="font-size: 11px;">Sends</span></th>
											<th>eCPM<br><span style="font-size: 11px;">Opens</span></th>
											<th>eCPC</th>
											<?php if(!empty($optt) && $optt == 1){ ?>
											<th>Actions</th>
											<?php } else if(!empty($optt) && $optt == 2){ ?>
											<?php } else { ?>
											<th>Actions</th>
											<?php } ?>
										</tr>
									</thead>
									
									<tr>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th colspan="2">
															<?php
																
																echo $tblAdunit['tblAdunit']['adunit_name'];
																
															?>
														</th>
													</tr>
												</thead>
												<tr>
												<?php if(!empty($optt) && $optt == 2){ ?>
												<td> <b>Created By </b></td>
												<td><b><?php echo $tblAdunit['OwnerCompanyName']; ?></b></td>
												<?php } else { ?>
												<td> Total List Size</td>
													<td><?php echo empty($tblAdunit['tblAdunit']['ad_list_size']) ? "N/A" : $tblAdunit['tblAdunit']['ad_list_size'] ?></td>
												<?php } ?>
														
												</tr>
												<tr>
													<td> Frequency</td>
													<td><span style="color:green;font-weight:600;">M T W T F</span> <span style="color:red;font-weight:600;">S S</span></td>
												</tr>
												<tr>
													<td> Ad Unit Desc</td>
													<td><?php echo empty($tblAdunit['tblAdunit']['ad_desc']) ? "N/A" : $tblAdunit['tblAdunit']['ad_desc'] ?></td>
												</tr>
												<tr>
													<td>Active Flights</td>
													<td><?php echo empty($tblAdunit[0]['li_id']) ? 0 : $tblAdunit[0]['li_id'] ?></td>
												</tr>
											</table>
											
										</td>
										<td> 
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															
															<?php echo empty($tblAdunit[0]['totalsent']) ? "0" : $tblAdunit[0]['totalsent'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												
											</table>	
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($tblAdunit[0]['delivered']) ? "0" : $tblAdunit[0]['delivered'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>
													<?php 
														if(!empty($tblAdunit[0]['totalsent'])){
															$totalsent = $tblAdunit[0]['totalsent'];
															$delivered = $tblAdunit[0]['delivered'];
															$delper = ($delivered *100)/$totalsent;
															echo number_format($delper,2)."%";
															}else{
															echo "0%";
														}
													?>
												</td></tr>
												<tr><td>N/A</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:90,129,102,48,87,117,111,132,90,99,45,63,87,102,120,120,69,48,63,18"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($tblAdunit['tblReportDay']['rd_imp']) ? "0" : $tblAdunit['tblReportDay']['rd_imp'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>
													<?php
														if(!empty($tblAdunit[0]['totalsent'])){
															$totalsent = $tblAdunit[0]['totalsent'];
															$opens = $tblAdunit['tblReportDay']['rd_imp'];
															$opensper = ($opens *100)/$totalsent;
															echo number_format($opensper,2)."%";
															}else{
															echo "0%";
														}
													?>
												</td></tr>
												<tr><td>
													
													<?php
														$open = 0;
														if(empty($tblAdunit['tblReportDay']['rd_imp'])){
															$open = 0;
															}else{
															$open = $tblAdunit['tblReportDay']['rd_imp'];		
														}
														$openlast = 0;	
														if(empty($tblAdunit['tblReportDayLast']['rd_imp'])){
															$openlast = 0;
															}else{
															$openlast = $tblAdunit['tblReportDayLast']['rd_imp']	;	
														}
														$openPer2 = 0;
														if($open == 0 && $openlast == 0){
															$openPer2 = 0;
															}else if ($openlast == 0){
															$openPer2 = 100;
															}else if($open == 0 ){
															$openPer2 = -100;
															}else{
															$V2 =  $open;
															$V1 = $openlast;
															$openPer2 = ((($V2 - $V1)/$V1)*100);
															
															$openPer = ((($open - $openlast)/(($open + $openlast)/2))*100);
															
														}
														echo $openlast."<br/>";
													?>
													<?php if($openPer2 > 0){ ?>
														<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
														<?php }else{ ?>
														<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
													<?php } ?>
													<?php  echo number_format($openPer2,2)."%"; ?>		
													
												</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=90x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:<?php echo (!empty($tblAdunit['tblReportDay']['impgraph']) ? implode(',', array_values($tblAdunit['tblReportDay']['impgraph'])) : 0); ?>"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($tblAdunit['tblReportDay']['rd_accepted']) ? "0" : $tblAdunit['tblReportDay']['rd_accepted'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>
													<?php
														if(!empty($tblAdunit[0]['totalsent'])){
															$totalsent = $tblAdunit[0]['totalsent'];
															$clicks = $tblAdunit['tblReportDay']['rd_accepted'];
															$clicksper = ($clicks *100)/$totalsent;
															echo number_format($clicksper,2)."%";
															}else{
															echo "0%";
														}
													?>
												</td></tr>
												<tr><td>
													<?php
														$accepted = 0;
														if(empty($tblAdunit['tblReportDay']['rd_accepted'])){
															$accepted = 0;
															}else{
															$accepted = $tblAdunit['tblReportDay']['rd_accepted'];		
														}
														$acceptedlast = 0;	
														if(empty($tblAdunit['tblReportDayLast']['rd_accepted'])){
															$acceptedlast = 0;
															}else{
															$acceptedlast = $tblAdunit['tblReportDayLast']['rd_accepted']	;	
														}
														$acceptedPer2 = 0;
														if($accepted == 0 && $acceptedlast == 0){
															$acceptedPer2 = 0;
															}else if ($acceptedlast == 0){
															$acceptedPer2 = 100;
															}else if($accepted == 0 ){
															$acceptedPer2 = -100;
															}else{
															$V2 =  $accepted;
															$V1 = $acceptedlast;
															$acceptedPer2 = ((($V2 - $V1)/$V1)*100);
															
															$acceptedPer = ((($accepted - $acceptedlast)/(($accepted + $acceptedlast)/2))*100);
															
														}
														echo $acceptedlast."<br/>";
													?>
													<?php if($acceptedPer2 > 0){ ?>
														<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
														<?php }else{ ?>
														<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
													<?php } ?>
													<?php  echo number_format($acceptedPer2,2)."%"; ?>
												</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($tblAdunit['tblReportDay']['takegraph']) ? implode(',', array_values($tblAdunit['tblReportDay']['takegraph'])) : 0); ?>"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															No Pixel
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($tblAdunit['tblReportDay']['rd_revenue']) ? "$0" : "$".$tblAdunit['tblReportDay']['rd_revenue'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php
																if(!empty($tblAdunit[0]['totalsent'])&& !empty($tblAdunit['tblReportDay']['rd_revenue'])){
																	$totalsent = $tblAdunit[0]['totalsent'];
																	$revenue = $tblAdunit['tblReportDay']['rd_revenue'];
																	$revenuesent = ($revenue *1000)/$totalsent;
																	echo "$".number_format($revenuesent,2);
																	}else{
																	echo "$0";
																}
															?>
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php
																if(!empty($tblAdunit[0]['opens'])&& !empty($tblAdunit['tblReportDay']['rd_revenue'])){
																	$opens = $tblAdunit[0]['opens'];
																	$revenue = $tblAdunit['tblReportDay']['rd_revenue'];
																	$revenueopen = ($revenue *1000)/$opens;
																	echo "$".number_format($revenueopen,2);
																	}else{
																	echo "$0";
																}
															?>
														</th>
													</tr>
												</thead>
												<tr><td><i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>0.12%</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php
																if(!empty($tblAdunit[0]['clicks'])&& !empty($tblAdunit['tblReportDay']['rd_revenue'])){
																	$clicks = $tblAdunit[0]['clicks'];
																	$revenue = $tblAdunit['tblReportDay']['rd_revenue'];
																	$revenueclick = $revenue/$clicks;
																	echo "$".number_format($revenueclick,2);
																	}else{
																	echo "$0";
																}
															?>
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<?php if((!empty($optt) && $optt == 1) || ($optt == 3)){ ?>
										<td>
											<table class="table table-striped table-bordered table-hover">
												
												<tr>
													<td>
														
														<?php echo $this->Html->link('Edit Ad Unit', array('controller' => 'inventory_management', 'action' => 'editAdUnit', $tblAdunit['tblAdunit']['adunit_id'])); ?>
													</td>
												</tr>
												
												<tr><td>
													<?php echo $this->Html->link('View Flights', array('controller' => 'orders', 'action' => 'flight_reports', $tblAdunit['tblAdunit']['adunit_id'])); ?>
												</td></tr>
												
												
												<tr><td><a href="#" data-toggle="modal" data-target="#myModal">Add Flights</a></td></tr>
												<tr><td><a href="#">View Audience Profile</a></td></tr>
												<tr><td><a href="#">View Avails & STR Report</a></td></tr>
											</table>
										</td>
										<?php } ?>
									</tr>
									
								</table>
								
								<?php } else if ($tblAdunit['tbmt']['mtype_id'] == "5") { ?>
								
								<table class="table table-striped table-bordered table-hover" id="reports" style="margin-bottom:40px;">
									<thead style="background-color: rgb(102, 102, 102);color:#fff;text-align:center;">
										<tr>
											<th>Ad Unit Details (Lead-Gen)</th>
											<th>Page Views</th>
											<th>Registrations</th>
											<th>Offer Views</th>
											<th>Takes</th>
											<th>Leads<br><span style="font-size: 11px;">Accepted Rejected </span></th>
											<th>Revenue</th>
											<th>eRPM<br><span style="font-size: 11px;">Page Views</span></th>
											<th>eRPM<br><span style="font-size: 11px;">Offerviews</span></th>
											<th>RPR</th>
											<?php if(!empty($optt) && $optt == 1){ ?>
											<th>Actions</th>
											<?php } else if(!empty($optt) && $optt == 2){ ?>
											<?php } else { ?>
											<th>Actions</th>
											<?php } ?>
										</tr>
									</thead>
									
									<tr>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th colspan="2">
															<?php
																
																echo $tblAdunit['tblAdunit']['adunit_name'];
																
															?>
														</th>
													</tr>
												</thead>
												<tr>
													<?php if(!empty($optt) && $optt == 2){ ?>
												<td> <b>Created By </b></td>
												<td><b><?php echo $tblAdunit['OwnerCompanyName']; ?></b></td>
												<?php } else { ?>
												<td> Registrations</td>
													<td>N/A</td>
													<?php } ?>
												</tr>
												<tr>
													<td> Leads</td>
													<td>N/A</td>
												</tr>
												<tr>
													<td> Revenue per Registration</td>
													<td><?php echo empty($tblAdunit['tblAdunit']['ad_desc']) ? "N/A" : $tblAdunit['tblAdunit']['ad_desc'] ?></td>
												</tr>
												<tr>
													<td>Active Flights</td>
													<td><?php echo empty($tblAdunit[0]['li_id']) ? 0 : $tblAdunit[0]['li_id'] ?></td>
												</tr>
											</table>
											
										</td>
										<td> 
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												
											</table>	
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>
													N/A
												</td></tr>
												<tr><td>N/A</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:90,129,102,48,87,117,111,132,90,99,45,63,87,102,120,120,69,48,63,18"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															
															<?php echo empty($tblAdunit['tblReportDay']['rd_imp']) ? "0" : $tblAdunit['tblReportDay']['rd_imp'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>
													N/A	
												</td></tr>
												<tr><td>
													<?php
														$open = 0;
														if(empty($tblAdunit['tblReportDay']['rd_imp'])){
															$open = 0;
															}else{
															$open = $tblAdunit['tblReportDay']['rd_imp'];		
														}
														$openlast = 0;	
														if(empty($tblAdunit['tblReportDayLast']['rd_imp'])){
															$openlast = 0;
															}else{
															$openlast = $tblAdunit['tblReportDayLast']['rd_imp']	;	
														}
														$openPer2 = 0;
														if($open == 0 && $openlast == 0){
															$openPer2 = 0;
															}else if ($openlast == 0){
															$openPer2 = 100;
															}else if($open == 0 ){
															$openPer2 = -100;
															}else{
															$V2 =  $open;
															$V1 = $openlast;
															$openPer2 = ((($V2 - $V1)/$V1)*100);
															
															$openPer = ((($open - $openlast)/(($open + $openlast)/2))*100);
															
														}
														echo $openlast."<br/>";
													?>
													<?php if($openPer2 > 0){ ?>
														<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
														<?php }else{ ?>
														<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
													<?php } ?>
													<?php  echo number_format($openPer2,2)."%"; ?>		
													
													
												</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=90x16&amp;cht=ls&amp;chco=0077CC&amp;chm=B,E6F2FA,0,0,0&amp;chls=1,0,0&amp;chd=t:<?php echo (!empty($tblAdunit['tblReportDay']['impgraph']) ? implode(',', array_values($tblAdunit['tblReportDay']['impgraph'])) : 0); ?>"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($tblAdunit['tblReportDay']['rd_accepted']) ? "0" : $tblAdunit['tblReportDay']['rd_accepted'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>
													N/A
												</td></tr>
												<tr><td>
													
													<?php
														$take = 0;
														if(empty($tblAdunit['tblReportDay']['rd_accepted'])){
															$take = 0;
															}else{
															$take = $tblAdunit['tblReportDay']['rd_accepted'];		
														}
														$takelast = 0;	
														if(empty($tblAdunit['tblReportDayLast']['rd_accepted'])){
															$takelast = 0;
															}else{
															$takelast = $tblAdunit['tblReportDayLast']['rd_accepted']	;	
														}
														$takePer2 = 0;
														if($take == 0 && $takelast == 0){
															$takePer2 = 0;
															}else if ($takelast == 0){
															$takePer2 = 100;
															}else if($take == 0 ){
															$takePer2 = -100;
															}else{
															$V2 =  $take;
															$V1 = $takelast;
															$takePer2 = ((($V2 - $V1)/$V1)*100);
															
															$takePer = ((($take - $takelast)/(($take + $takelast)/2))*100);
															
														}
														echo $takelast."<br/>";
													?>
													<?php if($takePer2 > 0){ ?>
														<i style="font-size:18px;color:green;margin-right:3px;" class="fa fa-arrow-up"></i>
														<?php }else{ ?>
														<i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>	
													<?php } ?>
													<?php  echo number_format($takePer2,2)."%"; ?>
													
												</td></tr>
												<tr><td><img src="http://chart.apis.google.com/chart?chs=70x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($tblAdunit['tblReportDay']['takegraph']) ? implode(',', array_values($tblAdunit['tblReportDay']['takegraph'])) : 0); ?>"></td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															<?php echo empty($tblAdunit['tblReportDay']['rd_accepted']) ? "0" : $tblAdunit['tblReportDay']['rd_accepted'] ?>
															
														</th>
													</tr>
												</thead>
												<tr><td>	
												<?php 
													$rejected = 0;
													if(empty($tblAdunit['tblReportDay']['rd_delivered'])){
													$rejected = 0;
													}else{
													$delivered = $tblAdunit['tblReportDay']['rd_delivered'];
													$accepted = $tblAdunit['tblReportDay']['rd_accepted'];
													$rejected = $delivered - $accepted ; 
													}
													echo $rejected ;
												?>
												</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
														<?php echo empty($tblAdunit['tblReportDay']['rd_revenue']) ? "$0" : "$".$tblAdunit['tblReportDay']['rd_revenue'] ?>
														</th>
													</tr>
												</thead>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td><i style="font-size:18px;color:red;margin-right:3px;" class="fa fa-arrow-down"></i>0.12%</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<td>
											<table class="table table-striped table-bordered table-hover">
												<thead>
													<tr>
														<th>
															N/A
														</th>
													</tr>
												</thead>
												<tr><td>N/A</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
												<tr><td>&nbsp;</td></tr>
											</table>
										</td>
										<?php if((!empty($optt) && $optt == 1) || ($optt == 3)){ ?>
										
											
										<td>
											<table class="table table-striped table-bordered table-hover">
												
												<tr>
													<td>
														
														<?php echo $this->Html->link('Edit Ad Unit', array('controller' => 'inventory_management', 'action' => 'editAdUnit', $tblAdunit['tblAdunit']['adunit_id'])); ?>
													</td>
												</tr>
												
												<tr><td>
													
													<?php echo $this->Html->link('View Flights', array('controller' => 'orders', 'action' => 'flight_reports', $tblAdunit['tblAdunit']['adunit_id'])); ?>
													
													
												</td></tr>
												<tr><td><a href="#" data-toggle="modal" data-target="#myModal">Add Flights</a></td></tr>
												<tr><td><a href="#">View Audience Profile</a></td></tr>
												<tr><td><a href="#">View Avails & STR Report</a></td></tr>
											</table>
										</td>
										<?php } ?>
									</tr>
								</table>
								
							<?php } ?>
							
							
							
							<?php
							}
						} ?>
				</div>
				
				<div class="table-responsive1">
				<table class="table table-striped table-bordered table-hover">
				       <tfoot>
                            <tr>
                                <td colspan="11">
                                    <?php if (!$tblAdunits) { ?>
                                        <div style='color:#FF0000'>No Record Found</div>
                                    <?php } else { ?>

                                        <ul class="pagination">

                                            <?php if ($this->Paginator->first()) { ?><li><?php echo $this->Paginator->first('« First', array('class' => '')); ?></li>
                                            <?php } ?>										
                                            <?php if ($this->Paginator->hasPrev()) { ?>
                                                <li><?php echo $this->Paginator->prev('< Previous', array('class' => ''), null, array('class' => 'disabled')); ?>&nbsp;  &nbsp;</li>
                                            <?php } ?>

                                            <?= $this->Paginator->numbers(array('modulus' => 6, 'tag' => 'li', 'class' => '', 'separator' => '')); ?>
                                            <?php if ($this->Paginator->hasNext()) { ?>

                                                <li><?php echo $this->Paginator->next('Next >', array('class' => '')); ?></li>
                                            <?php } ?>
                                            <?php if ($this->Paginator->last()) { ?>
                                                <li><?php echo $this->Paginator->last('Last »', array('class' => '')); ?></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>


                                </td>
                            </tr>
                        </tfoot>
				</table>
				
				</div>

               
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
	<div id="myModal" class="modal fade">
		
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close AddCompanyiframe" data-dismiss="modal">&times;</button>
				</div>
		<form class="" method="post" >
				<div class="row">
				
				  <div class="col-md-6">
                            <div class="form-group">

                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <label class="control-label col-md-4">All Orders <span class="required">
                                                * </span></label>
                                        <i class="fa"></i>
                                        <?php
                                        echo $this->Form->input('order_id.', array(
                                            'class' => 'js-order-array1 form-control',
                                            'label' => false,
                                            'type' => 'select',
                                         //   ' multiple' => 'multiple',
                                            'empty' => ' Please select Order',
                                            'selected' => (!empty($this->request->data['Lead']['order_id'])) ? $this->request->data['Lead']['order_id'] : array(),
                                            'options' => $orderlist
                                        ));
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">

                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <label class="control-label col-md-4"> All Line</label>
                                        <i class="fa"></i>
                                        <?php
                                        echo $this->Form->input('line_item.', array(
                                            'class' => 'js-line-array1 form-control',
                                            'label' => false,
                                            'type' => 'select',
                                           // ' multiple' => 'multiple',
                                            'empty' => ' Please select Order',
                                            'selected' => (!empty($this->request->data['Lead']['line_item'])) ? $this->request->data['Lead']['line_item'] : array(),
                                            'options' => $tblLineItemswithstatus
                                        ));
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
				 
				</div>
			
				<div class="modal-footer">
				<button type="button" class="btn btn-default" onclick="myJsFunc();">Add Flight</button>
					<button type="button" class="btn btn-default AddCompanyiframe" data-dismiss="modal">Close</button>
				</div>
				</form>
			<!-- Content will be loaded here from "remote.php" file -->
			</div>
		</div>
	</div>

<!-- END PAGE CONTENT INNER -->
<style>
    #sample_4_filter { 
        float:right;
    }
	
	#myModal .modal-dialog {
    width: 926px;
    margin: 30px auto;
	
}
.modal {
   z-index:0; 
}
</style>
<script>
function myJsFunc() {
var e = document.getElementById("line_item");
var str = e.options[e.selectedIndex].value
var res = str.split("-");
var li_id = res[0];
var li_status = res[1];
//var strSel = "The Value is: " + e.options[e.selectedIndex].value + " and text is: " + e.options[e.selectedIndex].text;
   // alert("id :" +li_id + "status" +li_status);
window.location.href = '/insertionorder/add_line_item_flight/'+li_id+'/'+li_status;
   
}
</script>

<script>

    jQuery(document).ready(function () {

        $(function () {
			
			var str = $('input[name="data[daterange]"]').val();
			if(str != ''){ 
			var res = str.split("-");
			//alert(res[0]);
			var start = res[0];
			var end = res[1];
			}else{
			var start = moment().subtract(29, 'days');
			var end = moment();
			}
            function cb(start, end) {
                //$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			}
			
			$('input[name="data[daterange]"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
				
				
			});
			
			$('input[name="data[daterange]"]').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
				
			});
			
            cb(start, end);
            $('input[name="data[daterange]"]').daterangepicker({
				"alwaysShowCalendars": true,
					"startDate": start,
					"endDate": end,
					"opens": "left",
				//"autoUpdateInput": false,
				
				
				ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

        });
        $(".js-company-array").select2({
            placeholder: "Select Company",
            maximumSelectionLength: 1,
            //allowClear: true
        });
        $(".js-producttype-array").select2({
            placeholder: "Select Product Type ",
			maximumSelectionLength: 1,
            //allowClear: true
        });
        $(".js-coststructure-array").select2({
            placeholder: "Select Price Format",
			maximumSelectionLength: 1,
            //allowClear: true
        });
        $(".js-media-array").select2({
            placeholder: "Select Media Type",
			maximumSelectionLength: 1,
            //allowClear: true
        });
		
		 $(".js-order-array1").select2({
            placeholder: "Select (or start typing) the order name",
			//maximumSelectionLength: 1,
            allowClear: true
        });
		 $(".js-line-array1").select2({
            placeholder: "Select (or start typing) the line item name",
			//maximumSelectionLength: 1,
            allowClear: true
        });

    });
	
	   $('#order_id').change(function () {

                // Remove all Manage Lead Allocation
                var favorite = [];
                var selectedItem = [];
                $.each($("#order_id :selected"), function () {
                    favorite.push($(this).val());
                });
                $("#line_item").empty();
                $(".js-line-array1").select2({
                    placeholder: "Select (or start typing) the line item name ",
                    //allowClear: true
                });
                var selecid = favorite.join(",");

                //alert("My favourite sports are: " + favorite.join(", "));
                $.ajax({
                    type: "POST",
                    url: "/Orders/getAllLineItemwithstatusByOrder",
                    data: 'order_id=' + favorite.join(","),
                    dataType: "json",
                    success: function (data) {
                        var count = 0;

                        $("#line_item").select2({
                            data: data
                        });
                        $(".js-line-array1").select2({
                            placeholder: "Select (or start typing) the line item name ",
                            allowClear: true
                        });


                    }
                });

            });
	
	
    function actionPlay(action, fl_id) {
        var id = '';
        if (action == 2) { // hide here play and show stop button
            var play_id = "play" + fl_id;
            var stop_id = "stop" + fl_id;
        } else {
            var play_id = "stop" + fl_id;
            var stop_id = "play" + fl_id;
        }

        $.ajax({
            type: "POST",
            url: "<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'update_flight_play_action'), true); ?>/" + action + "/" + fl_id,
            dataType: "json",
            success: function (data) {
                if (data == 1) {
                    $("#" + play_id).hide();
                    $("#" + stop_id).show();
                }
            }, error: function (data) {
                alert("There was an error. Try again please!");
            }
        });

    }

    $('#company_id').change(function () {
        // Remove all Manage Lead Allocation
        var favorite = [];
        var selectedItem = [];
        $.each($("#company_id :selected"), function () {
            favorite.push($(this).val());
        });
        $(".js-order-array").empty();
        $(".js-order-array").select2({
            placeholder: "Select Orders",
            allowClear: true
        });

        var selecid = favorite.join(",");

        //alert("My favourite sports are: " + favorite.join(", "));
        $.ajax({
            type: "POST",
            url: "<?php echo Router::url(array('controller' => 'orders', 'action' => 'getAllOrderList')); ?>",
            data: 'user_id=' + favorite.join(","),
            dataType: "json",
            success: function (data) {
                var count = 0;
                $(".js-order-array").select2({
                    data: data
                });
                $(".js-order-array").select2({
                    placeholder: "Select Orders",
                });

            }
        });

    });
	
	 function ShowProductType(value) {
	
		if (!value) {
           // alert('Please select Media Tpye');
		    
            return false;
        }
        $.ajax({
            type: "POST",
            url: "/orders/getproducttype_ajax/" + value,
            dataType: "json",
            success: function (data) {
				
                $(".js-producttype-array").empty();
                $(".js-producttype-array").select2({
                    data: data
                });
                
				$(".js-producttype-array").select2({
					placeholder: "Select Product Type ",
					maximumSelectionLength: 1,
				});
				
			$(".js-producttype-array").select2("open");
            }
        });
    }

</script>

	<script>
		
		$("input[name='options']").change(function(){
		//alert($(this).val());
		var opt = $(this).val();
		window.location.href = "/orders/adunit_reports?options=" + opt;
    // Do something interesting here
});
		</script>	

<?php

function getprocessbar($process) {
    $color = 'red';
    if ($process >= 0 && $process < 50) {
        $color = 'red';
    } else if ($process >= 50 && $process < 75) {
        $color = '#EB661E';
    } else if ($process >= 75 && $process < 90) {
        $color = 'orange';
    } else if ($process >= 90 && $process <= 100) {
        $color = 'green';
    }

    return $color;
}

function getPercent($total, $goal) {

    if ($total == 0)
        return 0;
    return sprintf('%0.2f', ($goal / $total ) * 100);
}
?>


