<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/orders/">Orders</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Orders Product</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="view_orders">
					<thead>
						<tr>
							<th>
								 
							</th>
							<th>
								Name
							</th>
							<th>
								Type
							</th>
							<th width="17%">
								Line Items
							</th>
							<th>
								Channels & Pricing Model
							</th>
							<th>
								Buyer
							</th>
							<th width="8%">
								Start Date
							</th>
							<th  width="8%">
								End Date
							</th>
							<th>
								Total / Revenue
							</th>
							<th>
								Impressions
							</th>
							<th width="15%">
								Clicks
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							if($results) {
								foreach ($results as $user):
								$i++;
							?>
							<tr class="odd gradeX">
								<td>
                                                                
                                                                <?php 
                                                                $stopstatus = $playstatus ="display:none;"; 
                                                                if($user['tblOrder']['status']==1){ 
                                                                    $stopstatus="display:block;";   
                                                                }else{
                                                                    $playstatus  ="display:block;";
                                                                }
                                                                ?>
                                                                    <button style ="<?php echo $stopstatus; ?>" id ="<?php echo "play".$user['tblOrder']['order_id']; ?>" onClick="return confirm('Are you sure want to pause?')?actionPlay(2,<?php echo $user['tblOrder']['order_id']; ?>):'';"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></button>
                                                                    <button style ="<?php echo $playstatus; ?>" id ="<?php echo "stop".$user['tblOrder']['order_id'] ?>" onClick="return confirm('Are you sure want to start?')?actionPlay(1,<?php echo $user['tblOrder']['order_id']; ?>):'';"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
                                                                   
                                                                
                                                            </td>
                                                            <td>
                                                                    <?php echo $this->Html->link($user['tblOrder']['order_name'],array('controller'=>'orders','action'=>'view_line_items',$user['tblOrder']['dfp_order_id']),array('style'=>'color:#6300FF;text-decoration:underline;'));
                                                                    ?><br/>
									<span style="color:grey;">ID : <?php echo $user['tblOrder']['order_id']; ?></span>
								</td>
								<td align="center">
									<?php echo $user['tblOrder']['isFree'] ? "House <br /> <i class='fa fa-home'></i>" : "Revenue <br /> <i class='fa fa-usd'></i>" ?>
								</td>
								<td>
									<?php echo !isset($status[$user['tblOrder']['dfp_order_id']]) ? "<span style='color:red;font-weight:bold;'>N/A</span>" : "" ?>
									<?php echo isset($status[$user['tblOrder']['dfp_order_id']]['1']) &&  $status[$user['tblOrder']['dfp_order_id']]['1'] ? "</br><span style='color:red;font-weight:bold;'>Inactive</span> (". $status[$user['tblOrder']['dfp_order_id']]['1'] . ")" : "" ; ?>
									<?php echo isset($status[$user['tblOrder']['dfp_order_id']]['2']) &&  $status[$user['tblOrder']['dfp_order_id']]['2'] ? "</br><span style='color:red;font-weight:bold;'>Needs Creatives</span> (". $status[$user['tblOrder']['dfp_order_id']]['2'] . ")" : "" ; ?>
									<?php echo isset($status[$user['tblOrder']['dfp_order_id']]['3']) &&  $status[$user['tblOrder']['dfp_order_id']]['3'] ? "</br><span style='color:#FF9934;font-weight:bold;'>Ready</span> (". $status[$user['tblOrder']['dfp_order_id']]['3'] . ")" : "" ; ?>
									<?php echo isset($status[$user['tblOrder']['dfp_order_id']]['4']) &&  $status[$user['tblOrder']['dfp_order_id']]['4'] ? "</br><span style='color:green;font-weight:bold;'>Delivering</span> (". $status[$user['tblOrder']['dfp_order_id']]['4'] . ")" : "" ; ?>
									<?php echo isset($status[$user['tblOrder']['dfp_order_id']]['5']) &&  $status[$user['tblOrder']['dfp_order_id']]['5'] ? "</br><span style='color:#FF9934;font-weight:bold;'>Paused</span> (". $status[$user['tblOrder']['dfp_order_id']]['5'] . ")" : "" ; ?>
									<?php echo isset($status[$user['tblOrder']['dfp_order_id']]['6']) &&  $status[$user['tblOrder']['dfp_order_id']]['6'] ? "</br><span style='color:#BB99AA;font-weight:bold;'>Completed</span> (". $status[$user['tblOrder']['dfp_order_id']]['6'] . ")" : "" ; ?>
									<?php echo isset($status[$user['tblOrder']['dfp_order_id']]['7']) &&  $status[$user['tblOrder']['dfp_order_id']]['7'] ? "</br><span style='color:#BB99AA;font-weight:bold;'>Archived</span> (". $status[$user['tblOrder']['dfp_order_id']]['7'] . ")" : "" ; ?>
									<?php echo isset($status[$user['tblOrder']['dfp_order_id']]['9']) &&  $status[$user['tblOrder']['dfp_order_id']]['9'] ? "</br><span style='color:red;font-weight:bold;'>Need Flight</span> (". $status[$user['tblOrder']['dfp_order_id']]['9'] . ")" : "" ; ?>
								</td>
								<td>
									<?php echo !isset($channelPricing[$user['tblOrder']['dfp_order_id']]) ? "<span style='color:red;font-weight:bold;'>N/A</span>" : "" ?>
									<?php echo isset($channelPricing[$user['tblOrder']['dfp_order_id']]['1']) &&  $channelPricing[$user['tblOrder']['dfp_order_id']]['1'] ? "<br /><span style='color:red;font-weight:bold;'>CPC</span> (". $channelPricing[$user['tblOrder']['dfp_order_id']]['1'] . ")" : "" ; ?>
									<?php echo isset($channelPricing[$user['tblOrder']['dfp_order_id']]['2']) &&  $channelPricing[$user['tblOrder']['dfp_order_id']]['2'] ? "<br /><span style='color:red;font-weight:bold;'>CPD</span> (". $channelPricing[$user['tblOrder']['dfp_order_id']]['2'] . ")" : "" ; ?>
									<?php echo isset($channelPricing[$user['tblOrder']['dfp_order_id']]['3']) &&  $channelPricing[$user['tblOrder']['dfp_order_id']]['3'] ? "<br /><span style='color:#FF9934;font-weight:bold;'>CPL</span> (". $channelPricing[$user['tblOrder']['dfp_order_id']]['3'] . ")" : "" ; ?>
									<?php echo isset($channelPricing[$user['tblOrder']['dfp_order_id']]['4']) &&  $channelPricing[$user['tblOrder']['dfp_order_id']]['4'] ? "<br /><span style='color:green;font-weight:bold;'>CPS</span> (". $channelPricing[$user['tblOrder']['dfp_order_id']]['4'] . ")" : "" ; ?>
									<?php echo isset($channelPricing[$user['tblOrder']['dfp_order_id']]['5']) &&  $channelPricing[$user['tblOrder']['dfp_order_id']]['5'] ? "<br /><span style='color:#FF9934;font-weight:bold;'>CPM</span> (". $channelPricing[$user['tblOrder']['dfp_order_id']]['5'] . ")" : "" ; ?>
									<?php echo isset($channelPricing[$user['tblOrder']['dfp_order_id']]['6']) &&  $channelPricing[$user['tblOrder']['dfp_order_id']]['6'] ? "<br /><span style='color:#BB99AA;font-weight:bold;'>CPV</span> (". $channelPricing[$user['tblOrder']['dfp_order_id']]['6'] . ")" : "" ; ?>
								</td>
								<td>
									<?php echo $user['tblp']['CompanyName']; ?>
								</td>
								<td align="center">
									<?php $dateformte = Configure::read('Php.dateformte'); ?>
									<?php echo isset($startDate[$user['tblOrder']['dfp_order_id']]) ? date( $dateformte, strtotime($startDate[$user['tblOrder']['dfp_order_id']]) ) : "<span style='font-weight:bold;color:red;'>N/A</span>" ; ?>
								</td>
								<td align="center">
									<?php echo isset($endDate[$user['tblOrder']['dfp_order_id']]) ? date( $dateformte, strtotime($endDate[$user['tblOrder']['dfp_order_id']]) ) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?>
								</td>
								<td align="center">
									<div style="width:100px; height:80px" align="center" id="revenue<?php echo $i; ?>">
									</div>
									
									<?php // echo isset($revenue[$user['tblOrder']['dfp_order_id']]) && $revenue[$user['tblOrder']['dfp_order_id']] != 0 ? "<span style='font-weight:bold;color:green;'>$".number_format($revenue[$user['tblOrder']['dfp_order_id']])."</span>" : "<span style='font-weight:bold;color:red;'>$0</span>"; ?>
								</td>
								<td align="center">
									<?php echo isset($impressions[$user['tblOrder']['dfp_order_id']]) ? number_format($impressions[$user['tblOrder']['dfp_order_id']])  : "0"; ?>
								</td>
								<td align="left">
									<?php echo isset($clicks[$user['tblOrder']['dfp_order_id']]) ? "<span style='color:#434343;font-weight:bold;'>". $clicks[$user['tblOrder']['dfp_order_id']] . "</span>" : "<span style='color:#434343;font-weight:bold;'>0</span>"; ?>
									<span style="color:#3400FF;font-weight:bold;"><br />Overall CTR 0.00%</span>
									<span style="color:green;font-weight:bold;"><br />Best Line Item CTR 0.00%</span>
									<span style="color:red;font-weight:bold;"><br />Worst Line Item CTR 0.00%</span>
								</td>
							</tr>
							<?php endforeach; } else { ?>
							Sorry! No Orders Founds.
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>
<script>
	<?php
		$i = 0;
		foreach ($results as $user){
		$i++; ?>
		var revenue<?php echo $i; ?>;
	<?php } ?>
	
	window.onload = function(){
		<?php
			$i = 0;
			foreach ($results as $user){
				$i++;
			?>
			var revenue<?php echo $i; ?> = new JustGage({
				id: "revenue<?php echo $i; ?>",
				value: <?php echo isset($revenue[$user['tblOrder']['dfp_order_id']]) ? $revenue[$user['tblOrder']['dfp_order_id']] : "0" ?>,
				min: 0,
				formatNumber: true,
				label: "$",
				gaugeWidthScale: 0.6,
				textRenderer: '',
				max: <?php echo isset($totalValue[$user['tblOrder']['dfp_order_id']]) ? $totalValue[$user['tblOrder']['dfp_order_id']] : "0"; ?>
			});
		<?php } ?>
	};

        function actionPlay(action, order_id){
            var id='';
            if(action ==2){ // hide here play and show stop button
                var play_id ="play"+order_id;
                var stop_id ="stop"+order_id;
            }else{
                 var play_id ="stop"+order_id;
                var stop_id ="play"+order_id;
            }

            $.ajax({
                        type: "POST",
                        url: "<?php echo Router::url( array('controller'=>'orders','action'=>'update_play_action'), true ); ?>/"+action+"/"+order_id,
                        dataType: "json",
                        success: function(data){
                            if(data==1){
                                 $("#"+play_id).hide();
                                  $("#"+stop_id).show();   
                            }else{
							 alert("There was an error. Try again please!");
							}   						
                        },error: function(data) {
                            alert("There was an error. Try again please!");
                        }
                });
             
        }
</script>

<?php 
	/*
		$dfpDateTime = $user[0]->endDateTime;
		if (isset($dfpDateTime->timeZoneID)) {
		$timezone = $dfpDateTime->timeZoneID;
		}
		$dateTimeString = sprintf("%d-%d-%dT%d:%d:%d", $dfpDateTime->date->year,
		$dfpDateTime->date->month, $dfpDateTime->date->day, $dfpDateTime->hour,
		$dfpDateTime->minute, $dfpDateTime->second);
		if (isset($timezone)) {
		$newDate = new DateTime($dateTimeString, new DateTimeZone($timezone));
		} else {
		$newDate = new DateTime($dateTimeString);
		}
	echo $newDate->format('m/d/Y'); */
?>
