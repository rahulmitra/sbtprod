<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="/">Home</a><i class="fa fa-circle"></i>
    </li>
    <li >
        <a href="/orders/">Orders</a><i class="fa fa-circle"></i>
    </li>
    <li  class="active">
        <a href="#">Line Items</a>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Line Items of "<?php echo $order_name ?>"</span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a href="/insertionorder/add_line_item/order_id:<?php echo $order_id ?>" id="sample_editable_1_new" class="btn green">
                                    Add New <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
                <?php if (!empty($tblLineItems)) {
                    ?>
                    <div class="table-responsive1">
                        <table class="table table-striped table-bordered table-hover" id="sample_4">
                            <thead>
                                <tr>
                                    <th>
                                    </th>
                                    <th>
                                        Line Items
                                    </th>
                                    <th>
                                        Pricing Model
                                    </th>
                                    <th>
                                        Status
                                    </th> 
                                    <th>
                                        Start Date
                                    </th>
                                    <th>
                                        End Date
                                    </th>
                                    <th>
                                        Rate
                                    </th>
                                    <th>
                                        Goal
                                    </th>
                                    <th>
                                        Booked
                                    </th>
                                    <th>
                                        Imp
                                    </th>
                                    <th>
                                        Clicks
                                    </th>
                                    <th>
                                        -
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($tblLineItems as $user):
                                    ?>
                                    <tr class="odd gradeX">
                                        <td>

                                            <?php
                                            $stopstatus = $playstatus = "display:none;";
                                            if ($user['tblLineItem']['tbl_lineitem_status'] == 1) {
                                                $stopstatus = "display:block;";
                                            } else {
                                                $playstatus = "display:block;";
                                            }
                                            ?>
                                            <button style ="<?php echo $stopstatus; ?>" id ="<?php echo "play" . $user['tblLineItem']['li_id']; ?>" onClick="return confirm('Are you sure want to pause?') ? actionPlay(2,<?php echo $user['tblLineItem']['li_id']; ?>) : '';"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></button>
                                            <button style ="<?php echo $playstatus; ?>" id ="<?php echo "stop" . $user['tblLineItem']['li_id'] ?>" onClick="return confirm('Are you sure want to start?') ? actionPlay(1,<?php echo $user['tblLineItem']['li_id']; ?>) : '';"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>


                                        </td>
                                        <td>
                                            <?php
                                            if ($user['tcs']['cs_id'] == 3) {
                                                //  echo $this->Html->link($user['tblLineItem']['li_name'], array('controller' => 'orders', 'action' => 'view_detail_lead_gen_stats/', $user['tblLineItem']['li_id']), array('style' => 'margin-right:5px;'));
                                            } else if ($user['tcs']['cs_id'] == 5) {
                                                // echo $this->Html->link($user['tblLineItem']['li_name'], array('controller' => 'orders', 'action' => 'view_cpm_stats/', $user['tblLineItem']['li_id']), array('style' => 'margin-right:5px;'));
                                                // echo "<br/>";
                                            } else {
                                                //  echo $this->Html->link($user['tblLineItem']['li_name'], array('controller' => 'orders', 'action' => 'view_detail_stats/', $user['tblLineItem']['li_id']), array('style' => 'margin-right:5px;'));
                                                //  echo "<br/>";
                                            }
                                            ?>
                                            <?php echo $user['tblLineItem']['li_name']; ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $user['tcs']['cs_id'] == 1 ? "<span style='color:red;font-weight:bold;'>" . $user['tcs']['cs_name'] .
                                                    "</span>" : "";
                                            ?>
                                            <?php echo $user['tcs']['cs_id'] == 2 ? "<span style='color:red;font-weight:bold;'>" . $user['tcs']['cs_name'] . "</span>" : ""; ?>
                                            <?php echo $user['tcs']['cs_id'] == 3 ? "<span style='color:#FF9934;font-weight:bold;'>" . $user['tcs']['cs_name'] . "</span>" : ""; ?>
                                            <?php echo $user['tcs']['cs_id'] == 4 ? "<span style='color:green;font-weight:bold;'>" . $user['tcs']['cs_name'] . "</span>" : ""; ?>
                                            <?php echo $user['tcs']['cs_id'] == 5 ? "<span style='color:#FF9934;font-weight:bold;'>" . $user['tcs']['cs_name'] . "</span>" : ""; ?>
                                            <?php echo $user['tcs']['cs_id'] == 6 ? "<span style='color:#BB99AA;font-weight:bold;'>" . $user['tcs']['cs_name'] . "</span>" : ""; ?>

                                            <?php echo $user['tcs']['cs_id'] == 4 ? "<span style='color:green;font-weight:bold;'>" . $user['tcs']['cs_name'] . "</span>" : ""; ?>

                                        </td>
                                        <td>
                                            <?php echo $user['tls']['ls_id'] == 1 ? "<span style='color:red;font-weight:bold;'>Inactive</span>" : ""; ?>
                                            <?php echo $user['tls']['ls_id'] == 2 ? "<a style='margin-right:5px;text-decoration:underline;font-weight:bold;' href='/orders/view_line_creatives/" . $user['tblLineItem']['li_id'] . "'>
									Upload Creative</a>" : ""; ?>
                                            <?php echo $user['tls']['ls_id'] == 3 ? "<span style='color:#FF9934;font-weight:bold;'>Ready</span>" : ""; ?>
                                            <?php echo $user['tls']['ls_id'] == 4 ? "<span style='color:green;font-weight:bold;'>Delivering</span>" : ""; ?>
                                            <?php echo $user['tls']['ls_id'] == 5 ? "<span style='color:#FF9934;font-weight:bold;'>Paused</span>" : ""; ?>
                                            <?php echo $user['tls']['ls_id'] == 6 ? "<span style='color:#BB99AA;font-weight:bold;'>Completed</span>" : ""; ?>
                                            <?php echo $user['tls']['ls_id'] == 7 ? "<span style='color:#BB99AA;font-weight:bold;'>Archived</span>" : ""; ?>
                                            <?php echo $user['tls']['ls_id'] == 9 ? "<a style='margin-right:5px;text-decoration:underline;font-weight:bold;' href='/insertionorder/add_line_item_flight/" . $user['tblLineItem']['li_id'] . "/" . $user['tls']['ls_id'] . "'>
									Create Flight</a>" : ""; ?><br>
                                            <?php /* if (in_array($user['tcs']['cs_id'], array('3', '7'))) { ?>
                                              <a style="margin-right:5px;text-decoration:underline;font-weight:bold;"   href="/insertionorder/test_lead_posting/<?php echo $user['tblLineItem']['li_id']; ?>" class="" data-toggle="modal" data-target="#myModal<?php echo $user['tblLineItem']['li_id']; ?>">Test Lead Posting</a>

                                              <!-- Modal HTML -->
                                              <div id="myModal<?php echo $user['tblLineItem']['li_id']; ?>" class="modal fade">
                                              <div class="modal-dialog">
                                              <div class="modal-content">
                                              <!-- Content will be loaded here from "remote.php" file -->
                                              </div>
                                              </div>
                                              </div>
                                              <?php } */ ?>	
                                        </td> 
                                        <td>
                                            <?php $dateformte = Configure::read('Php.dateformte'); ?>
                                            <?php echo isset($user['tblLineItem']['li_start_date']) ? $this->Time->format($dateformte, $user['tblLineItem']['li_start_date']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?>
                                        </td>
                                        <td>
                                            <?php echo isset($user['tblLineItem']['li_end_date']) ? $this->Time->format($dateformte, $user['tblLineItem']['li_end_date']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?>
                                        </td>
                                        <td>
                                            <?php echo '$' . $user['tblLineItem']['li_rate']; ?>
                                        </td>
                                        <td>
                                            <?php echo '$' . $user['tblLineItem']['li_total'] ?>
                                        </td>
                                        <td>
                                            <?php echo '$' . $user['tblLineItem']['li_revenue'] ?>
                                        </td>
                                        <td>
                                            <?php echo $user['tblLineItem']['li_imp'] ?>
                                        </td>
                                        <td>
                                            <?php echo $user['tblLineItem']['li_clicks'] ?>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a data-toggle="dropdown" href="javascript:;" class="btn btn-circle btn-default" aria-expanded="true">
                                                    <i class="fa fa-share"></i> Action <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a style="margin-right:5px;" href="/insertionorder/edit_line_item/<?php echo $user['tblLineItem']['li_id']; ?>/">
                                                            <i class="fa fa-codepen">
                                                            </i>Edit</a>
                                                    </li>
                                                    <li>
                                                        <a style="margin-right:5px;" href="/insertionorder/add_line_item_flight/<?php echo $user['tblLineItem']['li_id']; ?>/<?php echo $user['tls']['ls_id']; ?>">
                                                            <i class="fa fa-codepen">
                                                            </i>Create Flight</a>
                                                    </li>
                                                    <li>
                                                        <a style="margin-right:5px;" href="/orders/view_flights/<?php echo $user['tblLineItem']['li_id']; ?>/">
                                                            <i class="fa fa-database">
                                                            </i>View Flights</a>
                                                    </li>
                                                    <li>
                                                        <a href="/orders/view_line_creatives/<?php echo $user['tblLineItem']['li_id']; ?>/">
                                                            <i class="fa fa-database">
                                                            </i>View / Upload Creatives</a>
                                                    </li>
                                                    <li style="display:none;">
                                                        <a style="margin-right:5px;" href="/orders/view_line_creatives/<?php echo $user['tblLineItem']['li_id']; ?>/">
                                                            <i class="fa fa-codepen">
                                                            </i>View / Upload Creatives</a>
                                                    </li>
                                                    <li style="display:none;">
                                                        <a style="margin-right:5px;" href="/orders/view_leads/<?php echo $user[0]->id; ?>/">
                                                            <i class="fa fa-database">
                                                            </i>View Leads</a>
                                                    </li>
                                                </ul>
                                            </div>	

                                        </td>
                                    </tr> 
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php } else { ?>
                    No line item created yet for this order.
                <?php } ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
    #sample_4_filter { 
        float:right;
    }
</style>
<script>
    function actionPlay(action, li_id) {
        var id = '';
        if (action == 2) { // hide here play and show stop button
            var play_id = "play" + li_id;
            var stop_id = "stop" + li_id;
        } else {
            var play_id = "stop" + li_id;
            var stop_id = "play" + li_id;
        }

        $.ajax({
            type: "POST",
            url: "<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'update_play_action'), true); ?>/" + action + "/" + li_id,
            dataType: "json",
            success: function (data) {
                if (data == 1) {
                    $("#" + play_id).hide();
                    $("#" + stop_id).show();
                } else {
                    alert("There was an error. Try again please!");
                }
            }, error: function (data) {
                alert("There was an error. Try again please!");
            }
        });

    }
</script>