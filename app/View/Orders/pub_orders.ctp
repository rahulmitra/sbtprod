<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/orders/">Orders</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Orders Product</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_4">
					<thead>
						<tr>
							<th class="table-checkbox">
								Order ID
							</th>
							<th>
								Order Name
							</th>
							<th>
								Start Date
							</th>
							<th>
								End Date
							</th>
							<th>
								Total Projected Value
							</th>
							<th>
								Impressions
							</th>
							<th>
								Clicks
							</th>
							<th class="center" style="text-align:center;">
								
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							if($results) {
								foreach ($results as $user):
								$user = array($user);
								if($user[0]->id != "") {
									
								?>
								<tr class="odd gradeX">
									<td>
										<?php echo $user[0]->id; ?>
									</td>
									<td>
										<?php echo $user[0]->name; ?>
									</td>
									<td>
										<?php 
											$dfpDateTime = $user[0]->startDateTime;
											if (isset($dfpDateTime->timeZoneID)) {
												$timezone = $dfpDateTime->timeZoneID;
											}
											$dateTimeString = sprintf("%d-%d-%dT%d:%d:%d", $dfpDateTime->date->year,
											$dfpDateTime->date->month, $dfpDateTime->date->day, $dfpDateTime->hour,
											$dfpDateTime->minute, $dfpDateTime->second);
											if (isset($timezone)) {
												$newDate = new DateTime($dateTimeString, new DateTimeZone($timezone));
												} else {
												$newDate = new DateTime($dateTimeString);
											}
											echo $newDate->format('m/d/Y'); 
										?>
									</td>
									<td>
										<?php 
											$dfpDateTime = $user[0]->endDateTime;
											if (isset($dfpDateTime->timeZoneID)) {
												$timezone = $dfpDateTime->timeZoneID;
											}
											$dateTimeString = sprintf("%d-%d-%dT%d:%d:%d", $dfpDateTime->date->year,
											$dfpDateTime->date->month, $dfpDateTime->date->day, $dfpDateTime->hour,
											$dfpDateTime->minute, $dfpDateTime->second);
											if (isset($timezone)) {
												$newDate = new DateTime($dateTimeString, new DateTimeZone($timezone));
												} else {
												$newDate = new DateTime($dateTimeString);
											}
											echo $newDate->format('m/d/Y'); 
										?>
									</td>
									<td align="center">
										$<?php echo ($user[0]->totalBudget->microAmount / 1000000) ; ?>
									</td>
									<td>
										<?php echo $user[0]->totalImpressionsDelivered; ?>
									</td>
									<td>
										<?php echo $user[0]->totalClicksDelivered; ?>
									</td>
									<td align="center">
										<div class="btn-group">
											<a data-toggle="dropdown" href="javascript:;" class="btn btn-circle btn-default" aria-expanded="true">
												<i class="fa fa-share"></i> Action <i class="fa fa-angle-down"></i>
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<a style="margin-right:5px;" href="/orders/view_line_items/<?php echo $user[0]->id; ?>/">
														<i class="fa fa-database">
														</i> View Line Items</a>
												</li>
											</ul>
										</div>	
									</td>
								</tr>
							<?php } endforeach; } else { ?>
							Sorry! No Orders Founds.
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>
