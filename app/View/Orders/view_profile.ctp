<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/orders/">Orders</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/orders/view_line_items/<?php echo $od_order_id; ?>/">Line Items</a><i class="fa fa-circle"></i>
	</li>
	<li class="active">
		<a href="#">Lead Details</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Lead Details</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<ul class="list-unstyled">
					<li style="margin-bottom:10px;">
						<strong>Email:</strong> <?php echo $group['Dynamic']['od_email']; ?><br />
					</li>
					<li  style="margin-bottom:10px;">
						<strong>Referer Site:</strong> <?php echo $group['Dynamic']['od_site']; ?>
					</li>
					<li style="margin-bottom:10px;">
						<strong>Browser:</strong> <?php echo $group['Dynamic']['fp_browser']; ?>
					</li>
					<li style="margin-bottom:10px;">
						<strong>Connection:</strong> <?php echo $group['Dynamic']['fp_connection']; ?>
					</li>
					<li style="margin-bottom:10px;">
						<strong>Display:</strong> <?php echo $group['Dynamic']['fp_display']; ?>
					</li>
					<li style="margin-bottom:10px;">
						<strong>Flash:</strong> <?php echo $group['Dynamic']['fp_flash']; ?>
					</li>
					<li style="margin-bottom:10px;">
						<strong>Language:</strong> <?php echo $group['Dynamic']['fp_language']; ?>
					</li>
					<li style="margin-bottom:10px;">
						<strong>OS:</strong> <?php echo $group['Dynamic']['fp_os']; ?>
					</li>
					<li style="margin-bottom:10px;">
						<strong>TimeZone:</strong> <?php echo $group['Dynamic']['fp_timezone']; ?>
					</li>
					<li style="margin-bottom:10px;">
						<strong>User Agent:</strong> <?php echo $group['Dynamic']['fp_useragent']; ?>
					</li>
				</ul>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>