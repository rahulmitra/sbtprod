<!-- BEGIN PAGE BREADCRUMB -->

<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/orders/">Orders</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/orders/view_line_items/<?php echo $li_order_id; ?>/">Line Items</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/orders/">Creatives</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Uploaded Creatives of "<?php echo $line_name; ?>"</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_4">
					<thead>
						<tr>
						    <th></th>
							<th class="table-checkbox">
								CID
							</th>
							<th>
								Creative Name
							</th>
							<th>
								Creative Type
							</th>
							<th>
								Status
							</th>
							<th>
								Impressions
							</th>
							<th>
								Total Leads
							</th>
							<th>
								CTR
							</th>
							<th>
								Uploaded At
							</th>
							<th>
								Action
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							//echo "<pre>";print_r($group);
							foreach ($group as $user):
							
						?>
						<tr class="odd gradeX">
						    <?php
	                            $stopstatus = $playstatus = "display:none;";
	                            if ($user['tblCreative']['active_status'] == 0) {
	                                $stopstatus = "display:block;";
	                            } else {
	                                $playstatus = "display:block;";
	                            }
                            ?>
							<td>
							<button style="<?php echo $stopstatus; ?>" id="<?php echo "play" . $user['tblCreative']['cr_id']; ?>" onclick="return confirm('Are you sure want to pause?') ? abc(1,<?php echo $user['tblCreative']['cr_id']; ?>,<?php echo $user['tblCreative']['cr_lid']; ?>) : '';"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></button>
							
							<button style="<?php echo $playstatus; ?>;" id="<?php echo "stop" . $user['tblCreative']['cr_id'] ?>" onclick="return confirm('Are you sure want to start?') ? abc(0,<?php echo $user['tblCreative']['cr_id']; ?>,<?php echo $user['tblCreative']['cr_lid']; ?>) : '';"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
							</td>
							<td>
								<?php 
								echo $user['tblCreative']['cr_id']; ?>
							</td>
							<td>
								<b><?php if($user['tblCreative']['creatives_type'] == 55){
									echo "image";
								}
								else if( in_array($user['tblCreative']['creatives_type'],array(2,13,12))){
									echo  $user['tblCreative']['cr_header'];
								}
								else{
									echo  $user['tblCreative']['cr_name'];
								} ?></b>
							</td>
							<td>
								<b><?php echo (!empty($user['tblCreative']['creatives_type']))?$creatives_type[$user['tblCreative']['creatives_type']]:'N/A'; ?></b>
							</td>
							<td>
							<?php if(empty($user['FlightsCreative'])){ ?>
								 
								<?php echo $this->Html->link('Add a Flight', array('controller' => 'insertionorder', 'action' =>'add_line_item_flight',$line_id),array("style"=>"margin-right:5px;text-decoration:underline;font-weight:bold;"   ));?>
							<?php } else { ?>
								<span style='<?php echo $user['tblCreative']['cr_status'] == 1 ? "color:green;font-weight:bold;" : "color:red;font-weight:bold;" ?>'><?php echo ($user['tblCreative']['cr_status'])?'Active':'Inactive'; ?></span>
							<?php } ?>
							</td>
							<td>
								<?php echo $user['tblCreative']['cr_imp']; ?>
							</td>
							<td>
								<?php echo $user['tblCreative']['cr_click']; ?>
							</td>
							<td>
								0%
							</td>
							<td class="center">
								<?php echo $this->Time->format('m/d/Y', $user['tblCreative']['created_at']); ?>&nbsp;
							</td>
							<td style="">
								<a style="margin-right:5px;display:none;" target="_blank" href="<?php echo $user['tblCreative']['cr_preview_url']; ?>">
									<i class="fa fa-users">
									</i> Preview Creative</a>
									 
									
									<?php echo $this->Html->link('Edit', array('controller' => 'insertionorder', 'action' =>'edit_upload_creative_text_html',$user['tblCreative']['cr_id'],$line_id));?>
											
							</td>
						</tr> 
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Upload Creatives of "<?php echo $line_name; ?>"</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_4">
					<thead>
						<tr>
							<th class="table-checkbox">
								CID
							</th>
							 
							<th>
								Creative Type
							</th>
							<th>
								Action
							</th>
						</tr>
					</thead>
					<tbody>
						
						<?php
							$i = 1;
							if(!empty($creatives)){
							$user = $creatives[0];
							//foreach ($creatives as $user):?>
							
							<?php 	$action ="upload_creative_text_html";
								if($user['ad_unit_sizes'] == "text/html") {  
									$action ="upload_creative_text_html";
								} 
								else if($user['ad_unit_sizes'] == "300x250" || $user['ad_unit_sizes'] == "728x90") {  
									$action = "upload_creative";
								} 
							echo $this->Form->create('tblOrder',array('url'=>array('controller'=>'insertionorder','action'=>$action,$line_id,base64_encode($user['ad_unit_sizes'])),'class'=>'form-horizontal'));?>
							
							
							<tr class="odd gradeX">
								<td>
									<?php echo $i; $i++;?>
								</td>
								
								<td>
									<?php echo $this->Form->input('creatives_type', array(
										'class' => 'js-creative-array form-control select2-hidden-accessible',
										'label'=>false,
										'type'=>'select',
										'empty' => 'Please Select Creatives type',
										'options' => $creatives_type
									));?>
									
								</td>
								<td>
									
									<button type="submit" class="btn green"><i class="fa fa-users">
									</i> Upload Creative</button>
									
								</td>
							</tr> 
							<?php 
								echo $this->Form->end();
								//endforeach; 
							}else{ ?>
							<?php 	 
							echo $this->Form->create('tblOrder',array('url'=>array('controller'=>'insertionorder','action'=>'upload_creative_text_html',$line_id,base64_encode("text/html")),'class'=>'form-horizontal'));?>
							
							
							<tr class="odd gradeX">
								<td>1
								</td>
								
								<td>
									<?php echo $this->Form->input('creatives_type', array(
										'class' => 'js-creative-array form-control select2-hidden-accessible',
										'label'=>false,
										'type'=>'select',
										'empty' => 'Please Select Creatives type',
										'options' => $creatives_type
									));?>
									
								</td>
								<td>
									
									<button type="submit" class="btn green"><i class="fa fa-users">
									</i> Upload Creative</button>
									
								</td>
							</tr> 
							<?php 
								echo $this->Form->end();
							}?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<script>
function abc(action,cr_id,cr_lid){
	var id = '';
        if (action == 1) { // hide here play and show stop button
            var play_id = "play" + cr_id;
            var stop_id = "stop" + cr_id;
        } else {
            var play_id = "stop" + cr_id;
            var stop_id = "play" + cr_id;
        }
        
        $.ajax({
        	type: "POST",
        	url : "<?php echo $this->webroot; ?>orders/update_play_action_creative",
        	data : {
				action : action,
				cr_id  : cr_id,
				cr_lid : cr_lid
			},
			success: function (data) {
                if (data == 0) {
                	$("#" + play_id).hide();
                    $("#" + stop_id).show();
                } else if(data == 1) {
                    $("#" + play_id).hide();
                    $("#" + stop_id).show();
                }
            }, error: function (data) {
                alert("There was an error. Try again please!");
            }
        	
        });

}

</script>
<style>
	#sample_4_filter { 
	float:right;
	}
	
	.select2-container-multi .select2-choices .select2-search-choice {
	margin : 6px 0 3px 5px !important;
	}
	.select2-container--default .select2-selection--multiple .select2-selection__choice
	{
	width: 100%!important;
	}
</style>	