<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/newsletter/">Newsletter</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="#">View Subscribers</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Subscribers</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="view_subscribers">
					<thead>
						<tr>
							<th class="table-checkbox">
								S.NO.
							</th>
							<th>
								Email
							</th>
							<th>
								First Name
							</th>
							<th>
								Last Name
							</th>
							<th>
								Promotional
							</th>
							<th>
								Subscribe Date
							</th>
							<th class="center" style="text-align:center;">
								
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							$count = 1;
							foreach ($group as $user):
							$class = '';
							if ($i++ % 2 == 0) {
								$class = 'altrow';
							}
							
						?>
						<tr class="odd gradeX">
							<td>
								<?php echo $count; ?>
							</td>
							<td>
								<?php echo $user['Dynamic']['nl_email']; ?>
							</td>
							<td>
								<?php echo $user['Dynamic']['nl_fname']; ?>
							</td>
							<td>
								<?php echo $user['Dynamic']['nl_lname']; ?>
							</td>
							<td>
								<?php echo $user['Dynamic']['nl_ispromotion']; ?>
							</td>
							<td class="center">
								<?php echo $this->Time->format('d/m/Y', $user['Dynamic']['nl_create']); ?>&nbsp;
							</td>
							<td align="center">
								<div class="btn-group">
									<a data-toggle="dropdown" href="javascript:;" class="btn btn-circle btn-default" aria-expanded="true">
										<i class="fa fa-share"></i> Action <i class="fa fa-angle-down"></i>
									</a>
									<ul class="dropdown-menu pull-right">
										<li>
											<a style="margin-right:5px;" href="/newsletter/view_profile/<?php echo $user['Dynamic']['nl_email']; ?>/">
												<i class="icon-eye">
												</i> View Profile</a>
										</li>
									</ul>
								</div>							
							</td>
						</tr>
						<?php $count++; endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>