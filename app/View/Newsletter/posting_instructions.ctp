<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/newsletter/">Newsletter</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="#">Posting Instruction</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->

<div class="portlet light">
	<div class="portlet-body">
		<div class="row margin-bottom-30">
			<div class="col-md-12">
				<p>
					Post the Email, First Name & Last Name using the below URL.
				</p>
				<p>
				http://digitaladvertising.systems/nlsubscribers/add?aduid=<?php echo $uniqueID; ?>&nl_email={emailvalue}&nl_fname={firstname}&nl_lname={lastname}
				</p>
				<ul class="list-unstyled margin-top-10 margin-bottom-10 hidden">
					<li>
						<i class="fa fa-check icon-default"></i> Nam liber tempor cum soluta
					</li>
					<li>
						<i class="fa fa-check icon-success"></i> Mirum est notare quam
					</li>
					<li>
						<i class="fa fa-check icon-info"></i> Lorem ipsum dolor sit amet
					</li>
					<li>
						<i class="fa fa-check icon-danger"></i> Mirum est notare quam
					</li>
					<li>
						<i class="fa fa-check icon-warning"></i> Mirum est notare quam
					</li>
				</ul>
			</div>
		</div>
		<!--/row-->
	</div>
</div>