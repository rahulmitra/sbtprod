<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/newsletter/">Newsletter</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Newsletter Product</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<a href="#" id="sample_editable_1_new" class="btn green">
									Add New <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						<div class="col-md-6">
						</div>
					</div>
				</div>
				<table class="table table-striped table-bordered table-hover" id="sample_4">
					<thead>
						<tr>
							<th class="table-checkbox">
								PID
							</th>
							<th>
								Newsletter Name
							</th>
							<th>
								Subscribers
							</th>
							<th>
								Create Date
							</th>
							<th class="center" style="text-align:center;">
								
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($group as $user):
							$class = '';
							if ($i++ % 2 == 0) {
								$class = 'altrow';
							}
							
						?>
						<tr class="odd gradeX">
							<td>
								<?php echo $user['tblAdunit']['adunit_id']; ?>
							</td>
							<td>
								<a style="margin-right:5px;" href="/newsletter/view_stats/<?php echo $user['tblAdunit']['ad_uid']; ?>/"><?php echo $user['tblAdunit']['adunit_name']; ?></a>
								<?php foreach($nlchilds[$user['tblAdunit']['ad_uid']] as $nlchild) { ?>
								<br />
								 - <?php echo $nlchild['tblAdunit']['adunit_name']; ?>
								<?php } ?>
							</td>
							<td>
								<?php echo $subscribersCount[$user['tblAdunit']['ad_uid']]; ?>
							</td>
							<td class="center">
								<?php $dateformte= Configure::read('Php.dateformte'); ?>
								<?php echo $this->Time->format($dateformte, $user['tblAdunit']['ad_created_at']); ?>&nbsp;
							</td>
							<td align="center">
								<div class="btn-group">
									<a data-toggle="dropdown" href="javascript:;" class="btn btn-circle btn-default" aria-expanded="true">
										<i class="fa fa-share"></i> Action <i class="fa fa-angle-down"></i>
									</a>
									<ul class="dropdown-menu pull-right">
										<li class="hide">
											<a style="margin-right:5px;" href="/newsletter/view_list_stats/<?php echo $user['tblAdunit']['ad_uid']; ?>/">
												<i class="fa fa-bar-chart-o">
												</i> View List Stats</a>
										</li>
										<li>
											<a style="margin-right:5px;" href="/newsletter/posting_instructions/<?php echo $user['tblAdunit']['ad_uid']; ?>/">
												<i class="fa fa-database">
												</i> Posting Instructions</a>
										</li>
										<li>
											<a style="margin-right:5px;" href="/newsletter/newsletter_script/<?php echo $user['tblAdunit']['ad_uid']; ?>/">
												<i class="fa fa-database">
												</i> Newsletter Script</a>
										</li>
										<li  class="hide">
											<a style="margin-right:5px;" href="/newsletter/upload_subscribers/<?php echo $user['tblAdunit']['ad_uid']; ?>/">
												<i class="fa fa-upload">
												</i> Upload Subscribers</a>
										</li>
									</ul>
								</div>							
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>