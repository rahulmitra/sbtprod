<?php
	/**
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Pages
		* @since         CakePHP(tm) v 0.10.0.1076
	*/
	
	if (!Configure::read('debug')):
	throw new NotFoundException();
	endif;
	
	App::uses('Debugger', 'Utility');
?>
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/newsletter/">Newsletter</a><i class="fa fa-circle"></i>
	</li>
	<li class="active">
		<a href="#">List Stats</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-7">
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Email List Growth</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div id="chart_div" style="width: 100%; height: 250px;"></div>
			</div>
		</div>
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">List Users Removed</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div id="chart_div1" style="width: 100%; height: 250px;"></div>
			</div>
		</div>
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Top Source of Subscribers</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
				<div class="actions">
					<div data-toggle="buttons" class="btn-group btn-group-devided">
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
						<input type="radio" onchange="viewSummarygraph('day')" class="toggle" name="options">Day</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
						<input type="radio" onchange="viewSummarygraph('week')" class="toggle" name="options">Weekly</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
						<input type="radio" onchange="viewSummarygraph('month')" class="toggle" name="options">Monthly</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
						<input type="radio" onchange="viewSummarygraph('year')" class="toggle" name="options">Yearly</label>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div id="chart_div2" style="width: 100%; height: 250px;"></div>
			</div>
		</div>
		
		
		
	</div>
	<!--end col-md-5-->
	<div class="col-md-5">
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">List Size Movement</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div id="curve_chart" style="width: 100%; height: 220px;"></div>
			</div>
		</div>
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">New Subscribers Trends</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div id="curve_chart1" style="width: 100%; height: 220px;"></div>	
			</div>
		</div>
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Opt-Outs Trends</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div id="curve_chart2" style="width: 100%; height: 220px;"></div>
			</div>
		</div>
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Hard Bounce Trends</span>
					<span class="caption-helper hide">weekly stats...</span>
				</div>
			</div>
			<div class="portlet-body">
				<div id="curve_chart3" style="width: 100%; height: 220px;"></div>	
			</div> 
		</div>
	</div> 
	<!--end col-md-4-->
	
</div>

<!-- END PAGE CONTENT INNER -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawVisualization);
	google.setOnLoadCallback(drawVisualization1);
	google.setOnLoadCallback(drawVisualization2);
	google.setOnLoadCallback(drawChart);
	google.setOnLoadCallback(drawChart1);
	google.setOnLoadCallback(drawChart2);
	google.setOnLoadCallback(drawChart3);
	
	function drawVisualization() {
		// Some raw data (not necessarily accurate)
		var data = google.visualization.arrayToDataTable([
		['Month', 'Existing', 'Imports', 'Optins'],
		['Aug 2014',  400,      100,         72],
		['Sep 2014',  450,      150,        115],
		['Oct 2014',  480,      180,        140],
		['Nov 2014',  520,      230,        150],
		['Dec 2014',  550,      280,         220],
		['Jan 2015',  600,      250,         200]
		]);
		
		var options = {
			'width':'100%',
			'height':'220',
			chartArea:{left:45,top:20,width:"100%",height:"70%"},
			vAxis: {title: 'List Size'},
			legend: {
				position: 'top'
			},
			pointSize: 8,
			lineWidth:4,
			hAxis: {title: 'Month'},
			seriesType: 'line',
			series: {0: {type: 'bars'}}
		};
		
		var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
		chart.draw(data, options);
	}
	
	function drawVisualization1() {
		// Some raw data (not necessarily accurate)
		var data = google.visualization.arrayToDataTable([
		['Month', 'Opt-Outs', 'Hard Bonces', 'Unsubscribed'],
		['Aug 2014',  100,      30,         70],
		['Sep 2014',  80,      34,        46],
		['Oct 2014',  50,      30,        20],
		['Nov 2014',  70,      50,        20],
		['Dec 2014',  120,      80,         40],
		['Jan 2015',  150,      60,         90]
		]);
		
		var options = {
			'width':'100%',
			'height':'220',
			chartArea:{left:45,top:20,width:"100%",height:"70%"},
			vAxis: {title: 'Removed'},
			legend: {
				position: 'top'
			},
			pointSize: 8,
			lineWidth:4,
			hAxis: {title: 'Month'},
			seriesType: 'line',
			series: {0: {type: 'bars'}}
		};
		
		var chart = new google.visualization.ComboChart(document.getElementById('chart_div1'));
		chart.draw(data, options);
	}
	
	function drawVisualization2() {
		// Some raw data (not necessarily accurate)
		var data = google.visualization.arrayToDataTable([
		['Source', 'Subscribers'],
		['Reg-Page',  100],
		['N/L Pop-Up',  80],
		['Right Rail',  50],
		['Landing Page',  70],
		['3rd Party Coreg',  120],
		['Webinar',  150]
		]);
		
		var options = {
			'width':'100%',
			'height':'220',
			chartArea:{left:45,top:20,width:"100%",height:"70%"},
			vAxis: {title: 'Removed'},
			legend: {
				position: 'top'
			},
			pointSize: 8,
			lineWidth:4,
			hAxis: {title: 'Source'},
			seriesType: 'bars'
		};
		
		var chart = new google.visualization.ComboChart(document.getElementById('chart_div2'));
		chart.draw(data, options);
	}
	
	function drawChart() {
		var data = google.visualization.arrayToDataTable([
		['Date', 'List Size'],
		['9/1/2015',  8870],
		['9/2/2015',  8892],
		['9/3/2015',  8899],
		['9/4/2015',  8890],
		['9/5/2015',  8911],
		['9/6/2015',  8922],
		['9/7/2015',  8920],
		['9/8/2015',  8971],
		['9/9/2015',  8960],
		['9/10/2015',  8950]
		]);
		
		var options = {
			curveType: 'function',
			'width':'100%',
			'height':'220',
			chartArea:{left:45,top:20,width:"100%",height:"70%"},
			legend: {
				position: 'top'
			},
			hAxis: {title: 'Date'},
			pointSize: 8,
			lineWidth:4
		};
		
		var chart = new google.visualization.AreaChart(document.getElementById('curve_chart'));
		
		chart.draw(data, options);
	}
	
	function drawChart1() {
		var data = google.visualization.arrayToDataTable([
		['Date', 'New Subscribers'],
		['9/1/2015',  87],
		['9/2/2015',  89],
		['9/3/2015',  89],
		['9/4/2015',  89],
		['9/5/2015',  91],
		['9/6/2015',  92],
		['9/7/2015',  92],
		['9/8/2015',  97],
		['9/9/2015',  96],
		['9/10/2015',  95]
		]);
		
		var options = {
			curveType: 'function',
			'width':'100%',
			'height':'220',
			chartArea:{left:45,top:20,width:"100%",height:"70%"},
			legend: {
				position: 'top'
			},
			hAxis: {title: 'Date'},
			pointSize: 8,
			lineWidth:4
		};
		
		var chart = new google.visualization.AreaChart(document.getElementById('curve_chart1'));
		
		chart.draw(data, options);
	}
	
	function drawChart2() {
		var data = google.visualization.arrayToDataTable([
		['Date', 'Opt-Outs'],
		['9/1/2015',  0],
		['9/2/2015',  2],
		['9/3/2015',  9],
		['9/4/2015',  0],
		['9/5/2015',  1],
		['9/6/2015',  2],
		['9/7/2015',  3],
		['9/8/2015',  1],
		['9/9/2015',  5],
		['9/10/2015',  7]
		]);
		
		var options = {
			curveType: 'function',
			'width':'100%',
			'height':'220',
			chartArea:{left:45,top:20,width:"100%",height:"70%"},
			legend: {
				position: 'top'
			},
			hAxis: {title: 'Date'},
			pointSize: 8,
			lineWidth:4
		};
		
		var chart = new google.visualization.AreaChart(document.getElementById('curve_chart2'));
		
		chart.draw(data, options);
	}
	
	function drawChart3() {
		var data = google.visualization.arrayToDataTable([
		['Date', 'Hard Bounce'],
		['9/1/2015',  70],
		['9/2/2015',  92],
		['9/3/2015',  99],
		['9/4/2015',  90],
		['9/5/2015',  11],
		['9/6/2015',  22],
		['9/7/2015',  20],
		['9/8/2015',  71],
		['9/9/2015',  60],
		['9/10/2015',  50]
		]);
		
		var options = {
			curveType: 'function',
			'width':'100%',
			'height':'220',
			chartArea:{left:45,top:20,width:"100%",height:"70%"},
			legend: {
				position: 'top'
			},
			hAxis: {title: 'Date'},
			pointSize: 8,
			lineWidth:4
		};
		
		var chart = new google.visualization.AreaChart(document.getElementById('curve_chart3'));
		
		chart.draw(data, options);
	}
</script>		