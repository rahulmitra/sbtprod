<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/newsletter/">Newsletter</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="#">Posting Instruction</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->

<div class="portlet light">
	<div class="portlet-body">
		<div class="row margin-bottom-30">
			<div class="col-md-12">
				<p> 
					<?php if(empty($mappedUnits)) { ?>
						Please map with Ad unit with In-House Order
					<?php } else { ?>
					<h3 class="form-section">Put this code in header</h3>
					 <textarea class="form-control vresize"  style="height:400px">
<script type='text/javascript'>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
  (function() {
    var gads = document.createElement('script');
    gads.async = true;
    gads.type = 'text/javascript';
    var useSSL = 'https:' == document.location.protocol;
    gads.src = (useSSL ? 'https:' : 'http:') +
      '//www.googletagservices.com/tag/js/gpt.js';
    var node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(gads, node);
  })();
</script>

<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineOutOfPageSlot('/1575221/<?php echo $mappedUnits['tblAdunit']['ad_dfp_id']; ?>', 'div-gpt-ad-1450565627176-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>
</textarea>
<h3 class="form-section">Put this in body</h3>
<textarea class="form-control vresize"  style="height:100px">
<!-- /1575221/<?php echo $mappedUnits['tblAdunit']['ad_dfp_id']; ?> -->
<div id='div-gpt-ad-1450565627176-0'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1450565627176-0'); });
</script>
</div>
</textarea>		
					
					
					
					<?php } ?>
				</p>
				<ul class="list-unstyled margin-top-10 margin-bottom-10 hidden">
					<li>
						<i class="fa fa-check icon-default"></i> Nam liber tempor cum soluta
					</li>
					<li>
						<i class="fa fa-check icon-success"></i> Mirum est notare quam
					</li>
					<li>
						<i class="fa fa-check icon-info"></i> Lorem ipsum dolor sit amet
					</li>
					<li>
						<i class="fa fa-check icon-danger"></i> Mirum est notare quam
					</li>
					<li>
						<i class="fa fa-check icon-warning"></i> Mirum est notare quam
					</li>
				</ul>
			</div>
		</div>
		<!--/row-->
	</div>
</div>