<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/messages/">Messages</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Email Messages</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm">
							Total Sent
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo number_format($sent); ?> <span class="font-lg font-grey-mint"></span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm">
							Total Delivered
						</div>
						<div class="uppercase font-hg theme-font">
							<?php echo number_format($delivered); ?> <span class="font-lg font-grey-mint"></span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm">
							Total Unique Opens
						</div>
						<div class="uppercase font-hg font-blue-sharp">
							<?php echo number_format($opens); ?> <span class="font-lg font-grey-mint"></span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm">
							Total Unique Clicks
						</div>
						<div class="uppercase font-hg font-purple">
							<?php echo number_format($clicks); ?><span class="font-lg font-grey-mint"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<i class="icon-bar-chart theme-font hide"></i>
							<span class="caption-subject theme-font bold uppercase">Geography Breakdown</span>
						</div>
					</div>
					<div class="portlet-body">
						<img class="img-responsive"  src="https://tracking.digitaladvertising.systems/index.php?module=API&method=ImageGraph.get&idSite=<?php echo $tblMailing['tp']['tracksite_id'] ?>&apiModule=UserCountry&apiAction=getCountry&token_auth=29e9d1b76ecf64c487d880cd316470c0&graphType=horizontalBar&period=month&date=today&segment=contentPiece==DAS-MAILING-<?php echo $tblMailing['tblMailing']['m_id'] ?>" />
					</div>
				</div>
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<i class="icon-bar-chart theme-font hide"></i>
							<span class="caption-subject theme-font bold uppercase">Operating System Breakdown</span>
						</div>
					</div>
					<div class="portlet-body">
						<img class="img-responsive"  src="https://tracking.digitaladvertising.systems/index.php?module=API&method=ImageGraph.get&idSite=<?php echo $tblMailing['tp']['tracksite_id'] ?>&apiModule=DevicesDetection&apiAction=getOsFamilies &token_auth=29e9d1b76ecf64c487d880cd316470c0&graphType=horizontalBar&period=month&date=today&segment=contentPiece==DAS-MAILING-<?php echo $tblMailing['tblMailing']['m_id'] ?>" />
					</div>
				</div>
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<i class="icon-bar-chart theme-font hide"></i>
							<span class="caption-subject theme-font bold uppercase">Browsers Breakdown</span>
						</div>
					</div>
					<div class="portlet-body">
						<img class="img-responsive"  src="https://tracking.digitaladvertising.systems/index.php?module=API&method=ImageGraph.get&idSite=<?php echo $tblMailing['tp']['tracksite_id'] ?>&apiModule=DevicesDetection&apiAction=getBrowsers  &token_auth=29e9d1b76ecf64c487d880cd316470c0&graphType=horizontalBar&period=month&date=today&segment=contentPiece==DAS-MAILING-<?php echo $tblMailing['tblMailing']['m_id'] ?>" />
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<i class="icon-bar-chart theme-font hide"></i>
							<span class="caption-subject theme-font bold uppercase">Engagement by Devices</span>
						</div>
					</div>
					<div class="portlet-body">
						<img class="img-responsive"  src="https://tracking.digitaladvertising.systems/index.php?module=API&method=ImageGraph.get&idSite=<?php echo $tblMailing['tp']['tracksite_id'] ?>&apiModule=DevicesDetection&apiAction=getType &token_auth=29e9d1b76ecf64c487d880cd316470c0&graphType=horizontalBar&period=month&date=today&segment=contentPiece==DAS-MAILING-<?php echo $tblMailing['tblMailing']['m_id'] ?>" />
					</div>
				</div>
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<i class="icon-bar-chart theme-font hide"></i>
							<span class="caption-subject theme-font bold uppercase">Engagement by Time</span>
						</div>
					</div>
					<div class="portlet-body">
						<img class="img-responsive"  src="https://tracking.digitaladvertising.systems/index.php?module=API&method=ImageGraph.get&idSite=<?php echo $tblMailing['tp']['tracksite_id'] ?>&apiModule=VisitorInterest&apiAction=getNumberOfVisitsPerVisitDuration  &token_auth=29e9d1b76ecf64c487d880cd316470c0&graphType=verticalBar&period=month&date=today&segment=contentPiece==DAS-MAILING-<?php echo $tblMailing['tblMailing']['m_id'] ?>" />
					</div>
				</div>
				<div class="portlet light ">
					<div class="portlet-title">
						<div class="caption caption-md">
							<i class="icon-bar-chart theme-font hide"></i>
							<span class="caption-subject theme-font bold uppercase">Engagement in Last 10 Days</span>
						</div>
					</div>
					<div class="portlet-body">
						<img class="img-responsive"  src="https://tracking.digitaladvertising.systems/index.php?module=API&method=ImageGraph.get&idSite=<?php echo $tblMailing['tp']['tracksite_id'] ?>&apiModule=VisitsSummary&apiAction=get&token_auth=29e9d1b76ecf64c487d880cd316470c0&graphType=evolution&period=day&date=previous10&&segment=contentPiece==DAS-MAILING-<?php echo $tblMailing['tblMailing']['m_id'] ?>" />
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>	