<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="/">Dashboard</a>
    </li>
</ul>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Get User Information</span>
                </div>

            </div>
            <div class="portlet-body">

                <?php echo $this->Form->create('Info'); ?>
                <div class="row">
                    <div class="col-xs-8">
                        <?php echo $this->Form->input('email', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Add Email')); ?>
                    </div>
                    <div class="col-xs-4">
                        <?php echo $this->Form->submit('Submit', array('class' => 'btn green')); ?>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
                <?php
                if (!empty($user_info)) {
//                    print_r("<pre>");
//                    print_r($user_info);
//                    print_r("</pre>");
                    ?>
                    <div class="row profile margin-top-20">
                        <div class="col-md-12">
                            <!--BEGIN TABS-->
                            <div class="tabbable tabbable-custom tabbable-full-width">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab">Overview </a>
                                    </li>
                                    <li class="">
                                        <a href="#tab_1_3" data-toggle="tab">
                                            KMA
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_4" data-toggle="tab">
                                            Offers
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1_1">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <ul class="list-unstyled profile-nav">
                                                    <li>
                                                        <?php if (!empty($user_info->photos)) { ?>
                                                            <?php foreach ($user_info->photos as $photo) { ?>
                                                                <img src="<?php echo $photo->url; ?>" class="img-responsive" style="width:100%;" alt="<?php echo $photo->typeName; ?>" />
                                                            <?php } ?>
                                                        <?php } ?>

                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-md-8 profile-info">
                                                        <h1><?php echo $user_info->contactInfo->fullName; ?></h1>
                                                        <h5> Email : <?php echo $user_info->Email; ?></h5>
                                                        <h5> Family Name : <?php echo $user_info->contactInfo->familyName; ?></h5>
                                                        <h5> Given Name : <?php echo $user_info->contactInfo->givenName; ?></h5>
                                                    </div>
                                                    <!--end col-md-8-->
                                                    <?php if (!empty($user_info->contactInfo->chats)) { ?>
                                                        <div class="col-md-4">
                                                            <div class="portlet">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        Contact Info
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <table class="table">
                                                                        <tr><th>Client</th><th>name</th></tr>
                                                                        <?php foreach ($user_info->contactInfo->chats as $contact) { ?>
                                                                            <tr>
                                                                                <td><?php echo $contact->client; ?></td>
                                                                                <td><?php echo $contact->handle; ?></td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    </table>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                                <!--end row-->
                                                <div>
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_1_11">
                                                            <div class="portlet-body">
                                                                <h1>Social Info</h1>
                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Type</th>
                                                                            <th>Username</th>
                                                                            <th>Url</th>
                                                                            <th>Id</th>
                                                                            <th>Descrition</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php foreach ($user_info->socialProfiles as $profiles) { ?>
                                                                            <tr>
                                                                                <td><?php echo $profiles->typeName; ?></td>
                                                                                <td><?php echo $profiles->username; ?></td>
                                                                                <td><?php echo $profiles->url; ?></td>
                                                                                <td><?php echo $profiles->id; ?></td>
                                                                                <td>
                                                                                    <?php echo!empty($profiles->followers) ? '<div>Followers: ' . $profiles->followers . '</div>' : ''; ?>
                                                                                    <?php echo!empty($profiles->following) ? '<div>Following: ' . $profiles->following . '</div>' : ''; ?>
                                                                                    <?php echo $profiles->bio; ?>
                                                                                </td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="portlet yellow box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    Organizations
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Start Date</th>
                                                                            <th>Title</th>
                                                                            <th>Current</th>
                                                                            <th>Is Primary</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php foreach ($user_info->organizations as $org) { ?>
                                                                            <tr>
                                                                                <td><?php echo $org->name; ?></td>
                                                                                <td><?php echo $org->startDate; ?></td>
                                                                                <td><?php echo $org->title; ?></td>
                                                                                <td>
                                                                                    <?php echo!empty($org->current) ? 'Yes' : 'No'; ?>
                                                                                </td>
                                                                                <td>
                                                                                    <?php echo!empty($org->isPrimary) ? 'Yes' : 'No'; ?>
                                                                                </td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="portlet green box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    Demographics
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                    <?php foreach ($user_info->demographics->locationDeduced as $key => $org) { ?>
                                                                        <tr>
                                                                            <td><b><?php echo $key; ?>:</b></td>
                                                                            <td><?php echo $user_info->demographics->locationDeduced->$key->name; ?></td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                    <tr>
                                                                        <td><b>Gender:</b></td>
                                                                        <td><?php echo $user_info->demographics->gender; ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Location General:</b></td>
                                                                        <td><?php echo $user_info->demographics->locationGeneral; ?></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="portlet blue box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    Digital Footprint
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <h3>Scores</h3>
                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                    <tr>
                                                                        <th>Provider</th>
                                                                        <th>Type</th>
                                                                        <th>Value</th>
                                                                    </tr>
                                                                    <?php foreach ($user_info->digitalFootprint->scores as $key => $org) { ?>
                                                                        <tr>
                                                                            <td><?php echo $org->provider; ?></td>
                                                                            <td><?php echo $org->type; ?></td>
                                                                            <td><?php echo $org->value; ?></td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </table>
                                                                <h3>Topics</h3>
                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                    <tr>
                                                                        <th>Provider</th>
                                                                        <th>Value</th>
                                                                    </tr>
                                                                    <?php foreach ($user_info->digitalFootprint->topics as $key => $org) { ?>
                                                                        <tr>
                                                                            <td><?php echo $org->provider; ?></td>
                                                                            <td><?php echo $org->value; ?></td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="portlet green box">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    Macromeasures
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <h3>Interests</h3>
                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Score</th>
                                                                        <th>Category</th>
                                                                    </tr>
                                                                    <?php foreach ($user_info->macromeasures->interests as $key => $org) { ?>
                                                                        <tr>
                                                                            <td><?php echo $org->name; ?></td>
                                                                            <td><?php echo $org->score; ?></td>
                                                                            <td><?php echo $org->category; ?></td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </table>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--tab_1_2-->
                                    <div class="tab-pane" id="tab_1_3">
                                        <div class="">
                                            <div class='portlet'>
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>&nbsp;Name</th>
                                                            <th>Value</th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($user_info as $key=>$profiles) { 
                                                            if(substr($key, 0, 3) == 'KMA'){ 
                                                            ?>
                                                            <tr>
                                                                <td>&nbsp;<?php echo $key; ?></td>
                                                                <td><?php print_r($profiles); ?></td>
                                                               
                                                            </tr>
                                                        <?php }} ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                                </div>
                                        </div>
                                    </div>
                                    <!--end tab-pane-->
                                    <div class="tab-pane" id="tab_1_4">

                                    </div>
                                </div>
                            </div>
                            <!--END TABS-->
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<style>
    /***
Profile Page
***/
    .profile {
        position:relative;
    }

    .profile p {
        color:#636363;
        font-size:13px;
    }

    .profile p a {
        color:#169ef4;
    }

    .profile label {
        margin-top:10px;
    }

    .profile label:first-child {
        margin-top:0;
    }

    /*profile info*/
    .profile-classic .profile-image {
        position:relative;
    }

    .profile-classic .profile-edit {
        top:0;
        right:0;
        margin:0;
        color:#fff;
        opacity:0.6;
        padding:0 9px;
        font-size:11px;
        background:#000;
        position:absolute;
        filter:alpha(opacity=60); /*for ie*/
    }
    .profile-classic .profile-image img {
        margin-bottom:15px;
    }

    .profile-classic li {
        padding:8px 0;
        font-size:13px;
        border-top:solid 1px #f5f5f5;
    }

    .profile-classic li:first-child {
        border-top:none;
    }

    .profile-classic li span {
        color:#666;
        font-size:13px;
        margin-right:7px;
    }

    /*profile tabs*/
    .profile .tabbable-custom-profile .nav-tabs > li > a {
        padding:6px 12px;
    }


    /*profile navigation*/
    .profile ul.profile-nav {
        margin-bottom:30px;
    }

    .profile ul.profile-nav li {
        position:relative;
    }

    .profile ul.profile-nav li a {
        color:#557386;
        display:block;
        font-size:14px;
        padding:8px 10px;
        margin-bottom:1px;
        background:#f0f6fa;
        border-left:solid 2px #c4d5df;
    }

    .profile ul.profile-nav li a:hover {
        color:#169ef4;
        background:#ecf5fb;
        text-decoration:none;
        border-left:solid 2px #169ef4;
    }

    .profile ul.profile-nav li a.profile-edit {
        top:0;
        right:0;
        margin:0;
        color:#fff;
        opacity:0.6;
        border:none;
        padding:3px 9px;
        font-size:12px;
        background:#000;
        position:absolute;
        filter:alpha(opacity=60); /*for ie*/
    }

    .profile ul.profile-nav li a.profile-edit:hover {
        text-decoration:underline;
    }

    .profile ul.profile-nav a span {
        top:0;
        right:0;
        color:#fff;
        font-size:16px; 
        padding:7px 13px;
        position:absolute;
        background:#169ef4;
    }

    .profile ul.profile-nav a:hover span {
        background:#0b94ea;
    }

    /*profile information*/
    .profile-info h1 {
        color:#383839;
        font-size:24px;
        font-weight:400;
        margin:0 0 10px 0;
    }

    .profile-info ul {
        margin-bottom:15px;
    }

    .profile-info li {
        color:#6b6b6b;
        font-size:13px;
        margin-right:15px;
        margin-bottom:5px;
        padding:0 !important;
    }

    .profile-info li i {
        color:#b5c1c9;
        font-size:15px;
    }

    .profile-info li:hover i {
        color:#169ef4;
    }

    /*profile sales summary*/
    .sale-summary ul {
        margin-top:-12px;
    }
    .sale-summary li {
        padding:10px 0;
        overflow:hidden;
        border-top:solid 1px #eee;
    }

    .sale-summary li:first-child {
        border-top:none;
    }

    .sale-summary li .sale-info {
        float:left;
        color:#646464;
        font-size:14px;
        text-transform:uppercase;
    }

    .sale-summary li .sale-num {
        float:right;
        color:#169ef4;
        font-size:20px;
        font-weight:300;
    }

    .sale-summary li span i {
        top:1px;
        width:13px;
        height:14px;
        margin-left:3px;
        position:relative;
        display:inline-block;
    }

    .sale-summary li i.icon-img-up {
        background:url(../../img/icon-img-up.png) no-repeat !important;
    }

    .sale-summary li i.icon-img-down {
        background:url(../../img/icon-img-down.png) no-repeat !important;
    }

    .sale-summary .caption h4 {
        color:#383839;
        font-size:18px;
    }

    .sale-summary .caption {
        border-color:#c9c9c9;
    }

    /*latest customers table*/
    .profile .table-advance thead tr th {
        background:#f0f6fa;
    }

    .profile .table-bordered th, 
    .profile .table-bordered td,
    .profile .table-bordered {
        border-color:#e5eff6;
    }

    .profile .table-striped tbody > tr:nth-child(2n+1) > td, 
    .profile .table-striped tbody > tr:nth-child(2n+1) > th {
        background:#fcfcfc;
    }

    .profile .table-hover tbody tr:hover td, 
    .profile .table-hover tbody tr:hover th {
        background:#f5fafd;
    }

    /*add portfolio*/
    .add-portfolio {
        overflow:hidden;
        margin-bottom:30px;
        background:#f0f6fa;
        padding: 12px 14px;
    }

    .add-portfolio span {
        float: left;
        display: inline-block;
        font-weight: 300;
        font-size: 22px;
        margin-top: 0px;
    }

    .add-portfolio .btn {
        margin-left: 20px;
    }

    /*portfolio block*/
    .portfolio-block {
        background:#f7f7f7;
        margin-bottom:15px;
        overflow:hidden;
    }

    .portfolio-stat {
        overflow: hidden;
    }

    /*portfolio text*/
    .portfolio-text {
        overflow:hidden;
    }


    .portfolio-text img {
        float:left;
        margin-right:15px;
    }

    .portfolio-text .portfolio-text-info {
        overflow:hidden;
    }

    /*portfolio button*/
    .portfolio-btn a {
        display:block;
        padding:25px 0;
        background:#ddd !important;
    }

    .portfolio-btn a:hover {
        background:#1d943b !important;
    }

    .portfolio-btn span {
        color:#fff;
        font-size:22px;
        font-weight:200;  
    }

    /*portfolio info*/
    .portfolio-info {
        float:left;
        color:#616161;
        font-size:12px;
        padding:10px 25px;
        margin-bottom:5px;
        text-transform:uppercase;
    }

    .portfolio-info span {
        color:#16a1f2;
        display:block;
        font-size:28px;
        line-height: 28px;
        margin-top:0px;
        font-weight:200;
        text-transform:uppercase;
    }

    /*portfolio settings*/
    .profile-settings {
        background:#fafafa;
        padding:15px 8px 0;
        margin-bottom:5px;
    }

    .profile-settings p {
        padding-left:5px;
        margin-bottom:3px;
    }

    .profile-settings .controls > .radio, 
    .profile-settings .controls > .checkbox {
        font-size:12px;
        margin-top:2px !important;
    }

</style>