<?php $this->Html->addCrumb('Advertisers', $this->Html->url( null, true )); ?>
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Create Order Details
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
                                  <?php echo $this->Form->create(false,array('url'=>array('controller'=>'insertionorder','action'=>'add'),'class'=>'form-horizontal','name'=>'submit_form_order_add','id'=>'submit_form_order_add' ));?>
                                   <?php echo $this->Form->hidden('tblOrder.status',array('value'=>1));  ?>   
                                 							
				<!--<form action="/insertionorder/add" id="submit_form_order_add" method="POST" class="form-horizontal">
				-->	<div class="form-body">
						<div class="alert alert-danger display-hide">
							<button class="close" data-close="alert"></button>
							You have some form errors. Please check below.
						</div>
						<div class="alert alert-success display-hide">
							<button class="close" data-close="alert"></button>
							<img src="/img/ajax-loade.gif"> Creating..
						</div>
						<h3 class="form-section">Order Details</h3>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">IO Name <span class="required">
									* </span></label>
									<div class="col-md-9">
										<div class="input-icon right">
											<i class="fa"></i>
											<input type="text" class="form-control" name="data[tblOrder][order_name]" placeholder="Enter Order Name Here">
											<span class="help-block">
											Insertion Order Name </span>
										</div>
									</div>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Revenue Type</label>
									<div class="col-md-9">
										<div class="clearfix">
											<div class="icheck-inline">
												<label>
												<input type="radio" name="isFree" value="1" class="icheck revenue_type"> In-House</label>
												<label>
												<input type="radio" name="isFree" value="0"  checked class="icheck revenue_type">Revenue </label>
											</div>
										</div>
										<span class="help-block">
										Revenue Orders are for paying customers, whereas In-House orders are for internal campaigns with no income. </span>
									</div>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6 hide">
								<div class="form-group">
									<label class="control-label col-md-3">Sales Manager</label>
									<div class="col-md-9">
										<select name="data[tblOrder][salesmanager_id]" class="form-control">
											<?php foreach ($salesmanager as $option){ ?>
												<option value="<?php echo $option['id'] ?>"><?php echo $option['email'] ?></option>
											<?php } ?>
										</select>
										<span class="help-block">
										Select your Sales Manager. </span>
									</div>
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
						<div class="row">
							
							<div class="col-md-6 hide">
								<div class="form-group">
									<label class="control-label col-md-3">Trafficker</label>
									<div class="col-md-9">
										<select name="data[tblOrder][trafficker_id]" class="form-control">
											<?php foreach ($trafficker as $option){ ?>
												<option value="<?php echo $option['id'] ?>"><?php echo $option['email'] ?></option>
											<?php } ?>
										</select>
										<span class="help-block">
										Select your Trafficker. </span>
									</div>
								</div>
							</div>
							<!--/span-->
							
						</div>
						<h3 class="form-section">Advertiser Details</h3>
						<div  class="row">
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Company</label>
									<div class="col-md-9">
									
										<select name="data[tblOrder][advertiser_id]" class="select2me form-control js-example-basic-single">
											<option value="">--Select--</option>
											<?php foreach ($advertisers as $option){?>
												<option <?php echo $option['tblProfile']['dfp_company_id'] == $company_id ? "selected" : "" ?> data-foo="<?php echo $option['tblProfile']['dfp_company_id'];?>" value="<?php echo $option['tblProfile']['dfp_company_id'].'@'.$option['User']['id']; ?>"><?php echo $option['tblProfile']['CompanyName']." - ".$option['User']['ContactName']; ?></option>
											<?php } ?>
										</select>
									 
										<span class="help-block">
										Not there? <a href="#" data-toggle="modal" data-target="#myModal">Click to Create New Company >> </a></span>
										
									</div>
								</div>
							</div>
							<!--/span-->
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Contact Name <span class="required">
									* </span></label>
									<div class="col-md-9">
										<div class="input-icon right">
											<i class="fa"></i>
											<input type="text" class="form-control" name="data[User][ContactName]" placeholder="Enter Contact Name Here" />
											<span class="help-block">
											Enter Representative Name. </span>
										</div>
									</div>
								</div>
							</div>
							<!--/span-->
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Email <span class="required">
									* </span></label>
									<div class="col-md-9">
										<div class="input-icon right">
											<i class="fa"></i>
											<input type="text" class="form-control" name="data[User][email]" placeholder="Enter Email Here">
											<span class="help-block">
											Enter Representative Email. </span>
										</div>
									</div>
								</div>
							</div>
							<!--/span-->
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Phone No.</label>
									<div class="col-md-9">
										<div class="input-icon right">
											<i class="fa"></i>
											<input type="text" class="form-control" name="data[User][phone_no]" placeholder="Enter Phone No. Here" />
											<span class="help-block">
											Enter Representative Phone no. </span>
										</div>
									</div>
								</div>
							</div>
							<!--/span-->			
						</div>
						<!--/row-->
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-offset-6 col-md-6">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
		</div>
	</div>
	<div id="myModal" class="modal fade">
		
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close AddCompanyiframe" data-dismiss="modal">&times;</button>
				</div>
				<iframe class="embed-responsive-item" src="/company/add/pop" width="100%" height="650px"></iframe>
				<div class="modal-footer">
					<button type="button" class="btn btn-default AddCompanyiframe" data-dismiss="modal">Close</button>
				</div>
			<!-- Content will be loaded here from "remote.php" file -->
			</div>
		</div>
	</div>
<style>

#myModal .modal-dialog {
    width: 926px;
    margin: 30px auto;
}
</style>
<script>
	 var checkr = 0;
	jQuery('#contact_name').unbind('blur').bind('blur',function(){	
		str = $(this).val().replace(/\s/g, "\t");
		$(this).val(str);
	});
	$('input:radio[name="isFree"]').on('ifChecked', function(event){
		   revenue_type = this.checked ? this.value : 0
		   $.ajax({
					type: "POST",
					url: "/insertionorder/get_company_ajax/"+revenue_type,
					dataType: "text",
					success: function(data){
						$(".select2me").empty();
						$(".select2me").append(data);
						
					}
				});
	});
		
	 
</script>
<!-- END PAGE CONTENT INNER -->