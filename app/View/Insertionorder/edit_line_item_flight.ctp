<?php $this->Html->addCrumb('Advertisers', $this->Html->url(null, true)); ?>
<style>
    .panel-default>.panel-heading {
        background-color : #73716e !important;
        color : #fff;
    }
    /*#deliverySettings .form-group{overflow:hidden;}
    #deliverySettings .control-label{text-align:right;}
    */
    .panel-default>.panel-heading > .panel-title > a:hover {
        color : #fff;
        text-decoration:underline;
    }
    .panel-title{
        font-weight:200 !important;
    }
    .select2-container-multi .select2-choices .select2-search-choice {
        margin : 6px 0 3px 5px !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice
    {
        padding: 5px 5px;
    }
</style>
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey-gallery">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Add/Edit Flight
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <?php echo $this->Form->create(false, array('url' => array('controller' => 'insertionorder', 'action' => 'edit_line_item_flight', $flight_id, $lineItemId, $status), 'class' => 'form-horizontal', 'type' => 'file')); ?>
                <?php echo $this->Form->hidden('tblFlight.status'); ?>
                <?php echo $this->Form->hidden('tblFlight.fl_temp'); ?>	
                <?php echo $this->Form->hidden('tblFlight.fl_id'); ?>
                <?php echo $this->Form->hidden('tblFlight.fl_size'); ?>
                <?php echo $this->Form->hidden('tblDeliverySetting.ds_id'); ?>
                <?php echo $this->Form->hidden('tblFlight.zip_targeting'); ?>
                <?php echo $this->Form->hidden('tblLineItem.li_id', array('value' => $lineItemId)); ?>

                <div class="form-body">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        You have some form errors. Please check below.
                    </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        <img src="/img/ajax-loade.gif"> Creating..
                    </div>
                    <h3 class="form-section">Flight Details</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-12"><b>Flight Name</b></label>
                                <div class="col-md-12">
                                    <?php
                                    echo $this->Form->text('tblFlight.fl_name', array(
                                        'div' => false,
                                        'label' => false,
                                        'class' => 'form-control',
                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-12"><b>Line Item</b></label>
                                <div class="col-md-12">
                                    <?php echo $lineItem_details['tblLineItem']['li_name']; ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="col-md-12"><b>Cost Per Action </b></label>
                                <div class="col-md-12">
                                    <?php echo '$' . $lineItem_details['tblLineItem']['li_rate']; ?> <?php echo $lineItem_details['tcs']['cs_name']; ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-12"><b>Select Flight Dates</b></label>
                                <div class="col-md-12">
                                    <div class="row date date-picker form-group" id="event_period" data-date-start-date="+0d">
                                        <div class="col-md-6">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <?php echo $this->Form->input('tblFlight.fl_start', array('id' => 'dt1', 'type' => 'text', 'placeholder' => 'Start Date', 'div' => false, 'label' => false, 'class' => 'form-control actual_range')); ?>	
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <?php echo $this->Form->input('tblFlight.fl_end', array('id' => 'dt2', 'type' => 'text', 'placeholder' => 'End Date', 'div' => false, 'label' => false, 'class' => 'form-control actual_range')); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 trigger_action">
                            <div class="form-group">
                                <label class="col-md-12"></label>
                                <div class="col-md-12">
                                    <?php echo $this->Form->input('tblFlight.fl_trigger_action', array('type' => 'checkbox', 'id' => 'fl_trigger_action', 'label' => false, 'div' => false, 'data-checkbox' => 'icheckbox_square-grey', 'class' => 'icheck', 'after' => __('<label for="fl_trigger_action">Setup up as Trigger </label>'))); ?>


                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 trigger_action">
                            <div class="form-group">
                                <label class="col-md-12"></label>
                                <div class="col-md-12">
                                    <?php echo $this->Form->input('tblFlight.fl_default_flight', array('type' => 'checkbox', 'id' => 'default_flight', 'label' => false, 'div' => false, 'data-checkbox' => 'icheckbox_square-grey', 'class' => 'icheck', 'after' => __('<label for="fl_trigger_action">Make it Default </label>'))); ?>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label><b>Available Allocation</b></label>
                                    <div>
                                        <span class="current_allocation" style="display:none;"><?php
                                            /*$total_availabe = $lineItem_details['tblLineItem']['li_quantity'] - $allocatedallocation;
                                            echo $total_availabe_allocation = $total_availabe + get_percentage($total_availabe, $lineItem_details['tblLineItem']['li_rate']);*/
                                            ?>
                                        </span>
                                        <span class="available_allocation"><b>Daily: </b><?php echo $lineItem_details['tblLineItem']['li_daily_cap']; ?> | <b>Monthly: </b> <?php echo $lineItem_details['tblLineItem']['li_monthly_cap']; ?> | <b>Total: </b> <?php echo $lineItem_details['tblLineItem']['li_quantity']; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <?php
                                        echo $this->Form->input('tblFlight.fl_ptype_id', array(
                                            'class' => 'js-example-basic-single js-states form-control',
                                            'label' => false,
                                            'multiple' => 'multiple',
                                            'type' => 'select',
                                            'id' => 'selectMediaType',
                                            'options' => $producttype));
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="clearfix">
                                        <div class="icheck-inline">
                                            <?php
                                            $options = array(
                                                1 => 'Run as Direct Sold',
                                                2 => 'Run as Indirect Network Deals',
                                                3 => 'Run Remnant Only'
                                            );
                                            $attributes = array(
                                                'legend' => false,
                                                'div' => false,
                                                'data-checkbox' => 'icheckbox_square-grey',
                                                'class' => 'icheck',
                                            );
                                            echo $this->Form->radio('tblFlight.fl_run_as', $options, $attributes);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-group accordion col-md-12" id="accordion3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
                                            Ad Units</a>
                                    </h4>
                                </div>
                                <div id="collapse_3_1" class="panel-collapse in">
                                    <div class="panel-body">	
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <?php
                                                        echo $this->Form->input('adunit-select', array(
                                                            'class' => 'form-control',
                                                            'label' => false,
                                                            'multiple' => 'multiple',
                                                            'type' => 'select',
                                                            'size' => '10',
                                                            'id' => 'adunit-select',
                                                        ));
                                                        ?>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div  id="changeAdUnitDiv" style="display: <?php echo (!empty($adunit_select_div)) ? 'block' : 'none' ?>;padding: 0 15px;">
                            <h3  class="form-section" >Manage Lead Allocation</h3>
                            <div class="row">

                                <div class="col-md-12">
                                    <table class="table table-hover table-bordered table-striped" id= "adunitcontent">
                                        <thead>
                                            <tr>
                                                <th>Ad Unit Name</th>
                                                <th>Allocation(Daily, Monthly & Total)</th>
                                                <th>Ad Unit Revenue Share (in %)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                    </table> 
                                </div>		
                            </div>
                        </div>
                        <div class="panel-group accordion col-md-12" id="accordion5">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion5" href="#collapse_5_1">
                                            Ad Creative</a>
                                    </h4>
                                </div>
                                <div id="collapse_5_1" class="panel-collapse in">
                                    <div class="panel-body">	
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <?php
                                                        echo $this->Form->input('FlightsCreative.cr_id', array(
                                                            'class' => 'form-control',
                                                            'label' => false,
                                                            'type' => 'select',
                                                            'id' => 'adunit-Creative-type',
                                                            'multiple' => 'multiple',
                                                            'size' => '10',
                                                                )
                                                        );
                                                        ?> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-group accordion col-md-12" id="accordion3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2">
                                            Targeting - Geography </a>
                                    </h4>
                                </div>
                                <div id="collapse_3_2" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="clearfix">
                                                            <div class="icheck-inline">
                                                                <?php
                                                                if ($lineItem_details['tcs']['cs_id'] != 3) {
                                                                    $option = (!empty($this->request->data['tblFlight']['fl_targeting_option'])) ? json_decode($this->request->data['tblFlight']['fl_targeting_option'], true) : array();
                                                                    ?>

                                                                    <?php echo $this->Form->input('tblFlight.fl_targeting_option.', array('type' => 'checkbox', 'id' => 'fl_targeting_option1', 'label' => false, 'checked' => in_array(1, $option) ? True : False, 'value' => 1, 'div' => false, 'data-checkbox' => 'icheckbox_square-grey', 'class' => 'icheck', 'after' => __('<label for="fl_targeting_option1">Sign-Up Geo Location</label>'))); ?>

                                                                    <?php echo $this->Form->input('tblFlight.fl_targeting_option.', array('type' => 'checkbox', 'checked' => in_array(2, $option) ? True : False, 'id' => 'fl_targeting_option2', 'label' => false, 'value' => 2, 'div' => false, 'data-checkbox' => 'icheckbox_square-grey', 'class' => 'icheck', 'after' => __('<label for="fl_targeting_option2"> Post Email Opening Geo Location </label>'))); ?>

                                                                    <?php echo $this->Form->input('tblFlight.fl_targeting_option.', array('type' => 'checkbox', 'id' => 'fl_targeting_option3', 'label' => false, 'checked' => in_array(3, $option) ? True : False, 'value' => 3, 'div' => false, 'data-checkbox' => 'icheckbox_square-grey', 'class' => 'icheck', 'after' => __('<label for="fl_targeting_option3">Current Email Opening Geo-Location( Only in case of Newsletter Sponsorship Line Items ) </label>'))); ?>

                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <?php
                                                        echo $this->Form->input('geography-select', array(
                                                            'class' => 'form-control',
                                                            'label' => false,
                                                            'multiple' => 'multiple',
                                                            'type' => 'select',
                                                            'id' => 'geography-select',
                                                            'size' => '10',
                                                            'selected' => $geography_select,
                                                            'options' => $tblCountries));
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                </div>
                            </div>	
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="panel-group accordion col-md-12" id="accordion4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion4" href="#collapse_4_2">
                                            Audience Targeting </a>
                                    </h4>
                                </div>
                                <div id="collapse_4_2" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="col-md-12"><b>Select Flight Name For Clone Audience</b></label>
                                                    <div class="col-md-12">
                                                        <?php
                                                        echo
                                                        $this->Form->input('tblFlight.audiences', array(
                                                            'class' => 'audience form-control',
                                                            'label' => false,
                                                            'multiple' => 'multiple',
                                                            'type' => 'select',
                                                            'empty' => 'select flight name ',
                                                            'options' => $flight_list));
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <div id="container" class="search-template">
                                                        <div id="content">
                                                            <div id="builder"></div>
                                                            <?php echo $this->Form->textarea('tblFlight.fl_audience_segment', array('div' => false, 'label' => false, 'class' => 'form-control json-parsed display-hide', 'readonly' => true)); ?>


                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <div class="icheck-list">
                                                                    <label>
                                                                        <input type="checkbox" id="zipcodecsv_input" name="zipcodecsv_input" onclick="checkZipcodeAudience();"> 
                                                                        <?php echo empty($this->data['tblFlight']['zip_targeting']) ? 'Add Zip Code Allocation CSV' : 'Change Zip Code Allocation CSV'; ?>

                                                                    </label>
                                                                    <?php if (!empty($this->data['tblFlight']['zip_targeting'])) { ?>
                                                                        <p style="padding-left:45px;"><small class="help"><a id ="view_existing_zips" onclick="$('#existing_zips').toggle();">View Existing Zipcodes</a></small></p>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group" id="zipcodeCSV" style="display:none;">
                                                            <label>Upload Zipcode CSV</label>
                                                            <input type="file"  name="zipcode_csv">
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php if (!empty($this->data['tblFlight']['zip_targeting'])) { ?>
                                                    <div class="row" id="existing_zips" style="display:none;">
                                                        <div class="col-xs-9"  style="font-size:12px;color:#ccc;background-color: #f5f5f5; border: 1px solid #ccc;padding:5px;">
                                                            <?php echo implode(',', $csv_data); ?>
                                                        </div>
                                                        <div class='col-xs-3'><a  onclick="delete_zip_csv(<?php echo $this->data['tblFlight']['fl_id']; ?>);" class="btn red btn-xs" href="javascript:;">Delete</a></div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row hide">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2"> 
                                                        <div class="checkbox" style="float:left">
                                                            <div class="icheck-list">
                                                                <label>
                                                                    <input type="checkbox" name="data[tblLineItem][li_segment_gender]"  class="icheck" data-checkbox="icheckbox_square-grey"> GENDER </label>
                                                            </div>
                                                        </div>
                                                    </label>
                                                    <div class="col-md-4"> 
                                                        <select multiple="multiple" name="data[tblFlightAudiences][fl_as_id][]" id="audience1" class="form-control">
                                                            <option value="60">Male</option>
                                                            <option value="59">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row hide">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2"> 
                                                        <div class="checkbox" style="float:left">
                                                            <div class="icheck-list">
                                                                <label>
                                                                    <input type="checkbox" name="data[tblLineItem][li_segment_edu]"  class="icheck" data-checkbox="icheckbox_square-grey"> Education </label>
                                                            </div>
                                                        </div>
                                                    </label>
                                                    <div class="col-md-4"> 
                                                        <select multiple="multiple" name="data[tblFlightAudiences][fl_as_id][]" id="audience2" class="form-control">
                                                            <option value="63">Some High School</option>
                                                            <option value="64">High School Grad</option>
                                                            <option value="65">Some College</option>
                                                            <option value="66">College Graduate</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row hide">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2"> 
                                                        <div class="checkbox" style="float:left">
                                                            <div class="icheck-list">
                                                                <label>
                                                                    <input type="checkbox" name="data[tblLineItem][li_segment_income]"  class="icheck" data-checkbox="icheckbox_square-grey"> Income </label>
                                                            </div>
                                                        </div>
                                                    </label>
                                                    <div class="col-md-4"> 
                                                        <select multiple="multiple" name="data[tblFlightAudiences][fl_as_id][]" id="audience3" class="form-control">
                                                            <option value="81">Under 30 k</option>
                                                            <option value="82">30k to 50k</option>
                                                            <option value="83">50k to 75k</option>
                                                            <option value="84">75k to 100k</option>
                                                            <option value="85">100k to 150k</option>
                                                            <option value="86">150k+</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row hide">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2"> 
                                                        <div class="checkbox" style="float:left">
                                                            <div class="icheck-list">
                                                                <label>
                                                                    <input type="checkbox" name="data[tblLineItem][li_segment_occupation]"  class="icheck" data-checkbox="icheckbox_square-grey"> Occupation </label>
                                                            </div>
                                                        </div>
                                                    </label>
                                                    <div class="col-md-4"> 
                                                        <select multiple="multiple" name="data[tblFlightAudiences][fl_as_id][]" id="audience4" class="form-control">
                                                            <option value="67">Prof/Tech</option>
                                                            <option value="68">Admin / Manager</option>
                                                            <option value="69">Blue Collar</option>
                                                            <option value="70">Clerical/Service</option>
                                                            <option value="71">Homemaker</option>
                                                            <option value="72">Retired</option>
                                                            <option value="73">Business Owner</option>
                                                            <option value="74">Sales/Marketing</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row hide">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2"> 
                                                        <div class="checkbox" style="float:left">
                                                            <div class="icheck-list">
                                                                <label>
                                                                    <input type="checkbox" name="data[tblLineItem][li_segment_agegrp]"  class="icheck" data-checkbox="icheckbox_square-grey"> Age </label>
                                                            </div>
                                                        </div>
                                                    </label>
                                                    <div class="col-md-4"> 
                                                        <select multiple="multiple" name="data[tblFlightAudiences][fl_as_id][]" id="audience5" class="form-control">
                                                            <option value="75">18-34</option>
                                                            <option value="76">35-44</option>
                                                            <option value="77">45-54</option>
                                                            <option value="78">55-64</option>
                                                            <option value="79">65-74</option>
                                                            <option value="80">75-plus</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row hide">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2"> 
                                                        <div class="checkbox" style="float:left">
                                                            <div class="icheck-list">
                                                                <label>
                                                                    <input type="checkbox" name="data[tblLineItem][li_segment_home_owner]"  class="icheck" data-checkbox="icheckbox_square-grey"> Home Owner </label>
                                                            </div>
                                                        </div>
                                                    </label>
                                                    <div class="col-md-4"> 
                                                        <select multiple="multiple" name="data[tblFlightAudiences][fl_as_id][]" id="audience6" class="form-control">
                                                            <option value="94">Owner</option>
                                                            <option value="95">Rent</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row hide">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2"> 
                                                        <div class="checkbox" style="float:left">
                                                            <div class="icheck-list">
                                                                <label>
                                                                    <input type="checkbox" name="data[tblLineItem][li_segment_home_owner]"  class="icheck" data-checkbox="icheckbox_square-grey"> Interest </label>
                                                            </div>
                                                        </div>
                                                    </label>
                                                    <div class="col-md-4"> 
                                                        <select multiple="multiple" name="data[tblFlightAudiences][fl_as_id][]" id="audience7" class="form-control">
                                                            <?php foreach ($as_details as $options) { ?>
                                                                <option value='<?php echo $options['tblAudienceSegments']['kma_id'] ?>'><?php echo $options['tblAudienceSegments']['kma_data_label'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                </div>
                            </div>	
                        </div>
                    </div>
                    <h3 class="form-section" id="behaviourHeader">Behaviour Settings</h3>
                    <div class="row" id="behaviourSettings">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-12"><b>Select Behavior</b></label>
                                <div class="col-md-12">

                                    <?php
                                    echo $this->Form->input('tblFlight.fl_behavior', array(
                                        'class' => 'form-control',
                                        'id' => 'behaviour',
                                        'label' => false,
                                        'type' => 'select',
                                        'multiple' => 'multiple',
                                        'type' => 'select',
                                        'options' => array('0' => 'All', '1' => 'Sent', '2' => 'Not Opened', '3' => 'Opened', '4' => 'Clicked')
                                            )
                                    );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12"><b>Select Company / Campaign / Order / Line Item / Flight</b></label>
                                <div class="col-md-12">
                                    <?php echo $this->Form->text('tblFlight.fl_depending_items', array('placeholder' => 'Start Typing to Select Multiple', 'div' => false, 'label' => false, 'class' => 'form-control')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-12"><b>Action</b></label>	
                                <div class="clearfix col-md-12">
                                    <div class="icheck-inline">
                                        <?php
                                        $options = array('1' => 'Target');
                                        $attributes = array(
                                            'legend' => false,
                                            'div' => false,
                                            'data-checkbjox' => 'ichjeckbox_square-grey',
                                            'class' => 'icheck',
                                            'type' => 'radio',
                                        );
                                        echo $this->Form->radio('tblFlight.fl_action', $options, $attributes);
                                        ?>

                                        <div class="clearfix"></div>
                                        <label style="display:none;">
                                        <!--<input type="radio" name="data[tblFlight][fl_action]" value="2" checked class="icheck">Suppress </label>-->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4" style="display:none;">
                            <div class="form-group">
                                <label class="col-md-5"><b>Enter Frequency of Behavior</b></label>
                                <div class="col-md-5">

                                    <?php echo $this->Form->email('tblFlight.fl_bahavior_freq', array('placeholder' => '# Times', 'div' => false, 'label' => false, 'class' => 'form-control')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-8" style="display:none;">
                            <label class="col-md-12"><b>Do Not Traffic</b></label>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-1">For</div>	
                                    <div class="col-md-5">
                                        <?php
                                        echo $this->Form->input('tblFlight.fl_donot_traffic', array(
                                            'class' => 'js-example-basic-single js-states form-control',
                                            'label' => false,
                                            'type' => 'select',
                                            'empty' => '--Select One--',
                                            'options' => array('7' => '7', '30' => '30')));
                                        ?>


                                    </div>
                                    <div class="col-md-2">days</div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <h3 class="form-section" id="deliveryHeader">Delivery Settings</h3>
                    <div class="row" id="deliverySettings">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-12"><b>Select Flight Name For Clone of Delivery Settings</b></label>
                                <div class="col-md-12">
                                    <?php
                                    //pr($flight_list);
                                    echo
                                    $this->Form->input('tblFlight.flight_id_delivery', array(
                                        'class' => 'audience form-control',
                                        'label' => false,
                                        'type' => 'select',
                                        'multiple' => 'multiple',
                                        'empty' => 'select flight name ',
                                        'options' => $flight_list));
                                    ?>
                                </div>
                            </div>
                        </div>	
                        <div class="col-md-8" align="center">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="clearfix">
                                        <div class="icheck-inline">


                                            <?php
                                            //  print_r("<pre>");print_r($this->data);print_r("</pre>");
                                            $options = array(
                                                '1' => 'Real Time Delivery',
                                                '2' => 'Batch Post',
                                                '3' => 'FTP Upload',
                                                '4' => 'HTTP GET/POST'
                                            );
                                            $attributes = array(
                                                'legend' => false,
                                                //'label' => false,
                                                'div' => false,
                                                'data-checkbox' => 'icheckbox_square-grey',
                                                'class' => 'icheck',
                                                'value' => $this->data['tblDeliverySetting']['ds_delivery_mode']
                                            );
                                            echo $this->Form->radio('tblDeliverySetting.DeliveryMethod', $options, $attributes);
                                            ?>


                                        </div>
                                    </div>
                                    <div class="form-body" id="realTimeDelivery" style="display: <?php if ($this->data['tblDeliverySetting']['ds_delivery_mode'] == 1) { ?> block; <?php } else { ?> none; <?php } ?>">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Email Address</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-envelope"></i>
                                                    </span>

                                                    <?php echo $this->Form->email('tblDeliverySetting.ds_realtime_email', array('placeholder' => 'Email Address', 'div' => false, 'label' => false, 'class' => 'form-control')); ?>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body" id="batchPost"  style="display: <?php if ($this->data['tblDeliverySetting']['ds_delivery_mode'] == 2) { ?> block; <?php } else { ?> none; <?php } ?>">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Email Address</label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-envelope"></i>
                                                    </span>
                                                    <?php echo $this->Form->email('tblDeliverySetting.ds_batch_email', array('placeholder' => 'Email Address', 'div' => false, 'label' => false, 'class' => 'form-control')); ?>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Batch Subject</label>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('tblDeliverySetting.ds_batch_subject', array('placeholder' => 'Subject of Email', 'div' => false, 'label' => false, 'class' => 'form-control')); ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Frequency <span class="required">
                                                    * </span>
                                            </label>
                                            <div class="col-md-4">


                                                <?php
                                                echo $this->Form->input('tblDeliverySetting.ds_batch_frequency', array(
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'empty' => 'Select...',
                                                    'type' => 'select',
                                                    'options' => array('1' => 'Daily Batch', '2' => 'Weekly Batch', '3' => 'Montly Batch')
                                                ));
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body" id="ftpUpload"  style="display: <?php if ($this->data['tblDeliverySetting']['ds_delivery_mode'] == 3) { ?> block; <?php } else { ?> none; <?php } ?>">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">URL</label>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('tblDeliverySetting.ds_ftp_url', array("placeholder" => "URL (Prefix with ' ftp:// ')", 'div' => false, 'label' => false, 'class' => 'form-control')); ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">User Name</label>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('tblDeliverySetting.ds_ftp_username', array("placeholder" => "User Name", 'div' => false, 'label' => false, 'class' => 'form-control')); ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Password</label>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('tblDeliverySetting.ds_ftp_password', array("placeholder" => "Password", 'div' => false, 'label' => false, 'class' => 'form-control', 'type' => 'password')); ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Folder Name</label>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('tblDeliverySetting.ds_ftp_folder', array("placeholder" => "Folder Name", 'div' => false, 'label' => false, 'class' => 'form-control')); ?>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-body" id="httpGetPost"  style="display: <?php if ($this->data['tblDeliverySetting']['ds_delivery_mode'] == 4) { ?> block; <?php } else { ?> none; <?php } ?>">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Basic Authentication</label>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('tblDeliverySetting.ds_login', array("placeholder" => "username", 'div' => false, 'label' => false, 'class' => 'form-control')); ?>

                                            </div>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('tblDeliverySetting.ds_password', array("placeholder" => "password", 'type' => 'password', 'div' => false, 'label' => false, 'class' => 'form-control')); ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">URL</label>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('tblDeliverySetting.ds_http_url', array("placeholder" => "URL (Prefix with ' ftp:// ", 'div' => false, 'label' => false, 'class' => 'form-control')); ?>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Get / Post <span class="required">
                                                    * </span>
                                            </label>
                                            <div class="col-md-4">

                                                <?php
                                                echo $this->Form->input('tblDeliverySetting.ds_http_get_post', array(
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'empty' => 'Select...',
                                                    'type' => 'select',
                                                    'options' => array('1' => 'GET', '2' => 'POST')
                                                ));
                                                ?>


                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Posting Response</label>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('tblDeliverySetting.ds_http_response', array("placeholder" => "Posting Response", 'div' => false, 'label' => false, 'class' => 'form-control')); ?>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END ACCORDION PORTLET-->
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6">
                                        <button type="submit" class="btn green  parse-sql">Save and Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $this->end(); ?>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT INNER -->	
<?php
function get_percentage($percentage = 0, $total = 0) {
    return intval(($total == 0) ? 0 : ($percentage / 100) * $total);
}
?>
		
<script type="text/javascript">
$(document).ready(function(){
	ajaxFireonChange();
	
	$('#tblFlightFlightIdDelivery').val('<?php echo isset($flightID) && !empty($flightID) ? $flightID : ''; ?>');
	$(".audience").select2({
        maximumSelectionLength: 1,
        placeholder: "Start Typing to Flight Name "
    });
});
   /**
	* @module      : Onchange Ajax Fire
	* @Created By  : Satanik
	* @date        : 13/1/2017
	* @Description : Delivery Setting on change Ajax Fire 
	*/
    function ajaxFireonChange(){
	   $('#tblFlightFlightIdDelivery').on('change',function(){
			var currentValue = $(this).val(); 
			if(currentValue != null){
				$('.iradio_minimal-grey').each(function(){
		   	   	  $(this).removeClass('checked');
		   	   	  $('#deliverySettings #realTimeDelivery').find('.form-control').val('');
		   	   	  $('#deliverySettings #batchPost').find('.form-control').val('');
		   	   	  $('#deliverySettings #ftpUpload').find('.form-control').val('');
		   	   	  $('#deliverySettings #httpGetPost').find('.form-control').val('');
		   	    });
				$.ajax({
					type: "POST",
					url: "/insertionorder/getAjaxResponse",
					data: 'fl_delivery_id=' + currentValue,
					success: function (data) {
                        var obj = $.parseJSON(data);
                        if(obj.sucess){
							var ds_delivery_mode = obj.data.tblDeliverySetting.ds_delivery_mode;
							console.log(ds_delivery_mode);
							switch(ds_delivery_mode){
							    case '1':
							        $('#deliverySettings #tblDeliverySettingDeliveryMethod1').prop('checked', true).parent().addClass('checked');
							        $('#deliverySettings #realTimeDelivery').show();
							        $('#deliverySettings #batchPost').hide();
							        $('#deliverySettings #httpGetPost').hide();
							        $('#deliverySettings #ftpUpload').hide();
							        $('#deliverySettings #tblDeliverySettingDeliveryMethod1').val(obj.data.tblDeliverySetting.ds_delivery_mode);
							        $('#deliverySettings #tblDeliverySettingDsRealtimeEmail').val(obj.data.tblDeliverySetting.ds_realtime_email);
							        break;
							    case '2':
							        $('#deliverySettings #tblDeliverySettingDeliveryMethod2').prop('checked', true).parent().addClass('checked');
							        $('#deliverySettings #batchPost').show();
							        $('#deliverySettings #httpGetPost').hide();
							        $('#deliverySettings #realTimeDelivery').hide();
							        $('#deliverySettings #ftpUpload').hide();
							        $('#deliverySettings #tblDeliverySettingDeliveryMethod2').val(obj.data.tblDeliverySetting.ds_delivery_mode);
							        $('#deliverySettings #tblDeliverySettingDsBatchEmail').val(obj.data.tblDeliverySetting.ds_batch_email);
							        $('#deliverySettings #tblDeliverySettingDsBatchSubject').val(obj.data.tblDeliverySetting.ds_batch_subject);
							        $('#deliverySettings #tblDeliverySettingDsBatchFrequency').val(obj.data.tblDeliverySetting.ds_batch_frequency);
							        break;
							    case '3':
							        $('#deliverySettings #tblDeliverySettingDeliveryMethod3').prop('checked', true).parent().addClass('checked');
							        $('#deliverySettings #ftpUpload').show();
							        $('#deliverySettings #httpGetPost').hide();
							        $('#deliverySettings #batchPost').hide();
							        $('#deliverySettings #realTimeDelivery').hide();
							        $('#deliverySettings #tblDeliverySettingDeliveryMethod3').val(obj.data.tblDeliverySetting.ds_delivery_mode);
							        $('#deliverySettings #tblDeliverySettingDsFtpUrl').val(obj.data.tblDeliverySetting.ds_ftp_url);
							        $('#deliverySettings #tblDeliverySettingDsFtpUsername').val(obj.data.tblDeliverySetting.ds_ftp_username);
							        $('#deliverySettings #tblDeliverySettingDsFtpPassword').val(obj.data.tblDeliverySetting.ds_ftp_password);
							        $('#deliverySettings #tblDeliverySettingDsFtpFolder').val(obj.data.tblDeliverySetting.ds_ftp_folder);
							        break;
							    case '4':
							        $('#deliverySettings #tblDeliverySettingDeliveryMethod4').prop('checked', true).parent().addClass('checked');
							        $('#deliverySettings #httpGetPost').show(); 
							        $('#deliverySettings #ftpUpload').hide();
							        $('#deliverySettings #batchPost').hide();
							        $('#deliverySettings #realTimeDelivery').hide();
							        $('#deliverySettings #tblDeliverySettingDeliveryMethod4').val(obj.data.tblDeliverySetting.ds_delivery_mode);
							        $('#deliverySettings #tblDeliverySettingDsLogin').val(obj.data.tblDeliverySetting.ds_login);
							        $('#deliverySettings #tblDeliverySettingDsPassword').val(obj.data.tblDeliverySetting.ds_password);
							        $('#deliverySettings #tblDeliverySettingDsHttpUrl').val(obj.data.tblDeliverySetting.ds_http_url);
							        $('#deliverySettings #tblDeliverySettingDsHttpGetPost').val(obj.data.tblDeliverySetting.ds_http_get_post);
							        $('#deliverySettings #tblDeliverySettingDsHttpResponse').val(obj.data.tblDeliverySetting.ds_http_response);
							        break;
							    default: 
							        $('#deliverySettings #tblDeliverySettingDeliveryMethod4').prop('checked', true).parent().addClass('checked');
							        $('#deliverySettings #httpGetPost').show(); 
							        $('#deliverySettings #ftpUpload').hide();
							        $('#deliverySettings #batchPost').hide();
							        $('#deliverySettings #realTimeDelivery').hide();
							        $('#deliverySettings #tblDeliverySettingDeliveryMethod4').val(obj.data.tblDeliverySetting.ds_delivery_mode);
							        $('#deliverySettings #tblDeliverySettingDsLogin').val(obj.data.tblDeliverySetting.ds_login);
							        $('#deliverySettings #tblDeliverySettingDsPassword').val(obj.data.tblDeliverySetting.ds_password);
							        $('#deliverySettings #tblDeliverySettingDsHttpUrl').val(obj.data.tblDeliverySetting.ds_http_url);
							        $('#deliverySettings #tblDeliverySettingDsHttpGetPost').val(obj.data.tblDeliverySetting.ds_http_get_post);
							        $('#deliverySettings #tblDeliverySettingDsHttpResponse').val(obj.data.tblDeliverySetting.ds_http_response);
							}
						} else {
							$('#deliverySettings #tblDeliverySettingDeliveryMethod4').prop('checked', true).parent().addClass('checked');
					        $('#deliverySettings #httpGetPost').show(); 
					        $('#deliverySettings #ftpUpload').hide();
					        $('#deliverySettings #batchPost').hide();
					        $('#deliverySettings #realTimeDelivery').hide();
					        $('#deliverySettings #tblDeliverySettingDeliveryMethod4').val(obj.data.tblDeliverySetting.ds_delivery_mode);
					        $('#deliverySettings #tblDeliverySettingDsLogin').val(obj.data.tblDeliverySetting.ds_login);
					        $('#deliverySettings #tblDeliverySettingDsPassword').val(obj.data.tblDeliverySetting.ds_password);
					        $('#deliverySettings #tblDeliverySettingDsHttpUrl').val(obj.data.tblDeliverySetting.ds_http_url);
					        $('#deliverySettings #tblDeliverySettingDsHttpGetPost').val(obj.data.tblDeliverySetting.ds_http_get_post);
					        $('#deliverySettings #tblDeliverySettingDsHttpResponse').val(obj.data.tblDeliverySetting.ds_http_response);
						}
					}
				});	
			}
	   });
	}
    
    function calculate() {
        var e = document.getElementById("costStructure");
        if (e.selectedIndex != -1) {
            var strUser = e.options[e.selectedIndex].value;
            if (strUser != 5)
            {
                var rate = document.getElementById("txtRate").value;
                var txtQuantity = document.getElementById("txtQuantity").value;
                if (rate.replace('$', '').replace(' ', '') != '' && txtQuantity.replace(',', '') != '')
                {
                    document.getElementById("txtCalculate").value = parseFloat(rate.replace('$', '').replace(' ', '')) * parseInt(txtQuantity.replace(',', ''));
                }
            } else {
                var rate = document.getElementById("txtRate").value;
                var txtQuantity = document.getElementById("txtQuantity").value;
                if (rate.replace('$', '').replace(' ', '') != '' && txtQuantity.replace(',', '') != '')
                {
                    document.getElementById("txtCalculate").value = parseFloat(rate.replace('$', '').replace(' ', '')) * (parseInt(txtQuantity.replace(',', '')) / 1000);
                }
            }
        } else {
            document.getElementById("txtCalculate").value = 0;
        }
    }


    function adunit_delete(id, text) {
        fl_id = $('#tblFlightFlId').val();
        li_id = $('#tblLineItemLiId').val();
        $.get("<?php echo configure::read('Path.siteurl'); ?>insertionorder/deleteMappedAddunit/" + id + '/' + li_id + '/' + fl_id, function (ma_id) {
            $('#adunitcontent tr.' + id).remove();
        });
    }


    function adunit_apend(id, textlabal) {
        $('#changeAdUnitDiv').show();
        fl_id = $('#tblFlightFlId').val();
        li_id = $('#tblLineItemLiId').val();
        $.get("<?php echo configure::read('Path.siteurl'); ?>insertionorder/getCompanyRevShare/" + id, function (data) {
            var rev_share = data;
            $.get("<?php echo configure::read('Path.siteurl'); ?>insertionorder/addMappedAddunit/" + id + '/' + li_id + '/' + fl_id + '/' + rev_share, function (ma_id) {
                $('#adunitcontent tr:last').after('<tr class="ManageLead ' + id + '"><td>' + textlabal + '</td> <td><input name="dailyAllocation[' + id + ']" data-inputmask="\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true" placeholder="Daily Allocation"   class="form-control allocation onlynumber"><br><input name="monthlyAllocation[' + id + ']" data-inputmask="\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true" placeholder="Monthly Allocation"  class="form-control allocation onlynumber"><br><input name="allocation[' + id + ']" data-inputmask="\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true" placeholder="Total Allocation"  class="form-control allocation onlynumber"></td><td><input type="number" data-inputmask="\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true, \'placeholder\': \'0\'"  max=100 min=0  name="rev_share[' + id + ']" value="' + rev_share + '" class="form-control onlynumber"></td></tr>');
            });
        });
//        var rev_share = httpGet("<?php echo configure::read('Path.siteurl'); ?>insertionorder/getCompanyRevShare/" + id).success(data, function () {
//            httpGet("<?php echo configure::read('Path.siteurl'); ?>insertionorder/addMappedAddunit/" + id + '/' + li_id + '/' + fl_id).success(ma_id, function () {
//                $('#adunitcontent tr:last').after('<tr class="ManageLead ' + id + '"><td>' + textlabal + '</td> <td><input name="allocation[' + id + ']" data-inputmask="\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true, \'placeholder\': \'0\'"  onkeyup="calculateAllocation()" class="form-control allocation onlynumber"></td><td><input type="number" data-inputmask="\'alias\': \'numeric\', \'groupSeparator\': \',\', \'autoGroup\': true, \'placeholder\': \'0\'"  max=100 min=0  name="rev_share[' + id + ']" value="' + rev_share + '" class="form-control onlynumber"></td></tr>');
//            });
//        });

    }


    function calculateAllocation() {
        var total = 0;
        var availble_allocation = parseInt($(".current_allocation").html());
        $('input.allocation').each(function () {
            var num = parseInt(this.value, 10);

            if (!isNaN(num)) {
                $('.jsalertmsg').html('');
                total += num;
                if (availble_allocation < total) {
                    $('.jsalertmsg').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>No more Allocation left to assign</div>');
                    window.scrollTo(0, 0);
                    $(this).val('');
                    total -= num;
                }
            }
        });
        var free_allocation = availble_allocation - total;
        $(".available_allocation").html(free_allocation);
    }
    
    function httpGet(theUrl)
    {
        var xmlHttp = null;
        xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", theUrl, false);
        xmlHttp.send(null);
        return xmlHttp.responseText;
    }
    
    function delete_zip_csv(flight_id) {
        $.ajax({
            type: "POST",
            url: "/insertionorder/deleteZipCSV",
            data: 'fl_id=' + flight_id,
            success: function (data) {
                $('#existing_zips').remove();
                $('#view_existing_zips').remove();
                $('#tblFlightZipTargeting').remove();
            }
        });
    }
    
    function checkZipcodeAudience() {
        if ($("#zipcodecsv_input").is(':checked')) {
            $("#zipcodeCSV").show();  // checked
        } else {
            $("#zipcodeCSV").hide();  // unchecked
        }
    }

</script>					
