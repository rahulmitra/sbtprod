<?php $this->Html->addCrumb('Advertisers', $this->Html->url(null, true)); ?>
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Line Items
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <?php echo $this->Form->create(false, array('url' => array('controller' => 'insertionorder', 'action' => 'edit_line_item'), 'class' => 'form-horizontal', 'name' => 'submit_form_line_item_flight', 'id' => 'submit_form_line_item_flight')); ?>
                <?php echo $this->Form->hidden('tblLineItem.status'); ?>  

                <div class="form-body">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        You have some form errors. Please check below.
                    </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        Your form validation is successful!
                    </div>
                    <h3 class="form-section">Line Item Details</h3>
                    <div class="form-group">
                        <label class="control-label col-md-2">Select Order <span class="required">
                                * </span></label>
                        <div class="col-md-5">
                            <div class="input-icon right">
                                <i class="fa"></i>

                                <?php
                                $options = array();
                                if (!empty($orders)) {
                                    foreach ($orders as $key => $value) {
                                        $options[$value['tblOrder']['dfp_order_id']] = $value['tblOrder']['order_name'] . " - " . $value['tblp']['CompanyName'];
                                    }
                                }

                                echo $this->Form->input('tblLineItem.li_order_id', array(
                                    'class' => 'js-example-data-array js-states form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'options' => $options
                                        )
                                );
                                ?>


                            </div>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Line Item Name <span class="required">
                                * </span></label>
                        <div class="col-md-5">
                            <div class="input-icon right">
                                <i class="fa"></i>


                                <?php echo $this->Form->input('tblLineItem.li_name', array('class' => 'form-control', 'Placeholder' => 'Enter Line Item Name Here', 'div' => false, 'label' => false)) ?>
                                <span class="help-block">
                                    Line Items are the individual tactics within an order.  (For co-registration, a line-item is the actual offer name and is not tied to a specific publisher ) </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <label class="control-label col-md-2">Campaign Name</label>
                        <div class="col-md-5">

                            <?php echo $this->Form->input('tblLineItem.camp_name', array('class' => 'form-control', 'Placeholder' => 'Enter Campaign Name Here', 'div' => false, 'label' => false)) ?>
                            <span class="help-block">Many times, an advertiser issues multiple orders for different tactics for a single campaign.  This field ties them together.  Please add a new campaign name or select from a pre-existing set of campaigns for this Company.</span>
                        </div>
                    </div> 
                    <div class="form-group"> 
                        <label class="control-label col-md-2">Select Category</label>
                        <div class="col-md-5">
                            <?php
                            echo $this->Form->input('tblLineItem.li_category_id', array(
                                'class' => 'js-example-data-array-categories js-states form-control',
                                'label' => false,
                                'type' => 'select',
                                'onchange' => 'ShowSubCategory(this.value)',
                                'empty' => 'Select (or start typing) the category for the line item',
                                'options' => $categorylist
                                    )
                            );
                            ?>
                            <div class="col-md-10" style="position:absolute; left:100%; top: 0;"><span class="help-block" style="margin: 0;">We use standard IAB taxonomy to define categories of specific line items.  This helps our system optimize and run reporting with user data.  If you do not find the specific categories for your campaigns listed below, please add the closest categories, and include your categories in the NOTES section below.</span>
                            </div>
                        </div>

                    </div>
                    <div class="form-group"> 
                        <label class="control-label col-md-2">Select Sub Category</label>
                        <div class="col-md-5">
                            <?php
                            echo $this->Form->input('tblLineItem.li_subcategory_id', array(
                                'class' => 'js-example-data-array-categories js-example-data-array-subcategories js-states form-control',
                                'id' => 'subscategory_content',
                                'label' => false,
                                'type' => 'select',
                                'options' => $subcategory,
                                'empty' => 'Select (or start typing) the sub category for the line item',
                                    )
                            );
                            ?>

                        </div>
                    </div>
                    <div class="form-group"> 
                        <label class="control-label col-md-2">Notes</label>
                        <div class="col-md-5">

                            <?php echo $this->Form->input('tblLineItem.camp_name', array('class' => 'form-control', 'Placeholder' => 'Mention your notes here', 'div' => false, 'rows' => 3, 'label' => false)) ?>

                        </div>
                    </div> 
                    <h3 class="form-section">Pricing & Quantity</h3>
                    <div class="form-group">
                        <label class="control-label col-md-2">Rate Type / Cost Structure <span class="required">
                                * </span>	</label>
                        <div class="col-md-5"> 
                            <select multiple="multiple" onChange="checkMoreInfo(this.value);" name="data[tblLineItem][li_cs_id]" id="costStructure" class="form-control">
                                <?php foreach ($costStructure as $options) { ?>
                                    <option value='<?php echo $options['tblCostStructure']['cs_id']; ?>' <?php echo $this->data['tblLineItem']['li_cs_id'] == $options['tblCostStructure']['cs_id'] ? "selected" : ""; ?>><?php echo $options['tblCostStructure']['cs_name']; ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6">Quantity <span class="required">
                                        * </span></label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Enter Quantity" value="<?php echo $this->data['tblLineItem']['li_quantity']; ?>" name="data[tblLineItem][li_quantity]" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'placeholder': '0'" id="txtQuantity" onkeyup="calculate()" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label col-md-3">Rate <span class="required">
                                        * </span></label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Enter Rate" name="data[tblLineItem][li_rate]" value="<?php echo $this->data['tblLineItem']['li_rate']; ?>" id="txtRate" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" ,style="text-align: right;" ,onkeyup="calculate()", class="form-control" onkeyup="calculate()" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-2">Total Cost</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Auto-Calculate" name="data[tblLineItem][li_total]" value="<?php echo $this->data['tblLineItem']['li_total']; ?>" id="txtCalculate" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"  onkeyup="calculate()" class="form-control">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Daily</label>
                                    <div class="col-md-5">
                                        <input type="text" value="<?php echo $this->data['tblLineItem']['li_daily_cap']; ?>"   placeholder="Daily Limit" name="data[tblLineItem][li_daily]" data-inputmask="'alias': 'numeric',  'autoGroup': true,  'placeholder': '0'" id="txtDaily" onkeyup="calculate()" style="text-align: right;" class="form-control quntity_validation">
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Weekly </label>
                                    <div class="col-md-7">
                                        <input type="text" value="<?php //echo $this->data['tblLineItem']['li_weekly_cap']; ?>"  placeholder="Weekly Limit" name="data[tblLineItem][li_weekly]" id="txtweekly" data-inputmask="'alias': 'numeric',  'autoGroup': true,   'placeholder': '0'" onkeyup="calculate()" style="text-align: right;" class="form-control quntity_validation">
                                    </div>
                                </div>
                            </div>-->
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Monthly</label>
                                    <div class="col-md-4">
                                        <input type="text"  value="<?php echo $this->data['tblLineItem']['li_monthly_cap']; ?>"  placeholder="Monthly Limit" name="data[tblLineItem][li_monthly]"  id="txtMonthly" data-inputmask="'alias': 'numeric',  'autoGroup': true, 'placeholder': '0'" onkeyup="calculate()" style="text-align: right;" class="form-control quntity_validation">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





                    <h3 class="form-section" style ="padding:10px;">Flight Dates & Renewal</h3>
                    <div class="row date date-picker event_period" id="" style ="padding:10px;" data-date-start-date="+0d">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6">Start Date</label>
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <input type="text" name="data[tblLineItem][li_start_date]" value="<?php echo $this->request->data['tblLineItem']['li_start_date']; ?>" id="dt1" placeholder="Select Date" class="form-control actual_range">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label col-md-4">End Date</label>
                                <div class="col-md-7">
                                    <div class="input-group">
                                        <input id="dt2" name="data[tblLineItem][li_end_date]" value="<?php echo $this->request->data['tblLineItem']['li_end_date']; ?>" placeholder="Select Date" type="text" class="form-control actual_range">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-2">OR</label>
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <div class="icheck-list">
                                            <label>
                                                <input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" name="data[tblLineItem][li_run_till_end]" <?php echo $this->request->data['tblLineItem']['li_run_till_end'] ? "checked" : "" ?>> Run till end of allocation </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <div class="icheck-list">
                                            <label>
                                                <input type="checkbox" <?php echo $this->data['tblLineItem']['li_auto_renew'] ? "checked" : "" ?> name="data[tblLineItem][li_auto_renew]"  class="icheck" data-checkbox="icheckbox_square-grey"> Auto Renew Line Item </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">

                                <label class="control-label col-md-3">Renewal Frequency</label>
                                <div class="col-md-7">
                                    <select multiple="multiple" name="data[tblLineItem][li_renewal_frequency]" id="renFrequency" class="form-control">
                                        <option value='1' <?php echo ($this->request->data['tblLineItem']['li_renewal_frequency'] == 1) ? "selected" : ""; ?> >Every Calendar Month</option>
                                        <option value='2' <?php echo ($this->request->data['tblLineItem']['li_renewal_frequency'] == 2) ? "selected" : ""; ?>>Everytime Allocation Expires</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-1"></label>
                                <div class="col-md-5">
                                    <div class="checkbox">
                                        <div class="icheck-list" >

                                            <label for="ScrubAcceptance" >
                                                <?php echo $this->Form->checkbox('tblLineItem.li_is_scrub', array('class' => 'icheck', 'select' => 'true', 'id' => 'ScrubAcceptance', 'label' => true, 'data-checkbox' => 'icheckbox_square-grey', 'label' => false)); ?>  Post Scrub Acceptance. If Yes, enter maximum permissible scrub rate</label>											 



                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-1" style="float:left;">
                                    <?php
                                    echo $this->Form->input('tblLineItem.li_scrub_rate', array('placeholder' => 'in %'
                                        , 'id' => 'txtScrubRate', 'onkeyup' => 'calculate()', 'class' => 'form-control', 'data-inputmask' => "'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'", 'style' => 'text-align: right;', 'label' => false));
                                    ?> 

                                </div>
                            </div>
                        </div>
                    </div>



                    <div id="ScrubAcceptanceDiv" style="padding:10px;<?php echo (!empty($this->request->data['tblLineItem']['li_is_scrub'])) ? 'block' : 'none'; ?>;">
                        <h3 class="form-section">Adjusted Pricing & Quantity</h3>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Quantity <span class="required">
                                            * </span></label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Enter Quantity"  readonly value=""   data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'placeholder': '0'"   id="calquantity" style="text-align: right;" onkeyup="calculate()"  class="form-control" onkeyup="calculate()">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Rate <span class="required">
                                            * </span></label>
                                    <div class="col-md-7">
                                        <input type="text" placeholder="Enter Rate" readonly value="" id="calliRate" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" style="text-align: right;"  class="form-control" onkeyup="calculate()">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Total Cost</label>
                                    <div class="col-md-4">
                                        <input type="text" readonly placeholder="Auto-Calculate" id="caltxtCalculate" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"  class="form-control" style="text-align: right;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label col-md-6">Daily</label>
                                    <div class="col-md-5">
                                        <input type="text" placeholder="Daily Limit" readonly  data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true,  'placeholder': '0'" id="caltxtDaily" style="text-align: right;" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Weekly </label>
                                    <div class="col-md-7">
                                        <input type="text" placeholder="Weekly Limit" readonly id="caltxtweekly" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true,   'placeholder': '0'" style="text-align: right;" class="form-control">
                                    </div>
                                </div>
                            </div>-->
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Monthly</label>
                                    <div class="col-md-4">
                                        <input type="text" placeholder="Monthly Limit" readonly id="caltxtMonthly" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'placeholder': '0'" style="text-align: right;" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div style="padding:10px;">
                        <h3 class="form-section">Day Parting Setting:</h3>
                        <div class="row" >
                            <div class="col-xs-4" style="text-align: left;">
                                <div class="form-group" style="text-align: left;">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-md-5">
                                        <div class="checkbox">
                                            <div class="icheck-list">
                                                <label for="enable_parting" >
                                                    <?php echo $this->Form->checkbox('tblLineItem.enable_parting', array('class' => 'icheck', 'select' => 'true', 'id' => 'enable_parting', 'label' => true, 'data-checkbox' => 'icheckbox_square-grey', 'label' => false)); ?>  Enable Day Parting</label>

                                            </div>
                                        </div>
                                    </div>		


                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Select Time zone</label>
                                    <div class="input-icon right col-md-6">
                                        <?php
                                        echo $this->Form->input('tblLineItem.timezone', array(
                                            'class' => 'js-example-data-array js-states form-control',
                                            'label' => false,
                                            'type' => 'select',
                                            'options' => $timezones
                                                )
                                        );
                                        ?>
                                    </div> 
                                </div>
                            </div>
                            <div id="enable-day-parting-display" style="margin:0px 5px;display: <?php echo (!empty($this->request->data['tblLineItem']['enable_parting'])) ? 'block' : 'none'; ?>">
                                <?php $weekdays = array('2' => 'Monday', '3' => 'Tuesday', '4' => 'Wednesday', '5' => 'Thusday', '6' => 'Friday', '7' => 'Saturday', '1' => 'Sunday'); ?>
                                <?php foreach ($weekdays as $daykey => $day) { ?>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-1"></label>
                                            <div class="col-md-9">										 

                                                <label for="mon" class="col-lg-2">
                                                    <?php echo $this->Form->checkbox('LineItemDepart.week_day.' . $daykey, array('class' => 'icheck', 'select' => 'true', 'id' => $day, 'hiddenField' => false, 'data-checkbox' => 'icheckbox_square-grey', 'label' => false, 'checked' => (!empty($LineItemDeparts[$daykey])) ? true : false)) ?> <?php echo $day; ?> </label>
                                                <div class="col-lg-3">
                                                    <?php echo $this->Form->input('LineItemDepart.start_time.' . $daykey . '.', array('class' => 'timepicker', 'label' => false, 'div' => false, 'placeholder' => 'Start Time', 'value' => (!empty($LineItemDeparts[$daykey][0]['start_time'])) ? $LineItemDeparts[$daykey][0]['start_time'] : '')); ?>
                                                </div>
                                                <div class="col-lg-3">
                                                    <?php echo $this->Form->input('LineItemDepart.end_time.' . $daykey . '.', array('class' => 'timepicker', 'label' => false, 'div' => false, 'placeholder' => 'End Time', 'value' => (!empty($LineItemDeparts[$daykey][0]['end_time'])) ? $LineItemDeparts[$daykey][0]['end_time'] : '')) ?>  </div>
                                                <div class="col-lg-3">
                                                    <button id="b2" class="btn add-more-day" dataday="<?php echo $daykey; ?>" type="button">+</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="addmoreday<?php echo $daykey ?>"></div>	
                                <?php } ?>  	
                            </div>					 
                        </div>
                    </div>
                    <h3  style="display:none;padding:10px;" id="formfeildsh" class="form-section" >Form Fields to be Collected</h3>
                    <div  style="display:none;padding:10px;" id="formfeilds" class="row" >
	                    <div style="padding: 0 1px 6px 17px;">
		                  <input type="checkbox" name="data[tblLineItem][chekc_all]" id="chekc_all" value="<?php echo isset($prechecked) && !empty($prechecked) ? '1' : '0' ?>" <?php echo isset($prechecked) && !empty($prechecked) ? 'checked' : '' ?> />
		                  <label>Display This Offer as Pre-Checked</label>
		                </div>
                        <div class="col-md-12">
                            <table class="table table-hover table-bordered table-striped" id= "addmapfield">
                                <thead>
                                    <tr>
                                        <th width="10%">Enabled</th>
                                        <th width="16%">Field Description</th>
                                        <th width="20%">Field Type</th>
                                        <th width="20%">Value</th>
                                        <th width="25%">Field Label</th>
                                        <th width="10%" align="center">Required</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    // these configure form app/config/config.php
                                    $addfields = configure::read('CollectedCollect');
                                    $totalfields = count($addfields) - 1;
                                    if (!empty($addfields)) {
                                        $counter = 0;
                                        foreach ($addfields as $key => $value) {
                                            $fieldValue = arrayfind($lineItemFields, 'fld_label', $value['label']);
                                            $selectType = (!empty($fieldValue['fld_type'])) ? $fieldValue['fld_type'] : '';
                                            ?>
                                            <tr>

                                                <td>
                                                    <?php echo $this->Form->input('LineItemField.status.' . $counter, array('type' => 'checkbox', 'id' => 'status' . $counter, 'label' => false, 'hiddenField' => false, (!empty($fieldValue)) ? 'checked' : '', 'div' => false, 'class' => false, 'after' => __('<label for="status' . $counter . '">Yes</label>'))); ?>

                                                </td>
                                                <td>
                                                    <?php
                                                    /// this is used for identify custom field
                                                    echo $this->Form->hidden('LineItemField.custom.' . $counter, array('value' => 0, 'div' => false, 'label' => false));
                                                    echo
                                                    $this->Form->hidden('LineItemField.fld_label.' . $counter, array('class' => 'form-control demo auto-w', 'value' => $value['label'], 'div' => false, 'label' => false));
                                                    ?>

                                                    <?php echo $value['label']; ?> <?php echo $this->Form->hidden('LineItemField.fld_name.' . $counter, array('class' => 'form-control demo auto-w', 'value' => $key, 'div' => false, 'label' => false)); ?>
                                                </td>


                                                <td><?php
                                                    $CollectFieldType = configure::read('CollectFieldType');
                                                    ?>
                                                    <?php
                                                    /// this is used for identify custom field
                                                    if (in_array($key, array("phone", "dob"))) {
                                                        echo $this->Form->input('LineItemField.field_format.' . $counter, array(
                                                            'label' => false,
                                                            'value' => (!empty($fieldValue)) ? $fieldValue['field_format'] : '',
                                                            'placeholder' => $value['placeholder'],
                                                            'class' => 'form-control'));
                                                    } else {
                                                        echo (!empty($CollectFieldType[$value['type']])) ? $CollectFieldType[$value['type']] : 'N/A';
                                                    }
                                                    ?>
                                                    <div style="display:none">
                                                        <?php
                                                        echo $this->Form->input('LineItemField.fld_type.' . $counter, array(
                                                            'label' => false,
                                                            'type' => 'hidden',
                                                            'value' => $value['type']
                                                                //'class' => 'js-creative-field-type-array form-control',
                                                                //'selected'=>$selectType,
                                                                //'onchange'=>'ShowFieldType(this.value,'.$counter.')',
                                                                //'options' => configure::read('CollectFieldType')
                                                        ));
                                                        ?>
                                                    </div>
                                                    <div id="ShowDatetype<?php echo $counter ?>"></div></td>
                                                <td> 
                                                    <?php
                                                    /* if(in_array($key,array("phone"))){
                                                      echo $this->Form->input('LineItemField.fld_custom_texts.'.$counter,array(
                                                      'label' => false,
                                                      'value'=>(!empty($fieldValue))?$fieldValue['fld_custom_texts']:'',
                                                      'placeholder' => (!empty($value['placeholder']))?$value['placeholder']:'',
                                                      'class' => 'form-control'));

                                                      }
                                                      else */ if (in_array($key, array("gender"))) {
                                                        echo $this->Form->input('LineItemField.fld_custom_texts.' . $counter, array(
                                                            'label' => false,
                                                            'value' => (!empty($fieldValue)) ? $fieldValue['fld_custom_texts'] : '',
                                                            'placeholder' => 'Put your disclaimer here',
                                                            'class' => 'form-control'));
                                                    } else if (in_array($key, array("dob"))) {
                                                        echo $this->Form->input('LineItemField.fld_custom_texts.' . $counter, array(
                                                            'label' => false,
                                                            'type' => 'select',
                                                            'onchange' => 'ShowDatePlaceholder(this.value,' . $counter . ')',
                                                            'default' => (!empty($fieldValue)) ? $fieldValue['fld_custom_texts'] : '',
                                                            'options' => (!empty($value['format'])) ? $value['format'] : array(),
                                                            'class' => 'form-control dobformat'));
                                                    }
                                                    ?>
                                                    <div id="showFieldTypeValue<?php echo $counter ?>">
                                                     <?php
                                                      if(count($fieldValue)>0):
                                                        if ($fieldValue['custom'] == 1) {
                                                            if (in_array($selectType, array(2, 3))) {
                                                                echo $this->Form->input('LineItemField.fld_custom_texts.' . $counter, array('label' => false, 'value' => (!empty($fieldValue)) ? $fieldValue['fld_custom_texts'] : '', 'div' => false, 'class' => 'form-control demo auto-w'
                                                                    , 'placeholder' => 'Put values as comma seprated'));
                                                            }
                                                        }
                                                      endif;
                                                     ?>
                                                    </div></td>
                                                <td>
                                                    <?php
                                                    echo $this->Form->input('LineItemField.adv_fld_name.' . $counter, array('label' => false, 'value' => (!empty($fieldValue)) ? $fieldValue['adv_fld_name'] : '', 'div' => false, 'class' => 'form-control demo auto-w'));
                                                    ?>

                                                </td>
                                                <td>
                                                    <?php echo $this->Form->input('LineItemField.required.' . $counter, array('type' => 'checkbox', 'id' => 'status' . $counter, 'label' => false, 'hiddenField' => false, (!empty($fieldValue['required'])) ? 'checked' : '', 'div' => false, 'class' => false)); ?>

                                                </td>
                                            </tr>
                                            <?php
                                            $counter++; // increament of counter variable 
                                        } // end of foreach of addfileds
                                    } // end of if condition of addfileds
                                    ?>  <tr style="background-color: #f5f5f5;"><td colspan="6"><button id="b1" class="btn add-field-more" type="button">+</button>&nbsp;&nbsp;&nbsp;</td></tr> 
                            </table> 
                        </div>

                    </div>
                    <h3  style="display:none;padding:10px;" id="leadsh" class="form-section" >Lead Validation</h3>
                    <div  id="leads" style="display:none;padding:10px;" class="row" >

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-11">
                                    <div class="input-group">
                                        <div class="icheck-list">

                                            <label>
                                                <?php
                                                $fieldValue = arrayfindcheck($lineItemFields, 'fld_name', "chk3rdpartyVal");

                                                echo $this->Form->input('LineItemField.productTypes.', array(
                                                    'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => '3rd Party Email Address Validation @| chk3rdpartyVal @| Lead Validation @|',
                                                    'id' => 'chk3rdpartyVal', ($fieldValue) ? 'checked' : '', 'data-label' => '3rd Party Email Address Validation', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                                                ?>
                                            </label>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="icheck-list">

                                            <label>
                                                <?php
                                                $fieldValue = arrayfindcheck($lineItemFields, 'fld_name', "chk3rdpartyEmail");
                                                echo $this->Form->input('LineItemField.productTypes.', array(
                                                    'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Syntax Validation of Email Address @| chk3rdpartyEmail @| Lead Validation @|',
                                                    'id' => 'chk3rdpartyVal', 'data-label' => 'Syntax Validation of Email Address', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()', ($fieldValue) ? 'checked' : ''));
                                                ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="icheck-list">

                                            <label>
                                                <?php
                                                $fieldValue = arrayfindcheck($lineItemFields, 'fld_name', "3rdPartyPhoneVal");
                                                echo $this->Form->input('LineItemField.productTypes.', array(
                                                    'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Phone Number to Address Match @| 3rdPartyPhoneVal @| Lead Validation @|',
                                                    'id' => 'chk3rdpartyVal', 'data-label' => 'Phone Number to Address Match', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()', ($fieldValue) ? 'checked' : ''));
                                                ?>
                                            </label>	
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="icheck-list">

                                            <label>
                                                <?php
                                                $fieldValue = arrayfindcheck($lineItemFields, 'fld_name', "3rdPartyPostalMatch");

                                                echo $this->Form->input('LineItemField.productTypes.', array(
                                                    'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Postal Name Address Match @| 3rdPartyPostalMatch @| Lead Validation @|',
                                                    'id' => 'chk3rdpartyVal', 'data-label' => 'Postal Name Address Match', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()', ($fieldValue) ? 'checked' : ''));
                                                ?>
                                            </label>	
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3  id="autoh" style="display:none;padding:10px;" class="form-section"  >Auto Responder Setting</h3>
                    <div id="auto" style="display:none;padding:10px;" class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-1"></label>
                                <div class="col-md-5">
                                    <div class="checkbox">
                                        <div class="icheck-list" >

                                            <label for="AutoResponderIsAutoresp" >
                                                <?php echo $this->Form->checkbox('AutoResponder.is_autoresp', array('class' => 'icheck', 'select' => 'true', 'label' => true, 'data-checkbox' => 'icheckbox_square-grey', 'label' => false)); ?>  Enable Auto Responder</label>											 

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div id="AutoResponder" style="display:<?php echo (!empty($this->request->data['AutoResponder']['is_autoresp'])) ? 'block' : 'none'; ?>;" class="row">
                            <div class="col-md-12">
                                <div class="form-group"> 
                                    <label class="control-label col-md-2">From Name</label>
                                    <div class="col-md-5">
                                        <?php echo $this->Form->input('AutoResponder.from_name', array('placeholder' => 'Enter From Name', 'value' => $this->request->data['AutoResponder']['from_name'], 'label' => false, 'class' => 'form-control')); ?> 
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-12">
                                <div class="form-group"> 
                                    <label class="control-label col-md-2">From Email</label>
                                    <div class="col-md-5">
                                        <?php echo $this->Form->input('AutoResponder.from_email', array('placeholder' => 'Enter From Email', 'value' => $this->request->data['AutoResponder']['from_email'], 'label' => false, 'class' => 'form-control')); ?> 
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-12">
                                <div class="form-group"> 
                                    <label class="control-label col-md-2">Subject Line</label>
                                    <div class="col-md-5">
                                        <?php echo $this->Form->textarea('AutoResponder.subjectline', array('placeholder' => 'Enter Subject Line Here', 'value' => $this->request->data['AutoResponder']['subjectline'], 'label' => false, 'class' => 'form-control')); ?> 

                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-12">
                                <div class="form-group"> 
                                    <label class="control-label col-md-2">Email Body</label>
                                    <div class="col-md-8">
                                        <?php echo $this->Form->textarea('AutoResponder.emailbody', array('placeholder' => 'Enter email Body Here', 'cols' => '80', 'rows' => '10', 'label' => false, 'value' => $this->request->data['AutoResponder']['subjectline'])); ?> 
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-12">
                                <div class="form-group"> 
                                    <label class="control-label col-md-2">Footer</label>
                                    <div class="col-md-5">
                                        <?php echo $this->Form->textarea('AutoResponder.footer', array('label' => false, 'class' => 'form-control')); ?> 
                                    </div>
                                </div> 
                            </div>

                            <div class="col-md-12">
                                <div class="form-group"> 
                                    <label class="control-label col-md-2"></label>
                                    <div class="col-md-8">

                                        <?php echo $this->Form->checkbox('AutoResponder.chkARVal', array('class' => 'icheck', 'label' => true, 'id' => 'chkARVal', 'data-checkbox' => 'icheckbox_square-grey', 'label' => false)); ?>   		 

                                        <label for="chkARVal"> Enable Auto Responder Validation of Email Address</label>
                                        <span class="help-block">Please note that enabling the auto-responder validation will delay the lead submission to your client by up to 10 hours.  Our system will send out the auto-responder and will listen for all bounces, spam complaints and un-subscribe requests during this period, after which, the lead will be classified as valid or rejected based on user action.  
                                        </span>
                                    </div>
                                </div> 
                            </div>
                        </div>

                    </div>

                    <h3 class="form-section" style="display:none;padding:10px;">Media Type</h3>
                    <div class="row" style="display:none;padding:10px;">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-8">
                                    <label class="control-label"><b>DESKTOP</b></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <label class="control-label"><b>EMAIL </b></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <label class="control-label"><b>LEAD-GEN</b></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <label class="control-label"><b>SOCIAL</b></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <label class="control-label"><b>MOBILE</b></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row"  style="display:none;padding:10px;">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <div class="icheck-list">
                                            <?php foreach ($desktop as $options) { ?>
                                                <label>
                                                    <input type="checkbox" <?php echo in_array($options['tblProducttype']['ptype_id'], $allowedMediaTypes) ? "checked" : "" ?> name="productTypes[]" id="adunits" class="icheck" data-checkbox="icheckbox_line-grey" data-label="<?php echo $options['tblProducttype']['ptype_name']; ?>" onclick="adUnits()"  value="<?php echo $options['tblProducttype']['ptype_id']; ?>">
                                                </label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="icheck-list">
                                            <?php foreach ($email as $options) { ?>
                                                <label>
                                                    <input type="checkbox" <?php echo in_array($options['tblProducttype']['ptype_id'], $allowedMediaTypes) ? "checked" : "" ?>  name="productTypes[]" id="adunits" class="icheck" data-checkbox="icheckbox_line-grey" data-label="<?php echo $options['tblProducttype']['ptype_name']; ?>" value="<?php echo $options['tblProducttype']['ptype_id']; ?>">
                                                </label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="icheck-list">
                                            <?php foreach ($leadGen as $options) { ?>
                                                <label>
                                                    <input type="checkbox" <?php echo in_array($options['tblProducttype']['ptype_id'], $allowedMediaTypes) ? "checked" : "" ?>  name="productTypes[]" id="adunits" class="icheck" data-checkbox="icheckbox_line-grey" data-label="<?php echo $options['tblProducttype']['ptype_name']; ?>" value="<?php echo $options['tblProducttype']['ptype_id']; ?>">
                                                </label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="icheck-list">
                                            <?php foreach ($social as $options) { ?>
                                                <label>
                                                    <input type="checkbox" <?php echo in_array($options['tblProducttype']['ptype_id'], $allowedMediaTypes) ? "checked" : "" ?>  name="productTypes[]" id="adunits" class="icheck" data-checkbox="icheckbox_line-grey" data-label="<?php echo $options['tblProducttype']['ptype_name']; ?>" value="<?php echo $options['tblProducttype']['ptype_id']; ?>">
                                                </label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="icheck-list">
                                            <?php foreach ($mobile as $options) { ?>
                                                <label>
                                                    <input type="checkbox" <?php echo in_array($options['tblProducttype']['ptype_id'], $allowedMediaTypes) ? "checked" : "" ?>  name="productTypes[]" id="adunits" class="icheck" data-checkbox="icheckbox_line-grey" data-label="<?php echo $options['tblProducttype']['ptype_name']; ?>" value="<?php echo $options['tblProducttype']['ptype_id']; ?>">
                                                </label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h3 class="form-section"  style="display:none;padding:10px;">Trafficking Options</h3>
                    <!-- BEGIN ACCORDION PORTLET-->
                    <div class="portlet light"  style="display:none;padding:10px;">
                        <div class="portlet-body">
                            <div class="panel-group accordion" id="accordion3">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
                                                Ad Units</a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_1" class="panel-collapse in">
                                        <div class="panel-body">	
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <select multiple="multiple" id="adunit-select"  class="form-control" name="adunit-select[]" size="10">
                                                                <?php foreach ($mappedUnits as $mappedUnit) { ?>
                                                                    <option selected value="<?php echo $mappedUnit['tblMappedAdunit']['ma_ad_id']; ?>"><?php echo $mappedUnit['tblAdunit']['adunit_name']; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2">
                                                Geography </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <select multiple="multiple" id="geography-select"  class="form-control" name="geography-select[]" size="10">
                                                                <?php foreach ($tblCountries as $options) { ?>
                                                                    <option <?php echo in_array($options['tblCountries']['id'], $allowedCountries) ? "selected" : "" ?> value='<?php echo $options['tblCountries']['id'] ?>'><?php echo $options['tblCountries']['country_name'] ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <input type="hidden" name="data[tblLineItem][li_id]" value="<?php echo $lineItemId ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--code by faiz-->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3">
                                                Audience Segment  <span style="margin-left:20px;" id="total"></span> </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_3_3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <select multiple="multiple" id="audience-select"  class="form-control" name="audience-select[]" size="10">
                                                                <?php foreach ($audience_segments as $options) { ?>
                                                                    <option value='<?php echo $options['audience_segments']['segment_id'] ?>'><?php echo $options['audience_segments']['segment_name'] ?> (<?php echo $options['audience_segments']['segment_count'] ?>)</option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end code-->
                            </div>
                        </div>
                    </div>
                    <!-- END ACCORDION PORTLET-->
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-offset-6 col-md-6">
                                        <button type="submit" class="btn green">Submit</button>
                                        <button type="button" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->	
    <style>
        .select2-container-multi .select2-choices .select2-search-choice {
            margin : 6px 0 3px 5px !important;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice
        {
            padding: 5px 5px;
        }
    </style>		
    <script>
        function ShowFieldType(fieldtype, key) {
            var id = "showFieldTypeValue" + key;
            var date_type = "ShowDatetype" + key;
            $('#' + date_type).html('');
            if (fieldtype == 2 || fieldtype == 3 || fieldtype == 5) {
                document.getElementById(id).innerHTML = "<input name='data[LineItemField][fld_custom_texts][" + key + "]' value='' class='form-control' type='text' id='LineItemFieldFldCustomValues' placeholder='Put values as semicolon seprated eg:- emp1;emp2'> ";
            } else if (fieldtype == 4) {
                document.getElementById(id).innerHTML = "<input name='data[LineItemField][fld_custom_texts][" + key + "]' value='' class='form-control' type='text' id='LineItemFieldFldCustomValues' placeholder='Default value here'> ";
            }
            else if (fieldtype == 6) {
<?php
$DateFormat = configure::read('DateFormat');
$DateFormatOption = "";
if (!empty($DateFormat)) {
    foreach ($DateFormat as $key => $value) {
        $DateFormatOption = $DateFormatOption . "<option value='" . $key . "'>" . $value . "</option>";
    }
}
?>
                $('#' + date_type).html("<input name='data[LineItemField][field_format][" + key + "]' value='' class='form-control' type='text' id='field_format" + key + "' placeholder='eg:-mm/dd/yyyy'>");
                $('#' + id).html("<select  class='js-creative-field-type-array form-control' name='data[LineItemField][fld_custom_texts][" + key + "]' onchange ='ShowDatePlaceholder(this.value," + key + ");'id='fld_custom_texts" + key + "'><?php echo preg_replace("/\r|\n/", "", $DateFormatOption); ?></select>");
            }
            else {
                document.getElementById(id).innerHTML = "";
            }
        }

        function calculate() {

            var quantity = $('#txtQuantity').val().replace(/,/g, '');

            $(".quntity_validation").attr("min", 0);
            $(".quntity_validation").attr("max", quantity);

            var e = document.getElementById("costStructure");
            if (e.selectedIndex != -1) {
                var txtCalculate = 0;
                var strUser = e.options[e.selectedIndex].value;
                var rate = document.getElementById("txtRate").value;
                var txtQuantity = document.getElementById("txtQuantity").value;
                if (strUser != 5)
                {

                    if (rate.replace('$', '').replace(' ', '') != '' && txtQuantity.replace(/,/g, '') != '')
                    {

                        document.getElementById("txtCalculate").value = txtCalculate = parseFloat(rate.replace('$', '').replace(' ', '')) * quantity;
                    }
                } else {
                    if (rate.replace('$', '').replace(' ', '') != '' && txtQuantity.replace(/,/g, '') != '')
                    {
                        document.getElementById("txtCalculate").value = txtCalculate = parseFloat(rate.replace('$', '').replace(' ', '')) * (quantity / 1000);
                    }
                }
                var chkPassport = document.getElementById("ScrubAcceptance");
                if (chkPassport.checked) {
                    document.getElementById("ScrubAcceptanceDiv").style.display = "block";

                    var txtDaily = document.getElementById("txtDaily").value;
                    //var txtweekly = document.getElementById("txtweekly").value;
                    var txtMonthly = document.getElementById("txtMonthly").value;

                    var txtRate = document.getElementById("txtRate").value;

                    var txtScrubRate = document.getElementById("txtScrubRate").value;
                    document.getElementById("calquantity").value = quoteQuantity = fpercent((txtQuantity.replace(/,/g, '')), txtScrubRate);
                    document.getElementById("caltxtDaily").value = fpercent((txtDaily.replace(/,/g, '')), txtScrubRate);
                    //document.getElementById("caltxtweekly").value = fpercent((txtweekly.replace(/,/g, '')), txtScrubRate);
                    document.getElementById("caltxtMonthly").value = fpercent((txtMonthly.replace(/,/g, '')), txtScrubRate);
                    quoteRate = txtCalculate / quoteQuantity;
                    document.getElementById("calliRate").value = '$ ' + (quoteRate).toFixed(2);
                    document.getElementById("caltxtCalculate").value = '$ ' + txtCalculate;

                } else {
                    document.getElementById("ScrubAcceptanceDiv").style.display = "none";
                }

            } else {
                document.getElementById("txtCalculate").value = 0;
            }



        }
        function fpercent(quantity, percent)
        {
            var total = parseInt(quantity) + parseInt(quantity * percent / 100);
            return (isNaN(total)) ? 0 : total;
        }
        function subtractpercent(quantity, percent)
        {
            var total = parseInt(quantity) - parseFloat(quantity * percent / 100);
            return (isNaN(total)) ? 0 : total;
        }
        function checkMoreInfo(option) {
            switch (option) {
                case "3" :
                    document.getElementById("formfeilds").style.display = "block";
                    document.getElementById("formfeildsh").style.display = "block";
                    document.getElementById("leadsh").style.display = "block";
                    document.getElementById("leads").style.display = "block";
                    document.getElementById("auto").style.display = "block";
                    document.getElementById("autoh").style.display = "block";
                    break;
                case "7" :
                    document.getElementById("formfeilds").style.display = "block";
                    document.getElementById("formfeildsh").style.display = "block";
                    document.getElementById("leadsh").style.display = "block";
                    document.getElementById("leads").style.display = "block";
                    document.getElementById("auto").style.display = "block";
                    document.getElementById("autoh").style.display = "block";
                    break;
                default :
                    document.getElementById("formfeilds").style.display = "none";
                    document.getElementById("formfeildsh").style.display = "none";
                    document.getElementById("leadsh").style.display = "none";
                    document.getElementById("leads").style.display = "none";
                    document.getElementById("auto").style.display = "none";
                    document.getElementById("autoh").style.display = "none";
                    break;
            }
        }
        checkMoreInfo("<?php echo $this->request->data['tblLineItem']['li_cs_id']; ?>");
        ShowSubCategory("<?php echo $this->request->data['tblLineItem']['li_category_id']; ?>");
        function ShowSubCategory(value) {
            if (!value) {
                alert('Please select category');
                return false;
            }
            $.ajax({
                type: "POST",
                url: "/insertionorder/getcategory_ajax/" + value,
                dataType: "json",
                success: function (data) {
                    $(".js-example-data-array-subcategories").empty();
                    $(".js-example-data-array-subcategories").select2({
                        data: data
                    });
                    $(".js-example-data-array-subcategories").select2("open");
                    $('.js-example-data-array-subcategories').val("<?php echo $this->request->data['tblLineItem']['li_subcategory_id']; ?>").trigger("change");
                }
            });
        }
        function ShowDatePlaceholder(value, key) {
            if (value == 3 || value == 2) {
                $('#LineItemFieldAdvFldName' + key).attr("placeholder", "eg:- month;day;year");
            } else {
                $('#LineItemFieldAdvFldName' + key).removeAttr("placeholder");
            }
        }

    </script>													
    <style>
        .panel-default>.panel-heading {
            background-color : #73716e !important;
            color : #fff;
        }

        .panel-default>.panel-heading > .panel-title > a:hover {
            color : #fff;
            text-decoration:underline;
        }

        .panel-title{
            font-weight:200 !important;
        }
    </style>
    <script data-sample="1">
        CKEDITOR.replace('AutoResponderEmailbody', {
            height: 260
        });

        window.onload = function () {
            $(".dobformat").change(function () {
                var selectval = $(this).val();
                if (selectval == 3 || selectval == 2) {
                    $('#LineItemFieldAdvFldName4').attr("placeholder", "eg:- month;day;year");
                } else {
                    $('#LineItemFieldAdvFldName4').removeAttr("placeholder");
                }
            });
        };

    </script> 
    <script>
    jQuery(document).ready(function () {
        	
        /***********************************
		** Module     : Prechecked Checking 
		** Created By : Satanik
		** Date       : 21/1/2017
		************************************/
        jQuery("#chekc_all").on('click',function(){
        	if(jQuery(this).prop("checked")){
				jQuery(this).val('1');
			} else {
				jQuery(this).prop("checked", false);
				jQuery(this).val('0');
			}
        });

<?php
if (!empty($LineItemDeparts)) {
    foreach ($LineItemDeparts as $daykey => $dayvalue) {

        if (!empty($dayvalue)) {
            foreach ($dayvalue as $timekey => $time) {
                if ($timekey == 0) {
                    continue;
                }
                ?>
                            nextday++;
                            var appendclass = 'addmoreday' +<?php echo $daykey; ?>;
                            $("." + appendclass).append('<div class="col-md-12"><div class="form-group"><label class="control-label col-md-1"></label><div class="col-md-9"><div class="col-lg-2"></div><div class="col-lg-3"><input type="text" class="timepicker ui-timepicker-input" value="<?php echo $time['start_time']; ?>" name="data[LineItemDepart][start_time][' +<?php echo $daykey; ?> + '][]" placeholder="Start Time" autocomplete="off"></div><div class="col-lg-3"><input type="text"  class="timepicker ui-timepicker-input" value="<?php echo $time['end_time']; ?>" name="data[LineItemDepart][end_time][' +<?php echo $daykey; ?> + '][]" autocomplete="off" placeholder="End Time">  </div><div class="col-lg-3"><a href="javascript:void(0);" class="remday"><button  class="btn" type="button">-</button></a></div></div></div></div>');
                            $(".remday").on('click', function () {
                                $(this).parent().parent().parent().remove();
                                $(this).remove();
                            });
                <?php
            } // end of time foreach
        }// end of if conditon of time 
    }
}
?>
            var nextday = 0
            $(".add-more-day").click(function () {
                var day = $(this).attr('dataday');
                nextday++;
                var appendclass = 'addmoreday' + day;
                $("." + appendclass).append('<div class="col-md-12"><div class="form-group"><label class="control-label col-md-1"></label><div class="col-md-9"><div class="col-lg-2"></div><div class="col-lg-3"><input type="text" class="timepicker ui-timepicker-input" name="data[LineItemDepart][start_time][' + day + '][]" placeholder="Start Time" autocomplete="off"></div><div class="col-lg-3"><input type="text"  class="timepicker ui-timepicker-input" name="data[LineItemDepart][end_time][' + day + '][]" autocomplete="off" placeholder="End Time">  </div><div class="col-lg-3"><a href="javascript:void(0);" class="remday"><button  class="btn" type="button">-</button></a></div></div></div></div>');
                $('.timepicker').timepicker({'step': 5});
                $(".remday").on('click', function () {
                    $(this).parent().parent().parent().remove();
                });
            });

            jQuery('input[name="data[tblLineItem][enable_parting]"]').on('ifUnchecked', function (event) {
                $('#enable-day-parting-display').hide("slow");
            });
            jQuery('input[name="data[tblLineItem][enable_parting]"]').on('ifChecked', function (event) {
                jQuery('#enable-day-parting-display').show("slow");

            });
        });

    </script>
    <script>
        $(function () {
            $('.timepicker').timepicker({'step': 5});
        });
    </script>
    <?php

    function arrayfind($multidimensionals, $field, $value) {
        foreach ($multidimensionals as $key => $multidimensional) {
            if ($multidimensional['LineItemField'][$field] === $value)
                return $multidimensional['LineItemField'];
        }
        return array();
    }

    function arrayfindcheck($multidimensionals, $field, $value) {
        foreach ($multidimensionals as $key => $multidimensional) {

            if (trim($multidimensional['LineItemField'][$field]) == trim($value)) // echo "dfdfdf";
                return true;
        }
        return false;
    }
    ?>
