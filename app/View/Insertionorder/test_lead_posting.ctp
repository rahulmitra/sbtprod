
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
     <h2 style="margin:0px;">Send a Test Lead</h2>
</div>
 <?php echo $this->Form->create(false,array('url'=>'#'));?>
 <?php //echo $this->Form->create(false,array('url'=>array('controller'=>'insertionorder','action'=>'formxml')));?>

	<div class="modal-body">
	   
	  <p class="clearfix"><label style="float: left;">Flight  Name:</label> <b><?php echo 
									$this->Form->input('flight_id', array(
									  'class' => 'audience form-control',
									  'label'=>false,
									  'div'=>false,
									  'type'=>'select',
									  'empty'=>'select flight name ',
									 
									  'options' => $flight_list));
									?></b></p>
	  <p>Lead Name: <b><?php echo $line_item['tblLineItem']['li_name']?></b></p>
	  <p> 
		<?php echo $this->Form->input('xml_response', array('type'=>'checkbox','id'=>'xml_response','label' => false,'div' => false,'data-checkbox'=>'icheckbox_square-grey','class'=>'icheck','before'=>__('<label for="xml_response">Show XML Response &nbsp;&nbsp;   </label>')));?>												  
	 </p>
	   <p> 
		<?php echo $this->Form->input('test_lead', array('type'=>'checkbox','id'=>'test_lead','label' => false,'div' => false,'data-checkbox'=>'icheckbox_square-grey','class'=>'icheck','before'=>__('<label for="test_lead">Test Lead &nbsp;&nbsp;  </label>')));?>												  
	 </p>
		<table class="table table-striped table-hover" id="sample_4">

		<thead> 
		  <tr style="background:#197bac;">
			<th style="color:#fff;">Name</th>
			<th style="color:#fff;">Value</th>
			<th style="color:#fff;">Type & Constraints</th>
		  </tr>
		  <?php if(!empty($lineItemFields)){
				foreach($lineItemFields as $field){ 
					$this->FormDynamic->FieldCreate($field['LineItemField']);
				}	
			  ?>
		  <?php }else{ // end of if conditions ?>
			  <td colspan="3"> There are no fields here.</td>
		  <?php }?>
		</thead>
		<tbody>
			
		  
		</tbody>
	  </table>
	   
	  
	</div>
	<div class="modal-footer">
		<input type="submit" class="btn btn-primary" value="Test Lead">
		or <button type="button" class="btn btn-default" data-dismiss="modal">Cancel and go back</button>
	</div>
 <?php echo $this->Form->end();?> 

<?php  ?>
										 
<style>
.table-striped>tbody>tr:nth-of-type(odd) {
    background-color: #f9f9f9;
}	
	
#test_lead_postingForm .radio {
    min-height: 38px!important;
}
#test_lead_postingForm .icheck-inline {
     margin-top: 0px!important;
}
label {
	margin-left:5px;
	margin-right:10px;
}
#flight_id{
	width: 75%;
    float: right;
}
</style>
 
