<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/orders/">Orders</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/orders/view_line_items/<?php echo $line_dfp_id; ?>">Line Items</a><i class="fa fa-circle"></i>
	</li>
	<li>
		<a href="/orders/view_line_creatives/<?php echo $lineitem; ?>">Creatives</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="#">Upload Creatives</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Upload creative for "<?php echo $line_name ?>" of <?php echo $size; ?></span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<form id="fileupload" action="/insertionorder/upload_creative_ajax/<?php echo $lineitem ?>/<?php echo $size; ?>/">
							<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
							<div class="row fileupload-buttonbar">
								<div class="col-lg-7"  style="margin-top:20px;">
									<!-- The fileinput-button span is used to style the file input field as button -->
									<span class="btn green fileinput-button">
										<i class="fa fa-plus"></i>
										<span>
										Add files... </span>
										<input type="file" name="files[]" multiple="">
									</span>
									<button type="submit" class="btn blue start">
										<i class="fa fa-upload"></i>
										<span>
										Start upload </span>
									</button>
									<!-- The global file processing state -->
									<span class="fileupload-process">
									</span>
								</div>
								<!-- The global progress information -->
								<div class="col-lg-5 fileupload-progress fade">
									<!-- The global progress bar -->
									<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
										<div class="progress-bar progress-bar-success" style="width:0%;">
										</div>
									</div>
									<!-- The extended global progress information -->
									<div class="progress-extended">
										&nbsp;
									</div>
								</div>
							</div>
							<!-- The table listing the files available for upload/download -->
							<table role="presentation" class="table table-striped clearfix">
								<tbody class="files">
								</tbody>
							</table>
							<input type="hidden" name="listID" value="" />
						</form>
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Notes</h3>
							</div>
							<div class="panel-body">
								<ul>
									<li>
										The maximum file size for uploads is <strong>5 MB</strong>
									</li>
									<li>
										Only image files (<strong>JPG, GIF, PNG</strong>) are allowed.
									</li>
									<li>
										Upload all the Banner's of <?php echo $size; ?> with redirect URL.
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script id="template-upload" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
		<tr class="template-upload fade">
        <td>
	<span class="preview"></span>
</td>
<td>
	<p class="name">{%=file.name%}</p>
	<strong class="error text-danger label label-danger"></strong>
</td>
<td class="title">
	<label>Redirect URL: <input name="redirecturl[]" required></label>
</td>
<td>
	<p class="size">Processing...</p>
	<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
		<div class="progress-bar progress-bar-success" style="width:0%;"></div>
	</div>
</td>
<td>
	{% if (!i && !o.options.autoUpload) { %}
	<button class="btn blue start" disabled>
		<i class="fa fa-upload"></i>
		<span>Start</span>
	</button>
	{% } %}
	{% if (!i) { %}
	<button class="btn red cancel">
		<i class="fa fa-ban"></i>
		<span>Cancel</span>
	</button>
	{% } %}
</td>
</tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
		<tr class="template-download fade">
		<td>
		<span class="preview">
		{% if (file.thumbnailUrl) { %}
		<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" width="80" height="67"></a>
		{% } %}
	</span>
</td>
<td>
	<p class="name">
		{% if (file.url) { %}
		<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
		{% } else { %}
		<span>{%=file.name%}</span>
		{% } %}
		</p>
		{% if (file.error) { %}
		<div><span class="label label-danger">Error</span> {%=file.error%}</div>
		{% } %}
		</td>
		<td>
		<span class="size">{%=file.redirecturl%}</span>
		</td>
		<td>
		<span class="size">{%=o.formatFileSize(file.size)%}</span>
		</td>
		<td>
		{% if (file.deleteUrl) { %}
		<button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
		<i class="fa fa-trash-o"></i>
		<span>Delete</span>
		</button>
		<input type="checkbox" name="delete" value="1" class="toggle">
		{% } else { %}
		<button class="btn yellow cancel btn-sm">
		<i class="fa fa-ban"></i>
		<span>Cancel</span>
		</button>
		{% } %}
		</td>
		</tr>
        {% } %}
		</script>
				