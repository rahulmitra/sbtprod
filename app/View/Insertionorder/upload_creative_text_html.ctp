<!-- BEGIN PAGE BREADCRUMB -->
<style>
    .form-horizontal .radio {
        min-height: 38px!important;
    }
    .form-horizontal .icheck-inline {
        margin-top: 0px!important;
    }
    .icheck-inline label {
        margin-left:5px;
        margin-right:10px;
    }
</style>
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="/">Home</a><i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="/orders/">Orders</a><i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="/orders/view_line_items/<?php echo $line_order_id; ?>">Line Items</a><i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="/orders/view_line_creatives/<?php echo $lineitem; ?>">Creatives</a><i class="fa fa-circle"></i>
    </li>
    <li  class="active">
        <a href="#">Upload Creatives</a>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Upload creative for "<?php echo $line_name ?>" of <?php echo $size; ?></span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <?php echo $this->Form->create('tblCreative', array('url' => array('controller' => 'insertionorder', 'action' => 'upload_creative_text_html/' . $lineitem), 'type' => 'file', 'class' => 'form-horizontal')); ?>


                        <div class="form-body">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                You have some form errors. Please check below.
                            </div>
                            <div class="alert alert-success display-hide">
                                <button class="close" data-close="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Creative Type<span class="required">
                                        * </span></label>
                                <div class="col-md-5">
                                    <div class="input-icon right">

                                        <?php
                                        echo $this->Form->input('creatives_type', array(
                                            'class' => 'js-creative-array form-control select2-hidden-accessible',
                                            'label' => false,
                                            'type' => 'select',
                                            'empty' => 'Please Select Creatives type',
                                            'onChange' => 'checkMoreInfo(this.value);',
                                            'selected' => $creatives_type_id,
                                            'options' => $creatives_type
                                        ));
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div id="creative" style="display:none;">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Creative Name</label>
                                    <div class="col-md-5">
                                        <div class="input-icon right">
                                            <?php
                                            echo $this->Form->input('cr_name', array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Enter the Creative Name',
                                                'label' => false
                                            ));
                                            ?>
                                            <span class="help-block">Please add a friendly name that helps you identify the creative easily from a list of multiple options if you decide to add A/B Testing</span>

                                        </div>
                                    </div>
                                </div>


                            </div> 
                            <div id="cr_header" style="display:none;">
                                <div class="form-group">
                                    <label class="control-label col-md-2" id="Subject-line">Subject Line</label>
                                    <div class="col-md-8">
                                        <div class="input-icon right">
                                            <?php
                                            echo $this->Form->textarea('cr_header', array(
                                                'class' => 'form-control summernote',
                                                //'maxlength'=>60,
                                                'placeholder' => 'Enter the text',
                                                'label' => false, 'empty' => 'Please Select Type',
                                            ));
                                            ?>
                                            <span class="help-block Subjectlinehelp">Please add your offer headline.  We recommend keeping this under 60 characters.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>	
                            <div id="Linkaddmore" style="display:none;">
                                <div class="form-group" id="Not_Email_Newsletter">
                                    <label class="control-label col-md-2">Link</label>
                                    <div class="col-md-6">
                                        <div class="input-icon right" data-duplicate="link" >
                                            <?php
                                            echo $this->Form->input('link.', array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Enter the link',
                                                'style' => 'float:left;width:83%;margin-right:10px;margin-bottom:10px;',
                                                'label' => false,
                                            ));
                                            ?> <button id="b1" class="btn 
                                                    add-field-more" style="margin-bottom:10px;"  type="button">+</button>
                                        </div>
                                        <div  class="addmorelink">
                                        </div>
                                    </div>

                                </div>


                            </div>
                            <div id="other_default" style="display:none;">

                                <div class="form-group" id="Not_Email_Newsletter">
                                    <label class="control-label col-md-2">Offer Body</label>
                                    <div class="col-md-8">
                                        <div class="input-icon right">
                                            <?php
                                            echo $this->Form->textarea('cr_body', array(
                                                'class' => 'form-control summernote',
                                                'placeholder' => 'Enter the subject line',
                                                'label' => false, 'empty' => 'Please Select Type',
                                            ));
                                            ?>
                                            <span class="help-block creativebody">You may paste HTML tags in this field.  You can paste up to 300 characters, excluding HTML tags.</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div id="cr_cta" style="display:none;">
                                <div class="form-group">
                                    <label class="control-label col-md-2" id="Subject-line">Call to Action</label>
                                    <div class="col-md-5">
                                        <div class="input-icon right">
                                            <?php
                                            echo $this->Form->input('cr_cta', array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Enter the text',
                                                'label' => false, 'empty' => 'Please Select Type',
                                            ));
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>	



                            <div id="image_creative" style="display:none;">

                                <div class="form-group">
                                    <label class="control-label col-md-2" id="image_creative_title">Image</label>
                                    <div class="col-md-5">
                                        <div class="clearfix">
                                            <div class="icheck-inline">
                                                <span class="btn btn-success fileinput-button">
                                                    <i class="glyphicon glyphicon-plus"></i>
                                                    <span>Select file...</span>
                                                    <?php echo $this->Form->file('cr_banner_name', array('class' => 'file-loading', 'accept' => '.jpg,.jpeg,.png,.gif')); ?>
                                                </span>
                                                <span class="help-block image_creative_help">Recommended image size 120x60 pixels.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div id="cr_privacy" style="display:none;">
                                <div class="form-group" id="Not_Email_Newsletter">
                                    <label class="control-label col-md-2" id="Subject-line">Privacy Policy</label>
                                    <div class="col-md-8">
                                        <div class="input-icon right">
                                            <?php
                                            echo $this->Form->input('cr_privacy_url', array(
                                                'class' => 'form-control summernote',
                                                'placeholder' => 'Enter the Link',
                                                'label' => false, 'empty' => 'Please Select Type',
                                            ));
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Redirect Url</label>
                                <div class="col-md-5">
                                    <div class="input-icon right">
                                        <?php
                                        echo $this->Form->input('cr_redirect_url', array(
                                            'class' => 'form-control',
                                            'placeholder' => 'Enter the Redirect URL here',
                                            'label' => false, 'empty' => 'Please Select Type',
                                        ));
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div id="image_form" >

                                <div class="form-group">
                                    <label class="control-label col-md-2" >Image Type</label>
                                    <div class="col-md-5">
                                        <div class="input-icon right">
                                            <?php
                                            echo $this->Form->input('cr_size', array(
                                                'class' => 'js-creative-image-array form-control select2-hidden-accessible',
                                                'label' => false,
                                                'type' => 'select',
                                                'empty' => 'Please Select Image Type',
                                                'options' => array('300X250' => '300X250', '728x90' => '728x90')
                                            ));
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Status</label>
                                <div class="col-md-5">

                                    <div class="icheck-inline">

                                        <?php
                                        echo $this->Form->input('cr_status', array(
                                            'class' => 'form-control select2-hidden-accessible',
                                            'div' => false,
                                            'legend' => false,
                                            //'label'=>false,
                                            'type' => 'radio',
                                            //'empty' => 'Please Select Type',
                                            'value' => 'Active',
                                            'options' => array('Active' => 'Active', 'InActive' => 'Inactive')));
                                        ?>
                                    </div>
                                </div>
                            </div>


                            <?php //echo $this->Form->hidden('cr_size',array('value'=>$size)); ?> 
                            <?php echo $this->Form->hidden('cr_lid', array('value' => $line_dfp_id)); ?> 

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-offset-6 col-md-6">
                                            <button type="submit" class="btn green">Submit</button>
                                            <button type="button" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function checkMoreInfo(option) {
        // default none  
        document.getElementById("other_default").style.display = "none";
        document.getElementById("cr_header").style.display = "none";
        document.getElementById("cr_privacy").style.display = "none";
        document.getElementById("image_form").style.display = "none";
        document.getElementById("creative").style.display = "none";
        document.getElementById("image_creative").style.display = "none";
        document.getElementById("Linkaddmore").style.display = "none";
        $('.image_creative_help').hide();
        $('.Subjectlinehelp').hide();

        switch (option) {
            case "55" :
                document.getElementById("image_form").style.display = "block";
                document.getElementById("image_creative").style.display = "block";
                document.getElementById("image_creative_title").innerHTML = "Image";
                break;
            case "2" :
                document.getElementById("Not_Email_Newsletter").style.display = "block";
                document.getElementById("other_default").style.display = "block";
                document.getElementById("cr_header").style.display = "block";
                document.getElementById("Subject-line").innerHTML = "Subject Line";
                break;
            case "3" :
                document.getElementById("Not_Email_Newsletter").style.display = "block";
                document.getElementById("Linkaddmore").style.display = "block";

                document.getElementById("creative").style.display = "block";
                break;
            case "13" :
                document.getElementById("Not_Email_Newsletter").style.display = "block";
                document.getElementById("other_default").style.display = "block";
                document.getElementById("cr_cta").style.display = "block";
                document.getElementById("cr_header").style.display = "block";
                document.getElementById("Subject-line").innerHTML = "Subject Line";
                break;
            case "12" :
                document.getElementById("Not_Email_Newsletter").style.display = "block";
                document.getElementById("other_default").style.display = "block";
                document.getElementById("cr_header").style.display = "block";
                document.getElementById("Subject-line").innerHTML = "Subject Line";
                break;
            case "6" :
                document.getElementById("creative").style.display = "block";
                document.getElementById("other_default").style.display = "block";
                document.getElementById("image_creative").style.display = "block";
                document.getElementById("image_creative_title").innerHTML = "Offer Logo/Image";
                document.getElementById("cr_header").style.display = "block";
                document.getElementById("cr_privacy").style.display = "block";
                document.getElementById("Subject-line").innerHTML = "Offer Headline/Question";
                $('.image_creative_help').show();
                $('.Subjectlinehelp').show();

                break;
            default :
                document.getElementById("image_form").style.display = "block";
                document.getElementById("image_creative").style.display = "block";
                break;
        }
    }
//var text_max = 60;
//$('#count_message').html(text_max + ' remaining');
//$('#tblCreativeCrHeader').keyup(function() {
    //var text_length = $('#tblCreativeCrHeader').val().length;
    //var text_remaining = text_max - text_length;

    //$('#count_message').html(text_remaining + ' remaining');
//});


</script>

<script data-sample="1">
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 200, });
    });
//    CKEDITOR.replace('tblCreativeCrBody', {
//        height: 150
//
//    });
//    CKEDITOR.replace('tblCreativeCrHeader', {
//        height: 150
//
//    });

//	CKEDITOR.replace( 'tblCreativeCrHeader',{
//				extraPlugins : 'wordcount',
//				wordcount : {
//					showCharCount : true,
//					showWordCount : true,    
//					// Maximum allowed Word Count		
//
//					// Maximum allowed Char Count
//					maxCharCount: 60
//				}
//				} );

    CKEDITOR.replace('tblCreativeCrPrivacyUrl', {
        height: 150
    });
</script>
