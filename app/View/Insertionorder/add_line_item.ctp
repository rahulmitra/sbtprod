<?php $this->Html->addCrumb('Advertisers', $this->Html->url(null, true)); ?>
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Line Items
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <?php echo $this->Form->create(false, array('url' => array('controller' => 'insertionorder', 'action' => 'add_line_item'), 'class' => 'form-horizontal', 'name' => 'submit_form_line_item_flight', 'id' => 'submit_form_line_item_flight')); ?>
                <?php echo $this->Form->hidden('tblLineItem.status', array('value' => 1)); ?>   

                <div class="form-body">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        You have some form errors. Please check below.
                    </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        <img src="/img/ajax-loade.gif"> Creating..
                    </div>
                    <h3 class="form-section">Line Item Details</h3>
                    <div class="form-group">
                        <label class="control-label col-md-2">Select Order <span class="required">
                                * </span></label>
                        <div class="col-md-5">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <select name="data[tblLineItem][li_order_id]" class="js-example-data-array js-states form-control">
                                    <option value="">--Select--</option>
                                    <?php foreach ($orders as $option) { ?>
                                        <option value="<?php echo $option['tblOrder']['dfp_order_id'] ?>" <?php echo $option['tblOrder']['dfp_order_id'] == $order_id ? "selected" : "" ?> ><?php echo $option['tblOrder']['order_name'] . " - " . $option['tblp']['CompanyName'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Line Item Name <span class="required">
                                * </span></label>
                        <div class="col-md-5">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" name="data[tblLineItem][li_name]" placeholder="Enter Line Item Name Here" value="" >
                                <span class="help-block">
                                    Line Items are the individual tactics within an order.  (For co-registration, a line-item is the actual offer name and is not tied to a specific publisher ) </span>

                            </div>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <label class="control-label col-md-2">Campaign Name</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="data[tblLineItem][camp_name]" placeholder="Enter Campaign Name Here" value="" >
                            <span class="help-block">Many times, an advertiser issues multiple orders for different tactics for a single campaign.  This field ties them together.  Please add a new campaign name or select from a pre-existing set of campaigns for this Company.</span>
                        </div>
                    </div> 
                    <div class="form-group"> 
                        <label class="control-label col-md-2">Select Category</label>
                        <div class="col-md-5">
                            <?php
                            echo $this->Form->input('tblLineItem.li_category_id', array(
                                'class' => 'js-example-data-array-categories js-states form-control',
                                'label' => false,
                                'type' => 'select',
                                'onchange' => 'ShowSubCategory(this.value)',
                                'empty' => 'Select (or start typing) the category for the line item',
                                'options' => $categorylist
                                    )
                            );
                            ?>
                            <div class="col-md-10" style="position:absolute; left:100%; top: 0;">
                                <span class="help-block" style="margin: 0;">We use standard IAB taxonomy to define categories of specific line items.  This helps our system optimize and run reporting with user data.  If you do not find the specific categories for your campaigns listed below, please add the closest categories, and include your categories in the NOTES section below.</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <label class="control-label col-md-2">Select Sub Category</label>
                        <div class="col-md-5">
                            <?php
                            echo $this->Form->input('tblLineItem.li_subcategory_id', array(
                                'class' => 'js-example-data-array-categories js-example-data-array-subcategories js-states form-control',
                                'id' => 'subscategory_content',
                                'label' => false,
                                'type' => 'select',
                                'empty' => 'Select (or start typing) the sub category for the line item',
                                    )
                            );
                            ?>
                        </div>
                    </div>
                    <div class="form-group"> 
                        <label class="control-label col-md-2">Notes</label>
                        <div class="col-md-5">
                            <textarea name="data[tblLineItem][li_notes]" placeholder="Mention your notes here." rows="3" class="form-control"></textarea>
                        </div>
                    </div> 
                    <h3 class="form-section">Pricing & Quantity</h3>
                    <div class="form-group">
                        <label class="control-label col-md-2">Rate Type / Cost Structure <span class="required">
                                * </span>	</label>
                        <div class="col-md-5"> 
                            <select multiple="multiple" onChange="checkMoreInfo(this);" name="data[tblLineItem][li_cs_id]" id="costStructure" class="form-control">
                                <?php foreach ($costStructure as $options) { ?>
                                    <option value='<?php echo $options['tblCostStructure']['cs_id']; ?>'><?php echo $options['tblCostStructure']['cs_name']; ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6">Quantity <span class="required">
                                        * </span></label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Enter Quantity" value="" name="data[tblLineItem][li_quantity]" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'placeholder': '0'" id="txtQuantity" onkeyup="calculate()" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label col-md-3">Rate <span class="required">
                                        * </span></label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Enter Rate" name="data[tblLineItem][li_rate]" value="" id="txtRate" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" style="text-align: right;" onkeyup="calculate()" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-2">Total Cost</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Auto-Calculate" name="data[tblLineItem][li_total]" value="" id="txtCalculate" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"  class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6">Daily</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Daily Limit" name="data[tblLineItem][li_daily_cap]" data-inputmask="'alias': 'numeric',  'autoGroup': true,  'placeholder': '0'" id="txtDaily" onkeyup="calculate()" style="text-align: right;" class="form-control quntity_validation">
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label col-md-3">Weekly </label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Weekly Limit" name="data[tblLineItem][li_weekly_cap]" id="txtweekly" data-inputmask="'alias': 'numeric', 'autoGroup': true,   'placeholder': '0'" onkeyup="calculate()" style="text-align: right;" class="form-control quntity_validation">
                                </div>
                            </div>
                        </div>-->
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-2">Monthly</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Monthly Limit" name="data[tblLineItem][li_monthly_cap]"  id="txtMonthly" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'placeholder': '0'" onkeyup="calculate()" style="text-align: right;" class="form-control quntity_validation">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <h3 class="form-section" style ="padding:10px;">Flight Dates & Renewal</h3>
                <div class="row event_period"  style ="padding:10px;" data-date-start-date="+0d">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-6">Start Date</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <input type="text" name="data[tblLineItem][li_start_date]" value="" id="dt1" placeholder="Select Date" class="form-control actual_range"> 
                                    <span class="input-group-btn">
                                        <button class="btn default " type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-4">End Date</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <input id="dt2" name="data[tblLineItem][li_end_date]" placeholder="Select Date" type="text" class="form-control actual_range">
                                    <span class="input-group-btn">
                                        <button class="btn default event_period_button" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-md-2">OR</label>
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <div class="icheck-list">
                                        <label>
                                            <input type="checkbox" class="icheck" data-checkbox="icheckbox_square-grey" name="data[tblLineItem][li_run_till_end]"> Run till end of allocation </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <div class="icheck-list">
                                        <label>
                                            <input type="checkbox" name="data[tblLineItem][li_auto_renew]"  class="icheck" data-checkbox="icheckbox_square-grey"> Auto Renew Line Item </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Renewal Frequency</label>
                            <div class="col-md-7">
                                <select multiple="multiple" name="data[tblLineItem][li_renewal_frequency]" id="renFrequency" class="form-control">
                                    <option value='1'>Every Calendar Month</option>
                                    <option value='2'>Everytime Allocation Expires</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-1"></label>
                            <div class="col-md-5">
                                <div class="checkbox">
                                    <div class="icheck-list" >
                                        <label for="ScrubAcceptance" >
                                            <input type="checkbox" name="data[tblLineItem][li_is_scrub]"   id="ScrubAcceptance" class="icheck" data-checkbox="icheckbox_square-grey"> Post Scrub Acceptance. If Yes, enter maximum permissible scrub rate </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-1" style="float:left;">
                                <input type="text" placeholder="in %" name="data[tblLineItem][li_scrub_rate]"  id="txtScrubRate" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0'" style="text-align: right;" class="form-control" onkeyup="calculate()">
                            </div>
                        </div>
                    </div>
                </div>



                <div id="ScrubAcceptanceDiv" style="display:none;padding:10px;">
                    <h3 class="form-section">Adjusted Pricing & Quantity</h3>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6">Quantity <span class="required">
                                        * </span></label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Enter Quantity"  readonly value=""   data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'placeholder': '0'"   id="calquantity" style="text-align: right;" class="form-control" onkeyup="calculate()">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label col-md-3">Rate <span class="required">
                                        * </span></label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Enter Rate" readonly value="" id="calliRate" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'" style="text-align: right;"  class="form-control" onkeyup="calculate()">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-2">Total Cost</label>
                                <div class="col-md-4">
                                    <input type="text" readonly placeholder="Auto-Calculate" id="caltxtCalculate" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': '$ ', 'placeholder': '0'"  class="form-control" style="text-align: right;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label col-md-6">Daily</label>
                                <div class="col-md-5">
                                    <input type="text" placeholder="Daily Limit" readonly  data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true,  'placeholder': '0'" id="caltxtDaily" style="text-align: right;" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label col-md-3">Weekly </label>
                                <div class="col-md-7">
                                    <input type="text" placeholder="Weekly Limit" readonly id="caltxtweekly" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true,   'placeholder': '0'" style="text-align: right;" class="form-control">
                                </div>
                            </div>
                        </div>-->
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-2">Monthly</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Monthly Limit" readonly id="caltxtMonthly" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'placeholder': '0'" style="text-align: right;" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div style="padding:10px;">
                    <h3 class="form-section">Day Parting Setting</h3>
                    <div class="row">
                        <div class="col-xs-4" style="text-align: left;">
                            <div class="form-group" style="text-align: left;">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-5">
                                    <div class="checkbox">
                                        <div class="icheck-list">
                                            <label for="enable_parting" >
                                                <?php echo $this->Form->checkbox('tblLineItem.enable_parting', array('class' => 'icheck', 'select' => 'true', 'id' => 'enable_parting', 'label' => true, 'data-checkbox' => 'icheckbox_square-grey', 'label' => false)); ?>  Enable Day Parting</label>

                                        </div>
                                    </div>
                                </div>		


                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Select Time zone</label>
                                <div class="input-icon right col-md-6">
                                    <?php
                                    echo $this->Form->input('tblLineItem.timezone', array(
                                        'class' => 'js-example-data-array js-states form-control',
                                        'label' => false,
                                        'type' => 'select',
                                        'options' => $timezones
                                            )
                                    );
                                    ?>
                                </div> 
                            </div>
                        </div>

                        <div id="enable-day-parting-display" style="margin:0px 5px;display:none">
                            <?php $weekdays = array('1' => 'Monday', '2' => 'Tuesday', '3' => 'Wednesday', '4' => 'Thusday', '5' => 'Friday', '6' => 'Sunday'); ?>
                            <?php foreach ($weekdays as $daykey => $day) { ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-1"></label>
                                        <div class="col-md-9">										 

                                            <label for="mon" class="col-lg-2">
                                                <?php echo $this->Form->checkbox('LineItemDepart.week_day.' . $daykey, array('class' => 'icheck', 'select' => 'true', 'id' => $day, 'hiddenField' => false, 'data-checkbox' => 'icheckbox_square-grey', 'label' => false)); ?> <?php echo $day; ?> </label>
                                            <div class="col-lg-3">
                                                <?php echo $this->Form->input('LineItemDepart.start_time.' . $daykey . '.', array('class' => 'timepicker', 'label' => false, 'div' => false, 'placeholder' => 'Start Time')); ?>
                                            </div>
                                            <div class="col-lg-3">
                                                <?php echo $this->Form->input('LineItemDepart.end_time.' . $daykey . '.', array('class' => 'timepicker', 'label' => false, 'div' => false, 'placeholder' => 'End Time')); ?>  </div>
                                            <div class="col-lg-3">
                                                <button id="b2" class="btn add-more-day" dataday="<?php echo $daykey; ?>" type="button">+</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>	
                                <div class="addmoreday<?php echo $daykey ?>"></div>	
                            <?php } ?>  	
                        </div>					 
                    </div>
                </div>
                <h3  style="display:none;padding:10px;" id="formfeildsh" class="form-section" >Form Fields to be Collected</h3>
                <div  style="display:none;padding:10px;" id="formfeilds" class="row" >
                    <div style="padding: 0 1px 6px 17px;">
	                  <input type="checkbox" name="data[tblLineItem][chekc_all]" id="chekc_all" value="<?php echo isset($prechecked) && !empty($prechecked) ? '1' : '0' ?>" <?php echo isset($prechecked) && !empty($prechecked) ? 'checked' : '' ?> />
	                  <label>Display This Offer as Pre-Checked</label>
	                </div>
                    <div class="col-md-12">
                        <table class="table table-hover table-bordered table-striped" id= "addmapfield">
                            <thead>
                                <tr>
                                    <th width="10%">Enabled</th>
                                    <th width="16%">Field Description</th>
                                    <th width="20%">Field Type</th>
                                    <th width="20%">Value</th>
                                    <th width="25%">Field Label</th>
                                    <th width="10%" align="center">Required</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // these configure form app/config/config.php
                                $addfields = configure::read('CollectedCollect');
                                $totalfields = count($addfields) - 1;
                                if (!empty($addfields)) {
                                    $counter = 0;
                                    foreach ($addfields as $key => $value) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $this->Form->input('LineItemField.status.' . $counter, array('type' => 'checkbox', 'id' => 'status' . $counter, 'label' => false, 'hiddenField' => false, 'div' => false, 'class' => false, 'checked' => ($value['defaultcheck'] == 1) ? true : false, 'after' => __('<label for="status' . $counter . '">Yes</label>'))); ?>

                                            </td>
                                            <td>
                                                <?php
                                                /// this is used for identify custom field
                                                echo $this->Form->hidden('LineItemField.custom.' . $counter, array('value' => 0, 'div' => false, 'label' => false));
                                                echo $this->Form->hidden('LineItemField.fld_label.' . $counter, array('class' => 'form-control demo auto-w', 'value' => $value['label'], 'div' => false, 'label' => false));
                                                ?>

                                                <?php echo $value['label']; ?> <?php echo $this->Form->hidden('LineItemField.fld_name.' . $counter, array('class' => 'form-control demo auto-w', 'value' => $key, 'div' => false, 'label' => false)); ?>
                                            </td>


                                            <td>
                                                <?php
                                                $CollectFieldType = configure::read('CollectFieldType');
                                                ?>

                                                <?php
                                                /// this is used for identify custom field
                                                if (in_array($key, array("phone", "dob"))) {
                                                    echo $this->Form->input('LineItemField.field_format.' . $counter, array(
                                                        'label' => false,
                                                        'placeholder' => $value['placeholder'],
                                                        'class' => 'form-control'));
                                                } else {
                                                    echo (!empty($CollectFieldType[$value['type']])) ? $CollectFieldType[$value['type']] : 'N/A';
                                                }
                                                ?>
                                                <div style="display:none">
                                                    <?php
                                                    echo $this->Form->input('LineItemField.fld_type.' . $counter, array(
                                                        'label' => false,
                                                        'type' => 'hidden',
                                                        'value' => $value['type'],
                                                        'class' => 'js-creative-field-type-array form-control',
                                                            //'onchange'=>'ShowFieldType(this.value,'.$counter.')',
                                                            // 'options' => configure::read('CollectFieldType')
                                                    ));
                                                    ?>
                                                </div>
                                                <div id="ShowDatetype<?php echo $counter ?>"></div></td>
                                            </td>	
                                            <td> 
                                                <?php
                                                /* if(in_array($key,array("phone"))){
                                                  echo $this->Form->input('LineItemField.fld_custom_texts.'.$counter,array(
                                                  'label' => false,
                                                  'placeholder' => (!empty($value['placeholder']))?$value['placeholder']:'',
                                                  'class' => 'form-control'));

                                                  }
                                                  else */ if (in_array($key, array("gender"))) {
                                                    echo $this->Form->input('LineItemField.fld_custom_texts.' . $counter, array(
                                                        'label' => false,
                                                        'value' => $value['defaultoption'],
                                                        'placeholder' => 'Put your disclaimer here',
                                                        'class' => 'form-control'));
                                                } else if (in_array($key, array("dob"))) {
                                                    echo $this->Form->input('LineItemField.fld_custom_texts.' . $counter, array(
                                                        'label' => false,
                                                        'type' => 'select',
                                                        'onchange' => 'ShowDatePlaceholder(this.value,' . $counter . ')',
                                                        'options' => (!empty($value['format'])) ? $value['format'] : array(),
                                                        'class' => 'form-control'));
                                                }
                                                ?>
                                                <div id="showFieldTypeValue<?php echo $counter ?>"></div></td>
                                            <td>
                                                <?php
                                                echo $this->Form->input('LineItemField.adv_fld_name.' . $counter, array('label' => false, 'div' => false, 'class' => 'form-control demo auto-w'));
                                                ?>

                                            </td>
                                            <td>
        <?php echo $this->Form->input('LineItemField.required.' . $counter, array('type' => 'checkbox', 'id' => 'required' . $counter, 'label' => false, 'hiddenField' => false, 'div' => false, 'class' => false)); ?>

                                            </td>
                                        </tr>
        <?php
        $counter++; // increament of counter variable 
    } // end of foreach of addfileds
} // end of if condition of addfileds
?>
                                <tr style="background-color: #f5f5f5;"><td colspan="6"><button id="b1" class="btn add-field-more" type="button">+</button>&nbsp;&nbsp;</td></tr> 
                        </table> 
                    </div>
                    <!--<div class="col-md-3">
                            <div class="form-group">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-8">
                                            
                                                    
                                                    
                                                    <div class="input-group">
                                                            <div class="icheck-list">
                                                    
                                                            
                                                            
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Last Name @| firstname @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'Last Name', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Street Address @| address1 @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'Street Address', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'TimeStamp @| timestamp @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'TimeStamp', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                    </div>
                    
                    <div class="col-md-2">
                            <div class="form-group">
                                    <div class="col-md-12">
                                            <div class="input-group">
                                                    <div class="icheck-list">
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Last Name @| firstname @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'Last Name', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                            
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'City @| city @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'City', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'IP Address @| ipaddress @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'First Name', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                    </div>
                    <div class="col-md-2">
                            <div class="form-group">
                                    <div class="col-md-12">
                                            <div class="input-group">
                                                    <div class="icheck-list">
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Email Address @| email @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'Email Address', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                            
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'State @| state @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'State', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Lead Source @| source @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'Lead Source', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                    </div>
                    <div class="col-md-2">
                            <div class="form-group">
                                    <div class="col-md-12">
                                            <div class="input-group">
                                                    <div class="icheck-list">
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Phone Number @| phone @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'Phone Number', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                            <label>
                    <?php
                    echo $this->Form->input('LineItemField.productTypes.', array(
                        'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Zip Code @| zip @| Form Fields to be Collected @|',
                        'id' => 'adunits', 'data-label' => 'Zip Code', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                    ?>
                                                            </label>
                                                            
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                    </div>
                    <div class="col-md-2">
                            <div class="form-group">
                                    <div class="col-md-12">
                                            <div class="input-group">
                                                    <div class="icheck-list">
                                                            
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                    </div> !-->
                </div>
                <h3  style="display:none;padding:10px;" id="leadsh" class="form-section" >Lead Validation</h3>
                <div  id="leads" style="display:none; padding:10px;" class="row" >

                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-11">
                                <div class="input-group">
                                    <div class="icheck-list">

                                        <label>
                                            <?php
                                            echo $this->Form->input('LineItemField.productTypes.', array(
                                                'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => '3rd Party Email Address Validation @| chk3rdpartyVal @| Lead Validation @|',
                                                'id' => 'chk3rdpartyVal', 'data-label' => '3rd Party Email Address Validation', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                                            ?>
                                        </label>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="icheck-list">

                                        <label>
                                            <?php
                                            echo $this->Form->input('LineItemField.productTypes.', array(
                                                'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Syntax Validation of Email Address @| chk3rdpartyEmail @| Lead Validation @|',
                                                'id' => 'chk3rdpartyVal', 'data-label' => 'Syntax Validation of Email Address', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                                            ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="icheck-list">

                                        <label>
                                            <?php
                                            echo $this->Form->input('LineItemField.productTypes.', array(
                                                'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Phone Number to Address Match @| 3rdPartyPhoneVal @| Lead Validation @|',
                                                'id' => 'chk3rdpartyVal', 'data-label' => 'Phone Number to Address Match', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                                            ?>
                                        </label>	
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="icheck-list">

                                        <label>
                                            <?php
                                            echo $this->Form->input('LineItemField.productTypes.', array(
                                                'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => 'Postal Name Address Match @| 3rdPartyPostalMatch @| Lead Validation @|',
                                                'id' => 'chk3rdpartyVal', 'data-label' => 'Postal Name Address Match', 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                                            ?>
                                        </label>	
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h3  id="autoh" style="display:none; padding:10px;" class="form-section"  >Auto Responder Setting</h3>
                <div id="auto" style="display:none;padding:10px;" class="row">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-1"></label>
                            <div class="col-md-5">
                                <div class="checkbox">
                                    <div class="icheck-list" >

                                        <label for="AutoResponderIsAutoresp" >
                                            <?php echo $this->Form->checkbox('AutoResponder.is_autoresp', array('class' => 'icheck', 'label' => true, 'data-checkbox' => 'icheckbox_square-grey', 'label' => false)); ?>  Enable Auto Responder</label>											 

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="AutoResponder" style="display:none;" class="row">
                        <div class="col-md-12">
                            <div class="form-group"> 
                                <label class="control-label col-md-2">From Name</label>
                                <div class="col-md-5">
                                    <?php echo $this->Form->input('AutoResponder.from_name', array('placeholder' => 'Enter From Name', 'label' => false, 'class' => 'form-control')); ?> 
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group"> 
                                <label class="control-label col-md-2">From Email</label>
                                <div class="col-md-5">
                                    <?php echo $this->Form->input('AutoResponder.from_email', array('placeholder' => 'Enter From Email', 'label' => false, 'class' => 'form-control')); ?> 
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group"> 
                                <label class="control-label col-md-2">Subject Line</label>
                                <div class="col-md-5">
                                    <?php echo $this->Form->textarea('AutoResponder.subjectline', array('placeholder' => 'Enter Subject Line Here', 'label' => false, 'class' => 'form-control')); ?> 

                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group"> 
                                <label class="control-label col-md-2">Email Body</label>
                                <div class="col-md-8">
                                    <?php echo $this->Form->textarea('AutoResponder.emailbody', array('placeholder' => 'Enter email Body Here', 'cols' => '80', 'rows' => '10', 'label' => false)); ?> 
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group"> 
                                <label class="control-label col-md-2">Footer</label>
                                <div class="col-md-5">
                                    <?php echo $this->Form->textarea('AutoResponder.footer', array('label' => false, 'class' => 'form-control', 'value' => $test['tblProfile']['Address1'] . ',  ' . $test['tblProfile']['Address2'] . ',  ' . $test['tblProfile']['City'] . ',  ' . $test['tblProfile']['State_Province'] . ',  ' . $test['tblProfile']['ZIP_Postal'] . ',  ' . $test['tblProfile']['Country'])); ?> 
                                </div>
                            </div> 
                        </div>

                        <div class="col-md-12">
                            <div class="form-group"> 
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-8">

                                    <?php echo $this->Form->checkbox('AutoResponder.chkARVal', array('class' => 'icheck', 'label' => true, 'id' => 'chkARVal', 'data-checkbox' => 'icheckbox_square-grey', 'label' => false)); ?> <label for="chkARVal"> Enable Auto ResponderAuto Responder Validation of Email Address</label>
                                    <span class="help-block">Please note that enabling the auto-responder validation will delay the lead submission to your client by up to 10 hours.  Our system will send out the auto-responder and will listen for all bounces, spam complaints and un-subscribe requests during this period, after which, the lead will be classified as valid or rejected based on user action.  
                                    </span>

                                </div>
                            </div> 
                        </div>
                    </div>

                </div>
                <h3 class="form-section"  style="display:none;padding:10px;">Media Type</h3>
                <div class="row" style="display:none;padding:10px;">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-8">
                                <label class="control-label"><b>DESKTOP</b></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label class="control-label"><b>EMAIL </b></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label class="control-label"><b>LEAD-GEN</b></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label class="control-label"><b>SOCIAL</b></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <label class="control-label"><b>MOBILE</b></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="display:none;padding:10px;">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <div class="icheck-list">
                                        <?php foreach ($desktop as $options) { ?>
                                            <label>
                                                <?php
                                                echo $this->Form->input('LineItemField.productTypes.', array(
                                                    'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => $options['tblProducttype']['ptype_name'] . ' @| ' . $options['tblProducttype']['ptype_id'] . ' @| Media Type @| Desktop',
                                                    'id' => 'adunits', 'data-label' => $options['tblProducttype']['ptype_name'], 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                                                ?>
                                            </label>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="icheck-list">
                                        <?php foreach ($email as $options) { ?>
                                            <label>
                                                <?php
                                                echo $this->Form->input('LineItemField.productTypes.', array(
                                                    'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => $options['tblProducttype']['ptype_name'] . ' @| ' . $options['tblProducttype']['ptype_id'] . ' @| Media Type @| Email',
                                                    'id' => 'adunits', 'data-label' => $options['tblProducttype']['ptype_name'], 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                                                ?>
                                            </label>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="icheck-list">
                                        <?php foreach ($leadGen as $options) { ?>
                                            <label>
                                                <?php
                                                echo $this->Form->input('LineItemField.productTypes.', array(
                                                    'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => $options['tblProducttype']['ptype_name'] . ' @| ' . $options['tblProducttype']['ptype_id'] . ' @| Media Type @| LEAD-GEN',
                                                    'id' => 'adunits', 'data-label' => $options['tblProducttype']['ptype_name'], 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                                                ?>
                                            </label>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="icheck-list">
                                        <?php foreach ($social as $options) { ?>
                                            <label>
                                                <?php
                                                echo $this->Form->input('LineItemField.productTypes.', array(
                                                    'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => $options['tblProducttype']['ptype_name'] . ' @| ' . $options['tblProducttype']['ptype_id'] . ' @| Media Type @| SOCIAL',
                                                    'id' => 'adunits', 'data-label' => $options['tblProducttype']['ptype_name'], 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                                                ?>
                                            </label>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="icheck-list">
                                        <?php foreach ($mobile as $options) { ?>
                                            <label>
                                                <?php
                                                echo $this->Form->input('LineItemField.productTypes.', array(
                                                    'class' => 'icheck', 'label' => false, 'div' => false, 'type' => 'checkbox', 'value' => $options['tblProducttype']['ptype_name'] . ' @| ' . $options['tblProducttype']['ptype_id'] . ' @| Media Type @| MOBILE',
                                                    'id' => 'adunits', 'data-label' => $options['tblProducttype']['ptype_name'], 'data-checkbox' => 'icheckbox_line-grey', 'onclick' => 'adUnits()'));
                                                ?>
                                            </label>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h3 class="form-section"  style="display:none;padding:10px;">Trafficking Options</h3>
                <!-- BEGIN ACCORDION PORTLET-->
                <div class="portlet light"  style="display:none;padding:10px;">
                    <div class="portlet-body">
                        <div class="panel-group accordion" id="accordion3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
                                            Ad Units</a>
                                    </h4>
                                </div>
                                <div id="collapse_3_1" class="panel-collapse in">
                                    <div class="panel-body">	
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <select multiple="multiple" id="adunit-select"  class="form-control" name="adunit-select[]" size="10">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2">
                                            Geography </a>
                                    </h4>
                                </div>
                                <div id="collapse_3_2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <select multiple="multiple" id="geography-select"  class="form-control" name="geography-select[]" size="10">
                                                            <?php foreach ($tblCountries as $options) { ?>
                                                                <option value='<?php echo $options['tblCountries']['id'] ?>'><?php echo $options['tblCountries']['country_name'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--code by faiz-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3">
                                            Audience Segment  <span style="margin-left:20px;" id="total"></span> </a>
                                    </h4>
                                </div>
                                <div id="collapse_3_3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <select multiple="multiple" id="audience-select"  class="form-control" name="audience-select[]" size="10">
                                                            <?php foreach ($audience_segments as $options) { ?>
                                                                <option value='<?php echo $options['audience_segments']['segment_id'] ?>'><?php echo $options['audience_segments']['segment_name'] ?> (<?php echo $options['audience_segments']['segment_count'] ?>)</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--end code-->
                        </div>
                    </div>
                </div>

                <!-- END ACCORDION PORTLET-->
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div align="center">
                                    <button type="submit" class="btn green">Submit</button>
                                    <button type="button" class="btn default">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT INNER -->	
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        margin : 6px 0 3px 5px !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice
    {
        padding: 5px 5px;
    }
</style>

<script>
    jQuery(document).ready(function () {
        var nextday = 0
        $(".add-more-day").click(function () {
            var day = $(this).attr('dataday');
            nextday++;
            var appendclass = 'addmoreday' + day;
            $("." + appendclass).append('<div class="col-md-12"><div class="form-group"><label class="control-label col-md-1"></label><div class="col-md-9"><div class="col-lg-2"></div><div class="col-lg-3"><input type="text" class="timepicker ui-timepicker-input" name="data[LineItemDepart][start_time][' + day + '][' + nextday + ']" placeholder="Start Time" autocomplete="off"></div><div class="col-lg-3"><input type="text"  class="timepicker ui-timepicker-input" name="data[LineItemDepart][end_time][' + day + '][' + nextday + ']" autocomplete="off" placeholder="End Time">  </div><div class="col-lg-3"><a href="javascript:void(0);" class="remday"><button  class="btn" type="button">-</button></a></div></div></div></div>');
            $('.timepicker').timepicker({'step': 5});
            $(".remday").on('click', function () {
                $(this).parent().parent().parent().remove();
            });
        });

        jQuery('input[name="data[tblLineItem][enable_parting]"]').on('ifUnchecked', function (event) {
            $('#enable-day-parting-display').hide("slow");
        });
        jQuery('input[name="data[tblLineItem][enable_parting]"]').on('ifChecked', function (event) {
            jQuery('#enable-day-parting-display').show("slow");
        });
        
        /***********************************
		** Module     : Prechecked Checking 
		** Created By : Satanik
		** Date       : 21/1/2017
		************************************/
        jQuery("#chekc_all").on('click',function(){
        	if(jQuery(this).prop("checked")){
				jQuery(this).val('1');
			} else {
				jQuery(this).prop("checked", false);
				jQuery(this).val('0');
			}
        });
        
    });

</script>

<script>
    function ShowFieldType(fieldtype, key) {
        var id = "showFieldTypeValue" + key;
        var date_type = "ShowDatetype" + key;
        $('#' + date_type).html('');
        if (fieldtype == 2 || fieldtype == 3 || fieldtype == 5) {
            document.getElementById(id).innerHTML = "<input name='data[LineItemField][fld_custom_texts][" + key + "]' value='' class='form-control' type='text' id='LineItemFieldFldCustomValues' placeholder='Put values as semicolon seprated eg:- emp1;emp2'> ";
        } else if (fieldtype == 4) {
            document.getElementById(id).innerHTML = "<input name='data[LineItemField][fld_custom_texts][" + key + "]' value='' class='form-control' type='text' id='LineItemFieldFldCustomValues' placeholder='Default value here'> ";
        }
        else if (fieldtype == 6) {
<?php
$DateFormats = configure::read('DateFormat');
$DateFormatOption = "";
if (!empty($DateFormats)) {
    foreach ($DateFormats as $key => $value) {
        $DateFormatOption = $DateFormatOption . "<option value='" . $key . "'>" . $value . "</option>";
    }
}
?>
            $('#' + date_type).html("<input name='data[LineItemField][field_format][" + key + "]' value='' class='form-control' type='text' id='field_format" + key + "' placeholder='eg:-mm/dd/yyyy'>");
            $('#' + id).html("<select  class='js-creative-field-type-array form-control' name='data[LineItemField][fld_custom_texts][" + key + "]' onchange ='ShowDatePlaceholder(this.value," + key + ");'id='fld_custom_texts" + key + "'><?php echo preg_replace("/\r|\n/", "", $DateFormatOption); ?></select>");
        }
        else {
            document.getElementById(id).innerHTML = "";
        }
    }
    function calculate() {
        var quantity = $('#txtQuantity').val().replace(/,/g, '');
        $(".quntity_validation").attr("min", 0);
        $(".quntity_validation").attr("max", quantity);

        var e = document.getElementById("costStructure");
        if (e.selectedIndex != -1) {
            var txtCalculate = 0;
            var strUser = e.options[e.selectedIndex].value;
            var rate = document.getElementById("txtRate").value;
            var txtQuantity = document.getElementById("txtQuantity").value;
            if (strUser != 5)
            {

                if (rate.replace('$', '').replace(' ', '') != '' && txtQuantity.replace(/,/g, '') != '')
                {
                    document.getElementById("txtCalculate").value = txtCalculate = parseFloat(rate.replace('$', '').replace(' ', '')) * quantity;


                }
            } else {
                if (rate.replace('$', '').replace(' ', '') != '' && txtQuantity.replace(/,/g, '') != '')
                {
                    document.getElementById("txtCalculate").value = txtCalculate = parseFloat(rate.replace('$', '').replace(' ', '')) * (quantity / 1000);

                }
            }
            var chkPassport = document.getElementById("ScrubAcceptance");
            if (chkPassport.checked) {
                document.getElementById("ScrubAcceptanceDiv").style.display = "block";
            } else {
                document.getElementById("ScrubAcceptanceDiv").style.display = "none";
            }
            var txtDaily = document.getElementById("txtDaily").value;
           // var txtweekly = document.getElementById("txtweekly").value;
            var txtMonthly = document.getElementById("txtMonthly").value;

            var txtRate = document.getElementById("txtRate").value;

            var txtScrubRate = document.getElementById("txtScrubRate").value;
            document.getElementById("calquantity").value = quoteQuantity = fpercent((txtQuantity.replace(/,/g, '')), txtScrubRate);
            document.getElementById("caltxtDaily").value = fpercent((txtDaily.replace(/,/g, '')), txtScrubRate);
            //document.getElementById("caltxtweekly").value = fpercent((txtweekly.replace(/,/g, '')), txtScrubRate);
            document.getElementById("caltxtMonthly").value = fpercent((txtMonthly.replace(/,/g, '')), txtScrubRate);
            quoteRate = txtCalculate / quoteQuantity;

            document.getElementById("calliRate").value = '$ ' + (quoteRate).toFixed(2);
            document.getElementById("caltxtCalculate").value = '$ ' + txtCalculate;


        } else {
            document.getElementById("txtCalculate").value = 0;
        }



    }
    function fpercent(quantity, percent)
    {
        var total = parseInt(quantity) + parseInt(quantity * percent / 100);
        return (isNaN(total)) ? 0 : total;
    }
    function subtractpercent(quantity, percent)
    {
        var total = parseInt(quantity) - parseFloat(quantity * percent / 100);
        return (isNaN(total)) ? 0 : total;
    }

    function ShowDatePlaceholder(value, key) {
        if (value == 3 || value == 2) {
            $('#LineItemFieldAdvFldName' + key).attr("placeholder", "eg:- month;day;year");
        } else {
            $('#LineItemFieldAdvFldName' + key).removeAttr("placeholder");
        }
    }
    function checkMoreInfo(option) {
        switch (option.value) {
            case "3" :
                document.getElementById("formfeilds").style.display = "block";
                document.getElementById("formfeildsh").style.display = "block";
                document.getElementById("leadsh").style.display = "block";
                document.getElementById("leads").style.display = "block";
                document.getElementById("auto").style.display = "block";
                document.getElementById("autoh").style.display = "block";
                break;
            case "7" :
                document.getElementById("formfeilds").style.display = "block";
                document.getElementById("formfeildsh").style.display = "block";
                document.getElementById("leadsh").style.display = "block";
                document.getElementById("leads").style.display = "block";
                document.getElementById("auto").style.display = "block";
                document.getElementById("autoh").style.display = "block";
                $('AutoResponder').hide();
                break;
            default :
                document.getElementById("formfeilds").style.display = "none";
                document.getElementById("formfeildsh").style.display = "none";
                document.getElementById("leadsh").style.display = "none";
                document.getElementById("leads").style.display = "none";
                document.getElementById("auto").style.display = "none";
                document.getElementById("autoh").style.display = "none";
                break;
        }
    }
    function ShowSubCategory(value) {

        if (!value) {
            alert('Please select category');
            return false;
        }
        $.ajax({
            type: "POST",
            url: "/insertionorder/getcategory_ajax/" + value,
            dataType: "json",
            success: function (data) {
                $(".js-example-data-array-subcategories").empty();
                $(".js-example-data-array-subcategories").select2({
                    data: data
                });
                $(".js-example-data-array-subcategories").select2("open");
            }
        });
    }
</script>					
<style>
    .panel-default>.panel-heading {
        background-color : #73716e !important;
        color : #fff;
    }

    .panel-default>.panel-heading > .panel-title > a:hover {
        color : #fff;
        text-decoration:underline;
    }

    .panel-title{
        font-weight:200 !important;
    }
</style>
<script data-sample="1">
    CKEDITOR.replace('AutoResponderEmailbody', {
        height: 260
    });
</script> 
<script>
    $(function () {
        $('.timepicker').timepicker({'step': 5});
    });
</script>
