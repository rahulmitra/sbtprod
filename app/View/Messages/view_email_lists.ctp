<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/messages/">Message</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Email Lists</span>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
					<a href="javascript:;" class="remove">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover" id="sample_4">
					<thead>
						<tr>
							<th class="table-checkbox">
								AUID
							</th>
							<th>
								Publisher Name
							</th>
							<th>
								Email List Name
							</th>
							<th>
								Media Type
							</th>
							<th>
								Product Type
							</th>
							<th>
								Create Date
							</th>
							<th class="center" style="text-align:center;">
								Action
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($group as $user):
							$class = '';
							if ($i++ % 2 == 0) {
								$class = 'altrow';
							}
							if($user['tblAdunit']['ad_dfp_id'] != "") {
								
							?>
							<tr class="odd gradeX">
								<td>
									<?php echo $user['tblAdunit']['adunit_id']; ?>
								</td>
								<td>
									<?php echo $user['tblp']['CompanyName']; ?>
								</td>
								<td>
									<?php echo $user['tblAdunit']['adunit_name']; ?>
								</td>
								<td>
									<?php echo $user['tbmt']['mtype_name']; ?>
								</td>
								<td>
									<?php echo $user['tbpt']['ptype_name']; ?>
								</td>
								<td class="center">
									<?php $dateformte = Configure::read('Php.dateformte'); ?>
									<?php echo $this->Time->format($dateformte, $user['tblAdunit']['ad_created_at']); ?>&nbsp;
								</td>
								<td align="center">
									<div class="btn-group">
										<a data-toggle="dropdown" href="javascript:;" class="btn btn-circle btn-default" aria-expanded="true">
											<i class="fa fa-share"></i> Action <i class="fa fa-angle-down"></i>
										</a>
										<ul class="dropdown-menu pull-right">
											<li>
												<a style="margin-right:5px;" href="/messages/index/<?php echo $user['tblAdunit']['ad_uid']; ?>">
													<i class="fa fa-database">
													</i> View Stats</a>
											</li>
										</ul>
									</div>	
								</td>
							</tr>
						<?php } endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>