<!-- BEGIN PAGE BREADCRUMB -->
<?php //pr($group);?>

<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/messages/">Messages</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs font-green-sharp"></i>
					<span class="caption-subject font-green-sharp bold uppercase">Email Messages</span>
				</div>
				<div class="actions">
					<div class="btn-group btn-group-devided" data-toggle="buttons">
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "day" ? "active" : "" ?>">
						<input type="radio" name="options" onchange="chart1()" class="toggle" id="option1">Today</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "week" ? "active" : "" ?>">
						<input type="radio" name="options"  onchange="chart2()" class="toggle" id="option2">Last 7 Days</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "month" ? "active" : "" ?>">
						<input type="radio" name="options"  onchange="chart3()" class="toggle" id="option3">MTD</label>
						<label class="btn btn-transparent grey-salsa btn-circle btn-sm <?php echo  $duration == "year" ? "active" : "" ?>">
						<input type="radio" name="options"  onchange="chart4()" class="toggle" id="option2">YTD</label>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row list-separated">
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm">
							Total Sent
						</div>
						<div class="uppercase font-hg font-red-flamingo">
							<?php echo number_format($sent); ?> <span class="font-lg font-grey-mint"></span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm">
							Total Delivered
						</div>
						<div class="uppercase font-hg theme-font">
							<?php echo number_format($delivered); ?> <span class="font-lg font-grey-mint"></span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm">
							Total Unique Opens
						</div>
						<div class="uppercase font-hg font-blue-sharp">
							<?php echo number_format($opens); ?> <span class="font-lg font-grey-mint"></span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="font-grey-mint font-sm">
							Total Unique Clicks
						</div>
						<div class="uppercase font-hg font-purple">
							<?php echo number_format($clicks); ?><span class="font-lg font-grey-mint"></span>
						</div>
					</div>
				</div>
				<div class="clearfix">
					<div id="legendContainer"></div> 
				</div>
				<div id="chart_2" class="chart"></div>
				<div id="sales_statistics" class="portlet-body-morris-fit morris-chart" style="height: 120px">
				</div>
				
				<table class="table table-striped table-bordered table-hover" id="sample_4">
					<thead>
						<tr>
							<th class="table-checkbox">
								MID
							</th>
							<th>
								List
							</th>
							<th>
								Message Subject Line
							</th>
							<th>
								Launch Date
							</th>
							<th align="center">
								Sent / Delivered
							</th>
							<th  align="center">
								Total / Unique Opens
							</th>
							<th  align="center">
								Total / Unique Clicks
							</th>
							<th class="center hide" style="text-align:center;">
								
							</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($group as $user):
							$i++;
							
						?>
						<tr class="odd gradeX">
							<td>
								<?php echo $i; ?>
							</td>
							<td>
								<a href="/messages/index/<?php echo $user['tba']['ad_uid']; ?>"><?php echo $user['tba']['adunit_name']; ?></a>
							</td>
							<td>
								<a href="/messages/view_message_stats/<?php echo $user['tblMailing']['m_id']; ?>">
								<?php echo $user['tblMailing']['m_sl']; ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
								
								<?php echo $this->html->link('',array('controller'=>'messages','action'=>'view_message_stats_download/'.$user['tblMailing']['m_id']),array('class'=>'glyphicon glyphicon-download-alt'))?>
								 
								
							</td>
							<td class="center">
								<?php echo $this->Time->format('d/m/Y', $user['tblMailing']['m_schedule_date']); ?>&nbsp;
							</td>
							<td  class="center">
								<div style="width:100px; height:80px" align="center" id="sent<?php echo $i; ?>"></div>
							</td>
							<td  class="center">
								<div style="width:100px; height:80px;margin-left:20px;" align="center" id="open<?php echo $i; ?>"></div>
							</td>
							<td  class="center">
								<div style="width:100px; height:80px;margin-left:20px;" id="click<?php echo $i; ?>"></div>
							</td>
							
							<td align="center" class="hide">
								<div class="btn-group">
									<a data-toggle="dropdown" href="javascript:;" class="btn btn-circle btn-default" aria-expanded="true">
										<i class="fa fa-share"></i> Action <i class="fa fa-angle-down"></i>
									</a>
									<ul class="dropdown-menu pull-right">
										<li>
											<a style="margin-right:5px;" href="/message/view_stats/<?php echo $user['tblMailing']['m_ad_uid']; ?>/">
												<i class="fa fa-bar-chart-o">
												</i> View Email Stats</a>
										</li>
									</ul>
								</div>							
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
</style>
<script>
	<?php
		$i = 0;
		foreach ($group as $user){
		$i++; ?>
		var sent<?php echo $i; ?>;
		var open<?php echo $i; ?>;
		var click<?php echo $i; ?>;
	<?php } ?>
	
	window.onload = function(){
		<?php
			$i = 0;
			foreach ($group as $user){
				$i++;
				
			?>
			var sent<?php echo $i; ?> = new JustGage({
				id: "sent<?php echo $i; ?>",
				value: <?php echo $user['tblMailing']['m_delivered']; ?>,
				min: 0,
				gaugeWidthScale: 0.6,
				max: <?php echo $user['tblMailing']['m_sent']; ?>
			});
			
			var open<?php echo $i; ?> = new JustGage({
				id: "open<?php echo $i; ?>",
				value: <?php echo $user['tblMailing']['m_u_open']; ?>,
				min: 0,
				gaugeWidthScale: 0.6,
				max: <?php echo $user['tblMailing']['m_open']; ?>
			});
			
			var click<?php echo $i; ?> = new JustGage({
				id: "click<?php echo $i; ?>",
				value: <?php echo $user['tblMailing']['m_u_clicks']; ?>,
				min: 0,
				gaugeWidthScale: 0.6,
				max: <?php echo $user['tblMailing']['m_clicks']; ?>
			});
		<?php } ?>
	};
</script>
<!-- END PAGE CONTENT INNER -->
<script>
	var ChartsFlotcharts = function() {
		
		return {
			//main function to initiate the module
			initCharts: function() {
				
				if (!jQuery.plot) {
					return;
				}
				
				var data = [];
				var totalPoints = 250;
				
				//Interactive Chart
				
				function chart2() {
					if ($('#chart_2').size() != 1) {
						return;
					}
					
					function randValue() {
						return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
					}
					var sent = [
                    <?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblMailing']['m_schedule_date']); ?>,<?php echo $this->Time->format('m',$data['tblMailing']['m_schedule_date']) - 1; ?> ,<?php echo $this->Time->format('d',$data['tblMailing']['m_schedule_date']); ?>)).getTime(), <?php echo $data['tblMailing']['m_sent']; ?>],
					<?php } ?>
					];
					var delivered = [
                    <?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblMailing']['m_schedule_date']); ?>,<?php echo $this->Time->format('m',$data['tblMailing']['m_schedule_date'])  - 1; ?>,<?php echo $this->Time->format('d',$data['tblMailing']['m_schedule_date']); ?>)).getTime(), <?php echo $data['tblMailing']['m_delivered']; ?>],
					<?php } ?>
					];
					var uniqueOpens = [
                    <?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblMailing']['m_schedule_date']); ?>,<?php echo $this->Time->format('m',$data['tblMailing']['m_schedule_date']) - 1; ?>,<?php echo $this->Time->format('d',$data['tblMailing']['m_schedule_date']); ?>)).getTime(), <?php echo $data['tblMailing']['m_u_open']; ?>],
					<?php } ?>
					];
					var uniqueClicks = [
                    <?php foreach($group as $data) { ?>
						[(new Date(<?php echo $this->Time->format('Y',$data['tblMailing']['m_schedule_date']); ?>,<?php echo $this->Time->format('m',$data['tblMailing']['m_schedule_date']) - 1; ?>,<?php echo $this->Time->format('d',$data['tblMailing']['m_schedule_date']); ?>)).getTime(), <?php echo $data['tblMailing']['m_u_clicks']; ?>],
					<?php } ?>
					];
					
					
					var plot = $.plot($("#chart_2"), [{
						data: sent,
						label: "Sent",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:0
						}, {
						data: delivered,
						label: "Delivered",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:1
						}, {
						data: uniqueClicks,
						label: "Unique Clicks",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:2
						} , {
						data: uniqueOpens,
						label: "Unique Opens",
						lines: {
							lineWidth: 1,
						},
						shadowSize: 0,
						idx:3
						}], {
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
								fillColor: {
									colors: [{
										opacity: 0.05
										}, {
										opacity: 0.01
									}]
								}
							},
							points: {
								show: true,
								radius: 3,
								lineWidth: 1
							},
							shadowSize: 2
						},
						grid: {
							hoverable: true,
							clickable: true,
							tickColor: "#eee",
							borderColor: "#eee",
							borderWidth: 1
						},
						colors: ["#d12610", "#37b7f3", "#52e136"],
						xaxis: {
							mode: "time",
							tickSize: [2, "day"],    
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						yaxis: {
							ticks: 11,
							tickDecimals: 0,
							tickColor: "#eee",
						},
						legend:{         
							placement: 'outsideGrid',
							container:$("#legendContainer"),    
							labelFormatter: function(label, series){
								return '<button data-toggle="button" onclick="togglePlot('+series.idx+');" class="btn btn-default ' + ((series.lines.show) ? "active" : "") + '" type="button" aria-pressed="false"> '+label + ((series.lines.show) ? " <span style=\"margin-left: 5px; font-size: 10px;\">&#10006;</span>" : "")+'</button>';
							},
							noColumns: 0
						}
					});
					
					function gd(date_c) {
						var date = Date.parse(date_c);
						return date.toString('dd-MMM-yyyy');
					}
					
					function showTooltip(x, y, contents) {
						$('<div id="tooltip">' + contents + '</div>').css({
							position: 'absolute',
							display: 'none',
							top: y + 5,
							left: x + 15,
							border: '1px solid #333',
							padding: '4px',
							color: '#fff',
							'border-radius': '3px',
							'background-color': '#333',
							opacity: 0.80
						}).appendTo("body").fadeIn(200);
					}
					
					var previousPoint = null;
					$("#chart_2").bind("plothover", function(event, pos, item) {
						$("#x").text(pos.x);
						$("#y").text(pos.y);
						
						if (item) {
							if (previousPoint != item.dataIndex) {
								previousPoint = item.dataIndex;
								
								$("#tooltip").remove();
								var x = item.datapoint[0],
                                y = item.datapoint[1];
								showTooltip(item.pageX, item.pageY, item.series.label + " on " + convertTimestamp(x) + " is " + y);
							}
							} else {
							$("#tooltip").remove();
							previousPoint = null;
						}
					});
					
					togglePlot = function(seriesIdx)
					{
						var someData = plot.getData();  
						someData[seriesIdx].lines.show = !someData[seriesIdx].lines.show;  
						if (!someData[seriesIdx].lines.show){
							someData[seriesIdx].tempData = someData[seriesIdx].data;
							someData[seriesIdx].data = [];
						}
						else
						{
							someData[seriesIdx].data = someData[seriesIdx].tempData;
						}
						plot.setData(someData);
						plot.setupGrid();
						plot.draw();
					}
					
					
					function convertTimestamp(timestamp) {
						var nextDay = new Date(timestamp); // 86400000 - one day in ms 
						var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
						'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
						
						return months[nextDay.getUTCMonth()] + ' ' + nextDay.getUTCDate();
					}
				}
				
				//graph
				chart2();
				
			}
			
		};
		
	}();
	
	
	function chart1(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "/day";
		} else {
			window.location = "/messages/index/all/day";
		}
	}
	
	function chart2(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "/week";
		} else {
			window.location = "/messages/index/all/week";
		}
	}
	
	function chart3(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "/month";
		} else {
			window.location = "/messages/index/all/month";
		}
	}
	
	function chart4(){
		var href= window.location.href;
		if(href.indexOf('all') === -1)
		{
			window.location = href.replace("/day", "").replace("/week", "").replace("/month", "").replace("/year", "") + "/year";
		} else {
			window.location = "/messages/index/all/year";
		}
	}
</script>
<!-- CSS -->
<style type="text/css">
	#legendContainer {
	background-color: #fff;
	padding: 2px;
	margin-bottom: 8px;
	border-radius: 3px 3px 3px 3px;
	border: 1px solid #E6E6E6;
	display: inline-block;
	margin: 10px auto 20px;
	float:right;
	}
	
	#legendContainer .legendLabel {
	padding-left:5px;
	padding-right:5px;
	} 
</style>	