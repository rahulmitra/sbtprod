<?php $this->Html->addCrumb('Advertisers', $this->Html->url( null, true )); ?>
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Create Message
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				
				<!-- BEGIN FORM-->
				<form action="/wall/uploadCreative" class="dropzone" id="my-dropzone">
					<input type="hidden" value="0" name="hdnMappedId" id="hdnMappedId" />
				</form>
				
				<form action="/messages/create" id="submit_form_line_item" method="POST" class="form-horizontal">
					<div class="form-body">
						<div class="alert alert-danger display-hide">
							<button class="close" data-close="alert"></button>
							You have some form errors. Please check below.
						</div>
						<div class="alert alert-success display-hide">
							<button class="close" data-close="alert"></button>
							Your form validation is successful!
						</div>
						<h3 class="form-section">Mapping Details</h3>
						<div class="form-group">
							<label class="control-label col-md-2">Advertisers</label>
							<div class="col-md-5">
								<select id="selectAdvertisers"  class="js-advertiser-array js-states form-control" name="data[tblLineItem][li_order]">
									<option value="">--Select Advertisers--</option>
									<?php foreach ($company_name as $option){ ?>
										<option value="<?php echo $option['user_id'] ?>"><?php echo $option['CompanyName'] ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">Select Order <span class="required">
							* </span></label>
							<div class="col-md-5">
								<select id="selectIO"   class="js-io-array js-states form-control" name="data[tblLineItem][li_order]">
									<option value="">--Select IO--</option>
								</select>
							</div> 
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">Select Line Item <span class="required">
							* </span></label>
							<div class="col-md-5">
								<div class="input-icon right">
									<select id="selectLineItem" class="js-line-item-array js-states form-control" name="data[tblLineItem][li_order]">
										<option value="">--Select Line Item--</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">Select Ad Unit<span class="required">
							* </span></label>
							<div class="col-md-5">
								<div class="input-icon right">
									<select id="selectAdUnit" class="js-adunit-array js-states form-control" name="data[tblMailing][m_ad_uid]">
										<option value="">--Select Ad Unit--</option>
									</select>
								</div>
							</div>
						</div>
						<h3 class="form-section">Message Details</h3>
						
						
						<div class="form-group">
							<label class="control-label col-md-2">Subject Line<span class="required">
							* </span></label>
							<div class="col-md-5">
								<div class="input-icon right">
									<input type="text" class="form-control" name="data[tblMailing][m_sl]" id="txtSubjectLine" placeholder="Enter the Subject Line here" />
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-2">Send Date<span class="required">
							* </span></label>
							<div class="col-md-2">
								<div class="input-group">
									<input type="text" name="data[tblMailing][m_schedule_date]" value="" id="dt1" placeholder="Select Date" class="form-control actual_range" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">
									<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-offset-6 col-md-6">
										<button type="submit" class="btn green">Submit</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT INNER -->	
<style>
	.select2-container-multi .select2-choices .select2-search-choice {
	margin : 6px 0 3px 5px !important;
	}
	.select2-container--default .select2-selection--multiple .select2-selection__choice
	{
	padding: 5px 5px;
	}
</style>		
<script>
	function calculate(){
		var e = document.getElementById("costStructure");
		if(e.selectedIndex != -1){
			var strUser = e.options[e.selectedIndex].value;
			if(strUser != 5)
			{
				var rate = document.getElementById("txtRate").value;
				var txtQuantity = document.getElementById("txtQuantity").value;
				if(rate.replace('$','').replace(' ','') != '' && txtQuantity.replace(',','') != '')
				{
					document.getElementById("txtCalculate").value = parseFloat(rate.replace('$','').replace(' ','')) * parseInt(txtQuantity.replace(',',''));
				}
				} else {
				var rate = document.getElementById("txtRate").value;
				var txtQuantity = document.getElementById("txtQuantity").value;
				if(rate.replace('$','').replace(' ','') != '' && txtQuantity.replace(',','') != '')
				{
					document.getElementById("txtCalculate").value = parseFloat(rate.replace('$','').replace(' ','')) * (parseInt(txtQuantity.replace(',','')) / 1000);
				}
			}
			} else {
			document.getElementById("txtCalculate").value = 0;
		}
	}
</script>																	