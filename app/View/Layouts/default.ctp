<?php
	/**
		* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
		* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
		*
		* Licensed under The MIT License
		* For full copyright and license information, please see the LICENSE.txt
		* Redistributions of files must retain the above copyright notice.
		*
		* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Layouts
		* @since         CakePHP(tm) v 0.10.0.1076
		* @license       http://www.opensource.org/licenses/mit-license.php MIT License
	*/
	$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
	$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title><?php echo $this->fetch('title'); ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<meta content="" name="description"/>
		<meta content="" name="author"/>
		<?php echo $this->element('header');?>		
	</head>
	<!-- BEGIN BODY -->
	<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
	<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
	<body>
		<!-- BEGIN HEADER -->
		<div class="page-header">
			<!-- BEGIN HEADER TOP -->
			<div class="page-header-top">
				<div class="container-fluid">
					<!-- BEGIN LOGO -->
					<div class="page-logo">
						 <?php echo $this->Html->link($this->Html->image('das-logo.png', array('escape' => false,'class' => 'logo-default', 'style'=>'margin-top:20px;')), Router::url('/', true),array('escape' => false));?>
					</div>
					<!-- END LOGO -->
					<!-- BEGIN RESPONSIVE MENU TOGGLER -->
					<a href="javascript:;" class="menu-toggler"></a>
					<!-- END RESPONSIVE MENU TOGGLER -->
					<!-- BEGIN TOP NAVIGATION MENU -->
					<?php echo $this->element('topmenu');?>	
				</div>
			</div>
			<!-- END HEADER TOP -->
			<!-- BEGIN HEADER MENU -->
			<div class="page-header-menu">
				<div class="container-fluid">
					<!-- BEGIN HEADER SEARCH BOX -->
					<?php  $userId = $this->Authake->getUserId();  	
					if(!empty($userId)){
								if($userId == 16) { 
								}else{ ?>
							<form class="search-form" action="extra_search.html" method="GET">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search" name="query">
									<span class="input-group-btn">
										<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
									</span>
								</div>
							</form>
						<?php }
					} ?>
					<!-- END HEADER SEARCH BOX -->
					<!-- BEGIN MEGA MENU -->
					<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
					<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
					<?php	
					if(!empty($userId)){
						if($userId == 16) { 
							echo $this->element('Menunew');
						}else{  
							echo $this->element('Menunew2');  // here menu shift in element
						} 
					} ?>
					<!-- END MEGA MENU -->
				</div>
			</div>
			<!-- END HEADER MENU -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN PAGE CONTAINER -->
		<div class="page-container">
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<div class="container-fluid">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1><?php echo $this->fetch('title'); ?></h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->
					<?php echo $this->element('toolbar'); ?>
					<!-- END PAGE TOOLBAR -->
				
				</div>
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE CONTENT -->
			<div class="page-content">
				<div class="container-fluid">
					<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
					<?php // echo $this->element('model_diolog');?>
					<!-- /.modal -->
					<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
					<!-- BEGIN PAGE CONTENT INNER -->
					<?php echo $this->Session->flash(); ?>
					<div class="jsalertmsg"></div>
					<?php echo $this->element('breadcrumbs'); ?>
					<?php echo $this->fetch('content'); ?>
					<!-- END PAGE CONTENT INNER -->
				</div>
			</div>
			<!-- END PAGE CONTENT -->
		</div>		
		<?php echo $this->element('footer');?>		
	</body>
</html>
<?php //echo $this->element('sql_dump'); ?>
