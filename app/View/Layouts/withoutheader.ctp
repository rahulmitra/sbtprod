<?php
	/**
		* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
		* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
		*
		* Licensed under The MIT License
		* For full copyright and license information, please see the LICENSE.txt
		* Redistributions of files must retain the above copyright notice.
		*
		* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Layouts
		* @since         CakePHP(tm) v 0.10.0.1076
		* @license       http://www.opensource.org/licenses/mit-license.php MIT License
	*/
	
	$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
	$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $this->fetch('title'); ?>
		</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<meta content="" name="description"/>
		<meta content="" name="author"/>
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
		<link href="/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
		<link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
		
		<?php if($this->params['action'] == "add_line_item" || $this->params['action'] == "edit_line_item") {
				echo $this->Html->script('/plugins/ckeditor/vendor/ckeditor/ckeditor.js');
			}?>
		<!-- END GLOBAL MANDATORY STYLES -->
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->Html->css('components-rounded');
			echo $this->Html->css('plugins');
			echo $this->Html->css('layout');
			echo $this->Html->css('default');
			echo $this->Html->css('custom');
			
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
		<?php if(($this->params['controller'] == "company" && $this->params['action'] == "index") || ($this->params['controller'] == "leads" && $this->params['action'] == "index") || ($this->params['controller'] == "settings" && $this->params['action'] == "index")  || ($this->params['controller'] == "publishers" && $this->params['action'] == "index") || ($this->params['controller'] == "publishers" && $this->params['action'] == "index") || ($this->params['controller'] == "newsletter" && $this->params['action'] == "view_subscribers") || ($this->params['controller'] == "orders" && $this->params['action'] == "index"))
			{ ?>
			<!-- BEGIN PAGE LEVEL STYLES -->
			<link rel="stylesheet" type="text/css" href="/plugins/select2/select2.css"/>
			<link rel="stylesheet" type="text/css" href="/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
			<!-- END PAGE LEVEL STYLES -->
		<?php } ?>
		
		<?php if(($this->params['controller'] == "inventory_management" && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit"))|| ($this->params['controller'] == "esp" && $this->params['action'] == "report")) { ?>
			<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
			<link rel="stylesheet" type="text/css" href="/plugins/jquery-multi-select/css/multi-select.css"/>
		<?php } ?>
		
		<?php if($this->params['controller'] == "settings" && $this->params['action'] == "index") { ?>
			<link href="/css/pricing-table.css" rel="stylesheet" type="text/css"/>
		<?php } ?>
		
		<?php if(($this->params['controller'] == "company" && ($this->params['action'] == "add" || $this->params['action'] == "edit" )) || ($this->params['controller'] == "publishers" && $this->params['action'] == "add"))
			{ ?>
			<!-- BEGIN PAGE LEVEL STYLES -->
			<link rel="stylesheet" type="text/css" href="/plugins/jquery-multi-select/css/multi-select.css"/>
			<link rel="stylesheet" type="text/css" href="/plugins/icheck/skins/all.css"/>
			<link rel="stylesheet" type="text/css" href="/plugins/select2/select2country.css"/>
			<link rel="stylesheet" type="text/css" href="/plugins/bootstrap-datepicker/css/datepicker.css"/>
			<link rel="stylesheet" type="text/css" href="/plugins/typeahead/typeahead.css"/>
			<!-- END PAGE LEVEL STYLES -->
		<?php } ?>
		<?php if(in_array($this->params['controller'],array("insertionorder","leads")) && (
			in_array($this->params['action'],array("add_line_item","edit_line_item" ,"add" ,"add_line_item_flight" ,"edit_line_item_flight","reConciliation","upload_creative_text_html","edit_upload_creative_text_html","index" ))));
			{ ?>
			<!-- BEGIN PAGE LEVEL STYLES -->
			<link rel="stylesheet" type="text/css" href="/plugins/jquery-multi-select/css/multi-select.css"/>
			<link rel="stylesheet" type="text/css" href="/plugins/icheck/skins/all.css"/>
			<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
			<?php 
				$datepickercss= "/plugins/bootstrap-datepicker/css/datepicker.css";
				if($this->params['controller'] =="leads") $datepickercss= "/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css";?>
			
			<link rel="stylesheet" type="text/css" href="<?php echo $datepickercss;?>"/>
			<!-- END PAGE LEVEL STYLES -->
		<?php } ?>
		
		<?php if($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item_flight" || $this->params['action'] == "edit_line_item_flight" )) { ?>
			
			<link rel="stylesheet" type="text/css" href="/plugins/querybuilder/query-builder.default.min.css"/>
		<?php } ?>
		
		
		<?php if($this->params['controller'] == "newsletter" && $this->params['action'] == "upload_subscribers") { ?>
			<!-- BEGIN PAGE LEVEL STYLES -->
			<link href="/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet"/>
			<link href="/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet"/>
			<link href="/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
			<!-- END PAGE LEVEL STYLES -->	
		<?php } ?>
		
		<?php if(in_array($this->params['controller'],array("insertionorder","leads","newsletter")) && in_array($this->params['action'],array("upload_creative","upload_subscribers","reConciliation","upload_creative_text_html","edit_upload_creative_text_html"))) { ?>
			<!-- BEGIN PAGE LEVEL STYLES -->
			<link href="/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet"/>
			<link href="/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet"/>
			<link href="/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
			<!-- END PAGE LEVEL STYLES -->	
		<?php } ?>
		
		<?php if($this->params['controller'] == "wall" && $this->params['action'] == "index") { ?>
			<link rel="stylesheet" type="text/css" href="/plugins/jquery-multi-select/css/multi-select.css"/>
			<link href="/plugins/dropzone/css/dropzone.css" rel="stylesheet"/>
			<link href="/css/timeline.css" rel="stylesheet" type="text/css"/>
			<link href="/css/timeline2.css" rel="stylesheet" type="text/css"/>
			<link href="/css/fb-buttons.css" rel="stylesheet" type="text/css"/>
			<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
		<?php } ?>
		
		<?php if(($this->params['controller'] == "messages" ||$this->params['controller'] == "leads" || $this->params['controller'] == "insertionorder" || $this->params['controller'] == "orders") && ($this->params['action'] == "create" || $this->params['action'] == "edit_upload_creative_text_html"||$this->params['action'] == "upload_creative_text_html"|| $this->params['action'] == "view_line_creatives"|| $this->params['action'] == "reConciliation")) { ?>
			<link rel="stylesheet" type="text/css" href="/plugins/jquery-multi-select/css/multi-select.css"/>
			<link href="/plugins/dropzone/css/dropzone.css" rel="stylesheet"/>
			<link href="/css/timeline.css" rel="stylesheet" type="text/css"/>
			<link rel="stylesheet" type="text/css" href="/plugins/bootstrap-datepicker/css/datepicker.css"/>
			<link href="/css/timeline2.css" rel="stylesheet" type="text/css"/>
			<link href="/css/fb-buttons.css" rel="stylesheet" type="text/css"/>
			<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
		<?php } ?>
		
		<?php if(($this->params['controller'] == "inventory_management" && $this->params['action'] == "add_coreg_placement") || ($this->params['controller'] == "inventory_management" && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit"))) { ?>
			<link rel="stylesheet" type="text/css" href="/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
		<?php } ?>
		
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		
	</head>
	<!-- BEGIN BODY -->
	<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
	<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
	<body>
		<!-- BEGIN HEADER -->
	 	<div class="page-container">
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<div class="container-fluid">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1><?php echo $this->fetch('title'); ?></h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->
					<div class="page-toolbar" style="display:none;">
						<!-- BEGIN THEME PANEL -->
						<div class="btn-group btn-theme-panel">
							<a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
								<i class="icon-settings"></i>
							</a>
							<div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<h3>THEME COLORS</h3>
										<div class="row">
											<div class="col-md-6 col-sm-6 col-xs-12">
												<ul class="theme-colors">
													<li class="theme-color theme-color-default" data-theme="default">
														<span class="theme-color-view"></span>
														<span class="theme-color-name">Default</span>
													</li>
													<li class="theme-color theme-color-blue-hoki" data-theme="blue-hoki">
														<span class="theme-color-view"></span>
														<span class="theme-color-name">Blue Hoki</span>
													</li>
													<li class="theme-color theme-color-blue-steel" data-theme="blue-steel">
														<span class="theme-color-view"></span>
														<span class="theme-color-name">Blue Steel</span>
													</li>
													<li class="theme-color theme-color-yellow-orange" data-theme="yellow-orange">
														<span class="theme-color-view"></span>
														<span class="theme-color-name">Orange</span>
													</li>
													<li class="theme-color theme-color-yellow-crusta" data-theme="yellow-crusta">
														<span class="theme-color-view"></span>
														<span class="theme-color-name">Yellow Crusta</span>
													</li>
												</ul>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<ul class="theme-colors">
													<li class="theme-color theme-color-green-haze" data-theme="green-haze">
														<span class="theme-color-view"></span>
														<span class="theme-color-name">Green Haze</span>
													</li>
													<li class="theme-color theme-color-red-sunglo" data-theme="red-sunglo">
														<span class="theme-color-view"></span>
														<span class="theme-color-name">Red Sunglo</span>
													</li>
													<li class="theme-color theme-color-red-intense" data-theme="red-intense">
														<span class="theme-color-view"></span>
														<span class="theme-color-name">Red Intense</span>
													</li>
													<li class="theme-color theme-color-purple-plum" data-theme="purple-plum">
														<span class="theme-color-view"></span>
														<span class="theme-color-name">Purple Plum</span>
													</li>
													<li class="theme-color theme-color-purple-studio" data-theme="purple-studio">
														<span class="theme-color-view"></span>
														<span class="theme-color-name">Purple Studio</span>
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 seperator">
										<h3>LAYOUT</h3>
										<ul class="theme-settings">
											<li>
												Theme Style
												<select class="theme-setting theme-setting-style form-control input-sm input-small input-inline tooltips" data-original-title="Change theme style" data-container="body" data-placement="left">
													<option value="boxed" selected="selected">Square corners</option>
													<option value="rounded">Rounded corners</option>
												</select>
											</li>
											<li>
												Layout
												<select class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips" data-original-title="Change layout type" data-container="body" data-placement="left">
													<option value="boxed">Boxed</option>
													<option value="fluid"  selected="selected">Fluid</option>
												</select>
											</li>
											<li>
												Top Menu Style
												<select class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change top menu dropdowns style" data-container="body" data-placement="left">
													<option value="dark" selected="selected">Dark</option>
													<option value="light">Light</option>
												</select>
											</li>
											<li>
												Top Menu Mode
												<select class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) top menu" data-container="body" data-placement="left">
													<option value="fixed">Fixed</option>
													<option value="not-fixed" selected="selected">Not Fixed</option>
												</select>
											</li>
											<li>
												Mega Menu Style
												<select class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change mega menu dropdowns style" data-container="body" data-placement="left">
													<option value="dark" selected="selected">Dark</option>
													<option value="light">Light</option>
												</select>
											</li>
											<li>
												Mega Menu Mode
												<select class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) mega menu" data-container="body" data-placement="left">
													<option value="fixed" selected="selected">Fixed</option>
													<option value="not-fixed">Not Fixed</option>
												</select>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!-- END THEME PANEL -->
					</div>
					<!-- END PAGE TOOLBAR -->
				</div>
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE CONTENT -->
			<div class="page-content">
				<div class="container-fluid">
					<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
					<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Modal title</h4>
								</div>
								<div class="modal-body">
									Widget settings form goes here
								</div>
								<div class="modal-footer">
									<button type="button" class="btn blue">Save changes</button>
									<button type="button" class="btn default" data-dismiss="modal">Close</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
					
					<!-- BEGIN PAGE CONTENT INNER -->
					<?php echo $this->Session->flash(); ?>
					<div class="jsalertmsg"></div>
					<?php echo $this->element('breadcrumbs'); ?>
					<?php echo $this->fetch('content'); ?>
					<!-- END PAGE CONTENT INNER -->
				</div>
			</div>
			<!-- END PAGE CONTENT -->
		</div>
		<!-- END PAGE CONTAINER -->
		<!-- BEGIN PRE-FOOTER -->
	 	<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
		</div>
		<!-- END FOOTER -->
		<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
		<!-- BEGIN CORE PLUGINS -->
		<!--[if lt IE 9]>
			<script src="/plugins/respond.min.js"></script>
			<script src="/plugins/excanvas.min.js"></script> 
		<![endif]-->
		<script src="/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="/plugins/jquery-migrate.min.js" type="text/javascript"></script>
		<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
		<script src="/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
		<script src="/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
		<script src="/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="/plugins/jquery.cokie.min.js" type="text/javascript"></script>
		<script src="/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
		<!-- END CORE PLUGINS -->
		<?php if(($this->params['controller'] == "company" && $this->params['action'] == "index") || ($this->params['controller'] == "settings" && $this->params['action'] == "index") || ($this->params['controller'] == "publishers" && $this->params['action'] == "index") || ($this->params['controller'] == "insertionorder" && $this->params['action'] == "index") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "index")  || ($this->params['controller'] == "newsletter" && $this->params['action'] == "view_subscribers"))
			{ ?>
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script type="text/javascript" src="/plugins/select2/select2country.min.js"></script>
			<script type="text/javascript" src="/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
			<script type="text/javascript" src="/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
			<script src="/scripts/table-managed.js"></script>
			<!-- END PAGE LEVEL PLUGINS -->
		<?php } ?>
		
		<?php if(($this->params['controller'] == "company" && ($this->params['action'] == "add" || $this->params['action'] == "edit" )) || ($this->params['controller'] == "publishers" && $this->params['action'] == "add"))
			{ ?>
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script type="text/javascript" src="/plugins/jquery-validation/js/jquery.validate.min.js"></script>
			<script type="text/javascript" src="/plugins/jquery-validation/js/additional-methods.min.js"></script>
			<script type="text/javascript" src="/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
			<script type="text/javascript" src="/plugins/typeahead/typeahead.bundle.js"></script>
			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script type="text/javascript" src="/plugins/select2/select2country.min.js"></script>
			<script src="/scripts/form-wizard.js"></script>
			<!-- END PAGE LEVEL PLUGINS -->
		<?php } ?>
		<?php if($this->params['controller'] == "inventory_management" && $this->params['action'] == "add_coreg_placement") { ?>
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script type="text/javascript" src="/plugins/jquery-validation/js/jquery.validate.min.js"></script>
			<script type="text/javascript" src="/plugins/jquery-validation/js/additional-methods.min.js"></script>
			<script type="text/javascript" src="/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
			<script type="text/javascript" src="/plugins/typeahead/typeahead.bundle.js"></script>
			<script type="text/javascript" src="/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script type="text/javascript" src="/plugins/select2/select2country.min.js"></script>
			<script src="/scripts/form_wizard_coreg.js"></script>
		<?php } ?>
		<?php if(($this->params['controller'] == "newsletter" && $this->params['action'] == "view_stats") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_email") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_stats") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_lead_gen_stats") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_cpm_stats")  || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_leadgen")  || ($this->params['controller'] == "messages" && $this->params['action'] == "index")
		) { ?>
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="/plugins/flot/jquery.flot.min.js"></script>
			<script src="/plugins/flot/jquery.flot.resize.min.js"></script>
			<script src="/plugins/flot/jquery.flot.time.min.js"></script>
			<script src="/plugins/flot/jquery.flot.stack.min.js"></script>
			<script src="/plugins/flot/jquery.flot.crosshair.min.js"></script>
			<script src="/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
			<!-- END PAGE LEVEL PLUGINS -->
		<?php	} ?>
		
		<?php if(($this->params['controller'] == "newsletter" && $this->params['action'] == "upload_subscribers") || ($this->params['controller'] == "insertionorder" && $this->params['action'] == "upload_creative")) { ?>
			<!-- BEGIN:File Upload Plugin JS files-->
			<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
			<script src="/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
			<!-- The Templates plugin is included to render the upload/download listings -->
			<script src="/plugins/jquery-file-upload/js/vendor/tmpl.min.js"></script>
			<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
			<script src="/plugins/jquery-file-upload/js/vendor/load-image.min.js"></script>
			<!-- blueimp Gallery script -->
			<script src="/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js"></script>
			<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
			<script src="/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
			<!-- The basic File Upload plugin -->
			<script src="/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
			<!-- The File Upload processing plugin -->
			<script src="/plugins/jquery-file-upload/js/jquery.fileupload-process.js"></script>
			<!-- The File Upload image preview & resize plugin -->
			<script src="/plugins/jquery-file-upload/js/jquery.fileupload-image.js"></script>
			<!-- The File Upload validation plugin -->
			<script src="/plugins/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
			<!-- The File Upload user interface plugin -->
			<script src="/plugins/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
			<!-- The main application script -->
			<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
			<!--[if (gte IE 8)&(lt IE 10)]>
				<script src="/plugins/jquery-file-upload/js/cors/jquery.xdr-transport.js"></script>
			<![endif]-->
			<!-- END:File Upload Plugin JS files-->
			<script src="/scripts/form-fileupload.js"></script>
		<?php } ?>
		
		<?php if(($this->params['controller'] == "inventory_management" || $this->params['controller'] == "esp") && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit" || $this->params['action'] == "report")) { ?>
			 <script src="/plugins/jquery.slug.js"></script>
			<script type="text/javascript"> 
				$(document).ready(function(){ $("#title").slug(); });
				 $(window).load(function(){ 
					 $('#ad_isExternalNo').click();
				 }); 
			</script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
			<script type="text/javascript" src="/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
			<script type="text/javascript" src="/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
			
		<?php } ?>
		
		
		<?php if(in_array($this->params['controller'],array("insertionorder" ,"leads")) && in_array($this->params['action'],array("add_line_item","edit_line_item","add","add_line_item_flight","edit_line_item_flight","reConciliation","index"))) {  ?>
			
		 
			<script type="text/javascript" src="/plugins/jquery-validation/js/jquery.validate.min.js"></script>
			<script type="text/javascript" src="/plugins/jquery-validation/js/additional-methods.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
			<script type="text/javascript" src="/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
				
			<?php if($this->params['controller']!="leads"){?>
				<script type="text/javascript" src="/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
			<?php }else{?>
				
			 <script type="text/javascript" src="/plugins/bootstrap-daterangepicker/moment.min.js"></script>
			<?php }?>
				
			<script src="/plugins/icheck/icheck.min.js"></script>
			<script src="/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
			<script src="/plugins/quicksearch-master/jquery.quicksearch.js"></script>
			<script src="/scripts/form-validation.js"></script>
		<?php } ?>
	 
			
		<?php if($this->params['controller'] == "wall" && $this->params['action'] == "index") { ?>
			<script src="/plugins/wall/wall.js"></script>
			<script src="/plugins/dropzone/dropzone.js"></script>
			<script src="/plugins/justgage/justgage.js"></script>
			<script src="/plugins/justgage/raphael-2.1.4.min.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
			<script type="text/javascript" src="/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
			<script src="/scripts/form-dropzone.js"></script>
			
		<?php } ?>
		
		
		<?php if(($this->params['controller'] == "messages" || $this->params['controller'] == "insertionorder"|| $this->params['controller'] == "orders") && ($this->params['action'] == "create" || $this->params['action'] == "upload_creative_text_html"|| $this->params['action'] == "edit_upload_creative_text_html"|| $this->params['action'] == "view_line_creatives")) { ?>
			<script src="/plugins/wall/wall.js"></script> 
			
			<script src="/plugins/dropzone/dropzone.js"></script>
			<script type="text/javascript" src="/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
			<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
			<script type="text/javascript" src="/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
			<script src="/scripts/form-dropzone-message.js"></script>
		<?php } ?>
		
		<?php if(($this->params['controller'] == "orders" && $this->params['action'] == "index")|| ($this->params['controller'] == "leads" && $this->params['action'] == "index")) { ?>
			<script type="text/javascript" src="/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
			<script type="text/javascript" src="/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
			<script src="/scripts/table-managed.js"></script>
			<script src="/plugins/justgage/justgage.js"></script>
			<script src="/plugins/justgage/raphael-2.1.4.min.js"></script>
		<?php } ?>
		<?php if(($this->params['controller'] == "messages" && $this->params['action'] == "index") || ($this->params['controller'] == "newsletter" && $this->params['action'] == "view_stats")  || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_email") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_stats") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_cpm_stats")  || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_lead_gen_stats") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_leadgen"))
		 { ?>
			<script src="/plugins/justgage/justgage.js"></script>
			<script src="/plugins/justgage/raphael-2.1.4.min.js"></script>
		<?php } ?>
		<?php if(($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_user_profile") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_user_profile")) { ?>
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
			<script src="/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
			<script src="/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
			<script src="/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
			<script src="/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
			<script src="/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
			<script src="/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
			<script src="/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
			<script src="/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
			<script src="/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
			<script src="/scripts/charts-user.js"></script>
			<!-- END PAGE LEVEL PLUGINS -->
		<?php } ?>
		<?php if($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item_flight" || $this->params['action'] == "edit_line_item_flight")) { ?>
			<script src="/plugins/querybuilder/query-builder.js" type="text/javascript"></script>
		<?php } ?>
		<script src="/scripts/metronic.js" type="text/javascript"></script>
		<script src="/scripts/layout.js" type="text/javascript"></script>
		<script src="/scripts/demo.js" type="text/javascript"></script>
		
		<script>
			<?php if($this->params['controller'] == "insertionorder" && ($this->params['action'] == "upload_creative_text_html" || $this->params['action'] == "edit_upload_creative_text_html")) { ?>
			  jQuery(document).ready(function() {  
					checkMoreInfo('<?php echo $creatives_type_id;?>');
				});
			<?php } ?>
			
			
			function ShowSubCategory(value){
				
				if(!value){
					alert('Please select category');
					return false;
				}
				$.ajax({
					type: "POST",
					url: "/insertionorder/getcategory_ajax/"+value,
					dataType: "json",
					success: function(data){
						$(".js-example-data-array-subcategories").empty();
						$(".js-example-data-array-subcategories").select2({
							data: data
						});
						
					}
				});
			}
			jQuery(document).ready(function() {    
				
				<?php if(in_array($this->params['controller'],array("insertionorder")) && in_array($this->params['action'],array("edit_upload_creative_text_html","upload_creative_text_html"))) {  
					 
					$links = (!empty($creatives['tblCreative']['link']))?json_decode($creatives['tblCreative']['link'],true):array();?>
					var next = '<?php echo  count($links) ?>';<?php 
					 if(!empty($links)){
							
							foreach($links as $key=>$link){
								if($key == 0) {continue;}?>
								 next++;
								$(".addmorelink").append('<div class=""><input name="data[tblCreative][link]['+next+']" class="form-control" style="float:left;width:83%;margin-right:10px;margin-bottom:10px;" value="<?php echo $link?>" placeholder="Enter the link" type="text" id="tblCreativeLink"></div><a href="javascript:void(0);"  class="remCF"><button  class="btn"   type="button" style="margin-bottom:10px;">-</button></a></div>');
								$(".remCF").on('click',function(){
									
									$(this).prev().remove();
									$(this).remove();
								});	  
								<?php  
							}
					 }?>
					 	$(".add-field-more").click(function(){
						next++;
							$(".addmorelink").append('<div class=""><input style="float:left;width:83%;margin-right:10px;margin-bottom:10px;" name="data[tblCreative][link]['+next+']" class="form-control" placeholder="Enter the link" type="text" id="tblCreativeLink"></div> <a href="javascript:void(0);" class="remCF"><button  style="margin-bottom:10px;" class="btn" type="button">-</button></a></div> ');
							$(".remCF").on('click',function(){
								$(this).prev().remove();
								$(this).remove();
									
							});
						});
					
				<?php }?>
				
				
				
				<?php $addfields=configure::read('addfields');
				?>
				var next = '<?php echo  count($addfields) ?>';
				// edit page 
					<?php if(!empty($tblAdunitsFieldMapping)){
							foreach($tblAdunitsFieldMapping as $value){
								if($value['FieldMapping']['custom']==1){?>
								next++;
								$("#addmapfield").append('<tr> </td> <td> <div ><input type="checkbox" name="data[FieldMapping][status]['+next+']" checked value="1" id="check'+next+'"> <label for="check'+next+'">Yes</label></div></td><td><input type="hidden" name="data[FieldMapping][custom]['+next+']" value="1" ><input name="data[FieldMapping][field_description]['+next+']" class="form-control demo auto-w" type="text" value="<?php echo $value['FieldMapping']['field_description'];  ?>"><td>  <input name="data[FieldMapping][field_lable]['+next+']" value="<?php echo $value['FieldMapping']['field_lable'];  ?>" class="form-control demo auto-w" type="text" id="FieldMappingFieldLable">&nbsp; <a href="javascript:void(0);" class="remCF"><button class="btn" type="button">-</button></a> </td></tr>');
								$(".remCF").on('click',function(){
									$(this).parent().parent().remove();
								});	  
								<?php }
							}
					 }?>	
				
				$(".add-more").click(function(){
					next++;
					$("#addmapfield").append('<tr><td> <div class="ajaxcheckbox"><input type="checkbox" name="data[FieldMapping][status]['+next+']" checked value="1" id="check'+next+'"> <label for="check'+next+'">Yes</label></div></td><td><input type="hidden" name="data[FieldMapping][custom]['+next+']" value="1" ><input name="data[FieldMapping][field_description]['+next+']" class="form-control demo auto-w" type="text"> </td> <td><input name="data[FieldMapping][field_lable]['+next+']" class="form-control demo auto-w" type="text" id="FieldMappingFieldLable">&nbsp; <a href="javascript:void(0);" class="remCF"><button  class="btn" type="button">-</button></a> </td></tr>');
						 $(".remCF").on('click',function(){
							$(this).parent().parent().remove();
						});
				});
				
				<?php if($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item" || $this->params['action'] == "edit_line_item")) { ?>
					
					calculate();
					
					var index = '<?php echo  count(configure::read('CollectedCollect')); ?>';
					<?php if(!empty($lineItemFields)){
							
							$counter =count(configure::read('CollectedCollect'));
							foreach($lineItemFields as $value){
								if($value['LineItemField']['custom']==1){
									$counter++;
									?>
									index++;
									<?php 
									
									$selectType = (!empty($value['LineItemField']['fld_type']))?$value['LineItemField']['fld_type']:'';
									$selectbox = preg_replace( "/\r|\n/", "",$this->Form->input('LineItemField.fld_type.'.$counter, 
													array(
													'label' => false,
													'type' => 'select',
													'class' => 'js-creative-field-type-array form-control',
													'onchange'=>'ShowFieldType(this.value,'.$counter.')',
													'empty' => 'Please Select Field type',
													'selected'=>$selectType,
													'options' => configure::read('CollectFieldType'))));
													$fld_custom_texts ='';
													if(in_array($selectType,array(2,3))){
														$fld_custom_texts =	preg_replace( "/\r|\n/", "", $this->Form->input('LineItemField.fld_custom_texts.'.$counter, array('label' => false, 'value'=>(!empty($value))?$value['LineItemField']['fld_custom_texts']:'','div' => false,'class'=>'form-control demo auto-w')));
														}
								
								?>
													
													
													
													
								$("#addmapfield tr:last").before('<tr></td> <td> <div ><input type="checkbox" name="data[LineItemField][status]['+index+']" checked value="1" id="check'+index+'"> <label for="check'+index+'">Yes</label></div></td><td><input type="hidden" name="data[LineItemField][custom]['+index+']" value="1" ><input name="data[LineItemField][fld_label]['+index+']" class="form-control demo auto-w" type="text" value="<?php echo $value['LineItemField']['fld_label'];  ?>"> <td><?php echo $selectbox; ?></td><td id="showFieldTypeValue'+index+'"><?php echo $fld_custom_texts; ?> </td><td>  <input name="data[LineItemField][adv_fld_name]['+index+']" value="<?php echo $value['LineItemField']['adv_fld_name'];  ?>" class="form-control demo auto-w" type="text" id="LineItemFieldLable">&nbsp; <a href="javascript:void(0);" class="remCF"><button class="btn" type="button">-</button></a> </td></tr>');
								$(".remCF").on('click',function(){
									$(this).parent().parent().remove();
								});	  
								<?php }
							}
					 }?>	
					
					 
					$(".add-field-more").click(function(){
						index++;
						<?php
							$values = configure::read('CollectFieldType'); 
							$options = '<option value="">Please Select Field type</option>';
							if(!empty($values)){
								foreach($values as $key=>$value){
									$options = $options. '<option value="'.$key.'">'.$value.'</option>';
								}
							}
						
						?>
						$(this).parents('tr:last').before('<tr><td> <div class="ajaxcheckbox"><input type="checkbox" name="data[LineItemField][status]['+index+']" checked value="1" id="check'+index+'"> <label for="check'+index+'">Yes</label></div></td><td><input type="hidden" name="data[LineItemField][custom]['+index+']" value="1" ><input name="data[LineItemField][fld_label]['+index+']" class="form-control demo auto-w" type="text"> </td> <td><select onchange="ShowFieldType(this.value,'+index+')" class="js-creative-field-type-array form-control" name="data[LineItemField][fld_type]['+index+']" id="LineItemFieldFldType"><?php echo $options;?></select></td><td id="showFieldTypeValue'+index+'"></td><td><input name="data[LineItemField][adv_fld_name]['+index+']" class="form-control demo auto-w" type="text" id="FieldMappingFieldLable">&nbsp; <a href="javascript:void(0);" class="remCF"><button class="btn" type="button">-</button></a> </td></tr>');
						 $(".remCF").on('click',function(){
							$(this).parent().parent().remove();
						});
						
						 
				});
				
				<?php } ?>	
				
				
				
				Metronic.init(); // init metronic core components
				Layout.init(); // init current layout 
				Demo.init(); // init demo features
				<?php if(($this->params['controller'] == "company" && $this->params['action'] == "index") || ($this->params['controller'] == "settings" && $this->params['action'] == "index") || ($this->params['controller'] == "publishers" && $this->params['action'] == "index") || ($this->params['controller'] == "insertionorder" && $this->params['action'] == "index") || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "index") || ($this->params['controller'] == "newsletter" && $this->params['action'] == "view_subscribers") || ($this->params['controller'] == "orders" && $this->params['action'] == "index"))
					{ ?>
					TableManaged.init();
				<?php } ?>
				<?php if(($this->params['controller'] == "company" && ($this->params['action'] == "add" ||$this->params['action'] == "edit"  )) || ($this->params['controller'] == "publishers" && $this->params['action'] == "add"))
					{ ?>
					FormWizard.init();
					var myTypeahead = $('input.typeahead-devs').typeahead({
						name: 'data[tblProfile][CompanyName]',
						remote: '/company/getCompanyNameByQuery?q=%QUERY'
					});
				<?php } ?>
				<?php if(($this->params['controller'] == "newsletter" && $this->params['action'] == "view_stats")  || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_email") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_stats")  || ($this->params['controller'] == "orders" && $this->params['action'] == "view_cpm_stats")  || ($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_stats_leadgen") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_detail_lead_gen_stats")) { ?>
					//ChartsFlotcharts.initCharts();
					ChartsFlotcharts1.initCharts();
				<?php } ?>
				<?php if($this->params['controller'] == "messages" && $this->params['action'] == "index") { ?>
					ChartsFlotcharts.initCharts();
				<?php } ?>
				<?php if($this->params['controller'] == "newsletter" && $this->params['action'] == "upload_subscribers") { ?>
					FormFileUpload.init();
				<?php } ?>
				<?php if($this->params['controller'] == "insertionorder" && $this->params['action'] == "upload_creative") { ?>
					FormFileUploadCreative.init();
				<?php } ?>
				<?php if($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add")) { ?>
					$(".js-example-basic-single").select2({
						placeholder: "Start typing to select Advertiser Name ",
						allowClear: true
					});
					
					$(".js-example-basic-single").on("change", function (e) {
						//alert($(this).val());
						$.ajax({
							type: "POST",
							url: "/insertionorder/getContactInfoCompany",
							data:'company_id='+$(this).val(),
							dataType: "json",
							success: function(data){
								$('input[name="data[User][ContactName]"]').val(data.name);
								$('input[name="data[User][email]"]').val(data.email);
							}
						});
					});
					
					<?php if(!empty($company_id)) { ?>
						$('.js-example-basic-single').trigger("change");
					<?php } ?>
					
					FormValidation.init();
				<?php } ?>	
				<?php if($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item" || $this->params['action'] == "edit_line_item")) { ?>
					
					$("#costStructure").select2({
						maximumSelectionLength: 1,
						placeholder: "Start Typing to Select ONLY ONE "
					});
					$("#costStructure").on("change", function (e) {
						calculate();
					});
					$("#renFrequency").select2({
						maximumSelectionLength: 1,
						placeholder: "Select One "
					});
					$(".js-example-data-array").select2({
						placeholder: "Select (or start typing) the order name for the above selected advertiser"
					});
					$(".js-example-data-array-categories").select2({
						placeholder: "Select (or start typing) the category for the line item"
					});
					$("#txtRate").inputmask();
					$("#txtQuantity").inputmask();
					$("#txtCalculate").inputmask();
					$("#txtDaily").inputmask();
					$("#txtweekly").inputmask();
					$("#txtMonthly").inputmask();
					$('.event_period').datepicker({
						inputs: $('.actual_range'),					 
						autoclose: true,
						todayHighlight: true,
						startDate: '+0d'
						
						
					}); 
					$(document).on('click', '.input-group-btn', function(){
					 $(this).datepicker({
						 inputs: $('.actual_range'),					 
							autoclose: true,
							todayHighlight: true,
							startDate: '+0d'});
					});
					
					/*$(".event_period_button").click(function() {
						 $('.event_period').datepicker("show");
					});*/
					
					$('#adunit-select').multiSelect({
						selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search publisher or ad unit'>",
						selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search publisher or ad unit'>",
						afterInit: function(ms){
							var that = this,
							$selectableSearch = that.$selectableUl.prev(),
							$selectionSearch = that.$selectionUl.prev(),
							selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
							selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
							
							that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
							.on('keydown', function(e){
								if (e.which === 40){
									that.$selectableUl.focus();
									return false;
								}
							});
							
							that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
							.on('keydown', function(e){
								if (e.which == 40){
									that.$selectionUl.focus();
									return false;
								}
							});
						},
						afterSelect: function(){
							this.qs1.cache();
							this.qs2.cache();
						},
						afterDeselect: function(){
							this.qs1.cache();
							this.qs2.cache();
						}
					});
					// 04-05-16
					$('#adunit-Creative-type').multiSelect({
						selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search creative'>",
						selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search creative'>",
						afterInit: function(ms){
							var that = this,
							$selectableSearch = that.$selectableUl.prev(),
							$selectionSearch = that.$selectionUl.prev(),
							selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
							selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
							
							that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
							.on('keydown', function(e){
								if (e.which === 40){
									that.$selectableUl.focus();
									return false;
								}
							});
							
							that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
							.on('keydown', function(e){
								if (e.which == 40){
									that.$selectionUl.focus();
									return false;
								}
							});
						},
						afterSelect: function(){
							this.qs1.cache();
							this.qs2.cache();
						},
						afterDeselect: function(){
							this.qs1.cache();
							this.qs2.cache();
						}
					});
					
					//04-05-16	
					
					
					$('#geography-select').multiSelect({
						selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search location'>",
						selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search location'>",
						afterInit: function(ms){
							var that = this,
							$selectableSearch = that.$selectableUl.prev(),
							$selectionSearch = that.$selectionUl.prev(),
							selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
							selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
							
							that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
							.on('keydown', function(e){
								if (e.which === 40){
									that.$selectableUl.focus();
									return false;
								}
							});
							
							that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
							.on('keydown', function(e){
								if (e.which == 40){
									that.$selectionUl.focus();
									return false;
								}
							});
						},
						afterSelect: function(){
							this.qs1.cache();
							this.qs2.cache();
						},
						afterDeselect: function(){
							this.qs1.cache();
							this.qs2.cache();
						}
					});
					
					$('#audience-select').multiSelect({
						selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search location'>",
						selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Start typing to search location'>",
						afterInit: function(ms){
							var that = this,
							$selectableSearch = that.$selectableUl.prev(),
							$selectionSearch = that.$selectionUl.prev(),
							selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
							selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
							
							that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
							.on('keydown', function(e){
								if (e.which === 40){
									that.$selectableUl.focus();
									return false;
								}
							});
							
							that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
							.on('keydown', function(e){
								if (e.which == 40){
									that.$selectionUl.focus();
									return false;
								}
							});
						},
						afterSelect: function(){
							this.qs1.cache();
							this.qs2.cache();
						},
						afterDeselect: function(){
							this.qs1.cache();
							this.qs2.cache();
						}
					});
					
					$('#audience-select').change(function(){
						var count = 0; 
						var to = 0;
						$('#audience-select > option:selected').each(function() {
							var text = $(this).text();
							var splitted = text.split(" (");
							//var splitted = text.substring(text.lastIndexOf("("));
							var value = splitted[1].replace(")","");
							count = +count + +value;
							to++;
						});
						var result = (count * 40) / 100;
						if(to == 1)
						$('#total').html("(Available Emails : " + (count).toFixed() + ")");
						else
						$('#total').html("(Available Emails : " + (count - result).toFixed() + ")");
					});
					
					$('input[name="productTypes[]"]').on('ifChanged', function(event){
						var favorite = [];
						var selectedItem = [];
						$.each($("input[name='productTypes[]']:checked"), function(){            
							favorite.push($(this).val());
						});
						$('#adunit-select :selected').each(function(i, selected){ 
							selectedItem[i] = $(selected).val(); 
						});
						//alert("My favourite sports are: " + selectedItem.join(", "));
						$("#adunit-select").empty();
						$('#adunit-select').multiSelect('refresh');
						$.ajax({
							type: "POST",
							url: "/insertionorder/getAdUnitsByMediaType",
							data:'media_id='+favorite.join(","),
							dataType: "json",
							success: function(data){
								var count = 0;
								$.each(data, function(index) {
									$('#adunit-select').multiSelect('addOption', { value: data[index].id, text: data[index].text, index: count });
									count++;
								});
								$('#adunit-select').multiSelect('refresh');
								$('#adunit-select').multiSelect('select', selectedItem);
							}
						});
					});
					FormValidation.init();
				<?php } ?>
				<?php if($this->params['controller'] == "insertionorder" && ($this->params['action'] == "add_line_item_flight" || $this->params['action'] == "edit_line_item_flight")) { ?>
					<?php echo $this->element('default_line_item_flight_js'); ?>	
				<?php } ?>
				<?php if($this->params['controller'] == "insertionorder" && $this->params['action'] == "edit_line_item") { ?>
					var favorite = [];
					$.each($("input[name='productTypes[]']:checked"), function(){            
						favorite.push($(this).val());
					});
					$('#adunit-select').multiSelect('refresh');
					//alert("My favourite sports are: " + favorite.join(", "));
					$.ajax({
						type: "POST",
						url: "/insertionorder/getAdUnitsByMediaType",
						data:'media_id='+favorite.join(","),
						dataType: "json",
						success: function(data){
							var count = 0;
							$.each(data, function(index) {
								$('#adunit-select').multiSelect('addOption', { value: data[index].id, text: data[index].text, index: count });
								count++;
							});
							$('#adunit-select').multiSelect('refresh');
						}
					});
				<?php } ?>
				
				<?php if($this->params['controller'] == "wall" && $this->params['action'] == "index") {
				?>
				
				$("#socialShareWith").select2({
					placeholder: "Who should see this?",
					allowClear: true
				});
				
				$(".js-advertiser-array").select2({
					placeholder: "Start typing to select Advertiser Name ",
					allowClear: true
				});
				
				$(".js-line-item-array").select2({
					placeholder: "Start typing to select Line Item",
					allowClear: true
				});
				
				$(".js-io-array").select2({
					placeholder: "Select (or start typing) the order name for the above selected advertiser",
					allowClear: true
				});
				
				$(".js-adunit-array").select2({
					placeholder: "Start typing to select Ad Unit",
					allowClear: true
				});
				
				$(".js-advertiser-array").on("change", function (e) {
					//alert($(this).val());
					$('.js-io-array').empty();
					$.ajax({
						type: "POST",
						url: "/wall/getOrderByAdvertiser",
						data:'advertiser_id='+$(this).val(),
						dataType: "json",
						success: function(data){
							$(".js-io-array").select2({
								data: data
							});
						}
					});
				});
				
				$(".js-io-array").on("change", function (e) {
					//alert($(this).val());
					$('.js-line-item-array').empty();
					$.ajax({
						type: "POST",
						url: "/wall/getLineItems",
						data:'order_id='+$(this).val(),
						dataType: "json",
						success: function(data){
							$(".js-line-item-array").select2({
								data: data
							});
						}
					});
				});
				
				$(".js-line-item-array").on("change", function (e) {
					//alert($(this).val());
					$('.js-adunit-array').empty();
					$.ajax({
						type: "POST",
						url: "/wall/view_mapped_adunits",
						data:'line_item_id='+$(this).val(),
						dataType: "json",
						success: function(data){
							$(".js-adunit-array").select2({
								data: data
							});
						}
					});
				});
				
				$(".js-adunit-array").on("change", function (e) {
					$("#hdnMappedId").val($(this).val());
				});
				
				$("#LineItemWithCreative").select2({
					placeholder: "Select Line Item"
				});
				
				$('#tabs div:first').show();
				$('#tabs ul li:first').addClass('active');
				$('#tabs ul li a').click(function(){ 
					$('#tabs ul li').removeClass('active');
					$(this).parent().addClass('active'); 
					var currentTab = $(this).attr('href'); 
					$('#tabs div').hide();
					$('.dz-default').show();
					$(currentTab).show();
					return false;
				});
				FormDropzone.init();
				<?php } ?>
				
				<?php if(($this->params['controller'] == "orders" || $this->params['controller'] == "insertionorder" ) && ($this->params['action'] == "upload_creative_text_html"||$this->params['action'] == "edit_upload_creative_text_html"||$this->params['action'] == "view_line_creatives")) { ?>
					$(".js-creative-array").select2({
						placeholder: "Start typing to select creative type ",
						allowClear: true
					});
					$(".js-creative-image-array").select2({
						placeholder: "Start typing to select image type ",
						allowClear: true
					});
					$(".js-creative-status-array").select2({
						placeholder: "Start typing to select status ",
						allowClear: true
					});
					 
					 $(window).load(function(){ $('.select2-container--default').css("width", "100%");})
					 FormDropzone.init();
				<?php } ?>	
				
				<?php if($this->params['controller'] == "leads" && ($this->params['action'] == "reConciliation" || $this->params['action'] == "index")) { ?>
					<?php if($this->params['action']=="index"){?>
						$('input[name="data[Lead][daterange]"]').daterangepicker();
						
						$(function() {

						function cb(start, end) {
							$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
						}
						cb(moment().subtract(29, 'days'), moment());

						$('input[name="data[Lead][daterange]"]').daterangepicker({
							ranges: {
							   'Today': [moment(), moment()],
							   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
							   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
							   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
							   'This Month': [moment().startOf('month'), moment().endOf('month')],
							   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
							}
						}, cb);

					});
						
						
						
						
					<?php }?>
					
					$(".onlynumber").inputmask();
					$(".js-order-array").select2({
						placeholder: "Select (or start typing) the order name",
						//allowClear: true
					});
					$(".js-company-array").select2({
						placeholder: "Select (or start typing) the company name",
						//allowClear: true
					});
					$(".js-line-array").select2({
						placeholder: "Select (or start typing) the line item name",
						//allowClear: true
					});
					
					
					
					$('#LeadCompanyId').change(function(){
						
						// Remove all Manage Lead Allocation
						var favorite = [];
						var selectedItem = [];
						 $.each($("#LeadCompanyId :selected"), function(){
							favorite.push($(this).val());
						});
						$("#LeadOrderId").empty();
						$(".js-order-array").select2({
							placeholder: "Select (or start typing) the order name",
							allowClear: true
						}); 
						
						var selecid = favorite.join(",");
						
						 //alert("My favourite sports are: " + favorite.join(", "));
						$.ajax({
							type: "POST",
							url: "/leads/getAllOrderList",
							data:'user_id='+favorite.join(","),
							dataType: "json",
							success: function(data){
								var count = 0;
								 
								$("#LeadOrderId").select2({
									data: data
								});
								$(".js-order-array").select2({
									placeholder: "Select (or start typing) the order name",
									//allowClear: true
								}); 
								 
							}
						});
						
					});
					$('#LeadOrderId').change(function(){
						
						// Remove all Manage Lead Allocation
						var favorite = [];
						var selectedItem = [];
						 $.each($("#LeadOrderId :selected"), function(){
							favorite.push($(this).val());
						});
						$("#LeadLineItem").empty();
						$(".js-line-array").select2({
							placeholder: "Select (or start typing) the line item name ",
							//allowClear: true
						}); 
						var selecid = favorite.join(",");
						
						 //alert("My favourite sports are: " + favorite.join(", "));
						$.ajax({
							type: "POST",
							url: "/leads/getAllLineItemByOrder",
							data:'order_id='+favorite.join(","),
							dataType: "json",
							success: function(data){
								var count = 0;
								 
								$("#LeadLineItem").select2({
									data: data
								});
								$(".js-line-array").select2({
									placeholder: "Select (or start typing) the line item name ",
									allowClear: true
								}); 
								
								 
							}
						});
						
					});
					
					
				<?php }?>
				<?php if($this->params['controller'] == "messages" && $this->params['action'] == "create") { ?>
					$(".js-advertiser-array").select2({
						placeholder: "Start typing to select Advertiser Name ",
						allowClear: true
					});
					
					$(".js-line-item-array").select2({
						placeholder: "Start typing to select Line Item",
						allowClear: true
					});
					
					$(".js-io-array").select2({
						placeholder: "Select (or start typing) the order name for the above selected advertiser",
						allowClear: true
					});
					
					$(".js-adunit-array").select2({
						placeholder: "Start typing to select Ad Unit",
						allowClear: true
					});
					
					$(".js-advertiser-array").on("change", function (e) {
						//alert($(this).val());
						$('.js-io-array').empty();
						$.ajax({
							type: "POST",
							url: "/wall/getOrderByAdvertiser",
							data:'advertiser_id='+$(this).val(),
							dataType: "json",
							success: function(data){
								$(".js-io-array").select2({
									data: data
								});
							}
						});
					});
					
					$(".js-io-array").on("change", function (e) {
						//alert($(this).val());
						$('.js-line-item-array').empty();
						$.ajax({
							type: "POST",
							url: "/wall/getLineItems",
							data:'order_id='+$(this).val(),
							dataType: "json",
							success: function(data){
								$(".js-line-item-array").select2({
									data: data
								});
							}
						});
					});
					
					$(".js-line-item-array").on("change", function (e) {
						//alert($(this).val());
						$('.js-adunit-array').empty();
						$.ajax({
							type: "POST",
							url: "/wall/view_mapped_adunits",
							data:'line_item_id='+$(this).val(),
							dataType: "json",
							success: function(data){
								$(".js-adunit-array").select2({
									data: data
								});
							}
						});
					});
					 
					
					FormDropzone.init();
					$('#dt1').datepicker({
					});
				<?php } ?>
				<?php if(($this->params['controller'] == "inventory_management" && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit"))|| ($this->params['controller'] == "esp" && $this->params['action'] == "report")) { ?>
					// for report
						$(".js-profile-array").select2({
							placeholder: "Select the publisher name"
						});
						$(".js-adunit-array").select2({
							placeholder: "Select adunit name"
						});
						$(".js-mail-array").select2({
							placeholder: "Select mail name "
						});
						
						$(".js-profile-array").on("change", function (e) {
						$('.js-mail-array').empty();
						$('.js-adunit-array').empty();
						
						$.ajax({
							type: "POST",
							url: "/esp/getAdunits",
							data:'publisher_id='+$(this).val(),
							dataType: "json",
							success: function(data){
								$(".js-adunit-array").select2({
									data: data
								});
							}
						});
					});
						$(".js-adunit-array").on("change", function (e) {
						//alert($(this).val());
						 
						$('.js-mail-array').empty();
						$.ajax({
							type: "POST",
							url: "/esp/getMails",
							data:'adunit_id='+$(this).val(),
							dataType: "json",
							success: function(data){
								$(".js-mail-array").select2({
									data: data
									});
								}
							});
						});
						
					
					// close for report 
					$(".js-example-basic-single").select2({
						placeholder: "Start typing to select ESP Name ",
						allowClear: true
					});
					
					$(".js-esp-list-array").select2({
						placeholder: "Select the ESP provider first"
					});
					
					$(".js-comapny-array").select2({
						placeholder: "Select the ESP provider first"
					});
					$(".js-product-array").select2({
						placeholder: "Select the product type"
					});
					$(".js-placement-array").select2({
						placeholder: "Select the placement type"
					});
					$(".js-unit_type-array").select2({
						placeholder: "Select the unit type"
					});
					
					$('#cp3').colorpicker();
					$('#cp4').colorpicker();
					$('#cp5').colorpicker();
					$('#cp6').colorpicker();
					
					
					$(".js-example-basic-single").on("change", function (e) {
						//alert($(this).val());
						$('.js-esp-list-array').empty();
						if($(this).val() == 1)
						{
							$.ajax({
								type: "POST",
								url: "/inventory_management/getLyrisList",
								dataType: "json",
								success: function(data){
									$(".js-esp-list-array").select2({
										data: data
									});
								}
								 
							});
						} else if($(this).val() == 2)
						{
							$.ajax({
								type: "POST",
								url: "/inventory_management/getSocketLabList",
								dataType: "json",
								success: function(data){
									$(".js-esp-list-array").select2({
										data: data
									});
									
								}
							});
							} else if($(this).val() == 3)
						{
							$.ajax({
								type: "POST",
								url: "/inventory_management/getMailchimpList",
								dataType: "json",
								success: function(data){
									$(".js-esp-list-array").select2({
										data: data
									});
									
								}
								
								//$('js-esp-list-array').val('225791');
							});
							
							} else {
							$('.js-esp-list-array').empty();
						}
						$(".js-esp-list-array").select2().select2('val','225791');
						
					});
					
					$(".js-product-array").on("change", function (e) {
						//alert($(this).val());
						$('.js-placement-array').empty();
						if($(this).val() == 6)
						{
							$.ajax({
								type: "POST",
								url: "/inventory_management/getPlacementList",
								dataType: "json",
								success: function(data){
									$(".js-placement-array").select2({
										data: data
									});
								}
								 
							});
							
							$('.js-unit_type-array').empty();
						
							$.ajax({
								type: "POST",
								url: "/inventory_management/getUnitList",
								dataType: "json",
								success: function(data){
									$(".js-unit_type-array").select2({
										data: data
									});
								}
								 
							});
						}else {
							$('.js-placement-array').empty();
							$('.js-unit_type-array').empty();
						}
						$(".js-placement-array").select2().select2('val','225791');	
						$(".js-unit_type-array").select2().select2('val','225792');	
						
					});
					
					$('#productType').change(function() {
					if($("#productType").val() !="6"){ 
						
							jQuery('#placement').hide("slow");
							jQuery('#ad_adtype_id').hide("slow");
							 }
					
						if($("#productType").val()==="6"){ 
						    
							 jQuery('#placement').show("slow");
							 jQuery('#ad_adtype_id').show("slow"); 
							}
							
						});
						
						$('#tblAdunitAdPtypeId').change(function() {
					if($("#tblAdunitAdPtypeId").val() !="6"){ 
						
							jQuery('#placement').hide("slow");
							jQuery('#ad_adtype_id').hide("slow");
							 }
					
						if($("#tblAdunitAdPtypeId").val()==="6"){ 
						    
							 jQuery('#placement').show("slow");
							 jQuery('#ad_adtype_id').show("slow"); 
							}
							
						});
					
					
				<?php } ?>
				<?php if(($this->params['controller'] == "inventory_management" && $this->params['action'] == "view_user_profile") || ($this->params['controller'] == "orders" && $this->params['action'] == "view_user_profile")) { ?>
					ChartsAmcharts.init();
				<?php } ?>
				
				<?php if($this->params['controller'] == "inventory_management" && ($this->params['action'] == "addAdUnit" || $this->params['action'] == "editAdUnit")) { ?>
					$('#datacenter-select').multiSelect();
				<?php } ?>
				// edit upload  creative
				
				<?php if($this->params['controller'] == "insertionorder" && ( $this->params['action'] == "edit_upload_creative_text_html")) { 
					echo 'checkMoreInfo("'.$creatives['tblCreative']['creatives_type'].'")';
				  } ?>
				
				
				});
				
				<?php if($this->params['controller'] == "leads" && $this->params['action'] == "reConciliation" ) { ?>
					$(".LeadLineId").select2({
						placeholder: "Start typing to select order ",
						allowClear: true
					});
					function ShowOrderItem(option){	
					if(!option){
						alert('Please select order ');
						return false;
					}
					$('#LeadLineId').empty();
					 
						$.ajax({
							type: "GET",
							url: "/leads/order_line_item/"+option,
							dataType: "json",
							success: function(data){
								$("#LeadLineId").select2({
									data: data
								});
							}
						});
					}
						
				<?php }?>
				<?php if($this->params['controller'] == "insertionorder" && $this->params['action'] == "add" ) { ?>
					function checkRevenueType(option){	
					$('#adAutoAllCompany').empty();
					$('#adAutoAllCompany').select2({
						placeholder: "Start typing to select Advertiser Name ",
						allowClear: true
					});
						$.ajax({
							type: "GET",
							url: "/insertionorder/inhousecompany/"+option,
							dataType: "json",
							success: function(data){
								$("#adAutoAllCompany").select2({
									data: data
								});
							}
						});
					}
						
				<?php }?>
				<?php if($this->params['action'] == "add_line_item" || $this->params['action'] == "edit_line_item" ) { ?>
				jQuery(document).ready(function() { 
					
					jQuery('input[name="data[tblLineItem][li_is_scrub]"]').on('ifUnchecked', function(event){
					  jQuery('#ScrubAcceptanceDiv').hide("slow");
					  
					  
					});
					jQuery('input[name="data[tblLineItem][li_is_scrub]"]').on('ifChecked', function(event){
					  jQuery('#ScrubAcceptanceDiv').show("slow");
					  jQuery('#txtScrubRate').focus();				  
					  
					});
					
					jQuery('input[name="data[AutoResponder][is_autoresp]"]').on('ifUnchecked', function(event){
					  jQuery('#AutoResponder').hide("slow");
					});
					jQuery('input[name="data[AutoResponder][is_autoresp]"]').on('ifChecked', function(event){
					  jQuery('#AutoResponder').show("slow");
					});
 
					 
				});
				 
			<?php 	} ?>
		</script>
		
		<?php //echo $this->params['controller']; ?>
		<?php //echo $this->params['action']; ?>
		
		<?php //echo $this->element('sql_dump'); ?>
		
		<!-- scripts_for_layout -->
		<?php echo $scripts_for_layout; ?>
		<!-- Js writeBuffer -->
		<?php
			if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) echo $this->Js->writeBuffer();
			// Writes cached scripts
		?>
		<script type="text/javascript" src="/js/jquery.csv-0.71.min.js"></script>
		<script>
			function switchPanel() {
				$.ajax({
					url : '/Cron/switchPanel',
					type: 'POST',
					success : function(response){
						document.location.href="/";
					}
				});
			}
		</script>
		<!-- END JAVASCRIPTS -->
	</body>
</html>

<?php if($this->params['controller'] == "leads" && $this->params['action'] == "index" ) { ?>
<script type="text/javascript" src="/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<?php }?>
