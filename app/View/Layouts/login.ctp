
<?php
	/**
		* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
		* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
		*
		* Licensed under The MIT License
		* For full copyright and license information, please see the LICENSE.txt
		* Redistributions of files must retain the above copyright notice.
		*
		* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Layouts
		* @since         CakePHP(tm) v 0.10.0.1076
		* @license       http://www.opensource.org/licenses/mit-license.php MIT License
	*/
	
	$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
	$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $this->fetch('title'); ?>
		</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<meta content="" name="description"/>
		<meta content="" name="author"/>
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
		<link href="/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
		<link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
		
		<!-- BEGIN PAGE LEVEL STYLES -->
		<link href="/css/login.css" rel="stylesheet" type="text/css"/>
		<!-- END PAGE LEVEL SCRIPTS -->

		<!-- END GLOBAL MANDATORY STYLES -->
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->Html->css('components-rounded');
			echo $this->Html->css('plugins');
			echo $this->Html->css('layout');
			echo $this->Html->css('default');
			echo $this->Html->css('custom');
			
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
	</head>
	<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">

</div>
<!-- END LOGO -->
<!-- BEGIN PAGE CONTENT INNER -->
			<?php echo $this->Session->flash(); ?>
				
			<?php echo $this->fetch('content'); ?>
			<!-- END PAGE CONTENT INNER -->
			
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/plugins/respond.min.js"></script>
<script src="/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/scripts/metronic.js" type="text/javascript"></script>
<script src="/scripts/layout.js" type="text/javascript"></script>
<script src="/scripts/demo.js" type="text/javascript"></script>
<script src="/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Login.init();
	Demo.init();
});
</script>
<!-- END JAVASCRIPTS -->
</body>
</html>
