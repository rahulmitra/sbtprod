<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	<li  class="active">
		<a href="/wall/">Wall</a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE CONTENT INNER -->
		<div class="portlet light">
			<div class="portlet-body form">
				<div id="updateboxarea">
					
					<h3 class="form-section">Post on you Wall</h3>
					<form method="post" action="" class="form-horizontal">
						<div class="form-body">
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Your form validation is successful!
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="unit" id="tabs">
										<ul class="actions">
											<li><a href="#tabs-1"><i class="icon icon-status"></i>Status</a></li>
											<li><a href="#tabs-2"><i class="icon icon-photo"></i>Upload Creative</a></li>
											<li><a href="#tabs-3"><i class="icon icon-video"></i>Upload IO</a></li>
											<li style="float:right;">
												
											</li>
											
										</ul>
										<span class="ajax_indi"><img src="/img/loader.gif"></span>
										<!-- Units -->
										
										<div class="actionUnits" id="tabs-1">
											<form id="npost" name="npost">
												<p class="formUnit" id="Status"> <i class="active"></i>
													<textarea name="message" placeholder="What's on your mind?" id="update" cols="30" rows="5"></textarea>
													<ol class="controls clearfix">
														<li>
															<select multiple="multiple" name="data[tblLineItem][li_cs_id]" id="socialShareWith" class="form-control">
																<?php foreach ($connectedUsers as $options ){ ?>
																	<option value='<?php echo $options['User']['id']; ?>'><?php echo $options['User']['ContactName']; ?> </option>
																<?php } ?>
															</select>
														</li>
														<li class="post">
															<input class="uibutton confirm fb_submit" type="button" id="update_button" title="npost" value="Post">
														</li>
													</ol>
												</p>
											</form>
										</div>
										<div style="display:none;" class="actionUnits" id="tabs-2">
											<p class="formUnit"> <i class="active_pic"></i>
												<input type="text" name="txtSubjectLine" style="width:100%" id="txtSubjectLine" placeholder="Enter the Subject Line here" />
												<span id="statuss"></span>
												<input type="hidden" name="pic_url" id="pic_url" />
												<form action="/wall/uploadCreative" class="dropzone" id="my-dropzone">
													<input type="hidden" value="0" name="hdnMappedId" id="hdnMappedId" />
												</form>
												
												<ol class="controls clearfix">
													<li>
														<select id="selectAdvertisers"  style="width:200px;" class="js-advertiser-array js-states form-control" name="data[tblLineItem][li_order]">
															<option value="0">--Select Advertisers--</option>
															<?php foreach ($company_name as $option){ ?>
																<option value="<?php echo $option['user_id'] ?>"><?php echo $option['CompanyName'] ?></option>
															<?php } ?>
														</select>
													</li>
													<li>
														<select id="selectIO"  style="width:200px;"  class="js-io-array js-states form-control" name="data[tblLineItem][li_order]">
															<option value="0">--Select IO--</option>
														</select>
													</li>
													<li>
														<select id="selectLineItem" style="width:200px;" class="js-line-item-array js-states form-control" name="data[tblLineItem][li_order]">
															<option value="0">--Select Line Item--</option>
														</select>
													</li>
													<li>
														<select id="selectAdUnit" style="width:200px;" class="js-adunit-array js-states form-control" name="data[tblLineItem][li_order]">
															<option value="0">--Select Ad Unit--</option>
														</select>
													</li>
													<li class="post">
														<input class="uibutton confirm fb_submit"  type="button" value="Post" id="upload_c_button" title="picpost">
													</li>
												</ol>
											</p>
											<p id="files"></p>
											
										</div>
										<div  style="display:none;" class="actionUnits" id="tabs-3">
											<form id="vidpost" name="vidpost">
												<p class="formUnit" id="Status"> <i class="active_vid"></i>
													<textarea name="message" placeholder="Video Description" id="vmessage" cols="30" rows="5"></textarea>
													<input type="text" name="y_link" style="width:100%" id="y_link" placeholder="Enter Youtube Url - www.youtube.com/watch?v=rdmycu13Png">
													<ol class="controls clearfix">
														<li class="post">
															<input class="uibutton confirm fb_submit" type="button" value="Post" title="vidpost">
														</li>
													</ol>
												</p>
											</form>
										</div>
										<!-- / Units -->
									</div>
								</div>
							</div>
						</div>
					</form>
					
				</div>
				<div id='flashmessage'>
					<div id="flash" align="left"  ></div>
				</div>
				<div class="timeline"  id="content">
					<?php
						foreach($updatesarray as $data)
						{
							$msg_id=$data['tblSocialMessage']['msg_id'];
							$orimessage=$data['tblSocialMessage']['message'];
							$message= $data['tblSocialMessage']['message'];
							$time=$data['tblSocialMessage']['created'];
							$msg_type=$data['tblSocialMessage']['msg_type'];
							$username=$data['User']['ContactName'];
							$uid=$data['tblSocialMessage']['uid_fk'];
							$email=$data['User']['email'];
							$face=$this->Gravatar->get_gravatar($email,41,'','',false);
							//$commentsarray=$Wall->Comments($msg_id);
						?>
						
						<?php if($msg_type == "3") { 
							$original_array=unserialize($message);
							$sent = $original_array['sent'];
							$unsubscribes = $original_array['unsubscribes'];
							$opens = $original_array['opens'];
							$clicks = $original_array['clicks-unique'];
							$screenshot = $original_array['screenshot'];
						?>
						<!-- TIMELINE ITEM -->
						<div class="timeline-item" id="stbody<?php echo $msg_id;?>">
							<div class="timeline-badge">
								<img alt="" class="timeline-badge-userpic" src="<?php echo $face;?>" />
							</div>
							<div class="timeline-body">
								<div class="timeline-body-arrow">
								</div>
								<div class="timeline-body-head">
									<div class="timeline-body-head-caption">
										<a href="#" class="timeline-body-title font-blue-madison"><?php echo $username;?></a>
										<span class="timeline-body-time font-grey-cascade"><?php $this->Gravatar->time_stamp($time);?></span>  | <a href='#' class='commentopen' id='<?php echo $msg_id;?>' title='Comment'>Comment </a>
									</div>
									<?php if($ownerId == $uid) { ?>
										<div class="timeline-body-head-actions">
											<div class="btn-group">
												<a class="stdelete" href="#" id="<?php echo $msg_id;?>" title="Delete update">X</a>
											</div>
										</div>
									<?php } ?>
								</div>
								<div class="timeline-body-content row">
									<div class="col-md-6">
										<b>Here's the Report of the Forbes Blast</b><br /><br />
										<img src="<?php echo $screenshot;?>" />
									</div>
									<div class="col-md-6" align="center">
										<div id="g1"></div>
										<div id="g2"></div>
										<div id="g3"></div>
										<div id="g4"></div>
									</div>
									<div id="stexpandbox">
										<div id="stexpand<?php echo $msg_id;?>"></div>
									</div>
									
									<div class="commentcontainer" id="commentload<?php echo $msg_id;?>">
										<?php
											//Srinivas Tamada http://9lessons.info
											//Loading Comments link with load_updates.php 
											$commentsarray = $comments[$msg_id];
											foreach($commentsarray as $cdata)
											{
												$com_id=$cdata['tblSocialComment']['com_id'];
												$comment=$this->Gravatar->tolink(htmlentities($cdata['tblSocialComment']['comment'] ));
												$time=$cdata['tblSocialComment']['created'];
												$username=$cdata['User']['ContactName'];
												$uid=$cdata['tblSocialComment']['uid_fk'];
												$email=$data['User']['email'];
												$cface=$this->Gravatar->get_gravatar($email,41,'','',false);
											?>
											<div class="stcommentbody" id="stcommentbody<?php echo $com_id; ?>">
												<div class="stcommentimg">
													<img src="<?php echo $cface; ?>" class='small_face'/>
												</div> 
												<div class="stcommenttext">
													<?php if($ownerId == $uid) { ?>
														<a class="stcommentdelete" href="#" id='<?php echo $com_id; ?>' title='Delete Comment'>X</a>
													<?php } ?>
													<b><?php echo $username; ?></b> <?php echo $comment; ?>
													<div class="stcommenttime"><?php $this->Gravatar->time_stamp($time); ?></div> 
												</div>
											</div>
											<?php 
											}
										?>
									</div>
									<div class="commentupdate" style='display:none' id='commentbox<?php echo $msg_id;?>'>
										<div class="stcommentimg">
											<img src="<?php echo $face;?>" class='small_face'/>
										</div> 
										<div class="stcommenttext" >
											<form method="post" action="">
												
												<textarea name="comment" class="comment" maxlength="200"  id="ctextarea<?php echo $msg_id;?>"></textarea>
												<br />
												<input type="submit"  value=" Comment "  id="<?php echo $msg_id;?>" class="comment_button"/>
											</form>
											
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<script>
							var g1, g2, g3, g4;
							
							window.onload = function(){
								var g1 = new JustGage({
									id: "g1",
									value: <?php echo $sent; ?>,
									min: 0,
									max: <?php echo $sent; ?>,
									title: "Sent / Delivered",
									label: "Quantity"
								});
								
								var g2 = new JustGage({
									id: "g2",
									value: <?php echo $opens; ?>,
									min: 0,
									max: <?php echo intval($sent / 10) ; ?>,
									title: "Opens",
									label: "Count"
								});
								
								var g3 = new JustGage({
									id: "g3",
									value: <?php echo $clicks; ?>,
									min: 0,
									max: <?php echo intval($sent / 20) ; ?>,
									title: "Clicks",
									label: "Count"
								});
								
								var g4 = new JustGage({
									id: "g4",
									value: <?php echo $unsubscribes; ?>,
									min: 0,
									max: <?php echo intval($sent / 100) ; ?>,
									title: "Unsubscribes",
									label: "Count"
								});
							};							
						</script>
						<!-- END TIMELINE ITEM -->
						<?php
							
						} else { ?>
						
						<!-- TIMELINE ITEM -->
						<div class="timeline-item" id="stbody<?php echo $msg_id;?>">
							<div class="timeline-badge">
								<img alt="" class="timeline-badge-userpic" src="<?php echo $face;?>" />
							</div>
							<div class="timeline-body">
								<div class="timeline-body-arrow">
								</div>
								<div class="timeline-body-head">
									<div class="timeline-body-head-caption">
										<a href="#" class="timeline-body-title font-blue-madison"><?php echo $username;?></a>
										<span class="timeline-body-time font-grey-cascade"><?php $this->Gravatar->time_stamp($time);?></span>  | <a href='#' class='commentopen' id='<?php echo $msg_id;?>' title='Comment'>Comment </a>
									</div>
									<?php if($ownerId == $uid) { ?>
										<div class="timeline-body-head-actions">
											<div class="btn-group">
												<a class="stdelete" href="#" id="<?php echo $msg_id;?>" title="Delete update">X</a>
											</div>
										</div>
									<?php } ?>
								</div>
								<div class="timeline-body-content">
									
									<?php echo html_entity_decode($message);?>
									<div id="stexpandbox">
										<div id="stexpand<?php echo $msg_id;?>"></div>
									</div>
									
									<div class="commentcontainer" id="commentload<?php echo $msg_id;?>">
										<?php
											//Srinivas Tamada http://9lessons.info
											//Loading Comments link with load_updates.php 
											$commentsarray = $comments[$msg_id];
											foreach($commentsarray as $cdata)
											{
												$com_id=$cdata['tblSocialComment']['com_id'];
												$comment=$this->Gravatar->tolink(htmlentities($cdata['tblSocialComment']['comment'] ));
												$time=$cdata['tblSocialComment']['created'];
												$username=$cdata['User']['ContactName'];
												$uid=$cdata['tblSocialComment']['uid_fk'];
												$email=$data['User']['email'];
												$cface=$this->Gravatar->get_gravatar($email,41,'','',false);
											?>
											<div class="stcommentbody" id="stcommentbody<?php echo $com_id; ?>">
												<div class="stcommentimg">
													<img src="<?php echo $cface; ?>" class='small_face'/>
												</div> 
												<div class="stcommenttext">
													<?php if($ownerId == $uid) { ?>
														<a class="stcommentdelete" href="#" id='<?php echo $com_id; ?>' title='Delete Comment'>X</a>
													<?php } ?>
													<b><?php echo $username; ?></b> <?php echo $comment; ?>
													<div class="stcommenttime"><?php $this->Gravatar->time_stamp($time); ?></div> 
												</div>
											</div>
											<?php 
											}
										?>
									</div>
									<div class="commentupdate" style='display:none' id='commentbox<?php echo $msg_id;?>'>
										<div class="stcommentimg">
											<img src="<?php echo $face;?>" class='small_face'/>
										</div> 
										<div class="stcommenttext" >
											<form method="post" action="">
												
												<textarea name="comment" class="comment" maxlength="200"  id="ctextarea<?php echo $msg_id;?>"></textarea>
												<br />
												<input type="submit"  value=" Comment "  id="<?php echo $msg_id;?>" class="comment_button"/>
											</form>
											
											
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- END TIMELINE ITEM -->
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT INNER -->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<style>
	#sample_4_filter { 
	float:right;
	}
	#wall_container
	{
	text-align:left;
	background-color:#FFFFFF;
	padding:10px;
	width:550px;
	}
	#update
	{
	font-size:12px;
	}
	#content
	{
	margin-top:10px;
	}
	
	/* status body */
	
	.stbody
	{
	
	margin-bottom:10px;
	border-bottom:dashed 1px #dedede;
	overflow:auto;
	}
	.stimg
	{
	float:left;
	height:50px;
	width:50px;
	border:solid 1px #dedede;
	padding:3px;
	}
	.sttext
	{
	margin-left:70px;
	min-height:50px;
	word-wrap:break-word;
	overflow:hidden;
	padding:5px;
	display:block;
	font-size:12px;
	width:470px;
	}
	.sttext b
	{
	color:#006699;
	}
	
	.sttime
	{
	font-size:11px;
	color:#999;
	font-family:Arial, Helvetica, sans-serif;
	margin-top:5px;
	}
	.stdelete 
	{
	font-weight:bold;
	float:right;
	cursor:pointer;
	}
	#stexpandbox
	{
	margin-top:10px;
	}
	#stexpandbox img
	{
	border:solid 1px #dedede;
	padding:3px;
	}
	/* Comment */
	.stcommentbody
	{
	
	
	border-bottom:solid 1px #fff;
	background-color:#f2f2f2;
	padding:5px;
	width:400px;
	overflow:auto;
	}
	.stcommentimg
	{
	float:left;
	height:35px;
	width:35px;
	border:solid 1px #dedede;
	padding:2px;
	}
	.stcommenttext
	{
	margin-left:45px;
	min-height:40px;
	word-wrap:break-word;
	overflow:hidden;
	padding:3px;
	display:block;
	font-size:11px;
	width:350px;
	
	}
	.stcommenttext b
	{
	color:#006699;
	}
	
	.stcommenttime
	{
	font-size:11px;
	color:#999;
	font-family:Arial, Helvetica, sans-serif;
	margin-top:5px;
	}
	.stcommentdelete 
	{
	font-weight:bold;
	float:right;
	cursor:pointer;
	}
	.commentupdate
	{
	background-color:#f2f2f2;
	width:400px;
	padding:5px;
	}
	.comment
	{
	width:330px;
	height:35px;
	font-size:11px;
	}
	/* faceb image*/
	.small_face
	{
	width:35px;height:35px
	}
	.big_face
	{
	width:50px;height:50px
	}
	#flashmessage
	{
	height:15px;
	margin-top:10px;
	font-size:11px;
	color:#333;
	
	}
	
	/* Spinner */
	#Spinner {
	text-align: center;
	padding: 10px 0;
	}
	
</style>
<style>
	.select2-container-multi .select2-choices .select2-search-choice {
	margin : 6px 0 3px 5px !important;
	}
	.select2-container--default .select2-selection--multiple .select2-selection__choice
	{
	padding: 5px 5px;
	}
	
	#g1 {
	width:400px; height:320px;
	display: inline-block;
	margin: 1em;
	}
	
	#g2, #g3, #g4 {
	width:100px; height:80px;
	display: inline-block;
	margin: 1em;
	}
</style>			