<!-- TIMELINE ITEM -->
<?php
	$message = $message;
	$face = $this->Gravatar->get_gravatar($email,41,'','',false);
?>
<div class="timeline-item" id="stbody<?php echo $msg_id;?>">
	<div class="timeline-badge">
		<img alt="" class="timeline-badge-userpic" src="<?php echo $face;?>" />
	</div>
	<div class="timeline-body">
		<div class="timeline-body-arrow">
		</div>
		<div class="timeline-body-head">
			<div class="timeline-body-head-caption">
				<a href="#" class="timeline-body-title font-blue-madison"><?php echo $username;?></a>
				<span class="timeline-body-time font-grey-cascade"><?php $this->Gravatar->time_stamp($time);?></span>  | <a href='#' class='commentopen' id='<?php echo $msg_id;?>' title='Comment'>Comment </a>
			</div>
			<div class="timeline-body-head-actions">
				<div class="btn-group">
					<a class="stdelete" href="#" id="<?php echo $msg_id;?>" title="Delete update">X</a>
				</div>
			</div>
		</div>
		<div class="timeline-body-content">
			<span class="font-grey-cascade">
			<?php echo $message;?> </span>
			<div id="stexpandbox">
				<div id="stexpand<?php echo $msg_id;?>"></div>
			</div>
			
			<div class="commentcontainer" id="commentload<?php echo $msg_id;?>">
				<?php //include('load_comments.php') ?>
			</div>
			<div class="commentupdate" style='display:none' id='commentbox<?php echo $msg_id;?>'>
				<div class="stcommentimg">
					<img src="<?php echo $face;?>" class='small_face'/>
				</div> 
				<div class="stcommenttext" >
					<form method="post" action="">
						
						<textarea name="comment" class="comment" maxlength="200"  id="ctextarea<?php echo $msg_id;?>"></textarea>
						<br />
						<input type="submit"  value=" Comment "  id="<?php echo $msg_id;?>" class="comment_button"/>
					</form>
					
					
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END TIMELINE ITEM -->