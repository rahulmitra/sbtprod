<?php
App::uses('SystemComponent','Controller/Component');
class SystemHelper extends AppHelper {
	var $helpers = Array('Html','Form','Session');
	var $settings = array();
	var $view;
	public function __construct(View $view, $settings = array()) {
		$this->settings = $settings;
		//pr($this->settings);
		$this->view = $view;
		parent::__construct($view, $settings);
	}
	public function title_for_layout(){
		return (!empty($this->settings['seo']['site_title']))?$this->settings['seo']['site_title']:'';
	}
	public function meta_keyword(){
		return (!empty($this->settings['seo']['site_metakeyword']))?$this->settings['seo']['site_metakeyword']:'';
	}
	public function meta_description(){
		return (!empty($this->settings['seo']['site_metadescription']))?$this->settings['seo']['site_metadescription']:'';
	}
	public function google_analytics_code(){
		//print_r($this->settings['seo']['google_analytics_code']);die;
		return (!empty($this->settings['seo']['google_analytics_code']))?$this->settings['seo']['google_analytics_code']:'';
	}
	public function load_head(){
		
		return $this->view->element('head');
	}
	public function load_foot(){
		return $this->view->element('foot');
	}
	public function load_js($position = 'head'){
		if(empty($position)){
			$position = 'head';
		}
		$script = SystemComponent::$script_for_layout;
		if(!empty($script[$position])){
			return $this->Html->script($script[$position]);
		}
		return '';
	}
	public function load_banner($banner_image = ''){
		Configure::load('ContentManager.config', 'default', $merge = false);
		if(empty($banner_image)){
			$banner_image = $this->settings['data']['banner_image'];
		}
		return $this->view->element('banner',array('banner_image'=>$banner_image));
	}
	public function load_css($position = 'head'){
		if(empty($position)){
			$position = 'head';
		}
		$script = SystemComponent::$css_for_layout;
		
		if(!empty($script[$position])){
			return $this->Html->css($script[$position]);
		}
		return '';
	}
	public function load_scripts_block($position = 'head'){
		if(empty($position)){
			$position = 'head';
		}
		$script = SystemComponent::$scriptBlocks;
		$data = $this->Html->scriptStart(array('inline' => false));
			
		if(!empty($script[$position])){
			foreach($script[$position] as  $_script){
				$data .= $this->Html->scriptBlock($_script);
			}
		}
		$data .= $this->Html->scriptEnd();
		
		return $data;
	}
	public function load_css_block($position = 'head'){
		if(empty($position)){
			$position = 'head';
		}
		$script = SystemComponent::$cssBlocks;
		$data = "<style type=\"text/css\">";
			
		if(!empty($script[$position])){
			foreach($script[$position] as  $_script){
				$data .= $_script;
			}
		}
		$data .= "</style>";
		
		return $data;
	}
	public function load_scripts_document_block(){
		$scripts = SystemComponent::$scriptDocumentBlocks;
		$data ="";
			if(!empty($scripts)){
				$data .= "<script>$(document).ready(function () {";
				foreach($scripts as  $_script){
					$data .= $_script;
		 	}
			$data .="})</script>";
			//$data .= $this->Html->scriptEnd();
		} 
		return $data;
	}
	
	
	public function get_setting($module='',$key=''){
		$settings = $this->settings['settings'];
		if(empty($settings[$module][$key])){
			return null;
		}
		return $settings[$module][$key];
	}
	/*
	 $options['type'] = all, first, count
	 $options['fields'] 
	 $options['conditions'] 
	 $options['order'] 
	 $options['group'] 
	 */
	public function find_query($modal='',$options = array()){
		$modal = ClassRegistry::init($modal);
		return $modal->find($options['type'],$options);
	}
}

?>

