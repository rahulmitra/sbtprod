<?php
/** google chart helper 
 * @author Mohsin kabir
 * 
 */
App::import('Helper', 'Html');

class GchartHelper extends AppHelper {
    /** Create Chart 
     * $chartName used for type of chart
     * $dataHeader contain header value and option of title,hAxis,vAxis in array formate
     * $data used for chart row data in string formate eg :- ['data',1,2]
     * $element_id used for chart display on that id
     * */
    function create($chartName='AreaChart', $dataHeader = array(), $data = '', $element_id='default') { 
	?>
        <script>
			google.load("visualization", "1", {packages: ["corechart"]});
            google.setOnLoadCallback(<?php echo $element_id; ?>);
            function <?php echo $element_id; ?>() {
				var data = google.visualization.arrayToDataTable([
                    ['<?php echo implode('\',\'', $dataHeader['header']); ?>']
                      <?php echo (!empty($data))?','.$data:'';?>]);
                    
						var options = {
							<?php
								if(!empty($dataHeader['options'])){
									$title =self::ParsingData($dataHeader['options']);
									echo (!empty($title))?$title.' , ':'';
								}
								if(!empty($dataHeader['curveType'])){
									$curveType =self::ParsingData($dataHeader['curveType']);
									echo (!empty($curveType))?$curveType.' , ':'';
								}
							?>							
							hAxis: {<?php if(!empty($dataHeader['hAxis'])){
									$hAxis =self::ParsingData($dataHeader['hAxis']);
									echo (!empty($hAxis))?$hAxis:'';
								}
							 ?>},
							hAxis: {<?php if(!empty($dataHeader['vAxis'])){
									$vAxis =self::ParsingData($dataHeader['vAxis']);
									echo (!empty($vAxis))?$vAxis:'';
								}
							 ?>},
							legend: {<?php if(!empty($dataHeader['legend'])){
									$legend =self::ParsingData($dataHeader['legend']);
									echo (!empty($legend))?$legend:'';
								}
							 ?>} 
						};
			  

                var chart = new google.visualization.<?php echo $chartName; ?>(document.getElementById('<?php echo $element_id; ?>'));
                chart.draw(data, options);
            }
        </script>
	<?php   
    }
    /* ParsingData user for option parsing data {title: 'Overall Revenue Trends',  titleTextStyle: {color: '#333'}},*/
    private function ParsingData($dataHeader){
		$data ='';
		$dataOption =array();
		if(!empty($dataHeader)){
			foreach($dataHeader as $key=>$value){
				if(is_array($value)){
					$dataOption[] = $key.' : '.self::subArrayParsingData($value);	
				}else{
					$datavalue =(isset($value))?$value:'';
					$dataOption[] = $key.' : \''.$datavalue.'\'';
					 
				}
			}
		}
		
		if(!empty($dataOption)){
			$data = implode(",",$dataOption);
		}
		  
		return  $data;
	}
	/* subArrayParsingData user for option parsing data {title: 'Overall Revenue Trends',  titleTextStyle: {color: '#333'}},*/
	private function subArrayParsingData($dataHeader=array()){
		$data ='';
		$dataOption =array();
		if(!empty($dataHeader)){
			foreach($dataHeader as $key=>$value){
				if(is_string($value)){
					$datavalue =(isset($value))?$value:'';
					$dataOption[] = $key.' : \''.$datavalue.'\'';
				}
			}
		}
		
		if(!empty($dataOption)){
			$data ='{'. implode(",",$dataOption).'}';
		}
		return $data;
	}
	 /** Create Chart 
     * $chartName used for type of chart (Not completed this funtion)
     * $dataHeader contain header value and option of title,hAxis,vAxis in array formate
     * $data used for chart row data in string formate eg :- ['data',1,2]
     * $element_id used for chart display on that id
     * */
	function Multichart($chartName='AreaChart', $dataHeader = array(), $graphData = array(), $element_id='default') { 
	?>
        <script>
			google.load('visualization', '1', {'packages':['columnchart','piechart']});
            google.setOnLoadCallback(<?php echo $element_id; ?>);
            function <?php echo $element_id; ?>() {
				//create data table object
                var dataTableMulticolumn = new google.visualization.DataTable();
                //define columns for second example
                dataTableMulticolumn.addColumn('<?php echo implode('\',\'', $dataHeader['header']); ?>');
                 <?php  
				$contentp_revenuebyisp = $bounceReport_Explode = array(); 
				if(!empty($graphData)){
					 
					foreach($graphData as $data){
					?>
					dataTableMulticolumn.addColumn('number', '<?php echo $data['tblIspList']['isp_name']?>');
					<?php 
						 $bounceReport_Explode['OPENS'][] =$data['tblIspReport']['total_opens'];
						 $bounceReport_Explode['CLICKS'][] =$data['tblIspReport']['total_clicks'];
						 $bounceReport_Explode['BOUNCES'][] =$data['tblIspReport']['total_bounces'];
					}
				}
				if(!empty($bounceReport_Explode)){
						foreach($bounceReport_Explode as $key=>$value){
							if(!empty($value)){
								$contentp_revenuebyisp[] ="['".$key."',".implode(",",$value)."]";
							}
						}
				} 
				if(!empty($contentp_revenuebyisp)){ ?>
						dataTableMulticolumn.addRows([<?php echo implode(",",$contentp_revenuebyisp);?>]);
				<?php }?>
				var options = {
							<?php
								if(!empty($dataHeader['options'])){
									$title =self::ParsingData($dataHeader['options']);
									echo (!empty($title))?$title.' , ':'';
								}
								if(!empty($dataHeader['curveType'])){
									$curveType =self::ParsingData($dataHeader['curveType']);
									echo (!empty($curveType))?$curveType.' , ':'';
								}
							?>							
							hAxis: {<?php if(!empty($dataHeader['hAxis'])){
									$hAxis =self::ParsingData($dataHeader['hAxis']);
									echo (!empty($hAxis))?$hAxis:'';
								}
							 ?>},
							hAxis: {<?php if(!empty($dataHeader['vAxis'])){
									$vAxis =self::ParsingData($dataHeader['vAxis']);
									echo (!empty($vAxis))?$vAxis:'';
								}
							 ?>},
							legend: {<?php if(!empty($dataHeader['legend'])){
									$legend =self::ParsingData($dataHeader['legend']);
									echo (!empty($legend))?$legend:'';
								}
							 ?>} 
						};
						
				 var multiColumnChart = new google.visualization.<?php echo $chartName; ?>(document.getElementById('<?php echo $element_id; ?>'));
				 multiColumnChart.draw(dataTableMulticolumn, options);
                 
            }
        </script>
	<?php   
    }
}
?>