<?php
	 
	App::import('Helper', 'Html');
	
	class FormDynamicHelper extends AppHelper {
		public $helpers = array('Html', 'Form');
		 public function FieldCreate($filed=array()){
			 
				  echo $this->customField($filed);
			   
			  
		 }
		 private function customField($filed=array()){
			 $addfields=configure::read('CollectedCollect');
			  $texthelp= $data =$require =$required ='';
			  
			  if(!empty($addfields[$filed['fld_name']])){
				  
				  if($addfields[$filed['fld_name']]['texthelp']){
					  $texthelp = $addfields[$filed['fld_name']]['texthelp'];
					}
				  
				  if($addfields[$filed['fld_name']]['required']){
					$require='<span class="required" aria-required="true" style="color:red;">* </span>';
					$required=true;
				}
			  }
			  $data ="<tr>";
			  if($filed['fld_type'] !=4){
				$data .="<td>".$filed['fld_name'].$require."</td>";
				}else{
					$data .="<td></td>"; // hide when input field is hidden
				}
			 		 if($filed['fld_type'] ==1){
						 $data .= "<td>".$this->Form->input($filed['fld_name'],array('label'=>false,'div'=>false,'required'=>$required,'class'=>'form-control'))."</td>";
					}
					 else if($filed['fld_type'] ==2){
						$options=$this->getoption($filed['fld_custom_texts']); 
						$data .= "<td>".$this->Form->input($filed['fld_name'],array('type'=>'radio','div'=>false,'legend'=>false,'radio','options'=>$options,'required'=>$required,'class'=>''))."</td>";
					}
					 else if($filed['fld_type'] ==3){
					    $options=$this->getoption($filed['fld_custom_texts']); 
					    $data .="<td>".$this->Form->input($filed['fld_name'],array('type'=>'select','label'=>false,'div'=>false,'options'=>$options,'required'=>$required,'class'=>'form-control'))."</td>";
					 }
					else if($filed['fld_type'] ==4){
						 $data .= "<td>".$this->Form->hidden($filed['fld_name'],array('class'=>'form-control','value'=>$filed['fld_custom_texts']))."</td>";
				 }else{
						$type=(!empty($addfields[$filed['fld_name']]['type']))?$addfields[$filed['fld_name']]['type']:'input';
					   $data .= "<td>".$this->Form->$type($filed['fld_name'],array('label'=>false,'div'=>false,'required'=>$required,'class'=>'form-control'))."</td>";
				 }
			  if($filed['fld_type'] !=4){
				$data .= "<td><b>".$filed['fld_label']."</b><br>".$texthelp."</td>";
				}else{
					$data .="<td></td>"; // hide when input field is hidden
				}
			 
			 $data .= "</tr>";
			 return $data;
		 }
		 function getoption($option){
			 $options=array();
			 if(!empty($option)){
				 $data = explode(';',$option);
				  if(!empty($data)){
					 foreach($data as $value){
						$options[$value]=$value;
					}
				 }else{
					 $options[$value]=$value;
				 }
				
			 }
			 return  $options;
		 }
	}
?>
