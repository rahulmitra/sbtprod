<?php
	/**
		* Get either a Gravatar URL or complete image tag for a specified email address.
		*
		* @param string $email The email address
		* @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
		* @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
		* @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
		* @param boole $img True to return a complete IMG tag False for just the URL
		* @param array $atts Optional, additional key/value attributes to include in the IMG tag
		* @return String containing either just a URL or a complete image tag
		* @source http://gravatar.com/site/implement/images/php/
	*/
	
	App::import('Helper', 'Html');
	
	class GravatarHelper extends AppHelper {
		
		function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
			$url = 'https://www.gravatar.com/avatar/';
			$url .= md5( strtolower( trim( $email ) ) );
			$url .= "?s=$s&d=$d&r=$r";
			if ( $img ) {
				$url = '<img src="' . $url . '"';
				foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
				$url .= ' />';
			}
			return $url;
		}
		
		function tolink($text){
			$text = html_entity_decode($text);
			$text = " ".$text;
			$text = eregi_replace('(((f|ht){1}tp://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)',
			'<a href="\\1">\\1</a>', $text);
			$text = eregi_replace('(((f|ht){1}tps://)[-a-zA-Z0-9@:%_\+.~#?&//=]+)',
			'<a href="\\1">\\1</a>', $text);
			$text = eregi_replace('([[:space:]()[{}])(www.[-a-zA-Z0-9@:%_\+.~#?&//=]+)',
			'\\1<a href="http://\\2">\\2</a>', $text);
			$text = eregi_replace('([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})',
			'<a href="mailto:\\1">\\1</a>', $text);
			return $text;
		}
		
		
		function time_stamp($session_time) 
		{ 
			
			$time_difference = time() - $session_time ; 
			$seconds = $time_difference ; 
			$minutes = round($time_difference / 60 );
			$hours = round($time_difference / 3600 ); 
			$days = round($time_difference / 86400 ); 
			$weeks = round($time_difference / 604800 ); 
			$months = round($time_difference / 2419200 ); 
			$years = round($time_difference / 29030400 ); 
			
			if($seconds <= 60)
			{
				echo"$seconds seconds ago"; 
			}
			else if($minutes <=60)
			{
				if($minutes==1)
				{
					echo"one minute ago"; 
				}
				else
				{
					echo"$minutes minutes ago"; 
				}
			}
			else if($hours <=24)
			{
				if($hours==1)
				{
					echo"one hour ago";
				}
				else
				{
					echo"$hours hours ago";
				}
			}
			else if($days <=7)
			{
				if($days==1)
				{
					echo"one day ago";
				}
				else
				{
					echo"$days days ago";
				}
				
				
				
			}
			else if($weeks <=4)
			{
				if($weeks==1)
				{
					echo"one week ago";
				}
				else
				{
					echo"$weeks weeks ago";
				}
			}
			else if($months <=12)
			{
				if($months==1)
				{
					echo"one month ago";
				}
				else
				{
					echo"$months months ago";
				}
				
				
			}
			
			else
			{
				if($years==1)
				{
					echo"one year ago";
				}
				else
				{
					echo"$years years ago";
				}
				
				
			}
			
			
			
		} 
	}
?>