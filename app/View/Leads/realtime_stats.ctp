<!-- BEGIN PAGE CONTENT INNER -->
<link rel="stylesheet" type="text/css" href="/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" />
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Real Time Stats</span>
				</div>
				
			</div>
            <div class="portlet-body">
                <div class="table-toolbar">
					<?php echo $this->Form->create(false, array('url' => array('controller' => 'leads', 'action' => 'realtime_stats'), 'class' => 'form-horizontal', 'id' => 'reportsForm')); ?>
                    <?php echo $this->Form->hidden('submit_value', array('value' => 'submit')); ?>
                    <div class="row">
						<div class="col-md-3">
							<div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
									echo $this->Form->input('company_id', array(
                                    'class' => 'js-company-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Select Advertiser',
                                    'options' => $companylist
									));
								?>
								
								
								
							</div>
						</div>
                        <div class="col-md-3">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
									echo $this->Form->input('order_id', array(
                                    'class' => 'js-order-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Select Orders',
                                    'options' => $orderlist
									));
								?>
								
							</div>
						</div>
						<div class="col-md-3">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php echo $this->Form->input('daterange', array('label' => false, 'div' => false, 'class' => 'form-control', 'style'=>'padding-right: 0;','placeholder' => 'Date Range')); ?> 	<span></span>
								
								
							</div>
						</div>
						
						<div class="col-md-3">
                            <div class="btn-group">
                                <button type="submit" class="btn blue btn-secondary">Filter</button> 
							</div>
						</div>
					</div>
                    <?php echo $this->Form->end(); ?>
				</div>
                
                <div class="table-responsive1">
                    <table class="table table-striped table-bordered table-hover" id="reports">
                        <thead>
                            <tr>
                                <th>
								</th>
                                <th>
                                    <?php echo 'Adunit Name/Flight Name/ Line item Pricing Model/ Line item  Status'; ?>  
									
								</th>
                                <th>
                                    <?php echo 'Start/End Date'; ?>  
								</th>
                                
                                <th>Takes</th>
                                <th>Accepted</th>

                            </tr>
                        </thead>
                        <?php
                        if (!empty($flights)) {
                            $csnameArray = array(1 => '<span style="color:red;font-weight:bold;">{CSNAME}</span>', 2 => '<span style="color:red;font-weight:bold;">{CSNAME}</span>', 3 => '<span style="color:#FF9934;font-weight:bold;">{CSNAME}</span>', 4 => '<span style="color:green;font-weight:bold;">{CSNAME}</span>', 5 => '<span style="color:#FF9934;font-weight:bold;">{CSNAME}</span>', 6 => '<span style="color:#BB99AA;font-weight:bold;">{CSNAME}</span>');
                            $statusArray = array(1 => '<span style="color:red;font-weight:bold;">{STATUS}</span>', 2 => '<a style="margin-right:5px;text-decoration:underline;font-weight:bold;" href="{LINK}">Upload Creative</a>', 3 => '<span style="color:#FF9934;font-weight:bold;">{STATUS}</span>', 4 => '<span style="color:green;font-weight:bold;">{STATUS}</span>', 5 => '<span style="color:#FF9934;font-weight:bold;">{STATUS}</span>', 6 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 7 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 8 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 9 => '<a style="margin-right:5px;text-decoration:underline;font-weight:bold;" href="{LINK}">Create Flight</a>');
                            $dateformte = Configure::read('Php.dateformte');
                            foreach ($flights as $report) {
                                ?>
                                <tr>
                                    <td>


                                    </td>
                                    <td>
                                        <div class="borderBottom"><?php echo $report['tblFlight']['fl_name']; ?></div>
                                        <div class="borderBottom"><?php echo $report['tblAdunit']['adunit_name']; ?></div>
                                        <?php
                                        $lineitemLink = '';
                                        // link of lineitem   
                                        if ($report['tcs']['cs_id'] == 3) {
                                            $lineitemLink = Router::url(array('controller' => 'orders', 'action' => 'view_detail_lead_gen_stats', $report['tblLineItem']['li_id']));
                                        } else if ($report['tcs']['cs_id'] == 5) {
                                            $lineitemLink = Router::url(array('controller' => 'orders', 'action' => 'view_cpm_stats', $report['tblLineItem']['li_id']));
                                        } else {
                                            $lineitemLink = Router::url(array('controller' => 'orders', 'action' => 'view_detail_stats', $report['tblLineItem']['li_id']));
                                        }
                                        ?>
                                        <div class="borderBottom"><?php echo $report['tblp']['CompanyName'] . ' >> ' . $report['tblLineItem']['li_name']; ?></div>
                                        <?php /*<a href="<?php echo $lineitemLink; ?>" style="margin-right:5px;">
                                            <?php
                                            echo $report['tblp']['CompanyName'] . ' >> ' . $report['tblLineItem']['li_name'];
                                            ?>
                                        </a>  */?>

                                        <?php
                                        $priceModel = '';
                                        if (array_key_exists($report['tcs']['cs_id'], $csnameArray)) {
                                            $priceModel = str_replace('{CSNAME}', $report['tcs']['cs_name'], $csnameArray[$report['tcs']['cs_id']]);
                                        } else {
                                            $priceModel = $report['tcs']['cs_name'];
                                        }
                                        ?> 
                                        <br><?php echo $priceModel; ?><br> 
                                        <?php
                                        $linkstatus = '';
                                        if ($report['tls']['ls_id'] == 2) {
                                            $linkstatus = Router::url(array('controller' => 'orders', 'action' => 'view_line_creatives', $report['tblLineItem']['li_id']));
                                        } else if ($report['tls']['ls_id'] == 9) {
                                            $linkstatus = Router::url(array('controller' => 'insertionorder', 'action' => 'add_line_item_flight', $report['tblLineItem']['li_id'], $report['tls']['ls_id']));
                                        }
                                        $stautsData = '';
                                        if (array_key_exists($report['tls']['ls_id'], $statusArray)) {
                                            $report['tls']['ls_id'];
                                            $status_span = str_replace('{STATUS}', $report['tls']['ls_status'], $statusArray[$report['tls']['ls_id']]);
                                            $stautsData = str_replace('{LINK}', $linkstatus, $status_span);
                                            // line
                                        }
                                        /*$TestLead = '';
                                        if (in_array($report['tcs']['cs_id'], array('3', '7'))) {
                                            $TestLead = '<a style="margin-right:5px;text-decoration:underline;font-weight:bold;"   href="' . Router::url(array('controller' => 'insertionorder', 'action' => 'test_lead_posting', $report['tblLineItem']['li_id'], $report['tls']['ls_id'])) . '" class="" data-toggle="modal" data-target="#myModal' . $report['tblLineItem']['li_id'] . '">Test Lead Posting</a><div id="myModal' . $report['tblLineItem']['li_id'] . '" class="modal fade">
															<div class="modal-dialog">
																<div class="modal-content">
															 </div>
															</div>
														</div>';
                                        } */
                                        ?> <?php echo $stautsData; ?><br> 
                                        <?php // echo $TestLead; ?><br> 
                                        
                                    </td>
                                    <td>
                                        <?php echo (!empty($report['tblFlight']['fl_start'])) ? $this->Time->format($dateformte, $report['tblFlight']['fl_start']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?>
                                        <br>
                                        <?php echo (!empty($report['tblFlight']['fl_end'])) ? $this->Time->format($dateformte, $report['tblFlight']['fl_end']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?>
									</td>
									
									
									
                                    <td>
										<a href="#" data-toggle="modal" data-orderid="<?php echo $report['tblLineItem']['li_order_id'] ?>" data-mapuid="<?php echo $report['tblMappedAdunit']['ma_uid'] ?>" data-aduname="<?php echo $report['tblAdunit']['adunit_name']; ?>" data-startdate="<?php echo $report['daterange']['start_date'] ?>"  data-enddate=<?php echo $report['daterange']['end_date'] ?> data-target="#myModal" class="open-AddBookDialog" > <?php echo $report['tblOrderleads']['totaltake']; ?> </a>
										
									</td>
									
                                    <td>
										<a href="#" data-toggle="modal" data-target="#myModal2" data-orderid="<?php echo $report['tblLineItem']['li_order_id'] ?>" data-mapuid="<?php echo $report['tblMappedAdunit']['ma_uid'] ?>" data-aduname="<?php echo $report['tblAdunit']['adunit_name']; ?>" data-startdate="<?php echo $report['daterange']['start_date'] ?>"  data-enddate=<?php echo $report['daterange']['end_date'] ?>  class="open-AddBookDialog2" >    <?php echo $report['tblOrderleads']['totalAccepted'];?> </a>
										
										
									</tr>
									<?php
									}
								}
							?>
							<tfoot>
								<tr>
									<td colspan="9">
										<?php if (!$flights) { ?>
											<div style='color:#FF0000'>No Record Found</div>
											<?php } else { ?>
											
											<ul class="pagination">
												
												<?php if ($this->Paginator->first()) { ?><li><?php echo $this->Paginator->first('« First', array('class' => '')); ?></li>
												<?php } ?>										
												<?php if ($this->Paginator->hasPrev()) { ?>
													<li><?php echo $this->Paginator->prev('< Previous', array('class' => ''), null, array('class' => 'disabled')); ?>&nbsp;  &nbsp;</li>
												<?php } ?>
												
												<?= $this->Paginator->numbers(array('modulus' => 6, 'tag' => 'li', 'class' => '', 'separator' => '')); ?>
												<?php if ($this->Paginator->hasNext()) { ?>
													
													<li><?php echo $this->Paginator->next('Next >', array('class' => '')); ?></li>
												<?php } ?>
												<?php if ($this->Paginator->last()) { ?>
													<li><?php echo $this->Paginator->last('Last »', array('class' => '')); ?></li>
												<?php } ?>
											</ul>
										<?php } ?>
										
										
									</td>
								</tr>
							</tfoot>
							
						</table>
					</div>
					
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>
	<div id="myModal" class="modal fade">
		
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close AddCompanyiframe" data-dismiss="modal">&times;</button>
				</div>
				 <div class="modal-body">
     
    </div>
				<div class="table-responsive1" style="padding: 20px;">
					<table table id="leads-grid"  class="table table-striped table-bordered table-hover dataTable no-footer">
						<thead>
							<tr>
						
								<th>Leads Id</th>
								<th>Email Address </th>
								<th>Ip Address</th>
							</tr>
						</thead>
						</table>
				</div>  
			</div>  
		</div>
	</div>
	<div id="myModal2" class="modal fade">
		
		<div class="modal-dialog">
				<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close AddCompanyiframe" data-dismiss="modal">&times;</button>
				</div>
				 <div class="modal-body">
     
				</div>
				<div class="table-responsive1" style="padding: 20px;">
					<table table id="leads-grid2"  class="table table-striped table-bordered table-hover dataTable no-footer">
						<thead>
							<tr>
						
								<th>Leads Id</th>
								<th>Email Address </th>
								<th>Ip Address</th>
							</tr>
						</thead>
						</table>
				</div>  
			</div>  
		</div>
	</div>
	
	
	<!-- END PAGE CONTENT INNER -->
	<style>
		#sample_4_filter { 
		float:right;
		}
	</style>
	<script>
	
	$(document).on("click", ".open-AddBookDialog", function () {
	
	
     var OrderId = $(this).data('orderid');
	 var Mapuid = $(this).data('mapuid');
	 var Startdate = $(this).data('startdate');
	 var Enddate = $(this).data('enddate');
	 var adname = $(this).data('aduname');
	  //alert(OrderId + ", " + Mapuid + ", " + Startdate + ", " + Enddate + ", " +adname);
    // $(".modal-body #bookId").val( adname );
	
	var dataTable = $('#leads-grid').DataTable( {
	
            processing: true,
			destroy: true,
			serverSide: true,
			 ajax:{
				type: "POST",
				url: "<?php echo Router::url(array('controller' => 'Leads', 'action' => 'getOrderLeadsList')); ?>",
				data:{
					order_id : OrderId,
					map_uid : Mapuid,
					start_date : Startdate,
					end_date : Enddate,
					adu_name : adname
					}
				}
			         
           
			});
		
	});

	$(document).on("click", ".open-AddBookDialog2", function () {
	
	
     var OrderId = $(this).data('orderid');
	 var Mapuid = $(this).data('mapuid');
	 var Startdate = $(this).data('startdate');
	 var Enddate = $(this).data('enddate');
	 var adname = $(this).data('aduname');
	  //alert(OrderId + ", " + Mapuid + ", " + Startdate + ", " + Enddate + ", " +adname);
    // $(".modal-body #bookId").val( adname );
	
	var dataTable = $('#leads-grid2').DataTable( {
	
            processing: true,
			destroy: true,
			serverSide: true,
			 ajax:{
				type: "POST",
				url: "<?php echo Router::url(array('controller' => 'Leads', 'action' => 'getAcceptedOrderLeadsList')); ?>",
				data:{
					order_id : OrderId,
					map_uid : Mapuid,
					start_date : Startdate,
					end_date : Enddate,
					adu_name : adname
					}
				}
			         
           
			});
		
	});

		
		jQuery(document).ready(function () {
			
			$(function () {
				var start = moment().subtract(29, 'days');
				var end = moment();
				
				function cb(start, end) {
					//$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				}
				
				
				
				cb(start, end);
				$('input[name="data[daterange]"]').daterangepicker({
					"alwaysShowCalendars": true,
					//"startDate": start,
					//"endDate": end,
					"opens": "left",
					//"autoUpdateInput": false,
					
					
					ranges: {
						'Today': [moment(), moment()],
						'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
						'Last 7 Days': [moment().subtract(6, 'days'), moment()],
						'Last 30 Days': [moment().subtract(29, 'days'), moment()],
						'This Month': [moment().startOf('month'), moment().endOf('month')],
						'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, cb);
				
			});
			
			
			
			$(".js-company-array").select2({
				placeholder: "Select Advertiser",
				maximumSelectionLength: 1,
				//allowClear: true
			});
			$(".js-order-array").select2({
				placeholder: "Select Orders",
				//allowClear: true
			});
			
			
		});
		
		$('#company_id').change(function () {
			// Remove all Manage Lead Allocation
			var favorite = [];
			var selectedItem = [];
			$.each($("#company_id :selected"), function () {
				favorite.push($(this).val());
			});
			$(".js-order-array").empty();
			$(".js-order-array").select2({
				placeholder: "Select Orders",
				allowClear: true
			});
			
			var selecid = favorite.join(",");
			
			//alert("My favourite sports are: " + favorite.join(", "));
			$.ajax({
				type: "POST",
				url: "<?php echo Router::url(array('controller' => 'orders', 'action' => 'getAllOrderList')); ?>",
				data: 'user_id=' + favorite.join(","),
				dataType: "json",
				success: function (data) {
					var count = 0;
					$(".js-order-array").select2({
						data: data
					});
					$(".js-order-array").select2({
						placeholder: "Select Orders",
					});
					
				}
			});
			
		});
		
	</script>
	
	
	<style>
		.borderBottom{border-bottom: 1px solid #ccc; padding:5px 0px;}
	</style>									