<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        margin : 6px 0 3px 5px !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice
    {
        padding: 5px 5px;
    }
</style>
<?php $this->Html->addCrumb('Advertisers', $this->Html->url(null, true)); ?>
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Lead Listing
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <?php echo $this->Form->create('Lead', array('url' => array('controller' => 'leads', 'action' => 'index'), 'class' => 'form-horizontal')); ?>
                <div class="form-body">
                    <div class="alert alert-danger display-hide">
                        <button class="close" data-close="alert"></button>
                        You have some form errors. Please check below.
                    </div>
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        <img src="/img/ajax-loade.gif"> Creating..
                    </div>
                    <h3 class="form-section">Companies</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">

                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <label class="control-label col-md-4">All Company <span class="required">
                                                * </span></label>
                                        <i class="fa"></i>

                                        <?php
                                        echo $this->Form->input('company_id.', array(
                                            'class' => 'js-company-array form-control',
                                            'label' => false,
                                            'type' => 'select',
                                            //' multiple' => 'multiple',
                                            'empty' => ' Please select Company name ',
                                            'selected' => (!empty($this->request->data['Lead']['company_id'])) ? $this->request->data['Lead']['company_id'] : array(),
                                            'options' => $companylist
                                        ));
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">

                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <label class="control-label col-md-4">All Orders <span class="required">
                                                * </span></label>
                                        <i class="fa"></i>
                                        <?php
                                        echo $this->Form->input('order_id.', array(
                                            'class' => 'js-order-array form-control',
                                            'label' => false,
                                            'type' => 'select',
                                         //   ' multiple' => 'multiple',
                                            'empty' => ' Please select Order',
                                            'selected' => (!empty($this->request->data['Lead']['order_id'])) ? $this->request->data['Lead']['order_id'] : array(),
                                            'options' => $orderlist
                                        ));
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">

                                <div class="col-md-12">
                                    <div class="input-icon right">
                                        <label class="control-label col-md-4"> All Line</label>
                                        <i class="fa"></i>
                                        <?php
                                        echo $this->Form->input('line_item.', array(
                                            'class' => 'js-line-array form-control',
                                            'label' => false,
                                            'type' => 'select',
                                            //' multiple' => 'multiple',
                                            'empty' => ' Please select Order',
                                            'selected' => (!empty($this->request->data['Lead']['line_item'])) ? $this->request->data['Lead']['line_item'] : array(),
                                            'options' => $tblLineItems
                                        ));
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">

                                    <div class="col-md-6">
                                        <label class="col-md-2">Filter</label>
                                        <div class="input-icon right">


                                            <?php echo $this->Form->input('od_isRejected', array('type' => 'checkbox', 'id' => 'od_isRejected', 'label' => false, 'hiddenField' => false, 'div' => false, 'data-checkbox' => 'icheckbox_square-grey', 'class' => 'icheck', 'after' => __('<label for="od_isRejected">Accepted</label>'))); ?>

                                            <?php //echo $this->Form->input('od_return', array('type' => 'checkbox', 'id' => 'od_return', 'label' => false, 'hiddenField' => false, 'div' => false, 'data-checkbox' => 'icheckbox_square-grey', 'class' => 'icheck', 'after' => __('<label for="od_return">Return</label>'))); ?>

                                            <?php echo $this->Form->input('od_isTest', array('type' => 'checkbox', 'id' => 'od_isTest', 'label' => false, 'hiddenField' => false, 'div' => false, 'data-checkbox' => 'icheckbox_square-grey', 'class' => 'icheck', 'after' => __('<label for="od_isTest">Test</label>'))); ?>


                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>	

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-1">Cost Structure</label>
                                        <div class="input-icon right">
                                            <?php
                                            if (!empty($costStructure)) {
                                                foreach ($costStructure as $key => $value) {
                                                    $checked = '';

                                                    if (!empty($this->request->data['Lead']['cust_structure'])) {
                                                        $checked = in_array($key, $this->request->data['Lead']['cust_structure']) ? 'checked' : '';
                                                    }
                                                    echo $this->Form->input('cust_structure.', array('type' => 'checkbox', 'id' => $key, 'value' => $key, 'label' => false, 'hiddenField' => false, 'div' => false, $checked,
                                                        'data-checkbox' => 'icheckbox_square-grey', 'class' => 'icheck', 'after' => __('<label for="' . $key . '">' . $value . '</label>')));
                                                }
                                            }
                                            ?>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">

                            <div class="col-md-12">
                                <label class="col-md-2">Email</label>
                                <div class="input-icon right form-inline">
                                    <?php echo $this->Form->input('email', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Enter email id ')); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">

                            <div class="col-md-12">
                                <label class="col-md-4">Date Range</label>
                                <div class="input-icon right form-inline">
                                    <?php echo $this->Form->input('daterange', array('label' => false, 'div' => false, 'class' => 'form-control')); ?> 	<span></span>

                                </div>
                            </div>
                        </div>
                    </div>



                </div>




                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-offset-6 col-md-6">
                                    <input type="submit" class="btn green" name="leadPost" value="Submit" />
                                    <a href="/leads" class="btn default">Cancel</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                </div>
            </div>
      
        </div>
        <!-- END FORM-->
        <?php if (!empty($post_data)) { ?>	
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs font-green-sharp"></i>
                        <span class="caption-subject font-green-sharp bold uppercase">Leads</span>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div><?php
                    echo $this->Paginator->counter(array(
                        'format' => 'Page {:page} of {:pages}, showing {:current} records out of
             {:count} total, starting on record {:start}, ending on {:end}'
                    ));
                    ?></div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="viewleads">
                        <thead>
                            <tr> 
                                <th class="table-checkbox">Sr. No</th>
                                <th>Line Item Name</th>
                                <th>Flight Name</th>
                                <th>Country Name</th>
                                <th>Email</th>
                                <th>Create Date</th>
                                <th>ipaddress</th>
                                <th>Rejected</th>
                                <th>Test</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($ordersRecords)) {
                               $serial = $this->Paginator->counter(array('format' => '{:pages}' ));
                                foreach ($ordersRecords as $ordersRecord) {
                                    ?> 
                                    <tr>
                                        <td><?php echo $serial++; ?>.</td>

                                        <td><?php  echo  $ordersRecord['tblLineItem']['li_name'] ; ?></td>

                                        <td><?php echo (!empty($ordersRecord['Flight']['name'])) ? $ordersRecord['Flight']['name'] : ''; ?></td>
                                        <td><?php echo $ordersRecord['OrderLeads']['od_country_name']; ?></td>
                                        <td><?php echo $ordersRecord['OrderLeads']['od_email']; ?></td>

                                        <td>
                                            <?php $dateformte = Configure::read('Php.dateformte'); ?>
                                            <?php echo date($dateformte, strtotime($ordersRecord['OrderLeads']['od_create'])) ?>
                                        </td>
                                        <td><?php echo $ordersRecord['OrderLeads']['od_ipaddress']; ?></td>
                                        <td><?php echo ($ordersRecord['OrderLeads']['od_isRejected'] == 1) ? 'Yes' : 'No'; ?></td>
                                        <td><?php echo ($ordersRecord['OrderLeads']['od_isTest'] == 1) ? 'Yes' : 'No'; ?></td>
                                    </tr>

                                    <?php
                                } // end of foreach 
                            } else {// end of if condtion 
                                ?> 
                                <tr> <td colspan="13" align="center"> There are no record found</td></tr>
                            <?php } ?>
                        </tbody>

                    </table>
                </div>	
                <div class="pagination">
                    <?php $this->Paginator->options['url']['?'] = 'search=1';?>
                    <?php echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li')); ?>
                </div>
            </div>

        <?php } // condition of request data   ?>
    </div>
</div>
</div>
</div>
<style>
    #LeadIndexForm{
        overflow: hidden;
    }
    #viewleads{margin-top:15px;}
    .select2-container .select2-search--inline{float: none;}

    div label {
        margin-right: 10px;
    } 
</style> 
