<?php $this->Html->addCrumb('Advertisers', $this->Html->url( null, true )); ?>
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Lead Re-Conciliation 
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
				</div>
			</div>
			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				<?php echo $this->Form->create('Lead',array('url'=>array('controller'=>'leads','action'=>'reConciliation'),'type' => 'file','class'=>'form-horizontal'));?>
						
						
					<div class="form-body">
						<div class="alert alert-danger display-hide">
							<button class="close" data-close="alert"></button>
							You have some form errors. Please check below.
						</div>
						<div class="alert alert-success display-hide">
							<button class="close" data-close="alert"></button>
							<img src="/img/ajax-loade.gif"> Creating..
						</div>
						<h3 class="form-section">Order Details</h3>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Order List <span class="required">
									* </span></label>
									<div class="col-md-9">
										<div class="input-icon right">
											<i class="fa"></i>
											 <?php echo $this->Form->input('order_id', array(
													   'class' => 'js-order-array form-control',
													  'label'=>false,
													  'type'=>'select',
													  'empty' => ' Please select Order',
													  'onChange'=>'ShowOrderItem(this.value);',
													  'options' => $orderlist
											  ));?>
											<span class="help-block">
											Insertion Order Name </span>
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Line Items  <span class="required">
									* </span></label>
									<div class="col-md-9">
										<div class="input-icon right">
											<i class="fa"></i>
											 <?php echo $this->Form->input('line_id', array(
													   'class' => 'js-order-array form-control',
													  'label'=>false,
													  'type'=>'select',
													  'empty' => 'Please Select line item',
											));?>
											<span class="help-block">
											LIne Items </span>
										</div>
									</div>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Csv File </label>
									<div class="col-md-9">
										<div class="clearfix">
											<div class="icheck-inline">
												<span class="btn btn-success fileinput-button">
												<i class="glyphicon glyphicon-plus"></i>
												<span>Select file...</span>
													<!-- The file input field used as target for the file upload widget --><?php echo  $this->Form->file('csv_file', array('class'=>'file-loading','data-show-preview'=> 'false',
													'accept'=>'.csv')); ?>
												</span>
												 
											</div>
										</div>
										<span class="help-block">
											 
										Upload csv file.(<a href= "<?php echo Configure::read('Path.url_csvPath').'sample.csv';  ?>" download > Download sample file </a>) </span>
									</div>
								</div>
							</div>
							<!--/span-->
							 
						 
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-offset-6 col-md-6">
										<button type="submit" class="btn green">Upload</button>
										<button type="button" class="btn default">Cancel</button>
									</div>
								</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
			</div>
		</div>
	</div>
<!-- END PAGE CONTENT INNER -->

<script>

	/*function checkRevenueType(option){
		if(option ==1){
			
		}
 
	
	//alert( <?php echo $user_company_id['tblProfile']['dfp_company_id'] ?>);
	document.getElementById("adAutoAllCompany").value="<?php echo $user_company_id['tblProfile']['dfp_company_id'] ?>";
	


	}*/
</script>
