<!-- BEGIN PAGE CONTENT INNER -->
<style>
	th, td{
    text-align: center;
	}
</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">View Reports</span>
				</div>
			</div>
            <div class="portlet-body">
				<div class="table-toolbar">
                    <?php echo $this->Form->create(false, array('url' => array('controller' => 'delivery', 'action' => 'view_reports'), 'class' => 'form-horizontal', 'id' => 'reportsForm')); ?>
                    <?php echo $this->Form->hidden('submit_value', array('value' => 'submit')); ?>
                    <div class="row">
						
                        <div class="col-md-3">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
									echo $this->Form->input('company_id', array(
                                    'class' => 'js-company-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Select Advertiser',
                                    'options' => $companylist
									));
								?>
								
								
								
							</div>
						</div>
                        <div class="col-md-3">
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php
									echo $this->Form->input('order_id', array(
                                    'class' => 'js-order-array form-control',
                                    'label' => false,
                                    'type' => 'select',
                                    'multiple' => true,
                                    'empty' => ' Select Orders',
                                    'options' => $orderlist
									));
								?>
								
							</div>
						</div>
						
						
                        <div class="col-md-3">
							
							
                            <div class="input-icon right">
                                <i class="fa"></i> 
                                <?php echo $this->Form->input('daterange', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Date Range', 'style' => 'padding-right: 0;')); ?> 	<span></span>
								
								
							</div>
						</div>
                        <div class="col-md-3">
                            <div class="btn-group">
                                <button type="submit" class="btn blue btn-secondary">Filter</button> 
							</div>
						</div>
					</div>
                    <?php echo $this->Form->end(); ?>
				</div>
                
                <div class="table-responsive1">
					<?php
                        if (!empty($flights)) {
                            $csnameArray = array(1 => '<span style="color:red;font-weight:bold;">{CSNAME}</span>', 2 => '<span style="color:red;font-weight:bold;">{CSNAME}</span>', 3 => '<span style="color:#FF9934;font-weight:bold;">{CSNAME}</span>', 4 => '<span style="color:green;font-weight:bold;">{CSNAME}</span>', 5 => '<span style="color:#FF9934;font-weight:bold;">{CSNAME}</span>', 6 => '<span style="color:#BB99AA;font-weight:bold;">{CSNAME}</span>');
                            $statusArray = array(1 => '<span style="color:red;font-weight:bold;">{STATUS}</span>', 2 => '<a style="margin-right:5px;text-decoration:underline;font-weight:bold;" href="{LINK}">Upload Creative</a>', 3 => '<span style="color:#FF9934;font-weight:bold;">{STATUS}</span>', 4 => '<span style="color:green;font-weight:bold;">{STATUS}</span>', 5 => '<span style="color:#FF9934;font-weight:bold;">{STATUS}</span>', 6 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 7 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 8 => '<span style="color:#BB99AA;font-weight:bold;">{STATUS}</span>', 9 => '<a style="margin-right:5px;text-decoration:underline;font-weight:bold;" href="{LINK}">Create Flight</a>');
                            $dateformte = Configure::read('Php.dateformte');
							usort($flights, function($a, $b) {
								return $b['tblReportDay']['rd_imp'] - $a['tblReportDay']['rd_imp'];
							});
							foreach ($flights as $report) {
							?>
							
							<table class="table table-striped table-bordered table-hover" id="reports" style="margin-bottom:40px;">
								<thead style="background-color: rgb(102, 102, 102);color:#fff;text-align:center;">
									<tr>
										
										<th>
											<?php echo 'Flight Name/Adunit Name/ Line item Pricing Model/ Line item  Status'; ?>  
											
										</th>
										<th>
											<?php echo 'Start/End Date'; ?>  
										</th>
										<th>Revenue</th>
										<th>Imp & eCPM</th>
										<th>Takes & Take Rate</th>
										<th>Accepted</th>
										
									</tr>
								</thead>
								
								<tr>
									<td>
										
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th style="text-align:left;"><?php echo $report['tblFlight']['fl_name']; ?></th>
												</tr>
											</thead>
											<tr><td style="text-align:left;"><?php echo $report['tblAdunit']['adunit_name']; ?></td></tr>
											<tr><td style="text-align:left;"><?php echo $report['tblp']['CompanyName'] . ' >> ' . $report['tblLineItem']['li_name']; ?></td></tr>
											<tr>
												<?php
													$priceModel = '';
													if (array_key_exists($report['tcs']['cs_id'], $csnameArray)) {
														$priceModel = str_replace('{CSNAME}', $report['tcs']['cs_name'], $csnameArray[$report['tcs']['cs_id']]);
														} else {
														$priceModel = $report['tcs']['cs_name'];
													}
												?> 
												<td style="text-align:left;">
													<?php echo $priceModel; ?>
												</td></tr>
												<tr>
													<?php
														$linkstatus = '';
														if ($report['tls']['ls_id'] == 2) {
															$linkstatus = Router::url(array('controller' => 'orders', 'action' => 'view_line_creatives', $report['tblLineItem']['li_id']));
															} else if ($report['tls']['ls_id'] == 9) {
															$linkstatus = Router::url(array('controller' => 'insertionorder', 'action' => 'add_line_item_flight', $report['tblLineItem']['li_id'], $report['tls']['ls_id']));
														}
														$stautsData = '';
														if (array_key_exists($report['tls']['ls_id'], $statusArray)) {
															$report['tls']['ls_id'];
															$status_span = str_replace('{STATUS}', $report['tls']['ls_status'], $statusArray[$report['tls']['ls_id']]);
															$stautsData = str_replace('{LINK}', $linkstatus, $status_span);
															// line
														} ?>
												<td style="text-align:left;"><?php echo $stautsData; ?></td></tr>
										</table>
										
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><?php echo (!empty($report['tblFlight']['fl_start'])) ? $this->Time->format($dateformte, $report['tblFlight']['fl_start']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?></th>
												</tr>
											</thead>	
											<tr><td><?php echo (!empty($report['tblFlight']['fl_end'])) ? $this->Time->format($dateformte, $report['tblFlight']['fl_end']) : "<span style='font-weight:bold;color:red;'>N/A</span>"; ?></td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
										
									</td>
									
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>$<?php 
														$rev_share = 0;	if(empty($report['tblMappedAdunit']['rev_share'])){
															$rev_share = 50;
															}else{
															$rev_share = $report['tblMappedAdunit']['rev_share'];
														}
														$rdrevenue = $report['tblReportDay']['rd_revenue'];
														echo $revenue = ($rdrevenue*$rev_share)/100;
														
													?></th>
												</tr>
											</thead>	
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
										
									</td>
									
									<?php /*  <td>
										<?php
										$percentage = getPercent($report['tblLineItem']['li_total'], $report['tblReportDay']['rd_revenue']);
										?>
										
										
										<div class="progress">
										<div style="border: 2px solid <?php echo getprocessbar($percentage); ?>;
										height: 20px;">
										<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage; ?>%;color:black;box-shadow: none;background-color:<?php echo getprocessbar($percentage); ?>"> <?php echo $percentage; ?>%</div>
										</div>
										</div>
										Booked $<?php echo $report['tblReportDay']['rd_revenue'] . " out of $" . $report['tblLineItem']['li_total']; ?>
										<?php $percentage = getPercent($report['tblLineItem']['li_total'], $report['tblReportDay']['rd_delivered']); ?>
										<br/>
										<br/>
										<div class="progress">
										<div style="border: 2px solid <?php echo getprocessbar($percentage); ?>;
										height: 20px;">
										<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percentage; ?>%;color:black;box-shadow: none;background-color:<?php echo getprocessbar($percentage); ?>"> <?php echo $percentage; ?>%</div>
										</div>
										</div>
										Delevered $<?php echo $report['tblReportDay']['rd_delivered'] . " out of $" . $report['tblLineItem']['li_total']; ?> 
										
										</td> 
										<td>
										<?php echo  $report['tblReportDay']['rd_delivered']; ?>
										
									</td>*/?>
									<td>
										
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><?php echo $report['tblReportDay']['rd_imp']; ?></th>
												</tr>
											</thead>	
											<tr><td><img src="http://chart.apis.google.com/chart?chs=90x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($report['tblReportDay']['impgraph']) ? implode(',', array_values($report['tblReportDay']['impgraph'])) : 0); ?>"></td></tr>
											<tr><td>$<?php echo $ecpm = number_format(($report['tblReportDay']['rd_revenue'] * 1000) / ($report['tblReportDay']['rd_imp']),2) ; ?></td></tr>
											<tr><td><img src="http://chart.apis.google.com/chart?chs=90x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($report['tblReportDay']['eCPMgraph']) ? implode(',', array_values($report['tblReportDay']['eCPMgraph'])) : 0); ?>"></td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><?php echo $report['tblReportDay']['rd_leads']; ?></th>
												</tr>
											</thead>	
											<tr><td><?php
												echo number_format(($report['tblReportDay']['rd_leads']/$report['tblReportDay']['rd_imp']) * 100,2);
												//echo (empty($report['tblReportDay']['rd_leads'])) ? 0 : number_format(/$report['tblReportDay']['rd_imp']);
												//echo($report['tblReportDay']['totaltake'] == 0) ? 0 : (
												//      $report['tblReportDay']['rd_leads'] / ($report['0']['rd_imp'] == 0) ? 1 : $report['0']['ma_imp']);
											?>%</td></tr>
											<tr><td><img src="http://chart.apis.google.com/chart?chs=90x16&cht=ls&chco=0077CC&chm=B,E6F2FA,0,0,0&chls=1,0,0&chd=t:<?php echo (!empty($report['tblReportDay']['takegraph']) ? implode(',', array_values($report['tblReportDay']['takegraph'])) : 0); ?>"></td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
										
									</td>
									
									<td>
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><?php echo $report['tblReportDay']['rd_accepted'];?> (<?php  echo $accepted=getPercent($report['tblReportDay']['rd_leads'], $report['tblReportDay']['rd_accepted']);?>%)</th>
												</tr>
											</thead>	
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
											<tr><td>&nbsp;</td></tr>
										</table>
									</td>
								</tr>
								
								
								
							</table>
							<?php
							}
						}
					?>
				</div>
				
				<div class="table-responsive1">
					<table class="table table-striped table-bordered table-hover">
						<tfoot>
                            <tr>
                                <td colspan="6">
                                    <?php if (!$flights) { ?>
                                        <div style='color:#FF0000'>No Record Found</div>
										<?php } else { ?>
										
                                        <ul class="pagination">
											
                                            <?php if ($this->Paginator->first()) { ?><li><?php echo $this->Paginator->first('« First', array('class' => '')); ?></li>
											<?php } ?>										
                                            <?php if ($this->Paginator->hasPrev()) { ?>
                                                <li><?php echo $this->Paginator->prev('< Previous', array('class' => ''), null, array('class' => 'disabled')); ?>&nbsp;  &nbsp;</li>
											<?php } ?>
											
                                            <?= $this->Paginator->numbers(array('modulus' => 6, 'tag' => 'li', 'class' => '', 'separator' => '')); ?>
                                            <?php if ($this->Paginator->hasNext()) { ?>
												
                                                <li><?php echo $this->Paginator->next('Next >', array('class' => '')); ?></li>
											<?php } ?>
                                            <?php if ($this->Paginator->last()) { ?>
                                                <li><?php echo $this->Paginator->last('Last »', array('class' => '')); ?></li>
											<?php } ?>
										</ul>
										<?php } ?>
										
										
									</td>
								</tr>
							</tfoot>
							</table>
						</div>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT INNER -->
		<style>
			#sample_4_filter { 
			float:right;
		}
		</style>
		<script>
	
    jQuery(document).ready(function () {
		$(function () {
			
			var str = $('input[name="data[daterange]"]').val();
			if(str != ''){ 
				var res = str.split("-");
				//alert(res[0]);
				var start = res[0];
				var end = res[1];
				}else{
				var start = moment().subtract(29, 'days');
				var end = moment();
			}
			
            function cb(start, end) {
				// $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			}
			
			$('input[name="data[daterange]"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
				
				
			});
			
			$('input[name="data[daterange]"]').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
				
			});
			
            cb(start, end);
            $('input[name="data[daterange]"]').daterangepicker({
				"alwaysShowCalendars": true,
				"startDate": start,
				"endDate": end,
				"opens": "left",
				//"autoUpdateInput": false,
				
				
				ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

        });
        $(".js-company-array").select2({
            placeholder: "Select Advertiser",
            maximumSelectionLength: 1,
            //allowClear: true
        });
        $(".js-order-array").select2({
            placeholder: "Select Orders",
            //allowClear: true
        });
        $(".js-satus-array").select2({
            placeholder: "Status",
            //allowClear: true
        });
        $(".js-media-array").select2({
            placeholder: "Media Type",
            //allowClear: true
        });

    });
    function actionPlay(action, fl_id) {
        var id = '';
        if (action == 2) { // hide here play and show stop button
            var play_id = "play" + fl_id;
            var stop_id = "stop" + fl_id;
        } else {
            var play_id = "stop" + fl_id;
            var stop_id = "play" + fl_id;
        }

        $.ajax({
            type: "POST",
            url: "<?php echo Router::url(array('controller' => 'insertionorder', 'action' => 'update_flight_play_action'), true); ?>/" + action + "/" + fl_id,
            dataType: "json",
            success: function (data) {
                if (data == 1) {
                    $("#" + play_id).hide();
                    $("#" + stop_id).show();
                }
            }, error: function (data) {
                alert("There was an error. Try again please!");
            }
        });

    }

    $('#company_id').change(function () {
        // Remove all Manage Lead Allocation
        var favorite = [];
        var selectedItem = [];
        $.each($("#company_id :selected"), function () {
            favorite.push($(this).val());
        });
        $(".js-order-array").empty();
        $(".js-order-array").select2({
            placeholder: "Select Orders",
            allowClear: true
        });

        var selecid = favorite.join(",");

        //alert("My favourite sports are: " + favorite.join(", "));
        $.ajax({
            type: "POST",
            url: "<?php echo Router::url(array('controller' => 'orders', 'action' => 'getAllOrderList')); ?>",
            data: 'user_id=' + favorite.join(","),
            dataType: "json",
            success: function (data) {
                var count = 0;
                $(".js-order-array").select2({
                    data: data
                });
                $(".js-order-array").select2({
                    placeholder: "Select Orders",
                });

            }
        });

    });

</script>

<?php

function getprocessbar($process) {
    $color = 'red';
    if ($process >= 0 && $process < 50) {
        $color = 'red';
    } else if ($process >= 50 && $process < 75) {
        $color = '#EB661E';
    } else if ($process >= 75 && $process < 90) {
        $color = 'orange';
    } else if ($process >= 90 && $process <= 100) {
        $color = 'green';
    }

    return $color;
}

function getPercent($total, $goal) {

    if ($total == 0)
        return 0;
    return sprintf('%0.2f', ($goal / $total ) * 100);
}
?>


<style>
    .borderBottom{border-bottom: 1px solid #ccc; padding:5px 0px;}
    </style>