<?php
	/**
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Pages
		* @since         CakePHP(tm) v 0.10.0.1076
	*/
	
	if (!Configure::read('debug')):
	throw new NotFoundException();
	endif;
	
	App::uses('Debugger', 'Utility');
?>
<div class="portlet-body">
	
	<!-- BEGIN PAGE CONTENT INNER -->
	<div class="row">
		<h1>Money Map Press Q4 2015 IO</h1>
		<div class="col-md-12">
			<?php echo $this->element('revenue_widget'); ?>
		</div>
		<div class="col-md-6">
			<?php echo $this->element('monthly_revenue_breakdown_by_source'); ?>
			<?php echo $this->element('lead_generation_revenue_breakdown_placement'); ?>
			<?php echo $this->element('email_list_revenue'); ?>
		</div>
		<div class="col-md-6">
			<!-- BEGIN PORTLET-->
			<?php echo $this->element('daily_revenue_trend'); ?>
			<?php echo $this->element('lead_generation_daily_revenue_trends'); ?>
			<!-- END PORTLET-->
		</div>
	</div>
	<!-- END PAGE CONTENT INNER -->
	
</div>