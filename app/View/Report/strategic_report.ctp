<?php
	/**
		* @link          http://cakephp.org CakePHP(tm) Project
		* @package       app.View.Pages
		* @since         CakePHP(tm) v 0.10.0.1076
	*/
	
	if (!Configure::read('debug')):
	throw new NotFoundException();
	endif;
	
	App::uses('Debugger', 'Utility');
?>
<!-- BEGIN PAGE CONTENT INNER -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<div class="row">
	<div class="col-md-6">
		<!-- BEGIN PORTLET-->
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Total Registrations</span>
					
				</div>
				<script type='text/javascript'>

</script>
			</div>
			<div class="portlet-body">
				<script type='text/javascript'>
				google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(drawlgraphs);

function drawlgraphs()
{
	drawTrendlinesReg();
	drawTrendlinesLeads();
	drawTrendlinesLeadsPerReg();
	drawTrendlinesRevPerReg();
	drawTrendlinesAdvertisers();
	drawTrendlinesCampaigns();
	drawTrendlinesPublishers();
	drawTrendlinesPublishers();
	drawTrendlinesRevenue();
}
function drawTrendlinesReg() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Registrations');
      //data.addColumn('number', 'Cats');
//var json_arr = <?php echo json_encode($graph_arrayReg); ?>; 
var json_arrReg = <?php echo $graph_arrayReg; ?>; 
	  
data.addRows( json_arrReg);
	  


      var options = {
        hAxis: {
          title: 'Month'
        },
        vAxis: {
          title: 'Registrations'
        },
        colors: ['#AB0D06'],
        trendlines: {
          0: {type: 'exponential', color: '#333', opacity: 1}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_registrations'));
      chart.draw(data, options);
    }
	function drawTrendlinesLeads() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Leads');
      //data.addColumn('number', 'Cats');

      var json_arrReg = <?php echo $graph_arrayLeads; ?>; 
	  
data.addRows( json_arrReg);

      var options = {
        hAxis: {
          title: 'Month'
        },
        vAxis: {
          title: 'Leads'
        },
        colors: ['#AB0D06'],
        trendlines: {
          0: {type: 'exponential', color: '#333', opacity: 1}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_leads'));
      chart.draw(data, options);
    }
	
	function drawTrendlinesLeadsPerReg() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Leads Per Registrations');
      //data.addColumn('number', 'Cats');

      var json_arrReg = <?php echo $graph_arrayLeadsPerReg; ?>; 
	  
data.addRows( json_arrReg);

      var options = {
        hAxis: {
          title: 'Month'
        },
        vAxis: {
          title: 'Leads Per Registration'
        },
        colors: ['#AB0D06'],
        trendlines: {
          0: {type: 'exponential', color: '#333', opacity: 1}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_leadsperreg'));
      chart.draw(data, options);
    }
	function drawTrendlinesRevPerReg() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Revenue Per Registration');
      //data.addColumn('number', 'Cats');

      var json_arrReg = <?php echo $graph_arrayRevenuePerReg; ?>; 
	  
data.addRows( json_arrReg);


      var options = {
        hAxis: {
          title: 'Month'
        },
        vAxis: {
          title: 'Revenue Per Registrations'
        },
        colors: ['#AB0D06'],
        trendlines: {
          0: {type: 'exponential', color: '#333', opacity: 1}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_revenuesperreg'));
      chart.draw(data, options);
    }
	
	function drawTrendlinesAdvertisers() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Advertisers');
      //data.addColumn('number', 'Cats');

      data.addRows([
        ['Jan', 20],    ['Feb', 21],   ['Mar', 21],  ['Apr', 18],   ['May', 19],  ['Jun', 31],
		['Jul', 27], ['Aug', 31], ['Sep', 29]
      ]);

      var options = {
        hAxis: {
          title: 'Month'
        },
        vAxis: {
          title: 'Advertisers'
        },
        colors: ['#AB0D06'],
        trendlines: {
          0: {type: 'exponential', color: '#333', opacity: 1}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_advertisers'));
      chart.draw(data, options);
    }
	
	function drawTrendlinesCampaigns() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Campaigns');
      //data.addColumn('number', 'Cats');

      data.addRows([
        ['Jan', 35],    ['Feb', 45],   ['Mar', 39],  ['Apr', 39],   ['May', 46],  ['Jun', 51],
		['Jul', 51], ['Aug', 49], ['Sep', 67]
      ]);

      var options = {
        hAxis: {
          title: 'Month'
        },
        vAxis: {
          title: 'Campaigns'
        },
        colors: ['#AB0D06'],
        trendlines: {
          0: {type: 'exponential', color: '#333', opacity: 1}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_campaigns'));
      chart.draw(data, options);
    }
	
	function drawTrendlinesPublishers() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Publishers');
      //data.addColumn('number', 'Cats');

      data.addRows([
        ['Jan', 31],    ['Feb', 29],   ['Mar', 28],  ['Apr', 26],   ['May', 26],  ['Jun', 28],
		['Jul', 34], ['Aug', 35], ['Sep', 29]
      ]);

      var options = {
        hAxis: {
          title: 'Month'
        },
        vAxis: {
          title: 'Publishers'
        },
        colors: ['#AB0D06'],
        trendlines: {
          0: {type: 'exponential', color: '#333', opacity: 1}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_publishers'));
      chart.draw(data, options);
    }
	
	function drawTrendlinesRevenue() {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Revenue');
      //data.addColumn('number', 'Cats');

      data.addRows([
        ['Jan', 19734.50],    ['Feb', 19863.25],   ['Mar', 24092.25],  ['Apr', 27421.25],   ['May', 37369.50],  ['Jun', 47062.60],
		['Jul', 46933.25], ['Aug', 36585.75], ['Sep', 53439.50]
      ]);

      var options = {
        hAxis: {
          title: 'Month'
        },
        vAxis: {
          title: 'Revenue'
        },
        colors: ['#AB0D06'],
        trendlines: {
          0: {type: 'exponential', color: '#333', opacity: 1}
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_revenue'));
      chart.draw(data, options);
    }
				    </script>
					<div id="chart_registrations"></div>  
					 
					
			</div>
		</div>
		<!-- END PORTLET-->
		
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Total Leads Generated</span>
					
				</div>
				
			</div>
			<div class="portlet-body">
				<div id="chart_leads"></div> 
			</div>
		</div>
				<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Leads Per Registration</span>
					
				</div>
				
			</div>
			<div class="portlet-body">
				<div id="chart_leadsperreg"></div>  
			</div>
		</div>
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Revenue Per Registration</span>
					
				</div>
				
			</div>
			<div class="portlet-body">
				<div id="chart_revenuesperreg"></div>  
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<!-- BEGIN PORTLET-->
		
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Total Advertisers</span>
					
				</div>
				
			</div>
			<div class="portlet-body">
				<div id="chart_advertisers"></div>  
			</div>
		</div>
		<!-- END PORTLET-->
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Total Campaigns</span>
					
				</div>
				
			</div>
			<div class="portlet-body">
				<div id="chart_campaigns"></div> 
			</div>
		</div>
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Total Publishers</span>
					
				</div>
				
			</div>
			<div class="portlet-body">
				<div id="chart_publishers"></div> 
			</div>
		</div>
		<!-- BEGIN PORTLET-->
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font hide"></i>
					<span class="caption-subject theme-font bold uppercase">Total Revenue</span>
					
				</div>
				
			</div>
			<div class="portlet-body">
				<div id="chart_revenue"></div> 
			</div>
		</div>
		<!-- END PORTLET-->
	</div>
</div>
<!-- END PAGE CONTENT INNER -->
<script>
	function viewSummary(period){
		if(period == "year")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=3&period=year&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "week")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=3&period=week&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "month")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=3&period=month&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "day")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getSparklines&idSite=3&period=day&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		
		document.getElementById('viewSummaryiframe').src = loc;
		
	}
	
	
	function viewSummarygraph(period)
	{
		
		if(period == "year")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=year&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "week")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=week&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "month")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=month&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "day")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=day&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		
		document.getElementById('viewSummarygraphiframe').src = loc;
	}
	
	function pageviewSummarygraph(period)
	{
		
		if(period == "year")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_pageviews&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=year&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "week")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_pageviews&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=week&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "month")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_pageviews&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=month&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		else if(period == "day")
		var loc = "https://tracking.digitaladvertising.systems/index.php?module=Widgetize&action=iframe&columns[]=nb_pageviews&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=3&period=day&date=2015-08-12&disableLink=1&widget=1&token_auth=2e25b27b4dda9abd8de1d2d7dc7ffdb0";
		
		document.getElementById('pageviewSummarygraphiframe').src = loc;
	}
</script>