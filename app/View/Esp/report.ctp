<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
	<li>
		<a href="/">Home</a><i class="fa fa-circle"></i>
	</li>
	 
	<li  class="active">
		<?php echo $this->Html->link('ESP Reporting',array('controller' => 'esp', 'action' => 'report'));?></a>
	</li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE CONTENT INNER -->
<div class="row">
	<div class="col-md-12">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i> ESP Reporting
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config">
					</a>
					<a href="javascript:;" class="reload">
					</a>
				</div>
			</div>
				
				<div class="portlet-body form">
					<div class="form-body">
						 <div class="row">
							 <?php echo $this->Form->create('Esp',array('url'=>array('controller'=>'esp','action'=>'report')))?>
			
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-3">Publisher<span class="required">
									* </span></label>
									<div class="col-md-9">
										
										<?php echo $this->Form->input('publisher_id', array(
												  'class' => 'form-control js-profile-array',
												  'label'=>false,
												  'type'=>'select',
												   'empty' => 'Please select publisher name',
													'options' => $publishers
											  ));?>
												<span class="help-block">
											Select the publisher name. </span>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-3">Ad-Units<span class="required">
									* </span></label>
									<div class="col-md-9">
										
										<?php echo $this->Form->input('adunit_id', array(
												  'class' => 'form-control js-adunit-array',
												  'label'=>false,
												  'type'=>'select',
												   'empty' => 'Please select the adunit',
													 
											  ));?>
											  <span class="help-block">
											Select the ad units. </span>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-3">Mail list<span class="required">
									 </span></label>
									<div class="col-md-9">
										
										<?php echo $this->Form->input('m_id', array(
												  'class' => 'form-control js-mail-array',
												  'label'=>false,
												  'type'=>'select',
												   'empty' => 'Please select the mail',												 
											  ));?>
												<span class="help-block">
												Select the mailing list. </span>
												<button type="submit" class="btn green" style="float:right;">Run Report</button>
									</div>
									
								</div>
							</div>
								 
								<?php echo $this->Form->end();?>
							
							<div class="col-md-4">
								 
								<div class="row list-separated">
									<div class="col-md-4 col-sm-4 col-xs-6">
								 
										 <div id="espuseda" style="width: 500px; height: 400px;"></div>
									</div>
								</div>
						 
							
							</div>
							<div class="col-md-8">
									 
								<div class="row list-separated">
									<div class="col-md-4 col-sm-4 col-xs-6">
										<div id="3rdpartymailer" style="width: 600px; height: 400px;"> </div>
									</div>
								</div>
									 
								 
							
							</div>
							
						</div>
					</div>
				</div>	
			
		</div>
	</div>
</div>  
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load("current", {packages:["corechart"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
	var data = google.visualization.arrayToDataTable([
	  ['Task', 'Hours per Day'],
	  ['Sent',     11],
	  ['Open',      2],
	  ['Click',  2],
	  ['Delived', 2],
	  ['Bounce',    7]
	]);

	var options = {
	  title: 'My Daily Activities',
	  is3D: true,
	};

	var chart = new google.visualization.PieChart(document.getElementById('espuseda'));
	chart.draw(data, options);
	
	
	 var dataTableMulticolumn = new google.visualization.DataTable();
	//define columns for second example
	dataTableMulticolumn.addColumn('string','Quarters');
	//  PerformanceRevenueByISP Mandrill,Mailchimp,Aweber,Mailgun and Socketlabs
	dataTableMulticolumn.addColumn('number', 'Mandrill');
	dataTableMulticolumn.addColumn('number', 'Mailchimp');
	dataTableMulticolumn.addColumn('number', 'Aweber');
	dataTableMulticolumn.addColumn('number', 'Mailgun');
	dataTableMulticolumn.addColumn('number', 'Socketlabs');
	dataTableMulticolumn.addRows([['OPENS',1,31,31,2,31],['CLICKS',52,1,1,1,1],['BOUNCES',1,1,1,1,1]]);
	//define rows of data for secondf example


	var multiColumnChart = new google.visualization.ColumnChart (document.getElementById('3rdpartymailer'));
	var options = {width: 500, height: 200,   title: 'Mandrill,Mailchimp,Aweber,Mailgun and Socketlabs'};
	multiColumnChart.draw(dataTableMulticolumn, options);
  }
  
      
</script>


