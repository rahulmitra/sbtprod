<?php
	
	class MailgunController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblAdunit','Authake.tblNlsubscribers','Dynamic','Authake.tblMailing','Authake.tblProfile','Authake.tblMappedAdunit', 'Authake.tblMessage','UserLevelEmailPerformance','tblEspSettings');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {		
			
		}
		
		
		public function DedicatedEmail() {
			App::import('Vendor', 'src/html2text');
			App::import('Vendor', array('file' => 'autoload'));	
			
			$mailing_id = $this->request->param('mailing_id');
			$esp_id = $this->request->param('esp_id');
			$SubjectLine = $this->request->param('SubjectLine');
			
			
			$raa = rand(pow(10, 4), pow(10, 3)-1);
			
			$m_ad_uid = $this->tblMailing->field(
			'm_ad_uid',
			array('m_id' => $mailing_id)
			);
			
			$options_ad['joins'] = array(
			array('table' => 'tbl_mapped_adunits',
			'alias' => 'tma',
			'type' => 'INNER',
			'conditions' => array(
			'tma.ma_ad_id = tblAdunit.ad_dfp_id'
			)),
			array('table' => 'tbl_esp_settings',
			'alias' => 'tes',
			'type' => 'INNER',
			'conditions' => array(
			'tes.es_ad_id = tblAdunit.adunit_id'
			)),
			array('table' => 'tbl_email_service_providers',
			'alias' => 'tesp',
			'type' => 'INNER',
			'conditions' => array(
			'tes.es_esp_id = tesp.esp_id'
			))
			);
			
			$options_ad['conditions'] = array('tma.ma_uid' => $m_ad_uid, 'tes.es_esp_id' => $esp_id);
			$options_ad['fields'] = array('tblAdunit.ad_uid', 'tblAdunit.adunit_id', 'tblAdunit.mailling_address_footer', 'tblAdunit.automatic_address_footer', 'tblAdunit.publisher_id', 'tblAdunit.sender_email', 'tes.es_from_email', 'tes.es_private_key', 'tes.es_site_domain', 'tes.es_limit', 'tesp.esp_mail_tag', 'tesp.esp_unsubscribe_tag','tma.ma_ad_id','tma.ma_l_id');
			$adunitSettings = $this->tblAdunit->find('first',$options_ad);

			$aduid = $adunitSettings['tblAdunit']['ad_uid'];
			
			$pub_id = $adunitSettings['tblAdunit']['publisher_id'];
			
			$mail_tag = $adunitSettings['tesp']['esp_mail_tag'];
			
			$unsub_tag = $adunitSettings['tesp']['esp_unsubscribe_tag'];
			
			$aduidDFP = $adunitSettings['tma']['ma_ad_id'];
			
			$ma_l_id = $adunitSettings['tma']['ma_l_id'];

			$automatic_address_footer = $adunitSettings['tblAdunit']['automatic_address_footer'];
			
			$physical_address = $adunitSettings['tblAdunit']['mailling_address_footer'];
			
			$fromEmail = $adunitSettings['tes']['es_from_email'];
			$MAILGUNAPP_API_KEY = $adunitSettings['tes']['es_private_key'];
			$mg = new Mailgun\Mailgun($MAILGUNAPP_API_KEY);
			$siteName = $adunitSettings['tblAdunit']['sender_email'];
			$domain = $adunitSettings['tes']['es_site_domain'];
			$batchMsg = $mg->BatchMessage($domain);
			$isTest = 0;
			$limit = $adunitSettings['tes']['es_limit'];
			
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			$m_li_crtid = $this->tblMailing->field(
			'm_li_crtid',
			array('m_id' => $mailing_id)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			if($isTest == "1")
			{
			$tblSubscribers = array();
			$tblSubscribers[0]['tbl_email_segments']['es_email'] = "sunny.gulati@siliconbiztech.com";
			$tblSubscribers[0]['tbl_email_segments']['es_fname'] = "";
			$tblSubscribers[0]['tbl_email_segments']['es_lname'] = "";
			}
			else
			$tblSubscribers = $this->Dynamic->Query("SELECT * FROM tbl_email_segments WHERE es_ml_id=" . $mailing_id ." and es_esp_id=3 and es_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , 1");
			
			print_r($tblSubscribers);
			
			//$finalurl = $url . "?idsite=" . $tracksite_id . "&cid=" . $m_li_crtid . "&mid=" . $mailing_id . "&uid=" . $m_ad_uid . "&redirect=" . urlencode($redirect_uri); 
			
			$ulliItems = '';
			$fullhtml = '';
			$firstTitle = '';
			$i = 0;
			
			if(!empty($tblSubscribers))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber['tbl_email_segments']['es_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$message_id = $this->tblMessage->getLastInsertID();
						$strArray[] = array('email'=>''.$tblSubscriber['tbl_email_segments']['es_email'].'', 'name'=>$tblSubscriber['tbl_email_segments']['es_fname'], 'last'=>preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $tblSubscriber['tbl_email_segments']['es_lname']));
						$batchMsg->addToRecipient($tblSubscriber['tbl_email_segments']['es_email'], array("first"=> preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $tblSubscriber['tbl_email_segments']['es_fname']), "last" => preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $tblSubscriber['tbl_email_segments']['es_lname'])));
					}
				}
				
				$options2['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => 'text/html', 'tbc.cr_id' => $m_li_crtid);
				$options2['joins'] = array(
				array('table' => 'tbl_line_items',
				'alias' => 'tli',
				'type' => 'INNER',
				'conditions' => array(
				'tli.li_id = tblMappedAdunit.ma_l_id')),
				array('table' => 'tbl_creatives',
				'alias' => 'tbc', 
				'type' => 'INNER',
				'conditions' => array(
				'tbc.cr_lid = tli.li_dfp_id'
				)));
				$options2['fields'] = array('tbc.*','tblMappedAdunit.*');
				$sponsoredCreative = $this->tblMappedAdunit->find('first', $options2);
				$sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
				$sponsoredRedirect = $sponsoredCreative['tbc']['cr_redirect_url'];
				$sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&send_image=0";
				$sponsoredBody = $sponsoredCreative['tbc']['cr_body'] . $automatic_address_footer . "<br /><br /> " . $physical_address . "<br /><br /> Please click here to <a href='%unsubscribe_url%'>Unsubscribe</a><img src='http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=".$ma_l_id."&c_p=".$m_ad_uid ."-".$mailing_id ."&send_image=0' width='1' height='1'>";
				
				$h2t =& new html2text($sponsoredBody);
				$textVersion = $h2t->get_text();
				
				//print_r($strArray);
				try {
					$batchMsg->setFromAddress($fromEmail, array("first"=>$siteName));
					$batchMsg->setSubject($sponsoredTitle);
					$batchMsg->setHtmlBody($sponsoredBody);
					$batchMsg->setTextBody($textVersion);
					$batchMsg->addCampaignId('DASSOCEX-'.$mailing_id); 
					$batchMsg->addCustomData("MailingId", 'DASSOCEX-'.$mailing_id); 
					print_r($batchMsg);
					$result = $batchMsg->finalize();	
					
					$this->tblMailing->updateAll(
					array('tblMailing.m_esp_id' => "'DASSOCEX-".$mailing_id."'", 'tblMailing.m_sent_date' => "'" . date('Y-m-d H:i:s') . "'", 'tblMailing.m_schedule_date' =>"'" .  date('Y-m-d H:i:s') . "'"),
					array('tblMailing.m_id' => $mailing_id));  
					echo 'Successfully sent all messages.';
					//print_r($batchMsg);
					} catch(Exception $e) {
					$emails = array();
					foreach($tblSubscribers as $tblSubscriber)
					{
						$emails[] = $tblSubscriber['tbl_email_segments']['es_email'];
					}
					$condition = array('tblMessage.msg_email_id in' => $emails, 'msg_m_id' => $mailing_id);
					//$this->tblMessage->deleteAll($condition,false);
					echo 'Message: ' .$e->getMessage();
				}
				
			}
			$this->autoRender = false;
		}
		
		public function DedicatedEmailTrigger()
		{
			header('Content-Type: application/json');
			App::import('Vendor', 'src/html2text');
			App::import('Vendor', array('file' => 'autoload'));	
			
			$mailing_id = $this->request->param('mailing_id');
			$email = $this->request->param('email');
			$physical_address = "";
			$esp_id = 6;
			
			
			$raa = rand(pow(10, 4), pow(10, 3)-1);
			
			$m_ad_uid = $this->tblMailing->field(
			'm_ad_uid',
			array('m_id' => $mailing_id)
			);
			
			$options_ad['joins'] = array(
			array('table' => 'tbl_mapped_adunits',
			'alias' => 'tma',
			'type' => 'INNER',
			'conditions' => array(
			'tma.ma_ad_id = tblAdunit.ad_dfp_id'
			)),
			array('table' => 'tbl_esp_settings',
			'alias' => 'tes',
			'type' => 'INNER',
			'conditions' => array(
			'tes.es_ad_id = tblAdunit.adunit_id'
			)),
			array('table' => 'tbl_email_service_providers',
			'alias' => 'tesp',
			'type' => 'INNER',
			'conditions' => array(
			'tes.es_esp_id = tesp.esp_id'
			))
			);
			
			$options_ad['conditions'] = array('tma.ma_uid' => $m_ad_uid, 'tes.es_esp_id' => $esp_id);
			$options_ad['fields'] = array('tblAdunit.ad_uid', 'tblAdunit.adunit_id', 'tblAdunit.mailling_address_footer', 'tblAdunit.automatic_address_footer', 'tblAdunit.publisher_id', 'tblAdunit.sender_email', 'tes.es_from_email', 'tes.es_private_key', 'tes.es_site_domain', 'tes.es_tracking_domain', 'tes.es_limit', 'tesp.esp_mail_tag', 'tesp.esp_unsubscribe_tag','tma.ma_ad_id','tma.ma_l_id');
			$adunitSettings = $this->tblAdunit->find('first',$options_ad);

			$aduid = $adunitSettings['tblAdunit']['ad_uid'];
			
			$pub_id = $adunitSettings['tblAdunit']['publisher_id'];
			
			$mail_tag = $adunitSettings['tesp']['esp_mail_tag'];
			
			$unsub_tag = $adunitSettings['tesp']['esp_unsubscribe_tag'];
			
			$aduidDFP = $adunitSettings['tma']['ma_ad_id'];
			
			$ma_l_id = $adunitSettings['tma']['ma_l_id'];

			$automatic_address_footer = $adunitSettings['tblAdunit']['automatic_address_footer'];
			
			$physical_address = $adunitSettings['tblAdunit']['mailling_address_footer'];
			
			$fromEmail = $adunitSettings['tes']['es_from_email'];
			$MAILGUNAPP_API_KEY = $adunitSettings['tes']['es_private_key'];
			$mg = new Mailgun\Mailgun($MAILGUNAPP_API_KEY);
			$siteName = $adunitSettings['tblAdunit']['sender_email'];
			$domain = $adunitSettings['tes']['es_site_domain'];
			$batchMsg = $mg->BatchMessage($domain);
			$isTest = 0;
			$limit = $adunitSettings['tes']['es_limit'];
			
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			$m_li_crtid = $this->tblMailing->field(
			'm_li_crtid',
			array('m_id' => $mailing_id)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			if($isTest == "1")
			$tblSubscribers = "";
			else
			$tblSubscribers = $this->Dynamic->Query("SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." and msg_email_id='" . $email ."'");
			
			
			print_r($tblSubscribers);
			
			//$finalurl = $url . "?idsite=" . $tracksite_id . "&cid=" . $m_li_crtid . "&mid=" . $mailing_id . "&uid=" . $m_ad_uid . "&redirect=" . urlencode($redirect_uri); 
			
			$ulliItems = '';
			$fullhtml = '';
			$firstTitle = '';
			$i = 0;
			echo "dd";
			if(empty($tblSubscribers))
			{
				$tblSubscribers = array();
				$tblSubscribers[0]['trigger']['nl_email'] = $email;
				
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber['trigger']['nl_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$message_id = $this->tblMessage->getLastInsertID();
						$strArray[] = array('email'=>''.$tblSubscriber['trigger']['nl_email'].'', 'name'=>'', 'last'=>'');
						$batchMsg->addToRecipient($tblSubscriber['trigger']['nl_email'], array("first"=> '', "last" => ''));
					}
				}
				
				$options2['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => 'text/html', 'tbc.cr_id' => $m_li_crtid);
				$options2['joins'] = array(
				array('table' => 'tbl_line_items',
				'alias' => 'tli',
				'type' => 'INNER',
				'conditions' => array(
				'tli.li_id = tblMappedAdunit.ma_l_id')),
				array('table' => 'tbl_creatives',
				'alias' => 'tbc', 
				'type' => 'INNER',
				'conditions' => array(
				'tbc.cr_lid = tli.li_dfp_id'
				)));
				$options2['fields'] = array('tbc.*','tblMappedAdunit.*');
				$sponsoredCreative = $this->tblMappedAdunit->find('first', $options2);
				$sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
				$sponsoredRedirect = $sponsoredCreative['tbc']['cr_redirect_url'];
				$sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&send_image=0";
				$sponsoredBody = $sponsoredCreative['tbc']['cr_body'] . $automatic_address_footer . "<br /><br /> " . $physical_address . "<br /><br /> Please click here to <a href='%unsubscribe_url%'>Unsubscribe</a><img src='http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=".$ma_l_id."&c_p=".$m_ad_uid ."-".$mailing_id ."&send_image=0' width='1' height='1'>";
				
				$h2t =& new html2text($sponsoredBody);
				$textVersion = $h2t->get_text();
				
				try {
					$batchMsg->setFromAddress($fromEmail, array("first"=>$siteName));
					$batchMsg->setSubject($sponsoredTitle);
					$batchMsg->setHtmlBody($sponsoredBody);
					$batchMsg->setTextBody($textVersion);
					$batchMsg->addCampaignId('DASSOCEX-'.$mailing_id); 
					$batchMsg->addCustomData("MailingId", 'DASSOCEX-'.$mailing_id); 
					//print_r($batchMsg);
					$result = $batchMsg->finalize();	
					
					$this->tblMailing->updateAll(
					array('tblMailing.m_esp_id' => "'DASSOCEX-".$mailing_id."'", 'tblMailing.m_sent_date' => "'" . date('Y-m-d H:i:s') . "'", 'tblMailing.m_schedule_date' =>"'" .  date('Y-m-d H:i:s') . "'"),
					array('tblMailing.m_id' => $mailing_id));  
					echo 'Successfully sent all messages.';
					//print_r($batchMsg);
					} catch(Exception $e) {
					/*
					$emails = array();
					foreach($tblSubscribers as $tblSubscriber)
					{
						$emails[] = $tblSubscriber['trigger']['nl_email'];
					}
					$condition = array('tblMessage.msg_email_id in' => $emails, 'msg_m_id' => $mailing_id);
					$this->tblMessage->deleteAll($condition,false);*/
					echo 'Message: ' .$e->getMessage();
				}
				
			}
			
			$this->autoRender = false;
			
		}
		
		public function rssFeedTemplate1() {
			App::import('Vendor', 'src/html2text');
			App::import('Vendor', array('file' => 'autoload'));	
			$mailing_id = 0;
			
			$fromEmail = $this->request->param('fromEmail');
			$MAILGUNAPP_API_KEY = $this->request->param('MAILGUNAPP_API_KEY');
			$mg = new Mailgun\Mailgun($MAILGUNAPP_API_KEY);
			$siteName = $this->request->param('siteName');
			$domain = $this->request->param('domain');
			$url = $this->request->param('url');
			$url_feed = $this->request->param('url_feed');
			$batchMsg = $mg->BatchMessage($domain);
			$m_ad_uid = $this->request->param('m_ad_uid');
			$isTest = $this->request->param('isTest');
			
			$ma_l_id = $this->tblMappedAdunit->field(
			'ma_l_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduidDFP = $this->tblMappedAdunit->field(
			'ma_ad_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduid = $this->tblAdunit->field(
			'ad_uid',
			array('ad_dfp_id' => $aduidDFP)
			);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			echo "found=". $mailing_id = $this->tblMailing->field(
			'm_id',
			array('m_ad_uid' => $m_ad_uid, 'isNewsletter' => 1),
			'm_sent_date DESC'
			);
			if(!$mailing_id)
			{
				echo $date = date('Y-m-d H:i:s');
				$this->request->data['tblMailing']['m_ad_uid'] = $m_ad_uid;
				$this->request->data['tblMailing']['m_sl'] = '';
				$this->request->data['tblMailing']['isNewsletter'] = 1;
				$this->request->data['tblMailing']['m_sent_date'] = "'" . $date . "'";
				$this->request->data['tblMailing']['m_schedule_date'] = "'" . $date . "'";
				$this->tblMailing->create();
				if ($this->tblMailing->save($this->request->data))
				{
					$mailing_id = $this->tblMailing->getLastInsertID();
				}
				} else {
				$count = $this->Dynamic->Query("SELECT count(*) as count FROM " . $table_name . " WHERE nl_isActive=1 and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." )");
				//print_r($count);
				//echo "count=". $count[0][0]['count'];
				if($count[0][0]['count'] == 0){
					$this->request->data['tblMailing']['m_ad_uid'] = $m_ad_uid;
					$this->request->data['tblMailing']['m_sl'] = '';
					$this->request->data['tblMailing']['isNewsletter'] = 1;
					$this->request->data['tblMailing']['m_sent_date'] = "'" . date('Y-m-d H:i:s') . "'";
					$this->request->data['tblMailing']['m_schedule_date'] = "'" . date('Y-m-d H:i:s') . "'";
					$this->tblMailing->create();
					if ($this->tblMailing->save($this->request->data))
					{
						$mailing_id = $this->tblMailing->getLastInsertID();
					}
				}
			}
			
			echo "final=". $mailing_id;
			
			
			
			if($isTest == "1")
			{
				$tblSubscribers = array();
				$tblSubscribers[0][$table_name]['nl_email'] = "sunny.gulati@siliconbiztech.com";
			}
			else
			{
				$tblSubscribers = $this->Dynamic->Query("SELECT * FROM " . $table_name . " WHERE nl_isActive=1 and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , 80");
			}
			
			if(!empty($tblSubscribers))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber[$table_name]['nl_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$myArray = array();
						$message_id = $this->tblMessage->getLastInsertID();
						$strArray[] = array('email'=>''.$tblSubscriber[$table_name]['nl_email'].'', 'name'=>'', 'type'=>'to');
						$batchMsg->addToRecipient($tblSubscriber[$table_name]['nl_email'], array("first"=>"", "last" => ""));
						//$strArray[] = $myArray;
					}
				}
				
				$htmlmessage = $this->get_content($url . $mailing_id);
				
				$h2t =& new html2text($htmlmessage);
				$textVersion = $h2t->get_text();
				
				$rssfeed = $this->getFeed($url_feed);
				$firstTitle = $rssfeed[0]['title'];
				print_r($strArray);
				try {
					$batchMsg->setFromAddress($fromEmail, array("first"=> $siteName));
					$batchMsg->setSubject($firstTitle);
					$batchMsg->setHtmlBody($htmlmessage);
					$batchMsg->setTextBody($textVersion);
					$batchMsg->addCampaignId('DASSOCEX-'.$mailing_id); 
					$batchMsg->addCustomData("MailingId", 'DASSOCEX-'.$mailing_id); 
					//print_r($batchMsg);
					$result = $batchMsg->finalize();	
					
					$this->tblMailing->updateAll(
					array('tblMailing.m_esp_id' => "'DASSOCEX-".$mailing_id."'", 'tblMailing.m_sent_date' => "'" . date('Y-m-d H:i:s') . "'", 'tblMailing.m_schedule_date' =>"'" .  date('Y-m-d H:i:s') . "'",
					'tblMailing.m_sl' => "'".$firstTitle."'"),
					array('tblMailing.m_id' => $mailing_id));  
					echo 'Successfully sent all messages.';
					//print_r($batchMsg);
					} catch(Exception $e) {
					echo 'Message: ' .$e->getMessage();
				}
				
			}
			$this->autoRender = false;
		}
		
		public function rssFeedTemplateLive() {
			App::import('Vendor', 'src/html2text');
			App::import('Vendor', array('file' => 'autoload'));	
			$mailing_id = 0;
			
			$m_ad_uid = $this->request->param('m_ad_uid');
			$esp_id = $this->request->param('esp_id');
			$SubjectLine = $this->request->param('SubjectLine');
			$url_nl = $this->request->param('url_nl');
			
			$options_ad['joins'] = array(
			array('table' => 'tbl_mapped_adunits',
			'alias' => 'tma',
			'type' => 'INNER',
			'conditions' => array(
			'tma.ma_ad_id = tblAdunit.ad_dfp_id'
			)),
			array('table' => 'tbl_esp_settings',
			'alias' => 'tes',
			'type' => 'INNER',
			'conditions' => array(
			'tes.es_ad_id = tblAdunit.adunit_id'
			)),
			array('table' => 'tbl_email_service_providers',
			'alias' => 'tesp',
			'type' => 'INNER',
			'conditions' => array(
			'tes.es_esp_id = tesp.esp_id'
			))
			);
			
			$options_ad['conditions'] = array('tma.ma_uid' => $m_ad_uid, 'tes.es_esp_id' => $esp_id);
			$options_ad['fields'] = array('tblAdunit.ad_uid', 'tblAdunit.adunit_id', 'tblAdunit.publisher_id', 'tblAdunit.sender_email', 'tes.es_from_email', 'tes.es_private_key', 'tes.es_site_domain', 'tes.es_test', 'tes.es_limit', 'tesp.esp_mail_tag', 'tesp.esp_unsubscribe_tag');
			$adunitSettings = $this->tblAdunit->find('first',$options_ad);

			$aduid = $adunitSettings['tblAdunit']['ad_uid'];
			
			$pub_id = $adunitSettings['tblAdunit']['publisher_id'];
			
			$mail_tag = $adunitSettings['tesp']['esp_mail_tag'];
			
			$unsub_tag = $adunitSettings['tesp']['esp_unsubscribe_tag'];

			$fromEmail = $adunitSettings['tes']['es_from_email'];
			$MAILGUNAPP_API_KEY = $adunitSettings['tes']['es_private_key'];
			$mg = new Mailgun\Mailgun($MAILGUNAPP_API_KEY);
			$siteName = $adunitSettings['tblAdunit']['sender_email'];
			$domain = $adunitSettings['tes']['es_site_domain'];
			$batchMsg = $mg->BatchMessage($domain);
			$isTest = $adunitSettings['tes']['es_test'];
			$limit = $adunitSettings['tes']['es_limit'];
			
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			echo "found=". $mailing_id = $this->tblMailing->field(
			'm_id',
			array('m_ad_uid' => $m_ad_uid, 'isNewsletter' => 1),
			'm_sent_date DESC'
			);
			if(!$mailing_id)
			{
				echo $date = date('Y-m-d H:i:s');
				$this->request->data['tblMailing']['m_ad_uid'] = $m_ad_uid;
				$this->request->data['tblMailing']['m_sl'] = '';
				$this->request->data['tblMailing']['isNewsletter'] = 1;
				$this->request->data['tblMailing']['m_sent_date'] = "'" . $date . "'";
				$this->request->data['tblMailing']['m_schedule_date'] = "'" . $date . "'";
				$this->tblMailing->create();
				if ($this->tblMailing->save($this->request->data))
				{
					$mailing_id = $this->tblMailing->getLastInsertID();
				}
				} else {
				$count = $this->Dynamic->Query("SELECT count(*) as count FROM " . $table_name . " WHERE nl_isActive=1 and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." )");
				//print_r($count);
				//echo "count=". $count[0][0]['count'];
				if($count[0][0]['count'] == 0){
					$check_cycle = $this->tblMailing->field(
					'm_id',
					array('m_ad_uid' => $m_ad_uid, 'isNewsletter' => 1, 'cast(created_at as date)' => date('Y-m-d')),
					'm_sent_date DESC'
					);
					if($check_cycle)
						die();
					
					$this->request->data['tblMailing']['m_ad_uid'] = $m_ad_uid;
					$this->request->data['tblMailing']['m_sl'] = '';
					$this->request->data['tblMailing']['isNewsletter'] = 1;
					$this->request->data['tblMailing']['m_sent_date'] = "'" . date('Y-m-d H:i:s') . "'";
					$this->request->data['tblMailing']['m_schedule_date'] = "'" . date('Y-m-d H:i:s') . "'";
					$this->tblMailing->create();
					if ($this->tblMailing->save($this->request->data))
					{
						$mailing_id = $this->tblMailing->getLastInsertID();
					}
				}
			}
			
			echo "final=". $mailing_id;
			
			
			
			if($isTest == "1")
			{
				$tblSubscribers = array();
				$tblSubscribers[0][$table_name]['nl_email'] = "sunny.gulati@siliconbiztech.com";
				$tblSubscribers[1][$table_name]['nl_email'] = "cdautorio@gmail.com";
				$tblSubscribers[2][$table_name]['nl_email'] = "chris@siliconbiztech.com";
				$tblSubscribers[3][$table_name]['nl_email'] = "siliconbt@gmail.com";
				$tblSubscribers[4][$table_name]['nl_email'] = "Sandeep.bansal@siliconbiztech.com"; 
			}
			else
			{
				$tblSubscribers = $this->Dynamic->Query("SELECT * FROM " . $table_name . " WHERE nl_isActive=1 and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , ". $limit);
			}
			
			if(!empty($tblSubscribers))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber[$table_name]['nl_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$myArray = array();
						$message_id = $this->tblMessage->getLastInsertID();
						$strArray[] = array('email'=>''.$tblSubscriber[$table_name]['nl_email'].'', 'name'=>'', 'type'=>'to');
						$batchMsg->addToRecipient($tblSubscriber[$table_name]['nl_email'], array("first"=>"", "last" => ""));
						//$strArray[] = $myArray;
					}
				}
				
				$htmlmessage = $this->get_content($url_nl . $mailing_id . "/" . $esp_id);
				
				if(!$htmlmessage)
				{
					mail("sunny.gulati@siliconbiztech.com", "NL Alert", "Its Blank");
					die();
				}
				$h2t =& new html2text($htmlmessage);
				$textVersion = $h2t->get_text();
				
				try {
					$batchMsg->setFromAddress($fromEmail, array("first"=> $siteName));
					$batchMsg->setSubject($SubjectLine);
					$batchMsg->setHtmlBody($htmlmessage);
					$batchMsg->setTextBody($textVersion);
					$batchMsg->addCampaignId('DASSOCEX-'.$mailing_id); 
					$batchMsg->addCustomData("MailingId", 'DASSOCEX-'.$mailing_id); 
					//print_r($batchMsg);
					$result = $batchMsg->finalize();	
					
					$this->tblMailing->updateAll(
					array('tblMailing.m_esp_id' => "'DASSOCEX-".$mailing_id."'", 'tblMailing.m_sent_date' => "'" . date('Y-m-d H:i:s') . "'", 'tblMailing.m_schedule_date' =>"'" .  date('Y-m-d H:i:s') . "'",
					'tblMailing.m_sl' => "'".$SubjectLine."'"),
					array('tblMailing.m_id' => $mailing_id));  
					echo 'Successfully sent all messages.';
					//print_r($batchMsg);
					} catch(Exception $e) {
					echo 'Message: ' .$e->getMessage();
				}
				
			}
			$this->autoRender = false;
		}
		
		public function OTCTemplate() {
			App::import('Vendor', 'src/html2text');
			App::import('Vendor', array('file' => 'autoload'));	
			$mailing_id = 0;
			
			$url = $this->request->param('url');
			$siteName = $this->request->param('siteName');
			$siteURL = $this->request->param('siteURL');
			$fromEmail = $this->request->param('fromEmail');
			$physical_address = $this->request->param('physical_address');
			$MAILGUNAPP_API_KEY = $this->request->param('MAILGUNAPP_API_KEY');
			$mg = new Mailgun\Mailgun($MAILGUNAPP_API_KEY);
			$domain = $this->request->param('domain');
			$batchMsg = $mg->BatchMessage($domain);
			$m_ad_uid = $this->request->param('m_ad_uid');
			$isTest = $this->request->param('isTest');
			
			$mailing_id = $this->tblMailing->field(
			'm_id',
			array('m_ad_uid' => $m_ad_uid, 'cast(created_at as date)' => date('Y-m-d'))
			);
			
			if(!$mailing_id)
			{
				$this->request->data['tblMailing']['m_ad_uid'] = $m_ad_uid;
				$this->request->data['tblMailing']['m_sl'] = '';
				$this->request->data['tblMailing']['m_sent_date'] = "'" . date('Y-m-d H:i:s') . "'";
				$this->request->data['tblMailing']['m_schedule_date'] = "'" . date('Y-m-d H:i:s') . "'";
				$this->tblMailing->create();
				if ($this->tblMailing->save($this->request->data))
				{
					$mailing_id = $this->tblMailing->getLastInsertID();
				}
			}
			
			$raa = rand(pow(10, 4), pow(10, 3)-1);
			
			if($isTest == "1")
			$SubjectLine = "[PROOF: " . $raa . "-" . $mailing_id . "]" . $this->request->param('SubjectLine');
			else
			$SubjectLine = $this->request->param('SubjectLine');
			
			$m_ad_uid = $this->tblMailing->field(
			'm_ad_uid',
			array('m_id' => $mailing_id)
			);
			
			$ma_l_id = $this->tblMappedAdunit->field(
			'ma_l_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$m_li_crtid = $this->tblMailing->field(
			'm_li_crtid',
			array('m_id' => $mailing_id)
			);
			
			$aduidDFP = $this->tblMappedAdunit->field(
			'ma_ad_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduid = $this->tblAdunit->field(
			'ad_uid',
			array('ad_dfp_id' => $aduidDFP)
			);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			
			if($isTest == "1")
			{
				$tblSubscribers = array();
				$tblSubscribers[0]['tbl_email_segments']['es_email'] = "sunny.gulati@siliconbiztech.com";
				//$tblSubscribers[0]['tbl_email_segments']['es_email'] = "sandeep.bansal@siliconbiztech.com";
			}
			else
			$tblSubscribers = '';//$this->Dynamic->Query("SELECT * FROM tbl_email_segments WHERE es_ml_id=" . $mailing_id ." and es_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , 1");
			
			
			print_r($tblSubscribers);
			
			
			$ulliItems = '';
			$fullhtml = '';
			$firstTitle = 'OTC Rockstar Penny Stock Performance : '. date("F j, Y");
			$i = 0;
			
			
			if(!empty($tblSubscribers))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber['tbl_email_segments']['es_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$myArray = array();
						$message_id = $this->tblMessage->getLastInsertID();
						$strArray[] = array('email'=>''.$tblSubscriber['tbl_email_segments']['es_email'].'', 'name'=>'', 'type'=>'to');
						$batchMsg->addToRecipient($tblSubscriber['tbl_email_segments']['es_email'], array("first"=>"", "last" => ""));
						//$strArray[] = $myArray;
					}
				}
				
				//print_r($batchMsg);
				
				
				$htmlmessage = $this->get_content("http://quote.stockquotes.finance/dailyTopStocks.php");
				
				$h2t =& new html2text($htmlmessage);
				$textVersion = $h2t->get_text();
				
				//print_r($strArray);
				try {
					$batchMsg->setFromAddress("newsletter@das0.net", array("first"=>"OTC", "last" => "RockStar"));
					$batchMsg->setSubject($firstTitle);
					$batchMsg->setHtmlBody($htmlmessage);
					$batchMsg->setTextBody($textVersion);
					$batchMsg->addCampaignId('DASSOCEX-'.$mailing_id); 
					$batchMsg->addCustomData("MailingId", 'DASSOCEX-'.$mailing_id); 
					//print_r($batchMsg);
					$result = $batchMsg->finalize();	
					
					$this->tblMailing->updateAll(
					array('tblMailing.m_esp_id' => "'DASSOCEX-".$mailing_id."'", 'tblMailing.m_sent_date' => "'" . date('Y-m-d H:i:s') . "'", 'tblMailing.m_schedule_date' =>"'" .  date('Y-m-d H:i:s') . "'",
					'tblMailing.m_sl' => "'".$firstTitle."'"),
					array('tblMailing.m_id' => $mailing_id));  
					echo 'Successfully sent all messages.';
					//print_r($batchMsg);
					} catch(Exception $e) {
					echo 'Message: ' .$e->getMessage();
				}
				
			}
			$this->autoRender = false;
		}
		
		
		
		public function webhook() {		
			$MailingId = str_replace("\"","",$_POST['MailingId']);
			$strResp = serialize($_POST);		
			//mail("sunny.gulati@siliconbiztech.com","Maigun",$strResp);
			switch($_POST['event']) 
			{
				case 'delivered' :
				$conditions = array(
				'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'],
				'UserLevelEmailPerformance.ListId' => $MailingId
				);
				if ($this->UserLevelEmailPerformance->hasAny($conditions)){
					//do something
					$this->UserLevelEmailPerformance->updateAll(
					array('UserLevelEmailPerformance.Sent' => '1', 'UserLevelEmailPerformance.MailingDate' => "'" . date("Y-m-d H:i:s", $_POST['timestamp']) . "'"),       
					array('UserLevelEmailPerformance.ListId' => $MailingId
					,'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'])
					);
					
					} else {
					$this->request->data['UserLevelEmailPerformance']['ListId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $_POST['Message-Id'];
					$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['recipient'];
					$this->request->data['UserLevelEmailPerformance']['MailingDate'] = date("Y-m-d H:i:s", $_POST['timestamp']);
					$this->request->data['UserLevelEmailPerformance']['Sent'] = 1;
					$this->request->data['UserLevelEmailPerformance']['esp_id'] = 6;
					$this->UserLevelEmailPerformance->create();
					$this->UserLevelEmailPerformance->save($this->request->data);
				}
				break;
				
				case 'dropped' :
				$conditions = array(
				'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'],
				'UserLevelEmailPerformance.ListId' => $MailingId
				);
				if ($this->UserLevelEmailPerformance->hasAny($conditions)){
					$this->UserLevelEmailPerformance->updateAll(
					array('UserLevelEmailPerformance.Bounced' => '1', 'UserLevelEmailPerformance.BouncedAt' => "'" . date("Y-m-d H:i:s", $_POST['timestamp']) . "'",
					'UserLevelEmailPerformance.BounceReason' => "'" . $_POST['reason'] . "#" . $_POST['code'] . "#" . $_POST['description'] . "'"),       
					array('UserLevelEmailPerformance.ListId' => $MailingId
					,'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'])
					);
					} else {
					$this->request->data['UserLevelEmailPerformance']['ListId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['recipient'];
					$this->request->data['UserLevelEmailPerformance']['BouncedAt'] = date("Y-m-d H:i:s", $_POST['timestamp']);
					$this->request->data['UserLevelEmailPerformance']['localIp'] = '';
					$this->request->data['UserLevelEmailPerformance']['Bounced'] = 1;
					$this->request->data['UserLevelEmailPerformance']['esp_id'] = 6;
					$this->request->data['UserLevelEmailPerformance']['BounceReason'] = "'" . $_POST['reason'] . "#" . $_POST['code'] . "#" . $_POST['description'] . "'";
					$this->UserLevelEmailPerformance->create();
					$this->UserLevelEmailPerformance->save($this->request->data);	
				}
				break;
				
				case 'bounced' :
				$conditions = array(
				'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'],
				'UserLevelEmailPerformance.ListId' => $MailingId
				);
				if ($this->UserLevelEmailPerformance->hasAny($conditions)){
					$this->UserLevelEmailPerformance->updateAll(
					array('UserLevelEmailPerformance.Bounced' => '1', 'UserLevelEmailPerformance.BouncedAt' => "'" . date("Y-m-d H:i:s", $_POST['timestamp']) . "'",
					'UserLevelEmailPerformance.BounceReason' => "'" . $_POST['code'] . "#" . $_POST['error'] . "#" . $_POST['notification'] . "'"),       
					array('UserLevelEmailPerformance.ListId' => $MailingId
					,'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'])
					); 
					} else {
					$this->request->data['UserLevelEmailPerformance']['ListId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['recipient'];
					$this->request->data['UserLevelEmailPerformance']['BouncedAt'] = date("Y-m-d H:i:s", $_POST['timestamp']);
					$this->request->data['UserLevelEmailPerformance']['localIp'] = '';
					$this->request->data['UserLevelEmailPerformance']['Bounced'] = 1;
					$this->request->data['UserLevelEmailPerformance']['esp_id'] = 6;
					$this->request->data['UserLevelEmailPerformance']['BounceReason'] = "'" . $_POST['code'] . "#" . $_POST['error'] . "#" . $_POST['notification'] . "'";
					$this->UserLevelEmailPerformance->create();
					$this->UserLevelEmailPerformance->save($this->request->data);	
				}
				break;
				
				case 'opened' :
				$conditions = array(
				'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'],
				'UserLevelEmailPerformance.ListId' => $MailingId
				);
				if ($this->UserLevelEmailPerformance->hasAny($conditions)){
					$this->UserLevelEmailPerformance->incrementOpenCount($MailingId, $_POST['recipient'], '');
					} else {
					$this->request->data['UserLevelEmailPerformance']['ListId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['ExMessageId'] = '';
					$this->request->data['UserLevelEmailPerformance']['EmailAddress'] =  $_POST['recipient'];
					$this->request->data['UserLevelEmailPerformance']['OpenedAt'] = date("Y-m-d H:i:s", $_POST['timestamp']);
					$this->request->data['UserLevelEmailPerformance']['localIp'] = $_POST['ip'];
					$this->request->data['UserLevelEmailPerformance']['Opened'] = 1;
					$this->request->data['UserLevelEmailPerformance']['esp_id'] = 6;
					$this->UserLevelEmailPerformance->create();
					$this->UserLevelEmailPerformance->save($this->request->data);	
				}
				break;
				
				case 'clicked' :
				$conditions = array(
				'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'],
				'UserLevelEmailPerformance.ListId' => $MailingId
				);
				if ($this->UserLevelEmailPerformance->hasAny($conditions)){
					$this->UserLevelEmailPerformance->incrementClickCount($MailingId, $_POST['recipient'],'');
					} else {
					$this->request->data['UserLevelEmailPerformance']['ListId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['ExMessageId'] = '';
					$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['recipient'];
					$this->request->data['UserLevelEmailPerformance']['ClickedAt'] = date("Y-m-d H:i:s", $_POST['timestamp']);
					$this->request->data['UserLevelEmailPerformance']['localIp'] = $_POST['ip'];
					$this->request->data['UserLevelEmailPerformance']['Clicked'] = 1;
					$this->request->data['UserLevelEmailPerformance']['esp_id'] = 6;
					$this->UserLevelEmailPerformance->create();
					$this->UserLevelEmailPerformance->save($this->request->data);
				}
				break;
				
				case 'complained' :
				$conditions = array(
				'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'],
				'UserLevelEmailPerformance.ListId' => $MailingId
				);
				if ($this->UserLevelEmailPerformance->hasAny($conditions)){
					$this->UserLevelEmailPerformance->updateAll(
					array('UserLevelEmailPerformance.Unsubscribed' => '1',
					'UserLevelEmailPerformance.SpamComplaint' => '1', 'UserLevelEmailPerformance.UnsubscribedAt' => "'" . date("Y-m-d H:i:s", $_POST['timestamp']) . "'"),       
					array('UserLevelEmailPerformance.ListId' => $MailingId
					,'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'])
					);
					$m_ad_uid = $this->tblMailing->field(
					'm_ad_uid',
					array('m_id' => split("-",$MailingId)[1])
					);
					$aduidDFP = $this->tblMappedAdunit->field(
					'ma_ad_id',
					array('ma_uid' => $m_ad_uid)
					);
					$aduid = $this->tblAdunit->field(
					'ad_uid',
					array('ad_dfp_id' => $aduidDFP)
					);
					$pub_id = $this->tblAdunit->field(
					'publisher_id',
					array('ad_uid' => $aduid)
					);
					$table_name = $pub_id. "_" . $aduid;
					$this->Dynamic->useTable = $table_name; 
					$this->Dynamic->Query("Update " . $table_name . "  SET  nl_isActive=0 WHERE nl_email='" . $_POST['recipient'] ."'");
					} else {
					$this->request->data['UserLevelEmailPerformance']['ListId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $_POST['Message-Id'];
					$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['recipient'];
					$this->request->data['UserLevelEmailPerformance']['UnsubscribedAt'] = date("Y-m-d H:i:s", $_POST['timestamp']);
					$this->request->data['UserLevelEmailPerformance']['localIp'] = '';
					$this->request->data['UserLevelEmailPerformance']['Unsubscribed'] = 1;
					$this->request->data['UserLevelEmailPerformance']['SpamComplaint'] = 1;
					$this->request->data['UserLevelEmailPerformance']['esp_id'] = 6;
					$this->UserLevelEmailPerformance->create();
					$this->UserLevelEmailPerformance->save($this->request->data);	
					$m_ad_uid = $this->tblMailing->field(
					'm_ad_uid',
					array('m_id' => split("-",$MailingId)[1])
					);
					$aduidDFP = $this->tblMappedAdunit->field(
					'ma_ad_id',
					array('ma_uid' => $m_ad_uid)
					);
					$aduid = $this->tblAdunit->field(
					'ad_uid',
					array('ad_dfp_id' => $aduidDFP)
					);
					$pub_id = $this->tblAdunit->field(
					'publisher_id',
					array('ad_uid' => $aduid)
					);
					$table_name = $pub_id. "_" . $aduid;
					$this->Dynamic->useTable = $table_name; 
					$this->Dynamic->Query("Update " . $table_name . "  SET  nl_isActive=0 WHERE nl_email='" . $_POST['recipient'] ."'");
				}
				break;
				
				
				case 'unsubscribed' :
				$conditions = array(
				'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'],
				'UserLevelEmailPerformance.ListId' => $MailingId
				);
				if ($this->UserLevelEmailPerformance->hasAny($conditions)){
					$this->UserLevelEmailPerformance->updateAll(
					array('UserLevelEmailPerformance.Unsubscribed' => '1', 'UserLevelEmailPerformance.UnsubscribedAt' => "'" . date("Y-m-d H:i:s", $_POST['timestamp']) . "'"),       
					array('UserLevelEmailPerformance.ListId' => $MailingId
					,'UserLevelEmailPerformance.EmailAddress' => $_POST['recipient'])
					);
					$m_ad_uid = $this->tblMailing->field(
					'm_ad_uid',
					array('m_id' => split("-",$MailingId)[1])
					);
					
					$aduidDFP = $this->tblMappedAdunit->field(
					'ma_ad_id',
					array('ma_uid' => $m_ad_uid)
					);
					
					$aduid = $this->tblAdunit->field(
					'ad_uid',
					array('ad_dfp_id' => $aduidDFP)
					);
					
					$pub_id = $this->tblAdunit->field(
					'publisher_id',
					array('ad_uid' => $aduid)
					);
					
					$table_name = $pub_id. "_" . $aduid;
					$this->Dynamic->useTable = $table_name; 
					
					$this->Dynamic->Query("Update " . $table_name . "  SET  nl_isActive=0 WHERE nl_email='" . $_POST['recipient'] ."'");
					
					} else {
					$this->request->data['UserLevelEmailPerformance']['ListId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MailingId;
					$this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $_POST['Message-Id'];
					$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['recipient'];
					$this->request->data['UserLevelEmailPerformance']['UnsubscribedAt'] = date("Y-m-d H:i:s", $_POST['timestamp']);
					$this->request->data['UserLevelEmailPerformance']['localIp'] = $_POST['LocalIp'];
					$this->request->data['UserLevelEmailPerformance']['Unsubscribed'] = 1;
					$this->request->data['UserLevelEmailPerformance']['esp_id'] = 6;
					$this->UserLevelEmailPerformance->create();
					$this->UserLevelEmailPerformance->save($this->request->data);	
					$m_ad_uid = $this->tblMailing->field(
					'm_ad_uid',
					array('m_id' => split("-",$MailingId)[1])
					);
					$aduidDFP = $this->tblMappedAdunit->field(
					'ma_ad_id',
					array('ma_uid' => $m_ad_uid)
					);
					$aduid = $this->tblAdunit->field(
					'ad_uid',
					array('ad_dfp_id' => $aduidDFP)
					);
					$pub_id = $this->tblAdunit->field(
					'publisher_id',
					array('ad_uid' => $aduid)
					);
					$table_name = $pub_id. "_" . $aduid;
					$this->Dynamic->useTable = $table_name; 
					$this->Dynamic->Query("Update " . $table_name . "  SET  nl_isActive=0 WHERE nl_email='" . $_POST['recipient'] ."'");
				}
				break;
				
				default:
				break;				
			}
			$this->autoRender = false;
		}
		
		
		function get_content($URL){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $URL);
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;
		}
		
		private function verify($apiKey, $token, $timestamp, $signature)
		{
			//check if the timestamp is fresh
			if (time()-$timestamp>15) {
				return false;
			}
			
			//returns true if signature is valid
			return hash_hmac('sha256', $timestamp.$token, $apiKey) === $signature;
		}
		
		
		function getFeed($feed_url) {
			$rss = new DOMDocument();
			$rss->load($feed_url);
			$feed = array();
			foreach ($rss->getElementsByTagName('item') as $node) {
				$item = array ( 
				'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
				'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
				'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
				'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
				);
				array_push($feed, $item);
			}
			return $feed;
		}
		
	}												