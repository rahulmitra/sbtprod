<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class PublisherController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblProfile','Authake.tblAdunit','Authake.tblPlacement','Authake.Rule');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {
			
			$id = $this->Authake->getUserId();
			// form posted
			$this->set('title_for_layout','All AdUnits');
			$options['joins'] = array(
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.publisher_id = tblp.user_id')
			));
			
			$options['conditions'] = array('tblAdunit.publisher_id' => $id);
			
			$options['fields'] = array('tblAdunit.*', 'tblp.CompanyName');
			
			$tblAdunits = $this->tblAdunit->find('all', $options);
			//print_r($test);
			$this->set('group', $tblAdunits);
			
			
			/*
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
			App::import('Vendor', 'src/ExampleUtils');
			
			try {
				// Get DfpUser from credentials in "../auth.ini"
				// relative to the DfpUser.php file's directory.
				$user = new DfpUser();
				
				// Log SOAP XML request and response.
				$user->LogDefaults();
				
				// Get the CreativeService.
				$creativeService = $user->GetService('CreativeService', 'v201608');
				
				// Create a statement to select all creatives.
				$statementBuilder = new StatementBuilder();
				$statementBuilder->OrderBy('id ASC')
				->Limit(StatementBuilder::SUGGESTED_PAGE_LIMIT);
				
				// Default for total result set size.
				$totalResultSetSize = 0;
				
				do {
					// Get creatives by statement.
					$page = $creativeService->getCreativesByStatement(
					$statementBuilder->ToStatement());
					
					// Display results.
					if (isset($page->results)) {
						$totalResultSetSize = $page->totalResultSetSize;
						$i = $page->startIndex;
						foreach ($page->results as $creative) {
							printf("%d) Creative with ID %d, and name '%s' was found.\n", $i++,
							$creative->id, $creative->name);
						}
					}
					
					$statementBuilder->IncreaseOffsetBy(StatementBuilder::SUGGESTED_PAGE_LIMIT);
				} while ($statementBuilder->GetOffset() < $totalResultSetSize);
				
				printf("Number of results found: %d\n", $totalResultSetSize);
				} catch (OAuth2Exception $e) {
				ExampleUtils::CheckForOAuth2Errors($e);
				} catch (ValidationException $e) {
				ExampleUtils::CheckForOAuth2Errors($e);
				} catch (Exception $e) {
				printf("%s\n", $e->getMessage());
			}
			
			*/
			// form posted
			
		}
		
		
		public function all_placements() {
			$id = $this->Authake->getUserId();
			// form posted
			$this->set('title_for_layout','Add Publisher Placement');
			// form posted
			$options['joins'] = array(
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblPlacement.p_publisher_id = tblp.user_id')
			));
			
			$options['conditions'] = array('not' => array('tblPlacement.p_dfp_id' => 0), 'tblPlacement.p_publisher_id' => $id);
			
			$options['fields'] = array('tblPlacement.*', 'tblp.CompanyName');
			
			$tblPlacement = $this->tblPlacement->find('all', $options);
			//print_r($test);
			$this->set('group', $tblPlacement);
			
			
		}
		
	}						