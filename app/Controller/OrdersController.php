<?php

/**
 * Contact Controller
 * @author James Fairhurst <info@jamesfairhurst.co.uk>
 */
class OrdersController extends AppController {

    /**
     * Components
     */
    var $uses = array('Authake.Group', 'Authake.User', 'Authake.tblMailing', 'Authake.tblReportDay', 'Authake.tblLineItem', 'tblKmaResponse', 'Authake.tblAdunit', 'Authake.tblOrder', 'Dynamic', 'OrderLeads', 'Authake.tblCreative', 'Authake.tblMappedAdunit');
    var $components = array('RequestHandler', 'Authake.Filter', 'Session', 'Commonfunction', 'Reporting'); // var $layout = 'authake';
    var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc')); //var $scaffold;

    /**
     * Before Filter callback
     */

    public function beforeFilter() {
        parent::beforeFilter();

        // Change layout for Ajax requests
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
    }

    /**
     * Main index action
     */
    public function index($user_id = null) {

        $this->set('title_for_layout', 'All Orders');
        $userId = $this->Authake->getUserId();
        /*
          $options['joins'] = array(
          array('table' => 'tbl_profiles',
          'alias' => 'tblp',
          'type' => 'INNER',
          'conditions' => array(
          'tblOrder.advertiser_id = tblp.dfp_company_id')
          ));

          $options['conditions'] = array(
          'tblp.user_id' => $userId ); */

        $options['conditions'] = array('tblOrder.isActive' => 1);
        if (!empty($user_id)) {
            $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
        )));

        $options['fields'] = array('tblOrder.*', 'tblp.CompanyName');

        $options['order'] = array(
            'tblOrder.created_at' => 'DESC');

        $options['fields'] = array('tblOrder.*', 'tblp.*');

        $orders = $this->tblOrder->find('all', $options);
        //print_r($test);
        $this->set('results', $orders);

        $status = array();
        $channelPricing = array();
        $startDate = array();
        $endDate = array();
        $impressions = array();
        $clicks = array();
        $revenue = array();
        $totalValue = array();

        foreach ($orders as $order) {
            $options_l['conditions'] = array('tblLineItem.li_order_id' => $order['tblOrder']['dfp_order_id']);
            $lineitems = $this->tblLineItem->find("all", $options_l);
            $isExist = sizeof($lineitems);
            if ($isExist > 0) {
                $status[$order['tblOrder']['dfp_order_id']]['1'] = 0;
                $status[$order['tblOrder']['dfp_order_id']]['2'] = 0;
                $status[$order['tblOrder']['dfp_order_id']]['3'] = 0;
                $status[$order['tblOrder']['dfp_order_id']]['4'] = 0;
                $status[$order['tblOrder']['dfp_order_id']]['5'] = 0;
                $status[$order['tblOrder']['dfp_order_id']]['6'] = 0;
                $status[$order['tblOrder']['dfp_order_id']]['7'] = 0;
                $status[$order['tblOrder']['dfp_order_id']]['9'] = 0;
                $channelPricing[$order['tblOrder']['dfp_order_id']][1] = 0;
                $channelPricing[$order['tblOrder']['dfp_order_id']][2] = 0;
                $channelPricing[$order['tblOrder']['dfp_order_id']][3] = 0;
                $channelPricing[$order['tblOrder']['dfp_order_id']][4] = 0;
                $channelPricing[$order['tblOrder']['dfp_order_id']][5] = 0;
                $channelPricing[$order['tblOrder']['dfp_order_id']][6] = 0;
                $channelPricing[$order['tblOrder']['dfp_order_id']][7] = 0;
                $impressions[$order['tblOrder']['dfp_order_id']] = 0;
                $clicks[$order['tblOrder']['dfp_order_id']] = 0;
                $revenue[$order['tblOrder']['dfp_order_id']] = 0;
                $totalValue[$order['tblOrder']['dfp_order_id']] = 0;
                foreach ($lineitems as $lineitem) {
                    switch ($lineitem['tblLineItem']['li_status_id']) {
                        case 1 :
                            $status[$order['tblOrder']['dfp_order_id']]['1'] += 1;
                            break;
                        case 2 :
                            $status[$order['tblOrder']['dfp_order_id']]['2'] += 1;
                            break;
                        case 3 :
                            $status[$order['tblOrder']['dfp_order_id']]['3'] += 1;
                            break;
                        case 4 :
                            $status[$order['tblOrder']['dfp_order_id']]['4'] += 1;
                            break;
                        case 5 :
                            $status[$order['tblOrder']['dfp_order_id']]['5'] += 1;
                            break;
                        case 6 :
                            $status[$order['tblOrder']['dfp_order_id']]['6'] += 1;
                            break;
                        case 7 :
                            $status[$order['tblOrder']['dfp_order_id']]['7'] += 1;
                        case 9 :
                            $status[$order['tblOrder']['dfp_order_id']]['9'] += 1;
                            break;
                    }

                    switch ($lineitem['tblLineItem']['li_cs_id']) {
                        case 1 :
                            $channelPricing[$order['tblOrder']['dfp_order_id']]['1'] += 1;
                            break;
                        case 2 :
                            $channelPricing[$order['tblOrder']['dfp_order_id']]['2'] += 1;
                            break;
                        case 3 :
                            $channelPricing[$order['tblOrder']['dfp_order_id']]['3'] += 1;
                            break;
                        case 4 :
                            $channelPricing[$order['tblOrder']['dfp_order_id']]['4'] += 1;
                            break;
                        case 5 :
                            $channelPricing[$order['tblOrder']['dfp_order_id']]['5'] += 1;
                            break;
                        case 6 :
                            $channelPricing[$order['tblOrder']['dfp_order_id']]['6'] += 1;
                            break;
                    }
                    //echo $lineitem['tblLineItem']['li_revenue'];
                    $startDate[$order['tblOrder']['dfp_order_id']] = $lineitem['tblLineItem']['li_start_date'];
                    $endDate[$order['tblOrder']['dfp_order_id']] = $lineitem['tblLineItem']['li_end_date'];
                    $impressions[$order['tblOrder']['dfp_order_id']] += $lineitem['tblLineItem']['li_imp'];
                    $clicks[$order['tblOrder']['dfp_order_id']] += $lineitem['tblLineItem']['li_clicks'];
                    $revenue[$order['tblOrder']['dfp_order_id']] += $lineitem['tblLineItem']['li_revenue'];
                    $totalValue[$order['tblOrder']['dfp_order_id']] += $lineitem['tblLineItem']['li_total'];
                }
            }
        }
        $this->set('status', $status);
        $this->set('channelPricing', $channelPricing);
        $this->set('startDate', $startDate);
        $this->set('endDate', $endDate);
        $this->set('impressions', $impressions);
        $this->set('clicks', $clicks);
        $this->set('revenue', $revenue);
        $this->set('totalValue', $totalValue);
        //print_r($d);
    }

    public function pubOrders() {
        $userId = $this->Authake->getUserId();

        $options['joins'] = array(
            array('table' => 'tbl_line_items',
                'alias' => 'tli',
                'type' => 'INNER',
                'conditions' => array(
                    'tli.li_order_id = tblOrder.dfp_order_id'
                )),
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tma',
                'type' => 'INNER',
                'conditions' => array(
                    'tma.ma_l_id = tli.li_id'
                )),
            array('table' => 'tbl_adunits',
                'alias' => 'tad',
                'type' => 'INNER',
                'conditions' => array(
                    'tma.ma_ad_id = tad.ad_dfp_id'
                ))
        );

        $options['fields'] = array('tblOrder.*');

        $options['conditions'] = array(
            'tad.publisher_id' => $userId);

        $options['fields'] = array('tblOrder.*');

        $test = $this->tblOrder->find('all', $options);
        print_r($test);
        $this->set('group', $test);
    }

    public function view_line_items($li_order_id) {
        $options['joins'] = array(
            array('table' => 'tbl_cost_structures',
                'alias' => 'tcs',
                'type' => 'INNER',
                'conditions' => array(
                    'tcs.cs_id = tblLineItem.li_cs_id'
                )),
            array('table' => 'tbl_lineitem_status',
                'alias' => 'tls',
                'type' => 'INNER',
                'conditions' => array(
                    'tls.ls_id = tblLineItem.li_status_id'
                ))
        );

        $options['fields'] = array('tblLineItem.*', 'tcs.*', 'tls.*');

        $options['conditions'] = array('tblLineItem.li_order_id' => $li_order_id);
        $tblLineItems = $this->tblLineItem->find("all", $options);
        $isExist = sizeof($tblLineItems);
        $order_name = $this->tblOrder->field('order_name', array('dfp_order_id' => $li_order_id));
        if ($isExist > 0) {
            $type = array();
            $lid = array();
            foreach ($tblLineItems as $user) {
                $type[$user['tblLineItem']['li_dfp_id']] = $user['tblLineItem']['li_cs_id'];
                $lid[$user['tblLineItem']['li_dfp_id']] = $user['tblLineItem']['li_id'];
            }

            $this->set('tblLineItems', $tblLineItems);
            $this->set('type', $type);
            $this->set('lid', $lid);
            $this->set('order_name', $order_name);
            $this->set('order_id', $li_order_id);
        } else {
            $this->set('tblLineItems', "");
            $this->set('type', "");
            $this->set('lid', "");
            $this->set('order_name', $order_name);
            $this->set('order_id', $li_order_id);
        }
    }

    public function view_line_creatives($line_id) {
        $this->loadModel('Authake.tblFlight');
        $line_name = $this->tblLineItem->field('li_name', array('li_id' => $line_id));
        $li_dfp_id = $this->tblLineItem->field('li_dfp_id', array('li_id' => $line_id));
        $li_order_id = $this->tblLineItem->field('li_order_id', array('li_id' => $line_id));

        $optionm['conditions'] = array('tblMappedAdunit.ma_l_id' => $line_id, 'tblMappedAdunit.ma_isActive' => '1');
        $optionm['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = tblp.user_id')));
        $optionm['fields'] = array('tblMappedAdunit.*', 'tblAdunit.*', 'tblp.*');
        $mappedUnits = $this->tblMappedAdunit->find('all', $optionm);



        $creatives = array();
        $j = 0;

        // get creative list
        $options['conditions'] = array('tblCreative.cr_lid' => $li_dfp_id);
        $this->tblCreative->bindModel(array('hasMany' => array('FlightsCreative' =>array('foreignKey' => 'cr_id', 'conditions' => array(), 'fields' => array('FlightsCreative.fl_id')))));

        $options['fields'] = array('tblCreative.cr_id','tblCreative.cr_lid','tblCreative.active_status','tblCreative.cr_name', 'tblCreative.cr_size', 'tblCreative.creatives_type', 'tblCreative.cr_status', 'tblCreative.cr_imp', 'tblCreative.cr_click', 'tblCreative.created_at', 'tblCreative.cr_preview_url', 'tblCreative.cr_header');
        $tblCreative = $this->tblCreative->find("all", $options);
        $flight_ids = array();
        $flightFound = 0;

        foreach ($mappedUnits as $mappedUnit) {

            $ma_id = $mappedUnit['tblMappedAdunit']['ma_id'];
            $size = $mappedUnit['tblAdunit']['ad_unit_sizes'];


            if (!empty($tblCreative)) {
                $found = 0;
                foreach ($tblCreative as $tblCre) {
                    if (!empty($tblCre['FlightsCreative'])) {
                        foreach ($tblCre['FlightsCreative'] as $flight) {
                            $flight_ids[$flight['fl_id']] = $flight['fl_id'];
                        }
                        $flightFound = 1;
                    }
					
                    if ($tblCre['tblCreative']['cr_size'] == $size) {
                        $found = 1;
                    }
                }
                //if(!$found)
                $creatives[$j]['ad_unit_sizes'] = $size;
            } else {
                $creatives[$j]['ad_unit_sizes'] = $size;
            }
            $j++;
        }
        $flightDetils = $this->tblFlight->getFlightNameById($flight_ids);
        // get creatives type 
        $this->loadModel('CreativesType');

        $creatives_type = $this->CreativesType->GetCreativeType();
        //print_r($tblCreative);
        $this->set('flightDetils', $flightDetils);
        $this->set('group', $tblCreative);
        $this->set('li_dfp_id', $li_dfp_id);
        $this->set('li_order_id', $li_order_id);
        $this->set('line_id', $line_id);
        $this->set('line_name', $line_name);
        $this->set('creatives', $creatives);
        $this->set(compact('creatives_type'));
        if (!empty($tblCreative)) {
            $this->tblLineItem->updateAll(array('li_status_id' => 3), array('li_id' => $line_id));
            if ($flightFound == 0)
                $this->tblLineItem->updateAll(array('li_status_id' => 9), array('li_id' => $line_id));
        } else {
            $this->tblLineItem->updateAll(array('li_status_id' => 2), array('li_id' => $line_id));
        }
    }

    public function view_leads($line_id) {
        $test = $this->tblCreative->query("SELECT *,'' as count FROM tbl_creatives tc inner join tbl_line_items tli on tc.cr_lid=tli.li_dfp_id inner join tbl_orders tbo on tbo.dfp_order_id=tli.li_order_id where tc.cr_lid = {$line_id} order by tc.cr_status ASC");

        $line_name = $this->tblLineItem->field('li_name', array('li_dfp_id' => $line_id));
        $this->set('dfp_order_id', $test[0]['tbo']['dfp_order_id']);
        $table_name = "order_" . $test[0]['tbo']['dfp_order_id'];
        $this->Dynamic->useTable = $table_name;
        $options['conditions'] = array('Dynamic.od_lid' => $line_id);
        $test = $this->Dynamic->find("all", $options);
        $this->set('group', $test);
        $this->set('line_name', $line_name);
    }

    public function view_profile($od_id, $od_order_id) {
        $table_name = "order_" . $od_order_id;
        $this->Dynamic->useTable = $table_name;
        $options['conditions'] = array('Dynamic.od_id' => $od_id);
        $test = $this->Dynamic->find("first", $options);
        $this->set('group', $test);
        $this->set('od_order_id', $od_order_id);
    }

    public function view_detail_lead_gen_stats($line_item_id = null, $duration = "year", $isp_request = null, $demograpic_request = null, $source_request = null) {
        // load model of report
        //Configure::write('debug', 2);
        $this->loadModel('Authake.tblIspReport');

        if ($this->request->is('post')) {

            $isp_request = (!empty($this->request->data['Order']['isp'])) ? $this->request->data['Order']['isp'] : 'null';
            $source_request = (!empty($this->request->data['Order']['source'])) ? $this->request->data['Order']['source'] : 'null';
            $demograpic_request = (!empty($this->request->data['Order']['demographic_data'])) ? $this->request->data['Order']['demographic_data'] : 'null';
            $this->redirect(array('controller' => 'orders', 'action' => 'view_detail_lead_gen_stats', $line_item_id, $duration, $isp_request, $demograpic_request, $source_request));
        }

        $optionsli['conditions'] = array('li_id' => $line_item_id);
        $lineItem = $this->tblLineItem->find('first', $optionsli);
        if (empty($lineItem)) {
            $this->Session->setFlash(__('Sorry, Something went wrong!'), 'error');
            $this->redirect(array('controller' => 'orders', 'action' => 'index', $line_item_id));
        }
        $li_dfp_id = $lineItem['tblLineItem']['li_dfp_id'];
        // this function is comming from component
        $range = $this->Commonfunction->getDuration($duration);
        // get Lead Total
        $commonsql = 'select COALESCE(SUM(tblReportDay.rd_leads),0) from ' . $this->tblReportDay->table . ' tblReportDay where date(tblReportDay.rd_date) ';
        $this->tblReportDay->virtualFields = array('TotalLead' => $commonsql . '=CURDATE() and tblReportDay.rd_line_item_id=' . $li_dfp_id,
            'TotalWeek' => $commonsql . ' BETWEEN  DATE_SUB(CURDATE(), INTERVAL 7 DAY) and  CURDATE() and tblReportDay.rd_line_item_id=' . $li_dfp_id,
            'TotalMonth' => $commonsql . ' BETWEEN  DATE_SUB(CURDATE(), INTERVAL 30 DAY) and  CURDATE() and tblReportDay.rd_line_item_id=' . $li_dfp_id,
            'TotalYear' => $commonsql . ' BETWEEN  DATE_SUB(CURDATE(), INTERVAL 365 DAY) and  CURDATE() and tblReportDay.rd_line_item_id=' . $li_dfp_id
        ); //  
        $LeadTotal = $this->tblReportDay->find('first', array('fields' => array('tblReportDay.rd_id', 'TotalLead', 'TotalWeek', 'TotalMonth', 'TotalYear')));

        // Performance Vs. Budgets
        $date_diff = $this->Commonfunction->datediff($lineItem['tblLineItem']['li_start_date'], $lineItem['tblLineItem']['li_end_date']);
        $Performance['day_p_budget_a'] = $day_p_budget = $lineItem['tblLineItem']['li_quantity'] / $date_diff;
        $Performance['day_p_budget'] = $LeadTotal['tblReportDay']['TotalLead'] - $day_p_budget;

        $Performance['week_p_budget_a'] = $day_p_budget * 7;
        $Performance['week_p_budget'] = $LeadTotal['tblReportDay']['TotalWeek'] - ($day_p_budget * 7);

        $Performance['month_p_budget_a'] = $day_p_budget * 30;
        $Performance['month_p_budget'] = $LeadTotal['tblReportDay']['TotalMonth'] - ($day_p_budget * 30);

        $Performance['year_p_budget_a'] = $day_p_budget * 365;
        $Performance['year_p_budget'] = $LeadTotal['tblReportDay']['TotalYear'] - ($day_p_budget * 365);

        // get ISp // get Demographic Data 
        $this->OrderLeads->useTable = "order_" . $lineItem['tblLineItem']['li_order_id']; //das_email
        $OrderLeads = $this->OrderLeads->find('all', array('fields' => array('OrderLeads.od_id', 'OrderLeads.od_country_name', 'OrderLeads.od_email', 'OrderLeads.od_source'), 'conditions' => array()));
        // get isp data
        $isp = $demographic_data = $source_data = array();

        if (!empty($OrderLeads)) {
            foreach ($OrderLeads as $order) {
                $domain = substr(strrchr($order['OrderLeads']['od_email'], "@"), 1);
                if (!empty($domain))
                    $isp[$domain] = $domain;
                $demographic_data[$order['OrderLeads']['od_country_name']] = $order['OrderLeads']['od_country_name'];
                $source_data[$order['OrderLeads']['od_source']] = $order['OrderLeads']['od_source'];
            }
        }
        $totalGoalLead = $lineItem['tblLineItem']['li_quantity'];
        $this->set('duration', $duration);
        $this->set('totalGoalLead', $totalGoalLead);
        $this->set('li_rate', $lineItem['tblLineItem']['li_rate']);
        self::tblReportDayTrendingByChannel($li_dfp_id, $range);
        $mappedUnits = self::tblReportBySources($li_dfp_id, $line_item_id, $range);
        // this function is comming from component
        $isp_request = (!empty($isp_request) && $isp_request != 'null') ? $isp_request : '';
        $demograpic_request = (!empty($demograpic_request) && $demograpic_request != 'null') ? $demograpic_request : '';
        $source_request = (!empty($source_request) && $source_request != 'null') ? $source_request : '';

        if (empty($isp_request) && empty($demograpic_request)) {
            $piwikReport = $this->Commonfunction->piwikReport($mappedUnits, $range);
            $this->set('country', $piwikReport['country']);
            $this->set('DeviceType', $piwikReport['DeviceType']);
            $this->set('browsers', $piwikReport['browsers']);
            $this->set('osfamily', $piwikReport['osfamily']);
        } else {

            // get ISp // get Demographic Data 
            $this->OrderLeads->useTable = "order_" . $lineItem['tblLineItem']['li_order_id']; //das_email
            // country 
            $this->OrderLeads->virtualFields = array('totalcount' => 'count(OrderLeads.od_id)');
            $order_options['fields'] = array('OrderLeads.od_country_name', 'totalcount');
            if (!empty($isp_request)) {
                $order_options['conditions']['AND'][] = array('OrderLeads.od_email like' => '%' . $isp_request . '%');
            }
            if (!empty($demograpic_request)) {
                $order_options['conditions']['AND'][] = array('OrderLeads.od_country_name like' => '%' . $demograpic_request);
            }
            if (!empty($source_request)) {
                $order_options['conditions']['AND'][] = array('OrderLeads.od_source like' => '%' . $source_request);
            }
            $order_options['group'] = array('OrderLeads.od_country_name');
            $country = $this->OrderLeads->find('list', $order_options);

            //DeviceType
            $order_options['fields'] = array('OrderLeads.device_type', 'totalcount');
            $order_options['group'] = array('OrderLeads.device_type');
            $DeviceType = $this->OrderLeads->find('list', $order_options);

            //DeviceType
            $order_options['fields'] = array('OrderLeads.fp_browser', 'totalcount');
            $order_options['group'] = array('OrderLeads.fp_browser');
            $browsers = $this->OrderLeads->find('list', $order_options);
            //osfamily
            $order_options['fields'] = array('OrderLeads.fp_os', 'totalcount');
            $order_options['group'] = array('OrderLeads.fp_os');
            $osfamily = $this->OrderLeads->find('list', $order_options);

            $this->set('country', $country);
            $this->set('DeviceType', $DeviceType);
            $this->set('browsers', $browsers);
            $this->set('osfamily', $osfamily);
        }
        $lineItem_name = $this->tblLineItem->field('li_name', array('li_id' => $line_item_id));
        $this->set('lineItem_name', $lineItem_name);
        $li_order_id = $this->tblLineItem->field('li_order_id', array('li_id' => $line_item_id));
        $this->set('line_item_id', $line_item_id);
        $this->set('li_order_id', $li_order_id);
        $this->set('LeadTotal', $LeadTotal);
        $this->set('Performance', $Performance);
        $this->set('isp', $isp);
        $this->set('demographic_data', $demographic_data);
        $this->set('source_data', $source_data);
        $this->set('isp_request', $isp_request);
        $this->set('demograpic_request', $demograpic_request);
        $this->set('source_request', $source_request);
        // auto responder

        $respondeOption = array();
        $respondeOption['conditions'] = array('tblLineItem.li_id' => $line_item_id);
        $respondeOption['joins'] = array(
            array('table' => 'tbl_auto_responders',
                'alias' => 'AutoResponder',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_li_crtid = AutoResponder.id',
                    'tblMailing.is_autoresp' => 1
                )),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'AutoResponder.li_id = tblLineItem.li_id'
                ))
        );
        $this->tblMailing->virtualFields = array(
            'TotalSent' => 'COALESCE(SUM(tblMailing.m_sent),0)',
            'TotalDelivered' => 'COALESCE(SUM(tblMailing.m_delivered),0)',
            'TotalOpen' => 'COALESCE(SUM(tblMailing.m_open),0)',
            'TotalUOpen' => 'COALESCE(SUM(tblMailing.m_u_open),0)',
            'TotalClick' => 'COALESCE(SUM(tblMailing.m_clicks),0)',
            'TotalUClick' => 'COALESCE(SUM(tblMailing.m_u_clicks),0)',
            'TotalComplaint' => 'COALESCE(SUM(tblMailing.m_spam),0)' // complaint is pending
        );
        $respondeOption['fields'] = array('TotalSent', 'TotalDelivered', 'TotalOpen', 'TotalUOpen', 'TotalClick', 'TotalUClick', 'TotalComplaint');
        $auto_responder_statistics = $this->tblMailing->find('first', $respondeOption);

        $respondeOption = array();
        //$respondeOption['fields'] = array('tblIspReport.*');
        $respondeOption['conditions'] = array('tblLineItem.li_id' => $line_item_id);
        $respondeOption['joins'] = array(
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.m_id=tblMailing.m_id'
                )),
            array('table' => 'tbl_auto_responders',
                'alias' => 'AutoResponder',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_li_crtid = AutoResponder.id',
                    'tblMailing.is_autoresp' => 1
                )),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'AutoResponder.li_id = tblLineItem.li_id'
                )),
            array('table' => 'tbl_isp_list',
                'alias' => 'tblIspList',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.isp_master_id = tblIspList.id'
                ))
        );
        $this->tblIspReport->virtualFields = array(
            'Domain' => 'tblIspList.isp_name',
            'TotalEmail' => 'COALESCE(SUM(tblIspReport.sent_count),0)',
            'TotalBounce' => 'COALESCE(SUM(tblIspReport.bounce_count),0)',
            'TotalOpen' => 'COALESCE(SUM(tblIspReport.open_count),0)',
            'TotalClick' => 'COALESCE(SUM(tblIspReport.click_count),0)',
            'TotalUnsub' => 'COALESCE(SUM(tblIspReport.unsub_count),0)'
        );
        $respondeOption['fields'] = array('TotalEmail', 'Domain', 'TotalBounce', 'TotalOpen', 'TotalClick', 'TotalUnsub');
        //$respondeOption['fields'] = array('*');
        $respondeOption['group'] = array('Domain');
        $email_performace_report = $this->tblIspReport->find('all', $respondeOption);

        $this->set('email_performace_report', $email_performace_report);
        $this->set('auto_responder_statistics', $auto_responder_statistics);
    }

    private function tblReportBySources($li_dfp_id = null, $line_item_id = null, $range = array()) {
        $options3['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.ad_dfp_id = tblReportDay.rd_ad_unit_id',
                    'tba.owner_user_id' => array($this->Authake->getUserId())
        )));

        $options3['conditions'] = array('and' => array(
                array('tblReportDay.rd_date <= ' => $range['end'],
                    'tblReportDay.rd_date >= ' => $range['start'],
                    'tblReportDay.rd_line_item_id ' => $li_dfp_id
        )));

        $options3['fields'] = array('SUM(rd_imp) as rd_imp,SUM(rd_clicks) as rd_clicks ,SUM(rd_leads) as rd_leads,SUM(rd_delivered) as rd_delivered,SUM(rd_accepted) as rd_accepted,SUM(rd_ar_rejects) as rd_ar_rejects, tba.adunit_name as adunit_name, SUM(rd_leads) - SUM(rd_delivered) as rd_3party , SUM(rd_delivered) - SUM(rd_accepted) as rd_client_rejects');

        $options3['group'] = 'tba.ad_dfp_id';

        $options3['order'] = array('tblReportDay.rd_accepted' => 'DESC');

        $tblReportBySources = $this->tblReportDay->find('all', $options3);
        //pr($tblReportBySources);die;
        $this->set('tblReportBySources', $tblReportBySources);
        //print_r($tblReportBySources);

        $optionm['conditions'] = array('tblMappedAdunit.ma_l_id' => $line_item_id, 'tblMappedAdunit.ma_isActive' => '1');
        $optionm['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = tblp.user_id')));
        $optionm['fields'] = array('tblMappedAdunit.*', 'tblAdunit.*', 'tblp.*');
        $mappedUnits = $this->tblMappedAdunit->find('all', $optionm);
        $this->set('mappedUnits', $mappedUnits);
        return $mappedUnits;
    }

    private function tblReportDayTrendingByChannel($li_dfp_id = null, $range = array()) {
        $options2['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.ad_dfp_id = tblReportDay.rd_ad_unit_id',
                    'tba.owner_user_id' => array($this->Authake->getUserId())
                )),
            array('table' => 'tbl_product_types',
                'alias' => 'tbpt',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.ad_ptype_id = tbpt.ptype_id'
                )),
            array('table' => 'tbl_media_types',
                'alias' => 'tbmt',
                'type' => 'INNER',
                'conditions' => array(
                    'tbmt.mtype_id = tbpt.ptype_m_id'
        )));

        $options2['conditions'] = array('and' => array(
                array('tblReportDay.rd_date <= ' => $range['end'],
                    'tblReportDay.rd_date >= ' => $range['start'],
                    'tblReportDay.rd_line_item_id ' => $li_dfp_id
        )));

        $options2['fields'] = array('SUM(rd_imp) as rd_imp,SUM(rd_clicks) as rd_clicks ,SUM(rd_leads) as rd_leads,SUM(rd_delivered) as rd_delivered,SUM(rd_accepted) as rd_accepted,SUM(rd_ar_rejects) as rd_ar_rejects, rd_date as rd_date, mtype_name as mtype_name, SUM(rd_leads) - SUM(rd_delivered) as rd_3party , SUM(rd_delivered) - SUM(rd_accepted) as rd_client_rejects');

        $options2['group'] = 'tblReportDay.rd_date, tbmt.mtype_id';

        $options2['order'] = array('tblReportDay.rd_date' => 'DESC');

        $tblReportDayTrendingByChannel = $this->tblReportDay->find('all', $options2);

        $this->set('tblReportDayTrendingByChannel', $tblReportDayTrendingByChannel);
        $emailTotal = 0;
        $desktopTotal = 0;
        $mobileTotal = 0;
        $coregTotal = 0;
        foreach ($tblReportDayTrendingByChannel as $item) {
            if ($item['tbmt']['mtype_name'] == "Email") {
                $emailTotal += $item[0]['rd_accepted'];
            }
            if ($item['tbmt']['mtype_name'] == "Desktop") {
                $desktopTotal += $item[0]['rd_accepted'];
            }
            if ($item['tbmt']['mtype_name'] == "Mobile") {
                $mobileTotal += $item[0]['rd_accepted'];
            }
            if ($item['tbmt']['mtype_name'] == "Lead-Gen") {
                $coregTotal += $item[0]['rd_accepted'];
            }
        }
        $this->set('emailTotal', $emailTotal);
        $this->set('desktopTotal', $desktopTotal);
        $this->set('mobileTotal', $mobileTotal);
        $this->set('coregTotal', $coregTotal);
    }

    public function view_detail_stats($line_item_id, $duration = "year") {
        // this function is comming from component
        $range = $this->Commonfunction->getDuration($duration);

        //print_r($range);
        $optionsli['conditions'] = array('li_id' => $line_item_id);
        $lineItem = $this->tblLineItem->find('first', $optionsli);

        $li_dfp_id = $lineItem['tblLineItem']['li_dfp_id'];

        $options1['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.ad_dfp_id = tblReportDay.rd_ad_unit_id',
                    'tba.owner_user_id' => array($this->Authake->getUserId())
        )));

        $options1['conditions'] = array('and' => array(
                array('tblReportDay.rd_date <= ' => $range['end'],
                    'tblReportDay.rd_date >= ' => $range['start'],
                    'tblReportDay.rd_line_item_id ' => $li_dfp_id
        )));

        $options1['fields'] = array('SUM(rd_imp) as rd_imp,SUM(rd_clicks) as rd_clicks ,SUM(rd_leads) as rd_leads,SUM(rd_delivered) as rd_delivered,SUM(rd_accepted) as rd_accepted,SUM(rd_ar_rejects) as rd_ar_rejects, rd_date as rd_date, SUM(rd_leads) - SUM(rd_delivered) as rd_3party , SUM(rd_delivered) - SUM(rd_accepted) as rd_client_rejects');

        $options1['group'] = 'tblReportDay.rd_date';

        $options1['order'] = array('tblReportDay.rd_date' => 'DESC');

        $tblReportDayQuality = $this->tblReportDay->find('all', $options1);

        //print_r($tblReportDayQuality);

        $imp = 0;
        $clicks = 0;
        $leads = 0;
        $delivered = 0;
        $accepted = 0;
        $ar_rejects = 0;
        $third_party_rejects = 0;
        $clients_rejects = 0;
        foreach ($tblReportDayQuality as $item) {
            $imp += $item[0]['rd_imp'];
            $clicks += $item[0]['rd_clicks'];
            $leads += $item[0]['rd_leads'];
            $delivered += $item[0]['rd_delivered'];
            $accepted += $item[0]['rd_accepted'];
            $ar_rejects += $item[0]['rd_ar_rejects'];
        }
        $third_party_rejects = $leads - $delivered;
        $clients_rejects = $delivered - $accepted;
        $totalRejects = $ar_rejects + $third_party_rejects + $clients_rejects;
        $totalRejects = ($totalRejects == 0 ? "1" : $totalRejects);
        $totalGoalRev = $lineItem['tblLineItem']['li_total'];
        $totalGoalLead = $lineItem['tblLineItem']['li_quantity'];
        $totalAchivedRev = $accepted * $lineItem['tblLineItem']['li_rate'];
        $totalTrending = (!empty($totalGoalRev)) ? (($totalAchivedRev / $totalGoalRev) * 100) : 0;
        $this->set('imp', $imp);
        $this->set('clicks', $clicks);
        $this->set('leads', $leads);
        $this->set('delivered', $delivered);
        $this->set('accepted', $accepted);
        $this->set('ar_rejects', $ar_rejects);
        $this->set('third_party_rejects', $third_party_rejects);
        $this->set('clients_rejects', $clients_rejects);
        $this->set('totalRejects', $totalRejects);
        $this->set('duration', $duration);
        $this->set('tblReportDayQuality', $tblReportDayQuality);
        $this->set('totalGoalLead', $totalGoalLead);
        $this->set('totalTrending', $totalTrending);
        $this->set('totalGoalRev', $totalGoalRev);
        $this->set('totalAchivedRev', $totalAchivedRev);
        $this->set('li_rate', $lineItem['tblLineItem']['li_rate']);
        self::tblReportDayTrendingByChannel($li_dfp_id, $range);
        $mappedUnits = self::tblReportBySources($li_dfp_id, $line_item_id, $range);


        // this function is comming from component
        $piwikReport = $this->Commonfunction->piwikReport($mappedUnits, $range);
        $this->set('country', $piwikReport['country']);
        $this->set('DeviceType', $piwikReport['DeviceType']);
        $this->set('browsers', $piwikReport['browsers']);
        $this->set('osfamily', $piwikReport['osfamily']);

        //echo "<pre>";
        //print_r($mappedUnits);
        //echo "</pre>";

        $this->set('mappedUnits', $mappedUnits);
        $lineItem_name = $this->tblLineItem->field('li_name', array('li_id' => $line_item_id));
        $this->set('lineItem_name', $lineItem_name);

        $li_order_id = $this->tblLineItem->field('li_order_id', array('li_id' => $line_item_id));
        $this->set('li_order_id', $li_order_id);
        $this->set('line_item_id', $line_item_id);
    }

    public function view_flights($line_item_id = null, $flight_status = null) { // here flight_status 1 for Active , 2 Completed(comming from line item report)
        $this->loadModel('Authake.tblFlight');
        if ($flight_status == 1) {
            $optionm['conditions'][] = array('tblFlight.fl_end >=' => 'now');
        } else if ($flight_status == 2) {
            $optionm['conditions'][] = array('tblFlight.fl_end <' => 'now');
        }
        $optionm['conditions'][] = array('tblMappedAdunit.ma_l_id' => $line_item_id, 'tblMappedAdunit.ma_isActive' => '1');
        $optionm['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id = tblLineItem.li_id')
            ),
            array('table' => 'tbl_flights',
                'alias' => 'tblFlight',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_fl_id = tblFlight.fl_id')
            ),
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id')),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID')));
        $optionm['fields'] = array('tblMappedAdunit.ma_uid', 'tblMappedAdunit.ma_l_id', 'tblMappedAdunit.ma_id', 'tblMappedAdunit.lead_post_status', 'tblMappedAdunit.is_lead_post', 'tblAdunit.ad_uid', 'tblAdunit.adunit_name', 'tblAdunit.ad_dfp_id', 'tblp.CompanyName', 'tblp.tracksite_id', 'tblFlight.*', 'tblLineItem.li_name', 'tblLineItem.li_start_date', 'tblLineItem.li_end_date', 'tblLineItem.li_status_id');


        $mappedUnits = $this->tblMappedAdunit->find('all', $optionm);

        $flight_ids = array();
        $new_mappedUnits = array();
        if (!empty($mappedUnits)) {
            foreach ($mappedUnits as $mappedUnit) {
                $options['conditions'] = array('FlightsCreative.fl_id' => $mappedUnit['tblFlight']['fl_id']);
                $options['joins'] = array(
                    array('table' => 'tbl_flights_creatives',
                        'alias' => 'FlightsCreative',
                        'type' => 'INNER',
                        'conditions' => array(
                            'tblCreative.cr_id = FlightsCreative.cr_id')
                ));
                $options['fields'] = array('tblCreative.cr_id', 'tblCreative.cr_name');
                $tblCreative = $this->tblCreative->find("list", $options);
                $mappedUnit['tblCreative'] = $tblCreative;
                //$new_mappedUnits[] = $mappedUnit;
                // group flight name
                $new_mappedUnits[$mappedUnit['tblFlight']['fl_id']][] = $mappedUnit;
            }
        }


        $this->set('mappedUnits', $new_mappedUnits);
        $lineItem_data = $this->tblLineItem->getLineItemDetail($line_item_id);
        $this->set('lineItem_data', $lineItem_data);
        $this->set('line_item_id', $line_item_id);
    }

    public function view_cpm_stats($line_item_id, $duration = null) {
        Configure::write('debug', 2);
        $optionsli['conditions'] = array('li_id' => $line_item_id);
        $lineItem = $this->tblLineItem->find('first', $optionsli);

        $optionm['conditions'] = array('tblMappedAdunit.ma_l_id' => $line_item_id, 'tblMappedAdunit.ma_isActive' => '1');
        $optionm['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = tblp.user_id')));
        $optionm['fields'] = array('tblMappedAdunit.*', 'tblAdunit.*', 'tblp.*');
        $mappedUnits = $this->tblMappedAdunit->find('all', $optionm);



        $mappedUnit_id = '';
        foreach ($mappedUnits as $item) {
            $mappedUnit_id .= $item['tblMappedAdunit']['ma_uid'] . ",";
        }

        $mappedUnit_id = rtrim($mappedUnit_id, ',');

        $options['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tma',
                'type' => 'INNER',
                'conditions' => array(
                    'tma.ma_uid = tblMailing.m_ad_uid')),
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.ad_dfp_id = tma.ma_ad_id')),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.publisher_id = tblp.user_id')));

        $options['fields'] = array('tblMailing.*', 'tblp.tracksite_id');

        $options['conditions'] = array('tblMailing.m_ad_uid' => array($mappedUnit_id));
        $tblMailing = $this->tblMailing->find('all', $options);
        //print_r($tblMailing);

        $sent = 0;
        $delivered = 0;
        $opens = 0;
        $totalopens = 0;
        $clicks = 0;
        $totalclicks = 0;
        $unsubscribe = 0;
        $bounce = 0;
        $spam = 0;
        $country = array();
        $DeviceType = array();
        $browsers = array();
        $osfamily = array();
        App::import('Vendor', 'src/Piwik');
        if (!empty($tblMailing)) {
            foreach ($tblMailing as $item) {
                $sent += $item['tblMailing']['m_sent'];
                $delivered += $item['tblMailing']['m_delivered'];
                $opens += $item['tblMailing']['m_u_open'];
                $totalopens += $item['tblMailing']['m_open'];
                $clicks += $item['tblMailing']['m_u_clicks'];
                $totalclicks += $item['tblMailing']['m_clicks'];
                $unsubscribe += $item['tblMailing']['m_unsubscribe'];
                $bounce += $item['tblMailing']['m_bounce'];
                $spam += $item['tblMailing']['m_spam'];
                $track_code = $item['tblMailing']['m_ad_uid'] . "-" . $item['tblMailing']['m_id'];
                //echo $item['tblp']['tracksite_id'];
                $piwik = new Piwik(Configure::read('Piwik.url'), Configure::read('Piwik.token'), $item['tblp']['tracksite_id'], Piwik::FORMAT_JSON);
                $piwik->setPeriod(Piwik::PERIOD_RANGE);
                $piwik->setRange(date("Y-m-d", strtotime('-30 days')), date("Y-m-d"));
                $getCountries = $piwik->getCountry("contentPiece==" . $track_code);
                foreach ($getCountries as $getCountry) {
                    if (array_key_exists($getCountry->label, $country)) {
                        $country[$getCountry->label] += $getCountry->nb_visits;
                    } else {
                        $country[$getCountry->label] = $getCountry->nb_visits;
                    }
                }



                $getDeviceType = $piwik->getDeviceType("contentPiece==" . $track_code);
                foreach ($getDeviceType as $getDevice) {
                    if (array_key_exists($getDevice->label, $DeviceType)) {
                        $DeviceType[$getDevice->label] += $getDevice->nb_visits;
                    } else {
                        $DeviceType[$getDevice->label] = $getDevice->nb_visits;
                    }
                }

                $getBrowsers = $piwik->getBrowsers("contentPiece==" . $track_code);
                foreach ($getBrowsers as $getBrowser) {
                    if (array_key_exists($getBrowser->label, $browsers)) {
                        $browsers[$getBrowser->label] += $getBrowser->nb_visits;
                    } else {
                        $browsers[$getBrowser->label] = $getBrowser->nb_visits;
                    }
                }

                $getOSFamilies = $piwik->getOSFamilies("contentPiece==" . $track_code);
                foreach ($getOSFamilies as $getOSFamily) {
                    if (array_key_exists($getOSFamily->label, $osfamily)) {
                        $osfamily[$getOSFamily->label] += $getOSFamily->nb_visits;
                    } else {
                        $osfamily[$getOSFamily->label] = $getOSFamily->nb_visits;
                    }
                }
            }
        }
        $totalQuality = $unsubscribe + $bounce + $spam;

        $totalQuality = ($totalQuality == 0 ? "1" : $totalQuality);

        $totalGoalRev = (!empty($lineItem['tblLineItem']['li_total'])) ? $lineItem['tblLineItem']['li_total'] : 0;
        $totalAchivedRev = (!empty($lineItem['tblLineItem']['li_revenue'])) ? $lineItem['tblLineItem']['li_revenue'] : 0;
        $totalTrending = ($totalAchivedRev / ($totalGoalRev == 0) ? 1 : $totalGoalRev) * 100;


        $this->set('sent', $sent);
        $this->set('delivered', $delivered);
        $this->set('opens', $opens);
        $this->set('clicks', $clicks);
        $this->set('totalopens', $totalopens);
        $this->set('totalclicks', $totalclicks);
        $this->set('duration', $duration);
        $this->set('unsubscribe', $unsubscribe);
        $this->set('bounce', $bounce);
        $this->set('spam', $spam);
        $this->set('totalQuality', $totalQuality);
        $this->set('country', $country);
        $this->set('DeviceType', $DeviceType);
        $this->set('browsers', $browsers);
        $this->set('osfamily', $osfamily);
        $this->set('group', $tblMailing);
        $this->set('totalGoalRev', $totalGoalRev);
        $this->set('totalAchivedRev', $totalAchivedRev);
        $this->set('totalTrending', $totalTrending);
        $li_dfp_id = (!empty($lineItem['tblLineItem']['li_dfp_id'])) ? $lineItem['tblLineItem']['li_dfp_id'] : 0;

        $this->set('li_rate', (!empty($lineItem['tblLineItem']['li_rate'])) ? $lineItem['tblLineItem']['li_rate'] : 0);


        $options3['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.ad_dfp_id = tblReportDay.rd_ad_unit_id',
                    'tba.owner_user_id' => array($this->Authake->getUserId())
        )));
        $options = array();
        $options3['conditions'] = array('tblReportDay.rd_line_item_id ' => $li_dfp_id);
        $options['conditions'] = array('tblCreative.cr_lid' => $li_dfp_id);
        $this->tblCreative->bindModel(array('hasMany' => array('FlightsCreative' =>
                array('foreignKey' => 'cr_id', 'conditions' => array(), 'fields' => array('FlightsCreative.fl_id')))));

        $options['fields'] = array('tblCreative.cr_id', 'tblCreative.cr_name', 'tblCreative.cr_size', 'tblCreative.creatives_type', 'tblCreative.cr_status', 'tblCreative.cr_imp', 'tblCreative.cr_click', 'tblCreative.created_at', 'tblCreative.cr_preview_url', 'tblCreative.cr_header');
        $tblCreative = $this->tblCreative->find("all", $options);
        $options3['fields'] = array('SUM(rd_imp) as rd_imp,SUM(rd_clicks) as rd_clicks ,SUM(rd_leads) as rd_leads,SUM(rd_delivered) as rd_delivered,SUM(rd_accepted) as rd_accepted,SUM(rd_ar_rejects) as rd_ar_rejects, tba.adunit_name as adunit_name, SUM(rd_leads) - SUM(rd_delivered) as rd_3party , SUM(rd_delivered) - SUM(rd_accepted) as rd_client_rejects');

        $options3['group'] = 'tba.ad_dfp_id';

        $options3['order'] = array('tblReportDay.rd_accepted' => 'DESC');

        $tblReportBySources = $this->tblReportDay->find('all', $options3);
        $this->set('tblReportBySources', $tblReportBySources);
        //print_r($tblReportBySources);
        //echo "<pre>";
        //print_r($mappedUnits);
        //echo "</pre>";

        $this->set('mappedUnits', $mappedUnits);
        $lineItem_name = $this->tblLineItem->field('li_name', array('li_id' => $line_item_id));
        $this->set('lineItem_name', $lineItem_name);

        $li_order_id = $this->tblLineItem->field('li_order_id', array('li_id' => $line_item_id));
        $this->set('li_order_id', $li_order_id);
    }

    public function view_user_profile($line_id, $duration = null) {
        $listid = null;

        $li_order_id = $this->tblLineItem->field('li_order_id', array('li_id' => $line_id));
        $li_dfp_id = $this->tblLineItem->field('li_dfp_id', array('li_id' => $line_id));
        $table_name = "order_" . $li_order_id;
        $this->OrderLeads->useTable = $table_name;
        $options['conditions'] = array('od_lid' => $li_dfp_id);
        $options['fields'] = array('OrderLeads.od_email');
        $resutls = $this->OrderLeads->find('all', $options);

        if ($duration == null)
            $duration = "month";

        if ($listid == null)
            $listid = "all";
        $range = $this->Commonfunction->getDuration($duration);

        $line_name = $this->tblLineItem->field('li_name', array('li_id' => $line_id));
        $this->set('line_name', $line_name);

        $emails = array();
        foreach ($resutls as $email) {
            $emails[] = MD5($email['OrderLeads']['od_email']);
        }

        //print_r($emails);

        $userProfiles = $this->requestAction('/CouchDB/viewKMAProfile', array('emailArray' => $emails));
        //print_r($emails);


        $items = $userProfiles;
        //print_r($items);

        $this->tblKmaResponse->virtualFields['sum'] = 'COUNT(*)';


        $MARITALS = array_filter($items, function($item) {
            return $item['doc']['KMA_MARITAL_STAT'] == 'S';
        });
        $MARITALM = array_filter($items, function($item) {
            return $item['doc']['KMA_MARITAL_STAT'] == 'M';
        });
        $MARITALNA = array_filter($items, function($item) {
            return $item['doc']['KMA_MARITAL_STAT'] == '';
        });
        $tblKmaMaritalStats = array();
        $tblKmaMaritalStats['Single'] = count($MARITALS);
        $tblKmaMaritalStats['Married'] = count($MARITALM);
        $tblKmaMaritalStats['N/A'] = count($MARITALNA);

        $AgeAGF = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'A' && $item['doc']['GENDER'] == 'F';
        });
        $AgeAGM = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'A' && $item['doc']['GENDER'] == 'M';
        });
        $AgeAGNA = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'A' && $item['doc']['GENDER'] == '';
        });
        $AgeBGF = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'B' && $item['doc']['GENDER'] == 'F';
        });
        $AgeBGM = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'B' && $item['doc']['GENDER'] == 'M';
        });
        $AgeBGNA = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'B' && $item['doc']['GENDER'] == '';
        });
        $AgeCGF = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'C' && $item['doc']['GENDER'] == 'F';
        });
        $AgeCGM = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'C' && $item['doc']['GENDER'] == 'M';
        });
        $AgeCGNA = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'C' && $item['doc']['GENDER'] == '';
        });
        $AgeDGF = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'D' && $item['doc']['GENDER'] == 'F';
        });
        $AgeDGM = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'D' && $item['doc']['GENDER'] == 'M';
        });
        $AgeDGNA = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'D' && $item['doc']['GENDER'] == '';
        });
        $AgeEGF = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'E' && $item['doc']['GENDER'] == 'F';
        });
        $AgeEGM = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'E' && $item['doc']['GENDER'] == 'M';
        });
        $AgeEGNA = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'E' && $item['doc']['GENDER'] == '';
        });
        $AgeFGF = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'F' && $item['doc']['GENDER'] == 'F';
        });
        $AgeFGM = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'F' && $item['doc']['GENDER'] == 'M';
        });
        $AgeFGNA = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == 'F' && $item['doc']['GENDER'] == '';
        });
        $AgeF = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == '' && $item['doc']['GENDER'] == 'F';
        });
        $AgeM = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == '' && $item['doc']['GENDER'] == 'M';
        });
        $AgeNA = array_filter($items, function($item) {
            return $item['doc']['KMA_AGEGRP'] == '' && $item['doc']['GENDER'] == '';
        });
        $tblKmaAgeGroup = array();
        $tblKmaAgeGroup['A']['F'] = count($AgeAGF);
        $tblKmaAgeGroup['A']['M'] = count($AgeAGM);
        $tblKmaAgeGroup['A'][''] = count($AgeAGNA);
        $tblKmaAgeGroup['B']['F'] = count($AgeBGF);
        $tblKmaAgeGroup['B']['M'] = count($AgeBGM);
        $tblKmaAgeGroup['B'][''] = count($AgeBGNA);
        $tblKmaAgeGroup['C']['F'] = count($AgeCGF);
        $tblKmaAgeGroup['C']['M'] = count($AgeCGM);
        $tblKmaAgeGroup['C'][''] = count($AgeCGNA);
        $tblKmaAgeGroup['D']['F'] = count($AgeDGF);
        $tblKmaAgeGroup['D']['M'] = count($AgeDGM);
        $tblKmaAgeGroup['D'][''] = count($AgeDGNA);
        $tblKmaAgeGroup['E']['F'] = count($AgeEGF);
        $tblKmaAgeGroup['E']['M'] = count($AgeEGM);
        $tblKmaAgeGroup['E'][''] = count($AgeEGNA);
        $tblKmaAgeGroup['F']['F'] = count($AgeFGF);
        $tblKmaAgeGroup['F']['M'] = count($AgeFGM);
        $tblKmaAgeGroup['F'][''] = count($AgeFGNA);
        $tblKmaAgeGroup['']['M'] = count($AgeM);
        $tblKmaAgeGroup['']['F'] = count($AgeF);
        $tblKmaAgeGroup[''][''] = count($AgeNA);

        $KMA_EDUCA = array_filter($items, function($item) {
            return $item['doc']['KMA_EDUC'] == 'A';
        });
        $KMA_EDUCB = array_filter($items, function($item) {
            return $item['doc']['KMA_EDUC'] == 'B';
        });
        $KMA_EDUCC = array_filter($items, function($item) {
            return $item['doc']['KMA_EDUC'] == 'C';
        });
        $KMA_EDUCD = array_filter($items, function($item) {
            return $item['doc']['KMA_EDUC'] == 'D';
        });
        $KMA_EDUCNA = array_filter($items, function($item) {
            return $item['doc']['KMA_EDUC'] == '';
        });
        $tblKmaEducation = array();
        $tblKmaEducation['Some High School'] = count($KMA_EDUCA);
        $tblKmaEducation['High School Grad'] = count($KMA_EDUCB);
        $tblKmaEducation['Some College'] = count($KMA_EDUCC);
        $tblKmaEducation['College Graduate'] = count($KMA_EDUCD);
        $tblKmaEducation['N/A'] = count($KMA_EDUCNA);

        $OCCUPA = array_filter($items, function($item) {
            return $item['doc']['KMA_OCCUP'] == 'A';
        });
        $OCCUPB = array_filter($items, function($item) {
            return $item['doc']['KMA_OCCUP'] == 'B';
        });
        $OCCUPC = array_filter($items, function($item) {
            return $item['doc']['KMA_OCCUP'] == 'C';
        });
        $OCCUPD = array_filter($items, function($item) {
            return $item['doc']['KMA_OCCUP'] == 'D';
        });
        $OCCUPE = array_filter($items, function($item) {
            return $item['doc']['KMA_OCCUP'] == 'E';
        });
        $OCCUPF = array_filter($items, function($item) {
            return $item['doc']['KMA_OCCUP'] == 'F';
        });
        $OCCUPG = array_filter($items, function($item) {
            return $item['doc']['KMA_OCCUP'] == 'G';
        });
        $OCCUPH = array_filter($items, function($item) {
            return $item['doc']['KMA_OCCUP'] == 'H';
        });
        $OCCUPNA = array_filter($items, function($item) {
            return $item['doc']['KMA_OCCUP'] == '';
        });
        $tblKmaOccup = array();
        $tblKmaOccup['Prof/Tech'] = count($OCCUPA);
        $tblKmaOccup['Admin / Manager'] = count($OCCUPB);
        $tblKmaOccup['Blue Collar'] = count($OCCUPC);
        $tblKmaOccup['Clerical/Service'] = count($OCCUPD);
        $tblKmaOccup['Homemaker'] = count($OCCUPE);
        $tblKmaOccup['Retired'] = count($OCCUPF);
        $tblKmaOccup['Business Owner'] = count($OCCUPG);
        $tblKmaOccup['Sales/Marketing'] = count($OCCUPH);
        $tblKmaOccup['N/A'] = count($OCCUPNA);


        $KMA_HHINCA = array_filter($items, function($item) {
            return $item['doc']['KMA_HHINC'] == 'A';
        });
        $KMA_HHINCB = array_filter($items, function($item) {
            return $item['doc']['KMA_HHINC'] == 'B';
        });
        $KMA_HHINCC = array_filter($items, function($item) {
            return $item['doc']['KMA_HHINC'] == 'C';
        });
        $KMA_HHINCD = array_filter($items, function($item) {
            return $item['doc']['KMA_HHINC'] == 'D';
        });
        $KMA_HHINCE = array_filter($items, function($item) {
            return $item['doc']['KMA_HHINC'] == 'E';
        });
        $KMA_HHINCF = array_filter($items, function($item) {
            return $item['doc']['KMA_HHINC'] == 'F';
        });
        $KMA_HHINCNA = array_filter($items, function($item) {
            return $item['doc']['KMA_HHINC'] == '';
        });

        $tblKmahhinc = array();
        $tblKmahhinc['Under 30 k'] = count($KMA_HHINCA);
        $tblKmahhinc['30k to 50k'] = count($KMA_HHINCB);
        $tblKmahhinc['50k to 75k'] = count($KMA_HHINCC);
        $tblKmahhinc['75k to 100k'] = count($KMA_HHINCD);
        $tblKmahhinc['100k to 150k'] = count($KMA_HHINCE);
        $tblKmahhinc['150k+'] = count($KMA_HHINCF);
        $tblKmahhinc['N/A'] = count($KMA_HHINCNA);


        $KMA_NETWORTHA = array_filter($items, function($item) {
            return $item['doc']['KMA_NETWORTH'] == 'A';
        });
        $KMA_NETWORTHB = array_filter($items, function($item) {
            return $item['doc']['KMA_NETWORTH'] == 'B';
        });
        $KMA_NETWORTHC = array_filter($items, function($item) {
            return $item['doc']['KMA_NETWORTH'] == 'C';
        });
        $KMA_NETWORTHD = array_filter($items, function($item) {
            return $item['doc']['KMA_NETWORTH'] == 'D';
        });
        $KMA_NETWORTHNA = array_filter($items, function($item) {
            return $item['doc']['KMA_NETWORTH'] == '';
        });

        $tblKmaNetWorth = array();
        $tblKmaNetWorth['Under 100k'] = count($KMA_NETWORTHA);
        $tblKmaNetWorth['100k to 250k'] = count($KMA_NETWORTHB);
        $tblKmaNetWorth['250k to 500k'] = count($KMA_NETWORTHC);
        $tblKmaNetWorth['500k+'] = count($KMA_NETWORTHD);
        $tblKmaNetWorth['N/A'] = count($KMA_NETWORTHNA);



        $KMA_OWNRENTO = array_filter($items, function($item) {
            return $item['doc']['KMA_OWNRENT'] == 'O';
        });
        $KMA_OWNRENTR = array_filter($items, function($item) {
            return $item['doc']['KMA_OWNRENT'] == 'R';
        });
        $KMA_OWNRENTNA = array_filter($items, function($item) {
            return $item['doc']['KMA_OWNRENT'] == '';
        });

        $tblKmaOwnRent = array();
        $tblKmaOwnRent['Owner'] = count($KMA_OWNRENTO);
        $tblKmaOwnRent['Renter'] = count($KMA_OWNRENTR);
        $tblKmaOwnRent['N/A'] = count($KMA_OWNRENTNA);


        $KMA_HSGS = array_filter($items, function($item) {
            return $item['doc']['KMA_HSG'] == 'S';
        });
        $KMA_HSGM = array_filter($items, function($item) {
            return $item['doc']['KMA_HSG'] == 'M';
        });
        $KMA_HSGT = array_filter($items, function($item) {
            return $item['doc']['KMA_HSG'] == 'T';
        });
        $KMA_HSGNA = array_filter($items, function($item) {
            return $item['doc']['KMA_HSG'] == '';
        });

        $tblKmaHsg = array();
        $tblKmaHsg['Single Family Homes'] = count($KMA_HSGS);
        $tblKmaHsg['Apartments'] = count($KMA_HSGM);
        $tblKmaHsg['Mobile Homes'] = count($KMA_HSGT);
        $tblKmaHsg['N/A'] = count($KMA_HSGNA);


        //Pending
        $tblKmaLmos = array();


        $KMA_HOMEOFFICEArray = array_filter($items, function($item) {
            return $item['doc']['KMA_HOMEOFFICE'] == 'Y';
        });
        $KMA_HOMEOFFICE = count($KMA_HOMEOFFICEArray);


        $KMA_SOHOArray = array_filter($items, function($item) {
            return $item['doc']['KMA_SOHO'] == 'Y';
        });
        $KMA_SOHO = count($KMA_SOHOArray);


        $KMA_CONTEDArray = array_filter($items, function($item) {
            return $item['doc']['KMA_CONTED'] == 'Y';
        });
        $KMA_CONTED = count($KMA_CONTEDArray);


        $KMA_CIGAR_SMOKERArray = array_filter($items, function($item) {
            return $item['doc']['KMA_CIGAR_SMOKER'] == 'Y';
        });
        $KMA_CIGAR_SMOKER = count($KMA_CIGAR_SMOKERArray);


        $KMA_SMOKERArray = array_filter($items, function($item) {
            return $item['doc']['KMA_SMOKER'] == 'Y';
        });
        $KMA_SMOKER = count($KMA_SMOKERArray);


        $KMA_WTLOSSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_WTLOSS'] == 'Y';
        });
        $KMA_WTLOSS = count($KMA_WTLOSSArray);


        $KMA_HEALTHArray = array_filter($items, function($item) {
            return $item['doc']['KMA_HEALTH'] == 'Y';
        });
        $KMA_HEALTH = count($KMA_HEALTHArray);


        $KMA_EXERCISEArray = array_filter($items, function($item) {
            return $item['doc']['KMA_EXERCISE'] == 'Y';
        });
        $KMA_EXERCISE = count($KMA_EXERCISEArray);


        $KMA_INVESTMENTSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_INVESTMENTS'] == 'Y';
        });
        $KMA_INVESTMENTS = count($KMA_INVESTMENTSArray);


        $KMA_OPSEEKArray = array_filter($items, function($item) {
            return $item['doc']['KMA_OPSEEK'] == 'Y';
        });
        $KMA_OPSEEK = count($KMA_OPSEEKArray);


        $KMA_INVEST_HIGHENDArray = array_filter($items, function($item) {
            return $item['doc']['KMA_INVEST_HIGHEND'] == 'Y';
        });
        $KMA_INVEST_HIGHEND = count($KMA_INVEST_HIGHENDArray);


        $KMA_INVEST_LOWENDArray = array_filter($items, function($item) {
            return $item['doc']['KMA_INVEST_LOWEND'] == 'Y';
        });
        $KMA_INVEST_LOWEND = count($KMA_INVEST_LOWENDArray);


        $KMA_PETS_CATSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_PETS_CATS'] == 'Y';
        });
        $KMA_PETS_CATS = count($KMA_PETS_CATSArray);


        $KMA_PETS_DOGSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_PETS_DOGS'] == 'Y';
        });
        $KMA_PETS_DOGS = count($KMA_PETS_DOGSArray);


        $KMA_PETSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_PETS'] == 'Y';
        });
        $KMA_PETS = count($KMA_PETSArray);


        $KMA_SRPRODSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_SRPRODS'] == 'Y';
        });
        $KMA_SRPRODS = count($KMA_SRPRODSArray);


        $KMA_TRAVELArray = array_filter($items, function($item) {
            return $item['doc']['KMA_TRAVEL'] == 'Y';
        });
        $KMA_TRAVEL = count($KMA_TRAVELArray);


        $KMA_TRAVEL_HIGHENDArray = array_filter($items, function($item) {
            return $item['doc']['KMA_TRAVEL_HIGHEND'] == 'Y';
        });
        $KMA_TRAVEL_HIGHEND = count($KMA_TRAVEL_HIGHENDArray);


        $KMA_TRAVEL_LOWENDArray = array_filter($items, function($item) {
            return $item['doc']['KMA_TRAVEL_LOWEND'] == 'Y';
        });
        $KMA_TRAVEL_LOWEND = count($KMA_TRAVEL_LOWENDArray);


        $KMA_WOMFASHArray = array_filter($items, function($item) {
            return $item['doc']['KMA_WOMFASH'] == 'Y';
        });
        $KMA_WOMFASH = count($KMA_WOMFASHArray);


        $KMA_WOMFASH_HIGHENDArray = array_filter($items, function($item) {
            return $item['doc']['KMA_WOMFASH_HIGHEND'] == 'Y';
        });
        $KMA_WOMFASH_HIGHEND = count($KMA_WOMFASH_HIGHENDArray);


        $KMA_WOMFASH_LOWENDArray = array_filter($items, function($item) {
            return $item['doc']['KMA_WOMFASH_LOWEND'] == 'Y';
        });
        $KMA_WOMFASH_LOWEND = count($KMA_WOMFASH_LOWENDArray);


        $KMA_MENFASHArray = array_filter($items, function($item) {
            return $item['doc']['KMA_MENFASH'] == 'Y';
        });
        $KMA_MENFASH = count($KMA_MENFASHArray);


        $KMA_TECHNOLOGYArray = array_filter($items, function($item) {
            return $item['doc']['KMA_TECHNOLOGY'] == 'Y';
        });
        $KMA_TECHNOLOGY = count($KMA_TECHNOLOGYArray);


        $KMA_DEALSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_DEALS'] == 'Y';
        });
        $KMA_DEALS = count($KMA_DEALSArray);


        $KMA_FOODWINEArray = array_filter($items, function($item) {
            return $item['doc']['KMA_FOODWINE'] == 'Y';
        });
        $KMA_FOODWINE = count($KMA_FOODWINEArray);


        $KMA_GAMERSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_GAMERS'] == 'Y';
        });
        $KMA_GAMERS = count($KMA_GAMERSArray);


        $KMA_DECORArray = array_filter($items, function($item) {
            return $item['doc']['KMA_DECOR'] == 'Y';
        });
        $KMA_DECOR = count($KMA_DECORArray);


        $KMA_HOMEGARDArray = array_filter($items, function($item) {
            return $item['doc']['KMA_HOMEGARD'] == 'Y';
        });
        $KMA_HOMEGARD = count($KMA_HOMEGARDArray);


        $KMA_KIDSPRODArray = array_filter($items, function($item) {
            return $item['doc']['KMA_KIDSPROD'] == 'Y';
        });
        $KMA_KIDSPROD = count($KMA_KIDSPRODArray);


        $KMA_MOTORCYCLEArray = array_filter($items, function($item) {
            return $item['doc']['KMA_MOTORCYCLE'] == 'Y';
        });
        $KMA_MOTORCYCLE = count($KMA_MOTORCYCLEArray);


        $KMA_CURREVENTArray = array_filter($items, function($item) {
            return $item['doc']['KMA_CURREVENT'] == 'Y';
        });
        $KMA_CURREVENT = count($KMA_CURREVENTArray);


        $KMA_POL_LEFTArray = array_filter($items, function($item) {
            return $item['doc']['KMA_POL_LEFT'] == 'Y';
        });
        $KMA_POL_LEFT = count($KMA_POL_LEFTArray);


        $KMA_POL_RIGHTArray = array_filter($items, function($item) {
            return $item['doc']['KMA_POL_RIGHT'] == 'Y';
        });
        $KMA_POL_RIGHT = count($KMA_POL_RIGHTArray);


        $KMA_POL_INDArray = array_filter($items, function($item) {
            return $item['doc']['KMA_POL_IND'] == 'Y';
        });
        $KMA_POL_IND = count($KMA_POL_INDArray);


        $KMA_SWEEPSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_SWEEPS'] == 'Y';
        });
        $KMA_SWEEPS = count($KMA_SWEEPSArray);



        $KMA_DONORSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_DONORS'] == 'Y';
        });
        $KMA_DONORS = count($KMA_DONORSArray);

        $KMA_CRAFTS_COLLArray = array_filter($items, function($item) {
            return $item['doc']['KMA_CRAFTS_COLL'] == 'Y';
        });
        $KMA_CRAFTS_COLL = count($KMA_CRAFTS_COLLArray);

        $KMA_AUTOMOTIVEArray = array_filter($items, function($item) {
            return $item['doc']['KMA_AUTOMOTIVE'] == 'Y';
        });
        $KMA_AUTOMOTIVE = count($KMA_AUTOMOTIVEArray);

        $KMA_OUTDOORSArray = array_filter($items, function($item) {
            return $item['doc']['KMA_OUTDOORS'] == 'Y';
        });
        $KMA_OUTDOORS = count($KMA_OUTDOORSArray);

        $li_order_id = $this->tblLineItem->field('li_order_id', array('li_id' => $line_id));
        $this->set('li_order_id', $li_order_id);

        //print_r($tblKmaAgeGroup);
        $this->set('tblKmaMaritalStats', $tblKmaMaritalStats);
        $this->set('tblKmaAgeGroup', $tblKmaAgeGroup);
        $this->set('tblKmaEducation', $tblKmaEducation);
        $this->set('tblKmaOccup', $tblKmaOccup);
        $this->set('tblKmahhinc', $tblKmahhinc);
        $this->set('tblKmaNetWorth', $tblKmaNetWorth);
        $this->set('tblKmaOwnRent', $tblKmaOwnRent);
        $this->set('tblKmaHsg', $tblKmaHsg);
        $this->set('tblKmaLmos', $tblKmaLmos);
        $this->set('KMA_OUTDOORS', $KMA_OUTDOORS);
        $this->set('KMA_AUTOMOTIVE', $KMA_AUTOMOTIVE);
        $this->set('KMA_CRAFTS_COLL', $KMA_CRAFTS_COLL);
        $this->set('KMA_DONORS', $KMA_DONORS);
        $this->set('KMA_SWEEPS', $KMA_SWEEPS);
        $this->set('KMA_POL_IND', $KMA_POL_IND);
        $this->set('KMA_POL_RIGHT', $KMA_POL_RIGHT);
        $this->set('KMA_POL_LEFT', $KMA_POL_LEFT);
        $this->set('KMA_CURREVENT', $KMA_CURREVENT);
        $this->set('KMA_MOTORCYCLE', $KMA_MOTORCYCLE);
        $this->set('KMA_KIDSPROD', $KMA_KIDSPROD);
        $this->set('KMA_HOMEGARD', $KMA_HOMEGARD);
        $this->set('KMA_DECOR', $KMA_DECOR);
        $this->set('KMA_GAMERS', $KMA_GAMERS);
        $this->set('KMA_FOODWINE', $KMA_FOODWINE);
        $this->set('KMA_DEALS', $KMA_DEALS);
        $this->set('KMA_TECHNOLOGY', $KMA_TECHNOLOGY);
        $this->set('KMA_MENFASH', $KMA_MENFASH);
        $this->set('KMA_WOMFASH_LOWEND', $KMA_WOMFASH_LOWEND);
        $this->set('KMA_WOMFASH_HIGHEND', $KMA_WOMFASH_HIGHEND);
        $this->set('KMA_WOMFASH', $KMA_WOMFASH);
        $this->set('KMA_TRAVEL_LOWEND', $KMA_TRAVEL_LOWEND);
        $this->set('KMA_TRAVEL_HIGHEND', $KMA_TRAVEL_HIGHEND);
        $this->set('KMA_TRAVEL', $KMA_TRAVEL);
        $this->set('KMA_SRPRODS', $KMA_SRPRODS);
        $this->set('KMA_PETS', $KMA_PETS);
        $this->set('KMA_PETS_DOGS', $KMA_PETS_DOGS);
        $this->set('KMA_PETS_CATS', $KMA_PETS_CATS);
        $this->set('KMA_INVEST_LOWEND', $KMA_INVEST_LOWEND);
        $this->set('KMA_INVEST_HIGHEND', $KMA_INVEST_HIGHEND);
        $this->set('KMA_OPSEEK', $KMA_OPSEEK);
        $this->set('KMA_INVESTMENTS', $KMA_INVESTMENTS);
        $this->set('KMA_EXERCISE', $KMA_EXERCISE);
        $this->set('KMA_HEALTH', $KMA_HEALTH);
        $this->set('KMA_WTLOSS', $KMA_WTLOSS);
        $this->set('KMA_SMOKER', $KMA_SMOKER);
        $this->set('KMA_CIGAR_SMOKER', $KMA_CIGAR_SMOKER);
        $this->set('KMA_CONTED', $KMA_CONTED);
        $this->set('KMA_SOHO', $KMA_SOHO);
        $this->set('KMA_HOMEOFFICE', $KMA_HOMEOFFICE);
        $this->set('duration', $duration);
        $this->set('line_id', $line_id);
    }

    function coreg_detail($ad_uid, $line_item_id, $flight_id, $t = 1) {
        $tblAdunits = $this->tblAdunit->find('first', array('conditions' => array('ad_uid' => $ad_uid)));
        $this->set('flight_id', $flight_id);
        $this->set('line_item_id', $line_item_id);
        $this->set('ad_uid', $ad_uid);
        $this->set('adunit', $tblAdunits);
       
        //$detail_content =  $this->coreg_detail_content();
        //$this->set('detail_content', $detail_content);
        
    }

    public function coreg_detail_content($cr_id = null, $fl_id = null, $line_id = null) {
        configure::write('debug', 0);
        $this->autoRender = false;
        $this->loadModel('Authake.tblFlight');
        $options['conditions'] = array('tblCreative.cr_id' => $cr_id);
        $options['fields'] = array('tblCreative.*');
        $tblCreative = $this->tblCreative->find("first", $options);

        $flight_detils = $this->tblFlight->find("first", array('conditions' => array('tblFlight.fl_id' => $fl_id)));

        $destination = Configure::read('Path.url_creative_image');
        $path = Configure::read('Path.creative_image');
        $siteurl = Router::url('/', true);
        if (!empty($tblCreative['tblCreative']['cr_banner_name']) && file_exists($path . $tblCreative['tblCreative']['cr_banner_name'])) {
            $logo = $destination . $tblCreative['tblCreative']['cr_banner_name'];
        } else {
            $logo = $siteurl . "/coreg/images/no_logo_small.gif";
        }
        $company_name = $tblCreative['tblCreative']['cr_header'];
        if (!empty($company_name) and ! empty($tblCreative)) {

            $optionm['conditions'] = array('tblCreative.cr_id' => $cr_id);
            $optionm['joins'] = array(
                array('table' => 'tbl_line_items',
                    'alias' => 'tblLineItem',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tblCreative.cr_lid = tblLineItem.li_dfp_id')
                ),
                array('table' => 'tbl_mapped_adunits',
                    'alias' => 'tblMappedAdunit',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tblMappedAdunit.ma_l_id = tblLineItem.li_id')
            ));
            $optionm['fields'] = array('tblMappedAdunit.ma_uid', 'tblMappedAdunit.ma_l_id', 'tblMappedAdunit.ma_ad_id', 'tblLineItem.li_name', 'tblLineItem.li_dfp_id', 'tblLineItem.li_order_id', 'tblLineItem.li_status_id', 'tblCreative.*');


            $data = $this->tblCreative->find('first', $optionm);

            $redirect_url = Router::url(array('controller' => 'orders', 'action' => 'view_line_creatives/$line_id'));


            $content = "<div id='das_coregMain'><div style='position: absolute;    position: fixed;    top: 50%;    left: 50%;    border: 1px solid #2D527F;    background-color: White;    font-family: Verdana, Arial, Helvetica, sans-serif;    width: 200px;    height: 45px;    z-index: 500000;    margin-left: -100px;    margin-top: -50px; display: none;' id='dvprogress'>            <div style='float: left; padding-top: 8px;'>                <img src='" . $siteurl . "/coreg/images/loading.gif' alt='Please Wait..' style='margin-left: 15px; margin-top: 5px; height:16px; width:16px;' />            </div>            <div style='float: left; font-size:12px;   padding-top: 14px;    padding-left: 4px;'>                Processing..........            </div>        </div><style type='text/css'>.das_form-control {    background-color: #fff;    background-image: none;    border: 1px solid #ccc;    border-radius: 4px;    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;    color: #555;    display: block;    font-size: 16px;    height: 35px;    line-height: 1.42857;    padding: 6px 12px;    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;    width: 275px;}.das_form-control-invalid {    background-color: #fff;    background-image: none;    border: 1px solid #ff9231;    border-radius: 4px;    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;    color: #555;    display: block;    font-size: 16px;    height: 35px;    line-height: 1.42857;    padding: 6px 12px;    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;    width: 275px;} .button {    background-attachment: scroll;    background-color: #6ae167;   background-image: linear-gradient(to bottom, #6ae167, #4bc112);    background-position: 0 0;    background-repeat: repeat-x;    border-radius: 25px;    color: #fff;    display: inline-block;    font-size: 19px;    font-weight: bold;    padding: 6px 7px 6px 10px;    text-decoration: none;    text-shadow: 0 1px 1px #478d35;} .button span {    background: rgba(0, 0, 0, 0) url('" . $siteurl . "/coreg/images/arrow.png') no-repeat scroll right center;    display: inline-block;    line-height: 36px;    padding-right: 40px;} .button:hover {    background: #4bc112 none repeat scroll 0 0;} </style><script type='text/javascript' src='" . $siteurl . "/coreg/js/jquery.js'></script><script type='text/javascript' src='" . $siteurl . "/coreg/js/das_common.js'></script><script type='text/javascript' src='" . $siteurl . "/coreg/js/jquery.maskedinput.js'></script><script src='" . $siteurl . "/coreg/js/fp_details.js' type='text/javascript'></script><script src='http://digitaladvertising.systems/js/myipReturn.php' type='text/javascript'></script> <link rel='stylesheet' href='" . $siteurl . "/coreg/css/colorbox.css' /> <link href='" . $siteurl . "/coreg/css/stylesheet-pure-css.css' rel='stylesheet' type='text/css' /> <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script> <script type='text/javascript' src='" . $siteurl . "/coreg/js/jquery.colorbox.js'></script> <script type='text/javascript'> $(document).ready(function () {  $('.iframe').colorbox({ iframe: true, width: '80%', height: '80%' }); }); </script><div style='max-width:980px; margin:0 auto;padding-top:15px;'><input type='hidden' id='das_posting_params_156'  value='http://services.digitaladvertising.systems/data/coregpost.aspx?od_dfpid=" . $data['tblLineItem']['li_dfp_id'] . "&od_lid=" . $data['tblLineItem']['li_order_id'] . "&od_cid=" . $data['tblCreative']['cr_lid'] . "&od_ad_id=" . $data['tblMappedAdunit']['ma_ad_id'] . "&od_map_id=" . $data['tblMappedAdunit']['ma_uid'] . "&od_site=314&od_ipaddress=" . $_SERVER['SERVER_ADDR'] . "' /><img id='das_impx156' height='1' width='1' style='display:none' src='http://tracking.digitaladvertising.systems/piwik.php?idsite=314&rec=1&c_n=" . $data['tblCreative']['cr_lid'] . "&c_p=" . $data['tblMappedAdunit']['ma_uid'] . "' /><iframe id='das_posting_156' width='0px' height='0px' frameborder='0' ></iframe> <div style='border-bottom:1px solid #49769f; padding:5px 15px;'><div style='float:left; width:160px; position:relative;'><img src='" . $logo . "' alt='' style='max-width:120px;' /> <input id='das_chk_156' type='checkbox' style='float:right;' onclick='javarscript: calltoAction(this,156);' /> <div style='clear:both'></div></div><div style='margin-left:170px;cursor:pointer;' onclick='javascript: if(document.getElementById(&#39;das_chk_156&#39;).checked==true) document.getElementById(&#39;das_chk_156&#39;).checked=false; else document.getElementById(&#39;das_chk_156&#39;).checked=true; calltoAction(document.getElementById(&#39;das_chk_156&#39;),156);'>	<h4 style='font-size:16px; margin:0 0 2px; padding:0; font-family:Arial, Helvetica, sans-serif; line-height:1.2; font-weight:bold; color:#000;'> " . $company_name . "</h4>  <p style='margin:0 0 5px; padding:0; color:#000; line-height:1.2; font-size:14px; font-family:Arial, Helvetica, sans-serifl'> " . $tblCreative['tblCreative']['cr_body'] . " </p>   <p style='text-align:right; margin:0; line-height:1.2; font-size:12px;'>  	<a href='" . $tblCreative['tblCreative']['cr_privacy_url'] . "' style='font-size:12px; text-decoration:underline; color:#49769f;font-family:Arial, Helvetica, sans-serifl' target='_blank' class='iframe' >Privacy Policy</a>  </p> </div> <div style='clear:both;'></div> </div></div> <input type='hidden' id='das_li_fields_156' value='email,'/> <input type='hidden' id='das_global_fields' value='email,' /> <input type='hidden' id='das_global_change' value='' /><input type='hidden' id='das_selected_offers' value='' /></div> <div style='border-top:1px solid #49769f; padding:20px 15px; max-width:980px; margin: 0 auto;'> 	<div id='dv_das_firstname' style='padding-bottom:10px; display:none'> 	<div style='float:left; width:120px; position:relative;color: #555; font-weight: bold; padding-top: 5px;'>         	First Name:             <div style='clear:both'></div>         </div> 		<div style='margin-left:120px;'>         	<input type='text' id='das_firstname' class='das_form-control' value=''>         </div> 	<div style='clear:both;'></div> 	</div> 	<div id='dv_das_lastname' style='padding-bottom:10px; display:none'> 	<div style='float:left; width:120px; position:relative;color: #555; font-weight: bold; padding-top: 5px; '>         	Last Name:             <div style='clear:both'></div>         </div> 		<div style='margin-left:120px;'>         	<input type='text' id='das_lastname' class='das_form-control' value=''>         </div> 	<div style='clear:both;'></div> 	</div> 	<div id='dv_das_email' style='padding-bottom:10px; display:none'> 	<div style='float:left; width:120px; position:relative;color: #555; font-weight: bold; padding-top: 5px; '>         	Email Address:             <div style='clear:both'></div>         </div> 		<div style='margin-left:120px;'>         	<input type='text' id='das_email' class='das_form-control' value=''>         </div> 	<div style='clear:both;'></div> 	</div> 	<div id='dv_das_phone' style='padding-bottom:10px; display:none'> 	<div style='float:left; width:120px; position:relative;color: #555; font-weight: bold; padding-top: 5px; '>         	Phone Number:             <div style='clear:both'></div>         </div> 		<div style='margin-left:120px;'>         	<input type='text' id='das_phone' class='das_form-control' value=''>         </div> 	<div style='clear:both;'></div> 	</div> 	<div id='dv_das_address' style='padding-bottom:10px; display:none'> 	<div style='float:left; width:120px; position:relative;color: #555; font-weight: bold; padding-top: 5px; '>         	Street Address:             <div style='clear:both'></div>         </div> 		<div style='margin-left:120px;'>         	<input type='text' id='das_street_address' class='das_form-control' value=''>         </div> 	<div style='clear:both;'></div> 	</div> 	<div id='dv_das_City' style='padding-bottom:10px; display:none'> 	<div style='float:left; width:120px; position:relative;color: #555; font-weight: bold; padding-top: 5px; '>         	City:             <div style='clear:both'></div>         </div> 		<div style='margin-left:120px;'>         	<input type='text' id='das_city' class='das_form-control' value=''>         </div> 	<div style='clear:both;'></div> 	</div> 	<div id='dv_das_button' style='padding-bottom:10px;'> 	<div style='float:left; width:120px; position:relative;color: #555; font-weight: bold; padding-top: 5px; '>             <div style='clear:both'></div>         </div> 		<div style='margin-left:120px;'>  <a id='das_submit_coreg' class='button' onclick='javascript: return das_submitoffers();' href='#'><span>SUBMIT &amp; CONTINUE</span></a>        </div> 	<div style='clear:both;'></div> 	</div> 	</div> <script type='text/javascript'>     $( document ).ready(function() { das_field_init(); }); function das_field_init(){}  function calltoAction(caller, lid) {  var chk = caller;  if (chk.checked) {      var selOffers = document.getElementById('das_selected_offers').value;                  document.getElementById('das_selected_offers').value += 'das_li_fields_' + lid + ',';      var fields = document.getElementById('das_li_fields_' + lid).value.split(',');      for (j = 0; j < fields.length; j++) {          if (fields[j] != '') {             var global = document.getElementById('das_global_change').value;              if (global.indexOf(fields[j]) < 0) {                  document.getElementById('das_global_change').value += fields[j] + ',';              }              switch (fields[j]) {                  case 'email': document.getElementById('dv_das_email').style.display = 'block';                      break;                  case 'firstname': document.getElementById('dv_das_firstname').style.display = 'block';                      break;                  case 'lastname': document.getElementById('dv_das_lastname').style.display = 'block';                      break;                  case 'phone': document.getElementById('dv_das_phone').style.display = 'block';                      break;              }          }      }  }  else {      document.getElementById('das_global_change').value = '';      var selOffers = document.getElementById('das_selected_offers').value;      document.getElementById('das_selected_offers').value = selOffers.replace('das_li_fields_' + lid + ',', '');      document.getElementById('dv_das_firstname').style.display = 'none';      document.getElementById('dv_das_lastname').style.display = 'none';      document.getElementById('dv_das_email').style.display = 'none';      document.getElementById('dv_das_phone').style.display = 'none';      if (document.getElementById('das_selected_offers').value != '') {          var dasofferfields = document.getElementById('das_selected_offers').value.split(',');          for (var i = 0; i < dasofferfields.length; i++) {              if (dasofferfields[i] != '') {                  var allowvisiblity = document.getElementById(dasofferfields[i]).value.split(',');                  for (var k = 0; k < allowvisiblity.length; k++) {                      if (allowvisiblity[k] != '') {                          document.getElementById('dv_das_' + allowvisiblity[k]).style.display = 'block';                           var global = document.getElementById('das_global_change').value;                           if (global.indexOf(allowvisiblity[k]) < 0) {                               document.getElementById('das_global_change').value += allowvisiblity[k] + ',';                           }                      }                  }              }          }      }      var fields = document.getElementById('das_li_fields_' + lid).value.split(',');  } }     function das_validatefields() {       var activeFlds = document.getElementById('das_global_change').value.split(',');       var err = 0;       for (var i = 0; i < activeFlds.length; i++) {           if (activeFlds[i] != '') {               switch (activeFlds[i]) {                   case 'email':                       var email = document.getElementById('das_email').value;                       if (validateEmail(email) == false) {                           document.getElementById('das_email').className = 'das_form-control-invalid';                           err++;                       }                       else {                           document.getElementById('das_email').className = 'das_form-control';                       }                       break;                   case 'firstname':                       var firstname = document.getElementById('das_firstname').value;                       if (firstname == '') {                           document.getElementById('das_firstname').className = 'das_form-control-invalid';                           err++;                       }                       else {                           document.getElementById('das_firstname').className = 'das_form-control';                       }                       break;                   case 'lastname':                       var lastname = document.getElementById('das_lastname').value;                       if (lastname == '') {                           document.getElementById('das_lastname').className = 'das_form-control-invalid';                           err++;                       }                       else {                           document.getElementById('das_lastname').className = 'das_form-control';                       }                       break;                   case 'phone':                       var phone = document.getElementById('das_phone').value;                       if (phone == '') {                           document.getElementById('das_phone').className = 'das_form-control-invalid';                           err++;                       }                       else if(checkPhone(phone)==false){                           document.getElementById('das_phone').className = 'das_form-control-invalid';                           err++;                       }                       else {                           document.getElementById('das_phone').className = 'das_form-control';                       }                       break;               }           }       }       if (err > 0)           return false;       else           return true;   }   function das_submitoffers() {       var isValid = das_validatefields();       if (isValid) {       var fp_browser = fingerprint_browser();       var fp_connection = fingerprint_connection();       var fp_display = fingerprint_display();       var fp_flash = fingerprint_flash();       var fp_language = fingerprint_language();       var fp_os = fingerprint_os();       var fp_timezone = fingerprint_timezone();       var fp_useragent = fingerprint_useragent();       var fp_string = '&fp_browser=' + fp_browser + '&fp_connection=' + fp_connection + '&fp_display=' + fp_display + '&fp_flash=' + fp_flash + '&fp_language=' + fp_language + '&fp_os=' + fp_os + '&fp_timezone=' + fp_timezone + '&fp_useragent=' + fp_useragent;       var seloffers = document.getElementById('das_selected_offers').value;       if (seloffers != '') {           var offers = seloffers.split(',');           for (var i = 0; i < offers.length; i++) {               if (offers[i] != '') {                   var pFrame = offers[i].replace('das_li_fields_', 'das_posting_');                   var url = document.getElementById(offers[i].replace('das_li_fields_', 'das_posting_params_')).value;                   var fields = document.getElementById(offers[i]).value.split(',');                   var fldstring = '';                   for (var f = 0; f < fields.length; f++) {                   if (fields[f] != '') {                   switch (fields[f]) {                   case 'email': fldstring += '&od_email=' + document.getElementById('das_email').value;                   break;                   case 'firstname': fldstring += '&od_firstname=' + document.getElementById('das_firstname').value;                   break;                   case 'lastname': fldstring += '&od_lastname=' + document.getElementById('das_lastname').value;                   break;                   case 'phone': fldstring += '&od_phone=' + document.getElementById('das_phone').value;                   break;                   }                   }                   }                   document.getElementById(pFrame).src = url + fldstring + fp_string;                   return Delayer();               }           }       }       }       else           return false;   }function Delayer(){ ShowProgress(); if(document.getElementById('das_submit_coreg')!=null){ document.getElementById('das_submit_coreg').disabled=true;} setTimeout( 'Redirection()' , 4000); return true;} function Redirection(){  window.parent.location = '" . $redirect_url . "'; CloseProgress(); } function ShowProgress() { document.getElementById('dvprogress').style.display = 'inline'; } function CloseProgress() { document.getElementById('dvprogress').style.display = 'none'; }</script>  ";
        } else {
            $content = "<div id='das_coregMain'>Sorry no offer to display</div>";
        }
        echo 'document.write("' . $content . '");';
    }

    // this function is used for start and pause for order list         
    function update_play_action($action = 0, $order_id = null) {
        $this->autoRender = false;

        $userId = $this->Authake->getUserId();
        $options['conditions'] = array('tblOrder.isActive' => 1, 'tblOrder.order_id' => $order_id);
        if (!empty($user_id)) {
            $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
                )),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblOrder.dfp_order_id = tblLineItem.li_order_id')
        ));
        $options['order'] = array(
            'tblOrder.created_at' => 'DESC');
        $options['fields'] = array('tblLineItem.li_id', 'tblLineItem.li_id');
        $line_items = $this->tblOrder->find('list', $options);

        if (!empty($line_items)) {
            $this->tblOrder->updateAll(array('status' => $action), array('order_id' => $order_id));
            if (!empty(array_filter($line_items)) and $action != 1) {
                $this->tblLineItem->updateAll(array('tbl_lineitem_status' => $action), array('li_id' => $line_items));
                self::update_lineitems_flights($action, $line_items);
            }
            return 1;
        } else {
            return 0;
        }
    }
    
    /**
	* 
	* @module     : Ajax Fire for Creative Play & Stop
	* @Created By : Satanik
	* @date       : 21/1/2017
	*/
    function update_play_action_creative() {
        $this->autoRender = false;
        $userId = $this->Authake->getUserId();
        
        $action = $_REQUEST['action'];
        $cr_id  = $_REQUEST['cr_id'];
        $cr_lid = $_REQUEST['cr_lid'];
        
        $update = $this->tblCreative->updateAll(array('active_status' => $action), array('cr_id' => $cr_id,'cr_lid' => $cr_lid));
        if($update){
			echo $action;
		} else {
			echo $action;
		}
        exit;
    }

    private function update_lineitems_flights($action = 0, $li_id = array()) {
        $this->loadModel('Authake.tblFlight');
        $this->autoRender = false;
        $userId = $this->Authake->getUserId();
        $optionm['conditions'] = array('tblMappedAdunit.ma_l_id' => $li_id, 'tblMappedAdunit.ma_isActive' => '1');
        if (!empty($user_id)) {
            $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        $optionm['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id = tblLineItem.li_id')
            ),
            array('table' => 'tbl_flights',
                'alias' => 'tblFlight',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_fl_id = tblFlight.fl_id')
            ),
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id')),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID')));
        $optionm['fields'] = array('tblFlight.fl_id', 'tblFlight.fl_id');

        $line_items = $this->tblMappedAdunit->find('list', $optionm);
        if (!empty($line_items)) {
            $this->tblFlight->updateAll(array('status' => $action), array('fl_id' => $line_items));
            return 1;
        } else {
            return 0;
        }
    }

    function reports($pagedata = null) {
        // add script and load model
        self::CommonScriptReport();
        $this->set('title_for_layout', 'Line Item Reports');
        // Breadcrums of line item report
        $this->breadcrumbs[] = array(
            'url' => Router::url('/'),
            'name' => 'Home'
        );
        $this->breadcrumbs[] = array(
            'url' => Router::url(array('controller' => 'orders', 'action' => 'index')),
            'name' => 'Order'
        );
        $this->breadcrumbs[] = array(
            'url' => '#',
            'name' => 'Line Item Reports'
        );
        // listing here 
        $userId = $this->Authake->getUserId();
        $conditions = array();
        $this->paginate = array();
        $this->paginate['limit'] = 10; // set here pagination limit
        $conditions[] = array('tblOrder.isActive' => 1);
        if (!empty($user_id)) {
            $conditions[] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        //$this->request->data['daterange']=str_replace('_','/',$this->request->query['daterange']);
        if (!empty($this->request->data['company_id']) and ( !in_array('all', $this->request->data['company_id']))) {
            $conditions[] = array('tblOrder.advertiser_user_id' => $this->request->data['company_id']);
        }
        if (!empty($this->request->data['order_id']) and ( !in_array('all', $this->request->data['order_id']))) {
            $conditions[] = array('tblOrder.dfp_order_id' => $this->request->data['order_id']);
        }
        if (!empty($this->request->data['status_id']) and ( !in_array('all', $this->request->data['status_id']))) {
            $conditions[] = array('tblLineItem.li_status_id' => $this->request->data['status_id']);
        }
        if (!empty($this->request->data['media_id']) and ( !in_array('all', $this->request->data['media_id']))) {
            $conditions[] = array('tblFlight.fl_ptype_id' => $this->request->data['media_id']);
        }
        $range = array();

        if (!empty($this->request->data['daterange']) and $this->request->data['daterange'] != 'null') {
            $daterange = explode('-', $this->request->data['daterange']);
            $start_date = $this->Commonfunction->edit_datepickerFormat($daterange[0]);
            $end_date = $this->Commonfunction->edit_datepickerFormat($daterange[1]);
				//$conditions[] = array('OR' =>
				//  array('date(tblLineItem.li_start_date)  between ? and ?' => array($start_date, $end_date),
                //    'date(tblLineItem.li_end_date)  between ? and ?' => array($start_date, $end_date)
				//));
				$date=date_create($start_date);
				$start_date = date_format($date,"Y-m-d H:i:s");
				$date=date_create($end_date);
				$end_date = date_format($date,"Y-m-d H:i:s");
            $range['start_date'] = $start_date;
            $range['end_date'] = $end_date;
        }

        $this->paginate['joins'] = array(
            array('table' => 'tbl_orders',
                'alias' => 'tblOrder',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.dfp_order_id = tblLineItem.li_order_id'
                )),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
                )),
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tblMappedAdunit',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id = tblLineItem.li_id'
                )),
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblMappedAdunit.ma_uid'
                )),
            array('table' => 'tbl_flights',
                'alias' => 'tblFlight',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMappedAdunit.ma_fl_id = tblFlight.fl_id')
            ),
            array('table' => 'tbl_cost_structures',
                'alias' => 'tcs',
                'type' => 'INNER',
                'conditions' => array(
                    'tcs.cs_id = tblLineItem.li_cs_id'
                )),
            array('table' => 'tbl_lineitem_status',
                'alias' => 'tls',
                'type' => 'INNER',
                'conditions' => array(
                    'tls.ls_id = tblLineItem.li_status_id'
                )),
        );
        $this->paginate['fields'] = array('tblp.CompanyName', 'tblLineItem.*', 'tcs.cs_id', 'tcs.cs_name', 'tls.*', 'COALESCE(SUM(tblMailing.m_open),0) as opens', 'COALESCE(SUM(tblMailing.m_sent),0) as totalsent', 'COALESCE(SUM(tblMailing.m_clicks),0) as clicks', 'COALESCE(SUM(tblMailing.m_delivered),0) as delivered', 'COALESCE(SUM(tblMailing.m_bounce),0) as bounce', 'COALESCE(SUM(tblMailing.m_spam),0) as compain', 'COALESCE(SUM(tblMappedAdunit.ma_imp),0) as ma_imp');
        $this->paginate['order'] = array('tblLineItem.li_name' => 'ASC');
        $this->paginate['group'] = 'tblLineItem.li_id';
        $lineitems = $this->paginate("tblLineItem", $conditions);
        if (!empty($lineitems)) {
            foreach ($lineitems as $key => $lineitem) {
                // feching chart/ and report days record
                $lineitems[$key]['tblReportDay'] = $this->Reporting->tblReportByOrder($lineitem['tblLineItem']['li_order_id'], $lineitem['tblLineItem']['li_dfp_id'], $range, 30, $this->Authake->getUserId()); // here 30 used for 1 month show chart  
                $lineitems[$key]['total_active_flight'] = $this->tblMappedAdunit->get_total_active_flight($lineitem['tblLineItem']['li_id']);
                $lineitems[$key]['total_active_completed_flight'] = $this->tblMappedAdunit->get_total_completed_flight($lineitem['tblLineItem']['li_id']);
            }
        }
        $this->set('lineitems', $lineitems);
    }

    function getAllOrderList() {
        $this->autoRender = false;
        // get data from tblOrder mode       
        return $this->tblOrder->GetJsonOrderList($this->request->data, $this->Authake->getUserId());
    }

    # This commonscript for Lineitem,flight and adunit report

    private function CommonScriptReport() {
        // load model
        $this->loadModel('Authake.tblFlight');
        // add Script 
        $this->System->add_js(array('/plugins/jquery-validation/js/jquery.validate.min.js', '/plugins/jquery-validation/js/additional-methods.min.js', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js', '/plugins/jquery-multi-select/js/jquery.multi-select.js', '/plugins/datatables/media/js/jquery.dataTables.min.js', '/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js', '/scripts/table-managed.js', '/plugins/justgage/justgage.js', '/plugins/justgage/raphael-2.1.4.min.js', '/plugins/icheck/icheck.min.js', '/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', '/plugins/quicksearch-master/jquery.quicksearch.js', '/scripts/form-validation.js', '/plugins/bootstrap-daterangepicker/moment.min.js', '/plugins/bootstrap-daterangepicker/daterangepicker.js'), 'foot');

        $this->loadModel('Authake.tblCostStructure');
        $this->loadModel('Authake.tblProfile');
        $this->loadModel('Authake.tblProducttype');
		$this->loadModel('Authake.tblMediaType');
        $this->loadModel('Authake.tblLineitemStatus');
		
		//publisher name
		
		 $ids = $this->tblProfile->getAllConnectedProfileUserIds();


        $options['conditions'][] = array(
            'tblAdunit.ad_parent_id' => 0,
            'tblAdunit.ad_isactive' => 1,
            'or' => array(
                'tblAdunit.owner_user_id' => array($this->Authake->getUserId()),
                'and' => array(
                    'tblAdunit.owner_user_id' => $ids,
                    'tblAdunit.ad_isPublicVisible' => 1))
        );

        $options['joins'] = array(
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id'
                )),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID'
                )),
            array('table' => 'tbl_product_types',
                'alias' => 'tbpt',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_ptype_id = tbpt.ptype_id'
                )),
            array('table' => 'tbl_media_types',
                'alias' => 'tbmt',
                'type' => 'INNER',
                'conditions' => array(
                    'tbmt.mtype_id = tbpt.ptype_m_id'
        )));
		$options['group'] = array('tblAdunit.publisher_id');
        $options['fields'] = array('tblAdunit.publisher_id', 'tblp.CompanyName');

        $publishers = $this->tblAdunit->find('list', $options);
		
		//foreach($publishers as $key => $pubname){
		//	$publisher[] = $pubname[$key]['CompanyName'];
			//}
		
		$this->set('publishers', $publishers);
	
        // header field

        $this->set('orderlist', $this->tblOrder->GetOrderList($this->request->data, $this->Authake->getUserId()));
        $companylist = $this->tblProfile->getListConnectedProfiles(); //('tblProfile.dfp_company_id')
        $companyaddbefore['all'] = 'All';
        $this->set('companylist', $companyaddbefore + $companylist);
        // get all line item
        $this->set('tblLineItems', $this->tblLineItem->getListLineItems());
		// get all line item with line item status
		$this->set('tblLineItemswithstatus', $this->tblLineItem->getListLineItemswithstatus());
		
        $this->set('status', $this->tblLineitemStatus->getlist());
        $this->set('medialist', $this->tblProducttype->getProductTypeList());
		$this->set('mediatypelist', $this->tblMediaType->getMediaTypeList());
		$this->set('producttype', $producttype = $this->tblProducttype->getProducttype());
		$costStructure = $this->tblCostStructure->getCostStructure();
        $this->set('costStructure', $costStructure);

        // for submit request 
        if ($this->request->is('post')) {
            if ($this->request->data['submit_value'] == 'submit') {
                $this->Session->write('ReportPost', $this->request->data); // write here post value in session  
            }
        }
        // delete session 
        if (empty($this->request->params['named']) and empty($this->request->data)) {
            $this->Session->delete('ReportPost');
        }
        // read here session value of post
        $ReportPost = $this->Session->read('ReportPost');
        if (!empty($ReportPost)) {
            $this->request->data['company_id'] = (!empty($ReportPost['company_id'])) ? $ReportPost['company_id'] : '';
            $this->request->data['order_id'] = (!empty($ReportPost['order_id'])) ? $ReportPost['order_id'] : '';
            $this->request->data['status_id'] = (!empty($ReportPost['status_id'])) ? $ReportPost['status_id'] : '';
            $this->request->data['media_id'] = (!empty($ReportPost['media_id'])) ? $ReportPost['media_id'] : '';
            $this->request->data['daterange'] = (!empty($ReportPost['daterange'])) ? str_replace('_', '/', $ReportPost['daterange']) : '';
        }
    }
	
	//product type
	
	  public function getproducttype_ajax($cat_parent = 0) {

        $this->autoRender = false;
         $this->loadModel('Authake.tblProducttype');

        $producttype = $this->tblProducttype->getProducttype($cat_parent);
        //$producttypes[0]['id'] = "0";
        //$producttypes[0]['text'] = "Select (or start typing) the sub category for the line item";
        if (!empty($producttype)) {
            $count = 0;
            foreach ($producttype as $key => $value) {

                $producttypes[$count]['id'] = $key;
                $producttypes[$count]['text'] = $value;
                $count++;
            }
        }
		
        return json_encode($producttypes);
    }
	

    // this function is used for flight report
    function flight_reports($adunit_id = 0, $pagedata = null) {
        // add script and load model
        self::CommonScriptReport();
        $this->set('title_for_layout', 'Flight Reports');
        // order report breadcrums
        $this->breadcrumbs[] = array(
            'url' => Router::url('/'),
            'name' => 'Home'
        );
        $this->breadcrumbs[] = array(
            'url' => Router::url(array('controller' => 'orders', 'action' => 'index')),
            'name' => 'Order'
        );

        $this->breadcrumbs[] = array(
            'url' => '#',
            'name' => 'Flight Reports'
        );
        // listing here 
        $userId = $this->Authake->getUserId();
		$ad_id = 0;
		  if (!empty($adunit_id)) {
		  $ad_id = $this->tblAdunit->field('tblAdunit.adunit_name', array('tblAdunit.adunit_id' => $adunit_id));
		   }
        $conditions = array();
        $this->paginate = array();
        $this->paginate['limit'] = 10; // set here pagination limit
        $conditions[] = array('tblOrder.isActive' => 1);
		  if (!empty($adunit_id)) {
            $conditions[] = array('tblAdunit.adunit_id' => $adunit_id);
        }
        if (!empty($user_id)) {
            $conditions[] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        //$this->request->data['daterange']=str_replace('_','/',$this->request->query['daterange']);
        if (!empty($this->request->data['company_id']) and ( !in_array('all', $this->request->data['company_id']))) {
            $conditions[] = array('tblOrder.advertiser_user_id' => $this->request->data['company_id']);
        }
        if (!empty($this->request->data['order_id']) and ( !in_array('all', $this->request->data['order_id']))) {
            $conditions[] = array('tblOrder.dfp_order_id' => $this->request->data['order_id']);
        }
        if (!empty($this->request->data['status_id']) and ( !in_array('all', $this->request->data['status_id']))) {
            $conditions[] = array('tblLineItem.li_status_id' => $this->request->data['status_id']);
        }
        if (!empty($this->request->data['media_id']) and ( !in_array('all', $this->request->data['media_id']))) {
            $conditions[] = array('tblFlight.fl_ptype_id' => $this->request->data['media_id']);
        }
        $range = array();

        if (!empty($this->request->data['daterange']) and $this->request->data['daterange'] != 'null') {
            $daterange = explode('-', $this->request->data['daterange']);
            $start_date = $this->Commonfunction->edit_datepickerFormat($daterange[0]);
            $end_date = $this->Commonfunction->edit_datepickerFormat($daterange[1]);
            //$conditions[] = array('OR' =>
            //   array('date(tblLineItem.li_start_date)  between ? and ?' => array($start_date, $end_date),
            //        'date(tblLineItem.li_end_date)  between ? and ?' => array($start_date, $end_date)
            //));
				$date=date_create($start_date);
				$start_date = date_format($date,"Y-m-d H:i:s");
				$date=date_create($end_date);
				$end_date = date_format($date,"Y-m-d H:i:s");
            $range['start_date'] = $start_date;
            $range['end_date'] = $end_date;
        }

        $this->paginate['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tblMappedAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblFlight.fl_id=tblMappedAdunit.ma_fl_id'
                )),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id=tblMappedAdunit.ma_ad_id'
                )),
            array('table' => 'tbl_product_types',
                            'alias' => 'tbpt',
                            'type' => 'INNER',
                            'conditions' => array(
                                'tblAdunit.ad_ptype_id = tbpt.ptype_id'
                            )
                        ),
                        array('table' => 'tbl_media_types',
                            'alias' => 'tbmt',
                            'type' => 'INNER',
                            'conditions' => array(
                                'tbmt.mtype_id = tbpt.ptype_m_id'
                            )
            ),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id=tblLineItem.li_id'
                )),
            array('table' => 'tbl_orders',
                'alias' => 'tblOrder',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.dfp_order_id = tblLineItem.li_order_id'
                )),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
                )),
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblMappedAdunit.ma_uid'
                )),
            array('table' => 'tbl_cost_structures',
                'alias' => 'tcs',
                'type' => 'INNER',
                'conditions' => array(
                    'tcs.cs_id = tblLineItem.li_cs_id'
                )),
            array('table' => 'tbl_lineitem_status',
                'alias' => 'tls',
                'type' => 'INNER',
                'conditions' => array(
                    'tls.ls_id = tblLineItem.li_status_id'
                )),
        );
        $this->paginate['fields'] = array('tblp.CompanyName', 'tblFlight.fl_id', 'tblFlight.fl_name', 'tblFlight.fl_start', 'tblFlight.fl_end', 'tblFlight.fl_ptype_id', 'tblFlight.status', 'tbpt.ptype_name', 'tbmt.mtype_name','tbmt.mtype_id',
            'tblLineItem.*', 'tcs.cs_id', 'tcs.cs_name', 'tls.*', 'tblAdunit.adunit_name','tblAdunit.ad_dfp_id',
            'tblMappedAdunit.ma_uid', 'COALESCE(SUM(tblMailing.m_open),0) as opens', 'COALESCE(SUM(tblMailing.m_sent),0) as totalsent',
            'COALESCE(SUM(tblMailing.m_clicks),0) as clicks', 'COALESCE(SUM(tblMailing.m_delivered),0) as delivered',
            'COALESCE(SUM(tblMailing.m_bounce),0) as bounce', 'COALESCE(SUM(tblMailing.m_spam),0) as compain',
            'COALESCE(SUM(tblMappedAdunit.ma_imp),0) as ma_imp');
        $this->paginate['order'] = array('tblFlight.fl_name' => 'ASC');
        $this->paginate['group'] = 'tblFlight.fl_id';
        //pr($this->paginate);die;
        $flights = $this->paginate("tblFlight", $conditions);
        if (!empty($this->request->data['daterange']) and $this->request->data['daterange'] != 'null') {
                    $daterange = explode('-', $this->request->data['daterange']);
                    $start_date = $this->Commonfunction->edit_datepickerFormat($daterange[0]);
                    $end_date = $this->Commonfunction->edit_datepickerFormat($daterange[1]);
                    $startTimeStamp = strtotime($start_date);
                    $endTimeStamp = strtotime($end_date);
                    $timeDiff = abs($endTimeStamp - $startTimeStamp);
                    $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                    // and you might want to convert to integer
                    $numberDays = intval($numberDays);
                    $numberDays2 = $numberDays." days";
                    //print_r("Days");
                    //print_r($numberDays2);
                    $date=date_create($start_date);
                    $start_date = date_format($date,"Y-m-d H:i:s");
                    
                    date_sub($date,date_interval_create_from_date_string($numberDays2));
                    $back_start_date = date_format($date,"Y-m-d H:i:s");
                    $oneday = 1;
                    $back_start_date = date('Y-m-d H:i:s', strtotime('-' . $oneday . ' days', strtotime($back_start_date)));
                    $start_date2 = date('Y-m-d H:i:s', strtotime('-' . $oneday . ' days', strtotime($start_date)));
                    
                    $range2['start_date'] = $back_start_date;
                    $range2['end_date'] = $start_date2;
        }
        else{
                    $chartdays2 = 30;
                        $range2['end_date'] = date('Y-m-d H:i:s');
                        $start_date = date('Y-m-d H:i:s', strtotime('-' . $chartdays2 . ' days'));
                        
                    $numberDays2 = "30 days";
                    //print_r("Days");
                    //print_r($numberDays2);
                    $date=date_create($start_date);
                    date_sub($date,date_interval_create_from_date_string($numberDays2));
                    $back_start_date = date_format($date,"Y-m-d H:i:s");
                    $range2['start_date'] = $back_start_date;
                    $range2['end_date'] = $start_date;
        }
        if (!empty($flights)) {
            $lineItemIDs = array();
            foreach ($flights as $key => $flight) {
                $lineItemIDs[] = $flight['tblLineItem']['li_id'];
            }
            $autoResObj = ClassRegistry::init('AutoResponder');
            $autoResponder = $autoResObj->find('all', array('conditions' => array('li_id' => $lineItemIDs), 'fields' => array('li_id', 'is_autoresp')));
            foreach ($flights as $key => $flight) {
                $flights[$key]['tblReportDay'] = $this->Reporting->flight_report($flight['tblAdunit']['ad_dfp_id'],$flight['tblLineItem']['li_order_id'], $flight['tblLineItem']['li_dfp_id'], $flight['tblFlight']['fl_id'], $range, 30, $this->Authake->getUserId()); // here 30 used for 1 month show chart  
                $flights[$key]['tblReportDayLast'] = $this->Reporting->flight_report($flight['tblAdunit']['ad_dfp_id'],$flight['tblLineItem']['li_order_id'], $flight['tblLineItem']['li_dfp_id'], $flight['tblFlight']['fl_id'], $range2, 30, $this->Authake->getUserId());
                foreach ($autoResponder as $i => $auto) {
                    if ($flight['tblLineItem']['li_id'] == $auto['AutoResponder']['li_id']) {
                        $flights[$key]['AutoResponder'] = $auto['AutoResponder'];
                    }
                }
            }
        }
       //print_r('<pre>');print_r($flights);print_r('</pre>');
	   
	   $this->set('adunit_id', $ad_id);
        $this->set('flights', $flights);
    }

    /**
     * AdUnit Report Listing page
     * @param type $pagedata
     * @author Niraj
     * @created 8/11/2016
     */
    function adunit_reports($pagedata = null) {
        // add script and load model
        self::CommonScriptReport();
        $this->loadModel('Authake.tblFlight');
        $this->set('title_for_layout', 'Adunit Reports');
        // order report breadcrums
        $this->breadcrumbs[] = array(
            'url' => Router::url('/'),
            'name' => 'Home'
        );
        $this->breadcrumbs[] = array(
            'url' => Router::url(array('controller' => 'orders', 'action' => 'index')),
            'name' => 'Order'
        );
        $this->breadcrumbs[] = array(
            'url' => '#',
            'name' => 'Adunit Reports'
        );
        $userId = $this->Authake->getUserId();
        $conditions = array();
        $this->paginate = array();
        $this->paginate['limit'] = 10; // set here pagination limit
        //$conditions[] = array('tblOrder.isActive' => 1);
        if (!empty($user_id)) {
            $conditions[] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        //$this->request->data['daterange']=str_replace('_','/',$this->request->query['daterange']);
        if (!empty($this->request->data['company_id']) and ( !in_array('all', $this->request->data['company_id']))) {
            $conditions[] = array('tblAdunit.publisher_id' => $this->request->data['company_id']);
        }
        if (!empty($this->request->data['media_id']) and ( !in_array('all', $this->request->data['media_id']))) {
            $conditions[] = array('tbmt.mtype_id' => $this->request->data['media_id']);
        }
      
        if (!empty($this->request->data['product_id']) and ( !in_array('all', $this->request->data['product_id']))) {
            $conditions[] = array('tblAdunit.ad_ptype_id' => $this->request->data['product_id']);
        }
		$costid = 0;
		  if (!empty($this->request->data['cost_id']) and ( !in_array('all', $this->request->data['cost_id']))) {
            $conditions[] = array('tcs.cs_id' => $this->request->data['cost_id']);
			$costid = $this->request->data['cost_id'];
        }
        $range = array();
		$range2 = array();
        if (!empty($this->request->data['daterange']) and $this->request->data['daterange'] != 'null') {
            $daterange = explode('-', $this->request->data['daterange']);
            $start_date = $this->Commonfunction->edit_datepickerFormat($daterange[0]);
            $end_date = $this->Commonfunction->edit_datepickerFormat($daterange[1]);
            //$conditions[] = array('OR' =>
                //array('date(tblLineItem.li_start_date)  between ? and ?' => array($start_date, $end_date),
                  ///  'date(tblLineItem.li_end_date)  between ? and ?' => array($start_date, $end_date)
           // ));
			$date=date_create($start_date);
			$start_date = date_format($date,"Y-m-d H:i:s");
			
			$date=date_create($end_date);
			$end_date = date_format($date,"Y-m-d H:i:s");
			
            $range['start_date'] = $start_date;
            $range['end_date'] = $end_date;
        }

        $ids = $this->tblProfile->getAllConnectedProfileUserIds();
        $conditions[] = array(
            'tblAdunit.ad_parent_id' => 0,
            'tblAdunit.ad_isactive' => 1,
            'NOT' => array('tblAdunit.ad_dfp_id' => ''),
            'OR' => array(
                'tblAdunit.owner_user_id' => array($this->Authake->getUserId()),
                'AND' => array(
                    'tblAdunit.owner_user_id' => $ids,
                    'tblAdunit.ad_isPublicVisible' => 1))
        );
        $this->paginate['joins'] = array(
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id'
                )
            ),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID'
                )
            ),
            array('table' => 'tbl_product_types',
                'alias' => 'tbpt',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_ptype_id = tbpt.ptype_id'
                )
            ),
            array('table' => 'tbl_media_types',
                'alias' => 'tbmt',
                'type' => 'INNER',
                'conditions' => array(
                    'tbmt.mtype_id = tbpt.ptype_m_id'
                )
            ),
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tblMappedAdunit',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id=tblMappedAdunit.ma_ad_id'
                )
            ),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id=tblLineItem.li_id'
                )
            ),
            array('table' => 'tbl_orders',
                'alias' => 'tblOrder',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblOrder.dfp_order_id = tblLineItem.li_order_id'
                )
            ),
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblMappedAdunit.ma_uid'
                )
            ),
            array('table' => 'tbl_cost_structures',
                'alias' => 'tcs',
                'type' => 'LEFT',
                'conditions' => array(
                    'tcs.cs_id = tblLineItem.li_cs_id'
                )
            ),
            array('table' => 'tbl_lineitem_status',
                'alias' => 'tls',
                'type' => 'LEFT',
                'conditions' => array(
                    'tls.ls_id = tblLineItem.li_status_id'
                )
            )
        );
        $this->paginate['fields'] = array('tblAdunit.adunit_id', 'tblAdunit.adunit_name', 'tblAdunit.ad_dfp_id', 'tblAdunit.ad_created_at', 'tblAdunit.ad_uid', 'tblAdunit.ad_desc', 'tblAdunit.ad_list_size', 'tbpt.ptype_name', 'tbmt.mtype_name', 'tbmt.mtype_id', 'tblp.CompanyName','COUNT(DISTINCT tblLineItem.li_id) as li_id', 'COALESCE(tblLineItem.li_revenue,0) as li_revenue', 'COALESCE(tblLineItem.li_rate,0) as li_rate', 'COALESCE(tblLineItem.li_order_id,null) as li_order_id', 'COALESCE(tblLineItem.li_dfp_id,null) as li_dfp_id', 'tcs.cs_id', 'tcs.cs_name', 'tls.*', 'COALESCE(SUM(tblMailing.m_open),0) as opens', 'COALESCE(SUM(tblMailing.m_sent),0) as totalsent',
		'COALESCE(SUM(tblMailing.m_u_open),0) as u_opens',
		'COALESCE(SUM(tblMailing.m_clicks),0) as clicks',
		'COALESCE(SUM(tblMailing.m_u_clicks),0) as u_clicks', 'COALESCE(SUM(tblMailing.m_delivered),0) as delivered', 'COALESCE(SUM(tblMailing.m_bounce),0) as bounce', 'COALESCE(SUM(tblMailing.m_spam),0) as compain', 'COALESCE(SUM(tblMappedAdunit.ma_imp),0) as ma_imp');

        $this->paginate['order'] = array('tblAdunit.adunit_name' => 'ASC');
        $this->paginate['group'] = 'tblAdunit.adunit_id';
        $tblAdunits = $this->paginate("tblAdunit", $conditions);
		
	 if (!empty($this->request->data['daterange']) and $this->request->data['daterange'] != 'null') {
		$daterange = explode('-', $this->request->data['daterange']);
		$start_date = $this->Commonfunction->edit_datepickerFormat($daterange[0]);
		$end_date = $this->Commonfunction->edit_datepickerFormat($daterange[1]);
		$startTimeStamp = strtotime($start_date);
		$endTimeStamp = strtotime($end_date);
		$timeDiff = abs($endTimeStamp - $startTimeStamp);
		$numberDays = $timeDiff/86400;  // 86400 seconds in one day
		// and you might want to convert to integer
		$numberDays = intval($numberDays);
		$numberDays2 = $numberDays." days";
		//print_r("Days");
		//print_r($numberDays2);
		$date=date_create($start_date);
		$start_date = date_format($date,"Y-m-d H:i:s");
		
		date_sub($date,date_interval_create_from_date_string($numberDays2));
		$back_start_date = date_format($date,"Y-m-d H:i:s");
		$oneday = 1;
        $back_start_date = date('Y-m-d H:i:s', strtotime('-' . $oneday . ' days', strtotime($back_start_date)));
        $start_date2 = date('Y-m-d H:i:s', strtotime('-' . $oneday . ' days', strtotime($start_date)));
		$range2['start_date'] = $back_start_date;
        $range2['end_date'] = $start_date2;
		
        
        }
	 else{
		$chartdays2 = 30;
			$range2['end_date'] = date('Y-m-d H:i:s');
			$start_date = date('Y-m-d H:i:s', strtotime('-' . $chartdays2 . ' days'));
			
		$numberDays2 = "30 days";
		//print_r("Days");
		//print_r($numberDays2);
		$date=date_create($start_date);
		date_sub($date,date_interval_create_from_date_string($numberDays2));
		$back_start_date = date_format($date,"Y-m-d H:i:s");
		$range2['start_date'] = $back_start_date;
        $range2['end_date'] = $start_date;
		}
		
		
		
        if (!empty($tblAdunits)) {
            foreach ($tblAdunits as $key => $tblAdunit) {

                $tblAdunits[$key]['tblReportDay'] = $this->Reporting->tblReportByOrderAddunits($tblAdunit['tblAdunit']['ad_dfp_id'], $range, 7, $costid, $this->Authake->getUserId()); // here 7 used for 1 week show chart  
				
				$tblAdunits[$key]['tblReportDayLast'] = $this->Reporting->tblReportByOrderAddunits($tblAdunit['tblAdunit']['ad_dfp_id'], $range2, 7, $costid, $this->Authake->getUserId());
				
            }
        }
        $this->set('tblAdunits', $tblAdunits);
    }
	
	/* Get all orders for ad unit page*/
	
	function getAllOrdersList(){
	
	  $userId = $this->Authake->getUserId();
        $options['conditions'] = array('tblOrder.isActive' => 1);
        if (!empty($user_id)) {
            $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        $options['conditions']['NOT'] = array('tblOrder.dfp_order_id' => '');
        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
        )));
        $options['order'] = array('tblOrder.created_at' => 'DESC');
        $options['fields'] = array('tblOrder.dfp_order_id', 'tblOrder.order_name');
        $orderlist = $this->tblOrder->find('list', $options);
        $option['all'] = "All";
		 //print_r('<pre>');print_r($orderlist);print_r('</pre>');
        $this->set('orderlist', $option + $orderlist);
		
		
		}
	
	    function getAllLineItemwithstatusByOrder() {
        $this->autoRender = false;
        $order_id = array();
        if (!empty($this->request->data['order_id'])) {
            $order_id = explode(',', $this->request->data['order_id']);
        }

        $options = array();
        $options['joins'] = array(
            array('table' => 'tbl_cost_structures',
                'alias' => 'tcs',
                'type' => 'INNER',
                'conditions' => array(
                    'tcs.cs_id = tblLineItem.li_cs_id'
                )),
            array('table' => 'tbl_lineitem_status',
                'alias' => 'tls',
                'type' => 'INNER',
                'conditions' => array(
                    'tls.ls_id = tblLineItem.li_status_id'
                ))
        );

        if (!in_array('all', $order_id) and ! empty($order_id))
		$this->tblLineItem->virtualFields = array(
			"li_id_status" => "CONCAT_WS('-', tblLineItem.li_id, tblLineItem.li_status_id)"
			);
        $options['conditions'] = array('tblLineItem.li_order_id' => $order_id);
        $options['fields'] = array('li_id_status', 'tblLineItem.li_name');
        $tblLineItems = $this->tblLineItem->find("list", $options);
        $line_items[0]['id'] = "all";
        $line_items[0]['text'] = "All";

        if (!empty($tblLineItems)) {
            $count = 1;
            foreach ($tblLineItems as $key => $value) {
                $line_items[$count]['id'] = $key;
                $line_items[$count]['text'] = $value;
                $count++;
            }
        }
        return json_encode($line_items);
    }
	

    /*     * *
     * Change Lead Posting status 
     * @url : orders/view_flights/208
     *
     */

    function change_leadpost_status() {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->loadModel('Authake.tblFlight');
        if (!empty($_POST['flight_id'])) {
            $sql = "update tbl_mapped_adunits set lead_post_status=" . $_POST['status'] . " where ma_ad_id=" . $_POST['ad_dfp_id'] . " AND ma_l_id=" . $_POST['li_id'] . " AND ma_fl_id=" . $_POST['flight_id'];
            $this->tblFlight->query($sql);
            if ($_POST['status'] == 3) {
                $this->tblFlight->id = $_POST['flight_id'];
                $this->tblFlight->saveField('status', 1);
            }
            if ($_POST['status'] == 4) {
                $this->tblFlight->id = $_POST['flight_id'];
                $this->tblFlight->saveField('status', 2);
            }
        }
    }

    /**
     * Change Delivery Status
     * 0  = test mode, 1 = live
     */
    function change_flight_delivery_status() {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->loadModel('Authake.tblMappedAdunit');
        if (!empty($_POST['ma_id'])) {
            $this->tblMappedAdunit->primaryKey = 'ma_id';
            $this->tblMappedAdunit->id = $_POST['ma_id'];
            $this->tblMappedAdunit->saveField('is_lead_post', $_POST['status']);
        }
    }

    /**
     * Test Lead Posting
     */
    function lead_posting() {
        $this->autoLayout = false;
        $this->autoRender = false;
        Configure::write('debug', 2);
        $flight_id = $_POST['flight_id'];

        //$ipaddress = json_decode($_POST['ipconfig'], true);
        $formData = json_decode($_POST['fData'], true);
        $customData = json_decode($_POST['customData'], true);
        $deliveryObj = ClassRegistry::init('DeliverySetting');
        $delivery = $deliveryObj->find('first', array('conditions' => array('fl_id' => $flight_id)));

        foreach ($formData as $f) {
            if (!empty($f['id'])) {
                if ($f['id'] == 'email') {
                    $email = $f['value'];
                } elseif ($f['id'] == 'firstname') {
                    $firstname = $f['value'];
                } elseif ($f['id'] == 'lastname') {
                    $lastname = $f['value'];
                } elseif ($f['id'] == 'city') {
                    $city = $f['value'];
                } elseif ($f['id'] == 'state') {
                    $state = $f['value'];
                }
                $all_data[$f['id']] = $f['value'];
            }
        }
        foreach ($customData as $custom) {
            if (!empty($custom)) {
                foreach ($custom['fields'] as $d) {

                    $all_data[$d['id']] = $d['value'];
                }
                foreach ($custom['data'] as $c) {
                    $exp_c = explode('=', $c);
                    if ($exp_c[0] == 'order_id') {
                        $order_id = $exp_c[1];
                    } elseif ($exp_c[0] == 'od_lid') {
                        $od_lid = $exp_c[1];
                    } elseif ($exp_c[0] == 'od_cid') {
                        $od_cid = $exp_c[1];
                    } elseif ($exp_c[0] == 'od_ad_id') {
                        $od_ad_id = $exp_c[1];
                    } elseif ($exp_c[0] == 'od_map_id') {
                        $od_map_id = $exp_c[1];
                    } elseif ($exp_c[0] == 'ad_type') {
                        $ad_type = $exp_c[1];
                    }
                }

                $lineItem = $this->tblLineItem->find('first', array('conditions' => array('li_dfp_id' => $od_lid)));
                $lineItemFieldsObj = ClassRegistry::init('LineItemField');
                $lineItemFields = $lineItemFieldsObj->find('all', array('conditions' => array('fld_li_id' => $lineItem['tblLineItem']['li_id'])));

                $ipaddress = json_decode(str_replace(')', '', str_replace('(', '', file_get_contents('http://iplocation.siliconbiztech.com/?callback='))), true);
                $advertiser_fields = array();
                foreach ($lineItemFields as $fields) {
                    if (empty($fields['LineItemField']['custom'])) {
                        foreach ($all_data as $key => $data) {
                            if ($fields['LineItemField']['fld_name'] == $key) {
                                $advertiser_fields[$fields['LineItemField']['adv_fld_name']] = $data;
                            }
                            if ($fields['LineItemField']['fld_name'] == 'timestamp') {
                                $advertiser_fields[$fields['LineItemField']['adv_fld_name']] = time();
                            }

                            if ($fields['LineItemField']['fld_name'] == 'ip_address') {
                                $advertiser_fields[$fields['LineItemField']['adv_fld_name']] = $ipaddress['ip'];
                            }
                        }
                    } else {
                        foreach ($all_data as $key => $data) {
                            if ($fields['LineItemField']['adv_fld_name'] == $key) {
                                $advertiser_fields[$fields['LineItemField']['adv_fld_name']] = $data;
                            }
                        }
                    }
                }



                if ($delivery['DeliverySetting']['ds_delivery_mode'] == 4) {
                    $get_post = $delivery['DeliverySetting']['ds_http_get_post']; // 1 = get , 2 = post
                    $url = $delivery['DeliverySetting']['ds_http_url'];
                    $response_text = $delivery['DeliverySetting']['ds_http_response'];
                    if (!empty($url) && !empty($get_post)) {
                        if ($get_post == 1) {
                            $response = self::httpGet($url, $advertiser_fields);
                            $request = json_encode(array('url' => $response['url'], 'method' => 'GET'));
                        } elseif ($get_post == 2) {
                            $response = self::httpPost($delivery, $advertiser_fields);
                            $request = json_encode(array('url' => $response['url'], 'data' => $response['data'], 'method' => 'POST'));
                        }

                        //if ($response['text'] == $response_text) {
                        if ($_POST['status'] == 2) {
                            $sql = "update tbl_mapped_adunits set lead_post_status=" . $_POST['status'] . " where ma_ad_id=" . $_POST['ad_dfp_id'] . " AND ma_l_id=" . $_POST['li_id'] . " AND ma_fl_id=" . $_POST['flight_id'];
                            $lineItemFieldsObj->query($sql);
                        }
                        $data = array(
                            'od_lid' => $od_lid,
                            'od_cid' => $od_cid,
                            'od_ad_id' => $od_ad_id,
                            'od_map_id' => $od_map_id,
                            'od_email' => $email,
                            'od_fname' => $firstname,
                            'od_lname' => $lastname,
                            'od_site' => '', //$server['HTTP_ORIGIN'],
                            'od_create' => date('Y-m-d H:i:s'),
                            'od_isreturn' => 0,
                            'od_isTest' => 1,
                            'device_type' => '',
                            'od_phone' => '',
                            'od_source' => '',
                            'od_country_name' => $ipaddress['country_code'],
                            'fp_browser' => '', //$fp['fp_browser'],
                            'fp_connection' => '', //$fp['fp_connection'],
                            'fp_display' => '', //$fp['fp_display'],
                            'fp_flash' => '', //$fp['fp_flash'],
                            'fp_language' => '', //$fp['fp_language'],
                            'fp_os' => '', // $fp['fp_os'],
                            'fp_timezone' => '', //$fp['fp_browser'],
                            'fp_useragent' => '', //$fp['fp_useragent'],
                            'od_ipaddress' => $ipaddress['ip'],
                            'request' => $request,
                            'response' => $response['text'],
                            'data' => json_encode($all_data)
                        );
                        $order = ClassRegistry::init('OrderLeads');
                        $order->useDbConfig = 'DASOrderLeads';
                        $order->useTable = 'order_' . $order_id;
                        $order->save($data);
                    }
                    echo json_encode(array('request' => json_decode($request, true), 'response' => $response));
                    //}
                }
            }
        }
    }

    /**
     * Http GET Requet using curl
     * @param string $url
     * @param type $params
     * @return type
     */
    function httpGet($url, $params) {
        $getparams = '?';
        if (!empty($params)) {
            foreach ($params as $k => $v) {
                $getparams .= $k . '=' . $v . '&';
            }
        }
        $getparams=rtrim($getparams, "&");
        $url = $url . $getparams;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//  curl_setopt($ch,CURLOPT_HEADER, false); 
        $output = curl_exec($ch);
        curl_close($ch);
        return array('url' => $url, 'text' => $output);
    }

    /**
     * Http post Request using curl
     * @param type $url
     * @param type $params
     * @return type
     */
     function httpPost($delivery, $params) {
        $postData = '';
        //create name value pairs seperated by &
		$postData="{";
        foreach ($params as $k => $v) {
            $postData .= '"'.$k . '":"' . $v . '",';
        }
		$postData = rtrim($postData, ',');
		$postData.="}";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $delivery['DeliverySetting']['ds_http_url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $login = $delivery['DeliverySetting']['ds_login']; //'wallstreetsurvivor';
        $password = $delivery['DeliverySetting']['ds_password']; //'S57qY;J^;*|A02&b#o^yEM;wHvf=^>Pevf7E*u=b?97t2aQ.8iC{#80hk]Q}zmDb';
        if (!empty($login) && !empty($password)) {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
        }
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/json"
		));
        $output = curl_exec($ch);
        curl_close($ch);
        return array('url' => $delivery['DeliverySetting']['ds_http_url'], 'data' => $postData, 'text' => $output);
    }

    function preview_creative($mapped_ad_uid, $cr_id = '') {
        Configure::write('debug', 1);
        if(!empty($cr_id))
            $option['conditions'] = array('tblMappedAdunit.ma_uid' => $mapped_ad_uid,'Creative.cr_id'=> $cr_id);
        else
            $option['conditions'] = array('tblMappedAdunit.ma_uid' => $mapped_ad_uid);
        
        $option['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_line_items',
                'alias' => 'LineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id = LineItem.li_id'
                )),
            array('table' => 'tbl_orders',
                'alias' => 'Order',
                'type' => 'INNER',
                'conditions' => array(
                    'Order.dfp_order_id = LineItem.li_order_id'
                )),
           array('table' => 'tbl_flights',
                'alias' => 'Flight',
                'type' => 'INNER',
                'conditions' => array(
                    'Flight.fl_id = tblMappedAdunit.ma_fl_id'
                )),
            array('table' => 'tbl_flights_creatives',
                'alias' => 'FlightCreative',
                'type' => 'INNER',
                'conditions' => array(
                    'FlightCreative.fl_id = tblMappedAdunit.ma_fl_id'
                )),
            array('table' => 'tbl_creatives',
                'alias' => 'Creative',
                'type' => 'INNER',
                'conditions' => array(
                    'Creative.cr_id = FlightCreative.cr_id'
                )),
			array('table' => 'authake_users',
                'alias' => 'autUser',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = autUser.id')),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'autUser.tblProfileID = tblp.profileID')));
        //pr($option);
        $option['fields'] = array('tblAdunit.ad_uid','tblp.tracksite_id','Creative.cr_id','tblMappedAdunit.ma_uid','tblMappedAdunit.ma_ad_id',
            'LineItem.li_dfp_id','Order.dfp_order_id','Flight.fl_id',
            'Creative.cr_body','Creative.cr_cta','LineItem.li_id','LineItem.li_cs_id','Creative.cr_redirect_url');
        $mappedAdunitDetails = $this->tblMappedAdunit->find('first', $option);
		//$log=$this->tblMappedAdunit->getDataSource()->getLog(false, false);
        //debug($log);
        $mailings = $this->tblMappedAdunit->query('SELECT m_id FROM  tbl_mailings WHERE  m_ad_uid = "'.$mapped_ad_uid.'"');
       // pr($mailings);
       //pr($mappedAdunitDetails);
       $this->set('mailings',$mailings);
        $this->set('mappedAdunitDetails',$mappedAdunitDetails);

    }

}