<?php

App::uses('AppController', 'Controller');

class PagesController extends AppController {

    var $helpers = array('Form', 'Time', 'Html', 'Session', 'Js', 'Authake.Authake');
    var $components = array('Session', 'RequestHandler', 'Authake.Authake');

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Authake.tblAdunits', 'Authake.tblOrder', 'Authake.tblOrderDailySummary');

    public function display() {
        Configure::write('debug', 2);
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = $titleForLayout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $titleForLayout = Inflector::humanize($path[$count - 1]);
        }

        $groupId = $this->Authake->getGroupIds();
        if ($groupId[0] == "3") {
            $titleForLayout = "Advertiser Panel";
        }
        if ($groupId[0] == "4") {
            $titleForLayout = "Publisher Panel";
        }
        $this->set(array(
            'page' => $page,
            'subpage' => $subpage,
            'title_for_layout' => $titleForLayout
        ));

        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = tblp.user_id',
                    'tblAdunit.owner_user_id' => array($this->Authake->getUserId()))),
            array('table' => 'tbl_product_types',
                'alias' => 'tbpt',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_ptype_id = tbpt.ptype_id'
                )),
            array('table' => 'tbl_media_types',
                'alias' => 'tbmt',
                'type' => 'INNER',
                'conditions' => array(
                    'tbmt.mtype_id = tbpt.ptype_m_id',
                    'tbmt.mtype_id = 2'
        )));

        $options['fields'] = array('tblAdunit.*', 'tblp.CompanyName', 'tbpt.ptype_name', 'tbmt.mtype_name');

        $tblAdunits = $this->tblAdunit->find('all', $options);

        $numEmails = sizeof($tblAdunits);
        $isEmail = $numEmails > 0 ? "" : "hide";
        $this->set('isEmail', $isEmail);

        $optionNL['conditions'] = array(
            'tblAdunit.ad_ptype_id' => '13',
            'tblAdunit.owner_user_id' => $this->Authake->getUserId());
        $tblAdNLs = $this->tblAdunit->find('all', $optionNL);
        $numNL = sizeof($tblAdNLs);
        $isNL = $numNL > 0 ? "" : "hide";
        $this->set('isNL', $isNL);


        $userId = $this->Authake->getUserId();

        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
        )));

        $options['fields'] = array('tblOrder.*', 'tblp.CompanyName');

        $options['order'] = array(
            'tblOrder.created_at' => 'DESC');

        $options['fields'] = array('tblOrder.*', 'tblp.*');

        $orders = $this->tblOrder->find('all', $options);
        $grossrevenue = 0;
        $todaytotal = 0;
        $todayLeadGentotal = 0;
        $todayEmailtotal = 0;
        $todayDisplaytotal = 0;
        $todayNativetotal = 0;
        $todaySocialtotal = 0;
        $monthlydesktop = 0;
        $monthlyEmail = 0;
        $monthlyLeadGen = 0;
        $monthlySocial = 0;
        $monthlyMobile = 0;
        $monthlyCoreg = 0;
        $monthlyHostPost = 0;
        $monthlyLandingPage = 0;
        $monthlyDedicatedEmail = 0;
        $monthlyAutoResponder = 0;
        $monthlyNewsletters = 0;
        $orderIds = array();

        foreach ($orders as $order) {
            $grossrevenue += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_mtd_gross');
            $todaytotal += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_today_total');
            $todayLeadGentotal += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_today_leadgen');
            $todayEmailtotal += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_today_email');
            $todayDisplaytotal += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_today_desktop');
            $todayDisplaytotal += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_today_mobile');
            $todaySocialtotal += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_today_social');
            $monthlydesktop += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_desktop');
            $monthlyEmail += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_email');
            $monthlyLeadGen += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_leadgen');
            $monthlySocial += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_social');
            $monthlyMobile += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_mobile');
            $monthlyCoreg += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_leadgen_coreg');
            $monthlyHostPost += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_leadgen_hostpost');
            $monthlyLandingPage += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_leadgen_landingpage');
            $monthlyDedicatedEmail += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_email_dedicated');
            $monthlyAutoResponder += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_email_autoresponder');
            $monthlyNewsletters += Configure::read('Order.' . $order['tblOrder']['dfp_order_id'] . '_monthly_email_newsletter');
            $orderIds[] = $order['tblOrder']['dfp_order_id'];
        }

        $option_tods['order'] = array('tblOrderDailySummary.ods_date' => 'DESC');
        $option_tods['conditions'] = array(
            'tblOrderDailySummary.ods_order_id' => $orderIds);
        $OrderDailySummary = $this->tblOrderDailySummary->find('all', $option_tods);
        //print_r($orderIds);


        $dailyRevenueTrend = array();

        for ($i = 0; $i < 10; $i++) {
            $ods_daily_desktop = 0;
            $ods_daily_email = 0;
            $ods_daily_leadgen = 0;
            $ods_daily_social = 0;
            $ods_daily_total = 0;
            $ods_leadgen_total = 0;
            $ods_daily_leadgen_coreg = 0;
            $ods_daily_leadgen_hostpost = 0;
            $ods_daily_leadgen_landingpage = 0;
            $dailyRevenueTrend[$i]['date'] = date("d-M", strtotime($i . " days ago"));
            foreach ($OrderDailySummary as $OrderDaily) {
                //echo date("Y-m-d", strtotime($i." days ago")) . "<br />";
                if ($OrderDaily['tblOrderDailySummary']['ods_date'] == date("Y-m-d", strtotime($i . " days ago"))) {
                    //echo date("Y-m-d", strtotime($i." days ago")) . "<br />";
                    $ods_daily_desktop += $OrderDaily['tblOrderDailySummary']['ods_daily_desktop'];
                    $ods_daily_email += $OrderDaily['tblOrderDailySummary']['ods_daily_email'];
                    $ods_daily_leadgen += $OrderDaily['tblOrderDailySummary']['ods_daily_leadgen'];
                    $ods_daily_social += $OrderDaily['tblOrderDailySummary']['ods_daily_social'];
                    $ods_daily_total += $ods_daily_desktop + $ods_daily_email + $ods_daily_leadgen + $ods_daily_social;
                    $ods_daily_leadgen_coreg += $OrderDaily['tblOrderDailySummary']['ods_daily_leadgen_coreg'];
                    $ods_daily_leadgen_hostpost += $OrderDaily['tblOrderDailySummary']['ods_daily_leadgen_hostpost'];
                    $ods_daily_leadgen_landingpage += $OrderDaily['tblOrderDailySummary']['ods_daily_leadgen_landingpage'];
                    $ods_leadgen_total += $ods_daily_leadgen_coreg + $ods_daily_leadgen_hostpost + $ods_daily_leadgen_landingpage;
                }
            }
            $dailyRevenueTrend[$i]['ods_daily_desktop'] = $ods_daily_desktop;
            $dailyRevenueTrend[$i]['ods_daily_email'] = $ods_daily_email;
            $dailyRevenueTrend[$i]['ods_daily_leadgen'] = $ods_daily_leadgen;
            $dailyRevenueTrend[$i]['ods_daily_social'] = $ods_daily_social;
            $dailyRevenueTrend[$i]['ods_daily_total'] = $ods_daily_total;
            $dailyRevenueTrend[$i]['ods_leadgen_total'] = $ods_leadgen_total;
            $dailyRevenueTrend[$i]['ods_daily_leadgen_coreg'] = $ods_daily_leadgen_coreg;
            $dailyRevenueTrend[$i]['ods_daily_leadgen_hostpost'] = $ods_daily_leadgen_hostpost;
            $dailyRevenueTrend[$i]['ods_daily_leadgen_landingpage'] = $ods_daily_leadgen_landingpage;
        }



        //print_r($dailyRevenueTrend);

        $this->set('grossrevenue', $grossrevenue);
        $this->set('todaytotal', $todaytotal);
        $this->set('todayLeadGentotal', $todayLeadGentotal);
        $this->set('todayEmailtotal', $todayEmailtotal);
        $this->set('todayDisplaytotal', $todayDisplaytotal);
        $this->set('todaySocialtotal', $todaySocialtotal);
        $this->set('monthlydesktop', $monthlydesktop);
        $this->set('monthlyEmail', $monthlyEmail);
        $this->set('monthlyLeadGen', $monthlyLeadGen);
        $this->set('monthlySocial', $monthlySocial);
        $this->set('monthlyMobile', $monthlyMobile);
        $this->set('monthlyCoreg', $monthlyCoreg);
        $this->set('monthlyHostPost', $monthlyHostPost);
        $this->set('monthlyLandingPage', $monthlyLandingPage);
        $this->set('monthlyDedicatedEmail', $monthlyDedicatedEmail);
        $this->set('monthlyAutoResponder', $monthlyAutoResponder);
        $this->set('monthlyNewsletters', $monthlyNewsletters);
        $this->set('dailyRevenueTrend', $dailyRevenueTrend);



        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    function beforeFilter() {
        if (!$this->Authake->isLogged())
            $this->redirect('/login');
        if (isset($this->Configuration) && !empty($this->Configuration->table)) {
            $this->Configuration->load();
        }
    }

}
