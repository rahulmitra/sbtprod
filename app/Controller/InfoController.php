<?php

class InfoController extends AppController {

    var $uses = array();
    var $components = array('RequestHandler', 'Session', 'Coutch');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function index() {
        Configure::write('debug', 0);
        if (!empty($this->data)) {
            $user_info = $this->Coutch->get($this->data['Info']['email']);
            if (empty($user_info)) {
                $doc->_id = md5($this->data['Info']['email']);
                $doc->Type = "EmailProfile";
                $doc->Email = $this->data['Info']['email'];
                $data['campaign_id'] = 7;
                $data['publisher'] = 'Test Publisher';
                $data['md5email'] = md5($this->data['Info']['email']);
                $response = $this->Coutch->getFromFC($this->data['Info']['email'], $doc);
                $response = $this->Coutch->getFromKMA($data, $response);
                $this->Coutch->store($response);
                $user_info = $this->Coutch->get($this->data['Info']['email']);
                
            }
            $this->set('user_info', $user_info);
        }
        $this->set('title_for_layout', 'Users Information');
    }

}
