<?php
	
	class NewsletterCreateController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblAdunit','Authake.tblNlsubscribers','Dynamic','Authake.tblMailing','Authake.tblProfile','Authake.tblMappedAdunit', 'Authake.tblMessage','UserLevelEmailPerformance');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {		
			
		}
		
		public function wsm($mailing_id) {
			
			$this->layout = 'template';
			
			$m_ad_uid = "cb2c7254129911e6b91e123aa499f9d5";
			$siteName = "Daily Money Times";
			$siteURL  = "http://dailymoneytimes.com/";
			$physical_address = "<br>1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br>";
			
			$url = "http://quote.stockquotes.finance/digitaladvertising_wsm.php";
			
			
			$ma_l_id = $this->tblMappedAdunit->field(
			'ma_l_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduidDFP = $this->tblMappedAdunit->field(
			'ma_ad_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduid = $this->tblAdunit->field(
			'ad_uid',
			array('ad_dfp_id' => $aduidDFP)
			);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			$options2['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => 'text/html', 'tbc.cr_id' => 127);
			$options2['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options2['fields'] = array('tbc.*','tblMappedAdunit.*');
			$sponsoredCreative = $this->tblMappedAdunit->find('first', $options2);
			$sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
			$sponsoredBody = $sponsoredCreative['tbc']['cr_body'];
			$sponsoredRedirect = $sponsoredCreative['tbc']['cr_redirect_url'];
			$sponsoredClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%recipient_email%&redirect=".$sponsoredRedirect;
			$sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$options3['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => 'text/html', 'tbc.cr_id' => 128);
			$options3['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options3['fields'] = array('tbc.*','tblMappedAdunit.*');
			$sponsoredCreative1 = $this->tblMappedAdunit->find('first', $options3);
			$sponsoredTitle1 = $sponsoredCreative1['tbc']['cr_header'];
			$sponsoredBody1 = $sponsoredCreative1['tbc']['cr_body'];
			$sponsoredRedirect1 = $sponsoredCreative1['tbc']['cr_redirect_url'];
			$sponsoredClickURL1 = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative1['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative1['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%recipient_email%&redirect=".$sponsoredRedirect1;
			$sponsoredImpression1 = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative1['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative1['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			
			$this->set('sponsoredTitle',$sponsoredTitle);
			$this->set('sponsoredBody',$sponsoredBody);
			$this->set('sponsoredClickURL',$sponsoredClickURL);
			$this->set('sponsoredImpression',$sponsoredImpression);
			$this->set('sponsoredTitle1',$sponsoredTitle1);
			$this->set('sponsoredBody1',$sponsoredBody1);
			$this->set('sponsoredClickURL1',$sponsoredClickURL1);
			$this->set('sponsoredImpression1',$sponsoredImpression1);
			$this->set('physical_address',$physical_address);
			
			$result = file_get_contents($url);
			$content = (!empty($result))?json_decode($result,true):'';
			$this->set('content',$content);
			$this->set('title','Movers');
			
		}
		public function OTCNewsletter($mailing_id,$esp_id) {
			$this->layout = 'template';	
			$m_ad_uid = "6cc6c60dfd0b11e5b91e123aa499f9d5";
			
			$siteName = "OTCRockstar.com";
			$siteURL  = "http://www.otcrockstar.com/";
			$physical_address = "<br>1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br>";
			$email_tag = "";
			$unsubscribe_link = "";
			
			switch($esp_id)
			{
				case 6 :
				$email_tag = "%recipient_email%";
				$unsubscribe_link = 'To stop receiving these emails, you may <a href="%unsubscribe_url%" target="_blank">unsubscribe now</a>.';
				break;
				case 3 :
				$email_tag = "*|EMAIL|*";
				break;
				case 2 :
				$email_tag = "%%DeliveryAddress%%";
				$unsubscribe_link = 'To stop receiving these emails, you may <HsUnsubscribe>unsubscribe now</HsUnsubscribe>.';
				break;
				default :
				die();
				break;
			}
			
			$ma_l_id = $this->tblMappedAdunit->field(
			'ma_l_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduidDFP = $this->tblMappedAdunit->field(
			'ma_ad_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduid = $this->tblAdunit->field(
			'ad_uid',
			array('ad_dfp_id' => $aduidDFP)
			);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			$options1['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'ACTIVE', 'tbc.creatives_type' => 13);
			$options1['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options1['fields'] = array('tbc.*','tblMappedAdunit.*');
			$options1['order'] = 'rand()';
			$sponsoredCreatives = $this->tblMappedAdunit->find('all', $options1);
			
			if(count($sponsoredCreatives) < 2)
			{
				$this->autoRender = false;
				die();
			}
			$sponsoredCreative = $sponsoredCreatives[0];
			$sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
			$sponsoredBody = $sponsoredCreative['tbc']['cr_body'];
			$sponsoredRedirect = str_replace("%25%25EMAIL_ADDRESS%25%25",$email_tag,$sponsoredCreative['tbc']['cr_redirect_url']);
			$sponsoredClickCTA = $sponsoredCreative['tbc']['cr_cta'];
			$sponsoredClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&c_i=lead&email=".$email_tag."&redirect=".$sponsoredRedirect;
			$sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$sponsoredCreative1 = $sponsoredCreatives[1];
			$sponsoredTitle1 = $sponsoredCreative1['tbc']['cr_header'];
			$sponsoredBody1 = $sponsoredCreative1['tbc']['cr_body'];
			$sponsoredRedirect1 = str_replace("%25%25EMAIL_ADDRESS%25%25",$email_tag,$sponsoredCreative1['tbc']['cr_redirect_url']);
			$sponsoredClickCTA1 = $sponsoredCreative1['tbc']['cr_cta'];
			$sponsoredClickURL1 = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative1['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative1['tblMappedAdunit']['ma_uid']."&c_i=lead&email=".$email_tag."&redirect=".$sponsoredRedirect1;
			$sponsoredImpression1 = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative1['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative1['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			$nlpixel = '<img src="http://tracking.digitaladvertising.systems/piwik.php?idsite='.$tracksite_id.'&rec=1&c_n='.$ma_l_id.'&c_p='.$m_ad_uid .'-'.$mailing_id .'&send_image=0" width="1" height="1">';
			
			
			$nlpixel .= '<img src="http://digitaladvertising.systems/crons/triggerCamp/970/6/'.$email_tag.'" width="1" height="1">';
			
			
			
			$result = file_get_contents("http://quote.stockquotes.finance/digitaladvertising_dailyTopStocks.php");
			
			$content = (!empty($result))?json_decode($result,true):'';
			$this->set('sponsoredTitle',$sponsoredTitle);
			$this->set('sponsoredBody',$sponsoredBody);
			$this->set('sponsoredClickURL',$sponsoredClickURL);
			$this->set('sponsoredImpression',$sponsoredImpression);
			$this->set('sponsoredClickCTA',$sponsoredClickCTA);
			$this->set('sponsoredTitle1',$sponsoredTitle1);
			$this->set('sponsoredBody1',$sponsoredBody1);
			$this->set('sponsoredClickURL1',$sponsoredClickURL1);
			$this->set('sponsoredImpression1',$sponsoredImpression1);
			$this->set('sponsoredClickCTA1',$sponsoredClickCTA1);
			$this->set('physical_address',$physical_address);
			$this->set('unsubscribe_link',$unsubscribe_link);
			$this->set('content',$content);
			$this->set('nlpixel',$nlpixel);
			$this->set('esp_id',$esp_id);
			$this->set('title','ZURBemails');
		}
		
		public function dmtFeedTemplate($mailing_id,$esp_id) {
			$m_ad_uid = "3f2d62e3d17811e589213417ebe5d44c";
			$siteName = "Daily Money Times";
			$siteURL  = "http://dailymoneytimes.com/";
			$physical_address = "<br>1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br>";
			$email_tag = "";
			$unsubscribe_link = "";
			
			switch($esp_id)
			{
				case 6 :
				$email_tag = "%recipient_email%";
				$unsubscribe_link = 'To stop receiving these emails, you may <a href="%unsubscribe_url%" target="_blank">unsubscribe now</a>.';
				break;
				case 3 :
				$email_tag = "*|EMAIL|*";
				break;
				case 2 :
				$email_tag = "%%DeliveryAddress%%";
				$unsubscribe_link = 'To stop receiving these emails, you may <HsUnsubscribe>unsubscribe now</HsUnsubscribe>.';
				break;
				default :
				die();
				break;
			}
			
			$url = "http://dailymoneytimes.com/feed/";
			
			$ma_l_id = $this->tblMappedAdunit->field(
			'ma_l_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduidDFP = $this->tblMappedAdunit->field(
			'ma_ad_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduid = $this->tblAdunit->field(
			'ad_uid',
			array('ad_dfp_id' => $aduidDFP)
			);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			$options1['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'ACTIVE', 'tbc.creatives_type' => 13);
			$options1['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options1['fields'] = array('tbc.*','tblMappedAdunit.*');
			$options1['order'] = 'rand()';
			$sponsoredCreatives = $this->tblMappedAdunit->find('all', $options1);
			
			if(count($sponsoredCreatives) < 2)
			{
				$this->autoRender = false;
				die();
			}
			
			$sponsoredCreative = $sponsoredCreatives[0];
			$sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
			$sponsoredBody = $sponsoredCreative['tbc']['cr_body'];
			$sponsoredRedirect = str_replace("%25%25EMAIL_ADDRESS%25%25",$email_tag,$sponsoredCreative['tbc']['cr_redirect_url']);
			$sponsoredClickCTA = $sponsoredCreative['tbc']['cr_cta'];
			$sponsoredClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&c_i=lead&email=".$email_tag."&redirect=".$sponsoredRedirect;
			$sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$sponsoredCreative1 = $sponsoredCreatives[1];
			$sponsoredTitle1 = $sponsoredCreative1['tbc']['cr_header'];
			$sponsoredBody1 = $sponsoredCreative1['tbc']['cr_body'];
			$sponsoredRedirect1 = str_replace("%25%25EMAIL_ADDRESS%25%25",$email_tag,$sponsoredCreative1['tbc']['cr_redirect_url']);
			$sponsoredClickCTA1 = $sponsoredCreative1['tbc']['cr_cta'];
			$sponsoredClickURL1 = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative1['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative1['tblMappedAdunit']['ma_uid']."&c_i=lead&email=".$email_tag."&redirect=".$sponsoredRedirect1;
			$sponsoredImpression1 = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative1['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative1['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$rssfeed = $this->getFeed($url);
			$ulliItems = '';
			$fullhtml = '';
			$firstTitle = '';
			$i = 0;
			foreach($rssfeed as $entry) {
				if($i == 5)
				break;
				
				if($i == 0)
				$firstTitle = $entry['title'];
				
				$j = $i+1;
				$ulliItems .= '<a href="'.$entry['link'].'" target="_blank" style="color:#ffffff; text-decoration:underline; text-decoration:none;">'.$entry['title'].'</a>
				<div><img src="http://ads-demo.siliconbiztech.com/email_template/9/images/divider.png" style="display:block"></div>';
				
				
				if($i == 1) {
					$fullhtml .= '<tr>
					<td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:7px 7px 7px 10px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;">
					<div><b><span style="color:red;font-size:12px;">(SPONSORED POST)</span> <a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$sponsoredClickURL.'">'. $sponsoredTitle .'</a></b></div>
					<div>'. $sponsoredBody .' <a href="'.$sponsoredClickURL.'">('.$sponsoredClickCTA.')</a>.<img src="' . $sponsoredImpression . '"  width="1" height="1" /><br>
					<br>
					</div>
					</td>
					</tr>';
					
				}
				
				if($i == 4) {
					$fullhtml .= '<tr>
					<td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:7px 7px 7px 10px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;">
					<div><b><span style="color:red;font-size:12px;">(SPONSORED POST)</span> <a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$sponsoredClickURL1.'">'. $sponsoredTitle1 .'</a></b></div>
					<div>'. $sponsoredBody1 .' <a href="'.$sponsoredClickURL1.'">('.$sponsoredClickCTA1.')</a>".<img src="' . $sponsoredImpression1 . '"  width="1" height="1" /><br>
					<br>
					</div>
					</td>
					</tr>';
				}
				
				$fullhtml .= '<tr>
				<td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:7px 7px 7px 10px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;">
				<div><b><a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$entry['link'].'">'. $entry['title'] .'</a></b></div>
				<div>'. $entry['desc'] .' (<a target="_blank" href="'.$entry['link'].'">Read More...</a>)<br>
				<br>
				</div>
				</td>
				</tr>';
				
				
				$i++;
			}
			
			echo $htmlmessage = '<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Untitled Document</title>
			</head>
			<body>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td align="center" valign="top" bgcolor="#e5e3ce" style="background-color:#e5e3ce;"><br>
			<br>
			<table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
			<td align="center" valign="middle"><img src="http://ads-demo.siliconbiztech.com/email_template/9/images/top.jpg" width="730" height="27" style="display:block;"></td>
			</tr>
			<tr>
			<td align="center" valign="middle" bgcolor="#2c817c" style="background-color:#2c817c; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:20px;">
			<tr>
			<td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif;">
			<div style="font-size:48px; color:#f9e4ad;"><b>'. $siteName .'</b></div>
			<div style="font-size:18px; color:#ffffff;">Free daily newsletter that offers investment reports and trading strategies</div>
			
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
			<td align="center" valign="middle" height="20"></td>
			</tr>
			<tr>
			<td align="center" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td align="left" valign="top" bgcolor="#dda51c" style="background-color:#dda51c; padding:8px; font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#ffffff;"><b>Here are your updates for today...</b></td>
			</tr>
			'. $fullhtml . 	'
			</table>
			</td>
			</tr>
			</table>
			<br><br>
			<center>
			<p align="center" style="margin-top:0em;margin-bottom:0em;font-size:8px">
			<font face="Verdana, Arial, Helvetica, sans-serif"><span style="font-size:8pt;font-family:Arial">DISCLAIMER:<br>
			The content on this email is a combination of paid sponsorships and promoted articles. DailyMoneyTimes.com is not affiliated with nor does it endorse any trading system, newsletter or other similar service. DailyMoneyTimes.com does not guarantee or verify any performance claims made by such systems, newsletters or services. Trading involves a significant and substantial risk of loss and may not be suitable for everyone. You should only trade with money you can afford to lose. There is no guarantee that you will profit from your trading activity and it is possible that you may lose all of, or if trading on margin more than, your investment. Some of the results shown may be based on simulated performance. SIMULATED OR HYPOTHETICAL PERFORMANCE RESULTS HAVE CERTAIN INHERENT LIMITATIONS. UNLIKE THE RESULTS SHOWN IN AN ACTUAL PERFORMANCE RECORD, SUCH RESULTS DO NOT REPRESENT ACTUAL TRADING. ALSO, BECAUSE THE TRADES HAVE NOT ACTUALLY BEEN EXECUTED, THE RESULTS MY HAVE UNDER OR OVER-COMPENSATED FOR THE IMPACT, IF ANY, OF CERTAIN MARKET FACTORS, SUCH AS LACK OF LIQUIDITY. SIMULATED OR HYPOTHETICAL PROGRAMS IN GENERAL ARE ALSO SUBJECT TO THE FACT THAT THEY ARE DESIGNED WITH THE BENEFIT OF HINDSIGHT. NO REPRESENTATION IS BEING MADE THAT ANY ACCOUNT WILL OR IS LIKELY TO ACHIEVE PROFITS OR LOSSES SIMILAR TO THOSE SHOWN. Past performance is not necessarily indicative of future performance. This brief statement cannot disclose all the risks and other significant aspects of trading. You should carefully study trading and consider whether such activity is suitable for you in light of your circumstances and financial resources before you trade. 
			<br><br>
			This message is considered by regulation to be a commercial and advertising message. This is a permission-based message. You are receiving this email either because you opted-in to this subscription or because you have a prior existing relationship with DailyMoneyTimes.com or one of its subsidiaries, and previously provided your email address to us. This email fully complies with all laws and regulations. If you do not wish to receive this email, then we apologize for the inconvenience. You can immediately discontinue receiving this email by clicking on the un-subscribe or profile link and you will no longer receive this email. We will immediately redress any complaints you may have. If you have any questions, please send an email with your questions to newsletters@dailymoneytimes.com
			<br><br>
			DailyMoneyTimes.com does not recommend or promote any stock tickers, symbols or any such identifier.
			<br /><br />'.$physical_address.'</span></font></p>
			<br><br>'.$unsubscribe_link.'.
			</center>
			</td>
			</tr>
			</table>
			<br>
			<br></td>
			</tr>
			</table>
			<img src="http://tracking.digitaladvertising.systems/piwik.php?idsite='.$tracksite_id.'&rec=1&c_n='.$ma_l_id.'&c_p='.$m_ad_uid .'-'.$mailing_id .'&send_image=0" width="1" height="1">
			<img src="http://digitaladvertising.systems/crons/campTriggerPixel/908/'.$email_tag.'" width="1" height="1" style="display:none" alt="" />
			</body>
			</html>';
			
			$this->autoRender = false;
			
		}
		
		
		public function ffpFeedTemplate($mailing_id,$esp_id) {
			$m_ad_uid = "3d684b400cab11e6b91e123aa499f9d5";
			$siteName = "Financial Freedom Post";
			$siteURL  = "https://financialfreedompost.com/";
			$physical_address = "<br>1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br>";
			$email_tag = "";
			$unsubscribe_link = "";
			
			switch($esp_id)
			{
				case 6 :
				$email_tag = "%recipient_email%";
				$unsubscribe_link = 'To stop receiving these emails, you may <a href="%unsubscribe_url%" target="_blank">unsubscribe now</a>.';
				break;
				case 3 :
				$email_tag = "*|EMAIL|*";
				break;
				case 2 :
				$email_tag = "%%DeliveryAddress%%";
				$unsubscribe_link = 'To stop receiving these emails, you may <HsUnsubscribe>unsubscribe now</HsUnsubscribe>.';
				break;
				default :
				die();
				break;
			}
			
			
			$url = "http://dailymoneytimes.com/feed/";
			
			
			$ma_l_id = $this->tblMappedAdunit->field(
			'ma_l_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduidDFP = $this->tblMappedAdunit->field(
			'ma_ad_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduid = $this->tblAdunit->field(
			'ad_uid',
			array('ad_dfp_id' => $aduidDFP)
			);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			$options1['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'ACTIVE', 'tbc.creatives_type' => 13);
			$options1['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options1['fields'] = array('tbc.*','tblMappedAdunit.*');
			$options1['order'] = 'rand()';
			$sponsoredCreatives = $this->tblMappedAdunit->find('all', $options1);
			
			if(count($sponsoredCreatives) < 2)
			{
				$this->autoRender = false;
				die();
			}
			
			$sponsoredCreative = $sponsoredCreatives[0];
			$sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
			$sponsoredBody = $sponsoredCreative['tbc']['cr_body'];
			$sponsoredRedirect = str_replace("%25%25EMAIL_ADDRESS%25%25",$email_tag,$sponsoredCreative['tbc']['cr_redirect_url']);
			$sponsoredClickCTA = $sponsoredCreative['tbc']['cr_cta'];
			$sponsoredClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&c_i=lead&email=".$email_tag."&redirect=".$sponsoredRedirect;
			$sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$sponsoredCreative1 = $sponsoredCreatives[1];
			$sponsoredTitle1 = $sponsoredCreative1['tbc']['cr_header'];
			$sponsoredBody1 = $sponsoredCreative1['tbc']['cr_body'];
			$sponsoredRedirect1 = str_replace("%25%25EMAIL_ADDRESS%25%25",$email_tag,$sponsoredCreative1['tbc']['cr_redirect_url']);
			$sponsoredClickCTA1 = $sponsoredCreative1['tbc']['cr_cta'];
			$sponsoredClickURL1 = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative1['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative1['tblMappedAdunit']['ma_uid']."&c_i=lead&email=".$email_tag."&redirect=".$sponsoredRedirect1;
			$sponsoredImpression1 = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative1['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative1['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$rssfeed = $this->getFeed($url);
			$ulliItems = '';
			$fullhtml = '';
			$firstTitle = '';
			$i = 0;
			foreach($rssfeed as $entry) { 
				if($i == 5)
				break;
				
				if($i == 0)
				$firstTitle = $entry['title'];
				
				$j = $i+1;
				$ulliItems .= '<a href="'.$entry['link'].'" target="_blank" style="color:#ffffff; text-decoration:underline; text-decoration:none;">'.$entry['title'].'</a>
				<div><img src="http://ads-demo.siliconbiztech.com/email_template/9/images/divider.png" style="display:block"></div>';
				
				
				if($i == 1) {
					$fullhtml .= '<tr>
					<td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:7px 7px 7px 10px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;">
					<div><b><span style="color:red;font-size:12px;">(SPONSORED POST)</span> <a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$sponsoredClickURL.'">'. $sponsoredTitle .'</a></b></div>
					<div>'. $sponsoredBody .' <a href="'.$sponsoredClickURL.'">('.$sponsoredClickCTA.')</a>.<img src="' . $sponsoredImpression . '"  width="1" height="1" /><br>
					<br>
					</div>
					</td>
					</tr>';
					
				}
				if($i == 4) {
					$fullhtml .= '<tr>
					<td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:7px 7px 7px 10px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;">
					<div><b><span style="color:red;font-size:12px;">(SPONSORED POST)</span> <a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$sponsoredClickURL1.'">'. $sponsoredTitle1 .'</a></b></div>
					<div>'. $sponsoredBody1 .' <a href="'.$sponsoredClickURL1.'">('.$sponsoredClickCTA1.')</a>".<img src="' . $sponsoredImpression1 . '"  width="1" height="1" /><br>
					<br>
					</div>
					</td>
					</tr>';
					
				}
				$fullhtml .= '<tr>
				<td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:7px 7px 7px 10px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;">
				<div><b><a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$entry['link'].'">'. $entry['title'] .'</a></b></div>
				<div>'. $entry['desc'] .' (<a target="_blank" href="'.$entry['link'].'">Read More...</a>)<br>
				<br>
				</div>
				</td>
				</tr>';
				
				
				$i++;
			}
			
			echo $htmlmessage = '<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Untitled Document</title>
			</head>
			<body>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td align="center" valign="top" bgcolor="#e5e3ce" style="background-color:#e5e3ce;"><br>
			<br>
			<table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
			<td align="center" valign="middle"><img src="http://ads-demo.siliconbiztech.com/email_template/9/images/top.jpg" width="730" height="27" style="display:block;"></td>
			</tr>
			<tr>
			<td align="center" valign="middle" bgcolor="#2c817c" style="background-color:#2c817c; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:20px;">
			<tr>
			<td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif;">
			<div style="font-size:48px; color:#f9e4ad;"><b>'. $siteName .'</b></div>
			<div style="font-size:18px; color:#ffffff;">Free daily newsletter that offers investment reports and trading strategies</div>
			
			</td>
			</tr>
			</table>
			</td>
			</tr>
			<tr>
			<td align="center" valign="middle" height="20"></td>
			</tr>
			<tr>
			<td align="center" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			<td align="left" valign="top" bgcolor="#dda51c" style="background-color:#dda51c; padding:8px; font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#ffffff;"><b>Here are your updates for today...</b></td>
			</tr>
			'. $fullhtml . 	'
			</table>
			</td>
			</tr>
			</table>
			<br>
			<br>
			<center>
			<p align="center" style="margin-top:0em;margin-bottom:0em;font-size:8px">
			<font face="Verdana, Arial, Helvetica, sans-serif"><span style="font-size:8pt;font-family:Arial">DISCLAIMER:<br>
			The content on this email is a combination of paid sponsorships and promoted articles. FinancialFreedomPost.com is not affiliated with nor does it endorse any trading system, newsletter or other similar service. FinancialFreedomPost.com does not guarantee or verify any performance claims made by such systems, newsletters or services. Trading involves a significant and substantial risk of loss and may not be suitable for everyone. You should only trade with money you can afford to lose. There is no guarantee that you will profit from your trading activity and it is possible that you may lose all of, or if trading on margin more than, your investment. Some of the results shown may be based on simulated performance. SIMULATED OR HYPOTHETICAL PERFORMANCE RESULTS HAVE CERTAIN INHERENT LIMITATIONS. UNLIKE THE RESULTS SHOWN IN AN ACTUAL PERFORMANCE RECORD, SUCH RESULTS DO NOT REPRESENT ACTUAL TRADING. ALSO, BECAUSE THE TRADES HAVE NOT ACTUALLY BEEN EXECUTED, THE RESULTS MY HAVE UNDER OR OVER-COMPENSATED FOR THE IMPACT, IF ANY, OF CERTAIN MARKET FACTORS, SUCH AS LACK OF LIQUIDITY. SIMULATED OR HYPOTHETICAL PROGRAMS IN GENERAL ARE ALSO SUBJECT TO THE FACT THAT THEY ARE DESIGNED WITH THE BENEFIT OF HINDSIGHT. NO REPRESENTATION IS BEING MADE THAT ANY ACCOUNT WILL OR IS LIKELY TO ACHIEVE PROFITS OR LOSSES SIMILAR TO THOSE SHOWN. Past performance is not necessarily indicative of future performance. This brief statement cannot disclose all the risks and other significant aspects of trading. You should carefully study trading and consider whether such activity is suitable for you in light of your circumstances and financial resources before you trade. 
			<br><br>
			This message is considered by regulation to be a commercial and advertising message. This is a permission-based message. You are receiving this email either because you opted-in to this subscription or because you have a prior existing relationship with FinancialFreedomPost.com or one of its subsidiaries, and previously provided your email address to us. This email fully complies with all laws and regulations. If you do not wish to receive this email, then we apologize for the inconvenience. You can immediately discontinue receiving this email by clicking on the un-subscribe or profile link and you will no longer receive this email. We will immediately redress any complaints you may have. If you have any questions, please send an email with your questions to newsletters@FinancialFreedomPost.com
			<br><br>
			FinancialFreedomPost.com does not recommend or promote any stock tickers, symbols or any such identifier.<br><br>
			'.$physical_address.'</span></font></p>
			<br/><br>'.$unsubscribe_link.'
			</center>
			</td>
			</tr>
			</table>
			<br>
			<br>
			</td>
			</tr>
			</table>
			<img src="http://tracking.digitaladvertising.systems/piwik.php?idsite='.$tracksite_id.'&rec=1&c_n='.$ma_l_id.'&c_p='.$m_ad_uid .'-'.$mailing_id .'&send_image=0" width="1" height="1">
			<img src="http://digitaladvertising.systems/crons/campTriggerPixel/906/'.$email_tag.'" width="1" height="1" style="display:none" alt="" />
			</body>
			</html>';
			
			$this->autoRender = false;
			
		}
		
		
		
		public function politicsandmyportfolio($mailing_id,$esp_id)
		{
			$m_ad_uid = "cb302d98129911e6b91e123aa499f9d5";
			$siteName = "PoliticsAndMyPortfolio.com";
			$siteURL  = "http://www.politicsandmyportfolio.com/";
			$physical_address = "PoliticsAndMyPortfolio.com<br>118 LaPorte Lane<br>Mooresville, NC 28115";
			$email_tag = "";
			$unsubscribe_link = "";
			
			switch($esp_id)
			{
				case 6 :
				$email_tag = "%recipient_email%";
				$unsubscribe_link = 'To stop receiving these emails, you may <a href="%unsubscribe_url%" target="_blank">unsubscribe now</a>.';
				break;
				case 3 :
				$email_tag = "*|EMAIL|*";
				break;
				case 2 :
				$email_tag = "%%DeliveryAddress%%";
				$unsubscribe_link = 'To stop receiving these emails, you may <HsUnsubscribe>unsubscribe now</HsUnsubscribe>.';
				break;
				default :
				die();
				break;
			}
			
			
			$url = "http://www.politicsandmyportfolio.com/feed/";
			
			
			$ma_l_id = $this->tblMappedAdunit->field(
			'ma_l_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduidDFP = $this->tblMappedAdunit->field(
			'ma_ad_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduid = $this->tblAdunit->field(
			'ad_uid',
			array('ad_dfp_id' => $aduidDFP)
			);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			$options1['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'ACTIVE', 'tbc.creatives_type' => 13);
			$options1['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options1['fields'] = array('tbc.*','tblMappedAdunit.*');
			$options1['order'] = 'rand()';
			$sponsoredCreatives = $this->tblMappedAdunit->find('all', $options1);
			
			
			if(count($sponsoredCreatives) < 2)
			{
				$this->autoRender = false;
				die();
			}
			
			$sponsoredCreative = $sponsoredCreatives[0];
			$sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
			$sponsoredBody = $sponsoredCreative['tbc']['cr_body'];
			$sponsoredRedirect = str_replace("%25%25EMAIL_ADDRESS%25%25",$email_tag,$sponsoredCreative['tbc']['cr_redirect_url']);
			$sponsoredClickCTA = $sponsoredCreative['tbc']['cr_cta'];
			$sponsoredClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&c_i=lead&email=".$email_tag."&redirect=".$sponsoredRedirect;
			$sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$sponsoredCreative1 = $sponsoredCreatives[1];
			$sponsoredTitle1 = $sponsoredCreative1['tbc']['cr_header'];
			$sponsoredBody1 = $sponsoredCreative1['tbc']['cr_body'];
			$sponsoredRedirect1 = str_replace("%25%25EMAIL_ADDRESS%25%25",$email_tag,$sponsoredCreative1['tbc']['cr_redirect_url']);
			$sponsoredClickCTA1 = $sponsoredCreative1['tbc']['cr_cta'];
			$sponsoredClickURL1 = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative1['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative1['tblMappedAdunit']['ma_uid']."&c_i=lead&email=".$email_tag."&redirect=".$sponsoredRedirect1;
			$sponsoredImpression1 = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative1['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative1['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$rssfeed = $this->getFeed($url);
			$ulliItems = '';
			$fullhtml = '';
			$firstTitle = '';
			$i = 0;
			
			$fullhtml .= '<tr>
				<td align="left" valign="top" bgcolor="#EBEBEB" style="background-color:#EBEBEB; padding:7px 7px 7px 10px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;"><center>
				<div><b><span style="color:red;font-size:12px;">(SPONSORED POST)</span> <a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$sponsoredClickURL.'">'. $sponsoredTitle .'</a></b></div><div><p>'. $sponsoredBody .' <a href="'.$sponsoredClickURL.'">'.$sponsoredClickCTA.'</a></p><img src="' . $sponsoredImpression . '"  width="1" height="1" style="display:none;" /></div></center></td></tr>';
				
			foreach($rssfeed as $entry) { 
				if($i == 5)
				break;
				
				if($i == 0)
				$firstTitle = $entry['title'];
				
				$j = $i+1;
				$ulliItems .= '<a href="'.$entry['link'].'" target="_blank" style="color:#ffffff; text-decoration:underline; text-decoration:none;">'.$entry['title'].'</a>
				<div><img src="http://ads-demo.siliconbiztech.com/email_template/9/images/divider.png" style="display:block"></div>';
				
				
				
				
				if($i == 2) {
					$fullhtml .= '<tr>
					<td align="left" valign="top" bgcolor="#EBEBEB" style="background-color:#EBEBEB; padding:7px 7px 7px 10px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;"><center>
					<div><b><span style="color:red;font-size:12px;">(SPONSORED POST)</span> <a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$sponsoredClickURL1.'">'. $sponsoredTitle1 .'</a></b></div><div><p>'. $sponsoredBody1 .' <a href="'.$sponsoredClickURL1.'">'.$sponsoredClickCTA1.'</a></p><img src="' . $sponsoredImpression1 . '"  width="1" height="1" style="display:none;" /></div></center>
					</td>
					</tr>';
					
				}
				$fullhtml .= '<tr>
				<td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:7px 7px 7px 10px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;">
				<div><b><a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$entry['link'].'">'. $entry['title'] .'</a></b></div>
				<div>'. $entry['desc'] .' (<a target="_blank" href="'.$entry['link'].'">Read More...</a>)<br>
				<br>
				</div>
				</td>
				</tr>';
				
				
				$i++;
			}
			
			$fullhtml .= '<img src="http://tracking.digitaladvertising.systems/piwik.php?idsite='.$tracksite_id.'&rec=1&c_n='.$ma_l_id.'&c_p='.$m_ad_uid .'-'.$mailing_id .'&send_image=0" width="1" height="1">';
			
			$this->layout = 'template';
			$this->set('fullhtml',$fullhtml);
			$this->set('siteName',$siteName);
			$this->set('siteURL',$siteURL);
			$this->set('unsubscribe_link',$unsubscribe_link);
			$this->set('physical_address',$physical_address);
			$this->set('title',"Politics");
		}
		
		function getFeed($feed_url) {
			$rss = new DOMDocument();
			$rss->load($feed_url);
			$feed = array();
			foreach ($rss->getElementsByTagName('item') as $node) {
				$item = array ( 
				'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
				'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
				'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
				'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
				);
				array_push($feed, $item);
			}
			return $feed;
		}
	}
?>