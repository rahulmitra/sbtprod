<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class DeliveryController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User', 'Authake.tblMailing', 'Authake.tblReportDay','Authake.tblLineItem','Authake.tblAdunit','Authake.tblOrder','Dynamic', 'Authake.tblCreative', 'Authake.tblMappedAdunit');
		var $components = array('RequestHandler','Authake.Filter','Session','Commonfunction', 'Reporting');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {		
			$this->set('title_for_layout','All Delivery Orders');
			$userId = $this->Authake->getUserId();
			/*
				$options['joins'] = array(
				array('table' => 'tbl_profiles',
				'alias' => 'tblp',
				'type' => 'INNER',
				'conditions' => array(
				'tblOrder.advertiser_id = tblp.dfp_company_id')
				));
				
				$options['conditions'] = array(
			'tblp.user_id' => $userId ); */
			
			$options['joins'] = array(
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblOrder.advertiser_id = tblp.dfp_company_id',
			'tblOrder.advertiser_user_id' => array($userId)
			)));
			 
			$options['fields'] = array('tblOrder.*', 'tblp.CompanyName');
			
			$options['order'] = array(
			'tblOrder.created_at' => 'DESC' );
			
			$options['fields'] = array('tblOrder.*', 'tblp.*');
			
			$test = $this->tblOrder->find('all', $options);
			//print_r($test);
			$this->set('group', $test);
			
			$orders = '';
			$i = 1;
			foreach ($test as $user) {
				$orders .= ':orderId'.$i.',';
				$i++;
			}
			$orders = rtrim($orders, ',');
			
			if($orders)
			{
				App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
				App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
				App::import('Vendor', 'src/ExampleUtils');
				
				try {
					// Get DfpUser from credentials in "../auth.ini"
					// relative to the DfpUser.php file's directory.
					$user = new DfpUser();
					// Log SOAP XML request and response.
					$user->LogDefaults();
					// Get the OrderService.
					$orderService = $user->GetService('OrderService', 'v201608');
					// Create a statement to select all orders.
					$statementBuilder = new StatementBuilder();
					$statementBuilder->Where("id in ({$orders})");
					$count = 1;
					foreach ($test as $user) {
						$statementBuilder->WithBindVariableValue('orderId'.$count, "{$user['tblOrder']['dfp_order_id']}");
						$count++;
					}
					
					$statementBuilder->OrderBy('id DESC')
					->Limit(StatementBuilder::SUGGESTED_PAGE_LIMIT);
					
					// Default for total result set size.
					$totalResultSetSize = 0;
					do {
						// Get orders by statement.
						$page = $orderService->getOrdersByStatement(
						$statementBuilder->ToStatement());
						// Display results.
						if (isset($page->results)) {
							$results = (array)$page->results;
							$this->set('results',$results);
							$totalResultSetSize = $page->totalResultSetSize;
							$i = $page->startIndex;
						}
						$statementBuilder->IncreaseOffsetBy(StatementBuilder::SUGGESTED_PAGE_LIMIT);
					} while ($statementBuilder->GetOffset() < $totalResultSetSize);
					//printf("Number of results found: %d\n", $totalResultSetSize);
					} catch (OAuth2Exception $e) {
					ExampleUtils::CheckForOAuth2Errors($e);
					} catch (ValidationException $e) {
					ExampleUtils::CheckForOAuth2Errors($e);
					} catch (Exception $e) {
					printf("%s\n", $e->getMessage());
				}
				} else {
				
				$this->set('results','');
			}
			//print_r($d);
		}
		
		public function img() {		
			
			$nluid = $this->params['url']['nluid'];
			$trackid = $this->params['url']['trackid'];
			
			
			$imgpath = "../webroot/img/300x250_web_banner_example_4.jpg";
			// Get the mimetype for the file
			$finfo = finfo_open(FILEINFO_MIME_TYPE);  // return mime type ala mimetype extension
			$mime_type = finfo_file($finfo, $imgpath);
			finfo_close($finfo);
			
			switch ($mime_type){
				case "image/jpeg":
				// Set the content type header - in this case image/jpg
				header('Content-Type: image/jpeg');
				
				// Get image from file
				$img = imagecreatefromjpeg($imgpath);
				
				// Output the image
				imagejpeg($img);
				
				break;
				case "image/png":
				// Set the content type header - in this case image/png
				header('Content-Type: image/png');
				
				// Get image from file
				$img = imagecreatefrompng($imgpath);
				
				// integer representation of the color black (rgb: 0,0,0)
				$background = imagecolorallocate($img, 0, 0, 0);
				
				// removing the black from the placeholder
				imagecolortransparent($img, $background);
				
				// turning off alpha blending (to ensure alpha channel information 
				// is preserved, rather than removed (blending with the rest of the 
				// image in the form of black))
				imagealphablending($img, false);
				
				// turning on alpha channel information saving (to ensure the full range 
				// of transparency is preserved)
				imagesavealpha($img, true);
				
				// Output the image
				imagepng($img);
				
				break;
				case "image/gif":
				// Set the content type header - in this case image/gif
				header('Content-Type: image/gif');
				
				// Get image from file
				$img = imagecreatefromgif($imgpath);
				
				// integer representation of the color black (rgb: 0,0,0)
				$background = imagecolorallocate($img, 0, 0, 0);
				
				// removing the black from the placeholder
				imagecolortransparent($img, $background);
				
				// Output the image
				imagegif($img);
				
				break;
			}
			
			// Free up memory
			imagedestroy($img);
			$this->autoRender = false;
		}
		
		public function link() {		
			$isError = false;
			if($this->params['url']['aduid'])
			$this->request->data['tblNlsubscribers']['nl_adunit_uuid'] = $this->params['url']['aduid'];
			else
			$isError = true;
			if($this->params['url']['nl_email'])
			$this->request->data['tblNlsubscribers']['nl_email'] = $this->params['url']['nl_email'];
			else
			$isError = true;
			if(!$isError) {
				$this->tblNlsubscribers->create();
				$this->tblNlsubscribers->save($this->request->data);
			}
			$this->autoRender = false;
		}
		
		    // this function is used for view reports of publishers
    
	    function view_reports($pagedata = null) {
		Configure::write('debug',0);
        // add script and load model
        self::CommonScriptReport();
        $this->set('title_for_layout', 'View Reports');
        // order report breadcrums
        $this->breadcrumbs[] = array(
            'url' => Router::url('/'),
            'name' => 'Home'
        );
        $this->breadcrumbs[] = array(
            'url' => Router::url(array('controller' => 'delivery', 'action' => 'index')),
            'name' => 'Delivery'
        );

        $this->breadcrumbs[] = array(
            'url' => '#',
            'name' => 'View Reports'
        );
        // listing here 
        $userId = $this->Authake->getUserId();
        $conditions = array();
        $this->paginate = array();
        $this->paginate['limit'] = 10; // set here pagination limit
        $conditions[] = array('tblOrder.isActive' => 1);
		$conditions[] = array('tblAdunit.publisher_id' => array($userId));
		
        if (!empty($user_id)) {
            $conditions[] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        //$this->request->data['daterange']=str_replace('_','/',$this->request->query['daterange']);
        if (!empty($this->request->data['company_id']) and ( !in_array('all', $this->request->data['company_id']))) {
            $conditions[] = array('tblOrder.advertiser_user_id' => $this->request->data['company_id']);
        }
        if (!empty($this->request->data['order_id']) and ( !in_array('all', $this->request->data['order_id']))) {
            $conditions[] = array('tblOrder.dfp_order_id' => $this->request->data['order_id']);
        }
        if (!empty($this->request->data['status_id']) and ( !in_array('all', $this->request->data['status_id']))) {
            $conditions[] = array('tblLineItem.li_status_id' => $this->request->data['status_id']);
        }
        if (!empty($this->request->data['media_id']) and ( !in_array('all', $this->request->data['media_id']))) {
            $conditions[] = array('tblFlight.fl_ptype_id' => $this->request->data['media_id']);
        }
        $range = array();

        if (!empty($this->request->data['daterange']) and $this->request->data['daterange'] != 'null') {
            $daterange = explode('-', $this->request->data['daterange']);
            $start_date = $this->Commonfunction->edit_datepickerFormat($daterange[0]);
            $end_date = $this->Commonfunction->edit_datepickerFormat($daterange[1]);
            //$conditions[] = array('OR' =>
            //   array('date(tblLineItem.li_start_date)  between ? and ?' => array($start_date, $end_date),
            //        'date(tblLineItem.li_end_date)  between ? and ?' => array($start_date, $end_date)
            //));
           $date=date_create($start_date);
				$start_date = date_format($date,"Y-m-d H:i:s");
				$date=date_create($end_date);
				$end_date = date_format($date,"Y-m-d H:i:s");
            $range['start_date'] = $start_date;
            $range['end_date'] = $end_date;
        }

        $this->paginate['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tblMappedAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblFlight.fl_id=tblMappedAdunit.ma_fl_id'
                )),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id=tblMappedAdunit.ma_ad_id'
                )),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id=tblLineItem.li_id'
                )),
            array('table' => 'tbl_orders',
                'alias' => 'tblOrder',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.dfp_order_id = tblLineItem.li_order_id'
                )),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id'
                    
                )),
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblMappedAdunit.ma_uid'
                )),
            array('table' => 'tbl_cost_structures',
                'alias' => 'tcs',
                'type' => 'INNER',
                'conditions' => array(
                    'tcs.cs_id = tblLineItem.li_cs_id'
                )),
            array('table' => 'tbl_lineitem_status',
                'alias' => 'tls',
                'type' => 'INNER',
                'conditions' => array(
                    'tls.ls_id = tblLineItem.li_status_id'
                )),
        );
        $this->paginate['fields'] = array('tblp.CompanyName', 'tblFlight.fl_id', 'tblFlight.fl_name', 'tblFlight.fl_start', 'tblFlight.fl_end', 'tblFlight.status',
            'tblLineItem.*', 'tcs.cs_id', 'tcs.cs_name', 'tls.*', 'tblAdunit.adunit_name','tblAdunit.ad_dfp_id',
            'tblMappedAdunit.ma_uid', 'tblMappedAdunit.rev_share', 'COALESCE(SUM(tblMailing.m_open),0) as opens', 'COALESCE(SUM(tblMailing.m_sent),0) as totalsent',
            'COALESCE(SUM(tblMailing.m_clicks),0) as clicks', 'COALESCE(SUM(tblMailing.m_delivered),0) as delivered',
            'COALESCE(SUM(tblMailing.m_bounce),0) as bounce', 'COALESCE(SUM(tblMailing.m_spam),0) as compain',
            'COALESCE(SUM(tblMappedAdunit.ma_imp),0) as ma_imp');
        $this->paginate['order'] = array('ma_imp' => 'DESC');
        $this->paginate['group'] = 'tblFlight.fl_id';
        //pr($this->paginate);die;
        $flights = $this->paginate("tblFlight", $conditions);
        if (!empty($flights)) {
            $lineItemIDs = array();
            foreach ($flights as $key => $flight) {
                $lineItemIDs[] = $flight['tblLineItem']['li_id'];
            }
            $autoResObj = ClassRegistry::init('AutoResponder');
            $autoResponder = $autoResObj->find('all', array('conditions' => array('li_id' => $lineItemIDs), 'fields' => array('li_id', 'is_autoresp')));
            foreach ($flights as $key => $flight) {
                $flights[$key]['tblReportDay'] = $this->Reporting->view_report($flight['tblAdunit']['ad_dfp_id'],$flight['tblLineItem']['li_order_id'], $flight['tblLineItem']['li_dfp_id'], $flight['tblFlight']['fl_id'], $range, 30, $this->Authake->getUserId()); // here 30 used for 1 month show chart  
                foreach ($autoResponder as $i => $auto) {
                    if ($flight['tblLineItem']['li_id'] == $auto['AutoResponder']['li_id']) {
                        $flights[$key]['AutoResponder'] = $auto['AutoResponder'];
                    }
                }
            }
        }
       //print_r('<pre>');print_r($flights);print_r('</pre>');
        $this->set('flights', $flights);
    }
	
	 private function CommonScriptReport() {
        // load model
        $this->loadModel('Authake.tblFlight');
        // add Script 
        $this->System->add_js(array('/plugins/jquery-validation/js/jquery.validate.min.js', '/plugins/jquery-validation/js/additional-methods.min.js', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js', '/plugins/jquery-multi-select/js/jquery.multi-select.js', '/plugins/datatables/media/js/jquery.dataTables.min.js', '/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js', '/scripts/table-managed.js', '/plugins/justgage/justgage.js', '/plugins/justgage/raphael-2.1.4.min.js', '/plugins/icheck/icheck.min.js', '/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', '/plugins/quicksearch-master/jquery.quicksearch.js', '/scripts/form-validation.js', '/plugins/bootstrap-daterangepicker/moment.min.js', '/plugins/bootstrap-daterangepicker/daterangepicker.js'), 'foot');

        $this->loadModel('Authake.tblCostStructure');
        $this->loadModel('Authake.tblProfile');
        $this->loadModel('Authake.tblProducttype');
		$this->loadModel('Authake.tblMediaType');
        $this->loadModel('Authake.tblLineitemStatus');
		
		//publisher name
		
		 $ids = $this->tblProfile->getAllConnectedProfileUserIds();


        $options['conditions'][] = array(
            'tblAdunit.ad_parent_id' => 0,
            'tblAdunit.ad_isactive' => 1,
            'or' => array(
                'tblAdunit.owner_user_id' => array($this->Authake->getUserId()),
                'and' => array(
                    'tblAdunit.owner_user_id' => $ids,
                    'tblAdunit.ad_isPublicVisible' => 1))
        );

        $options['joins'] = array(
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id'
                )),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID'
                )),
            array('table' => 'tbl_product_types',
                'alias' => 'tbpt',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_ptype_id = tbpt.ptype_id'
                )),
            array('table' => 'tbl_media_types',
                'alias' => 'tbmt',
                'type' => 'INNER',
                'conditions' => array(
                    'tbmt.mtype_id = tbpt.ptype_m_id'
        )));
		$options['group'] = array('tblAdunit.publisher_id');
        $options['fields'] = array('tblAdunit.publisher_id', 'tblp.CompanyName');

        $publishers = $this->tblAdunit->find('list', $options);
		
		//foreach($publishers as $key => $pubname){
		//	$publisher[] = $pubname[$key]['CompanyName'];
			//}
		
		$this->set('publishers', $publishers);
	
        // header field

        $this->set('orderlist', $this->tblOrder->GetOrderList($this->request->data, $this->Authake->getUserId()));
        $companylist = $this->tblProfile->getListConnectedProfiles(); //('tblProfile.dfp_company_id')
        $companyaddbefore['all'] = 'All';
        $this->set('companylist', $companyaddbefore + $companylist);
        // get all line item
        $this->set('tblLineItems', $this->tblLineItem->getListLineItems());
        $this->set('status', $this->tblLineitemStatus->getlist());
        $this->set('medialist', $this->tblProducttype->getProductTypeList());
		$this->set('mediatypelist', $this->tblMediaType->getMediaTypeList());
		$this->set('producttype', $producttype = $this->tblProducttype->getProducttype());
		$costStructure = $this->tblCostStructure->getCostStructure();
        $this->set('costStructure', $costStructure);

        // for submit request 
        if ($this->request->is('post')) {
            if ($this->request->data['submit_value'] == 'submit') {
                $this->Session->write('ReportPost', $this->request->data); // write here post value in session  
            }
        }
        // delete session 
        if (empty($this->request->params['named']) and empty($this->request->data)) {
            $this->Session->delete('ReportPost');
        }
        // read here session value of post
        $ReportPost = $this->Session->read('ReportPost');
        if (!empty($ReportPost)) {
            $this->request->data['company_id'] = (!empty($ReportPost['company_id'])) ? $ReportPost['company_id'] : '';
            $this->request->data['order_id'] = (!empty($ReportPost['order_id'])) ? $ReportPost['order_id'] : '';
            $this->request->data['status_id'] = (!empty($ReportPost['status_id'])) ? $ReportPost['status_id'] : '';
            $this->request->data['media_id'] = (!empty($ReportPost['media_id'])) ? $ReportPost['media_id'] : '';
            $this->request->data['daterange'] = (!empty($ReportPost['daterange'])) ? str_replace('_', '/', $ReportPost['daterange']) : '';
        }
    }
		
		

}													