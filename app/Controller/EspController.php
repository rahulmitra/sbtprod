<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class EspController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblAdunit','Authake.tblNlsubscribers','Dynamic','Authake.tblMailing','Authake.tblProfile','Authake.tblMappedAdunit', 'Authake.tblMessage');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {		
			
		}
		
		
		public function SandpiperNLSocketLabs()
		{
			header('Content-Type: application/json');
			define('SOCKETLABS_URL', 'https://inject.socketlabs.com/api/v1/email');
			define('SOCKETLABS_SERVERID', '12888');
			define('SOCKETLABS_KEY', 'Dm35SjPk46Yqe2Q7GbNn');
			
			$mailing_id = 0;
			
			$m_ad_uid = $this->request->param('m_ad_uid');
			$url = $this->request->param('url');
			
			$mailing_id = $this->tblMailing->field(
			'm_id',
			array('m_ad_uid' => $m_ad_uid, 'cast(created_at as date)' => date('Y-m-d'))
			);
			
			if(!$mailing_id)
			{
				$this->request->data['tblMailing']['m_ad_uid'] = $m_ad_uid;
				$this->request->data['tblMailing']['m_sl'] = $firstTitle;
				$this->request->data['tblMailing']['m_sent_date'] = "'" . date('Y-m-d H:i:s') . "'";
				$this->request->data['tblMailing']['m_schedule_date'] = "'" . date('Y-m-d H:i:s') . "'";
				$this->tblMailing->create();
				if ($this->tblMailing->save($this->request->data))
				{
					$mailing_id = $this->tblMailing->getLastInsertID();
				}
			}
			
			$aduidDFP = $this->tblMappedAdunit->field(
			'ma_ad_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduid = $this->tblAdunit->field(
			'ad_uid',
			array('ad_dfp_id' => $aduidDFP)
			);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			$tblSubscribers = $this->Dynamic->Query("SELECT * FROM " . $table_name . " WHERE nl_isActive=1 and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , 490");
			
			
			$options['conditions'] = array('tblMappedAdunit.ma_ad_id' => '392927071', 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => '300x250');
			$options['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options['fields'] = array('tbc.*','tblMappedAdunit.*');
			$tblMapped300x250 = $this->tblMappedAdunit->find('first', $options);
			$Banner300X250Image = $tblMapped300x250['tbc']['cr_preview_url'];
			$Banner300X250Redirect = $tblMapped300x250['tbc']['cr_redirect_url'];
			$Banner300X250ClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped300x250['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped300x250['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%%DeliveryAddress%%&redirect=".$Banner300X250Redirect;
			$Banner300X250Impression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped300x250['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped300x250['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$options1['conditions'] = array('tblMappedAdunit.ma_ad_id' => '392927311', 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => '728x90');
			$options1['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options1['fields'] = array('tbc.*','tblMappedAdunit.*');
			$tblMapped728x90 = $this->tblMappedAdunit->find('first', $options1);
			$Banner728x90Image = $tblMapped728x90['tbc']['cr_preview_url'];
			$Banner728x90Redirect = $tblMapped728x90['tbc']['cr_redirect_url'];
			$Banner728x90ClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped728x90['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped728x90['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%%DeliveryAddress%%&redirect=".$Banner728x90Redirect;
			$Banner728x90Impression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped728x90['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped728x90['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$options2['conditions'] = array('tblMappedAdunit.ma_ad_id' => '400047271', 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => 'text/html');
			$options2['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options2['fields'] = array('tbc.*','tblMappedAdunit.*');
			$sponsoredCreative = $this->tblMappedAdunit->find('first', $options2);
			$sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
			$sponsoredBody = $sponsoredCreative['tbc']['cr_body'];
			$sponsoredRedirect = $sponsoredCreative['tbc']['cr_redirect_url'];
			$sponsoredClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%%DeliveryAddress%%&redirect=".$sponsoredRedirect;
			$sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			
			$rssfeed = $this->getFeed($url);
			$ulliItems = '';
			$fullhtml = '';
			$firstTitle = '';
			$i = 0;
			foreach($rssfeed as $entry) {
				if($i == 5)
				break;
				
				if($i == 0)
				$firstTitle = $entry->title;
				
				$ulliItems .= "<li><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
				
				
				if($i == 3) {
					$fullhtml .= "<table border='0' cellpadding='20' cellspacing='0' width='100%' mc:repeatable='siwc_600' mc:variant='content with right image'>
					<tr>
					<td valign='top'>
					<div mc:edit='riwc600_content00'>
					<a href='". $Banner728x90ClickURL . "'><img src='". $Banner728x90Image . "' /></a></div>
					</td>
					</tr>
					</table><hr />";
				}
				
				
				if($i == 1) {
					$fullhtml .= "  <table border='0' cellpadding='20' cellspacing='0' width='100%'>
					<tr><td valign='top' style='padding-top:0px;'>
					<div mc:edit='std_content00'>
					<span style='color:red;'>SPONSORED POST</span>
					<h2 class='h4'><a style='text-decoration:none;color:#3366A4;'  href='" . $sponsoredClickURL . "'>" . $sponsoredTitle . "</a></h2>" . $sponsoredBody . " (<a href='" . $sponsoredClickURL . "'>Click Here to Automatically Subscribe</a>)
					<img src='" . $sponsoredImpression . "'  width='1' height='1' />
					</div>
					</td>
					</tr>
					</table> <hr />";
					
				}
				
				$fullhtml .= "<table border='0' cellpadding='20' cellspacing='0' width='100%' mc:repeatable='siwc_600' mc:variant='content with right image'>
				<tr>
				<td valign='top'  style='padding-top:0px;'>
				<div mc:edit='riwc600_content00'>
				<h2 class='h4'><a style='text-decoration:none;color:#3366A4;'  href='$entry->link'>$entry->title</a></h2>$entry->description</div>
				</td>
				</tr>
				</table><hr />";
				$i++;
			}
			
			
			if(!empty($tblSubscribers) && !empty($rssfeed))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber[$table_name]['nl_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$myArray = array();
						$message_id = $this->tblMessage->getLastInsertID();
						$myArray[] = array('Field'=>'DeliveryAddress', 'Value'=>''.$tblSubscriber[$table_name]['nl_email'].'');
						$myArray[] = array('Field'=>'ExMessageId', 'Value'=> 'DA-'.$month_year.'-'.$message_id);
						$strArray[] = $myArray;
					}
				}
				//build merge data
				//echo $strSubscribers;
				$merge_data = new stdClass();
				$merge_data->PerMessage = $strArray;
				//%%MessageId%%
				// Create JSON object to POST to SocketLabs
				$data = new stdClass();
				$data->ServerId=SOCKETLABS_SERVERID;
				$data->ApiKey=SOCKETLABS_KEY;
				$data->Messages= array(array('MergeData'=>$merge_data,
				'Subject'=>"'".$firstTitle."'",
				'To'=>array(array('EmailAddress'=>'%%DeliveryAddress%%')),
				'From'=>array('EmailAddress'=>'politicsandmyportfolio@digitaladvertising.systems'),
				'HtmlBody'=>"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
				<html>
				<head>
				<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
				
				<!-- Facebook sharing information tags -->
				<meta property='og:title' content='Review: Ten Economic Questions for 2015' />
				
				<title>Review: Ten Economic Questions for 2015</title>
				<style type='text/css'>
				/* Client-specific Styles */
				#outlook a{padding:0;} /* Force Outlook to provide a 'view in browser' button. */
				body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
				body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */
				
				/* Reset Styles */
				body{margin:0; padding:0;}
				img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
				table td{border-collapse:collapse;}
				#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}
				
				/* Template Styles */
				
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COMMON PAGE ELEMENTS /\/\/\/\/\/\/\/\/\/\ */
				
				/**
				* @tab Page
				* @section background color
				* @tip Set the background color for your email. You may want to choose one that matches your company's branding.
				* @theme page
				*/
				body, #backgroundTable{
				/*@editable*/ background-color:#FAFAFA;
				}
				
				/**
				* @tab Page
				* @section email border
				* @tip Set the border for your email.
				*/
				#templateContainer{
				/*@editable*/ border: 1px solid #DDDDDD;
				}
				
				/**
				* @tab Page
				* @section heading 1
				* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
				* @style heading 1
				*/
				h1, .h1{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Page
				* @section heading 2
				* @tip Set the styling for all second-level headings in your emails.
				* @style heading 2
				*/
				h2, .h2{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:30px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Page
				* @section heading 3
				* @tip Set the styling for all third-level headings in your emails.
				* @style heading 3
				*/
				h3, .h3{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:26px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Page
				* @section heading 4
				* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
				* @style heading 4
				*/
				h4, .h4{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:22px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
				}
				
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: PREHEADER /\/\/\/\/\/\/\/\/\/\ */
				
				/**
				* @tab Header
				* @section preheader style
				* @tip Set the background color for your email's preheader area.
				* @theme page
				*/
				#templatePreheader{
				/*@editable*/ background-color:#FAFAFA;
				}
				
				/**
				* @tab Header
				* @section preheader text
				* @tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
				*/
				.preheaderContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:10px;
				/*@editable*/ line-height:100%;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Header
				* @section preheader link
				* @tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
				*/
				.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
				}
				
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: HEADER /\/\/\/\/\/\/\/\/\/\ */
				
				/**
				* @tab Header
				* @section header style
				* @tip Set the background color and border for your email's header area.
				* @theme header
				*/
				#templateHeader{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border-bottom:0;
				}
				
				/**
				* @tab Header
				* @section header text
				* @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
				*/
				.headerContent{
				/*@editable*/ color:#202020;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ padding:0;
				/*@editable*/ text-align:center;
				/*@editable*/ vertical-align:middle;
				}
				
				/**
				* @tab Header
				* @section header link
				* @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
				*/
				.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
				}
				
				#headerImage{
				height:auto;
				max-width:600px;
				}
				
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: MAIN BODY /\/\/\/\/\/\/\/\/\/\ */
				
				/**
				* @tab Body
				* @section body style
				* @tip Set the background color for your email's body area.
				*/
				#templateContainer, .bodyContent{
				/*@editable*/ background-color:#FFFFFF;
				}
				
				/**
				* @tab Body
				* @section body text
				* @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
				* @theme main
				*/
				.bodyContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Body
				* @section body link
				* @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
				*/
				.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
				}
				
				.bodyContent img{
				display:inline;
				height:auto;
				}
				
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: FOOTER /\/\/\/\/\/\/\/\/\/\ */
				
				/**
				* @tab Footer
				* @section footer style
				* @tip Set the background color and top border for your email's footer area.
				* @theme footer
				*/
				#templateFooter{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border-top:0;
				}
				
				/**
				* @tab Footer
				* @section footer text
				* @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
				* @theme footer
				*/
				.footerContent div{
				/*@editable*/ color:#707070;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:12px;
				/*@editable*/ line-height:125%;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Footer
				* @section footer link
				* @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
				*/
				.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
				}
				
				.footerContent img{
				display:inline;
				}
				
				/**
				* @tab Footer
				* @section social bar style
				* @tip Set the background color and border for your email's footer social bar.
				* @theme footer
				*/
				#social{
				/*@editable*/ background-color:#FAFAFA;
				/*@editable*/ border:0;
				}
				
				/**
				* @tab Footer
				* @section social bar style
				* @tip Set the background color and border for your email's footer social bar.
				*/
				#social div{
				/*@editable*/ text-align:center;
				}
				
				/**
				* @tab Footer
				* @section utility bar style
				* @tip Set the background color and border for your email's footer utility bar.
				* @theme footer
				*/
				#utility{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border:0;
				}
				
				/**
				* @tab Footer
				* @section utility bar style
				* @tip Set the background color and border for your email's footer utility bar.
				*/
				#utility div{
				/*@editable*/ text-align:center;
				}
				
				#monkeyRewards img{
				max-width:190px;
				}
				</style>
				</head>
				<body leftmargin='0' marginwidth='0' topmargin='0' marginheight='0' offset='0'>
				<center>
				<table border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='backgroundTable'>
            	<tr>
				<td align='center' valign='top'>
				<!-- // Begin Template Preheader \\ -->
				<table border='0' cellpadding='10' cellspacing='0' width='750' id='templatePreheader'>
				<tr>
				<td valign='top' class='preheaderContent'>
				
				<!-- // Begin Module: Standard Preheader \ -->
				<table border='0' cellpadding='10' cellspacing='0' width='100%'>
				<tr>
				<td valign='top'>
				<div mc:edit='std_preheader_content'>
				<img  width='200px' src='http://www.politicsandmyportfolio.com/wp-content/uploads/2015/09/politicsandmyportfolio-logo.png' />
				</div>
				</td>
				<!-- *|IFNOT:ARCHIVE_PAGE|* -->
				<td valign='top' width='190'>
				<div mc:edit='std_preheader_links'>
				
				</div>
				</td>
				<!-- *|END:IF|* -->
				</tr>
				</table>
				<!-- // End Module: Standard Preheader \ -->
				
				</td>
				</tr>
				</table>
				<!-- // End Template Preheader \\ -->
				
				</td>
				</tr>
				</table>
				
				<table border='0' cellpadding='0' cellspacing='0' width='750' id='templateContainer'  style='padding-top:10px'>
				
				<tr>
				<td align='center' valign='top'  style='padding-top:0px;'>
				<!-- // Begin Template Body \\ -->
				<table border='0' cellpadding='0' cellspacing='0' width='750' id='templateBody'>
				<tr>
				<td valign='top'  style='padding-top:0px;' class='bodyContent'>
				<table border='0' cellpadding='20' cellspacing='0' width='100%'>
				<tr>
				<td valign='top' style='padding-top:0px;'>
				<div mc:edit='std_content00'>
				<h2 class='h4'>Today's Top Stories</h2>
				<ul style='clear:both;padding:0 0 0 1.2em;width:100%'>" . $ulliItems
				. "
				</ul>
				
				</div>
				</td>
				<td  valign='top' style='padding-top:0px;'>
				<a href='". $Banner300X250ClickURL . "'><img src='". $Banner300X250Image . "' /></a>
				</td>
				</tr>
				</table>". $fullhtml . 	"</td>
				</tr>
				</table>
				<!-- // End Template Body \\ -->
				</td>
				</tr>
				<tr>
				<td align='center' valign='top'>
				<!-- // Begin Template Footer \\ -->
				<table border='0' cellpadding='10' cellspacing='0' width='750' id='templateFooter'>
				<tr>
				<td valign='top' class='footerContent'>
				<!-- // Begin Module: Standard Footer \\ -->
				<table border='0' cellpadding='10' cellspacing='0' width='100%'>
				<tr>
				<td style='text-align:left;font-family:Helvetica,Arial,Sans-Serif;font-size:11px;margin:0 6px 1.2em 0;color:#333'>You are subscribed to email updates from <a target='_blank' href='http://www.politicsandmyportfolio.com/'>Politics and Myportfolio</a>
				<br>To stop receiving these emails, you may <UNSUBSCRIBE><TEXT>unsubscribe now</UNSUBSCRIBE></TEXT></a>.</td>
				<td style='font-family:Helvetica,Arial,Sans-Serif;font-size:11px;margin:0 6px 1.2em 0;color:#333;text-align:right;vertical-align:top'>Powered by <a target='_blank' href='http://digitaladvertising.systems/'>DigitalAdvertising</a></td>
				</tr>
				<tr>
				<td style='text-align:left;font-family:Helvetica,Arial,Sans-Serif;font-size:11px;margin:0 6px 1.2em 0;color:#333' colspan='2'>Politics and Myportfolio, New York</td>
				</tr>
				</table>
				<!-- // End Module: Standard Footer \\ -->
				
				</td>
				</tr>
				</table>
				<!-- // End Template Footer \\ -->
				</td>
				</tr>
				</table>
				<br />
				</td>
				</tr>
				</table>
				<img src='http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=".$m_ad_uid."&c_p=DAS-MAILING-".$mailing_id."&send_image=0'  width='1' heigth='1'  />
				<img src='".$Banner300X250Impression."' width='1' heigth='1' />
				<img src='".$Banner728x90Impression."' width='1' heigth='1' />
				</center>
				</body></html>",
				'MailingId'=>'DASSOCEX-'.$mailing_id,
				'MessageId'=>'%%MessageId%%'
				)); 
				$bodyJson = json_encode($data);
				//echo $bodyJson;
				
				
				//use CURL to POST the JSON to SocketLabs
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,SOCKETLABS_URL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt($ch, CURLOPT_POST,           1 );
				curl_setopt($ch, CURLOPT_POSTFIELDS,     $bodyJson ); 
				curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json')); 
				$result=curl_exec ($ch);
				curl_close ($ch);
				
				//process result
				if ($result==false) {
					echo 'HTTP Error';
					} else {
					$result_obj = json_decode($result);
					if ($result_obj->ErrorCode=='Success') {
						$this->tblMailing->updateAll(
						array('tblMailing.m_esp_id' => "'DASSOCEX-".$mailing_id."'", 'tblMailing.m_sent_date' => "'" . date('Y-m-d H:i:s') . "'", 'tblMailing.m_schedule_date' =>"'" .  date('Y-m-d H:i:s') . "'" ),
						array('tblMailing.m_id' => $mailing_id));  
						echo 'Successfully sent all messages.';
						} elseif ($result_obj->ErrorCode=='Warning') {
						echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.'; 
						//not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
						} else {
						echo 'Failure Code: ' . $result_obj->ErrorCode;
					}
					print_r($result_obj);
				}
				print_r($merge_data);
				
			}
			
			$this->autoRender = false;
			
		}
		
		
		public function SandpiperNLSocketLabs1()
		{
			header('Content-Type: application/json');
			
			App::import('Vendor', 'src/html2text');
			
			$mailing_id = 0;
			
			$m_ad_uid = $this->request->param('m_ad_uid');
			$url = $this->request->param('url');
			$siteName = $this->request->param('siteName');
			$siteURL = $this->request->param('siteURL');
			$fromEmail = $this->request->param('fromEmail');
			$physical_address = $this->request->param('physical_address');
			$SOCKETLABS_SERVERID = $this->request->param('SOCKETLABS_SERVERID');
			$SOCKETLABS_KEY = $this->request->param('SOCKETLABS_KEY');
			define('SOCKETLABS_URL', 'https://inject.socketlabs.com/api/v1/email');
			define('SOCKETLABS_SERVERID', $SOCKETLABS_SERVERID);
			define('SOCKETLABS_KEY', $SOCKETLABS_KEY);
			
			$mailing_id = $this->tblMailing->field(
			'm_id',
			array('m_ad_uid' => $m_ad_uid, 'cast(created_at as date)' => date('Y-m-d'))
			);
			
			if(!$mailing_id)
			{
				$this->request->data['tblMailing']['m_ad_uid'] = $m_ad_uid;
				$this->request->data['tblMailing']['m_sl'] = '';
				$this->request->data['tblMailing']['m_sent_date'] = "'" . date('Y-m-d H:i:s') . "'";
				$this->request->data['tblMailing']['m_schedule_date'] = "'" . date('Y-m-d H:i:s') . "'";
				$this->tblMailing->create();
				if ($this->tblMailing->save($this->request->data))
				{
					$mailing_id = $this->tblMailing->getLastInsertID();
				}
			}
			
			$aduidDFP = $this->tblMappedAdunit->field(
			'ma_ad_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduid = $this->tblAdunit->field(
			'ad_uid',
			array('ad_dfp_id' => $aduidDFP)
			);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			$tblSubscribers = $this->Dynamic->Query("SELECT * FROM " . $table_name . " WHERE nl_isActive=1 and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , 1");
			
			
			$options['conditions'] = array('tblMappedAdunit.ma_ad_id' => '392927071', 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => '300x250');
			$options['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			))); 
			$options['fields'] = array('tbc.*','tblMappedAdunit.*');
			$tblMapped300x250 = $this->tblMappedAdunit->find('first', $options);
			$Banner300X250Image = $tblMapped300x250['tbc']['cr_preview_url'];
			$Banner300X250Redirect = $tblMapped300x250['tbc']['cr_redirect_url'];
			$Banner300X250ClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped300x250['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped300x250['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%%DeliveryAddress%%&redirect=".$Banner300X250Redirect;
			$Banner300X250Impression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped300x250['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped300x250['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$options1['conditions'] = array('tblMappedAdunit.ma_ad_id' => '392927311', 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => '728x90');
			$options1['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options1['fields'] = array('tbc.*','tblMappedAdunit.*');
			$tblMapped728x90 = $this->tblMappedAdunit->find('first', $options1);
			$Banner728x90Image = $tblMapped728x90['tbc']['cr_preview_url'];
			$Banner728x90Redirect = $tblMapped728x90['tbc']['cr_redirect_url'];
			$Banner728x90ClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped728x90['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped728x90['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%%DeliveryAddress%%&redirect=".$Banner728x90Redirect;
			$Banner728x90Impression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped728x90['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped728x90['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			$options2['conditions'] = array('tblMappedAdunit.ma_ad_id' => '400047271', 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => 'text/html');
			$options2['joins'] = array(
			array('table' => 'tbl_line_items',
			'alias' => 'tli',
			'type' => 'INNER',
			'conditions' => array(
			'tli.li_id = tblMappedAdunit.ma_l_id')),
			array('table' => 'tbl_creatives',
			'alias' => 'tbc', 
			'type' => 'INNER',
			'conditions' => array(
			'tbc.cr_lid = tli.li_dfp_id'
			)));
			$options2['fields'] = array('tbc.*','tblMappedAdunit.*');
			$sponsoredCreative = $this->tblMappedAdunit->find('first', $options2);
			$sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
			$sponsoredBody = $sponsoredCreative['tbc']['cr_body'];
			$sponsoredRedirect = $sponsoredCreative['tbc']['cr_redirect_url'];
			$sponsoredClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%%DeliveryAddress%%&redirect=".$sponsoredRedirect;
			$sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&send_image=0";
			
			
			
			$rssfeed = $this->getFeed($url);
			$ulliItems = '';
			$fullhtml = '';
			$firstTitle = '';
			$i = 0;
			foreach($rssfeed as $entry) {
				if($i == 5)
				break;
				
				if($i == 0)
				$firstTitle = $entry->title;
				
				
				//if($i == 2)
				//$ulliItems .= '<li><span style="color:red;font-size:11px">(Sponsored) </span> <a target="_blank" href="http://clicks.investingchannel.com/aff_clicktrack.aspx?q1=7&amp;q2=459&amp;q3=2117&amp;q4=1240&amp;q5=0&amp;q7=8&amp;trackid=sunny.gulati@siliconbiztech.com">Free: Zacks Rank #1 Bull Stock of the Day</a></li>';
				
				
				//$ulliItems .= "<li><a href='$entry->link' title='$entry->title'>$entry->title</a></li>";
				$j = $i+1;
				$ulliItems .= '<a href="'.$entry->link.'" style="color:#ffffff; text-decoration:underline; text-decoration:none;">'.$entry->title.'</a>
				<div><img src="http://ads-demo.siliconbiztech.com/email_template/9/images/divider.png" style="display:block"></div>';
				
				
				if($i == 1) {
					$fullhtml .= '<tr>
					<td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:7px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;">
					<div><b><span style="color:red;font-size:12px;">(SPONSORED POST)</span> <a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$sponsoredClickURL.'">'. $sponsoredTitle .'</a></b></div>
					<div>'. $sponsoredBody .' (<a href="'.$sponsoredClickURL.'">Click to get this free report now</a>) <img src="' . $sponsoredImpression . '"  width="1" height="1" /><br>
					<br>
					</div>
					</td>
					</tr>';
					
				}
				$fullhtml .= '<tr>
                <td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:7px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#444444;">
				<div><b><a  style="font-size:16px; color:#589985; text-decoration:none;" target="_blank" href="'.$entry->link.'">'. $entry->title .'</a></b></div>
                <div>'. $entry->description .' (<a href="'.$entry->link.'">Read More...</a>)<br>
				<br>
                </div>
                </td>
				</tr>';
				
				
				$i++;
			}
			
			
			if(!empty($tblSubscribers) && !empty($rssfeed))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber[$table_name]['nl_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$myArray = array();
						$message_id = $this->tblMessage->getLastInsertID();
						$myArray[] = array('Field'=>'DeliveryAddress', 'Value'=>''.$tblSubscriber[$table_name]['nl_email'].'');
						$myArray[] = array('Field'=>'ExMessageId', 'Value'=> 'DA-'.$month_year.'-'.$message_id);
						$strArray[] = $myArray;
					}
				}
				//build merge data
				//echo $strSubscribers;
				
				
				$htmlmessage = '<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Untitled Document</title>
				</head>
				<body>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td align="center" valign="top" bgcolor="#e5e3ce" style="background-color:#e5e3ce;"><br>
				<br>
				<table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
				<td align="center" valign="middle"><img src="http://ads-demo.siliconbiztech.com/email_template/9/images/top.jpg" width="600" height="27" style="display:block;"></td>
				</tr>
				<tr>
				<td align="center" valign="middle" bgcolor="#2c817c" style="background-color:#2c817c; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:20px;">
				<tr>
				<td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif;">
				<div style="font-size:48px; color:#f9e4ad;"><b>'. $siteName .'</b></div>
				<div style="font-size:18px; color:#ffffff;">Free daily newsletter that offers investment reports and trading strategies</div>
				
				</td>
				</tr>
				</table>
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-bottom:15px;">
				<tr>
				<td width="270" align="left" valign="middle"><table width="245" border="0" cellspacing="0" cellpadding="4">
				<tr>
                <td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#ffffff;"><b>Today\'s Top Stories</b></td>
				</tr>
				<tr>
                <td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#ffffff;">
				' . $ulliItems . '
				</td>
				</tr>
				</table></td>
				<td align="right" valign="top" style="font-family:Arial, Helvetica, sans-serif; color:#abcdcb; font-size:16px; padding:5px;"><a href="'. $Banner300X250ClickURL . '"><img src="'. $Banner300X250Image . '" width="250px;" alt="RJO" class="CToWUd"></a></td>              
				</tr>
				</table></td>
				</tr>
				<tr>
				<td height="15" align="center" valign="middle"><img src="http://ads-demo.siliconbiztech.com/email_template/9/images/blank.png" width="20" height="20" style="display:block;"></td>
				</tr>
				<tr>
				<td align="center" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td width="176" align="left" valign="top"><table width="176" border="0" cellspacing="0" cellpadding="0">
				<tr>
                <td align="left" valign="top" bgcolor="#47657d" style="background-color:#47657d; padding:8px; font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#ffffff;"><b>CONTACT US</b></td>
				</tr>
				<tr>
                <td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:8px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#47657d;"><a target="_blank" href="'. $siteURL . 	'">'. $siteName . 	'</a>'. $physical_address .'
				<br></td>
				</tr>
				</table>
				<table width="176" border="0" cellspacing="0" cellpadding="0">
                <tr>
				<td align="left" valign="top" bgcolor="#708d23" style="background-color:#708d23; padding:8px; font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#ffffff;"><b>Our Sponsors »</b></td>
                </tr>
                <tr>
				<td align="left" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; padding:8px; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#757575;"><span style="font-size:16px;color:#589985;text-decoration:none"><strong>Has This Economist Lost His Mind?</strong></span><br>
				The economist who recently told CNBC contributor Ron Insana: “You and Robert Malthus don’t know what you’re talking about!”... now predicts gold will plummet to $750… real estate will drop another 30%... and the DOW will plunge to 6,000.
				<br />
				So has Harry Dent lost his mind? When you see his convincing research, I think you’ll see he is simply stating the facts...<strong><br><u><a href="http://pro1.dentresearch.com/451587/"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:blue">Full details here.</span></a></u></strong>
				<br>
				
				<br></td>
                </tr>
				</table>
				</td>
				<td width="20" align="left" valign="top"><img src="http://ads-demo.siliconbiztech.com/email_template/9/images/blank.png" width="20" height="14" style="display:block;"></td>
				<td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
                <td align="left" valign="top" bgcolor="#dda51c" style="background-color:#dda51c; padding:8px; font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#ffffff;"><b>Here are your updates for today...</b></td>
				</tr>
				'. $fullhtml . 	'
				</table>
				</td>
				</tr>
				</table></td>
				</tr>
				</table>
				<br>
				<br></td>
				</tr>
				</table>
				<center>
				<p align="center" style="margin-top:0em;margin-bottom:0em;font-size:8px">
				<font face="Verdana, Arial, Helvetica, sans-serif"><span style="font-size:8pt;font-family:Arial">DISCLAIMER:<br>
				The content on this email is a combination of paid sponsorships and promoted articles. DailyMoneyTimes.com is not affiliated with nor does it endorse any trading system, newsletter or other similar service. DailyMoneyTimes.com does not guarantee or verify any performance claims made by such systems, newsletters or services. Trading involves a significant and substantial risk of loss and may not be suitable for everyone. You should only trade with money you can afford to lose. There is no guarantee that you will profit from your trading activity and it is possible that you may lose all of, or if trading on margin more than, your investment. Some of the results shown may be based on simulated performance. SIMULATED OR HYPOTHETICAL PERFORMANCE RESULTS HAVE CERTAIN INHERENT LIMITATIONS. UNLIKE THE RESULTS SHOWN IN AN ACTUAL PERFORMANCE RECORD, SUCH RESULTS DO NOT REPRESENT ACTUAL TRADING. ALSO, BECAUSE THE TRADES HAVE NOT ACTUALLY BEEN EXECUTED, THE RESULTS MY HAVE UNDER OR OVER-COMPENSATED FOR THE IMPACT, IF ANY, OF CERTAIN MARKET FACTORS, SUCH AS LACK OF LIQUIDITY. SIMULATED OR HYPOTHETICAL PROGRAMS IN GENERAL ARE ALSO SUBJECT TO THE FACT THAT THEY ARE DESIGNED WITH THE BENEFIT OF HINDSIGHT. NO REPRESENTATION IS BEING MADE THAT ANY ACCOUNT WILL OR IS LIKELY TO ACHIEVE PROFITS OR LOSSES SIMILAR TO THOSE SHOWN. Past performance is not necessarily indicative of future performance. This brief statement cannot disclose all the risks and other significant aspects of trading. You should carefully study trading and consider whether such activity is suitable for you in light of your circumstances and financial resources before you trade. 
				<br><br>
				This message is considered by regulation to be a commercial and advertising message. This is a permission-based message. You are receiving this email either because you opted-in to this subscription or because you have a prior existing relationship with DailyMoneyTimes.com or one of its subsidiaries, and previously provided your email address to us. This email fully complies with all laws and regulations. If you do not wish to receive this email, then we apologize for the inconvenience. You can immediately discontinue receiving this email by clicking on the unsubscribe or profile link and you will no longer receive this email. We will immediately redress any complaints you may have. If you have any questions, please send an email with your questions to newsletters@dailymoneytimes.com
				<br><br>
				DailyMoneyTimes.com does not recommend or promote any stock tickers, symbols or any such identifier.
				</span></font></p>
				<br>To stop receiving these emails, you may <HsUnsubscribe>unsubscribe now</HsUnsubscribe>.
				</center>
				<img src="http://tracking.digitaladvertising.systems/piwik.php?idsite=301&rec=1&c_n=118&c_p='.$m_ad_uid .'-'.$mailing_id .'&send_image=0" width="1" height="1">
				</body>
				</html>';
				$h2t =& new html2text($htmlmessage);
				$textVersion = $h2t->get_text();
				
				$merge_data = new stdClass();
				$merge_data->PerMessage = $strArray;
				//%%MessageId%%
				// Create JSON object to POST to SocketLabs
				$data = new stdClass();
				$data->ServerId=SOCKETLABS_SERVERID;
				$data->ApiKey=SOCKETLABS_KEY;
				$data->Messages= array(array('MergeData'=>$merge_data,
				'Subject'=> "$firstTitle",
				'To'=>array(array('EmailAddress'=>'%%DeliveryAddress%%')),
				'From'=>array('EmailAddress'=>$fromEmail, 'FriendlyName' => $siteName),
				'HtmlBody'=>"$htmlmessage",
				'TextBody'=>"$textVersion",
				'MailingId'=>'DASSOCEX-'.$mailing_id,
				'MessageId'=>'%%MessageId%%'
				)); 
				$bodyJson = json_encode($data);
				//echo $bodyJson;
				
				
				//use CURL to POST the JSON to SocketLabs
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,SOCKETLABS_URL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt($ch, CURLOPT_POST,           1 );
				curl_setopt($ch, CURLOPT_POSTFIELDS,     $bodyJson ); 
				curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json')); 
				$result=curl_exec ($ch);
				curl_close ($ch);
				
				//process result
				if ($result==false) {
					echo 'HTTP Error';
					} else {
					$result_obj = json_decode($result);
					if ($result_obj->ErrorCode=='Success') {
						$this->tblMailing->updateAll(
						array('tblMailing.m_esp_id' => "'DASSOCEX-".$mailing_id."'", 'tblMailing.m_sent_date' => "'" . date('Y-m-d H:i:s') . "'", 'tblMailing.m_schedule_date' =>"'" .  date('Y-m-d H:i:s') . "'",
						'tblMailing.m_sl' => "'".$firstTitle."'"),
						array('tblMailing.m_id' => $mailing_id));  
						echo 'Successfully sent all messages.';
						} elseif ($result_obj->ErrorCode=='Warning') {
						echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.'; 
						//not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
						} else {
						echo 'Failure Code: ' . $result_obj->ErrorCode;
					}
					print_r($result_obj);
				}
				print_r($merge_data);
				
			}
			
			$this->autoRender = false;
			
		}
		
		public function DedicatedMailSocketLabs()
		{
			header('Content-Type: application/json');
			
			App::import('Vendor', 'src/html2text');
			
			$mailing_id = 0;
			
			$m_ad_uid = $this->request->param('m_ad_uid');
			$url = $this->request->param('url');
			$siteName = $this->request->param('siteName');
			$siteURL = $this->request->param('siteURL');
			$fromEmail = $this->request->param('fromEmail');
			$physical_address = $this->request->param('physical_address');
			$SubjectLine = $this->request->param('SubjectLine');
			$SOCKETLABS_SERVERID = $this->request->param('SOCKETLABS_SERVERID');
			$SOCKETLABS_KEY = $this->request->param('SOCKETLABS_KEY');
			define('SOCKETLABS_URL', 'https://inject.socketlabs.com/api/v1/email');
			define('SOCKETLABS_SERVERID', $SOCKETLABS_SERVERID);
			define('SOCKETLABS_KEY', $SOCKETLABS_KEY);
			
			$mailing_id = $this->tblMailing->field(
			'm_id',
			array('m_ad_uid' => $m_ad_uid)
			);
			
			if(!$mailing_id)
			{
				$this->request->data['tblMailing']['m_ad_uid'] = $m_ad_uid;
				$this->request->data['tblMailing']['m_sl'] = $SubjectLine;
				$this->request->data['tblMailing']['m_sent_date'] = "'" . date('Y-m-d H:i:s') . "'";
				$this->request->data['tblMailing']['m_schedule_date'] = "'" . date('Y-m-d H:i:s') . "'";
				$this->tblMailing->create();
				if ($this->tblMailing->save($this->request->data))
				{
					$mailing_id = $this->tblMailing->getLastInsertID();
				}
			}
			
			$aduidDFP = $this->tblMappedAdunit->field(
			'ma_ad_id',
			array('ma_uid' => $m_ad_uid)
			);
			
			$aduid = $this->tblAdunit->field(
			'ad_uid',
			array('ad_dfp_id' => $aduidDFP)
			);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			
			$user_id = $this->User->field(
			'tblProfileID',
			array('id' => $pub_id)
			);
			
			$tracksite_id = $this->tblProfile->field(
			'tracksite_id',
			array('profileID' => $user_id)
			);
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			$tblSubscribers = $this->Dynamic->Query("SELECT * FROM " . $table_name . " WHERE nl_isActive=1 and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , 20");
			
			$htmlContent = $this->get_content($url);
			$ulliItems = '';
			$fullhtml = '';
			$firstTitle = '';
			$i = 0;
			
			
			if(!empty($tblSubscribers) && !empty($htmlContent))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber[$table_name]['nl_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$myArray = array();
						$message_id = $this->tblMessage->getLastInsertID();
						$myArray[] = array('Field'=>'DeliveryAddress', 'Value'=>''.$tblSubscriber[$table_name]['nl_email'].'');
						$myArray[] = array('Field'=>'ExMessageId', 'Value'=> 'DA-'.$month_year.'-'.$message_id);
						$strArray[] = $myArray;
					}
				}
				//build merge data
				//echo $strSubscribers;
				
				
				$htmlmessage = $htmlContent . ' <br /><br /> ' . $physical_address .  '
				<img src="http://tracking.digitaladvertising.systems/piwik.php?idsite=301&rec=1&c_n=106&c_p='.$m_ad_uid .'-'.$mailing_id .'&send_image=0" width="1" height="1">
				</body>
				</html>';
				$h2t =& new html2text($htmlmessage);
				$textVersion = $h2t->get_text();
				
				$merge_data = new stdClass();
				$merge_data->PerMessage = $strArray;
				//%%MessageId%%
				// Create JSON object to POST to SocketLabs
				$data = new stdClass();
				$data->ServerId=SOCKETLABS_SERVERID;
				$data->ApiKey=SOCKETLABS_KEY;
				$data->Messages= array(array('MergeData'=>$merge_data,
				'Subject'=> "$SubjectLine",
				'To'=>array(array('EmailAddress'=>'%%DeliveryAddress%%')),
				'From'=>array('EmailAddress'=>$fromEmail, 'FriendlyName' => $siteName),
				'HtmlBody'=>"$htmlmessage",
				'TextBody'=>"$textVersion",
				'MailingId'=>'DASSOCEX-'.$mailing_id,
				'MessageId'=>'%%MessageId%%'
				)); 
				$bodyJson = json_encode($data);
				//echo $bodyJson;
				
				
				//use CURL to POST the JSON to SocketLabs
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,SOCKETLABS_URL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt($ch, CURLOPT_POST,           1 );
				curl_setopt($ch, CURLOPT_POSTFIELDS,     $bodyJson ); 
				curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json')); 
				$result=curl_exec ($ch);
				curl_close ($ch);
				
				//process result
				if ($result==false) {
					echo 'HTTP Error';
					} else {
					$result_obj = json_decode($result);
					if ($result_obj->ErrorCode=='Success') {
						$this->tblMailing->updateAll(
						array('tblMailing.m_esp_id' => "'DASSOCEX-".$mailing_id."'", 'tblMailing.m_sent_date' => "'" . date('Y-m-d H:i:s') . "'", 'tblMailing.m_schedule_date' =>"'" .  date('Y-m-d H:i:s') . "'"),
						array('tblMailing.m_id' => $mailing_id));  
						echo 'Successfully sent all messages.';
						} elseif ($result_obj->ErrorCode=='Warning') {
						echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.'; 
						//not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
						} else {
						echo 'Failure Code: ' . $result_obj->ErrorCode;
					}
					print_r($result_obj);
				}
				print_r($merge_data);
				
			}
			
			$this->autoRender = false;
			
		}
		
		public function VineNLSocketLabs()
		{
			header('Content-Type: application/json');
			define('SOCKETLABS_URL', 'https://inject.socketlabs.com/api/v1/email');
			define('SOCKETLABS_SERVERID', '12888');
			define('SOCKETLABS_KEY', 'Dm35SjPk46Yqe2Q7GbNn');
			
			$mailing_id = 0;
			
			$aduid = $this->request->param('aduid');
			
			$mailing_id = $this->tblMailing->field(
			'm_id',
			array('m_ad_uid' => $aduid, 'cast(created_at as date)' => date('Y-m-d'))
			);
			
			if(!$mailing_id)
			{
				$this->request->data['tblMailing']['m_ad_uid'] = $aduid;
				$this->request->data['tblMailing']['m_sl'] = "Most popular vines this week";
				$this->tblMailing->create();
				if ($this->tblMailing->save($this->request->data))
				{
					$mailing_id = $this->tblMailing->getLastInsertID();
				}
			}
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			$tblSubscribers = $this->Dynamic->Query("SELECT * FROM " . $table_name . " WHERE nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , 490");
			
			
			
			if(!empty($tblSubscribers))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber[$table_name]['nl_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$myArray = array();
						$message_id = $this->tblMessage->getLastInsertID();
						$myArray[] = array('Field'=>'DeliveryAddress', 'Value'=>''.$tblSubscriber[$table_name]['nl_email'].'');
						$myArray[] = array('Field'=>'MessageId', 'Value'=> 'DA-'.$month_year.'-'.$message_id);
						$strArray[] = $myArray;
					}
				}
				//build merge data
				//echo $strSubscribers;
				$merge_data = new stdClass();
				$merge_data->PerMessage = $strArray;
				//%%MessageId%%
				// Create JSON object to POST to SocketLabs
				$homepage = file_get_contents('http://www.digitaladvertising.systems/vine/vine_videos.php');
				$data = new stdClass();
				$data->ServerId=SOCKETLABS_SERVERID;
				$data->ApiKey=SOCKETLABS_KEY;
				$data->Messages= array(array('MergeData'=>$merge_data,
				'Subject'=>'[TEST '.$mailing_id . '] : 4 Financial Stocks Up Over 90% This Week & Up Today',
				'To'=>array(array('EmailAddress'=>'%%DeliveryAddress%%')),
				'From'=>array('EmailAddress'=>'daytraders.finance@gmail.com'),
				'HtmlBody'=>$homepage,
				'MailingId'=>'DA-'.$mailing_id,
				'MessageId'=>'%%MessageId%%'
				)); 
				$bodyJson = json_encode($data);
				//echo $bodyJson;
				
				//use CURL to POST the JSON to SocketLabs
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,SOCKETLABS_URL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt($ch, CURLOPT_POST,           1 );
				curl_setopt($ch, CURLOPT_POSTFIELDS,     $bodyJson ); 
				curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json')); 
				$result=curl_exec ($ch);
				curl_close ($ch);
				
				//process result
				if ($result==false) {
					echo 'HTTP Error';
					} else {
					$result_obj = json_decode($result);
					if ($result_obj->ErrorCode=='Success') {
						echo 'Successfully sent all messages.';
						} elseif ($result_obj->ErrorCode=='Warning') {
						echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.'; 
						//not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
						} else {
						echo 'Failure Code: ' . $result_obj->ErrorCode;
					}
					print_r($result_obj);
				}
				print_r($merge_data);
			}
			
			$this->autoRender = false;
			
		}
		
		
		public function videoNLSocketLabs()
		{
			header('Content-Type: application/json');
			define('SOCKETLABS_URL', 'https://inject.socketlabs.com/api/v1/email');
			define('SOCKETLABS_SERVERID', '12888');
			define('SOCKETLABS_KEY', 'Dm35SjPk46Yqe2Q7GbNn');
			$tblSubscribers = "sunny";
			
			
			if(!empty($tblSubscribers))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				$myArray = array();
				$message_id = 'video';
				$myArray[] = array('Field'=>'DeliveryAddress', 'Value'=>'sandeep.bansal@siliconbiztech.com');
				$myArray[] = array('Field'=>'MessageId', 'Value'=> 'DA-'.$month_year.'-'.$message_id);
				$strArray[] = $myArray;
				//build merge data
				//echo $strSubscribers;
				$merge_data = new stdClass();
				$merge_data->PerMessage = $strArray;
				//%%MessageId%%
				// Create JSON object to POST to SocketLabs
				$homepage = file_get_contents('http://digitaladvertising.systems/video_test_email.php');
				$data = new stdClass();
				$data->ServerId=SOCKETLABS_SERVERID;
				$data->ApiKey=SOCKETLABS_KEY;
				$data->Messages= array(array('MergeData'=>$merge_data,
				'Subject'=>'SandeepBansalExperiments - Video in Email Test - '.date("m/d/Y") ,
				'To'=>array(array('EmailAddress'=>'%%DeliveryAddress%%')),
				'From'=>array('EmailAddress'=>'mailer@digitaladvertising.systems',  "FriendlyName"=> "Sandeep Bansal"),
				'HtmlBody'=>$homepage,
				'MailingId'=>'DA-video',
				'MessageId'=>'%%MessageId%%'
				)); 
				$bodyJson = json_encode($data);
				//echo $bodyJson;
				
				//use CURL to POST the JSON to SocketLabs
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,SOCKETLABS_URL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt($ch, CURLOPT_POST,           1 );
				curl_setopt($ch, CURLOPT_POSTFIELDS,     $bodyJson ); 
				curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json')); 
				$result=curl_exec ($ch);
				curl_close ($ch);
				
				//process result
				if ($result==false) {
					echo 'HTTP Error';
					} else {
					$result_obj = json_decode($result);
					if ($result_obj->ErrorCode=='Success') {
						echo 'Successfully sent all messages.';
						} elseif ($result_obj->ErrorCode=='Warning') {
						echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.'; 
						//not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
						} else {
						echo 'Failure Code: ' . $result_obj->ErrorCode;
					}
					print_r($result_obj);
				}
				print_r($merge_data);
			}
			
			$this->autoRender = false;
			
		}
		
		
		public function instagramNLSocketLabs()
		{
			header('Content-Type: application/json');
			define('SOCKETLABS_URL', 'https://inject.socketlabs.com/api/v1/email');
			define('SOCKETLABS_SERVERID', '12888');
			define('SOCKETLABS_KEY', 'Dm35SjPk46Yqe2Q7GbNn');
			
			$mailing_id = 0;
			
			$aduid = $this->request->param('aduid');
			
			$mailing_id = $this->tblMailing->field(
			'm_id',
			array('m_ad_uid' => $aduid, 'cast(created_at as date)' => date('Y-m-d'))
			);
			
			//if(!$mailing_id)
			//{
			$this->request->data['tblMailing']['m_ad_uid'] = $aduid;
			$this->request->data['tblMailing']['m_sl'] = "SandeepBansalExperiments - Instagram Updates";
			$this->tblMailing->create();
			if ($this->tblMailing->save($this->request->data))
			{
				$mailing_id = $this->tblMailing->getLastInsertID();
			} 
			//}
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $aduid)
			);
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			$tblSubscribers = $this->Dynamic->Query("SELECT * FROM " . $table_name . " WHERE nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , 490");
			
			
			
			if(!empty($tblSubscribers))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber[$table_name]['nl_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$myArray = array();
						$message_id = $this->tblMessage->getLastInsertID();
						$myArray[] = array('Field'=>'DeliveryAddress', 'Value'=>''.$tblSubscriber[$table_name]['nl_email'].'');
						$myArray[] = array('Field'=>'MessageId', 'Value'=> 'DA-'.$month_year.'-'.$message_id);
						$strArray[] = $myArray;
					}
				}
				//build merge data
				//echo $strSubscribers;
				$merge_data = new stdClass();
				$merge_data->PerMessage = $strArray;
				//%%MessageId%%
				// Create JSON object to POST to SocketLabs
				$homepage = file_get_contents('http://digitaladvertising.systems/instagram_nl.php');
				$data = new stdClass();
				$data->ServerId=SOCKETLABS_SERVERID;
				$data->ApiKey=SOCKETLABS_KEY;
				$data->Messages= array(array('MergeData'=>$merge_data,
				'Subject'=>'SandeepBansalExperiments - Instagram Updates - '.date("m/d/Y") ,
				'To'=>array(array('EmailAddress'=>'%%DeliveryAddress%%')),
				'From'=>array('EmailAddress'=>'mailer@digitaladvertising.systems',  "FriendlyName"=> "Sandeep Bansal"),
				'HtmlBody'=>$homepage,
				'MailingId'=>'DA-'.$mailing_id,
				'MessageId'=>'%%MessageId%%'
				)); 
				$bodyJson = json_encode($data);
				//echo $bodyJson;
				
				//use CURL to POST the JSON to SocketLabs
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,SOCKETLABS_URL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt($ch, CURLOPT_POST,           1 );
				curl_setopt($ch, CURLOPT_POSTFIELDS,     $bodyJson ); 
				curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json')); 
				$result=curl_exec ($ch);
				curl_close ($ch);
				
				//process result
				if ($result==false) {
					echo 'HTTP Error';
					} else {
					$result_obj = json_decode($result);
					if ($result_obj->ErrorCode=='Success') {
						echo 'Successfully sent all messages.';
						} elseif ($result_obj->ErrorCode=='Warning') {
						echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.'; 
						//not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
						} else {
						echo 'Failure Code: ' . $result_obj->ErrorCode;
					}
					print_r($result_obj);
				}
				print_r($merge_data);
			}
			
			$this->autoRender = false;
			
		}
		
		
		
		
		public function mailFromMandrill()
		{
			$this->autoRender = false;
			App::import('Vendor', 'src/Mandrill');
			try {
				$tracksite_id = "1";
				$mailing_id = "1";
				$m_ad_uid  = "1";
				$options['conditions'] = array('tblMappedAdunit.ma_ad_id' => '392927071', 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => '300x250');
				$options['joins'] = array(
				array('table' => 'tbl_line_items',
				'alias' => 'tli',
				'type' => 'INNER',
				'conditions' => array(
				'tli.li_id = tblMappedAdunit.ma_l_id')),
				array('table' => 'tbl_creatives',
				'alias' => 'tbc', 
				'type' => 'INNER',
				'conditions' => array(
				'tbc.cr_lid = tli.li_dfp_id'
				)));
				$options['fields'] = array('tbc.*','tblMappedAdunit.*');
				$tblMapped300x250 = $this->tblMappedAdunit->find('first', $options);
				$Banner300X250Image = $tblMapped300x250['tbc']['cr_preview_url'];
				$Banner300X250Redirect = $tblMapped300x250['tbc']['cr_redirect_url'];
				$Banner300X250ClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped300x250['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped300x250['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%%DeliveryAddress%%&redirect=".$Banner300X250Redirect;
				$Banner300X250Impression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped300x250['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped300x250['tblMappedAdunit']['ma_uid']."&send_image=0";
				
				
				$options1['conditions'] = array('tblMappedAdunit.ma_ad_id' => '392927311', 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => '728x90');
				$options1['joins'] = array(
				array('table' => 'tbl_line_items',
				'alias' => 'tli',
				'type' => 'INNER',
				'conditions' => array(
				'tli.li_id = tblMappedAdunit.ma_l_id')),
				array('table' => 'tbl_creatives',
				'alias' => 'tbc', 
				'type' => 'INNER',
				'conditions' => array(
				'tbc.cr_lid = tli.li_dfp_id'
				)));
				$options1['fields'] = array('tbc.*','tblMappedAdunit.*');
				$tblMapped728x90 = $this->tblMappedAdunit->find('first', $options1);
				$Banner728x90Image = $tblMapped728x90['tbc']['cr_preview_url'];
				$Banner728x90Redirect = $tblMapped728x90['tbc']['cr_redirect_url'];
				$Banner728x90ClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped728x90['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped728x90['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%%DeliveryAddress%%&redirect=".$Banner728x90Redirect;
				$Banner728x90Impression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$tblMapped728x90['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$tblMapped728x90['tblMappedAdunit']['ma_uid']."&send_image=0";
				
				
				$options2['conditions'] = array('tblMappedAdunit.ma_ad_id' => '400047271', 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => 'text/html');
				$options2['joins'] = array(
				array('table' => 'tbl_line_items',
				'alias' => 'tli',
				'type' => 'INNER',
				'conditions' => array(
				'tli.li_id = tblMappedAdunit.ma_l_id')),
				array('table' => 'tbl_creatives',
				'alias' => 'tbc', 
				'type' => 'INNER',
				'conditions' => array(
				'tbc.cr_lid = tli.li_dfp_id'
				)));
				$options2['fields'] = array('tbc.*','tblMappedAdunit.*');
				$sponsoredCreative = $this->tblMappedAdunit->find('first', $options2);
				$sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
				$sponsoredBody = $sponsoredCreative['tbc']['cr_body'];
				$sponsoredRedirect = $sponsoredCreative['tbc']['cr_redirect_url'];
				$sponsoredClickURL = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&c_i=lead&email=%%DeliveryAddress%%&redirect=".$sponsoredRedirect;
				$sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=creative_".$sponsoredCreative['tbc']['cr_id']."-mailing_".$mailing_id."&c_p=".$sponsoredCreative['tblMappedAdunit']['ma_uid']."&send_image=0";
				
				
				$url = 'http://politicsandmyportfolio.com/rss/';
				$rssfeed = $this->getFeed($url);
				$ulliItems = '';
				$fullhtml = '';
				$firstTitle = '';
				$i = 0;
				foreach($rssfeed as $entry) {
					if($i == 5)
					break;
					
					if($i == 0)
					$firstTitle = $entry->title;
					
					$ulliItems .= "<li><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
					
					
					if($i == 3) {
						$fullhtml .= "<table border='0' cellpadding='20' cellspacing='0' width='100%' mc:repeatable='siwc_600' mc:variant='content with right image'>
						<tr>
						<td valign='top'>
						<div mc:edit='riwc600_content00'>
						<a href='". $Banner728x90ClickURL . "'><img src='". $Banner728x90Image . "' /></a></div>
						</td>
						</tr>
						</table><hr />";
					}
					
					
					if($i == 1) {
						$fullhtml .= "  <table border='0' cellpadding='20' cellspacing='0' width='100%'>
						<tr><td valign='top' style='padding-top:0px;'>
						<div mc:edit='std_content00'>
						<span style='color:red;'>SPONSORED POST</span>
						<h2 class='h4'><a style='text-decoration:none;color:#3366A4;'  href='" . $sponsoredClickURL . "'>" . $sponsoredTitle . "</a></h2>" . $sponsoredBody . " (<a href='" . $sponsoredClickURL . "'>Click Here to Automatically Subscribe</a>)
						<img src='" . $sponsoredImpression . "'  width='1' height='1' />
						</div>
						</td>
						</tr>
						</table> <hr />";
						
					}
					
					$fullhtml .= "<table border='0' cellpadding='20' cellspacing='0' width='100%' mc:repeatable='siwc_600' mc:variant='content with right image'>
					<tr>
					<td valign='top'  style='padding-top:0px;'>
					<div mc:edit='riwc600_content00'>
					<h2 class='h4'><a style='text-decoration:none;color:#3366A4;'  href='$entry->link'>$entry->title</a></h2>$entry->description</div>
					</td>
					</tr>
					</table><hr />";
					$i++;
				}
				$mandrill = new Mandrill('UVRlnjaTpI2KefEWXiF_Ng');
				$message = array(
				'html' => "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
				<html>
				<head>
				<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
				
				<!-- Facebook sharing information tags -->
				<meta property='og:title' content='Review: Ten Economic Questions for 2015' />
				
				<title>Review: Ten Economic Questions for 2015</title>
				<style type='text/css'>
				/* Client-specific Styles */
				#outlook a{padding:0;} /* Force Outlook to provide a 'view in browser' button. */
				body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
				body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */
				
				/* Reset Styles */
				body{margin:0; padding:0;}
				img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
				table td{border-collapse:collapse;}
				#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}
				
				/* Template Styles */
				
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COMMON PAGE ELEMENTS /\/\/\/\/\/\/\/\/\/\ */
				
				/**
				* @tab Page
				* @section background color
				* @tip Set the background color for your email. You may want to choose one that matches your company's branding.
				* @theme page
				*/
				body, #backgroundTable{
				/*@editable*/ background-color:#FAFAFA;
				}
				
				/**
				* @tab Page
				* @section email border
				* @tip Set the border for your email.
				*/
				#templateContainer{
				/*@editable*/ border: 1px solid #DDDDDD;
				}
				
				/**
				* @tab Page
				* @section heading 1
				* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
				* @style heading 1
				*/
				h1, .h1{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Page
				* @section heading 2
				* @tip Set the styling for all second-level headings in your emails.
				* @style heading 2
				*/
				h2, .h2{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:30px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Page
				* @section heading 3
				* @tip Set the styling for all third-level headings in your emails.
				* @style heading 3
				*/
				h3, .h3{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:26px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Page
				* @section heading 4
				* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
				* @style heading 4
				*/
				h4, .h4{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:22px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
				}
				
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: PREHEADER /\/\/\/\/\/\/\/\/\/\ */
				
				/**
				* @tab Header
				* @section preheader style
				* @tip Set the background color for your email's preheader area.
				* @theme page
				*/
				#templatePreheader{
				/*@editable*/ background-color:#FAFAFA;
				}
				
				/**
				* @tab Header
				* @section preheader text
				* @tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
				*/
				.preheaderContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:10px;
				/*@editable*/ line-height:100%;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Header
				* @section preheader link
				* @tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
				*/
				.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
				}
				
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: HEADER /\/\/\/\/\/\/\/\/\/\ */
				
				/**
				* @tab Header
				* @section header style
				* @tip Set the background color and border for your email's header area.
				* @theme header
				*/
				#templateHeader{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border-bottom:0;
				}
				
				/**
				* @tab Header
				* @section header text
				* @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
				*/
				.headerContent{
				/*@editable*/ color:#202020;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ padding:0;
				/*@editable*/ text-align:center;
				/*@editable*/ vertical-align:middle;
				}
				
				/**
				* @tab Header
				* @section header link
				* @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
				*/
				.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
				}
				
				#headerImage{
				height:auto;
				max-width:600px;
				}
				
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: MAIN BODY /\/\/\/\/\/\/\/\/\/\ */
				
				/**
				* @tab Body
				* @section body style
				* @tip Set the background color for your email's body area.
				*/
				#templateContainer, .bodyContent{
				/*@editable*/ background-color:#FFFFFF;
				}
				
				/**
				* @tab Body
				* @section body text
				* @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
				* @theme main
				*/
				.bodyContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Body
				* @section body link
				* @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
				*/
				.bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
				}
				
				.bodyContent img{
				display:inline;
				height:auto;
				}
				
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: FOOTER /\/\/\/\/\/\/\/\/\/\ */
				
				/**
				* @tab Footer
				* @section footer style
				* @tip Set the background color and top border for your email's footer area.
				* @theme footer
				*/
				#templateFooter{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border-top:0;
				}
				
				/**
				* @tab Footer
				* @section footer text
				* @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
				* @theme footer
				*/
				.footerContent div{
				/*@editable*/ color:#707070;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:12px;
				/*@editable*/ line-height:125%;
				/*@editable*/ text-align:left;
				}
				
				/**
				* @tab Footer
				* @section footer link
				* @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
				*/
				.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
				}
				
				.footerContent img{
				display:inline;
				}
				
				/**
				* @tab Footer
				* @section social bar style
				* @tip Set the background color and border for your email's footer social bar.
				* @theme footer
				*/
				#social{
				/*@editable*/ background-color:#FAFAFA;
				/*@editable*/ border:0;
				}
				
				/**
				* @tab Footer
				* @section social bar style
				* @tip Set the background color and border for your email's footer social bar.
				*/
				#social div{
				/*@editable*/ text-align:center;
				}
				
				/**
				* @tab Footer
				* @section utility bar style
				* @tip Set the background color and border for your email's footer utility bar.
				* @theme footer
				*/
				#utility{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border:0;
				}
				
				/**
				* @tab Footer
				* @section utility bar style
				* @tip Set the background color and border for your email's footer utility bar.
				*/
				#utility div{
				/*@editable*/ text-align:center;
				}
				
				#monkeyRewards img{
				max-width:190px;
				}
				</style>
				</head>
				<body leftmargin='0' marginwidth='0' topmargin='0' marginheight='0' offset='0'>
				<center>
				<table border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='backgroundTable'>
            	<tr>
				<td align='center' valign='top'>
				<!-- // Begin Template Preheader \\ -->
				<table border='0' cellpadding='10' cellspacing='0' width='750' id='templatePreheader'>
				<tr>
				<td valign='top' class='preheaderContent'>
				
				<!-- // Begin Module: Standard Preheader \ -->
				<table border='0' cellpadding='10' cellspacing='0' width='100%'>
				<tr>
				<td valign='top'>
				<div mc:edit='std_preheader_content'>
				<img  width='200px' src='http://www.politicsandmyportfolio.com/wp-content/uploads/2015/09/politicsandmyportfolio-logo.png' />
				</div>
				</td>
				<!-- *|IFNOT:ARCHIVE_PAGE|* -->
				<td valign='top' width='190'>
				<div mc:edit='std_preheader_links'>
				
				</div>
				</td>
				<!-- *|END:IF|* -->
				</tr>
				</table>
				<!-- // End Module: Standard Preheader \ -->
				
				</td>
				</tr>
				</table>
				<!-- // End Template Preheader \\ -->
				
				</td>
				</tr>
				</table>
				
				<table border='0' cellpadding='0' cellspacing='0' width='750' id='templateContainer'  style='padding-top:10px'>
				
				<tr>
				<td align='center' valign='top'  style='padding-top:0px;'>
				<!-- // Begin Template Body \\ -->
				<table border='0' cellpadding='0' cellspacing='0' width='750' id='templateBody'>
				<tr>
				<td valign='top'  style='padding-top:0px;' class='bodyContent'>
				<table border='0' cellpadding='20' cellspacing='0' width='100%'>
				<tr>
				<td valign='top' style='padding-top:0px;'>
				<div mc:edit='std_content00'>
				<h2 class='h4'>Today's Top Stories</h2>
				<ul style='clear:both;padding:0 0 0 1.2em;width:100%'>" . $ulliItems
				. "
				</ul>
				
				</div>
				</td>
				<td  valign='top' style='padding-top:0px;'>
				<a href='". $Banner300X250ClickURL . "'><img src='". $Banner300X250Image . "' /></a>
				</td>
				</tr>
				</table>". $fullhtml . 	"</td>
				</tr>
				</table>
				<!-- // End Template Body \\ -->
				</td>
				</tr>
				<tr>
				<td align='center' valign='top'>
				<!-- // Begin Template Footer \\ -->
				<table border='0' cellpadding='10' cellspacing='0' width='750' id='templateFooter'>
				<tr>
				<td valign='top' class='footerContent'>
				<!-- // Begin Module: Standard Footer \\ -->
				<table border='0' cellpadding='10' cellspacing='0' width='100%'>
				<tr>
				<td style='text-align:left;font-family:Helvetica,Arial,Sans-Serif;font-size:11px;margin:0 6px 1.2em 0;color:#333'>You are subscribed to email updates from <a target='_blank' href='http://www.politicsandmyportfolio.com/'>Politics and Myportfolio</a>
				<br>To stop receiving these emails, you may <UNSUBSCRIBE><TEXT>unsubscribe now</UNSUBSCRIBE></TEXT></a>.</td>
				<td style='font-family:Helvetica,Arial,Sans-Serif;font-size:11px;margin:0 6px 1.2em 0;color:#333;text-align:right;vertical-align:top'>Powered by <a target='_blank' href='http://digitaladvertising.systems/'>DigitalAdvertising</a></td>
				</tr>
				<tr>
				<td style='text-align:left;font-family:Helvetica,Arial,Sans-Serif;font-size:11px;margin:0 6px 1.2em 0;color:#333' colspan='2'>Politics and Myportfolio, New York</td>
				</tr>
				</table>
				<!-- // End Module: Standard Footer \\ -->
				
				</td>
				</tr>
				</table>
				<!-- // End Template Footer \\ -->
				</td>
				</tr>
				</table>
				<br />
				</td>
				</tr>
				</table>
				<img src='http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tracksite_id."&rec=1&c_n=".$m_ad_uid."&c_p=DAS-MAILING-".$mailing_id."&send_image=0'  width='1' heigth='1'  />
				<img src='".$Banner300X250Impression."' width='1' heigth='1' />
				<img src='".$Banner728x90Impression."' width='1' heigth='1' />
				</center>
				</body></html>",
				'text' => 'Example text content',
				'subject' => 'How Fear Will Ruin Your Financial Future',
				'from_email' => 'info@mandrill.digitaladvertising.systems',
				'from_name' => 'PoliticsAndMyPortfolio',
				'to' => array(
				array(
                'email' => 'sunny.gulati@siliconbiztech.com',
                'name' => 'Sunny Gulati',
                'type' => 'to'
				),array(
                'email' => 'neha.gulati@siliconbiztech.com',
                'name' => 'Neha Gulati',
                'type' => 'to'
				),array(
                'email' => 'fakhruzzama@siliconbiztech.com',
                'name' => 'Fakhruzzama',
                'type' => 'to'
				)
				),
				'headers' => array('Reply-To' => 'Sunny Gulati'),
				'important' => false,
				'auto_text' => true,
				'auto_html' => null,
				'inline_css' => null,
				'url_strip_qs' => null,
				'preserve_recipients' => false,
				'tracking_domain' => null,
				'signing_domain' => 'mandrill.digitaladvertising.systems',
				'return_path_domain' => null,
				'merge' => true,
				'merge_language' => 'mailchimp',
				'tags' => array('password-resets'),
				'subaccount' => 'Test',
				);
				$async = false;
				$ip_pool = 'Main Pool';
				$send_at = '';
				$result = $mandrill->messages->send($message, $async, $ip_pool, $send_at);
				print_r($result);
				/*
					Array
					(
					[0] => Array
					(
					[email] => recipient.email@example.com
					[status] => sent
					[reject_reason] => hard-bounce
					[_id] => abc123abc123abc123abc123abc123
					)
					
					)
				*/
				} catch(Mandrill_Error $e) {
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
				throw $e;
			}
		}
		
		
		
		function getFeed($feed_url) {
			
			$content = file_get_contents($feed_url);
			$x = new SimpleXmlElement($content);
			return $x->channel->item;
		}
		
		function get_content($URL){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $URL);
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;
		}
	}
