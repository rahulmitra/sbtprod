<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class ReportController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','RegistrationsData','Authake.tblAdunit','Dynamic');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		public function strategic_report() {
		$optionm=array();
		$optionm['fields'] = array('RegistrationsData.*');
		$RegAll = $this->RegistrationsData->find('all',$optionm);
		$this->set('RegStatistics', $RegAll);
		$php_arrayReg="[";
		$php_arrayLeads="[";
		$php_arrayLeadsPerReg="[";
		$php_arrayRevenuePerReg="[";
		foreach ($RegAll as $Monthly) {
				$MonthName = $Monthly['RegistrationsData']['MonthName'];
				$RegCount = $Monthly['RegistrationsData']['RegCount'];
				$LeadCount = $Monthly['RegistrationsData']['LeadCount'];
				$LeadPerReg = $Monthly['RegistrationsData']['LeadPerReg'];
				$RevPerReg = $Monthly['RegistrationsData']['RevPerReg'];
				$php_arrayReg .="['".$MonthName."',".$RegCount."],";
				$php_arrayLeads .="['".$MonthName."',".$LeadCount."],";
				$php_arrayLeadsPerReg .="['".$MonthName."',".$LeadPerReg."],";
				$php_arrayRevenuePerReg .="['".$MonthName."',".$RevPerReg."],";
			}
			$php_arrayReg .="]";
			$php_arrayLeads .="]";
			$php_arrayLeadsPerReg .="]";
			$php_arrayRevenuePerReg .="]";
			//$php_array=array("Jan" => "6369","Feb" => "7052","Mar" => "7611","Apr" => "8849","May" => "11618","Jun" => "14950","Jul" => "15485","Aug" => "13124","Sep" => "21861");
			//$php_array='{"Jan":"6369","Feb":"7052","Mar":"7611","Apr":"8849","May":"11618","Jun":"14950","Jul":"15485","Aug":"13124","Sep":"21861"}';
			//$php_array = array('Jan','def','ghi');
			//$php_array="[['Jan', 66247],    ['Feb', 58137],   ['Mar', 85002],  ['Apr', 81138],   ['May', 55556],  ['Jun', 34469],['Jul', 23098], ['Aug', 40655], ['Sep', 75386]]";
			$this->set('graph_arrayReg', $php_arrayReg);
			$this->set('graph_arrayLeads', $php_arrayLeads);
			$this->set('graph_arrayLeadsPerReg', $php_arrayLeadsPerReg);
			$this->set('graph_arrayRevenuePerReg', $php_arrayRevenuePerReg);
			
		}
		
		
		
		public function order_report($orderID)
		{
			
			
			
		}
		
	}
?>