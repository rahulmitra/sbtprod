<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class SettingsController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblEmailServiceProvider','RegistrationsData','Authake.tblAdunit','Authake.tblProfile','Dynamic', 'tblUserSettings');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests/
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		public function index() {
			// add css 
			$this->System->add_css(array('/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css'),'head');
			
			$esps = $this->tblEmailServiceProvider->find('all');
			/*
				try {
				App::import('Vendor', 'src/lyrisapi');
				$isSuccessLyris = 0;
				$lyrisSite = Configure::read('User.lyris_siteid_'.$this->Authake->getUserId());
				$lyrisPassword = Configure::read('User.lyris_password_'.$this->Authake->getUserId());
				if(!empty($lyrisSite) && !empty($lyrisPassword))
				{
				$lyriss = new lyrisapi($lyrisSite,$lyrisPassword);
				$messages = $lyriss->listQuery("Lyris@API");
				$orders_array = array();
				$total = count($messages);
				$total = (int)$total - 1;
				if($total > 0)
				{
				$isSuccessLyris = 1;
				}
				} 
				} catch (Exception $e) {
				$isSuccessLyris = 0;
			} */
			$this->set('isSuccessLyris', '0');
			$test = $this->tblProfile->getAllConnectedProfiles();
			$this->set('group', $test);
			$this->set('esps', $esps);
		}
		
		
		public function lyrisConnect() {
			App::import('Vendor', 'src/lyrisapi');
			
			$isSuccess = 0;
			$lyrisSite = Configure::read('User.lyris_siteid_'.$this->Authake->getUserId());
			$lyrisPassword = Configure::read('User.lyris_password_'.$this->Authake->getUserId());
			if(!empty($lyrisSite) && !empty($lyrisPassword))
			{
				$lyriss = new lyrisapi($lyrisSite,$lyrisPassword);
				$messages = $lyriss->listQuery("Lyris@API");
				$orders_array = array();
				$total = count($messages);
				$total = (int)$total - 1;
				if($total > 0)
				{
					$isSuccess = 3;
				}
			}
			
			
			if (!empty($this->request->data))
			{
				$isSuccess = 1;
				$total = 0;
				try {
					$lyriss = new lyrisapi($this->request->data['tblUserSettings']['SiteID'],$this->request->data['tblUserSettings']['password']);
					$messages = $lyriss->listQuery("Lyris@API");
					$orders_array = array();
					$total = count($messages);
					$total = (int)$total - 1;
					} catch (Exception $e) {
					$total = 0;
				}
				
				
				if($total > 0)
				{
					$this->request->data['tblUserSettings']['name'] = "lyris_siteid_".$this->Authake->getUserId();
					$this->request->data['tblUserSettings']['value'] = $this->request->data['tblUserSettings']['SiteID'];
					$this->request->data['tblUserSettings']['userId'] = $this->Authake->getUserId();
					$this->tblUserSettings->create();
					if ($this->tblUserSettings->save($this->request->data))
					{
						$this->request->data['tblUserSettings']['name'] = "lyris_password_".$this->Authake->getUserId();
						$this->request->data['tblUserSettings']['value'] = $this->request->data['tblUserSettings']['password'];
						$this->request->data['tblUserSettings']['userId'] = $this->Authake->getUserId();
						$this->tblUserSettings->create();
						if ($this->tblUserSettings->save($this->request->data))
						{
							$isSuccess = 2;
						}
					}
				}
			}
			$this->set('isSuccess', $isSuccess);
			$this->set('lyrisSite', $lyrisSite);
			$this->set('lyrisPassword', $lyrisPassword);
		}
		
		public function mailchimpConnect(){
		App::import('Vendor', 'src/html2text');
		App::import('Vendor', array('file' => 'autoload'));
	
		$isSuccess=0;
		if (!empty($this->request->data))
			{
			$api_key = $this->request->data['tblUserSettings']['ApiKey'];
			//print_r($api_key);
			if ($api_key == '6c7a3b4ecb8a3027ed8cf01a8ac3894d-us13'){
			$MailChimp = new \DrewM\MailChimp\MailChimp($api_key);
			$result = $MailChimp->get('lists');
			if(isset($result)){
			$isSuccess=2;
			$this->request->data['tblUserSettings']['name'] = "Malichimp_Api_".$this->Authake->getUserId();
			$this->request->data['tblUserSettings']['value'] = $api_key ;
			$this->request->data['tblUserSettings']['userId'] = $this->Authake->getUserId();
			$this->tblUserSettings->save($this->request->data);
			} else {
			$isSuccess=1;
			}
			} else {
			$isSuccess=1;
			}
		}
		
		$this->set('isSuccess', $isSuccess);
		
	
	}
		public function socketLabConnect() {
			
		}
	}	