<?php
	
	class FullcontactController extends AppController {
		
		var $uses = array('Authake.Group','Authake.User','Authake.tblAdunit','Authake.tblNlsubscribers','Dynamic','Authake.tblMailing','Authake.tblProfile','Authake.tblMappedAdunit', 'Authake.tblMessage','UserLevelEmailPerformance');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		public function index() {		
			
		}
		
		public function getInfo($email){
			$URL = 'https://api.fullcontact.com/v2/person.json?email='.$email;
			
			$headr = array();
			$headr[] = 'X-FullContact-APIKey:5b745db78eb012df';
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$URL);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
			$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
			$result=curl_exec ($ch);
			curl_close ($ch);
			echo "<pre>";
			print_r($result);
			echo "</pre>";
			$this->autoRender = false;
			
		}
		
		public static function createApiCall($url, $method, $headers, $data = array())
        {
            if ($method == 'PUT')
            {
                $headers[] = 'X-HTTP-Method-Override: PUT';
			}
			
            $headers = self::mergeArray(array("Cache-Control: no-cache"), $headers);
            $handle = curl_init();
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($handle, CURLOPT_FRESH_CONNECT, true);
			
            switch($method)
            {
                case 'GET':
				break;
                case 'POST':
				curl_setopt($handle, CURLOPT_POST, true);
				curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));
				break;
                case 'PUT':
				curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));
				break;
                case 'DELETE':
				curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			}
            $response = curl_exec($handle);
            return $response;
		}
        
		public static function mergeArray($a,$b)
		{
			$args=func_get_args();
			$res=array_shift($args);
			while(!empty($args))
			{
				$next=array_shift($args);
				foreach($next as $k => $v)
				{
					if(is_integer($k))
					isset($res[$k]) ? $res[]=$v : $res[$k]=$v;
					elseif(is_array($v) && isset($res[$k]) && is_array($res[$k]))
					$res[$k]=self::mergeArray($res[$k],$v);
					else
					$res[$k]=$v;
				}
			}
			return $res;
		}
		
		
		public function userProfile(){
			$couch_dsn = "http://admin:C0uchADm!n@ec2-54-161-239-191.compute-1.amazonaws.com/";
			$couch_db = "digitaladvertising";
			App::import('Vendor', 'couchdb/couch');
			App::import('Vendor', 'couchdb/couchClient');
			App::import('Vendor', 'couchdb/couchDocument');
			$client = new couchClient($couch_dsn,$couch_db);
			$email = $this->request->param('email');
			//$email = 'mohd.kamil@siliconbiztech.com';
			$campaign_id = 7; // Campaign id. Change accordingly 
			$publisher = 'Test Publisher';
			$response_return = "";
			$md5email = md5($email);
			
			$data['campaign_id']=$campaign_id;
			$data['publisher']=$publisher;
			$data['md5email']=$md5email;
			try {
				$response_return = $client->getDoc($md5email);
				
				/*
				$URL = 'https://api.fullcontact.com/v2/person.json?email='.$email;
				
				$headr = array();
				$headr[] = 'X-FullContact-APIKey:5b745db78eb012df';
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$URL);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
				$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
				$response=curl_exec ($ch);
				curl_close ($ch);
				
				$response = json_decode($response, true);
				
				$headers = array(
				'Accept: application/json',
				'SMART_SESSION_ID: 7ccd0eb975032cad8c41dd80587f515daaa',
				'SMART_TOKEN: 564cdcae13d08aa',
				'SMART_API_REQUEST_TYPE: REST',
				);
				// call the API 
				$responsekma = $this->createApiCall('http://kmadigital.com/api/index.php/api/campaign/', 'POST', $headers,$data);
				$responsekma = json_decode($responsekma, true);
				
				if ($response['status'] == '200')
				{
					$data = json_decode(json_encode($response));
					
					foreach ($data as $key => $value)
					{
						$response_return->$key = $value;
					}
					$response_return->match_fc = 'Y';
					}else{
					$response_return->match_fc = 'N';
				}
				//kma
				
				if ($responsekma['status'] == 'SUCCESS')
				{
					
					$data = (object)$responsekma['data'];
					
					foreach ($data as $key => $value)
					{
						$response_return->$key = $value;
					}
					$response_return->match_kma = 'Y';
					}else{
					$response_return->match_kma = 'N';
				}
				
				try {
					$resp = $client->storeDoc($response_return);
					$response_return = $client->getDoc($md5email);
					} catch (Exception $e) {
					echo "Something weird happened: ".$e->getMessage()." (errcode=".$e->getCode().")\n";
					exit(1);
				}
				
				*/
				} catch (Exception $e) {
				
				$data['md5email']=$md5email;
				
				$URL = 'https://api.fullcontact.com/v2/person.json?email='.$email;
				
				$headr = array();
				$headr[] = 'X-FullContact-APIKey:5b745db78eb012df';
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$URL);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
				$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
				$response=curl_exec ($ch);
				curl_close ($ch);
				
				$response = json_decode($response, true);
				
				$headers = array(
				'Accept: application/json',
				'SMART_SESSION_ID: 7ccd0eb975032cad8c41dd80587f515daaa',
				'SMART_TOKEN: 564cdcae13d08aa',
				'SMART_API_REQUEST_TYPE: REST',
				);
				
				$responsekma = $this->createApiCall('http://kmadigital.com/api/index.php/api/campaign/', 'POST', $headers,$data);
				$responsekma = json_decode($responsekma, true);
				
				$doc = new stdClass();
				$doc->_id = $md5email;
				$doc->Type = "EmailProfile";
				$doc->Email = $email;
				
				
				if ($response['status'] == '200')
				{
					$data = json_decode(json_encode($response));
					foreach ($data as $key => $value)
					{
						$doc->$key = $value;
					}
					$doc->match_fc = 'Y';
					
					} else {
					$doc->match_fc = 'N';
					
				}
				
				//kmanew 
				if ($responsekma['status'] == 'SUCCESS')
				{
					
					echo "<p> Kma </p>";
					//echo "<pre>";
					//print_r($responsekma);
					//echo "</pre>";
					 
					$data = (object)$responsekma['data'];
					foreach ($data as $key => $value)
					{
						$doc->$key = $value;
					}
					$doc->match_kma = 'Y';
					} else {
					$doc->match_kma = 'N';
				}
				
				try {
					$resp = $client->storeDoc($doc);
					$response_return = $client->getDoc($md5email);
					} catch (Exception $e) {
					echo "Something weird happened: ".$e->getMessage()." (errcode=".$e->getCode().")\n";
					exit(1);
				}
				
				sleep(1);
			}
			//return $response_return;
			$this->autoRender = false;
			
		}
		
		
	}