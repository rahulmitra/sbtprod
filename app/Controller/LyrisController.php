<?php
	App::import('Controller', 'LyrisController');
	
	class LyrisController extends AppController {
		
		var $uses = array('Authake.tblMessage','Authake.tblTrackingDeliver','Authake.tblTrackingOpen','Authake.tblTrackingClick', 'Authake.tblCreative', 'Authake.tblLineItem');
		
		public function beforeFilter() {
			parent::beforeFilter();
			$this->layout=null;
		}
		
		public function index($listid) {
			App::import('Vendor', 'src/lyrisapi');
			$lyriss = new lyrisapi("666767","Lyris@API");
			$messages = $lyriss->messageQueryListData($listid,null,null,"Lyris@API");
			$results = array();
			$count = 1;
			foreach($messages as $message)
			{
				if($message['sent'] == 'yes' &&  strpos($message['subject'],'PROOF') === false)
				{	
					//print_r($message);
					$stats = $lyriss->messageQueryStats($message['mid'],"224629");
					$results[$count]['mid'] = $message['mid'];
					$results[$count]['subject'] = $message['subject'];
					$results[$count]['sent_time_tz'] = $message['sent_time_tz'];
					$results[$count]['sent'] = $stats['stats-sent'];
					$results[$count]['opens'] = $stats['stats-opens'];
					$results[$count]['opens-unique'] = $stats['stats-opens-unique'];
					$results[$count]['spam'] = $stats['stats-spam'];
					$results[$count]['clicks-unique'] = $stats['stats-clicks-unique'];
					$results[$count]['bounces'] = $stats['stats-bounces'];
					$results[$count]['unsubscribes'] = $stats['stats-unsubscribes'];
					$count++;
				}
			}
			
			//fputcsv($output, array('mid','subject','sent_time_tz','sent','opens','opens-unique','spam','clicks-unique','bounces','unsubscribes'));
			 
			
			$filePath = '/csv/'.$listid.'.csv';
			
			$exists = file_exists($filePath) && filesize($filePath) > 0;
			
			$myfile = fopen($filePath, 'a+');
			
			if (!$exists) {
				foreach($results as $product) {
					fputcsv($myfile, $product);
				}
			}
			
			//fputcsv($myfile, $posts_meta);
			
			$this->autoRender = false;
			return;
		}
	}
?>