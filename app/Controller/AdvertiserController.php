<?php

/**
 * Contact Controller
 * @author James Fairhurst <info@jamesfairhurst.co.uk>
 */
class AdvertiserController extends AppController {

	/**
	 * Components
	 */
	public $components = array('RequestHandler');

	/**
	 * Before Filter callback
	 */
	public function beforeFilter() {
		parent::beforeFilter();

		// Change layout for Ajax requests
		if ($this->request->is('ajax')) {
			$this->layout = 'ajax';
		}
	}

	/**
	 * Main index action
	 */
	public function index() {
		// form posted
		$this->set('title_for_layout','All Advertiser : SiliconBiztech.com');
	}
	
	public function add() {
		// form posted
		$this->set('title_for_layout','Advertiser Add : SiliconBiztech.com');
	}
}