<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class NewsletterAdUnitLeadsController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblAdunit','Authake.tblNlsubscribers','Dynamic');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {		
			$this->autoRender = false;
			if(!isset($_POST['uname']) || !isset($_POST['pwd']))
			{
				echo "Authentication failed.";
				die();
				} else {
				$pub_id = $this->User->field('id',array('email' => ''.$_POST['uname'].'','api_key' => ''. $_POST['pwd'] . ''));
				if(!$pub_id)
				{
					echo "Authentication failed.";
					die();
				}
			}
			
			if(!isset($_POST['email']))
			{
				echo "Email Id not provided";
				die();
			}
			if(!isset($_POST['aduid']))
			{
				echo "Ad Uid not provided";
				die();
			}
			if(!isset($_POST['ipaddress']))
			{
				echo "IP Address not provided";
				die();
			}
			$pub_id = $this->tblAdunit->field('publisher_id',array('ad_uid' => $_POST['aduid']));
			$fname = isset($_POST['fname']) ? $_POST['fname'] : "";
			$lname = isset($_POST['lname']) ? $_POST['lname'] : "";
			$source = isset($_POST['source']) ? $_POST['source'] : "";
			$ipaddress = isset($_POST['ipaddress']) ? $_POST['ipaddress'] : "";
			$timestamp = isset($_POST['timestamp']) ? $_POST['timestamp'] : "";
			$lastid = 0;
			try
			{
				$table_name = $pub_id. "_" . $_POST['aduid'];
				$this->Dynamic->useTable = $table_name; 
				$options['conditions'] = array('nl_email' => $_POST['email']);
				$resutls = $this->Dynamic->find('all', $options);
				$count = count($resutls);
				if($count > 0)
				{
					echo "Duplicate Record.";
					} else {
					$db = $this->Dynamic->getDataSource(); 
					$data = array(
					'Dynamic' => array(
					'nl_email' => $_POST['email'],
					'nl_fname' => $fname,
					'nl_lname' => $lname,
					'nl_source' => $source,
					'nl_timestamp' => $timestamp,
					'nl_ip_address' => $db->expression("INET_ATON('{$ipaddress}')")
					));
					$this->Dynamic->create();
					$this->Dynamic->save($data);
					$lastid =  $this->Dynamic->getLastInsertId();
					echo "Record added successfully.";
				}
				if($_POST['aduid'] == "79c7ca8d236411e6b91e123aa499f9d5")
				{
					$url = 'http://api.red3i.com/partner_leads.php';
					$data = array('username' => 'siliconbiztech', 'password' => '2eunuRqlQ3', 'manager_id' => '230', 'source_url' => 'www.qualityhealth.com', 'ipaddr' => $ipaddress, 'timestamp' => date('Y-m-d H:i:s'), 'email' => $_POST['email']);
					$options = array(
					'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
					'method'  => 'POST',
					'content' => http_build_query($data)
					));
					$context  = stream_context_create($options);
					$result = file_get_contents($url, false, $context);
					$this->Dynamic->updateAll(
					array('nl_pos_response' => "'".$result."'"),
					array('nl_id' => $lastid)
					);
 					$msg = file_get_contents("http://spmglms.com/Lead/addLead.php?partner=37&advertiser=51&offer=42&aff_id=0&traffictype=Coreg&FirstName=".$fname."&LastName=".$lname."&Email=".$_POST['email']);
					if ($result === FALSE) { 
						mail("sunny.gulati@siliconbiztech.com","Red3i Posting Error","<b>Error</b>".$result);
					}
					//echo $result;
					
				}
			}
			catch (Exception $e)
			{
				echo "Posting Failed";
				die();
			}
		}
		
		public function add() {		
			$isError = false;
			
			if(!$this->params['url']['aduid'])
			$isError = true;
			
			if(!$this->params['url']['nl_email'])
			$isError = true;
			
			//print_r($this->request->data);
			
			if(!$isError) {
				$pub_id = $this->tblAdunit->field(
				'publisher_id',
				array('ad_uid' => $this->params['url']['aduid'])
				);
				$table_name = $pub_id. "_" . $this->params['url']['aduid'];
				$this->Dynamic->useTable = $table_name; 
				$insertQuery = ("INSERT INTO " . $table_name . "(nl_email) VALUES ('{$this->params['url']['nl_email']}')");
				$this->Dynamic->query($insertQuery);
				} else {
				echo "Something Went Wrong";
			}
			$this->autoRender = false;
		}
		
	}																					