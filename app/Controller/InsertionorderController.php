<?php

/**
 * Contact Controller
 * @author James Fairhurst <info@jamesfairhurst.co.uk>
 */
class InsertionorderController extends AppController {

    /**
     * Components
     */
    var $uses = array('Authake.Group', 'Authake.User', 'Authake.tblOrder', 'Authake.Rule', 'Dynamic', 'Authake.tblLineItem', 'Authake.tblPlacement', 'Authake.tblProducttype', 'Authake.tblCostStructure', 'Authake.tblAdunit', 'Authake.tblCountries', 'Authake.tblAllowedMediaType', 'Authake.tblMappedAdunit', 'Authake.tblAllowedCountries', 'Authake.tblProfile', 'Authake.tblCreative', 'OrderLeads', 'Authake.audience_segments', 'Authake.tblFlightAudiences', 'Authake.tblAudienceSegments', 'Authake.tblDeliverySetting', 'Authake.tblFlight', 'Authake.tblMailing');
    var $components = array('RequestHandler', 'Authake.Filter', 'Session', 'Commonfunction', 'Timezone'); // var $layout = 'authake';
    var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc')); //var $scaffold;
    var $helpers = array('Js', 'FormDynamic');

    /**
     * Before Filter callback
     */
    public function beforeFilter() {
        parent::beforeFilter();

        // Change layout for Ajax requests
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
    }

    /**
     * Main index action
     */
    public function index() {
        // form posted
        $this->set('title_for_layout', 'All Orders');


        /* $options['conditions'] = array(
          'Ugroup.group_id' => '3' ); */

        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblp.owner_user_id' => array($this->Authake->getUserId()))
        ));

        $options['fields'] = array('tblOrder.*', 'tblp.CompanyName');

        $test = $this->tblOrder->find('all', $options);
        //print_r($test);
        $this->set('group', $test);


        /*
          App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
          App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
          App::import('Vendor', 'src/ExampleUtils');
          try {
          // Get DfpUser from credentials in "../auth.ini"
          // relative to the DfpUser.php file's directory.
          $user = new DfpUser();

          // Log SOAP XML request and response.
          $user->LogDefaults();

          // Get the OrderService.
          $orderService = $user->GetService('OrderService', 'v201608');

          // Create a statement to select all orders.
          $statementBuilder = new StatementBuilder();
          $statementBuilder->OrderBy('id ASC')
          ->Limit(StatementBuilder::SUGGESTED_PAGE_LIMIT);

          // Default for total result set size.
          $totalResultSetSize = 0;

          do {
          // Get orders by statement.
          $page = $orderService->getOrdersByStatement(
          $statementBuilder->ToStatement());

          // Display results.
          if (isset($page->results)) {
          $totalResultSetSize = $page->totalResultSetSize;
          $i = $page->startIndex;
          print_r($page->results);
          foreach ($page->results as $order) {
          printf("%d) Order with ID %d, name '%s', and advertiser ID %d was "
          . "found.\n", $i++, $order->id, $order->name, $order->advertiserId);
          }
          }

          $statementBuilder->IncreaseOffsetBy(StatementBuilder::SUGGESTED_PAGE_LIMIT);
          } while ($statementBuilder->GetOffset() < $totalResultSetSize);

          printf("Number of results found: %d\n", $totalResultSetSize);
          } catch (OAuth2Exception $e) {
          ExampleUtils::CheckForOAuth2Errors($e);
          } catch (ValidationException $e) {
          ExampleUtils::CheckForOAuth2Errors($e);
          } catch (Exception $e) {
          printf("%s\n", $e->getMessage());
          } */
    }

    public function add() {
        // form posted
        $this->set('title_for_layout', 'Order Creation');

        $company_id = isset($this->params['named']['company_id']) ? $this->params['named']['company_id'] : null;

        App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
        App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
        App::import('Vendor', 'src/ExampleUtils');

        //$this->set('user_company_name', $user_company_name);

        try {

            //$this->set('group', $test);
            // Get DfpUser from credentials in "../auth.ini"
            // relative to the DfpUser.php file's directory.
            $dfpuser = new DfpUser();

            // Log SOAP XML request and response.
            $dfpuser->LogDefaults();

            if (!empty($this->request->data)) {
                // remove here user id from advertiser 
                if (!empty($this->request->data['tblOrder']['advertiser_id'])) {
                    $company = explode("@", $this->request->data['tblOrder']['advertiser_id']);
                    $this->request->data['tblOrder']['advertiser_id'] = $company[0];
                }
                $email_id = $this->request->data['User']['email'];
                $option_order['joins'] = array(
                    array('table' => 'tbl_profiles',
                        'alias' => 'tblp',
                        'type' => 'INNER',
                        'conditions' => array(
                            'User.tblProfileID = tblp.ProfileID',
                            'User.email' => array($email_id))
                ));
                $option_order['fields'] = array('User.*', 'tblp.*');
                $user_find = $this->User->find('first', $option_order);
                //print_r($user_find);
                if (count($user_find) == 0) {
                    $randomString = "7654321";

                    $p = $randomString;
                    $this->request->data['User']['password'] = md5($p);
                    $this->request->data['User']['tblProfileID'] = $this->tblProfile->field('profileID', array('dfp_company_id' => $this->request->data['tblOrder']['advertiser_id']));
                    $this->request->data['User']['disable'] = 1;
                    $this->User->create();
                    if ($this->User->save($this->request->data)) {
                        try {
                            $user = new DfpUser();
                            // Log SOAP XML request and response.
                            $user->LogDefaults();

                            // Get the ContactService.
                            $contactService = $user->GetService('ContactService', 'v201608');
                            $advertiserCompanyId = $this->request->data['tblOrder']['advertiser_id'];

                            // Create an advertiser contact.
                            $advertiserContact = new Contact();
                            $advertiserContact->name = $this->request->data['User']['ContactName'];
                            $advertiserContact->email = $this->request->data['User']['email'];
                            $advertiserContact->companyId = $advertiserCompanyId;
                            // Create the contacts on the server.
                            $contacts = $contactService->createContacts(array($advertiserContact));

                            // Display results.
                            if (isset($contacts)) {
                                foreach ($contacts as $contact) {
                                    $this->request->data['User']['dfp_contact_id'] = $contact->id;
                                }
                            } else {
                                $this->request->data['User']['dfp_contact_id'] = 0;
                            }
                            $this->User->updateAll(
                                    array('User.dfp_contact_id' => $this->request->data['User']['dfp_contact_id']), array('User.id' => $this->User->getLastInsertID()));
                            $this->request->data['tblOrder']['advertiser_user_id'] = $this->User->getLastInsertID();
                        } catch (OAuth2Exception $e) {
                            ExampleUtils::CheckForOAuth2Errors($e);
                        } catch (ValidationException $e) {
                            ExampleUtils::CheckForOAuth2Errors($e);
                        } catch (Exception $e) {
                            print $e->getMessage() . "\n";
                        }
                    }
                }

                if (count($user_find) > 0 && $user_find['tblp']['dfp_company_id'] == $this->request->data['tblOrder']['advertiser_id']) {
                    $this->request->data['tblOrder']['advertiser_user_id'] = $user_find['User']['id'];
                }

                if (count($user_find) > 0 && $user_find['tblp']['dfp_company_id'] != $this->request->data['tblOrder']['advertiser_id']) {

                    $this->Session->setFlash(__('Error : This email id is already connected'), 'error');
                    $user_find = 0;
                    return;
                }

                if (count($user_find) > 0) {
                    //print_r($this->request->data);
                    $this->request->data['tblOrder']['owner_user_id'] = $this->Authake->getUserId();
                    $this->tblOrder->create();
                    if ($this->tblOrder->save($this->request->data)) {
                        // Get the OrderService.
                        $orderService = $dfpuser->GetOrderService('v201608');

                        // Set the advertiser (company), salesperson, and trafficker to assign to each
                        // order.
                        $advertiserId = $this->request->data['tblOrder']['advertiser_id'];
                        $salespersonId = $this->request->data['tblOrder']['salesmanager_id'];
                        $traffickerId = $this->request->data['tblOrder']['trafficker_id'];

                        // Create an array to store local order objects.
                        $orders = array();

                        $order = new Order();
                        $order->name = $this->request->data['tblOrder']['order_name'] . "_" . $this->tblOrder->getLastInsertID();
                        $order->advertiserId = $advertiserId;
                        $order->salespersonId = $salespersonId;
                        $order->traffickerId = $traffickerId;

                        $orders[] = $order;

                        // Create the orders on the server.
                        $orders = $orderService->createOrders($orders);

                        // Display results.
                        if (isset($orders)) {
                            foreach ($orders as $order) {
                                $orderid = $this->tblOrder->getLastInsertID();
                                $this->tblOrder->updateAll(
                                        array('dfp_order_id' => "$order->id"), array('order_id' => $orderid)
                                );

                                $ad_uid_table = "order_" . $order->id;

                                $sql_table = "CREATE TABLE IF NOT EXISTS " . $ad_uid_table . " (
									`od_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
									`od_lid` int NOT NULL DEFAULT 0,
									`od_cid` int NOT NULL DEFAULT 0,
									`od_ad_id` int NOT NULL DEFAULT 0,
									`od_mailing_id` int DEFAULT 0,
									`od_map_id` char(36) NOT NULL,
									`od_country_name` varchar(200) NOT NULL,
									`od_source` varchar(200) NOT NULL,
									`od_email` varchar(100) NOT NULL,
									`od_phone` varchar(20) NOT NULL,
									`od_fname` varchar(100) DEFAULT NULL,
									`od_lname` varchar(100) DEFAULT NULL,
									`od_site` varchar(100) DEFAULT NULL,
									`device_type` varchar(100) DEFAULT NULL,
									`od_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
									`fp_browser` varchar(200) DEFAULT NULL,
									`fp_connection` varchar(200) DEFAULT NULL,
									`fp_display` varchar(200) DEFAULT NULL,
									`fp_flash` varchar(100) DEFAULT NULL,
									`fp_language` varchar(200) DEFAULT NULL,
									`fp_os` varchar(200) DEFAULT NULL,
									`fp_timezone` varchar(100) DEFAULT NULL,
									`fp_useragent` varchar(500) DEFAULT NULL,
									`od_isDuplicate` tinyint(1) DEFAULT '0',
									`od_isTest` tinyint(1) DEFAULT '0',
									`od_ipaddress` varchar(250) DEFAULT NULL,
									`od_isRejected` tinyint(1) DEFAULT '0',
                                                                        `is_posted` tinyint(2) DEFAULT '0',
                                                                        `request` TEXT  DEFAULT NULL ,
                                                                        `response` TEXT  DEFAULT NULL ,                                                                        
                                                                        `data` TEXT  DEFAULT NULL ,
									PRIMARY KEY (`od_id`)
									) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
                                $this->OrderLeads->query($sql_table);
                                $this->Session->setFlash(__('Your order "' . $order->id . '" was successfully created under Company "' . $order->name . "\" . Let’s add the first line item to this order."), 'success');
                                $this->redirect(array('controller' => 'insertionorder', 'action' => 'add_line_item', 'order_id' => $order->id));
                            }
                        } else {
                            print "No orders created.";
                        }
                    }
                }
            }

            $advertisers = $this->tblProfile->getAllConnectedProfiles();
            //print_r($advertisers);
            // Get the UserService.
            $dfpuserService = $dfpuser->GetService('UserService', 'v201608');

            // Create a statement to select all users.
            $statementBuilder = new StatementBuilder();
            $statementBuilder->OrderBy('id ASC')
                    ->Limit(StatementBuilder::SUGGESTED_PAGE_LIMIT);

            // Default for total result set size.
            $totalResultSetSize = 0;

            do {
                // Get users by statement.
                $page = $dfpuserService->getUsersByStatement(
                        $statementBuilder->ToStatement());
                $trafficker = array();
                $salesmanager = array();
                // Display results.
                if (isset($page->results)) {
                    $totalResultSetSize = $page->totalResultSetSize;
                    $i = 0;
                    $j = 0;
                    foreach ($page->results as $dfpuser) {
                        if ($dfpuser->roleId == "-7" && $dfpuser->isActive == "1") {
                            $trafficker[$i]['email'] = $dfpuser->email;
                            $trafficker[$i]['id'] = $dfpuser->id;
                            $i++;
                        }
                        if ($dfpuser->roleId == "-4" && $dfpuser->isActive == "1") {
                            $salesmanager[$j]['email'] = $dfpuser->email;
                            $salesmanager[$j]['id'] = $dfpuser->id;
                            $j++;
                        }
                    }
                }

                $statementBuilder->IncreaseOffsetBy(StatementBuilder::SUGGESTED_PAGE_LIMIT);
            } while ($statementBuilder->GetOffset() < $totalResultSetSize);

            $this->set('trafficker', $trafficker);
            $this->set('salesmanager', $salesmanager);
            $this->set('advertisers', $advertisers);
            $this->set('company_id', $company_id);
        } catch (OAuth2Exception $e) {
            ExampleUtils::CheckForOAuth2Errors($e);
        } catch (ValidationException $e) {
            ExampleUtils::CheckForOAuth2Errors($e);
        } catch (Exception $e) {
            $this->Session->setFlash(__('Error : "' . $e->getMessage()), 'error');
        }
    }

    public function getcategory_ajax($cat_parent = 0) {

        $this->autoRender = false;
        $this->loadModel('Category');

        $categorylist = $this->Category->GetCategory($cat_parent);
        $subcategories[0]['id'] = "0";
        $subcategories[0]['text'] = "Select (or start typing) the sub category for the line item";
        if (!empty($categorylist)) {
            $count = 1;
            foreach ($categorylist as $key => $value) {

                $subcategories[$count]['id'] = $key;
                $subcategories[$count]['text'] = $value;
                $count++;
            }
        }

        return json_encode($subcategories);
    }

    private function saveLineItemDepart($line_item_id = null) {

        $this->loadModel('LineItemDepart');
        $this->LineItemDepart->deleteAll(array('LineItemDepart.line_item_id' => $line_item_id), false, false);
        if ($this->request->data['tblLineItem']['enable_parting'] == 1) {
            $LineItemDepart = $this->request->data['LineItemDepart'];
            if ($LineItemDepart['week_day']) {
                foreach ($LineItemDepart['week_day'] as $daykey => $dayvalue) {
                    if (isset($LineItemDepart['start_time'][$daykey])) {
                        foreach ($LineItemDepart['start_time'][$daykey] as $timekey => $timevalue) {
                            $savedata = array();
                            if (!empty($LineItemDepart['start_time'][$daykey][$timekey]) and ( !empty($LineItemDepart['start_time'][$daykey][$timekey]))) {

                                $savedata['LineItemDepart'] = array('week_day' => $daykey, 'line_item_id' => $line_item_id, 'start_time' => date('H:i', strtotime($LineItemDepart['start_time'][$daykey][$timekey])), 'end_time' => date('H:i', strtotime($LineItemDepart['end_time'][$daykey][$timekey])), 'created' => date("Y-m-d H:i:s"));
                                //save data
                                $this->LineItemDepart->create();
                                $this->LineItemDepart->save($savedata);
                            }
                        }
                    }
                }
            }
        }
    }

    public function add_line_item() {
        $this->System->add_js(array('jquery.timepicker.js'), 'foot');
        $this->System->add_css(array('jquery.timepicker.css'), 'foot');
        //print_r($this->params);
        $this->loadModel('Authake.AutoResponder');
        $order_id = isset($this->params['named']['order_id']) ? $this->params['named']['order_id'] : "";
        $this->set('title_for_layout', 'Add Line Items');
        // set category list
        $this->loadModel('Category');
        $this->loadModel('LineItemField');
        $categorylist = $this->Category->GetCategory(0);
        $this->set('categorylist', $categorylist);

        $options['joins'] = array(
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblProfile.profileID',
                    'User.id' => array($this->Authake->getUserId()))
        ));

        $options['fields'] = array('tblProfile.Address1', 'tblProfile.Address2', 'tblProfile.City', 'tblProfile.State_Province', 'tblProfile.ZIP_Postal', 'tblProfile.Country');

        $test = $this->tblProfile->find('first', $options);

        //print_r($test);
        $this->set('test', $test);

        if ($this->request->is('post') || $this->request->is('put')) {

            // form posted
            App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
            App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/DateTimeUtils');
            App::import('Vendor', 'src/ExampleUtils');
            try {

                // Get DfpUser from credentials in "../auth.ini"
                // relative to the DfpUser.php file's directory.
                $user = new DfpUser();

                // Log SOAP XML request and response.
                $user->LogDefaults();

                // Get the LineItemService.
                $lineItemService = $user->GetService('LineItemService', 'v201608');


                $orderId = $this->request->data['tblLineItem']['li_order_id'];
                //$placement_id = $this->request->data['tblLineItem']['placement_id'];
                $lineItem_name = $this->request->data['tblLineItem']['li_name'];
                $cost = (!empty($this->request->data['tblLineItem']['li_cs_id'])) ? $this->request->data['tblLineItem']['li_cs_id'] : 0;
                $rate = $this->request->data['tblLineItem']['li_rate'];
                $cost_type = "";


                $quantity = $this->request->data['tblLineItem']['li_quantity'];
                // Set the order that all created line items will belong to and the placement
                // ID to target.
                //$orderId = $orderId;
                // Get the PlacementService.

                $lineItem_increament_id = $this->tblLineItem->nextId();

                $placementName = "AD_PLACEMENT_" . $lineItem_increament_id . '_' . str_replace(" ", "_", $lineItem_name);
                $placementService = $user->GetService('PlacementService', 'v201608');
                $bannerAdUnitPlacement = new Placement();
                $bannerAdUnitPlacement->name = $placementName;
                $bannerAdUnitPlacement->description = $lineItem_name;
                if (isset($this->request->data['adunit-select']))
                    $adunitSelected = $this->request->data['adunit-select'];
                else
                    $adunitSelected = array('386001871');

                foreach ($adunitSelected as $chk1) {
                    $bannerAdUnitPlacement->targetedAdUnitIds[] = $chk1;
                }

                $placementList = array();

                if (count($bannerAdUnitPlacement->targetedAdUnitIds) > 0) {
                    $placementList[] = $bannerAdUnitPlacement;
                }


                $placements = $placementService->createPlacements($placementList);

                if (isset($placements))
                    $targetPlacementIds = array($placements[0]->id);
                else
                    $targetPlacementIds = array('3249271');

                $this->request->data['tblPlacement']['p_name'] = $placementName;
                $this->request->data['tblPlacement']['p_desc'] = $lineItem_name;
                $this->request->data['tblPlacement']['p_dfp_id'] = $placements[0]->id;

                $this->tblPlacement->create();
                $this->tblPlacement->save($this->request->data);

                // Create inventory targeting.
                $inventoryTargeting = new InventoryTargeting();
                $inventoryTargeting->targetedPlacementIds = $targetPlacementIds;


                // Create targeting.
                $targeting = new Targeting();
                $targeting->inventoryTargeting = $inventoryTargeting;


                // Create an array to store local line item objects.

                $lineItem = new LineItem();
                $lineItem->name = $lineItem_name;
                $lineItem->orderId = $orderId;
                $lineItem->targeting = $targeting;
                $lineItem->lineItemType = 'STANDARD';
                $lineItem->allowOverbook = 'TRUE';

                // Create the creative placeholder.
                $creativePlaceholder = new CreativePlaceholder();
                $creativePlaceholder->size = new Size(970, 90, false);

                // Set the size of creatives that can be associated with this line item.
                $lineItem->creativePlaceholders = array($creativePlaceholder);

                // Set the creative rotation type to even.
                $lineItem->creativeRotationType = 'EVEN';

                // Set the length of the line item to run.
                $start_date = $this->Commonfunction->datepickerFormat($this->request->data['tblLineItem']['li_start_date']);
                $end_date = $this->Commonfunction->datepickerFormat($this->request->data['tblLineItem']['li_end_date']);

                // add one day if select same date
                if (strtotime($start_date) == strtotime($end_date)) {
                    $end_date = date('Y-m-d', strtotime($end_date . ' +1 day'));
                }
                $lineItem->startDateTime = DateTimeUtils::ToDfpDateTime(new DateTime($start_date, new DateTimeZone('America/New_York')));
                $lineItem->endDateTime = DateTimeUtils::ToDfpDateTime(new DateTime($end_date, new DateTimeZone('America/New_York')));

                if ($cost != "5") {
                    $lineItem->costType = 'CPC';
                    $lineItem->costPerUnit = new Money('USD', $rate * 1000 * 1000);
                    $goal = new Goal();
                    $goal->units = $quantity;
                    $goal->unitType = 'CLICKS';
                    $goal->goalType = 'LIFETIME';
                    $lineItem->primaryGoal = $goal;
                } else {
                    $lineItem->costType = 'CPM';
                    $lineItem->costPerUnit = new Money('USD', $rate * 1000 * 1000);
                    $goal = new Goal();
                    $goal->units = $quantity;
                    $goal->unitType = 'IMPRESSIONS';
                    $goal->goalType = 'LIFETIME';
                    $lineItem->primaryGoal = $goal;
                }

                $lineItems[] = $lineItem;


                // Create the line items on the server.
                $lineItems = $lineItemService->createLineItems($lineItems);

                // Display results.
                // this line comment after api --- niraj(remove below this line)
                if (isset($lineItems)) {
                    // get here actual taken date
                    $start_date = $this->Commonfunction->edit_datepickerFormat($this->request->data['tblLineItem']['li_start_date']);
                    $end_date = $this->Commonfunction->edit_datepickerFormat($this->request->data['tblLineItem']['li_end_date']);
                    $this->request->data['tblLineItem']['li_run_till_end'] = isset($this->request->data['tblLineItem']['li_run_till_end'])?1:0;     
                    $this->request->data['tblLineItem']['li_auto_renew'] = isset($this->request->data['tblLineItem']['li_auto_renew'])?1:0;     
                    
                    foreach ($lineItems as $lineItem) {
                        $this->request->data['tblLineItem']['li_dfp_id'] = $lineItem->id;
                        $this->request->data['tblLineItem']['li_status_id'] = 2;
                        $this->request->data['tblLineItem']['li_start_date'] = date("Y-m-d H:i:s", strtotime($start_date));
                        $this->request->data['tblLineItem']['li_end_date'] = date("Y-m-d H:i:s", strtotime($end_date));
                        $this->request->data['tblLineItem']['pre_checked']   = isset($this->request->data['tblLineItem']['chekc_all']) && !empty($this->request->data['tblLineItem']['chekc_all']) ? $this->request->data['tblLineItem']['chekc_all'] : 0;
                        $this->tblLineItem->create();
                        if ($this->tblLineItem->save($this->request->data)) {
                            // save lineitem depart by lineitem id
                            self::saveLineItemDepart($this->tblLineItem->id);
                            // save autoresponder data if Enable Auto Responder
                            $lineItemId = $this->tblLineItem->getLastInsertID();
                            if (!empty($this->request->data['AutoResponder']['is_autoresp'])) {
                                $this->request->data['AutoResponder']['li_id'] = $lineItemId;
                                $this->AutoResponder->create();
                                $this->AutoResponder->save($this->request->data);
                                // saving mailing
                                //if(!empty($this->request->data['AutoResponder']['chkARVal'])){
                                $this->request->data['tblMailing']['m_li_crtid'] = $this->AutoResponder->id;
                                $this->request->data['tblMailing']['is_autoresp'] = 1;
                                $this->request->data['tblMailing']['m_sl'] = "Auto Responder - " . $this->request->data['tblLineItem']['li_name'];
                                $this->tblMailing->create();
                                $this->tblMailing->save($this->request->data);
                            }


                            if (isset($this->request->data['productTypes'])) {
                                $productTypes = $this->request->data['productTypes'];
                                $chk = "";
                                foreach ($productTypes as $chk1) {
                                    $this->request->data['amt_p_id'] = $chk1;
                                    $this->request->data['amt_l_id'] = $lineItemId;
                                    $this->tblAllowedMediaType->create();
                                    $this->tblAllowedMediaType->save($this->request->data);
                                }
                            }
                            if (isset($this->request->data['adunit-select'])) {
                                $adunitSelected = $this->request->data['adunit-select'];
                                $chk = "";
                                foreach ($adunitSelected as $chk1) {
                                    $this->request->data['ma_ad_id'] = $chk1;
                                    $this->request->data['ma_l_id'] = $lineItemId;
                                    $this->tblMappedAdunit->create();
                                    $this->tblMappedAdunit->save($this->request->data);
                                }
                            }

                            if (isset($this->request->data['geography-select'])) {
                                $tblAllowedCountries = $this->request->data['geography-select'];
                                $chk = "";
                                foreach ($tblAllowedCountries as $chk1) {
                                    $this->request->data['ac_c_id'] = $chk1;
                                    $this->request->data['ac_m_ad_id'] = $lineItemId;
                                    $this->tblAllowedCountries->create();
                                    $this->tblAllowedCountries->save($this->request->data);
                                }
                            }

                            /// end save line field
                            if ($this->request->data['tblLineItem']['li_cs_id'] == 3 || $this->request->data['tblLineItem']['li_cs_id'] == 7) {

                                if (!empty($this->request->data['LineItemField']['productTypes'])) {
                                    foreach ($this->request->data['LineItemField']['productTypes'] as $value) {
                                        if (!empty($value)) {
                                            $linedata = array();
                                            list($fld_label, $fld_name, $fld_type, $fld_subtype) = explode('@|', $value);
                                            $linedata['LineItemField'] = array('fld_li_id' => $this->tblLineItem->id, 'fld_label' => $fld_label, 'fld_name' => $fld_name, 'fld_type' => $fld_type, 'fld_subtype' => $fld_subtype, 'fld_required' => 'On', 'category_type' => 2);
                                            $this->LineItemField->create();
                                            $this->LineItemField->save($linedata);
                                        }
                                    }
                                }

                                if (!empty($this->request->data['LineItemField']['status'])) {
                                    $i = 1;
                                    foreach ($this->request->data['LineItemField']['status'] as $key => $value) {
                                        if (!empty($this->request->data['LineItemField']['status'])) { // save only who checked
                                            $FieldData = array();
                                            if (!empty($this->request->data['LineItemField']['custom'][$key])) {

                                                $fld_name = "cvalue" . $i++; // $this->Commonfunction->removespecail_text($this->request->data['LineItemField']['fld_label'][$key]);
                                                $FieldData['LineItemField'] = array(
                                                    'fld_li_id' => $this->tblLineItem->id,
                                                    'fld_label' => $this->request->data['LineItemField']['fld_label'][$key],
                                                    //'fld_label'=>$this->request->data['FieldMapping']['custom'][$key],
                                                    'fld_name' => $fld_name,
                                                    'custom' => 1,
                                                    'required' => !empty($this->request->data['LineItemField']['required'][$key]) ? 1 : 0,
                                                    'category_type' => 1, //
                                                    'field_format' => (!empty($this->request->data['LineItemField']['field_format'][$key])) ? $this->request->data['LineItemField']['field_format'][$key] : '',
                                                    'fld_custom_texts' => (!empty($this->request->data['LineItemField']['fld_type'][$key])) ? (in_array($this->request->data['LineItemField']['fld_type'][$key], array(2, 3, 5, 4, 6))) ? $this->request->data['LineItemField']['fld_custom_texts'][$key] : '' : '',
                                                    'fld_type' => $this->request->data['LineItemField']['fld_type'][$key],
                                                    'adv_fld_name' => $this->request->data['LineItemField']['adv_fld_name'][$key]);
                                            } else {
                                                $FieldData['LineItemField'] = array(
                                                    'fld_li_id' => $this->tblLineItem->id,
                                                    'fld_label' => $this->request->data['LineItemField']['fld_label'][$key],
                                                    'fld_name' => !empty($this->request->data['LineItemField']['fld_name'][$key]) ? $this->request->data['LineItemField']['fld_name'][$key] : "",
                                                    'fld_type' => !empty($this->request->data['LineItemField']['fld_type'][$key]) ? $this->request->data['LineItemField']['fld_type'][$key] : "",
                                                    'custom' => 0,
                                                    'category_type' => 1, // 
                                                    'required' => !empty($this->request->data['LineItemField']['required'][$key]) ? 1 : 0,
                                                    'field_format' => (!empty($this->request->data['LineItemField']['field_format'][$key])) ? $this->request->data['LineItemField']['field_format'][$key] : '',
                                                    'fld_custom_texts' => (!empty($this->request->data['LineItemField']['fld_custom_texts'][$key])) ? $this->request->data['LineItemField']['fld_custom_texts'][$key] : '',
                                                    'adv_fld_name' => $this->request->data['LineItemField']['adv_fld_name'][$key]);
                                            }


                                            $this->LineItemField->create();
                                            $this->LineItemField->save($FieldData);
                                        }
                                    }
                                }
                            }

                            $this->Session->setFlash(__('An LineItem with ID ' . $lineItemId . ' was created. <a href="/insertionorder/add_line_item/" id="sample_editable_1_new">Add Another Line Item</a> / <a href="/orders/view_line_creatives/' . $lineItemId . '" id="sample_editable_1_new">Add Creative to this Line Item</a>'), 'success');
                            //$this->redirect(array('action' => 'edit_line_item', $lineItemId));
                            $this->redirect(array('controller' => 'orders', 'action' => 'view_line_items', $orderId));
                            // save line field
                        }
                        //$this->redirect(array('action'=>'index'));
                    }
                } else {
                    printf("No line items created.");
                }
            } catch (OAuth2Exception $e) {
                ExampleUtils::CheckForOAuth2Errors($e);
            } catch (ValidationException $e) {
                ExampleUtils::CheckForOAuth2Errors($e);
            } catch (Exception $e) {
                printf("%s\n", $e->getMessage());
            }
        }
        $this->form();
        $this->set('timezones', $this->Timezone->getList());
        $this->set('order_id', $order_id);
        //$order_name = $this->tblOrder->field('order_name', array('dfp_order_id' =>$order_id));
    }

    public function edit_line_item($lineItemId = NULL) {


        $this->System->add_js(array('jquery.timepicker.js'), 'foot');
        $this->System->add_css(array('jquery.timepicker.css'), 'foot');
        $this->loadModel('Authake.AutoResponder');
        $this->loadModel('LineItemField');
        $this->loadModel('LineItemDepart');


        if ($this->request->is('post') || $this->request->is('put')) {
            // form posted
            //pr($this->request->data);
            App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
            App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
            App::import('Vendor', 'src/ExampleUtils');
            $lineItemId = $this->request->data['tblLineItem']['li_id'];
            $lineItemDFPID = $this->tblLineItem->find('first', array('conditions' => array('li_id' => $lineItemId), 'fields' => array('li_dfp_id', 'li_order_id')));
            try {

                // Get DfpUser from credentials in "../auth.ini"
                // relative to the DfpUser.php file's directory.
                $user = new DfpUser();

                // Log SOAP XML request and response.
                $user->LogDefaults();

                // Get the LineItemService.
                $lineItemService = $user->GetService('LineItemService', 'v201608');

                // Create a statement to select a single line item by ID.
                $statementBuilder = new StatementBuilder();
                $statementBuilder->Where('id = :id')
                        ->OrderBy('id ASC')
                        ->Limit(1)
                        ->WithBindVariableValue('id', $lineItemDFPID['tblLineItem']['li_dfp_id']);

                $page = $lineItemService->getLineItemsByStatement(
                        $statementBuilder->ToStatement());
                $lineItem = $page->results[0];


                if (isset($lineItem->lineItemType)) {

                    $lineItem_name = $this->request->data['tblLineItem']['li_name'];
                    $cost = $this->request->data['tblLineItem']['li_cs_id'];
                    $rate = $this->request->data['tblLineItem']['li_rate'];
                    $cost_type = "";


                    $quantity = $this->request->data['tblLineItem']['li_quantity'];
                    // Set the order that all created line items will belong to and the placement
                    // ID to target.
                    //$orderId = $orderId;
                    // Get the PlacementService.
                    // Create an array to store local line item objects.

                    $lineItem->name = $lineItem_name;

                    // Set the length of the line item to run.

                    $start_date = $this->Commonfunction->datepickerFormat($this->request->data['tblLineItem']['li_start_date']);

                    $end_date = $this->Commonfunction->datepickerFormat($this->request->data['tblLineItem']['li_end_date']);

                    if (strtotime($start_date) == strtotime($end_date)) {
                        $end_date = date('Y-m-d', strtotime($end_date . ' +1 day'));
                    }

                    $lineItem->startDateTime = DateTimeUtils::ToDfpDateTime(new DateTime($start_date, new DateTimeZone('America/New_York')));
                    $lineItem->endDateTime = DateTimeUtils::ToDfpDateTime(new DateTime($end_date, new DateTimeZone('America/New_York')));

                    if ($cost != "5") {
                        $lineItem->costType = 'CPC';
                        $lineItem->costPerUnit = new Money('USD', $rate * 1000 * 1000);
                        $goal = new Goal();
                        $goal->units = $quantity;
                        $goal->unitType = 'CLICKS';
                        $goal->goalType = 'LIFETIME';
                        $lineItem->primaryGoal = $goal;
                    } else {
                        $lineItem->costType = 'CPM';
                        $lineItem->costPerUnit = new Money('USD', $rate * 1000 * 1000);
                        $goal = new Goal();
                        $goal->units = $quantity;
                        $goal->unitType = 'IMPRESSIONS';
                        $goal->goalType = 'LIFETIME';
                        $lineItem->primaryGoal = $goal;
                    }
                     
                    // Create the line items on the server.
                    $lineItems = $lineItemService->updateLineItems($lineItem);
                    $this->request->data['tblLineItem']['li_run_till_end'] = isset($this->request->data['tblLineItem']['li_run_till_end'])?1:0;        
                    $this->request->data['tblLineItem']['li_auto_renew'] = isset($this->request->data['tblLineItem']['li_auto_renew'])?1:0;
                    //$this->request->data['tblLineItem']['pre_checked']   = $this->request->data['tblLineItem']['chekc_all'];
                    
                   // pr($this->request->data);exit;

                    // Display results.
                    foreach ($lineItems as $updatedLineItem) {
                        $this->request->data['tblLineItem']['li_start_date'] = date("Y-m-d H:i:s", strtotime($start_date));
                        $this->request->data['tblLineItem']['li_end_date']   = date("Y-m-d H:i:s", strtotime($end_date));
                        $this->request->data['tblLineItem']['pre_checked']   = isset($this->request->data['tblLineItem']['chekc_all']) && !empty($this->request->data['tblLineItem']['chekc_all']) ? $this->request->data['tblLineItem']['chekc_all'] : 0;
                        $lineItemId = $this->request->data['tblLineItem']['li_id'];
                        $this->tblLineItem->create();
                        if ($this->tblLineItem->save($this->request->data)) {
                            // save lineitem depart
                            self::saveLineItemDepart($this->tblLineItem->id);
                            //save autoresponder 
                            $autorender = $this->AutoResponder->find('first', array('conditions' => array('AutoResponder.li_id' => $lineItemId)));

                            if (!empty($autorender)) {

                                $mailing = $this->tblMailing->find('first', array('conditions' => array('tblMailing.m_li_crtid' => $autorender['AutoResponder']['id'], 'tblMailing.is_autoresp' => 1)));
                                // if not not is_autoresp then delete 

                                $this->AutoResponder->deleteAll(array('AutoResponder.li_id' => $lineItemId), false, false);
                                if (!empty($autorender))
                                    $this->tblMailing->deleteAll(array('tblMailing.m_li_crtid' => $autorender['AutoResponder']['id'], 'tblMailing.is_autoresp' => 1), false, false);
                            }

                            if (!empty($this->request->data['AutoResponder']['is_autoresp'])) {

                                $this->request->data['AutoResponder']['li_id'] = $lineItemId;
                                $this->AutoResponder->create();
                                $this->AutoResponder->save($this->request->data);
                                // saving mailing
                                //if(!empty($this->request->data['AutoResponder']['chkARVal'])){
                                $this->request->data['tblMailing']['m_li_crtid'] = $this->AutoResponder->id;
                                $this->request->data['tblMailing']['is_autoresp'] = 1;
                                $this->request->data['tblMailing']['m_sl'] = "Auto Responder - " . $this->request->data['tblLineItem']['li_name'];
                                $this->tblMailing->create();
                                $this->tblMailing->save($this->request->data);

                                //}
                                // ending saving mail 
                            }
                            $this->tblAllowedMediaType->updateAll(
                                    array('tblAllowedMediaType.amt_isActive' => 0), array('tblAllowedMediaType.amt_l_id' => $lineItemId)
                            );

                            $allowedMediaTypes = $this->tblAllowedMediaType->find('list', array(
                                'fields' => array('tblAllowedMediaType.amt_id', 'tblAllowedMediaType.amt_p_id'),
                                'conditions' => array('tblAllowedMediaType.amt_l_id' => $lineItemId),
                                'recursive' => 0
                            ));

                            //saving line item field
                            if ($this->request->data['tblLineItem']['li_cs_id'] == 3 || $this->request->data['tblLineItem']['li_cs_id'] == 7) {
                                // delete all 

                                $this->LineItemField->deleteAll(array('LineItemField.fld_li_id' => $this->tblLineItem->id), false, false);
                                if (!empty($this->request->data['LineItemField']['productTypes'])) {
                                    foreach ($this->request->data['LineItemField']['productTypes'] as $value) {
                                        if (!empty($value)) {
                                            $linedata = array();
                                            list($fld_label, $fld_name, $fld_type, $fld_subtype) = explode('@|', $value);
                                            $linedata['LineItemField'] = array('fld_li_id' => $this->tblLineItem->id,
                                                'fld_label' => $fld_label,
                                                'fld_name' => $fld_name,
                                                'fld_type' => $fld_type,
                                                'fld_subtype' => $fld_subtype,
                                                'fld_required' => 'On',
                                                'category_type' => 2);
                                            $this->LineItemField->create();
                                            $this->LineItemField->save($linedata);
                                        }
                                    }
                                }
                                $i = 1;
                                if (!empty($this->request->data['LineItemField']['status'])) {
                                    foreach ($this->request->data['LineItemField']['status'] as $key => $value) {
                                        if (!empty($this->request->data['LineItemField']['status'])) { // save only who checked
                                            $FieldData = array();
                                            if (!empty($this->request->data['LineItemField']['custom'][$key])) {
                                                $fld_name = "cvalue" . $i++;
                                                //$fld_name = $this->Commonfunction->removespecail_text($this->request->data['LineItemField']['fld_label'][$key]);
                                                $FieldData['LineItemField'] = array(
                                                    'fld_li_id' => $this->tblLineItem->id,
                                                    'fld_label' => $this->request->data['LineItemField']['fld_label'][$key],
                                                    //'fld_label'=>$this->request->data['FieldMapping']['custom'][$key],
                                                    'fld_name' => $fld_name,
                                                    'custom' => 1,
                                                    'category_type' => 1, // 
                                                    'required' => !empty($this->request->data['LineItemField']['required'][$key]) ? 1 : 0,
                                                    'field_format' => (!empty($this->request->data['LineItemField']['field_format'][$key])) ? $this->request->data['LineItemField']['field_format'][$key] : '',
                                                    'fld_custom_texts' => (!empty($this->request->data['LineItemField']['fld_type'][$key])) ? (in_array($this->request->data['LineItemField']['fld_type'][$key], array(2, 3, 5, 4, 6))) ? $this->request->data['LineItemField']['fld_custom_texts'][$key] : '' : '',
                                                    'fld_type' => $this->request->data['LineItemField']['fld_type'][$key],
                                                    'adv_fld_name' => $this->request->data['LineItemField']['adv_fld_name'][$key]);
                                            } else {
                                                $FieldData['LineItemField'] = array(
                                                    'fld_li_id' => $this->tblLineItem->id,
                                                    'fld_label' => $this->request->data['LineItemField']['fld_label'][$key],
                                                    'fld_name' => !empty($this->request->data['LineItemField']['fld_name'][$key]) ? $this->request->data['LineItemField']['fld_name'][$key] : "",
                                                    'fld_type' => !empty($this->request->data['LineItemField']['fld_type'][$key]) ? $this->request->data['LineItemField']['fld_type'][$key] : "",
                                                    'custom' => 0,
                                                    'category_type' => 1, // 
                                                    'required' => !empty($this->request->data['LineItemField']['required'][$key]) ? 1 : 0,
                                                    'fld_custom_texts' => (!empty($this->request->data['LineItemField']['fld_custom_texts'][$key])) ? $this->request->data['LineItemField']['fld_custom_texts'][$key] : '',
                                                    'field_format' => (!empty($this->request->data['LineItemField']['field_format'][$key])) ? $this->request->data['LineItemField']['field_format'][$key] : '',
                                                    'adv_fld_name' => $this->request->data['LineItemField']['adv_fld_name'][$key]);
                                            }

                                            $this->LineItemField->create();
                                            $this->LineItemField->save($FieldData);
                                        }
                                    }
                                }
                            } else {
                                $this->LineItemField->deleteAll(array('LineItemField.fld_li_id' => $this->tblLineItem->id), false, false);
                            }




                            if (isset($this->request->data['productTypes'])) {
                                $productTypes = $this->request->data['productTypes'];
                                $chk = "";
                                foreach ($productTypes as $chk1) {
                                    if (in_array($chk1, $allowedMediaTypes)) {
                                        $this->tblAllowedMediaType->updateAll(
                                                array('tblAllowedMediaType.amt_isActive' => 1), array('tblAllowedMediaType.amt_p_id' => $chk1, 'tblAllowedMediaType.amt_l_id' => $lineItemId)
                                        );
                                    } else {
                                        $this->request->data['amt_p_id'] = $chk1;
                                        $this->request->data['amt_l_id'] = $lineItemId;
                                        $this->tblAllowedMediaType->create();
                                        $this->tblAllowedMediaType->save($this->request->data);
                                    }
                                }
                            }
                            $this->tblMappedAdunit->updateAll(
                                    array('tblMappedAdunit.ma_isActive' => 0), array('tblMappedAdunit.ma_l_id' => $lineItemId)
                            );

                            $mappedAdUnits = $this->tblMappedAdunit->find('list', array(
                                'fields' => array('tblMappedAdunit.ma_id', 'tblMappedAdunit.ma_ad_id'),
                                'conditions' => array('tblMappedAdunit.ma_l_id' => $lineItemId),
                                'recursive' => 0
                            ));

                            if (isset($this->request->data['adunit-select'])) {
                                $adunitSelected = $this->request->data['adunit-select'];
                                $chk = "";
                                foreach ($adunitSelected as $chk1) {
                                    if (in_array($chk1, $mappedAdUnits)) {
                                        $this->tblMappedAdunit->updateAll(
                                                array('tblMappedAdunit.ma_isActive' => 1), array('tblMappedAdunit.ma_ad_id' => $chk1, 'tblMappedAdunit.ma_l_id' => $lineItemId)
                                        );
                                    } else {
                                        $this->request->data['ma_ad_id'] = $chk1;
                                        $this->request->data['ma_l_id'] = $lineItemId;
                                        $this->tblMappedAdunit->create();
                                        $this->tblMappedAdunit->save($this->request->data);
                                    }
                                }
                            }

                            $this->tblAllowedCountries->updateAll(
                                    array('tblAllowedCountries.ac_isActive' => 0), array('tblAllowedCountries.ac_m_ad_id' => $lineItemId)
                            );

                            $allowedCountries = $this->tblAllowedCountries->find('list', array(
                                'fields' => array('tblAllowedCountries.ac_id', 'tblAllowedCountries.ac_c_id'),
                                'conditions' => array('tblAllowedCountries.ac_m_ad_id' => $lineItemId),
                                'recursive' => 0
                            ));

                            if (isset($this->request->data['geography-select'])) {
                                $tblAllowedCountries = $this->request->data['geography-select'];
                                $chk = "";
                                foreach ($tblAllowedCountries as $chk1) {
                                    if (in_array($chk1, $allowedCountries)) {
                                        $this->tblAllowedCountries->updateAll(
                                                array('tblAllowedCountries.ac_isActive' => 1), array('tblAllowedCountries.ac_c_id' => $chk1, 'tblAllowedCountries.ac_m_ad_id' => $lineItemId)
                                        );
                                    } else {
                                        $this->request->data['ac_c_id'] = $chk1;
                                        $this->request->data['ac_m_ad_id'] = $lineItemId;
                                        $this->tblAllowedCountries->create();
                                        $this->tblAllowedCountries->save($this->request->data);
                                    }
                                }
                            }
                            $this->Session->setFlash(__('An LineItem "' . $updatedLineItem->name . "\" has been updated."), 'success');
                            $this->redirect(array('controller' => 'orders', 'action' => 'view_line_items', $lineItemDFPID['tblLineItem']['li_order_id']));
                        }
                        //$this->redirect(array('action'=>'index'));
                    }
                } else {
                    printf('No line items were updated.');
                }
            } catch (OAuth2Exception $e) {
                ExampleUtils::CheckForOAuth2Errors($e);
            } catch (ValidationException $e) {
                ExampleUtils::CheckForOAuth2Errors($e);
            } catch (Exception $e) {

                $this->Session->setFlash(__($e->getMessage()), 'error');
            }
            $this->redirect(array('action' => 'edit_line_item', $lineItemId));
        } //end of saving post data 

        if (!is_null($lineItemId)) {


            $options = array();
            $options['joins'] = array(
                array('table' => 'authake_users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => array(
                        'User.tblProfileID = tblProfile.profileID',
                        'User.id' => array($this->Authake->getUserId()))
            ));

            $options['fields'] = array('tblProfile.Address1', 'tblProfile.Address2', 'tblProfile.City', 'tblProfile.State_Province', 'tblProfile.ZIP_Postal', 'tblProfile.Country');

            $test = $this->tblProfile->find('first', $options);

            //print_r($test);
            $this->set('test', $test);

            $options = array();
            $options['conditions'] = array('tblLineItem.li_id' => $lineItemId);

            $options['joins'] = array(
                array('table' => 'tbl_orders',
                    'alias' => 'tblOrder',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tblOrder.dfp_order_id = tblLineItem.li_order_id')
                ),
                array('table' => 'tbl_profiles',
                    'alias' => 'tblp',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tblOrder.advertiser_id = tblp.dfp_company_id')
                ),
                array('table' => 'tbl_auto_responders',
                    'alias' => 'AutoResponder',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'tblLineItem.li_id = AutoResponder.li_id')
                )
            );

            $options['fields'] = array('tblLineItem.*', 'AutoResponder.*', 'tblOrder.order_name', 'tblp.user_id');

            $this->request->data = $this->tblLineItem->find('first', $options);
            //pr($this->request->data);exit;
            $phpStartdate = strtotime($this->data['tblLineItem']['li_start_date']);
            $this->request->data['tblLineItem']['li_start_date'] = date('m/d/Y', $phpStartdate);
            $phpEnddate = strtotime($this->data['tblLineItem']['li_end_date']);
            $this->request->data['tblLineItem']['li_end_date'] = date('m/d/Y', $phpEnddate);
            $LineItemDeparts = $this->LineItemDepart->getAllLineItemDepart($lineItemId);
            $this->set('LineItemDeparts', $LineItemDeparts);
            $this->set('prechecked', $this->request->data['tblLineItem']['pre_checked']);  /** prechecked value set / By - Satanik **/
            $optionm['conditions'] = array('tblMappedAdunit.ma_l_id' => $lineItemId, 'tblMappedAdunit.ma_isActive' => '1');
            $optionm['joins'] = array(
                array('table' => 'tbl_adunits',
                    'alias' => 'tblAdunit',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ));
            $optionm['fields'] = array('tblMappedAdunit.ma_ad_id', 'tblAdunit.adunit_name');
            $mappedUnits = $this->tblMappedAdunit->find('all', $optionm);
            $this->set('mappedUnits', $mappedUnits);
            //print_r($mappedUnits);


            $allowedMediaTypes = $this->tblAllowedMediaType->find('list', array(
                'fields' => array('tblAllowedMediaType.amt_id', 'tblAllowedMediaType.amt_p_id'),
                'conditions' => array('tblAllowedMediaType.amt_l_id' => $lineItemId, 'tblAllowedMediaType.amt_isActive' => '1'),
                'recursive' => 0
            ));
            $this->set('allowedMediaTypes', $allowedMediaTypes);


            $allowedCountries = $this->tblAllowedCountries->find('list', array(
                'fields' => array('tblAllowedCountries.ac_id', 'tblAllowedCountries.ac_c_id'),
                'conditions' => array('tblAllowedCountries.ac_m_ad_id' => $lineItemId, 'tblAllowedCountries.ac_isActive' => 1),
                'recursive' => 0
            ));
            $this->set('allowedCountries', $allowedCountries);
            $this->set('lineItemId', $lineItemId);
            $this->form();
        }
        $lineItemFields = array();
        if ($this->request->data['tblLineItem']['li_cs_id'] == 3 || $this->request->data['tblLineItem']['li_cs_id'] == 7) {
            $lineItemFields = $this->LineItemField->find('all', array('conditions' => array('LineItemField.fld_li_id' => $lineItemId)));
        }

        $this->set('lineItemFields', $lineItemFields);

        $this->loadModel('Category');

        $categorylist = $this->Category->GetCategory(0);
        $this->set('categorylist', $categorylist);

        $category_id = (!empty($this->request->data['tblLineItem']['li_category_id'])) ? $this->request->data['tblLineItem']['li_category_id'] : 0;
        $subcategory = $this->Category->GetCategory($category_id);
        $this->set('timezones', $this->Timezone->getList());
        $this->set('subcategory', $subcategory);
        $this->render('edit_line_item');
        //pr($lineItemFields);die;
    }

    public function form() {

        $advertisers = $this->tblProfile->getAllConnectedProfiles();
        $this->set('advertisers', $advertisers);


        //print_r($user_company_id);

        $option_order['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($this->Authake->getUserId()))
        ));
        $option_order['fields'] = array('tblOrder.*', 'tblp.CompanyName', 'tblp.user_id');
        $orders = $this->tblOrder->find('all', $option_order);
        //print_r($orders);
        $company_name = array();
        $count = 0;
        foreach ($orders as $order) {
            //$company_name .=  "{ id: ".$order['tblp']['user_id'].", text: '".$order['tblp']['CompanyName']."' },";
            if (!$this->checkCompanyExist($company_name, 'user_id', $order['tblp']['user_id'])) {
                $company_name[$count]['user_id'] = $order['tblp']['user_id'];
                $company_name[$count]['CompanyName'] = $order['tblp']['CompanyName'];
                $count++;
            }
        }



        //$company_name = rtrim($company_name, ',');
        $this->set('orders', $orders);
        $this->set('company_name', $company_name);

        $optionsd['conditions'] = array(
            'tblProducttype.ptype_m_id' => '1');
        $desktop = $this->tblProducttype->find('all', $optionsd);
        $this->set('desktop', $desktop);

        $optionse['conditions'] = array(
            'tblProducttype.ptype_m_id' => '2');
        $email = $this->tblProducttype->find('all', $optionse);
        $this->set('email', $email);

        $optionsm['conditions'] = array(
            'tblProducttype.ptype_m_id' => '3');
        $mobile = $this->tblProducttype->find('all', $optionsm);
        $this->set('mobile', $mobile);

        $optionss['conditions'] = array(
            'tblProducttype.ptype_m_id' => '4');
        $social = $this->tblProducttype->find('all', $optionss);
        $this->set('social', $social);

        $optionsl['conditions'] = array(
            'tblProducttype.ptype_m_id' => '5');
        $leadGen = $this->tblProducttype->find('all', $optionsl);
        $this->set('leadGen', $leadGen);

        $costStructure = $this->tblCostStructure->find('all', array('order' => array('tblCostStructure.cs_name' => 'asc')));
        $this->set('costStructure', $costStructure);

        $tblCountries = $this->tblCountries->find('all');
        $this->set('tblCountries', $tblCountries);

        $audience_segments = $this->audience_segments->find('all');
        $this->set('audience_segments', $audience_segments);
    }

    public function getOrderByAdvertiser() {
        $this->autoRender = false;
        $advertiser_id = $this->request->data['advertiser_id'];

        $option_order['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($this->Authake->getUserId()),
                    'tblp.user_id' => $advertiser_id)
        ));
        $option_order['fields'] = array('tblOrder.*', 'tblp.CompanyName', 'tblp.user_id');
        $orders = $this->tblOrder->find('all', $option_order);
        $orders_array = array();
        $count = 0;
        foreach ($orders as $order) {
            $orders_array[$count]['id'] = $order['tblOrder']['dfp_order_id'];
            $orders_array[$count]['text'] = $order['tblOrder']['order_name'];
            $count++;
        }
        return json_encode($orders_array);
    }

    public function getAdUnitsByMediaType() {
        $this->autoRender = false;
        $media_id = $this->request->data['media_id'];
        $my_media_id = explode(',', $media_id);
        $ids = $this->tblProfile->getAllConnectedProfileUserIds();

        if (count($ids) > 1) {
            $optionsl['conditions'] = array(
                'or' => array(
                    'tblAdunit.owner_user_id' => array($this->Authake->getUserId()),
                    'and' => array(
                        'tblAdunit.owner_user_id in' => $ids,
                        'tblAdunit.ad_isPublicVisible' => 1))
            );
        } else {
            $optionsl['conditions'] = array(
                'tblAdunit.owner_user_id' => array($this->Authake->getUserId()),
            );
        }

        $optionsl['joins'] = array(
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id')),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID')),
            array('table' => 'tbl_product_types',
                'alias' => 'tbpt',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_ptype_id = tbpt.ptype_id',
                    'tblAdunit.ad_ptype_id' => $my_media_id
                )),
            array('table' => 'tbl_media_types',
                'alias' => 'tbmt',
                'type' => 'INNER',
                'conditions' => array(
                    'tbmt.mtype_id = tbpt.ptype_m_id'
        )));

        $optionsl['group'] = array('tblAdunit.ad_dfp_id');
        $optionsl['fields'] = array('tblAdunit.*', 'tblp.CompanyName', 'tbpt.ptype_name', 'tbmt.mtype_name');

        $adunits = $this->tblAdunit->find('all', $optionsl);
        $orders_array = array();
        $count = 0;
        foreach ($adunits as $adunit) {
            if ($adunit['tblAdunit']['ad_dfp_id'] != "") {
                $orders_array[$count]['id'] = $adunit['tblAdunit']['ad_dfp_id'];
                $orders_array[$count]['text'] = $adunit['tblp']['CompanyName'] . " => " . $adunit['tblAdunit']['adunit_name'];
                $count++;
            }
        }
        return json_encode($orders_array);
    }

    public function getCreativeByMediaType() {
        $this->autoRender = false;
        $media_id = (!empty($this->request->data['media_id'])) ? $this->request->data['media_id'] : null;
        $cr_lid = (!empty($this->request->data['li_dfp_id'])) ? $this->request->data['li_dfp_id'] : null;
        $creatives_type_id = (in_array($media_id, array('2', '3', '6', '12', '13'))) ? $media_id : 55; // if selected 55 for image creative type
        if ($creatives_type_id == 2) {
            $creatives_type_id = array(2, 3);
        }
        $optionsl['conditions'] = array(
            'tblCreative.cr_status' => 'Active',
            'tblCreative.creatives_type' => $creatives_type_id,
            'tblCreative.cr_lid' => $cr_lid
        );
        $optionsl['joins'] = array(
            array('table' => 'tbl_creatives_types',
                'alias' => 'creativesTypes',
                'type' => 'INNER',
                'conditions' => array(
                    'tblCreative.creatives_type = creativesTypes.id')),
        );

        $optionsl['fields'] = array('tblCreative.*', 'creativesTypes.type');

        $creative_data = $this->tblCreative->find('all', $optionsl);

        $creative_arraylist = array();
        $count = 0;
        if (!empty($creative_data)) {
            foreach ($creative_data as $data) {

                $creative_arraylist[$count]['id'] = $data['tblCreative']['cr_id'];
                if ($data['tblCreative']['creatives_type'] == 55) {
                    $creative_arraylist[$count]['text'] = "Image banner => " . $data['tblCreative']['cr_size'];
                } else {
                    $creative_arraylist[$count]['text'] = $data['creativesTypes']['type'] . " => " . $data['tblCreative']['cr_header'];
                }
                $count++;
            }
        }
        return json_encode($creative_arraylist);
    }

    public function getCompanyRevShare($comapnyid = null) {
        $this->autoRender = false;

        $optionsl['conditions']['AND'] = array('tblAdunit.ad_dfp_id' => $comapnyid);


        $optionsl['joins'] = array(
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id')),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID')));

        $optionsl['fields'] = array('tblp.rev_share');
        $rev_share = $this->tblAdunit->find('first', $optionsl);
        echo (!empty($rev_share)) ? $rev_share['tblp']['rev_share'] : 0;
    }

    /**
     * Add New Mapped Adunit on Flight Edit page
     * @param type $adunit_id
     * @param type $line_item_id
     * @param type $flight_id
     * @param type $rev_share
     * @return type
     */
    function addMappedAddunit($adunit_id, $line_item_id, $flight_id, $rev_share) {
        $this->autoLayout = false;
        $this->autoRender = false;
        $data['tblMappedAdunit'] = array(
            'ma_l_id' => $line_item_id,
            'ma_ad_id' => $adunit_id,
            'ma_fl_id' => $flight_id,
            'rev_share' => $rev_share
        );
        $this->tblMappedAdunit->create();
        $this->tblMappedAdunit->save($data);
        return $this->tblMappedAdunit->id;
    }

    /**
     * Delete mapped Adunit on flight edit page
     * @param type $adunit_id
     * @param type $line_item_id
     * @param type $flight_id
     */
    function deleteMappedAddunit($adunit_id, $line_item_id, $flight_id) {
        $this->autoLayout = false;
        $this->autoRender = false;
        $conditions = array(
            'ma_l_id' => $line_item_id,
            'ma_ad_id' => $adunit_id,
            'ma_fl_id' => $flight_id
        );
        $mappedAdunit = $this->tblMappedAdunit->find('first', array('conditions' => $conditions));
        if (!empty($mappedAdunit)) {
            $this->tblMappedAdunit->primaryKey = 'ma_id';
            $this->tblMappedAdunit->delete($mappedAdunit['tblMappedAdunit']['ma_id']);
        }
    }

    function checkCompanyExist($array, $key, $val) {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val)
                return true;
        return false;
    }

    function getContactInfoCompany() {

        $this->autoRender = false;
        $company_id = $user_id = null;
        if (!empty($this->request->data['company_id'])) {
            $company = explode("@", $this->request->data['company_id']);
            $company_id = $company[0];
            $user_id = $company[1];
        }

        $option_order['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.ProfileID')
        ));

        $option_order['fields'] = array('User.*', 'tblp.*');

        $option_order['conditions'] = array('tblp.dfp_company_id' => $company_id, 'User.id' => $user_id);

        $user_find = $this->User->find('first', $option_order);
        $orders_array['name'] = $user_find['User']['ContactName'];
        $orders_array['email'] = $user_find['User']['email'];

        return json_encode($orders_array);
    }

    public function upload_creative($lineitem, $size) {
        $line_name = $this->tblLineItem->field('li_name', array('li_id' => $lineitem));
        if (!empty($size)) {
            $size = base64_decode($size);
        }
        $this->set('line_name', $line_name);
        $line_dfp_id = $this->tblLineItem->field('li_order_id', array('li_id' => $lineitem));
        $this->set('line_dfp_id', $line_dfp_id);
        $this->set('size', $size);
        $this->set('lineitem', $lineitem);
        $this->set('size', $size);
    }

    public function upload_creative_text_html($lineitem = null, $size = null) {
        //$this->System->add_js(array('/plugins/ckeditor/vendor/ckeditor/ckeditor.js'), 'head');
        $this->System->add_css(array('/plugins/summernote/summernote.css'), 'head');
        $this->System->add_js(array('/plugins/summernote/summernote.min.js'), 'head');
        if (!empty($this->request->data['tblCreative']['creatives_type'])) {
            /// end of file upload 
            $image_name = '';

            if ($this->request->data['tblCreative']['cr_banner_name']['error'] == 0) {
                $image_name = self::_manage_image($this->request->data['tblCreative']['cr_banner_name']);
            }

            // creative type emil dedicate link

            $links = $this->request->data['tblCreative']['link'];
            $this->request->data['tblCreative']['link'] = json_encode($links);
            if (!empty($links) and $this->request->data['tblCreative']['creatives_type'] == 3) {
                $body_content = '';
                $body_content .='<ol>';
                foreach ($links as $link) {
                    $body_content .='<li><a href="' . $link . '">' . $link . ' </a></li>';
                }
                $body_content .='<ol>';
                $this->request->data['tblCreative']['cr_body'] = $body_content;
            }

            $this->request->data['tblCreative']['cr_banner_name'] = $image_name;
            if (empty($this->request->data['tblCreative']['cr_name'])) {
                $this->request->data['tblCreative']['cr_name'] = $this->request->data['tblCreative']['cr_header'];
            } else {
                $this->request->data['tblCreative']['cr_header'] = $this->request->data['tblCreative']['cr_name'];
            }
            if(empty($this->request->data['tblCreative']['cr_redirect_url']))       
            {
                 $this->Session->setFlash(__('Please Enter Redirect Url'), 'danger');
            }else
            {
                 $this->tblCreative->create();      
                 if ($this->tblCreative->save($this->request->data)) {      
                    $this->Session->setFlash(__("The Creative has been uploaded. <a href='/insertionorder/add_line_item_flight/" . $lineitem . "'>Add a Flight</a>"), 'success');       
                    $this->redirect(array('controller' => 'orders', 'action' => 'view_line_creatives', $lineitem));     
                 }       
            }
        }

        if (!empty($size)) {
            $size = base64_decode($size);
        }
        $creatives_type_id = '';
        if (!empty($this->request->data['tblOrder']['creatives_type'])) {
            $creatives_type_id = $this->request->data['tblOrder']['creatives_type'];
        }
        $this->loadModel('CreativesType');
        $creatives_type = $this->CreativesType->GetCreativeType();
        $line_name = $this->tblLineItem->field('li_name', array('li_id' => $lineitem));
        $this->set('line_name', $line_name);
        $line_order_id = $this->tblLineItem->field('li_order_id', array('li_id' => $lineitem));

        $this->set('line_order_id', $line_order_id);
        $line_dfp_id = $this->tblLineItem->field('li_dfp_id', array('li_id' => $lineitem));
        $this->set('line_dfp_id', $line_dfp_id);
        $this->set('size', $size);
        $this->set('lineitem', $lineitem);
        $this->set('size', $size);
        $this->set(compact('creatives_type_id'));
        $this->set(compact('creatives_type'));
    }

    public function edit_upload_creative_text_html($creative_id = null, $lineitem = null, $size = null) {
        $this->System->add_css(array('/plugins/summernote/summernote.css'), 'head');
        $this->System->add_js(array('/plugins/summernote/summernote.min.js'), 'head');
        //$this->System->add_js(array('/plugins/ckeditor/vendor/ckeditor/ckeditor.js'), 'head');
        // find creative
        $creatives = $this->tblCreative->find('first', array('conditions' => array('tblCreative.cr_id' => $creative_id)));
        if (empty($creatives)) {
            $this->Session->setFlash(__('Sorry, Something went wrong!'), 'error');
            $this->redirect(array('controller' => 'orders', 'action' => 'view_line_creatives', $lineitem));
        }
        if (!empty($this->request->data['tblCreative']['creatives_type'])) {
            if (empty($this->request->data['tblCreative']['cr_header']))
                $this->request->data['tblCreative']['cr_header'] = $this->request->data['tblCreative']['cr_name'];

            /// end of file upload 
            $image_name = '';

            //if (!empty($this->request->data['tblCreative']['cr_name'])) {
            //$this->request->data['tblCreative']['cr_header'] = $this->request->data['tblCreative']['cr_name'];
            //}
            if ($this->request->data['tblCreative']['cr_banner_name']['error'] == 0) {
                $image_name = self::_manage_image($this->request->data['tblCreative']['cr_banner_name']);
            }
            // on edit case
            if ($this->request->data['tblCreative']['cr_id'] && $image_name != '') {
                $path = Configure::read('Path.creative_image');
                if (!empty($creatives['tblCreative']['cr_banner_name'])) {
                    unlink($path . $creatives['tblCreative']['cr_banner_name']);
                }
            } else {
                $image_name = $creatives['tblCreative']['cr_banner_name'];
            }

            $this->request->data['tblCreative']['cr_banner_name'] = $image_name;

            $links = $this->request->data['tblCreative']['link'];
            $this->request->data['tblCreative']['link'] = json_encode($links);
            if (!empty($links) and $this->request->data['tblCreative']['creatives_type'] == 3) {
                $body_content = '';
                $body_content .='<ol>';
                foreach ($links as $link) {
                    $body_content .='<li><a href="' . $link . '">' . $link . ' </a></li>';
                }
                $body_content .='<ol>';
                $this->request->data['tblCreative']['cr_body'] = $body_content;
            }
            //   pr($this->request->data);die;
            if(empty($this->request->data['tblCreative']['cr_redirect_url'])){
                $this->Session->setFlash(__('Please Enter Redirect Url'), 'danger');
            }
            else{
                $this->tblCreative->create();
                if ($this->tblCreative->save($this->request->data)) {
                    $this->Session->setFlash(__('Creative updated Successfully'), 'success');       
                    $this->redirect(array('controller' => 'orders', 'action' => 'view_line_creatives', $lineitem));
                }
            }
        }

        if (!empty($size)) {
            $size = base64_decode($size);
        }
        $creatives_type_id = '';
        if (!empty($this->request->data['tblOrder']['creatives_type'])) {
            $creatives_type_id = $this->request->data['tblOrder']['creatives_type'];
        }
        $this->loadModel('CreativesType');
        $creatives_type = $this->CreativesType->GetCreativeType();
        $line_name = $this->tblLineItem->field('li_name', array('li_id' => $lineitem));
        $this->set('line_name', $line_name);
        $line_order_id = $this->tblLineItem->field('li_order_id', array('li_id' => $lineitem));

        $this->set('line_order_id', $line_order_id);
        $line_dfp_id = $this->tblLineItem->field('li_dfp_id', array('li_id' => $lineitem));
        $this->set('line_dfp_id', $line_dfp_id);
        $this->set('size', $size);
        $this->set('lineitem', $lineitem);
        $this->set('size', $size);
        $this->set('creatives', $creatives);
        $this->set(compact('creatives_type_id'));
        $this->set(compact('creatives_type'));
    }

    public function upload_creative_ajax($lineitem, $size) {
        $this->autoRender = false;
        $line_name = $this->tblLineItem->field('li_name', array('li_id' => $lineitem));
        $line_dfp_id = $this->tblLineItem->field('li_dfp_id', array('li_id' => $lineitem));
        $result = array();
        if (!empty($_FILES)) {
            $ds = "/";
            $storeFolder = 'upload';
            $targetPath = "/home/stockprice/public_html/digitaladvertising.systems/app/webroot/" . $storeFolder . $ds . $lineitem . $ds;

            if (!is_dir($targetPath)) {
                mkdir($targetPath);
            }
            $i = 0;
            foreach ($_FILES['files']['tmp_name'] as $index => $tmpName) {
                $targetFile = $targetPath . $_FILES['files']['name'][$index];
                $image_info = getimagesize($tmpName);
                $image_width = $image_info[0];
                $image_height = $image_info[1];
                $files_up = array();
                if ($size == $image_width . "x" . $image_height) {
                    if (move_uploaded_file($tmpName, $targetFile)) {
                        $this->request->data['tblCreative']['cr_lid'] = $line_dfp_id;
                        $this->request->data['tblCreative']['cr_name'] = $size . " Banner";
                        $this->request->data['tblCreative']['cr_size'] = $size;
                        $this->request->data['tblCreative']['cr_status'] = "ACTIVE";
                        $this->request->data['tblCreative']['cr_banner_name'] = $_FILES['files']['name'][$index];
                        $this->request->data['tblCreative']['cr_redirect_url'] = $_REQUEST['redirecturl'][$index];
                        $this->request->data['tblCreative']['cr_preview_url'] = "http://digitaladvertising.systems/upload/" . $lineitem . "/" . $_FILES['files']['name'][$index];
                        $this->tblCreative->create();
                        if ($this->tblCreative->save($this->request->data)) {
                            $files_up['name'] = $_FILES['files']['name'][$index];
                            $files_up['size'] = $_FILES['files']['size'][$index];
                            $files_up['thumbnailUrl'] = "http://digitaladvertising.systems/upload/" . $lineitem . "/" . $_FILES['files']['name'][$index];
                            $files_up['redirecturl'] = $_REQUEST['redirecturl'][$index];
                            $files_up['url'] = "http://digitaladvertising.systems/upload/" . $lineitem . "/" . $_FILES['files']['name'][$index];
                            $result['files'][$i] = $files_up;
                        } else {
                            $files_up['name'] = $_FILES['files']['name'][$index];
                            $files_up['size'] = $_FILES['files']['size'][$index];
                            $files_up['error'] = "Something went wrong! Please try again.";
                            $result['files'][$i] = $files_up;
                        }
                    }
                } else {
                    $files_up['name'] = $_FILES['files']['name'][$index];
                    $files_up['size'] = $_FILES['files']['size'][$index];
                    $files_up['error'] = "Please upload image of " . $size . " only";
                    $result['files'][$i] = $files_up;
                }
            }
            //$out = array_values($result);
            return json_encode($result);
            exit();
        }
    }

    /**
	* 
	* @modified By - Satanik
	* @Description - Edit Flight w.r.t the line items
	* @date        - 12/1/2017
	* @module      - Edit Line Item Flight
	*/
    public function edit_line_item_flight($flight_id = null, $lineItemId = null, $status = null) {
        
        $this->loadModel('FlightsCreative');
        
        if ($this->request->is('post') || $this->request->is('put')) {
        	//pr($this->request->data);exit;
            $order_dfp_id = $this->tblLineItem->field('li_order_id', array('li_id' => $lineItemId));
            $order_id = $this->tblOrder->field('order_id', array('dfp_order_id' => $order_dfp_id));
            
            // remove array to string or json 
            $this->request->data['tblFlight']['fl_targeting_option'] = (!empty($this->request->data['tblFlight']['fl_targeting_option'])) ? json_encode($this->request->data['tblFlight']['fl_targeting_option']) : json_encode(array());

            $this->request->data['tblFlight']['fl_ptype_id'] = (!empty($this->request->data['tblFlight']['fl_ptype_id'])) ? $this->request->data['tblFlight']['fl_ptype_id'][0] : 0;

            $this->request->data['tblFlight']['fl_behavior'] = (!empty($this->request->data['tblFlight']['fl_behavior'])) ? $this->request->data['tblFlight']['fl_behavior'][0] : 0;


            if (isset($this->request->data['adunit-select'])) {
                $adunitSelected = $this->request->data['adunit-select'];
                $ptype = $this->request->data['tblFlight']['fl_ptype_id'];
                
                if (isset($this->request->data['tblFlightAudiences']['fl_as_id']))
                    $flightAudience = $this->request->data['tblFlightAudiences']['fl_as_id'];
                else
                    $flightAudience = array();
                $this->request->data['tblFlight']['fl_start'] = date("Y-m-d H:i:s", strtotime(str_replace('-', '/', $this->request->data['tblFlight']['fl_start'])));
                $this->request->data['tblFlight']['fl_end'] = date("Y-m-d H:i:s", strtotime(str_replace('-', '/', $this->request->data['tblFlight']['fl_end'])));

                // copy when select Flight setting in audience 
                if (!empty($this->request->data['tblFlight']['audiences'])) {
                    $this->request->data['tblFlight']['fl_audience_segment'] = self::getAudience($this->request->data['tblFlight']['audiences']);
                }
                
                $this->tblFlight->create();
                if ($this->tblFlight->save($this->request->data)) {
                    $flightID = $this->tblFlight->id;
                    
                    if (!empty($_FILES['zipcode_csv']['name'])) {
                        $directoryPath = getcwd() . DS . 'files' . DS . 'flight' . DS;
                        if (!empty($this->data['tblFlight']['zip_targeting'])) {
                            unlink($directoryPath . $this->data['tblFlight']['zip_targeting']);
                        }
                        $fileName = time() . '_' . str_replace(" ", "", $_FILES['zipcode_csv']['name']);
                        move_uploaded_file($_FILES['zipcode_csv']['tmp_name'], $directoryPath . $fileName);
                        $this->tblFlight->id = $flightID;
                        $this->tblFlight->saveField('zip_targeting', $fileName);
                    }

                    // save FlightsCreative table 
                    if (!empty($this->request->data['FlightsCreative']['cr_id'])) {
                        $this->loadModel('FlightsCreative');
                        $this->FlightsCreative->deleteAll(array('FlightsCreative.fl_id' => $this->tblFlight->id), false, false);
                        foreach ($this->request->data['FlightsCreative']['cr_id'] as $cr_id) {
                            $datacreative = array();
                            $datacreative['FlightsCreative'] = array('cr_id' => $cr_id, 'fl_id' => $this->tblFlight->id);
                            $this->FlightsCreative->create();
                            $this->FlightsCreative->save($datacreative);
                        }
                    }

                    $chk = "";
                    if(count($adunitSelected)>0 && !empty($adunitSelected)):
                        foreach ($adunitSelected as $chk1) {
                        $this->request->data['tblMappedAdunit']['ma_ad_id'] = $chk1;
                        $this->request->data['tblMappedAdunit']['ma_l_id'] = $lineItemId;
                        $this->request->data['tblMappedAdunit']['ma_fl_id'] = $flightID;
                        $data['tblMappedAdunit']['rev_share'] = (!empty($this->request->data['rev_share'][$chk1])) ? $this->request->data['rev_share'][$chk1] : '';
                        $data['tblMappedAdunit']['dailyAllocation'] = (!empty($this->request->data['dailyAllocation'][$chk1])) ? $this->request->data['dailyAllocation'][$chk1] : '';       
                        $data['tblMappedAdunit']['monthlyAllocation'] = (!empty($this->request->data['monthlyAllocation'][$chk1])) ? $this->request->data['monthlyAllocation'][$chk1] : '';
                        $data['tblMappedAdunit']['allocation'] = (!empty($this->request->data['allocation'][$chk1])) ? $this->request->data['allocation'][$chk1] : '';
                        //$this->tblMappedAdunit->create();
                        $conditions = array(
                            'ma_l_id' => $lineItemId,
                            'ma_ad_id' => $chk1,
                            'ma_fl_id' => $flightID
                        );
                        $check=$this->tblMappedAdunit->find('first',array('conditions'=>$conditions));
                        $mappedAdUnitID = $check['tblMappedAdunit']['ma_id'];
                        $this->tblMappedAdunit->primaryKey='ma_id';
                        $this->tblMappedAdunit->id=$mappedAdUnitID;
                        $m_uid =  $check['tblMappedAdunit']['ma_uid'];
                        if ($this->tblMappedAdunit->save($data)) {
                        	
                            $optionscom['conditions'] = array(
                                'tblAdunit.ad_dfp_id' => $chk1);
                            $optionscom['fields'] = array('tblAdunit.ad_esp', 'tblAdunit.ad_esp_list_id', 'tblAdunit.sender_email');
                            $audit_details = $this->tblAdunit->find('first', $optionscom);
                            $ad_esp = $audit_details['tblAdunit']['ad_esp'];
                            $ad_esp_list_id = $audit_details['tblAdunit']['ad_esp_list_id'];
                            $to_reply = $audit_details['tblAdunit']['sender_email'];

                            if ($ptype == "2") {

                                if (!empty($this->request->data['FlightsCreative']['cr_id'])) {
                                    foreach ($this->request->data['FlightsCreative']['cr_id'] as $cr_id) {

                                        $m_esp_id = 0;

                                        if ($ad_esp == "3" && !empty($ad_esp_list_id)) {

                                            $options['conditions'] = array(
                                                'tblCreative.cr_id' => $cr_id);
                                            $options['fields'] = array('tblCreative.cr_header');
                                            $cr_details = $this->tblCreative->find('first', $options);
                                            $cr_header = $cr_details['tblCreative']['cr_header'];

                                            App::import('Vendor', array('file' => 'autoload'));

                                            $MailchimpApi = Configure::read('User.Mailchimp_Api_' . $this->Authake->getUserId());

                                            $Mailchimp = new \DrewM\MailChimp\MailChimp($MailchimpApi);
                                            $result = $Mailchimp->post("campaigns", [
                                                'type' => 'regular',
                                                'recipients' => ['list_id' => $ad_esp_list_id, 'list_name' => 'Test'],
                                                'settings' => ['subject_line' => $cr_header, 'from_name' => 'Jones', 'reply_to' => $to_reply],
                                            ]);

                                            //print_r($result);
                                            $m_esp_id = $result['id'];
                                        }

                                        $this->request->data['tblMailing']['m_ad_uid'] = $m_uid;
                                        $this->request->data['tblMailing']['m_sl'] = $this->request->data['tblFlight']['fl_name'];
                                        $this->request->data['tblMailing']['m_li_crtid'] = $cr_id;
                                        $this->request->data['tblMailing']['m_esp_id'] = $m_esp_id;
                                        $this->request->data['tblMailing']['m_sent_date'] = date("Y-m-d H:i:s", strtotime(str_replace('-', '/', $this->request->data['tblFlight']['fl_start'])));
                                        $this->request->data['tblMailing']['m_schedule_date'] = date("Y-m-d H:i:s", strtotime(str_replace('-', '/', $this->request->data['tblFlight']['fl_start'])));
                                        $this->tblMailing->create();
                                        $this->tblMailing->save($this->request->data);
                                    }
                                }
                            }
                        }
                    }
                    endif;

  
                    // copy setting according to selected flight id
                    /*if (!empty($this->request->data['tblFlight']['flight_id_delivery'])) {
                        $this->request->data['tblDeliverySetting'] = self::get_tblDeliverySetting($this->request->data['tblFlight']['flight_id_delivery']);
                    }*/

                    $this->request->data['tblDeliverySetting']['ds_m_ad_id'] = $mappedAdUnitID;
                    $this->request->data['tblDeliverySetting']['fl_id'] = $flightID;
                    $this->request->data['tblDeliverySetting']['flight_id_delivery'] = $this->request->data['tblFlight']['flight_id_delivery'][0];
                    $this->request->data['tblDeliverySetting']['ds_delivery_mode'] = $this->request->data['tblDeliverySetting']['DeliveryMethod'];
                    $this->tblDeliverySetting->create();
                    $this->tblDeliverySetting->save($this->request->data);

                    if (isset($this->request->data['geography-select'])) {
                        $this->tblAllowedCountries->deleteAll(array('tblAllowedCountries.ac_m_ad_id' => $flightID), false, false);

                        $tblAllowedCountries = $this->request->data['geography-select'];
                        $chk = "";
                        if(count($tblAllowedCountries)>0 && !empty($tblAllowedCountries)):
	                        foreach ($tblAllowedCountries as $chk1) {
	                            $this->request->data['ac_c_id'] = $chk1;
	                            $this->request->data['ac_m_ad_id'] = $flightID;
	                            $this->tblAllowedCountries->create();
	                            $this->tblAllowedCountries->save($this->request->data);
	                        }
	                    endif;    
                    }
                    $this->tblFlightAudiences->deleteAll(array('tblFlightAudiences.fl_flight_id' => $flightID), false, false);
                    if (!empty($flightAudience)) {
                        foreach ($flightAudience as $chk1) {
                            $this->request->data['tblFlightAudiences']['fl_as_id'] = $chk1;
                            $this->request->data['tblFlightAudiences']['fl_isActive'] = 1;
                            $this->request->data['tblFlightAudiences']['fl_flight_id'] = $flightID;
                            $this->tblFlightAudiences->create();
                            $this->tblFlightAudiences->save($this->request->data);
                        }
                    }
                    //$this->tblLineItem->updateAll(array('li_status_id' => 3),array('li_id' => $lineItemId));
                }
                $this->Session->setFlash(__('Flight has been updated successfully'), 'success');
                $this->redirect(array('controller' => 'orders', 'action' => 'view_flights', $lineItemId));
            } else {
                $this->Session->setFlash(__('Sorry, Something went wrong!'), 'error');
                //$this->redirect(array('action' => 'edit_line_item', $lineItemId));
                $this->redirect(array('action' => 'add_line_item_flight', $lineItemId));
            }
        }

        // get flight detail 
        $optionsl['joins'] = array(
            array('table' => 'tbl_delivery_settings',
                'alias' => 'tblDeliverySetting',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblFlight.fl_id = tblDeliverySetting.fl_id'))
        );
        $optionsl['conditions'] = array('tblFlight.fl_id' => $flight_id);
        $optionsl['fields'] = array('tblFlight.*', 'tblDeliverySetting.*');
        $this->request->data = $this->tblFlight->find('first', $optionsl);
        //pr($this->request->data);exit;
        
        
        $optionsas['conditions'] = array('kma_group' => '1');
        $as_details = $this->tblAudienceSegments->find('all', $optionsas);
        
        if (!empty($this->request->data['tblFlight']['zip_targeting'])) {
            $file = getcwd() . DS . 'files' . DS . 'flight' . DS . $this->request->data['tblFlight']['zip_targeting'];
            if(file_exists($file)){
				$csv = array_map('str_getcsv', file($file));
	            if(count($csv)>0 && !empty($csv)):
		            foreach ($csv as $i => $c) {
		                $csv_data[$i] = $c[0];
		            }
		            $this->set('csv_data', $csv_data);
		        endif;  	
			}
        }
       
        $lineItem_details = $this->tblLineItem->getLineItemDetail($lineItemId);
        $phpStartdate = strtotime($lineItem_details['tblLineItem']['li_start_date']);
        $lineItem_details['tblLineItem']['li_start_date'] = date('m/d/Y', $phpStartdate);
        $phpEnddate = strtotime($lineItem_details['tblLineItem']['li_end_date']);
        $lineItem_details['tblLineItem']['li_end_date'] = date('m/d/Y', $phpEnddate);

        
        // date formate change 
        $fl_start = strtotime($this->request->data['tblFlight']['fl_start']);
        $this->request->data['tblFlight']['fl_start'] = date('m/d/Y', $fl_start);
        $fl_end = strtotime($this->request->data['tblFlight']['fl_end']);
        $this->request->data['tblFlight']['fl_end'] = date('m/d/Y', $fl_end);
        
         // get mappedadunit & set to HTML Page 
        $this->set('creative_select', self::GetCreativeByFlight($flight_id));
        $this->set('geography_select', self::GetGeographyByFlight($flight_id));
        $this->set('adunit_select', self::GetmappedAdunitListByFlight($flight_id));
        $this->set('adunit_select_div', self::GetmappedAdunitDetailByFlight($flight_id));
        $this->set('producttype', $this->tblProducttype->getProductTypeList());
        $this->set('tblCountries', $this->tblCountries->CountryList());
        
        // set data to HTML Page
        $this->set('flightID', $this->request->data['tblDeliverySetting']['flight_id_delivery']);
        if ($status == 2) {
            $this->Session->setFlash(__('Please Upload the Creative First'), 'error');
            $this->redirect(array('controller' => 'orders', 'action' => 'view_line_creatives', $lineItemId));
        }
        $this->set('title_for_layout', 'Insertion Order');
        $this->set('lineItem_details', $lineItem_details);
        $this->set('as_details', $as_details);
        $this->set('lineItemId', $lineItemId);
        $this->set('flight_id', $flight_id);
        $this->set('status', $status);
        $this->set('flight_list', self::getflight_list($lineItemId));
        $this->set('allocatedallocation', self::getTotalAllocated($lineItemId));
        
        // order breadcrums
        $this->breadcrumbs[] = array(
            'url' => Router::url('/'),
            'name' => 'Home'
        );
        $this->breadcrumbs[] = array(
            'url' => Router::url(array('controller' => 'orders', 'action' => 'view_line_items', $lineItem_details['tblLineItem']['li_order_id'])),
            'name' => $lineItem_details['tblOrder']['order_name']
        );
        $this->breadcrumbs[] = array(
            'url' => Router::url(array('controller' => 'insertionorder', 'action' => 'add_line_item_flight', $lineItem_details['tblLineItem']['li_id'])),
            'name' => $lineItem_details['tblLineItem']['li_name']
        );
        $this->breadcrumbs[] = array(
            'url' => '#',
            'name' => $this->request->data['tblFlight']['fl_name']
        );
    }
    
    /**
	* @modified By - Satanik
	* @Description - Ajax Response to view w.r.t the flight delivery ID
	* @date        - 12/1/2017
	* @module      - Ajax Resoponse w.r.t flight delivery ID
	*/
    
    public function getAjaxResponse(){
		$this->autoRender = false;
        $fl_delivery_id = $_POST['fl_delivery_id'];
        //$this->loadModel('Authake.tblDeliverySetting');
        
        $optionsl['conditions'] = array('tblDeliverySetting.flight_id_delivery' => $fl_delivery_id);
        $optionsl['fields'] = array('tblDeliverySetting.*');
        $result = $this->tblDeliverySetting->find('first', $optionsl);
        //pr($result);exit;
        if(count($result)>0 && !empty($result)){
			 $output = array('sucess'=>true,'data'=>$result);
		} else {
			 $output = array('sucess'=>false);
		}
        echo json_encode($output);
        exit;
	}

    /** Delete allocated zip csv file frorm flight edit page */
    function deleteZipCSV() {
        $this->autoRender = false;
        $id = $_POST['fl_id'];
        if (!empty($id)) {
            $data = $this->tblFlight->find('first', array('conditions' => array('tblFlight.fl_id' => $id), 'fields' => array('zip_targeting')));
            $file = getcwd() . DS . 'files' . DS . 'flight' . DS . $data['tblFlight']['zip_targeting'];
            unlink($file);
            $this->tblFlight->id = $id;
            $this->tblFlight->saveField('zip_targeting', '');
        }
    }

    private function GetCreativeByFlight($flight_id) {

        return $this->FlightsCreative->find('list', array('fields' => array('FlightsCreative.cr_id', 'FlightsCreative.cr_id'), 'conditions' => array('FlightsCreative.fl_id' => $flight_id)));
    }

    private function GetGeographyByFlight($flight_id) {
        return $this->tblAllowedCountries->find('list', array('fields' => array('tblAllowedCountries.ac_c_id', 'tblAllowedCountries.ac_c_id'), 'conditions' => array('tblAllowedCountries.ac_m_ad_id' => $flight_id, 'tblAllowedCountries.ac_isActive' => 1)));
    }

    private function GetmappedAdunitListByFlight($flight_id = nulll) {

        $optionsl['conditions'][] = array('tblMappedAdunit.ma_fl_id' => $flight_id);
        $optionsl['fields'] = array('tblMappedAdunit.ma_ad_id', 'tblMappedAdunit.ma_ad_id');
        return $this->tblMappedAdunit->find('list', $optionsl);
    }

    public function GetmappedAdunitDetailByFlight($flight_id = nulll) {
        $optionsl['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_ad_id = tblAdunit.ad_dfp_id')),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = tblp.user_id')),
            array('table' => 'tbl_product_types',
                'alias' => 'tbpt',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_ptype_id = tbpt.ptype_id',
                )),
            array('table' => 'tbl_media_types',
                'alias' => 'tbmt',
                'type' => 'INNER',
                'conditions' => array(
                    'tbmt.mtype_id = tbpt.ptype_m_id'
        )));

        $optionsl['group'] = array('tblAdunit.ad_dfp_id');
        $optionsl['conditions'][] = array('tblMappedAdunit.ma_fl_id' => $flight_id);
        $optionsl['fields'] = array('tblMappedAdunit.rev_share', 'tblMappedAdunit.dailyAllocation', 'tblMappedAdunit.monthlyAllocation', 'tblMappedAdunit.allocation', 'tblMappedAdunit.ma_ad_id', 'tblp.CompanyName', 'tblAdunit.adunit_name');
        return $this->tblMappedAdunit->find('all', $optionsl);
    }
    
    /**
	* @modified By - Satanik
	* @Description - Changes on Cloning isuse on Add Line Item
	* @date        - 18/1/2017
	* @module      - Add Line Item Flight
	*/

     public function add_line_item_flight($line_item_id = null, $status = null) {
        if ($status == 2) {
            $this->Session->setFlash(__('Please Upload the Creative First'), 'error');
            $this->redirect(array('controller' => 'orders', 'action' => 'view_line_creatives', $line_item_id));
        }
        $this->set('title_for_layout', 'Insertion Order');
        if ($this->request->is('post') || $this->request->is('put')) {
        	
        	//pr($this->request->data);exit;

            $this->request->data['tblFlight']['fl_targeting_option'] = (!empty($this->request->data['tblFlight']['fl_targeting_option'])) ? json_encode($this->request->data['tblFlight']['fl_targeting_option']) : json_encode(array());


            $lineItemId = $line_item_id;
            $order_dfp_id = $this->tblLineItem->field('li_order_id', array('li_id' => $lineItemId));
            $order_id = $this->tblOrder->field('order_id', array('dfp_order_id' => $order_dfp_id));


            if (isset($this->request->data['adunit-select'])) {

                // copy when select Flight setting in audience 

                if (!empty($this->request->data['tblFlight']['audiences'])) {
                    $this->request->data['tblFlight']['fl_audience_segment'] = self::getAudience($this->request->data['tblFlight']['audiences']);
                }

                $adunitSelected = $this->request->data['adunit-select'];
                $ptype = $this->request->data['tblFlight']['fl_ptype_id'];
                if (isset($this->request->data['tblFlightAudiences']['fl_as_id']))
                    $flightAudience = $this->request->data['tblFlightAudiences']['fl_as_id'];
                else
                    $flightAudience = array();
                $this->request->data['tblFlight']['fl_start'] = date("Y-m-d H:i:s", strtotime(str_replace('-', '/', $this->request->data['tblFlight']['fl_start'])));
                $this->request->data['tblFlight']['fl_end'] = date("Y-m-d H:i:s", strtotime(str_replace('-', '/', $this->request->data['tblFlight']['fl_end'])));
                //print_r($this->request->data); exit();
                $this->tblFlight->create();
                if ($this->tblFlight->save($this->request->data)) {
                    $flightID = $this->tblFlight->getLastInsertID();
                    if (!empty($_FILES['zipcode_csv'])) {
                        $directoryPath = getcwd() . DS . 'files' . DS . 'flight' . DS;
                        $fileName = time() . '_' . str_replace(" ", "", $_FILES['zipcode_csv']['name']);
                        move_uploaded_file($_FILES['zipcode_csv']['tmp_name'], $directoryPath . $fileName);
                        $this->tblFlight->id = $flightID;
                        $this->tblFlight->saveField('zip_targeting', $fileName);
                    }
                    // save FlightsCreative table 
                    if (!empty($this->request->data['FlightsCreative']['cr_id'])) {
                        $this->loadModel('FlightsCreative');
                        foreach ($this->request->data['FlightsCreative']['cr_id'] as $cr_id) {
                            $datacreative = array();
                            $datacreative['FlightsCreative'] = array('cr_id' => $cr_id, 'fl_id' => $this->tblFlight->id);
                            $this->FlightsCreative->create();
                            $this->FlightsCreative->save($datacreative);
                        }
                    }

                    $chk = "";

                    foreach ($adunitSelected as $chk1) {
                        $this->request->data['tblMappedAdunit']['ma_ad_id'] = $chk1;
                        $this->request->data['tblMappedAdunit']['ma_l_id'] = $lineItemId;
                        $this->request->data['tblMappedAdunit']['ma_fl_id'] = $flightID;
                        $this->request->data['tblMappedAdunit']['rev_share'] = (!empty($this->request->data['rev_share'][$chk1])) ? $this->request->data['rev_share'][$chk1] : '';
                        $this->request->data['tblMappedAdunit']['dailyAllocation'] = (!empty($this->request->data['dailyAllocation'][$chk1])) ? $this->request->data['dailyAllocation'][$chk1] : '';        
                        $this->request->data['tblMappedAdunit']['monthlyAllocation'] = (!empty($this->request->data['monthlyAllocation'][$chk1])) ? $this->request->data['monthlyAllocation'][$chk1] : '';
                        $this->request->data['tblMappedAdunit']['allocation'] = (!empty($this->request->data['allocation'][$chk1])) ? $this->request->data['allocation'][$chk1] : '';
                      
         		$this->tblMappedAdunit->create();
                        if ($this->tblMappedAdunit->save($this->request->data)) {
                            $mappedAdUnitID = $this->tblMappedAdunit->getLastInsertID();
                            $m_uid = $this->tblMappedAdunit->field(
                                    'ma_uid', array('ma_id' => $mappedAdUnitID)
                            );

                            $optionscom['conditions'] = array(
                                'tblAdunit.ad_dfp_id' => $chk1);
                            $optionscom['fields'] = array('tblAdunit.ad_esp', 'tblAdunit.ad_esp_list_id', 'tblAdunit.sender_email');
                            $audit_details = $this->tblAdunit->find('first', $optionscom);
                            $ad_esp = $audit_details['tblAdunit']['ad_esp'];
                            $ad_esp_list_id = $audit_details['tblAdunit']['ad_esp_list_id'];
                            $to_reply = $audit_details['tblAdunit']['sender_email'];

                            if ($ptype == "2" || $ptype == "13") {

                                if (!empty($this->request->data['FlightsCreative']['cr_id'])) {
                                    foreach ($this->request->data['FlightsCreative']['cr_id'] as $cr_id) {

                                        $m_esp_id = 0;

                                        if ($ad_esp == "3" && !empty($ad_esp_list_id)) {

                                            $options['conditions'] = array(
                                                'tblCreative.cr_id' => $cr_id);
                                            $options['fields'] = array('tblCreative.cr_header');
                                            $cr_details = $this->tblCreative->find('first', $options);
                                            $cr_header = $cr_details['tblCreative']['cr_header'];

                                            App::import('Vendor', array('file' => 'autoload'));

                                            $MailchimpApi = Configure::read('User.Mailchimp_Api_' . $this->Authake->getUserId());

                                            $Mailchimp = new \DrewM\MailChimp\MailChimp($MailchimpApi);
                                            $result = $Mailchimp->post("campaigns", [
                                                'type' => 'regular',
                                                'recipients' => ['list_id' => $ad_esp_list_id, 'list_name' => 'Test'],
                                                'settings' => ['subject_line' => $cr_header, 'from_name' => 'Jones', 'reply_to' => $to_reply],
                                            ]);

                                            //print_r($result);
                                            $m_esp_id = $result['id'];
                                        }
										

                                        $this->request->data['tblMailing']['m_ad_uid'] = $m_uid;
                                        $this->request->data['tblMailing']['m_sl'] = $this->request->data['tblFlight']['fl_name'];
                                        $this->request->data['tblMailing']['m_li_crtid'] = $cr_id;
                                        $this->request->data['tblMailing']['m_esp_id'] = $m_esp_id;
                                        $this->request->data['tblMailing']['m_sent_date'] = date("Y-m-d H:i:s", strtotime(str_replace('-', '/', $this->request->data['tblFlight']['fl_start'])));
                                        $this->request->data['tblMailing']['m_schedule_date'] = date("Y-m-d H:i:s", strtotime(str_replace('-', '/', $this->request->data['tblFlight']['fl_start'])));
                                        $this->tblMailing->create();
                                        if($this->tblMailing->save($this->request->data)){
										$last_insert_id = $this->tblMailing->getLastInsertID();
										$this->tblMailing->updateAll(
										array('m_esp_id' => "'$last_insert_id'"),
										array('m_id' => $last_insert_id)
									);
									}
										
                                    }
                                }
                            }


                            // copy setting according to selected flight id
                            if (!empty($this->request->data['tblFlight']['flight_id_delivery'])) {
                                $this->request->data['tblDeliverySetting'] = self::get_tblDeliverySetting($this->request->data['tblFlight']['flight_id_delivery']);
                            }
                            $this->request->data['tblDeliverySetting']['fl_id'] = $flightID;
                            $this->request->data['tblDeliverySetting']['ds_m_ad_id'] = $mappedAdUnitID;
                            $this->request->data['tblDeliverySetting']['flight_id_delivery'] = isset($this->request->data['tblFlight']['flight_id_delivery'][0]) && !empty($this->request->data['tblFlight']['flight_id_delivery'][0]) ? $this->request->data['tblFlight']['flight_id_delivery'][0]  : $flightID;
                            $this->request->data['tblDeliverySetting']['ds_delivery_mode'] = $this->request->data['tblDeliverySetting']['DeliveryMethod'];
                            $this->tblDeliverySetting->create();
                            $this->tblDeliverySetting->save($this->request->data);
                            if (isset($this->request->data['geography-select'])) {
                                $tblAllowedCountries = $this->request->data['geography-select'];
                                $chk = "";
                                foreach ($tblAllowedCountries as $chk1) {
                                    $this->request->data['ac_c_id'] = $chk1;
                                    $this->request->data['ac_m_ad_id'] = $flightID;
                                    $this->tblAllowedCountries->create();
                                    $this->tblAllowedCountries->save($this->request->data);
                                }
                            }
				
                        }
                        }
                        
                       //$log = $this->tblMappedAdunit->getDataSource()->getLog(false, false);
					   //debug($log);
                    foreach ($flightAudience as $chk1) {
                        $this->request->data['tblFlightAudiences']['fl_as_id'] = $chk1;
                        $this->request->data['tblFlightAudiences']['fl_isActive'] = 1;
                        $this->request->data['tblFlightAudiences']['fl_flight_id'] = $flightID;
                        $this->tblFlightAudiences->create();
                        $this->tblFlightAudiences->save($this->request->data);
                    }
                    $this->tblLineItem->updateAll(array('li_status_id' => 3), array('li_id' => $lineItemId));
                }
                $this->Session->setFlash(__('Flight Created Successfully'), 'success');
                $this->redirect(array('controller' => 'orders', 'action' => 'view_flights', $line_item_id));
            } else {
                $this->Session->setFlash(__('Sorry, Something went wrong!'), 'error');
                //$this->redirect(array('action' => 'edit_line_item', $lineItemId));
                $this->redirect(array('action' => 'add_line_item_flight', $line_item_id));
            }
        }
        $optionsp['joins'] = array(
            array('table' => 'tbl_media_types',
                'alias' => 'mtypes',
                'type' => 'INNER',
                'conditions' => array(
                    'tblProducttype.ptype_m_id = mtypes.mtype_id')
        ));

        $optionsp['fields'] = array('mtypes.*', 'tblProducttype.*');

        $producttype = $this->tblProducttype->find('all', $optionsp);
        //print_r($producttype);

        $this->set('producttype', $producttype);

        $tblCountries = $this->tblCountries->find('all');
        $this->set('tblCountries', $tblCountries);

        $lineItem_details = $this->tblLineItem->getLineItemDetail($line_item_id);
        $phpStartdate = strtotime($lineItem_details['tblLineItem']['li_start_date']);
        $lineItem_details['tblLineItem']['li_start_date'] = date('m/d/Y', $phpStartdate);
        $phpEnddate = strtotime($lineItem_details['tblLineItem']['li_end_date']);
        $lineItem_details['tblLineItem']['li_end_date'] = date('m/d/Y', $phpEnddate);

        $optionsas['conditions'] = array('kma_group' => '1');
        $as_details = $this->tblAudienceSegments->find('all', $optionsas);

        $this->set('lineItem_details', $lineItem_details);
        $this->set('as_details', $as_details);
        $this->set('flight_list', self::getflight_list($line_item_id));
        $this->set('allocatedallocation', self::getTotalAllocated($line_item_id));
        $this->set('allocatedDaily', self::getDailyAllocated($line_item_id));       
        $this->set('allocatedMonthly', self::getMonthlyAllocated($line_item_id));
        $this->breadcrumbs[] = array(
            'url' => Router::url('/'),
            'name' => 'Home'
        );
        $this->breadcrumbs[] = array(
            'url' => Router::url(array('controller' => 'orders', 'action' => 'view_line_items', $lineItem_details['tblLineItem']['li_order_id'])),
            'name' => $lineItem_details['tblOrder']['order_name']
        );
        $this->breadcrumbs[] = array(
            'url' => Router::url(array('controller' => 'insertionorder', 'action' => 'add_line_item_flight', $lineItem_details['tblLineItem']['li_id'])),
            'name' => $lineItem_details['tblLineItem']['li_name']
        );
    }
    private function _manage_image($image = array()) {
        if ($image['error'] > 0) {
            return null;
        } else {
            $existing_image = array();
            if ($image['error'] > 0) {
                return $existing_image['tblCreative']['cr_banner_name'];
            } else {
                $destination = Configure::read('Path.creative_image');
                $ext = explode('.', $image['name']);
                $get_ext = array_pop($ext);
                $name = basename($image['name'], '.' . $get_ext);


                $image_name = self::clean($name) . '_' . time() . '.' . $get_ext;
                //$image_name = time() . '_' . time() . '.' . array_pop($ext);
                move_uploaded_file($image['tmp_name'], $destination . $image_name);
                if (!empty($existing_image)) {
                    unlink($destination . $existing_image['tblCreative']['cr_banner_name']);
                }
                //move_uploaded_file($filename, $destination);
                return $image_name;
            }
        }
    }

    private function getTotalAllocated($lineItemId = null) {

        $this->tblMappedAdunit->virtualFields = array(
            'alotallocation' => 'COALESCE(SUM(tblMappedAdunit.allocation),0)');
        $totalAllocation = $this->tblMappedAdunit->find('first', array('fields' => array('tblMappedAdunit.alotallocation'), 'conditions' => array('tblMappedAdunit.ma_l_id' => $lineItemId)));      
            
        return (!empty($totalAllocation)) ? intval($totalAllocation['tblMappedAdunit']['alotallocation']) : 0;      
    }
    private function getDailyAllocated($lineItemId = null)     
    {       
             $this->tblMappedAdunit->virtualFields = array(     
                'alotallocation' => 'COALESCE(SUM(tblMappedAdunit.dailyAllocation),0)');        
            $totalAllocation = $this->tblMappedAdunit->find('first', array('fields' => array('tblMappedAdunit.alotallocation'), 'conditions' => array('tblMappedAdunit.ma_l_id' => $lineItemId)));      
            
            return (!empty($totalAllocation)) ? intval($totalAllocation['tblMappedAdunit']['alotallocation']) : 0;      
    }   
    private function getMonthlyAllocated($lineItemId = null)        
    {       
             $this->tblMappedAdunit->virtualFields = array(     
                'alotallocation' => 'COALESCE(SUM(tblMappedAdunit.monthlyAllocation),0)');  
             $totalAllocation = $this->tblMappedAdunit->find('first', array('fields' => array('tblMappedAdunit.alotallocation'), 'conditions' => array('tblMappedAdunit.ma_l_id' => $lineItemId)));      
            
        return (!empty($totalAllocation)) ? intval($totalAllocation['tblMappedAdunit']['alotallocation']) : 0;      
    }     

    private function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function inhousecompany($company_type = 0) {
        $this->autoRender = false;
        $owner_user_id = '';
        if ($company_type == 1) {
            $owner_user_id = $this->Authake->getUserId();
        }
        $advertisers = $this->tblProfile->getAllConnectedProfiles($owner_user_id);

        $advertiser_data[0]['id'] = "0";
        $advertiser_data[0]['text'] = "Start typing to select Advertiser Name ";
        if (!empty($advertisers)) {
            $count = 1;
            foreach ($advertisers as $key => $value) {

                $advertiser_data[$count]['id'] = $value['tblProfile']['dfp_company_id'] . '@' . $value['User']['id'];
                $advertiser_data[$count]['text'] = $value['tblProfile']['CompanyName'] . " - " . $value['User']['ContactName'];
                $count++;
            }
        }

        return json_encode($advertiser_data);
    }

    public function test_lead_posting($lead_id = null) {
        $this->loadModel('LineItemField');
        if ($this->request->is('post')) {
            pr($this->request->data);
        }
        $this->layout = 'ajax';
        $lineItemFields = $this->LineItemField->find('all', array('conditions' => array('LineItemField.fld_li_id' => $lead_id, 'LineItemField.category_type' => 1)));

        $options = array();
        $options['conditions'] = array('tblLineItem.li_id' => $lead_id);

        $options['joins'] = array(
            array('table' => 'tbl_orders',
                'alias' => 'tblOrder',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.dfp_order_id = tblLineItem.li_order_id')
            ),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id')
            ),
            array('table' => 'tbl_auto_responders',
                'alias' => 'AutoResponder',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblLineItem.li_id = AutoResponder.li_id')
            )
        );

        $options['fields'] = array('tblLineItem.*', 'AutoResponder.*', 'tblOrder.order_name', 'tblp.user_id');

        $line_item = $this->tblLineItem->find('first', $options);
        $this->set('lineItemFields', $lineItemFields);
        $this->set('line_item', $line_item);
        $this->set('flight_list', self::getflight_list($line_item['tblLineItem']['li_id']));
    }

    public function formxml() {
        pr($this->request->data);
        echo "dsfdsfds";
        die;
    }

    public function get_company_ajax($revenue_type = 0) {
        $owner_user_id = null;
        if ($revenue_type == 1) {
            $owner_user_id = $this->Authake->getUserId();
        }

        $this->autoRender = false;
        $this->loadModel('Category');

        $advertisers = $this->tblProfile->getAllConnectedProfiles($owner_user_id);
        $subcategories[0]['id'] = "0";
        $subcategories[0]['text'] = "Start typing to select Advertiser Name ";
        $data = '';
        if (!empty($advertisers)) {
            foreach ($advertisers as $option) {
                $data .='<option data-foo="' . $option['tblProfile']['dfp_company_id'] . '" value="' . $option['tblProfile']['dfp_company_id'] . '@' . $option['User']['id'] . '">' . $option['tblProfile']['CompanyName'] . " - " . $option['User']['ContactName'] . '</option>';
            }
        }

        $this->autoRender = false;
        return $data;
    }

    // 20-07-16
    private function getflight_list($line_item_id = null) {
        $this->loadModel('Authake.tblFlight');
        $optionm['conditions'] = array('tblMappedAdunit.ma_isActive' => '1', 'tblLineItem.li_id' => $line_item_id);
        $optionm['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id = tblLineItem.li_id')
            ),
            array('table' => 'tbl_flights',
                'alias' => 'tblFlight',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMappedAdunit.ma_fl_id = tblFlight.fl_id')
            ),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblAdunit.publisher_id = tblp.user_id',
                    'tblp.owner_user_id' => array($this->Authake->getUserId()))));
        $optionm['fields'] = array('tblFlight.fl_id', 'tblFlight.fl_name');
        
        // $result = $this->tblMappedAdunit->find('list', $optionm);
        // pr($result);exit;
        return $this->tblMappedAdunit->find('list', $optionm);
    }

    private function getAudience($flight_id = null) {
        $AudienceData = array();
        if (!empty($flight_id)) {
            $AudienceData = $this->tblFlight->find('first', array('fields' => array('tblFlight.fl_audience_segment'), 'conditions' => array('tblFlight.fl_id' => $flight_id)));
        }
        if (!empty($AudienceData)) {
            return $AudienceData['tblFlight']['fl_audience_segment'];
        }
        return "";
    }

    private function get_tblDeliverySetting($flight_id = null) {
        $AudienceData = array();
        if (!empty($flight_id)) {
            $AudienceData = $this->tblDeliverySetting->find('first', array('conditions' => array('tblDeliverySetting.fl_id' => $flight_id)));
        }
        if (!empty($AudienceData)) {
            unset($AudienceData['tblDeliverySetting']['ds_id']);
            unset($AudienceData['tblDeliverySetting']['ds_m_ad_id']);
            unset($AudienceData['tblDeliverySetting']['fl_id']);
            return $AudienceData['tblDeliverySetting'];
        }
        return array();
    }

    // this function is used for start and pause for line item  list         
    function update_play_action($action = 0, $li_id = null) {
        $this->autoRender = false;
        $userId = $this->Authake->getUserId();
        $optionm['conditions'] = array('tblMappedAdunit.ma_l_id' => $li_id, 'tblMappedAdunit.ma_isActive' => '1');
        if (!empty($user_id)) {
            $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        $optionm['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id = tblLineItem.li_id')
            ),
            array('table' => 'tbl_flights',
                'alias' => 'tblFlight',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMappedAdunit.ma_fl_id = tblFlight.fl_id')
            ),
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id')),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID')));
        $optionm['fields'] = array('tblFlight.fl_id', 'tblFlight.fl_id');
        $line_items = $this->tblMappedAdunit->find('list', $optionm);

        if (!empty($line_items)) {
            $this->tblLineItem->updateAll(array('tbl_lineitem_status' => $action), array('li_id' => $li_id));
            if ($action != 1) {
                $this->tblFlight->updateAll(array('status' => $action), array('fl_id' => $line_items));
            }
            return 1;
        } else {
            return 0;
        }
    }

    // this function is used for start and pause for flight view         
    function update_flight_play_action($action = 0, $fl_id = null) {
        $this->autoRender = false;
        $this->loadModel('Authake.tblFlight');
        $optionm['conditions'] = array('tblFlight.fl_id' => $fl_id);
        $flights = $this->tblFlight->find('count', $optionm);

        if (!empty($flights)) {
            $this->tblFlight->updateAll(array('status' => $action), array('fl_id' => $fl_id));
            return 1;
        } else {
            return 0;
        }
    }

}
