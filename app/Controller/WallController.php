<?php
	App::import('Controller', 'LyrisController');
	
	class WallController extends AppController {
		
		var $uses = array('Authake.User', 'Authake.Rule', 'Authake.Group','Authake.tblSocialMessage','Authake.tblSocialComment','Authake.tblProfile','Authake.tblOrder','Authake.tblLineItem','Authake.tblMappedAdunit','Authake.tblMailing','RegistrationsData','Dynamic');
		
		public function beforeFilter() {
			parent::beforeFilter();
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		public function index() {
			
			$connectedUsers = $this->tblProfile->getAllConnectedProfilesWall();
			$ownerId = $this->Authake->getUserId();
			//print_r($connectedUsers);
			$users = array();
			/*
				foreach($connectedUsers as $connectedUser)
				{
				$users[] = array('FIND_IN_SET(\''. $connectedUser['User']['id'] .'\',tblSocialMessage.allowed_uids) > 0');
			} */
			
			$users[] = array('tblSocialMessage.uid_fk' => $ownerId);
			$users[] = array('FIND_IN_SET(\''.  $ownerId .'\',tblSocialMessage.allowed_uids) > 0');
			
			
			//print_r($users);
			$options['conditions'] = array(
			'OR' => $users);
			
			$options['joins'] = array(
			array('table' => 'authake_users',
			'alias' => 'User',
			'type' => 'INNER',
			'conditions' => array(
			'tblSocialMessage.uid_fk = User.id'
			)));
			
			$options['fields'] = array('tblSocialMessage.*', 'User.*');
			$options['order'] = array('tblSocialMessage.msg_id DESC');
			
			$updatesarray = $this->tblSocialMessage->find('all', $options);
			//print_r($updatesarray);
			$comments = array();
			foreach($updatesarray as $update)
			{
				$optionc['conditions'] = array('tblSocialComment.msg_id_fk' => $update['tblSocialMessage']['msg_id']);
				
				$optionc['joins'] = array(
				array('table' => 'authake_users',
				'alias' => 'User',
				'type' => 'INNER',
				'conditions' => array(
				'tblSocialComment.uid_fk = User.id'
				)));
				
				$optionc['fields'] = array('tblSocialComment.*', 'User.*');
				$optionc['order'] = array('tblSocialComment.com_id ASC');
				
				$comment = $this->tblSocialComment->find('all', $optionc);
				$comments[$update['tblSocialMessage']['msg_id']] = $comment;
				
			}
			
			$option_order['joins'] = array(
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblOrder.advertiser_id = tblp.dfp_company_id',
			'tblOrder.owner_user_id' => array($this->Authake->getUserId()))
			));
			$option_order['fields'] = array('tblOrder.*', 'tblp.CompanyName', 'tblp.user_id');	
			$orders = $this->tblOrder->find('all', $option_order);
			$company_name = array();
			$count = 0;
			foreach($orders as $order)
			{
				//$company_name .=  "{ id: ".$order['tblp']['user_id'].", text: '".$order['tblp']['CompanyName']."' },";
				if (!$this->checkCompanyExist($company_name, 'user_id', $order['tblOrder']['advertiser_user_id'])) {
					$company_name[$count]['user_id'] = $order['tblOrder']['advertiser_user_id'];
					$company_name[$count]['CompanyName'] = $order['tblp']['CompanyName'];
					$count++;
				}
			}
			//$company_name = rtrim($company_name, ',');
			$this->set('orders', $orders);
			$this->set('company_name', $company_name);
			//print_r($company_name);
			$this->set('updatesarray', $updatesarray);
			$this->set('comments', $comments);
			$this->set('connectedUsers', $connectedUsers);
			$this->set('ownerId', $this->Authake->getUserId());
			
		}
		
		public function messageInsertAjax() {
			$this->layout=null;
			if(isset($this->request->data['update']))
			{
				$time=time();
				$ip=$_SERVER['REMOTE_ADDR'];
				$update = $this->request->data['update'];
				$alloweduid = $this->request->data['alloweduid'];
				$this->request->data['tblSocialMessage']['message'] = $update;
				$this->request->data['tblSocialMessage']['uid_fk'] = $this->Authake->getUserId();
				$this->request->data['tblSocialMessage']['ip'] = $ip;
				$this->request->data['tblSocialMessage']['allowed_uids'] = $alloweduid;
				$this->request->data['tblSocialMessage']['created'] = $time;
				$this->tblSocialMessage->create();
				if($this->tblSocialMessage->save($this->request->data))
				{
					$this->set('msg_id', $this->tblSocialMessage->getLastInsertID());
					$email = $this->Authake->getUserEmail();
					$this->set('email', $email);
					$this->set('time', $time);
					$this->set('message', $update);
					$username = $this->User->field('ContactName', array('id' => $this->Authake->getUserId()));
					$this->set('username', $username);
				}
			}
			
		}
		
		public function commentInsertAjax() {
			$this->layout=null;
			if(isset($this->request->data['comment']) && isset($this->request->data['msg_id']))
			{
				$time=time();
				$comment=$this->request->data['comment'];
				$msg_id=$this->request->data['msg_id'];
				$ip=$_SERVER['REMOTE_ADDR'];
				$this->request->data['tblSocialComment']['comment'] = $comment;
				$this->request->data['tblSocialComment']['msg_id_fk'] = $msg_id;
				$this->request->data['tblSocialComment']['uid_fk'] = $this->Authake->getUserId();
				$this->request->data['tblSocialComment']['ip'] = $ip;
				$this->request->data['tblSocialComment']['created'] = $time;
				$this->tblSocialComment->create();
				if($this->tblSocialComment->save($this->request->data))
				{
					$this->set('com_id', $this->tblSocialComment->getLastInsertID());
					$this->set('time', $time);
					$email = $this->Authake->getUserEmail();
					$this->set('email', $email);
					$this->set('comment', $comment);
					$username = $this->User->field('ContactName', array('id' => $this->Authake->getUserId()));
					$this->set('username', $username);
				}
			}
		}
		
		
		public function commentRemoveAjax() {
			$this->autoRender = false;
			if(isset($this->request->data['com_id']))
			{
				$this->tblSocialComment->deleteAll([
				'tblSocialComment.com_id' => $this->request->data['com_id'], 
				'tblSocialComment.uid_fk' => $this->Authake->getUserId()
				],
				false
				);
				return true;
			}
		}
		
		public function messageRemoveAjax() {
			$this->autoRender = false;
			if(isset($this->request->data['msg_id']))
			{
				$this->tblSocialComment->deleteAll([
				'tblSocialComment.msg_id_fk' => $this->request->data['msg_id']
				],
				false
				);
				$this->tblSocialMessage->deleteAll([
				'tblSocialMessage.msg_id' => $this->request->data['msg_id'],
				'tblSocialMessage.uid_fk' => $this->Authake->getUserId()
				],
				false
				);
				return true;
			}
		}
		
		function checkCompanyExist($array, $key, $val) {
			foreach ($array as $item)
			if (isset($item[$key]) && $item[$key] == $val)
			return true;
			return false;
		}
		
		public function getOrderByAdvertiser() {
			$this->autoRender = false;
			$advertiser_id = $this->request->data['advertiser_id'];
			
			$option_order['joins'] = array(
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblOrder.advertiser_id = tblp.dfp_company_id',
			'tblOrder.owner_user_id' => array($this->Authake->getUserId()))
			));
			
			$option_order['conditions'] = array('tblOrder.advertiser_user_id' => $advertiser_id );
			
			$option_order['fields'] = array('tblOrder.*', 'tblp.CompanyName');	
			$orders = $this->tblOrder->find('all', $option_order);
			$orders_array = array();
			$orders_array[0]['id'] = "0";
			$orders_array[0]['text'] = "--Select IO--";
			$count = 1;
			foreach($orders as $order)
			{
				$orders_array[$count]['id'] = $order['tblOrder']['dfp_order_id'];
				$orders_array[$count]['text'] = $order['tblOrder']['order_name'];
				$count++;
			}
			return json_encode($orders_array);
		}
		
		public function getLineItems() {
			$this->autoRender = false;
			$order_id = $this->request->data['order_id'];
			$options['conditions'] = array('tblLineItem.li_order_id' => $order_id );
			$lineitems = $this->tblLineItem->find("all",$options);
			$orders_array = array();
			$orders_array[0]['id'] = "0";
			$orders_array[0]['text'] = "--Select Line Item--";
			$count = 1;
			foreach($lineitems as $order)
			{
				$orders_array[$count]['id'] = $order['tblLineItem']['li_id'];
				$orders_array[$count]['text'] = $order['tblLineItem']['li_name'];
				$count++;
			}
			return json_encode($orders_array);
		}
		
		public function view_mapped_adunits() {
			$this->autoRender = false;
			$line_item_id = $this->request->data['line_item_id'];
			
			$optionm['conditions'] = array('tblMappedAdunit.ma_l_id' => $line_item_id,  'tblMappedAdunit.ma_isActive' => '1');
			$optionm['joins'] = array(
			array('table' => 'tbl_adunits',
			'alias' => 'tblAdunit',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
			),
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.publisher_id = tblp.user_id')));
			$optionm['fields'] = array('tblMappedAdunit.*', 'tblAdunit.*');
			$mappedUnits = $this->tblMappedAdunit->find('all',$optionm);
			$orders_array = array();
			$orders_array[0]['id'] = "0";
			$orders_array[0]['text'] = "--Select Line Item--";
			$count = 1;
			foreach($mappedUnits as $order)
			{
				$orders_array[$count]['id'] = $order['tblMappedAdunit']['ma_uid'];
				$orders_array[$count]['text'] = $order['tblAdunit']['adunit_name'];
				$count++;
			}
			return json_encode($orders_array);
		}
		
		public function uploadCreative(){
			$this->autoRender = false;
			$ds = "/";  //1
			$storeFolder = 'upload';   //2
			$hdnMappedId = $this->request->data['hdnMappedId'];
			if (!empty($_FILES)) {
				
				$tempFile = $_FILES['file']['tmp_name'];          //3             
				
				$targetPath = "/home/stockprice/public_html/digitaladvertising.systems/app/webroot/". $storeFolder . $ds . $hdnMappedId . $ds;  //4
				
				if ( ! is_dir($targetPath)) {
					mkdir($targetPath);
				}
				
				$targetFile =  $targetPath. $_FILES['file']['name'];  //5
				
				move_uploaded_file($tempFile,$targetFile); //6
			}
		}
		
		
		public function creativeUploadAjax(){
			$this->layout=null;
			if(isset($this->request->data['adUnitId']))
			{
				$time=time();
				$ip=$_SERVER['REMOTE_ADDR'];
				$adUnitId = $this->request->data['adUnitId'];
				$subject = $this->request->data['subject'];
				
				
				$optionm['conditions'] = array('tblMappedAdunit.ma_uid' => $adUnitId,  'tblMappedAdunit.ma_isActive' => '1');
				$optionm['joins'] = array(
				array('table' => 'tbl_adunits',
				'alias' => 'tblAdunit',
				'type' => 'INNER',
				'conditions' => array(
				'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
				),
				array('table' => 'tbl_profiles',
				'alias' => 'tblp',
				'type' => 'INNER',
				'conditions' => array(
				'tblAdunit.publisher_id = tblp.user_id')));
				$optionm['fields'] = array('tblMappedAdunit.*', 'tblAdunit.*');
				$mappedUnits = $this->tblMappedAdunit->find('first',$optionm);
				//print_r($mappedUnits);
				
				$filename = "";
				foreach(glob("/home/stockprice/public_html/digitaladvertising.systems/app/webroot/upload/". $mappedUnits['tblMappedAdunit']['ma_uid'] . "/*.html") as $filename)
				{
					$filename =  substr(strrchr($filename, "/"), 1);
				}
				
				$html = file_get_contents('http://digitaladvertising.systems/upload/'. $mappedUnits['tblMappedAdunit']['ma_uid'] . '/' . $filename);
				
				//echo $html;
				
				App::import('Vendor', 'src/html2text');
				$h2t = new html2text($html);
				$text = $h2t->get_text();
				
				$imageURL = "";
				try {
					App::import('Vendor', 'src/html2pdf/HTML2PDF');
					$html2pdf = new HTML2PDF('P', 'A4');
					$html2pdf->writeHTML($html);
					$file = $html2pdf->Output('temp.pdf','F');
					$im = new imagick('temp.pdf');
					$im->setImageFormat( "jpg" );
					$img_name = time().'.jpg';
					$im->cropImage(600,400, 0,0);
					$im->writeImage("/home/stockprice/public_html/digitaladvertising.systems/app/webroot/upload/". $mappedUnits['tblMappedAdunit']['ma_uid'] . "/" .$img_name);
					$im->clear();
					$im->destroy();
					$imageURL = "/upload/".$mappedUnits['tblMappedAdunit']['ma_uid']."/". $img_name;
					} catch(Exception $e) {
					$imageURL = "/img/no-preview.png";
				}
				
				//print_r($mappedUnits);
				$messageID = array(); 
				if(isset($mappedUnits['tblAdunit']['ad_esp_list_id']) && $mappedUnits['tblAdunit']['ad_esp'] == "1")
				{
					App::import('Vendor', 'src/lyrisapi');
					$lyriss = new lyrisapi("666767","Lyris@API");
					$messageID = $lyriss->messageAdd("225411","","",$subject,"HTML",$text,$html,"Lyris@API");
					
					if($messageID['status'] == "success"){
						$this->request->data['tblMailing']['m_ad_uid'] = $mappedUnits['tblMappedAdunit']['ma_uid'];
						$this->request->data['tblMailing']['m_sl'] = $subject;
						$this->request->data['tblMailing']['m_esp_id'] = $messageID['messageid'];
						$this->request->data['tblMailing']['m_screenshot'] = $imageURL;
						$this->tblMailing->create();
						if ($this->tblMailing->save($this->request->data))
						{
							$alloweduid = $this->Authake->getUserId() . "," . $mappedUnits['tblAdunit']['publisher_id'];
							$this->request->data['tblSocialMessage']['message'] = trim(stripslashes(htmlspecialchars("Succesfully Uploaded the Creative on Lyris, Message Id :" . $messageID['messageid'] . "<br /> <img src='".$imageURL."' />")));
							$this->request->data['tblSocialMessage']['uid_fk'] = $this->Authake->getUserId();
							$this->request->data['tblSocialMessage']['ip'] = $ip;
							$this->request->data['tblSocialMessage']['allowed_uids'] = $alloweduid;
							$this->request->data['tblSocialMessage']['created'] = $time;
							$this->tblSocialMessage->create();
							if($this->tblSocialMessage->save($this->request->data))
							{
								$this->set('msg_id', $this->tblSocialMessage->getLastInsertID());
								$email = $this->Authake->getUserEmail();
								$this->set('email', $email);
								$this->set('time', $time);
								$this->set('message', "Succesfully Uploaded the Creative on Lyris, Message Id :" . $messageID['messageid'] . "<br /> <img src='".$imageURL."' />");
								$username = $this->User->field('ContactName', array('id' => $this->Authake->getUserId()));
								$this->set('username', $username);
							}
						}
					}
				} else if(isset($mappedUnits['tblAdunit']['ad_esp_list_id']) && $mappedUnits['tblAdunit']['ad_esp'] == "2")
				{
					$messageID['messageid'] = 'DASSOCEX-';
					$this->request->data['tblMailing']['m_ad_uid'] = $mappedUnits['tblMappedAdunit']['ma_uid'];
					$this->request->data['tblMailing']['m_sl'] = $subject;
					$this->request->data['tblMailing']['m_esp_id'] = $messageID['messageid'];
					$this->request->data['tblMailing']['m_screenshot'] = $imageURL;
					$this->tblMailing->create();
					if ($this->tblMailing->save($this->request->data))
					{
						$mailingID = $this->tblMailing->getLastInsertID();
						$this->tblMailing->updateAll(
						array('tblMailing.m_esp_id' => 'DASSOCEX-'.$mailingID ),
						array('tblMailing.m_id' => $mailingID));
						
						$alloweduid = $this->Authake->getUserId() . "," . $mappedUnits['tblAdunit']['publisher_id'];
						$this->request->data['tblSocialMessage']['message'] = trim(stripslashes(htmlspecialchars("Succesfully Added the Message in System, Please use the Newsletter Name <b><u>DASSOCEX-". $mailingID ."</u></b> in SocketLab <br /> <img src='".$imageURL."' />")));
						$this->request->data['tblSocialMessage']['uid_fk'] = $this->Authake->getUserId();
						$this->request->data['tblSocialMessage']['ip'] = $ip;
						$this->request->data['tblSocialMessage']['allowed_uids'] = $alloweduid;
						$this->request->data['tblSocialMessage']['created'] = $time;
						$this->tblSocialMessage->create();
						if($this->tblSocialMessage->save($this->request->data))
						{
							
							$this->set('msg_id', $this->tblSocialMessage->getLastInsertID());
							$email = $this->Authake->getUserEmail();
							$this->set('email', $email);
							$this->set('time', $time);
							$this->set('message', "Succesfully Uploaded the Creative on Lyris, Message Id :" . $messageID['messageid'] . "<br /> <img src='".$imageURL."' />");
							$username = $this->User->field('ContactName', array('id' => $this->Authake->getUserId()));
							$this->set('username', $username);
						}
					}
				}  else if(isset($mappedUnits['tblAdunit']['ad_esp_list_id']) && $mappedUnits['tblAdunit']['ad_esp'] == "0")
				{
					$messageID['messageid'] = 'DASSOCEX-';
					$this->request->data['tblMailing']['m_ad_uid'] = $mappedUnits['tblMappedAdunit']['ma_uid'];
					$this->request->data['tblMailing']['m_sl'] = $subject;
					$this->request->data['tblMailing']['m_esp_id'] = '';
					$this->request->data['tblMailing']['m_screenshot'] = $imageURL;
					$this->tblMailing->create();
					if ($this->tblMailing->save($this->request->data))
					{	
						$alloweduid = $this->Authake->getUserId() . "," . $mappedUnits['tblAdunit']['publisher_id'];
						$this->request->data['tblSocialMessage']['message'] = trim(stripslashes(htmlspecialchars("Succesfully Added the Message in System, <br /> <img src='".$imageURL."' />")));
						$this->request->data['tblSocialMessage']['uid_fk'] = $this->Authake->getUserId();
						$this->request->data['tblSocialMessage']['ip'] = $ip;
						$this->request->data['tblSocialMessage']['allowed_uids'] = $alloweduid;
						$this->request->data['tblSocialMessage']['created'] = $time;
						$this->tblSocialMessage->create();
						if($this->tblSocialMessage->save($this->request->data))
						{
							
							$this->set('msg_id', $this->tblSocialMessage->getLastInsertID());
							$email = $this->Authake->getUserEmail();
							$this->set('email', $email);
							$this->set('time', $time);
							$this->set('message', "Succesfully Added the Message in System : <br /> <img src='".$imageURL."' />");
							$username = $this->User->field('ContactName', array('id' => $this->Authake->getUserId()));
							$this->set('username', $username);
						}
					}
				}
			}
			
			
		}
		
		public function postEmails(){
			ini_set('max_execution_time', 1000);
			$this->autoRender=false;
			$tblSubscribers =array();
			$this->Dynamic->useTable = "360_79c7ca8d236411e6b91e123aa499f9d5";
			$totalSubscribers = $this->Dynamic->Query("select nl_email from 360_79c7ca8d236411e6b91e123aa499f9d5 where DATE(nl_create) = '2016-07-23'"); 
			foreach($totalSubscribers as $totalSubscriber){
				echo $email = $totalSubscriber['360_79c7ca8d236411e6b91e123aa499f9d5']['nl_email'];
				$resp = $this->requestAction('/full_contact/userProfile', array('email' => $email)); 
			}
		}
		
		public function postEmailsChris(){
			ini_set('max_execution_time', 1000);
			$this->autoRender=false;
			$tblSubscribers =array();
			$this->Dynamic->useTable = "360_79c7ca8d236411e6b91e123aa499f9d5";
			$totalSubscribers = $this->Dynamic->Query("select nl_email from 360_79c7ca8d236411e6b91e123aa499f9d5 where DATE(nl_create) = '2016-07-04'"); 
			$couch_dsn = "http://admin:C0uchADm!n@ec2-54-161-239-191.compute-1.amazonaws.com/";
			$couch_db = "digitaladvertising";
			App::import('Vendor', 'couchdb/couch');
			App::import('Vendor', 'couchdb/couchClient');
			App::import('Vendor', 'couchdb/couchDocument');
			$client = new couchClient($couch_dsn,$couch_db);
			$url = '';
			$i=1;
			foreach($totalSubscribers as $totalSubscriber){
				$fname = '';
				$lname = '';
				$age = '';
				$gender = '';
				$income = '';
				$homeowner = '';
				$email = $totalSubscriber['360_79c7ca8d236411e6b91e123aa499f9d5']['nl_email'];
				$md5email = MD5($email);
				try {
					$response_return = $client->getDoc($md5email);
					$response_return = json_decode(json_encode($response_return), true);
					if($response_return['match_kma'] == 'Y')
					{
						$gender = isset($response_return['GENDER']) ? $response_return['GENDER'] : '';
						
						$age = isset($response_return['KMA_AGEGRP']) ? $response_return['KMA_AGEGRP'] : '';
						
						$income = isset($response_return['KMA_HHINC']) ? $response_return['KMA_HHINC'] : '';
						
						$homeowner = isset($response_return['KMA_OWNRENT']) ? $response_return['KMA_OWNRENT'] : '';
					}
					if($response_return['match_fc'] == 'Y')
					{
						$fullname = isset($response_return['contactInfo']['fullName']) ? $response_return['contactInfo']['fullName'] : '';
						$fname = explode(' ',$fullname)[0];
						$lname = isset(explode(' ',$fullname)[1]) ? explode(' ',$fullname)[1] : "";
					}
					$url = "http://spmglms.com/Lead/addLead.php?partner=37&advertiser=51&offer=42&aff_id=0&traffictype=Coreg&FirstName=".$fname."&LastName=".$lname."&Email=".$email."&age=".$age."&gender=".$gender."&income=".$income."&homeowner=".$homeowner."&match_kma=".$response_return['match_kma']."&match_fc=".$response_return['match_fc']."";
					echo $i . "-". $this->curl_download($url) . "-". $email;
					$i++;
					} catch (Exception $e) {
					echo "exp aya".$email;
				}
			}
		}
		
		public function viewKMAProfile() {
			$this->autoRender = false;
			//$couch_dsn = "http://localhost:5984/";
			### AUTHENTICATED DSN
			$couch_dsn = "http://admin:C0uchADm!n@ec2-54-161-239-191.compute-1.amazonaws.com/";
			
			$couch_db = "digitaladvertising";
			
			
			App::import('Vendor', 'couchdb/couch');
			App::import('Vendor', 'couchdb/couchClient');
			App::import('Vendor', 'couchdb/couchDocument');
			
			$client = new couchClient($couch_dsn,$couch_db);
			
			$arr = array('a.oconnell@aol.com'); //$this->request->param('emailArray');
			$view = $client->include_docs(false)->reduce(true)->group(true)->getView('GetEmailProfiles','kma_deals_count');
			print_r($view);
			
		}
		
		function curl_download($Url){
			if (!function_exists('curl_init')){
				die('Sorry cURL is not installed!');
			}
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $Url);
			curl_setopt($ch, CURLOPT_REFERER, "http://digitaladvertising.systems/");
			curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			$output = curl_exec($ch);
			curl_close($ch);
			return $output;
		}
	}
?>	
