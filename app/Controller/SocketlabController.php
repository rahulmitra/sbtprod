<?php
	App::import('Controller', 'EspController');
	
	class SocketlabController extends AppController {
		
		var $uses = array('Authake.Group','Authake.User','Authake.tblAdunit','Authake.tblNlsubscribers','Dynamic','Authake.tblMailing','Authake.tblProfile','Authake.tblMappedAdunit', 'Authake.tblMessage', 'UserLevelEmailPerformance');
		
		public function beforeFilter() {
			parent::beforeFilter();
			$this->layout=null;
		}
		
		
		public function stocksearningAutoresponder(){
			
			App::import('Vendor', 'src/html2text');
			$email= $this->request->param('email');
			$strArray = array();
			$myArray[] = array('Field'=>'DeliveryAddress', 'Value'=>''.$email.'');
			$myArray[] = array('Field'=>'MessageId', 'Value'=> 'DA-'.$email.'-SEAUTORESP');
			$strArray[] = $myArray;
			$merge_data = new stdClass();
			$merge_data->PerMessage = $strArray;
			$htmlmessage = $this->get_content("http://digitaladvertising.systems/upload/stocks-earning-ar.html");
			
			$h2t =& new html2text($htmlmessage);
			$textVersion = $h2t->get_text();
			
			$data = new stdClass();
			$data->ServerId="12888";
			$data->ApiKey="Dm35SjPk46Yqe2Q7GbNn";
			$data->Messages= array(array('MergeData'=>$merge_data,
			'Subject'=>"Welcome to StocksEarning.com",
			'To'=>array(array('EmailAddress'=>'%%DeliveryAddress%%')),
			'From'=>array('EmailAddress'=>"stocksearning@das0.net", 'FriendlyName' => "StocksEarning"),
			'HtmlBody'=>$htmlmessage,
			'TextBody'=>$textVersion,
			'MailingId'=>'DASSOCEX-SLAUTORESP',
			'MessageId'=>'%%MessageId%%'
			)); 
			$bodyJson = json_encode($data);
			//echo $bodyJson;
			
			
			//use CURL to POST the JSON to SocketLabs
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://inject.socketlabs.com/api/v1/email");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt($ch, CURLOPT_POST,           1 );
			curl_setopt($ch, CURLOPT_POSTFIELDS,     $bodyJson ); 
			curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json')); 
			$result=curl_exec ($ch);
			curl_close ($ch);
			
			//process result
			if ($result==false) {
				echo 'HTTP Error';
				} else {
				$result_obj = json_decode($result);
				if ($result_obj->ErrorCode=='Success') {
					echo 'Successfully sent all messages.';
					} elseif ($result_obj->ErrorCode=='Warning') {
					echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.'; 
					//not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
					} else {
					echo 'Failure Code: ' . $result_obj->ErrorCode;
				}
				print_r($result_obj);
			}
			print_r($merge_data);
			$this->autoRender = false;
		}
		
		public function rssFeedTemplateLive() {
			App::import('Vendor', 'src/html2text');
			$mailing_id = 0;
			
			$m_ad_uid = $this->request->param('m_ad_uid');
			$esp_id = $this->request->param('esp_id');
			$SubjectLine = $this->request->param('SubjectLine');
			$url_nl = $this->request->param('url_nl');
			
			$options_ad['joins'] = array(
			array('table' => 'tbl_mapped_adunits',
			'alias' => 'tma',
			'type' => 'INNER',
			'conditions' => array(
			'tma.ma_ad_id = tblAdunit.ad_dfp_id'
			)),
			array('table' => 'tbl_esp_settings',
			'alias' => 'tes',
			'type' => 'INNER',
			'conditions' => array(
			'tes.es_ad_id = tblAdunit.adunit_id'
			)),
			array('table' => 'tbl_email_service_providers',
			'alias' => 'tesp',
			'type' => 'INNER',
			'conditions' => array(
			'tes.es_esp_id = tesp.esp_id'
			))
			);
			
			$options_ad['conditions'] = array('tma.ma_uid' => $m_ad_uid, 'tes.es_esp_id' => $esp_id);
			$options_ad['fields'] = array('tblAdunit.ad_uid', 'tblAdunit.adunit_id', 'tblAdunit.publisher_id', 'tblAdunit.sender_email', 'tes.es_from_email', 'tes.es_private_key', 'tes.es_site_domain', 'tes.es_test', 'tes.es_tracking_domain', 'tes.es_limit', 'tesp.esp_mail_tag', 'tesp.esp_unsubscribe_tag');
			$adunitSettings = $this->tblAdunit->find('first',$options_ad);
			
			$aduid = $adunitSettings['tblAdunit']['ad_uid'];
			
			$pub_id = $adunitSettings['tblAdunit']['publisher_id'];
			
			$mail_tag = $adunitSettings['tesp']['esp_mail_tag'];
			
			$unsub_tag = $adunitSettings['tesp']['esp_unsubscribe_tag'];
			
			$fromEmail = $adunitSettings['tes']['es_from_email'];
			$SOCKETLABS_SERVERID = $adunitSettings['tes']['es_site_domain'];
			$SOCKETLABS_KEY = $adunitSettings['tes']['es_private_key'];
			define('SOCKETLABS_URL', 'https://inject.socketlabs.com/api/v1/email');
			define('SOCKETLABS_SERVERID', $SOCKETLABS_SERVERID);
			define('SOCKETLABS_KEY', $SOCKETLABS_KEY);
			
			$siteName = $adunitSettings['tblAdunit']['sender_email'];
			$domain = $adunitSettings['tes']['es_site_domain'];
			$isTest = $adunitSettings['tes']['es_test'];
			$limit = $adunitSettings['tes']['es_limit'];
			
			
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			echo "found=". $mailing_id = $this->tblMailing->field(
			'm_id',
			array('m_ad_uid' => $m_ad_uid, 'isNewsletter' => 1),
			'm_sent_date DESC'
			);
			if(!$mailing_id)
			{
				echo $date = date('Y-m-d H:i:s');
				$this->request->data['tblMailing']['m_ad_uid'] = $m_ad_uid;
				$this->request->data['tblMailing']['m_sl'] = '';
				$this->request->data['tblMailing']['isNewsletter'] = 1;
				$this->request->data['tblMailing']['m_sent_date'] = "'" . $date . "'";
				$this->request->data['tblMailing']['m_schedule_date'] = "'" . $date . "'";
				$this->tblMailing->create();
				if ($this->tblMailing->save($this->request->data))
				{
					$mailing_id = $this->tblMailing->getLastInsertID();
				}
				} else {
				$count = $this->Dynamic->Query("SELECT count(*) as count FROM " . $table_name . " WHERE nl_isActive=1 and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." )");
				//print_r($count);
				//echo "count=". $count[0][0]['count'];
				if($count[0][0]['count'] == 0){
					$this->request->data['tblMailing']['m_ad_uid'] = $m_ad_uid;
					$this->request->data['tblMailing']['m_sl'] = '';
					$this->request->data['tblMailing']['isNewsletter'] = 1;
					$this->request->data['tblMailing']['m_sent_date'] = "'" . date('Y-m-d H:i:s') . "'";
					$this->request->data['tblMailing']['m_schedule_date'] = "'" . date('Y-m-d H:i:s') . "'";
					$this->tblMailing->create();
					if ($this->tblMailing->save($this->request->data))
					{
						$mailing_id = $this->tblMailing->getLastInsertID();
					}
				}
			}
			
			echo "final=". $mailing_id;
			
			
			
			if($isTest == "1")
			{
				$tblSubscribers = array();
				$tblSubscribers[0][$table_name]['nl_email'] = "sunny.gulati@siliconbiztech.com";
			}
			else
			{
				$tblSubscribers = $this->Dynamic->Query("SELECT * FROM " . $table_name . " WHERE nl_isActive=1 and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , ". $limit);
			}
			
			if(!empty($tblSubscribers))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber[$table_name]['nl_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$message_id = $this->tblMessage->getLastInsertID();
						$myArray = array();
						$myArray[] = array('Field'=>'DeliveryAddress', 'Value'=>''.$tblSubscriber[$table_name]['nl_email'].'');
						$myArray[] = array('Field'=>'MessageId', 'Value'=> 'DA-'.$tblSubscriber[$table_name]['nl_email'].'-SEAUTORESP');
						$strArray[] = $myArray;
					}
				}
				
				$htmlmessage = $this->get_content($url_nl . $mailing_id . "/" . $esp_id);
				
				$h2t =& new html2text($htmlmessage);
				$textVersion = $h2t->get_text();
				
				$merge_data = new stdClass();
				$merge_data->PerMessage = $strArray;
				
				$data = new stdClass();
				$data->ServerId= ''. $domain .'';
				$data->ApiKey= ''.$SOCKETLABS_KEY.'';
				$data->Messages= array(array('MergeData'=>$merge_data,
				'Subject'=>$SubjectLine,
				'To'=>array(array('EmailAddress'=>'%%DeliveryAddress%%')),
				'From'=>array('EmailAddress'=>''.$fromEmail.'', 'FriendlyName' => ''.$siteName.''),
				'HtmlBody'=>$htmlmessage,
				'TextBody'=>$textVersion,
				'MailingId'=>'DASSOCEX-'.$mailing_id,
				'MessageId'=>'%%MessageId%%'
				)); 
				$bodyJson = json_encode($data);
				//echo $bodyJson;
				
				
				//use CURL to POST the JSON to SocketLabs
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,"https://inject.socketlabs.com/api/v1/email");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt($ch, CURLOPT_POST,           1 );
				curl_setopt($ch, CURLOPT_POSTFIELDS,     $bodyJson ); 
				curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json')); 
				$result=curl_exec ($ch);
				curl_close ($ch);
				
				//process result
				if ($result==false) {
					echo 'HTTP Error';
					} else {
					$result_obj = json_decode($result);
					if ($result_obj->ErrorCode=='Success') {
						echo 'Successfully sent all messages.';
						print_r($result_obj);
						} elseif ($result_obj->ErrorCode=='Warning') {
						echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.'; 
						//not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
						} else {
						echo 'Failure Code: ' . $result_obj->ErrorCode;
					}
				}
			}
			$this->autoRender = false;
		}
		
		
		public function socketLabResponse(){
			$ServerId = $_POST['ServerId'];
			$SecretKey = $_POST['SecretKey'];
			$type = $_POST['Type'];
			if($ServerId == '13442')
			echo "t3WDi68Lmd2P5Fbe4G";
			else
			echo "So3r8K5WpEz94Zkx2H7T";
			// Send
			$message = serialize($_POST);
			//mail("sunny.gulati@siliconbiztech.com","Socketlab 3",$message);
			
			$MaillingIdArr  = $_POST['MailingId'];
			if (strpos($MaillingIdArr,'DASSOCEX') !== false) {
				$MessageId  = $_POST['MessageId'];
				$MaillingId = end(explode('-',$MaillingIdArr));
				$MaillingIdtxt = "DASSOCEX-".$MaillingId;
				$DateTime = $_POST['DateTime'];
				$timestamp = strtotime($DateTime);
				
				$reponse = serialize($_POST);
				
				switch($type) 
				{
					case 'Validation' :
					break;
					
					case 'Complaint' :
					$conditions = array(
					'UserLevelEmailPerformance.EmailAddress' => $_POST['Address'],
					'UserLevelEmailPerformance.ListId' => $MaillingIdtxt
					);
					if ($this->UserLevelEmailPerformance->hasAny($conditions)){
						$this->UserLevelEmailPerformance->updateAll(
						array('UserLevelEmailPerformance.Unsubscribed' => '1', 
						'UserLevelEmailPerformance.SpamComplaint' => '1','UserLevelEmailPerformance.UnsubscribedAt' => "'" . date("Y-m-d H:i:s", $timestamp) . "'"),       
						array('UserLevelEmailPerformance.ListId' => $MaillingIdtxt
						,'UserLevelEmailPerformance.EmailAddress' => $_POST['Address'])
						);
						$m_ad_uid = $this->tblMailing->field(
						'm_ad_uid',
						array('m_id' => split("-",$MaillingIdtxt)[1])
						);
						$aduidDFP = $this->tblMappedAdunit->field(
						'ma_ad_id',
						array('ma_uid' => $m_ad_uid)
						);
						$aduid = $this->tblAdunit->field(
						'ad_uid',
						array('ad_dfp_id' => $aduidDFP)
						);
						$pub_id = $this->tblAdunit->field(
						'publisher_id',
						array('ad_uid' => $aduid)
						);
						$table_name = $pub_id. "_" . $aduid;
						$this->Dynamic->useTable = $table_name; 
						$this->Dynamic->Query("Update " . $table_name . "  SET nl_isActive=0 WHERE nl_email='" . $_POST['Address'] ."'");
						} else {
						$this->request->data['UserLevelEmailPerformance']['ListId'] = $MaillingIdtxt;
						$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MaillingIdtxt;
						$this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $MessageId;
						$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['Address'];
						$this->request->data['UserLevelEmailPerformance']['UnsubscribedAt'] = date("Y-m-d H:i:s", $timestamp);
						$this->request->data['UserLevelEmailPerformance']['localIp'] = $_POST['LocalIp'];
						$this->request->data['UserLevelEmailPerformance']['Unsubscribed'] = 1;
						$this->request->data['UserLevelEmailPerformance']['SpamComplaint'] = 1;
						$this->request->data['UserLevelEmailPerformance']['esp_id'] = 2;
						$this->UserLevelEmailPerformance->create();
						$this->UserLevelEmailPerformance->save($this->request->data);	
						$m_ad_uid = $this->tblMailing->field(
						'm_ad_uid',
						array('m_id' => split("-",$MaillingIdtxt)[1])
						);
						$aduidDFP = $this->tblMappedAdunit->field(
						'ma_ad_id',
						array('ma_uid' => $m_ad_uid)
						);
						$aduid = $this->tblAdunit->field(
						'ad_uid',
						array('ad_dfp_id' => $aduidDFP)
						);
						$pub_id = $this->tblAdunit->field(
						'publisher_id',
						array('ad_uid' => $aduid)
						);
						$table_name = $pub_id. "_" . $aduid;
						$this->Dynamic->useTable = $table_name; 
						$this->Dynamic->Query("Update " . $table_name . "  SET nl_isActive=0 WHERE nl_email='" . $_POST['Address'] ."'");
					}
					break;
					
					case 'Failed' :
					$conditions = array(
					'UserLevelEmailPerformance.EmailAddress' => $_POST['Address'],
					'UserLevelEmailPerformance.ListId' => $MaillingIdtxt
					);
					if ($this->UserLevelEmailPerformance->hasAny($conditions)){
						$this->UserLevelEmailPerformance->updateAll(
						array('UserLevelEmailPerformance.Bounced' => '1', 'UserLevelEmailPerformance.BouncedAt' => "'" . date("Y-m-d H:i:s", $timestamp) . "'",
						'UserLevelEmailPerformance.BounceReason' => "'" . $_POST['FailureCode'] . "#" . $_POST['FailureType'] . "#" . $_POST['Reason'] . "'"),       
						array('UserLevelEmailPerformance.ListId' => $MaillingIdtxt
						,'UserLevelEmailPerformance.EmailAddress' => $_POST['Address'])
						);
						} else {
						$this->request->data['UserLevelEmailPerformance']['ListId'] = $MaillingIdtxt;
						$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MessageId;
						$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['Address'];
						$this->request->data['UserLevelEmailPerformance']['BouncedAt'] = date("Y-m-d H:i:s", $timestamp);
						$this->request->data['UserLevelEmailPerformance']['localIp'] = $_POST['LocalIp'];
						$this->request->data['UserLevelEmailPerformance']['Bounced'] = 1;
						$this->request->data['UserLevelEmailPerformance']['BounceReason'] = "'" . $_POST['FailureCode'] . "#" . $_POST['FailureType'] . "#" . $_POST['Reason'] . "'";
						$this->request->data['UserLevelEmailPerformance']['esp_id'] = 2;
						$this->UserLevelEmailPerformance->create();
						$this->UserLevelEmailPerformance->save($this->request->data);	
					}
					
					break;
					
					case 'Delivered' :
					$conditions = array(
					'UserLevelEmailPerformance.EmailAddress' => $_POST['Address'],
					'UserLevelEmailPerformance.ListId' => $MaillingIdtxt
					);
					if ($this->UserLevelEmailPerformance->hasAny($conditions)){
						//do something
						$this->UserLevelEmailPerformance->updateAll(
						array('UserLevelEmailPerformance.Sent' => '1', 'UserLevelEmailPerformance.MailingDate' => "'" . date("Y-m-d H:i:s", $timestamp) . "'"),       
						array('UserLevelEmailPerformance.ListId' => $MaillingIdtxt
						,'UserLevelEmailPerformance.EmailAddress' => $_POST['Address'])
						);
						
						} else {
						$this->request->data['UserLevelEmailPerformance']['ListId'] = $MaillingIdtxt;
						$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MaillingIdtxt;
						$this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $MessageId;
						$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['Address'];
						$this->request->data['UserLevelEmailPerformance']['MailingDate'] = date("Y-m-d H:i:s", $timestamp);
						$this->request->data['UserLevelEmailPerformance']['localIp'] = $_POST['LocalIp'];
						$this->request->data['UserLevelEmailPerformance']['Sent'] = 1;
						$this->request->data['UserLevelEmailPerformance']['esp_id'] = 2;
						$this->UserLevelEmailPerformance->create();
						$this->UserLevelEmailPerformance->save($this->request->data);
					}
					break;
					
					case 'Tracking' :
					$trackingType  = $_POST['TrackingType'];
					if($trackingType == "0")
					{
						$conditions = array(
						'UserLevelEmailPerformance.EmailAddress' => $_POST['Address'],
						'UserLevelEmailPerformance.ListId' => $MaillingIdtxt
						);
						if ($this->UserLevelEmailPerformance->hasAny($conditions)){
							$this->UserLevelEmailPerformance->incrementClickCount($MaillingIdtxt, $_POST['Address'],$DateTime);
							} else {
							$this->request->data['UserLevelEmailPerformance']['ListId'] = $MaillingIdtxt;
							$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MaillingIdtxt;
							$this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $MessageId;
							$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['Address'];
							$this->request->data['UserLevelEmailPerformance']['ClickedAt'] = date("Y-m-d H:i:s", $timestamp);
							$this->request->data['UserLevelEmailPerformance']['localIp'] = $_POST['LocalIp'];
							$this->request->data['UserLevelEmailPerformance']['Clicked'] = 1;
							$this->request->data['UserLevelEmailPerformance']['esp_id'] = 2;
							$this->UserLevelEmailPerformance->create();
							$this->UserLevelEmailPerformance->save($this->request->data);
						}
						} else if($trackingType == "1") {
						//mail("sunny.gulati@siliconbiztech.com","Socketlab 1",$message);
						$conditions = array(
						'UserLevelEmailPerformance.EmailAddress' => $_POST['Address'],
						'UserLevelEmailPerformance.ListId' => $MaillingIdtxt
						);
						if ($this->UserLevelEmailPerformance->hasAny($conditions)){
							$this->UserLevelEmailPerformance->incrementOpenCount($MaillingIdtxt, $_POST['Address'], $DateTime);
							} else {
							$this->request->data['UserLevelEmailPerformance']['ListId'] = $MaillingIdtxt;
							$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MaillingIdtxt;
							$this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $MessageId;
							$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['Address'];
							$this->request->data['UserLevelEmailPerformance']['OpenedAt'] = date("Y-m-d H:i:s", $timestamp);
							$this->request->data['UserLevelEmailPerformance']['localIp'] = $_POST['LocalIp'];
							$this->request->data['UserLevelEmailPerformance']['Opened'] = 1;
							$this->request->data['UserLevelEmailPerformance']['esp_id'] = 2;
							$this->UserLevelEmailPerformance->create();
							$this->UserLevelEmailPerformance->save($this->request->data);	
						}
						} else if($trackingType == "2") {
						$conditions = array(
						'UserLevelEmailPerformance.EmailAddress' => $_POST['Address'],
						'UserLevelEmailPerformance.ListId' => $MaillingIdtxt
						);
						if ($this->UserLevelEmailPerformance->hasAny($conditions)){
							$this->UserLevelEmailPerformance->updateAll(
							array('UserLevelEmailPerformance.Unsubscribed' => '1', 'UserLevelEmailPerformance.UnsubscribedAt' => "'" . date("Y-m-d H:i:s", $timestamp) . "'"),       
							array('UserLevelEmailPerformance.ListId' => $MaillingIdtxt
							,'UserLevelEmailPerformance.EmailAddress' => $_POST['Address'])
							);
							$m_ad_uid = $this->tblMailing->field(
							'm_ad_uid',
							array('m_id' => split("-",$MaillingIdtxt)[1])
							);
							$aduidDFP = $this->tblMappedAdunit->field(
							'ma_ad_id',
							array('ma_uid' => $m_ad_uid)
							);
							$aduid = $this->tblAdunit->field(
							'ad_uid',
							array('ad_dfp_id' => $aduidDFP)
							);
							$pub_id = $this->tblAdunit->field(
							'publisher_id',
							array('ad_uid' => $aduid)
							);
							$table_name = $pub_id. "_" . $aduid;
							$this->Dynamic->useTable = $table_name; 
							$this->Dynamic->Query("Update " . $table_name . "  SET nl_isActive=0 WHERE nl_email='" . $_POST['Address'] ."'");
							} else {
							$this->request->data['UserLevelEmailPerformance']['ListId'] = $MaillingIdtxt;
							$this->request->data['UserLevelEmailPerformance']['MessageId'] = $MaillingIdtxt;
							$this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $MessageId;
							$this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $_POST['Address'];
							$this->request->data['UserLevelEmailPerformance']['UnsubscribedAt'] = date("Y-m-d H:i:s", $timestamp);
							$this->request->data['UserLevelEmailPerformance']['localIp'] = $_POST['LocalIp'];
							$this->request->data['UserLevelEmailPerformance']['Unsubscribed'] = 1;
							$this->request->data['UserLevelEmailPerformance']['esp_id'] = 2;
							$this->UserLevelEmailPerformance->create();
							$this->UserLevelEmailPerformance->save($this->request->data);	
							$m_ad_uid = $this->tblMailing->field(
							'm_ad_uid',
							array('m_id' => split("-",$MaillingIdtxt)[1])
							);
							$aduidDFP = $this->tblMappedAdunit->field(
							'ma_ad_id',
							array('ma_uid' => $m_ad_uid)
							);
							$aduid = $this->tblAdunit->field(
							'ad_uid',
							array('ad_dfp_id' => $aduidDFP)
							);
							$pub_id = $this->tblAdunit->field(
							'publisher_id',
							array('ad_uid' => $aduid)
							);
							$table_name = $pub_id. "_" . $aduid;
							$this->Dynamic->useTable = $table_name; 
							$this->Dynamic->Query("Update " . $table_name . "  SET nl_isActive=0 WHERE nl_email='" . $_POST['Address'] ."'");
						}
					}
					break;
					
					default:
					break;				
				}
				
				
			}
			$this->autoRender = false;
			return;
		}
		
		function get_content($URL){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $URL);
			$data = curl_exec($ch);
			curl_close($ch);
			return $data;
		}
	}	
?>