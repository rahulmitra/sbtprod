<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	class CompanyController extends AppController {
		
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblProfile', 'Authake.Rule', 'Authake.tblUserConnect');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index($pop=null) {
			
			$this->System->add_css(array('/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css'),'head');
			// form posted
			$this->set('title_for_layout','All Companies');
			//$this->set('group', $this->Group->read(null, '3'));
			//$groupa = $this->User->find('all', array('joins' => array('table' => '','type' => 'inner','foreignKey' => false,'conditions'=> array('authake_groups_users.user_id = User.id'))));
			
			//$groups = $this->tblProfile->getAllConnectedProfiles();
			//$this->set('group', $groups);
		}
		
		/**
			* Main index action
		*/
		public function ajax_company_add() {
			self::add();
			$this->layout = 'withoutheader';
			$this->render('add');
			
		}
		
		public function add($pop=null) {
			if(!empty($pop)){
				$this->layout = 'withoutheader';
			}
			// add css 
			$this->System->add_css(array('/plugins/jquery-multi-select/css/multi-select.css','/plugins/icheck/skins/all.css','/plugins/select2/select2country.css','/plugins/bootstrap-datepicker/css/datepicker.css','/plugins/typeahead/typeahead.css'),'head');
			
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
			App::import('Vendor', 'src/ExampleUtils');
			App::import('Vendor', 'src/Piwik');
			$piwik = new Piwik(Configure::read('Piwik.url'), Configure::read('Piwik.token'), 3, Piwik::FORMAT_JSON);
			$this->set('title_for_layout','Add Company');
			
			//print_r($this->Authake->getUserId());
			if (!empty($this->request->data))
			{
				 
				// login name add here 
				$this->request->data['User']['login']=$this->request->data['User']['ContactName'];
				// only an admin can make an admin
				if ($this->request->data['Group']['Group'] == "1" and !in_array(1, $this->Authake->getGroupIds()))
				{
					$this->Session->setFlash(__('You cannot add a user in administrators group'), 'warning');
					$this->redirect(array('action'=>'index'));
				}
				// check user already exits in db
				$userCheck = $this->User->findByEmail($this->request->data['User']['email']);
				$userprofile_id ='';
				if(!empty($userCheck)){
					$userprofile_id =$userCheck['User']['tblProfileID'];;
				}
				
				if ((!empty($userCheck) and empty($this->request->data['tblUserConnect']['tblProfileId'])) ||  (!empty($userCheck) and $userprofile_id !=$this->request->data['tblUserConnect']['tblProfileId'])){
					
					$this->Session->setFlash(__('"'.$this->request->data['User']['email'].'" email is already exists'), 'warning');
					
				}else{
					 if(!empty($this->request->data['tblUserConnect']['tblProfileId']) && !empty($this->request->data['tblUserConnect']['userId'])){
						$this->request->data['tblUserConnect']['accepted'] = 1;
						$this->request->data['tblUserConnect']['datemade'] = date("Y-m-d H:i:s");
						$this->request->data['tblUserConnect']['requestedBy'] = $this->Authake->getUserId();
						$this->tblUserConnect->create();
						$advertiserCompanyId = $this->tblProfile->field('dfp_company_id', array('profileID' =>$this->request->data['tblUserConnect']['tblProfileId']));
						if($this->tblUserConnect->save($this->request->data))
						{
							$this->Session->setFlash(__('You have successfully connected with the user. <a href="/insertionorder/add/company_id:'.$advertiserCompanyId.'" id="sample_editable_1_new">Add an Order for this Company</i></a> / <a href="/inventory_management/addAdUnit/company_id:'.$this->request->data['tblUserConnect']['userId'].'" id="sample_editable_1_new">Add a New Ad-Unit for this Company</a>'), 'success');
							$this->redirect(array('action'=>'index'));
						}
					}
					else if(!empty($this->request->data['tblUserConnect']['tblProfileId']) && empty($this->request->data['tblUserConnect']['userId'])) {
						
						$randomString  = "7654321";
						
						$p = $randomString;
						$this->request->data['User']['creator_user_id'] = $this->Authake->getUserId();;
						$this->request->data['User']['password'] = md5($p);
						$this->request->data['User']['tblProfileID'] = (!empty($this->request->data['tblUserConnect']['tblProfileId']))?$this->request->data['tblUserConnect']['tblProfileId']:0;
						//$this->request->data['User']['disable'] = 1;
						$this->User->create();
						if ($this->User->save($this->request->data))
						{
							try {
								$user = new DfpUser();
								// Log SOAP XML request and response.
								$user->LogDefaults();
								
								// Get the ContactService.
								$contactService = $user->GetService('ContactService', 'v201608');
								$advertiserCompanyId = $this->tblProfile->field('dfp_company_id', array('profileID' =>$this->request->data['tblUserConnect']['tblProfileId']));
								
								// Create an advertiser contact.
								$advertiserContact = new Contact();
								$advertiserContact->name = $this->request->data['User']['ContactName'];
								$advertiserContact->email = $this->request->data['User']['email'];
								$advertiserContact->companyId = $advertiserCompanyId;
								// Create the contacts on the server.
								$contacts = $contactService->createContacts(array($advertiserContact));
								
								// Display results.
								if (isset($contacts)) {
									foreach ($contacts as $contact) {
										$this->request->data['User']['dfp_contact_id'] = $contact->id;
									}
									} else {
									$this->request->data['User']['dfp_contact_id'] = 0;
								}
								$this->User->updateAll(
								array('User.dfp_contact_id' => $this->request->data['User']['dfp_contact_id']),
								array('User.id' => $this->User->getLastInsertID()));
								$this->request->data['tblUserConnect']['accepted'] = 1;
								$this->request->data['tblUserConnect']['userId'] = $this->User->getLastInsertID();
								$this->request->data['tblUserConnect']['datemade'] = date("Y-m-d H:i:s");
								$this->request->data['tblUserConnect']['requestedBy'] = $this->Authake->getUserId();
								$this->tblUserConnect->create();
								if($this->tblUserConnect->save($this->request->data))
								{
									$this->Session->setFlash(__('You have successfully connected with the user. <a href="/insertionorder/add/company_id:'.$advertiserCompanyId.'" id="sample_editable_1_new">Add an Order for this Company</i></a> / <a href="/inventory_management/addAdUnit/company_id:'.$this->User->getLastInsertID().'" id="sample_editable_1_new">Add a New Ad-Unit for this Company</a>'), 'success');
								}
								$this->redirect(array('action'=>'index'));
							}
							catch (OAuth2Exception $e) {
								ExampleUtils::CheckForOAuth2Errors($e);
								} catch (ValidationException $e) {
								ExampleUtils::CheckForOAuth2Errors($e);
								} catch (Exception $e) {
								print $e->getMessage() . "\n";
							}
						}
						}else {
						
						//$length = 6;
						//$randomString = substr(str_shuffle(md5(time())),0,$length);
						$randomString  = "7654321";
						
						$p = $randomString;
						$this->request->data['User']['tblProfileID'] = 0;
						$this->request->data['User']['creator_user_id'] =$this->Authake->getUserId();;
						$this->request->data['User']['dfp_contact_id'] = null;
						$this->request->data['User']['password'] = md5($p);
						//$this->request->data['User']['disable'] = 1;
						$this->User->create();
						$this->tblProfile->create();
						if ($this->User->save($this->request->data))
						{
							
							try {
								// Get DfpUser from credentials in "../auth.ini"
								// relative to the DfpUser.php file's directory.
								$user = new DfpUser();
								
								// Log SOAP XML request and response.
								$user->LogDefaults();
								
								// Get the CompanyService.
								$companyService = $user->GetService('CompanyService', 'v201608');
								// Get the ContactService.
								$contactService = $user->GetService('ContactService', 'v201608');
								
								// Create an array to store local company objects.
								$companies = array();
								
								$company = new Company();
								$company->name = $this->request->data['tblProfile']['CompanyName'];
								$company->type = 'ADVERTISER';
								
								$companies[] = $company;
								
								// Create the companies on the server.
								$companies = $companyService->createCompanies($companies);
								
								// Display results.
								$advertiserCompanyId='';
								if (isset($companies)) {
									foreach ($companies as $company) {
										$advertiserCompanyId = $this->request->data['tblProfile']['dfp_company_id'] = $company->id;
									}
									// Create an advertiser contact.
									$advertiserContact = new Contact();
									$advertiserContact->name = $this->request->data['User']['ContactName'];
									$advertiserContact->email = $this->request->data['User']['email'];
									$advertiserContact->companyId = $advertiserCompanyId;
									// Create the contacts on the server.
									$contacts = $contactService->createContacts(array($advertiserContact));
									
									// Display results.
									if (isset($contacts)) {
										foreach ($contacts as $contact) {
											$this->request->data['User']['dfp_contact_id'] = $contact->id;
										}
										} else {
										$this->request->data['User']['dfp_contact_id'] = 0;
									}
									} else {
									$this->request->data['tblProfile']['dfp_company_id'] = 0;
								}
								} catch (OAuth2Exception $e) {
								ExampleUtils::CheckForOAuth2Errors($e);
								} catch (ValidationException $e) {
								ExampleUtils::CheckForOAuth2Errors($e);
								} catch (Exception $e) {
								//print $e->getMessage() . "\n";
								$this->Session->setFlash(__($e->getMessage()), 'error'); 
							}
							$this->User->updateAll(
							array('User.dfp_contact_id' => $this->request->data['User']['dfp_contact_id']),
							array('User.id' => $this->User->getLastInsertID()));
							$piwikSiteID = $piwik->addSite($this->request->data['tblProfile']['CompanyName'],$this->request->data['tblProfile']['CompanyURL']);
							$this->request->data['tblProfile']['tracksite_id'] = (!empty($piwikSiteID))?$piwikSiteID:0;
							$this->request->data['tblProfile']['user_id'] = $this->User->getLastInsertID();
							$this->request->data['tblProfile']['owner_user_id'] = $this->Authake->getUserId();
							$this->tblProfile->save($this->request->data);
							
							$this->User->updateAll(
							array('User.tblProfileID' => $this->tblProfile->getLastInsertID()),
							array('User.id' => $this->User->getLastInsertID()));
							$this->Session->setFlash(__('The Company has been saved. <a href="/insertionorder/add/company_id:'.$advertiserCompanyId.'" id="sample_editable_1_new">Add an Order for this Company</i></a> / <a href="/inventory_management/addAdUnit/company_id:'.$this->User->getLastInsertID().'" id="sample_editable_1_new">Add a New Ad-Unit for this Company</a>'), 'success');
							if(empty($pop)){
								$this->redirect(array('action'=>'index'));
							}else{
								$this->request->data=array();
								$this->redirect(array('action'=>'add',$pop));
							}
							
						}
						else
						{
							$this->Session->setFlash(__('The Company could not be saved. Please, try again.'), 'error');
						}
					}
				}
				
				
				/*$options['conditions'] = array(
					'Ugroup.group_id' => '5','tblProfile.owner_user_id' => array($this->Authake->getUserId()) );
					
					$options['joins'] = array(
					array('table' => 'authake_groups_users',
					'alias' => 'Ugroup',
					'type' => 'INNER',
					'conditions' => array(
					'User.id = Ugroup.user_id'
					)),
					array('table' => 'tbl_profiles',
					'alias' => 'tblProfile',
					'type' => 'INNER',
					'conditions' => array(
					'User.id = tblProfile.user_id'
					)));
					
					$options['fields'] = array('tblProfile.CompanyName','tblProfile.profileID'); 
					
					$company = $this->User->find('all', $options);
				$this->set('company', $company);*/
				
				$this->request->data['User']['password'] = '';
				$groups = $this->User->Group->find('list');
				$this->set(compact('groups'));
				
			}
			$this->set(compact('pop'));
		}
		
			public function edit($user_id=null){
			
			/*App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
				App::import('Vendor', 'src/ExampleUtils');
				App::import('Vendor', 'src/Piwik');
			$piwik = new Piwik(Configure::read('Piwik.url'), Configure::read('Piwik.token'), 3, Piwik::FORMAT_JSON);*/
			$this->System->add_css(array('/plugins/jquery-multi-select/css/multi-select.css','/plugins/icheck/skins/all.css','/plugins/select2/select2country.css','/plugins/bootstrap-datepicker/css/datepicker.css','/plugins/typeahead/typeahead.css'),'head');
			
			$options['conditions'] = array(
			'Ugroup.group_id' => '5','User.id' => $user_id  );
			
			$options['joins'] = array(
			array('table' => 'authake_groups_users',
			'alias' => 'Ugroup',
			'type' => 'INNER',
			'conditions' => array(
			'User.id = Ugroup.user_id'
			)) ,
			array('table' => 'tbl_user_connects',
			'alias' => 'tblUserConnect',
			'type' => 'LEFT',
			'conditions' => array(
			'User.id = tblUserConnect.userId',
			'tblUserConnect.requestedBy ='.$this->Authake->getUserId() ,
			))
			);
			
			$options['fields'] = array('User.*','Ugroup.*','tblUserConnect.*'); 
			//$this->User->recursive=-1;
			$user = $this->User->find('first', $options);
			if(empty($user['tblProfile'])){
				$userProfile = $this->tblProfile->find('first', array('conditions'=>array('tblProfile.profileID'=>$user['tblUserConnect']['tblProfileId'])));
				$userProfile['tblProfile']['disable']=1;
				$user['tblProfile']=$userProfile['tblProfile'];
				
				}else{
				$user['tblProfile']['disable']=0;
			}
			
			
			
			$this->set('title_for_layout','Edit Company');
			try {
				if (!empty($this->request->data))
				{	// only an admin can make an admin
					if ($this->request->data['Group']['Group'] == "1" and !in_array(1, $this->Authake->getGroupIds()))
					{
						$this->Session->setFlash(__('You cannot add a user in administrators group'), 'warning');
						$this->redirect(array('action'=>'index'));
					}
					
					if ($this->request->data['User']['id'] !=$user_id )
					{
						$this->Session->setFlash(__('Something is going wrong'), 'warning');
						$this->redirect(array('action'=>'index'));
					}
					
					
					unset($this->request->data['User']['login']);
					unset($this->request->data['User']['password']);
					
					$this->User->create();
					$this->User->save($this->request->data);  
					
					if($user['tblProfile']['disable']==0){
						//pr($this->request->data);die;
						//set here primary key (Note:-  not set in model problem)
						$this->tblProfile->primaryKey ='profileID';
						$this->tblProfile->create();
						$this->tblProfile->save($this->request->data);
						
					}
					
					$this->Session->setFlash(__('The Company has been updated. <a href="/insertionorder/add/company_id:'.$user['tblProfile']['dfp_company_id'].'" id="sample_editable_1_new">Add an Order for this Company</i></a> / <a href="/inventory_management/addAdUnit/company_id:'.$user['User']['id'].'" id="sample_editable_1_new">Add a New Ad-Unit for this Company</a>'), 'success');
					$this->redirect(array('action'=>'index'));
				} 
				}catch (Exception $e) {
				print $e->getMessage() . "\n"; 
				
			}
			$this->request->data  = $user;
			$this->request->data['User']['password'] = '';
			$groups = $this->User->Group->find('list');
			$this->set(compact('groups'));
			$this->set(compact('user_id'));
			
			}
			public function getCompanyNameByQuery() {
				$this->autoRender = false;
				$this->RequestHandler->respondAs('json');
				
				// get the search term from URL
				$term = $this->request->query['q'];
				$users = $this->tblProfile->find('all',array(
				'conditions' => array(
				'tblProfile.CompanyName LIKE' => '%'.$term.'%'),'order'=>array('tblProfile.CompanyName')
				
				));
				
				// Format the result for select2
				$result = array();
				foreach($users as $key => $user) {
					array_push($result, $user['tblProfile']['CompanyName']);
				}
				$users = $result;
				
				echo json_encode($users);
			}
			 
			
			
			public function getCompanyInfoByEmail() {
				$this->autoRender = false;
				$email_id = $this->request->data['email_id'];
				
				$option_order['joins'] = array(
				array('table' => 'tbl_profiles',
				'alias' => 'tblp',
				'type' => 'INNER',
				'conditions' => array(
				'User.tblProfileID = tblp.ProfileID',
				'User.email' => array($email_id))
				));
				$option_order['fields'] = array('User.*', 'tblp.*');	
				$orders = $this->User->find('all', $option_order);
				$orders_array = array();
				$count = 0;
				
				foreach($orders as $order)
				{
					$orders_array[$count]['ContactName'] = $order['User']['ContactName'];
					$orders_array[$count]['CompanyName'] = $order['tblp']['CompanyName'];
					$orders_array[$count]['CompanyURL'] = $order['tblp']['CompanyURL'];
					$orders_array[$count]['CompanyDesc'] = $order['tblp']['CompanyDesc'];
					$orders_array[$count]['PrimaryPhone'] = $order['tblp']['PrimaryPhone'];
					$orders_array[$count]['Address1'] = $order['tblp']['Address1'];
					$orders_array[$count]['Address2'] = $order['tblp']['Address2'];
					$orders_array[$count]['City'] = $order['tblp']['City'];
					$orders_array[$count]['State_Province'] = $order['tblp']['State_Province'];
					$orders_array[$count]['ZIP_Postal'] = $order['tblp']['ZIP_Postal'];
					$orders_array[$count]['Country'] = $order['tblp']['Country'];
					$orders_array[$count]['profileID'] = $order['tblp']['profileID'];
					$orders_array[$count]['id'] = $order['User']['id'];
					$count++;
				}
				
				//print_r($orders);
				return json_encode($orders_array);
			}
			
			
			public function getCompanyInfoByName() {
				$this->autoRender = false;
				$CompanyName = $this->request->data['name'];
				
				$option_order['joins'] = array(
				array('table' => 'tbl_profiles',
				'alias' => 'tblp',
				'type' => 'INNER',
				'conditions' => array(
				'User.tblProfileID = tblp.ProfileID',
				'tblp.CompanyName' => array($CompanyName))
				));
				$option_order['fields'] = array('User.*', 'tblp.*');	
				$orders = $this->User->find('first', $option_order);
				$orders_array = array();
				if(isset($orders['tblp']))
				$orders_array[0]['profileID'] = $orders['tblp']['profileID'];
				else 
				$orders_array[0]['profileID'] = "";
				//print_r($orders);
				
				$options['conditions'] = array(
				'Ugroup.group_id' => '5','tblProfile.owner_user_id' => array($this->Authake->getUserId()) );
				
				$options['joins'] = array(
				array('table' => 'authake_groups_users',
				'alias' => 'Ugroup',
				'type' => 'INNER',
				'conditions' => array(
				'User.id = Ugroup.user_id'
				)),
				array('table' => 'tbl_profiles',
				'alias' => 'tblProfile',
				'type' => 'INNER',
				'conditions' => array(
				'User.id = tblProfile.user_id'
				)));
				
				$options['fields'] = array('tblProfile.*');
				
				$existingCompany = $this->User->find('all', $options);
				
				
				foreach($existingCompany as $order)
				{
					if($orders_array[0]['profileID'] == $order['tblProfile']['profileID']) {
						$orders_array[0]['profileID'] = -1;
					}
				}
				
				echo  json_encode($orders_array);
				
			}
		public function company_ajax_list(){
				$loggedUserId = $this->Authake->getUserId();
				$usersconnected = $this->tblUserConnect->find('all', array('conditions' => array('OR' => array('requestedBy' => $loggedUserId, 'userId' => $loggedUserId), 'accepted' => '1')));
				$loggedProfileID = $this->User->field('tblProfileID', array('id' =>$loggedUserId));
				$connectedUser = array();
				foreach($usersconnected as $user)
				{
					$connectedUser[] = $user['tblUserConnect']['userId'];			
					$connectedUser[] = $user['tblUserConnect']['requestedBy'];		
				}
				$connectedUser = array_unique($connectedUser);
				$options['conditions'][] = array(
					'Ugroup.group_id' => '5', 
					'OR' => array(
					array('tblProfile.owner_user_id' => array($loggedUserId)),
					array('User.id' => $connectedUser),
					array('tblProfile.profileID' => $loggedProfileID)
					),
				'NOT'=>array('tblProfile.dfp_company_id'=>''));
				if(!empty($owner_user_id)){
					$options['conditions'][]= array('tblProfile.owner_user_id'=>$owner_user_id);
				}
				$options['joins'] = array(
				array('table' => 'authake_users',
				'alias' => 'User',
				'type' => 'INNER',
				'conditions' => array(
				'User.tblProfileID = tblProfile.profileID'
				)),
				array('table' => 'authake_groups_users',
				'alias' => 'Ugroup',
				'type' => 'INNER',
				'conditions' => array(
				'User.id = Ugroup.user_id'
				)));
				$options['fields'] = array('tblProfile.*','User.*');
				
				// sorting order 
				if($this->request->data['order'][0]['column']==0){
					$options['order']=array('tblProfile.CompanyName'=>$this->request->data['order'][0]['dir']);
				}
				if($this->request->data['order'][0]['column']==1){
					$options['order']=array('User.email'=>$this->request->data['order'][0]['dir']);
				}
				if($this->request->data['order'][0]['column']==2){
					$options['order']=array('User.created'=>$this->request->data['order'][0]['dir']);
				}
				if($this->request->data['order'][0]['column']==3){
					$options['order']=array('User.expire_account'=>$this->request->data['order'][0]['dir']);
				}
				//searching 
				if(!empty($this->request->data['search']['value'])){
					$options['conditions']['OR'][]=array('tblProfile.CompanyName like'=>'%'.$this->request->data['search']['value'].'%');
				}
				$totalcompany =  $this->tblProfile->find('count', $options);
				$options['limit']=(!empty($this->request->data['length']))?$this->request->data['length']:10;
				$options['offset']=(!empty($this->request->data['start']))?$this->request->data['start']:0;
				 
				$resultsdata  = $this->tblProfile->find('all', $options);
				
				
				$data =array();
				App::uses('TimeHelper', 'View/Helper');
				$timehelper = new TimeHelper(new View());
				if(!empty($resultsdata)){
					foreach($resultsdata as $company){
						  if ($company['User']['disable']){
								$status= "<span class='label label-important'>Disabled</span>";
							} else {		
								$exp = $company['User']['expire_account'];
								if ($exp != '0000-00-00' && $timehelper->fromString($exp) < time()) $status= "<span class='label label-success'>Approved</span>";
							}  
						  
						$data[]=array($company['tblProfile']['CompanyName'],"<a href='mailto:{$company['User']['email']}'>{$company['User']['email']}</a>",$timehelper->format('d/m/Y', $company['User']['created']),$status,'<div class="btn-group"><a data-toggle="dropdown" href="javascript:;" class="btn btn-circle btn-default" aria-expanded="true"><i class="fa fa-share"></i> Action <i class="fa fa-angle-down"></i></a><ul class="dropdown-menu pull-right"><li><a style="margin-right:5px;" href="'.Router::url(array('controller'=>'insertionorder','action'=>'add/company_id:'. $company['tblProfile']['dfp_company_id'])).'"><i class="fa fa-codepen"></i>Create Order </a></li><li><a style="margin-right:5px;" href="'.Router::url(array('controller'=>'orders','action'=>'index',$company['User']['id'])).'"><i class="fa fa-codepen"></i>View Order</a></li><li><a style="margin-right:5px;" href="'.Router::url(array('controller'=>'inventory_management','action'=>'index',0,$company['User']['id'])).'"><i class="fa fa-database"></i>Manage Ad-Units </a></li><li><a style="margin-right:5px;" href="'.Router::url(array('controller'=>'company','action'=>'edit', $company['User']['id'])).'"><i class="icon-pencil"></i>Edit </a></li></ul></div>');	
					}
				}
				    $json_data = array(
						"draw"            => intval( (!empty($this->request->data['draw']))?$this->request->data['draw']:1 ),   
						"recordsTotal"    => intval( $totalcompany ),  
						"recordsFiltered" => intval($totalcompany),
						"data"            => $data   // total data array
						);
					echo  json_encode($json_data); 
				die;
			}	
		}						
