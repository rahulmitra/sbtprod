<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	class InventoryManagementController extends AppController {
		
		/**
			* Components
		*/
		var $uses = array('Authake.Group', 'Authake.User', 'Authake.tblProfile', 'Authake.tblAdunit', 'Authake.tblPlacement', 'Authake.Rule', 'Authake.tblProductType', 'Authake.tblMappedAdunit', 'Authake.tblMailing', 'Authake.tblAdunitsType', 'Authake.tblAdunitsPlacement', 'Authake.tblReportDay', 'Authake.tblMediaType', 'tblKmaResponse', 'Dynamic');
		var $components = array('RequestHandler', 'Authake.Filter', 'Session', 'Commonfunction', 'Chart'); // var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc')); //var $scaffold;
		var $helper = array('Gchart');
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index($adunit = 0, $user_id = null) {
			$this->loadModel('FieldMapping');
			$this->System->add_css(array('/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css'), 'head');
			// form posted
			$this->set('title_for_layout', 'All AdUnits');
			if (!empty($user_id)) {
				$ids = array($user_id);
				$options['conditions'][] = array('User.id' => $user_id);
			}
			$ids = $this->tblProfile->getAllConnectedProfileUserIds();
			
			
			$options['conditions'][] = array(
            'tblAdunit.ad_parent_id' => 0,
            'tblAdunit.ad_isactive' => 1,
            'or' => array(
			'tblAdunit.owner_user_id' => array($this->Authake->getUserId()),
			'and' => array(
			'tblAdunit.owner_user_id' => $ids,
			'tblAdunit.ad_isPublicVisible' => 1))
			);
			
			$options['joins'] = array(
            array('table' => 'authake_users',
			'alias' => 'User',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.publisher_id = User.id'
			)),
            array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'User.tblProfileID = tblp.profileID'
			)),
            array('table' => 'tbl_product_types',
			'alias' => 'tbpt',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.ad_ptype_id = tbpt.ptype_id'
			)),
            array('table' => 'tbl_media_types',
			'alias' => 'tbmt',
			'type' => 'INNER',
			'conditions' => array(
			'tbmt.mtype_id = tbpt.ptype_m_id'
			)));
			
			$options['fields'] = array('tblAdunit.*', 'tblp.CompanyName', 'tbpt.ptype_name', 'tbmt.mtype_name', 'tbmt.mtype_id');
			
			$tblAdunits = $this->tblAdunit->find('all', $options);
			//print_r($tblAdunits);
			
			$option_feilds['conditions'] = array('FieldMapping.adunit_id' => $adunit);
			$AllFields = $this->FieldMapping->find('all', $option_feilds);
			$field = '';
			foreach ($AllFields as $fields) {
				$field .= $fields['FieldMapping']['field_lable'] . '={VALUE}&';
			}
			
			
			$subscribersCount = array();
			
			$nlChilds = array();
			$ad_ptype_id = '';
			$ad_uid = '';
			
			foreach ($tblAdunits as $nllist) {
				$options1['conditions'] = array(
                'tblAdunit.owner_user_id' => $this->Authake->getUserId(),
                'tblAdunit.ad_parent_id' => $nllist['tblAdunit']['adunit_id']
				);
				
				$nlchilds[$nllist['tblAdunit']['ad_uid']] = $this->tblAdunit->find('all', $options1);
				if ($nllist['tblAdunit']['adunit_id'] == $adunit) {
					$ad_ptype_id = $nllist['tblAdunit']['ad_ptype_id'];
					$ad_uid = $nllist['tblAdunit']['ad_uid'];
				}
			}
			$this->set('ad_ptype_id', $ad_ptype_id);
			$this->set('group', $tblAdunits);
			$this->set('adunit', $adunit);
			$this->set('ad_uid', $ad_uid);
			$this->set('field', $field);
			if ($nlChilds)
            $this->set('nlchilds', $nlchilds);
			else
            $this->set('nlchilds', '');
		}
		
		public function editAdUnit($adunit_id = null) {
			
			// load model 
			$this->loadModel('Authake.tblProductType');
			$this->loadModel('FieldMapping');
			$this->loadModel('AdunitLayout');
			
			$this->set('title_for_layout', 'Edit Publisher Ad Unit');
			$ids = $this->tblProfile->getAllConnectedProfileUserIds();
			$options['conditions'] = array(
            'tblAdunit.ad_parent_id' => 0,
            'tblAdunit.adunit_id' => $adunit_id,
            'or' => array(
			'tblAdunit.owner_user_id' => array($this->Authake->getUserId()),
			'and' => array(
			'tblAdunit.owner_user_id in' => $ids,
			'tblAdunit.ad_isPublicVisible' => 1)),
            'tblAdunit.ad_isactive ' => 1
			);
			
			$options['joins'] = array(
            array('table' => 'authake_users',
			'alias' => 'User',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.publisher_id = User.id'
			)),
            array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'User.tblProfileID = tblp.profileID'
			)),
            array('table' => 'tbl_product_types',
			'alias' => 'tbpt',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.ad_ptype_id = tbpt.ptype_id'
			)),
            array('table' => 'tbl_media_types',
			'alias' => 'tbmt',
			'type' => 'INNER',
			'conditions' => array(
			'tbmt.mtype_id = tbpt.ptype_m_id'
			)));
			
			$options['fields'] = array('tblAdunit.*', 'tblp.CompanyName', 'tbpt.ptype_name', 'tbmt.mtype_name', 'tbmt.mtype_id');
			
			$tblAdunits = $this->tblAdunit->find('first', $options);
			if (empty($tblAdunits)) {
				$this->Session->setFlash(__('Sorry, Something went wrong!'), 'error');
				$this->redirect(array('controller' => 'inventory_management', 'action' => 'index'));
			}
			$this->set('tblAdunits', $tblAdunits);
			//pr($tblAdunits); exit();
			// template layout
			$tblAdunitsLayout = array();
			$tblAdunitsFieldMapping = array();
			
			if ($tblAdunits['tblAdunit']['ad_ptype_id'] == 6) {
				$tblAdunitsLayout = $this->AdunitLayout->find('first', array('conditions' => array('AdunitLayout.adunit_id' => $tblAdunits['tblAdunit']['adunit_id'])));
				$tblAdunitsFieldMapping = $this->FieldMapping->find('all', array('conditions' => array('FieldMapping.adunit_id' => $tblAdunits['tblAdunit']['adunit_id'])));
			}
			// if selected email dedicated
			$adesplist = array();
			if ($tblAdunits['tblAdunit']['ad_ptype_id'] == 2) {
				$adesplist = ($tblAdunits['tblAdunit']['ad_esp'] == 2) ? (self::getSocketLabListArray()) : (self::getLyrisListArray());
			}
			$this->set('adesplist', $adesplist);
			$this->set('tblAdunitsFieldMapping', $tblAdunitsFieldMapping);
			$this->set('tblAdunitsLayout', $tblAdunitsLayout);
			
			// vendor 
			
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
			App::import('Vendor', 'src/ExampleUtils');
			// Get DfpUser from credentials in "../auth.ini"
			// relative to the DfpUser.php file's directory.
			$user = new DfpUser();
			// Log SOAP XML request and response.
			$user->LogDefaults();
			// Get the InventoryService.
			$inventoryService = $user->GetService('InventoryService', 'v201608');
			// Create a statement to select all ad unit sizes.
			$statementBuilder = new StatementBuilder();
			// Get all ad unit sizes by statement.
			$adUnitSizes = $inventoryService->getAdUnitSizesByStatement($statementBuilder->ToStatement());
			$adUnit_size['text/html'] = 'Text / HTML';
			if (!empty($adUnitSizes)) {
				foreach ($adUnitSizes as $data) {
					if (!empty($data->fullDisplayString))
                    $adUnit_size[$data->fullDisplayString] = $data->fullDisplayString;
				}
			}
			
			$this->set('adUnit_size', $adUnit_size);
			self::adunitslist();
			// get publisher list 
			self::ProductTypeList();
			self::isSuccessLyris();
			
			$publisher = $this->tblProfile->getListConnectedProfiles();
			$this->set('publisher', $publisher);
			
			$adUnitTypes = $this->tblAdunitsType->getAdunitTypesList();
			$this->set('adUnitTypes', $adUnitTypes);
			
			
			$adUnitPlacements = $this->tblAdunitsPlacement->getAdunitPlacementsList();
			$this->set('adUnitPlacements', $adUnitPlacements);
			$this->set('adunit_id', $adunit_id);
			//pr($tblAdunits);
			// updating data 
			if (!empty($this->request->data)) {
				
				$this->request->data['tblAdunit']['owner_user_id'] = $this->Authake->getUserId();
				$ad_unit_sizes = $this->request->data['tblAdunit']['ad_unit_sizes'];
				$ad_parent_id = $this->request->data['tblAdunit']['ad_parent_id'];
				if ($ad_parent_id != 0) {
					$options['conditions'] = array(
                    'tblAdunit.adunit_id' => $ad_parent_id);
					$tblAdunits = $this->tblAdunit->find('first', $options);
					$this->request->data['tblAdunit']['publisher_id'] = $tblAdunits['tblAdunit']['publisher_id'];
					$this->request->data['tblAdunit']['ad_ptype_id'] = $tblAdunits['tblAdunit']['ad_ptype_id'];
					$this->request->data['tblAdunit']['ad_placement_id'] = $tblAdunits['tblAdunit']['ad_placement_id'];
					$this->request->data['tblAdunit']['ad_adtype_id'] = $tblAdunits['tblAdunit']['ad_adtype_id'];
					$this->request->data['tblAdunit']['ad_isExternal'] = $tblAdunits['tblAdunit']['ad_isExternal'];
					$this->request->data['tblAdunit']['ad_auto_optimize'] = (!empty($this->request->data['tblAdunit']['ad_audience_profile'])) ? $this->request->data['tblAdunit']['ad_auto_optimize'] : 0;
				}
				$this->tblAdunit->create();
				$adunitname = 'Ad_Unit_' . $this->request->data['tblAdunit']['adunit_name'];
				if ($this->tblAdunit->save($this->request->data['tblAdunit'])) {
					// save template  
					// Save field maping
					$PostData = $this->request->data;
					if (!empty($PostData) and $PostData['tblAdunit']['ad_ptype_id'] == 6) {
						$this->loadModel('FieldMapping');
						$this->loadModel('AdunitLayout');
						
						if (!empty($PostData['AdunitLayout'])) {
							$PostData['AdunitLayout']['adunit_id'] = $this->tblAdunit->id;
							$this->AdunitLayout->create();
							$this->AdunitLayout->save($PostData['AdunitLayout']);
						}
						// delete all filed mapping 
						$this->FieldMapping->deleteAll(array('FieldMapping.adunit_id' => $this->tblAdunit->id));
						$i = 1;
						foreach ($PostData['FieldMapping']['field_description'] as $key => $value) {
							if (!empty($PostData['FieldMapping']['status'][$key])) { // save only who checked
								$FieldData = array();
								$available_status = (!empty($PostData['FieldMapping']['status'][$key])) ? 1 : 0;
								$field_name = '';
								if ($PostData['FieldMapping']['custom'][$key] == 1) {
									$field_name = "cvalue" . $i++;
									} else {
									$field_name = (!empty($PostData['FieldMapping']['field_name'][$key])) ? $PostData['FieldMapping']['field_name'][$key] : '';
								}
								
								$FieldData['FieldMapping'] = array('adunit_id' => $this->tblAdunit->id, 'field_description' => $value, 'field_lable' => $PostData['FieldMapping']['field_lable'][$key]
                                , 'field_type' => (!empty($PostData['FieldMapping']['field_type'][$key])) ? $PostData['FieldMapping']['field_type'][$key] : 1,
                                'field_format' => (!empty($PostData['FieldMapping']['field_format'][$key])) ? $PostData['FieldMapping']['field_format'][$key] : '',
                                'field_name' => $field_name
                                , 'field_custom_text' => (!empty($PostData['FieldMapping']['field_custom_text'][$key])) ? $PostData['FieldMapping']['field_custom_text'][$key] : ''
                                , 'custom' => $PostData['FieldMapping']['custom'][$key], 'status' => $available_status, 'date' => date('Y-m-d H:i:s'));
								$this->FieldMapping->create();
								$this->FieldMapping->save($FieldData);
							}
						}
						try {
							$product_id = $this->request->data['tblAdunit']['ad_ptype_id'];
							if ($product_id == 13 || $product_id == 2) {
								$ad_uid = $this->tblAdunit->field(
								'ad_uid', array('ad_created_at <' => date('Y-m-d H:i:s')), 'ad_created_at DESC'
								);
								$ad_uid_table = $this->request->data['tblAdunit']['publisher_id'] . "_" . $ad_uid;
								
								$sql_table = "CREATE TABLE IF NOT EXISTS " . $ad_uid_table . " (
								`nl_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
								`nl_adunit_uuid` char(50) NOT NULL,
								`nl_email` varchar(100) NOT NULL,
								`nl_fname` varchar(100) DEFAULT NULL,
								`nl_lname` varchar(100) DEFAULT NULL,
								`nl_ispromotion` int(11) DEFAULT '1',
								`nl_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
								PRIMARY KEY (`nl_id`)
								) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
								$this->Dynamic->query($sql_table);
							}
							
							if ($ad_unit_sizes == "text/html") {
								$this->request->data['tblAdunit']['ad_unit_sizes'] = "1x1";
							}
							// Get the NetworkService.
							$networkService = $user->GetService('NetworkService', 'v201608');
							
							// Get the effective root ad unit's ID for all ad units to be created under.
							$network = $networkService->getCurrentNetwork();
							$effectiveRootAdUnitId = $network->effectiveRootAdUnitId;
							
							//$adunitname = 'Ad_Unit_'.$this->request->data['tblAdunit']['adunit_name'];
							$pubid = $this->request->data['tblAdunit']['publisher_id'];
							$ad_desc = $this->request->data['tblAdunit']['ad_desc'];
							$ad_size = $this->request->data['tblAdunit']['ad_unit_sizes'];
							
							
							// Create an array to store local ad unit objects.
							/*
								$adUnits = array();
								
								$adUnit = new AdUnit();
								$adUnit->name = $adunitname;
								$adUnit->parentId = $effectiveRootAdUnitId;
								$adUnit->description = $ad_desc;
								$adUnit->targetWindow = 'BLANK';
								
								// Create ad unit size.
								$adUnitSize = new AdUnitSize();
								
								if (strpos($ad_size,'x') !== false)
								$ad_sizes = explode("x",$ad_size);
								else if (strpos($ad_size,':') !== false)
								$ad_sizes = explode(":",$ad_size);
								
								$adUnitSize->size = new Size($ad_sizes[0], $ad_sizes[1], FALSE);
								$adUnitSize->environmentType = 'BROWSER';
								
								// Set the size of possible creatives that can match this ad unit.
								$adUnit->adUnitSizes = array($adUnitSize);
								
								$adUnits[] = $adUnit;
								
								// Create the ad units on the server.
								$adUnits = $inventoryService->createAdUnits($adUnits);
								
								// Display results.
								if (isset($adUnits)) {
								foreach ($adUnits as $adUnit) {
								$adUnitid = $this->tblAdunit->getLastInsertID();
								$this->tblAdunit->updateAll(
								array('ad_dfp_id' => "$adUnit->id"),
								array('adunit_id' => $adUnitid)
								);
								
								}
								} else {
								print "No ad units created.\n";
							} */
							} catch (OAuth2Exception $e) {
							ExampleUtils::CheckForOAuth2Errors($e);
						}
					}
					$this->Session->setFlash(__('An AdUnit with with ID "' . $this->tblAdunit->id . '" and name "' . $adunitname . "\" was created. Copy the following tags based on your implementation to start delivering line-items to this ad-unit"), 'success');
					$this->redirect('/inventory_management/index/' . $adunit_id . '/');
				}
			}
		}
		
		private function isSuccessLyris() {
			App::import('Vendor', 'src/lyrisapi');
			$isSuccessLyris = 0;
			$tblAdunits = array();
			$lyrisSite = Configure::read('User.lyris_siteid_' . $this->Authake->getUserId());
			$lyrisPassword = Configure::read('User.lyris_password_' . $this->Authake->getUserId());
			if (!empty($lyrisSite) && !empty($lyrisPassword)) {
				$lyriss = new lyrisapi($lyrisSite, $lyrisPassword);
				$messages = $lyriss->listQuery("Lyris@API");
				$orders_array = array();
				$total = count($messages);
				$total = (int) $total - 1;
				if ($total > 0) {
					$isSuccessLyris = 1;
				}
			}
			$this->set('isSuccessLyris', $isSuccessLyris);
		}
		
		private function isSuccessMailchimp() {
			
			App::import('Vendor', array('file' => 'autoload'));
			$MailchimpApi = Configure::read('User.Mailchimp_Api_' . $this->Authake->getUserId());
			$Mailchimp = new \DrewM\MailChimp\MailChimp($MailchimpApi);
			$result = $Mailchimp->get('lists');
			//print_r($result);
			$isSuccessMailchimp = 0;
			if (isset($result)) {
				$isSuccessMailchimp = 1;
			}
			$this->set('$isSuccessMailchimp', $isSuccessMailchimp);
		}
		
		private function ProductTypeList() {
			
			$optionsp['joins'] = array(
            array('table' => 'tbl_media_types',
			'alias' => 'mtypes',
			'type' => 'INNER',
			'conditions' => array(
			'tblProductType.ptype_m_id = mtypes.mtype_id')
			));
			$this->tblProductType->virtualFields = array("productname" => 'CONCAT(mtypes.mtype_name, "=>", tblProductType.ptype_name)');
			$optionsp['fields'] = array('tblProductType.ptype_id', 'productname');
			$optionsp['conditions']=array('tblProductType.ptype_m_id'=> array(1,2,5));
			$optionsp['order']=array('tblProductType.ptype_m_id'=> 'ASC');
			$ProductTypeList = $this->tblProductType->find('list', $optionsp);
			$this->set('ProductTypeList', $ProductTypeList);
		}
		
		private function adunitslist() {
			$options['joins'] = array(
            array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.publisher_id = tblp.user_id',
			'tblAdunit.owner_user_id' => array($this->Authake->getUserId()))),
            array('table' => 'tbl_product_types',
			'alias' => 'tbpt',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.ad_ptype_id = tbpt.ptype_id'
			)),
            array('table' => 'tbl_media_types',
			'alias' => 'tbmt',
			'type' => 'INNER',
			'conditions' => array(
			'tbmt.mtype_id = tbpt.ptype_m_id'
			)));
			
			$options['fields'] = array('tblAdunit.adunit_id', 'tblAdunit.adunit_name');
			$ParentAdUnit = $this->tblAdunit->find('list', $options);
			array_unshift($ParentAdUnit, array('0' => 'None'));
			$this->set('ParentAdUnit', $ParentAdUnit);
		}
		
		private function vendor_adunit_size() {
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
			App::import('Vendor', 'src/ExampleUtils');
			// Get DfpUser from credentials in "../auth.ini"
			// relative to the DfpUser.php file's directory.
			$user = new DfpUser();
			// Log SOAP XML request and response.
			$user->LogDefaults();
			// Get the InventoryService.
			$inventoryService = $user->GetService('InventoryService', 'v201608');
			// Create a statement to select all ad unit sizes.
			$statementBuilder = new StatementBuilder();
			// Get all ad unit sizes by statement.
			$adUnitSizes = $inventoryService->getAdUnitSizesByStatement($statementBuilder->ToStatement());
			$adUnit_size['text/html'] = 'Text / HTML';
			if (!empty($adUnitSizes)) {
				foreach ($adUnitSizes as $data) {
					if (!empty($data->fullDisplayString))
                    $adUnit_size[$data->fullDisplayString] = $data->fullDisplayString;
				}
			}
			return $adUnit_size;
		}
		
		public function addAdUnit() {
			
			$PostData = $this->request->data;
			//if(!empty($PostData)){ pr($PostData);die;}
			$company_id = isset($this->params['named']['company_id']) ? $this->params['named']['company_id'] : "";
			// form posted
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
			App::import('Vendor', 'src/ExampleUtils');
			App::import('Vendor', 'src/lyrisapi');
			App::import('Vendor', array('file' => 'autoload'));
			
			$this->loadModel('FieldMapping');
			$this->loadModel('AdunitLayout');
			
			$PostData = $this->request->data;
			if(!empty($PostData)){ pr($PostData);die;}
			
			$company_id = isset($this->params['named']['company_id']) ? $this->params['named']['company_id'] : "";
			$this->set('title_for_layout', 'Add Publisher Ad Unit');
			
			// Get DfpUser from credentials in "../auth.ini"
			// relative to the DfpUser.php file's directory.
			$user = new DfpUser();
			
			// Log SOAP XML request and response.
			$user->LogDefaults();
			
			// Get the InventoryService.
			$inventoryService = $user->GetService('InventoryService', 'v201608');
			
			// Create a statement to select all ad unit sizes.
			$statementBuilder = new StatementBuilder();
			
			// Get all ad unit sizes by statement.
			$adUnitSizes = $inventoryService->getAdUnitSizesByStatement(
			$statementBuilder->ToStatement());
			
			//print_r($adUnitSizes);
			
			$this->set('adUnitSizes', $adUnitSizes);
			
			
			App::import('Vendor', 'src/lyrisapi');
			$isSuccessLyris = 0;
			$tblAdunits = array();
			/*
			$lyrisSite = Configure::read('User.lyris_siteid_' . $this->Authake->getUserId());
			$lyrisPassword = Configure::read('User.lyris_password_' . $this->Authake->getUserId());
			if (!empty($lyrisSite) && !empty($lyrisPassword)) {
				$lyriss = new lyrisapi($lyrisSite, $lyrisPassword);
				$messages = $lyriss->listQuery("Lyris@API");
				$orders_array = array();
				$total = count($messages);
				$total = (int) $total - 1;
				if ($total > 0) {
					$isSuccessLyris = 1;
				}
			} */
			$this->set('isSuccessLyris', $isSuccessLyris);
			//$this->set('isSuccessLyris', 0);
			App::import('Vendor', array('file' => 'autoload'));
			$isSuccessMailchimp = 0;
			try {
				$MailchimpApi = Configure::read('User.Mailchimp_Api_' . $this->Authake->getUserId());
				$Mailchimp = new \DrewM\MailChimp\MailChimp($MailchimpApi);
				$result = $Mailchimp->get('lists');
				} catch (Exception $e) {
				$isSuccessMailchimp = 0;
				if (isset($result)) {
					$isSuccessMailchimp = 1;
				}
			}
			//print_r($isSuccessMailchimp);
			$this->set('isSuccessMailchimp', $isSuccessMailchimp);
			
			
			//print_r($this->Authake->getGroupIds());
			if (!empty($this->request->data)) {
				$this->request->data['tblAdunit']['owner_user_id'] = $this->Authake->getUserId();
				$ad_unit_sizes = $this->request->data['tblAdunit']['ad_unit_sizes'];
				$ad_parent_id = $this->request->data['tblAdunit']['ad_parent_id'];
				if ($ad_parent_id != 0) {
					$options['conditions'] = array('tblAdunit.adunit_id' => $ad_parent_id);
					$tblAdunits = $this->tblAdunit->find('first', $options);
					$this->request->data['tblAdunit']['publisher_id'] = $tblAdunits['tblAdunit']['publisher_id'];
					$this->request->data['tblAdunit']['ad_ptype_id'] = $tblAdunits['tblAdunit']['ad_ptype_id'];
					$this->request->data['tblAdunit']['ad_placement_id'] = $tblAdunits['tblAdunit']['ad_placement_id'];
					$this->request->data['tblAdunit']['ad_adtype_id'] = $tblAdunits['tblAdunit']['ad_adtype_id'];
					$this->request->data['tblAdunit']['ad_isExternal'] = $tblAdunits['tblAdunit']['ad_isExternal'];
					$this->request->data['tblAdunit']['ad_auto_optimize'] = (!empty($this->request->data['tblAdunit']['ad_audience_profile'])) ? $this->request->data['tblAdunit']['ad_auto_optimize'] : 0;
				}	
				$this->tblAdunit->create();
				if ($this->tblAdunit->save($this->request->data)) {
					// save template  
					// Save field maping
					if (!empty($PostData) and $PostData['tblAdunit']['ad_ptype_id'] == 6) {
						$this->loadModel('FieldMapping');
						$this->loadModel('AdunitLayout');
						if (!empty($PostData['AdunitLayout'])) {
							$PostData['AdunitLayout']['adunit_id'] = $this->tblAdunit->id;
							$this->AdunitLayout->create();
							$this->AdunitLayout->save($PostData['AdunitLayout']);
						}
						$i = 1;
						foreach ($PostData['FieldMapping']['field_description'] as $key => $value) {
							if (!empty($PostData['FieldMapping']['status'][$key])) { // save only who checked
								$FieldData = array();
								$available_status = (!empty($PostData['FieldMapping']['status'][$key])) ? 1 : 0;
								$field_name = '';
								if ($PostData['FieldMapping']['custom'][$key] == 1) {
									$field_name = "cvalue" . $i++;
									} else {
									$field_name = (!empty($PostData['FieldMapping']['field_name'][$key])) ? $PostData['FieldMapping']['field_name'][$key] : '';
								}
								
								$FieldData['FieldMapping'] = array('adunit_id' => $this->tblAdunit->id, 'field_description' => $value, 'field_lable' => $PostData['FieldMapping']['field_lable'][$key]
                                , 'field_type' => $PostData['FieldMapping']['field_type'][$key],
                                'field_format' => (!empty($PostData['FieldMapping']['field_format'][$key])) ? $PostData['FieldMapping']['field_format'][$key] : '',
                                'field_name' => $field_name
                                , 'field_custom_text' => (!empty($PostData['FieldMapping']['field_custom_text'][$key])) ? $PostData['FieldMapping']['field_custom_text'][$key] : ''
                                , 'custom' => $PostData['FieldMapping']['custom'][$key], 'status' => $available_status, 'date' => date('Y-m-d H:i:s'));
								$this->FieldMapping->create();
								$this->FieldMapping->save($FieldData);
							}
						}
					}
					// Save field maping
					try {
						$product_id = $this->request->data['tblAdunit']['ad_ptype_id'];
						if ($product_id == 13 || $product_id == 2) {
							$ad_uid = $this->tblAdunit->field(
							'ad_uid', array('ad_created_at <' => date('Y-m-d H:i:s')), 'ad_created_at DESC'
							);
							$ad_uid_table = $this->request->data['tblAdunit']['publisher_id'] . "_" . $ad_uid;
							
							$sql_table = "CREATE TABLE IF NOT EXISTS " . $ad_uid_table . " (
							`nl_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
							`nl_adunit_uuid` char(50) NOT NULL,
							`nl_email` varchar(100) NOT NULL,
							`nl_fname` varchar(100) DEFAULT NULL,
							`nl_lname` varchar(100) DEFAULT NULL,
							`nl_ispromotion` int(11) DEFAULT '1',
							`nl_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
							PRIMARY KEY (`nl_id`)
							) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
							$this->Dynamic->query($sql_table);
						}
						if ($ad_unit_sizes == "text/html") {
							$this->request->data['tblAdunit']['ad_unit_sizes'] = "1x1";
						}
						// Get the NetworkService.
						$networkService = $user->GetService('NetworkService', 'v201608');
						
						// Get the effective root ad unit's ID for all ad units to be created under.
						$network = $networkService->getCurrentNetwork();
						$effectiveRootAdUnitId = $network->effectiveRootAdUnitId;
						
						$adunitname = $this->request->data['tblAdunit']['adunit_dfp_name'];
						$pubid = $this->request->data['tblAdunit']['publisher_id'];
						$ad_desc = $this->request->data['tblAdunit']['ad_desc'];
						$ad_size = $this->request->data['tblAdunit']['ad_unit_sizes'];
						
						// Create an array to store local ad unit objects.
						$adUnits = array();
						
						$adUnit = new AdUnit();
						$adUnit->name = $adunitname;
						$adUnit->parentId = $effectiveRootAdUnitId;
						$adUnit->description = $ad_desc;
						$adUnit->targetWindow = 'BLANK';
						
						// Create ad unit size.
						$adUnitSize = new AdUnitSize();
						
						if (strpos($ad_size, 'x') !== false)
                        $ad_sizes = explode("x", $ad_size);
						else if (strpos($ad_size, ':') !== false)
                        $ad_sizes = explode(":", $ad_size);
						
						$adUnitSize->size = new Size($ad_sizes[0], $ad_sizes[1], FALSE);
						$adUnitSize->environmentType = 'BROWSER';
						
						// Set the size of possible creatives that can match this ad unit.
						$adUnit->adUnitSizes = array($adUnitSize);
						
						$adUnits[] = $adUnit;
						
						// Create the ad units on the server.
						$adUnits = $inventoryService->createAdUnits($adUnits);
						// Display results.
						if (isset($adUnits)) {
							foreach ($adUnits as $adUnit) {
								$adUnitid = $this->tblAdunit->getLastInsertID();
								$this->tblAdunit->updateAll(
								array('ad_dfp_id' => "$adUnit->id"), array('adunit_id' => $adUnitid)
								);
								$this->Session->setFlash(__('An AdUnit with with ID "' . $adUnit->id . '" and name "' . $adunitname . "\" was created. Copy the following tags based on your implementation to start delivering line-items to this ad-unit"), 'success');
								$this->redirect('/inventory_management/index/' . $adUnitid . '/');
							}
							} else {
							print "No ad units created.\n";
						}
						} catch (OAuth2Exception $e) {
						ExampleUtils::CheckForOAuth2Errors($e);
						} catch (ValidationException $e) {
						ExampleUtils::CheckForOAuth2Errors($e);
						} catch (Exception $e) {
						$this->Session->setFlash(__('Error : "' . $e->getMessage()), 'error');
					}
				}
			}
			
			$publisher1 = $this->tblProfile->getAllConnectedProfiles();
			//print_r($publisher1);
			$this->set('publisher1', $publisher1);
			
			$adUnitTypes = $this->tblAdunitsType->getAdunitTypes();
			$this->set('adUnitTypes', $adUnitTypes);
			
			$adUnitPlacements = $this->tblAdunitsPlacement->getAdunitPlacements();
			$this->set('adUnitPlacements', $adUnitPlacements);
			
			$optionsp['joins'] = array(
            array('table' => 'tbl_media_types',
			'alias' => 'mtypes',
			'type' => 'INNER',
			'conditions' => array(
			'tblProductType.ptype_m_id = mtypes.mtype_id')
			));
			$optionsp['conditions']=array('tblProductType.ptype_m_id'=> array(1,2,4,5));
			$optionsp['order']=array('tblProductType.ptype_m_id'=> 'ASC');
			$optionsp['fields'] = array('mtypes.*', 'tblProductType.*');
			
			$producttype = $this->tblProductType->find('all', $optionsp);
			$this->set('producttype', $producttype);
			
			$options['joins'] = array(
            array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.publisher_id = tblp.user_id',
			'tblAdunit.owner_user_id' => array($this->Authake->getUserId()))),
            array('table' => 'tbl_product_types',
			'alias' => 'tbpt',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.ad_ptype_id = tbpt.ptype_id'
			)),
            array('table' => 'tbl_media_types',
			'alias' => 'tbmt',
			'type' => 'INNER',
			'conditions' => array(
			'tbmt.mtype_id = tbpt.ptype_m_id'
			)));
			
			$options['fields'] = array('tblAdunit.*', 'tblp.CompanyName', 'tbpt.ptype_name', 'tbmt.mtype_name');
			
			$tblAdunits = $this->tblAdunit->find('all', $options);
			$this->set('tblAdunits', $tblAdunits);
			$this->set('company_id', $company_id);
		}
		
		public function addAdPlacement() {
			// form posted
			$this->set('title_for_layout', 'All Placements');
			
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
			App::import('Vendor', 'src/ExampleUtils');
			
			if (!empty($this->request->data)) {
				$this->tblPlacement->create();
				if ($this->tblPlacement->save($this->request->data)) {
					try {
						// Get DfpUser from credentials in "../auth.ini"
						// relative to the DfpUser.php file's directory.
						$user = new DfpUser();
						
						// Log SOAP XML request and response.
						$user->LogDefaults();
						
						// Get the PlacementService.
						$placementService = $user->GetService('PlacementService', 'v201608');
						
						
						// Create local placement object to store banner ad units.
						$bannerAdUnitPlacement = new Placement();
						$bannerAdUnitPlacement->name = $this->request->data['tblPlacement']['p_name'];
						$bannerAdUnitPlacement->description = $this->request->data['tblPlacement']['p_desc'];
						
						$bannerAdUnitPlacement->targetedAdUnitIds[] = $this->request->data['tblPlacement']['p_adunits'];
						
						$placementList = array();
						
						
						if (count($bannerAdUnitPlacement->targetedAdUnitIds) > 0) {
							$placementList[] = $bannerAdUnitPlacement;
						}
						
						// Create the placements on the server.
						$placements = $placementService->createPlacements($placementList);
						
						// Display results.
						if (isset($placements)) {
							foreach ($placements as $placement) {
								$placementId = $this->tblPlacement->getLastInsertID();
								$this->tblPlacement->updateAll(
								array('p_dfp_id' => "$placement->id"), array('p_id' => $placementId)
								);
								$this->Session->setFlash(__('An Placement with ID "' . $placement->id . '" and name "' . $placement->name . "\" was created."), 'success');
								$this->redirect(array('action' => 'all_placements'));
							}
							} else {
							print "No placements created.\n";
						}
						} catch (OAuth2Exception $e) {
						ExampleUtils::CheckForOAuth2Errors($e);
						} catch (ValidationException $e) {
						ExampleUtils::CheckForOAuth2Errors($e);
						} catch (Exception $e) {
						$this->Session->setFlash(__('Error : "' . $e->getMessage()), 'error');
					}
				}
			}
			
			$options['conditions'] = array(
            'Ugroup.group_id' => '4');
			
			$options['joins'] = array(
            array('table' => 'authake_groups_users',
			'alias' => 'Ugroup',
			'type' => 'INNER',
			'conditions' => array(
			'User.id = Ugroup.user_id')
            )
			);
			
			$publisher = $this->User->find('all', $options);
			$this->set('publishers', $publisher);
		}
		
		public function all_placements() {
			// form posted
			$this->set('title_for_layout', 'Add Publisher Placement');
			// form posted
			$options['joins'] = array(
            array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblPlacement.p_publisher_id = tblp.user_id')
			));
			
			$options['conditions'] = array('not' => array('tblPlacement.p_dfp_id' => 0));
			
			$options['fields'] = array('tblPlacement.*', 'tblp.CompanyName');
			
			$tblPlacement = $this->tblPlacement->find('all', $options);
			//print_r($test);
			$this->set('group', $tblPlacement);
		}
		
		function ajax_getAdUnitByPublisher() {
			// Fill select form field after Ajax request.
			if (!empty($this->request->data)) {
				$pubid = $this->request->data['tblPlacement']['p_publisher_id'];
				$options['conditions'] = array(
                'publisher_id' => $pubid, 'not' => array('tblAdunit.ad_dfp_id' => null));
				$adUnits = $this->tblAdunit->find('all', $options);
				//print_r($adUnits);
				$this->set('adUnits', $adUnits);
			}
			$this->layout = 'ajax';
		}
		
		function getPlacementList() {
			$this->autoRender = false;
			
			
			$messages = $this->tblAdunitsPlacement->getAdunitPlacements();
			//$messages = $messages['tblAdunitsPlacement'];
			
			$orders_array = array();
			$total = count($messages);
			$total = (int) $total - 1;
			for ($count = 0; $count < $total; $count++) {
				$orders_array[$count]['id'] = $messages[$count]['tblAdunitsPlacement']['ap_id'];
				$orders_array[$count]['text'] = $messages[$count]['tblAdunitsPlacement']['ap_name'];
			}
			return json_encode($orders_array);
		}
		
		function getUnitList() {
			$this->autoRender = false;
			$messages = $this->tblAdunitsType->getAdunitTypes();
			//print_r($messages);
			$orders_array = array();
			$total = count($messages);
			$total = (int) $total - 1;
			for ($count = 0; $count < $total; $count++) {
				$orders_array[$count]['id'] = $messages[$count]['tblAdunitsType']['at_id'];
				$orders_array[$count]['text'] = $messages[$count]['tblAdunitsType']['at_name'];
			}
			return json_encode($orders_array);
		}
		
		function getLyrisList() {
			$this->autoRender = false;
			App::import('Vendor', 'src/lyrisapi');
			$lyriss = new lyrisapi("666767", "Lyris@API");
			$messages = $lyriss->listQuery("Lyris@API");
			//print_r($messages);
			$orders_array = array();
			$total = count($messages);
			$total = (int) $total - 1;
			for ($count = 0; $count < $total; $count++) {
				$orders_array[$count]['id'] = $messages[$count]['mlid'];
				$orders_array[$count]['text'] = $messages[$count]['name'];
			}
			return json_encode($orders_array);
		}
		
		function getLyrisListArray() {
			App::import('Vendor', 'src/lyrisapi');
			$lyriss = new lyrisapi("666767", "Lyris@API");
			$messages = $lyriss->listQuery("Lyris@API");
			
			$options = array();
			foreach ($messages as $value) {
				if (is_array($value))
                $options[$value['mlid']] = $value['name'];
			}
			return $options;
		}
		
		function getSocketLabListArray() {
			define("SERVER_ID", "12888");
			define("API_USER", "12888");
			define("API_PASSWORD", "Kr6g5W7Moa2Y4FsHd93A");
			//calls messagesFailed
			$service_url = 'https://api.socketlabs.com/marketing/v1/lists';
			$curl = curl_init($service_url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_USERPWD, API_USER . ':' . API_PASSWORD);
			$messages = curl_exec($curl);
			curl_close($curl);
			$orders_array = array();
			$messages = json_decode($messages, true);
			//print_r($messages);
			
			$options = array();
			foreach ($messages as $value) {
				$options[$value['id']] = $value['name'];
			}
			return $options;
		}
		
		function getMailchimpList() {
			
			$this->autoRender = false;
			App::import('Vendor', 'src/html2text');
			App::import('Vendor', array('file' => 'autoload'));
			
			$Mailchimp = new \DrewM\MailChimp\MailChimp('6c7a3b4ecb8a3027ed8cf01a8ac3894d-us13');
			$messages = $Mailchimp->get('lists');
			$messages = $messages['lists'];
			//print_r($messages);
			
			$orders_array = array();
			$total = count($messages);
			
			$total = (int) $total - 1;
			//print_r($total);
			for ($count = 0; $count <= $total; $count++) {
				$orders_array[$count]['id'] = $messages[$count]['id'];
				$orders_array[$count]['text'] = $messages[$count]['name'];
			}
			
			return json_encode($orders_array);
		}
		
		function getMailchimpListArray() {
			
			App::import('Vendor', 'src/html2text');
			App::import('Vendor', array('file' => 'autoload'));
			
			$Mailchimp = new \DrewM\MailChimp\MailChimp('6c7a3b4ecb8a3027ed8cf01a8ac3894d-us13');
			$messages = $Mailchimp->get('lists');
			
			//print_r($messages);
			$options = array();
			foreach ($messages as $value) {
				if (is_array($value))
                $options[$value['id']] = $value['name'];
			}
			
			return $options;
		}
		
		function getSocketLabList() {
			$this->autoRender = false;
			define("SERVER_ID", "12888");
			define("API_USER", "12888");
			define("API_PASSWORD", "Kr6g5W7Moa2Y4FsHd93A");
			//calls messagesFailed
			$service_url = 'https://api.socketlabs.com/marketing/v1/lists';
			$curl = curl_init($service_url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_USERPWD, API_USER . ':' . API_PASSWORD);
			$messages = curl_exec($curl);
			curl_close($curl);
			$orders_array = array();
			$messages = json_decode($messages);
			//print_r($messages);
			
			$total = count($messages);
			$total = (int) $total;
			for ($count = 0; $count < $total; $count++) {
				$orders_array[$count]['id'] = $messages[$count]->id;
				$orders_array[$count]['text'] = $messages[$count]->name;
			}
			return json_encode($orders_array);
		}
		
		/**
			* AdUnit Reporting 
			* tab1 : Revenue Report
			* tab2 : Impression Reports
			* tab3 : Audience Profile
			* tab4 : Inventory
			* tab
			* @param type $nl_adunit_uuid
			* @param type $tab
			* @param string $duration
		*/
		public function view_stats_email($nl_adunit_uuid, $tab = 'tab2', $duration = 'month') {
			Configure::write('debug', 2);
			$ids = $this->tblProfile->getAllConnectedProfileUserIds();
			$this->loadModel('Authake.tblFlight');
			$this->loadModel('Authake.tblIspReport');
			$this->loadModel('Authake.tblReportDay');
			$user_id = $this->Authake->getUserId();
			// getting date reange here by argument
			$DateRange = $this->Commonfunction->getDuration($duration);
			
			$tblMailing = $this->tblMailing->getMaillingAdunitByAdunitId($user_id, $nl_adunit_uuid, $DateRange);
			//print_r($tblMailing);
			// debug($this->tblMailing->getDataSource()->getLog(false,false));
			//tab wise conditions
			switch ($tab) {
				case 'tab1' :
                $this->set('revenueTrends', $this->tblReportDay->revenueTrends($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('RevenueTable', $this->Chart->GrossRevenueTable($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('eCPMTrade', $this->tblReportDay->eCPMTrade($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('Daily_New_Subscribers', $this->Chart->Daily_New_Subscribers($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('PerformanceRevenueByISP', $this->tblIspReport->PerformanceRevenueByISP($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('group', $tblMailing);
                break;
				case 'tab2' :
                $this->set('RevenueTable', $this->Chart->GrossRevenueTable($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('revenueTrends', $this->tblReportDay->revenueTrends($nl_adunit_uuid, $DateRange, $duration, $user_id));
				
                $this->set('RevenueByPriceFormat', $this->tblReportDay->RevenueByPriceFormat($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('RevenueByVertical', $this->tblReportDay->RevenueByVertical($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('RevenueByISP', $this->tblReportDay->RevenueByISP($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('eCPMTrade', $this->tblReportDay->eCPMTrade($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('ecpmbyISP', $this->tblIspReport->ecpmbyISP($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('EaringISP', $this->tblReportDay->EaringISP($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('OpenClickTrendByIsp', $this->tblIspReport->OpenClickTrendByIsp($nl_adunit_uuid, $DateRange, $duration, $user_id));
				
                break;
				case 'tab3' :
				
                $this->set('TotalEmailSents', $this->tblMailing->TotalEmailSentsTable($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('sendToOpenPer', $this->tblMailing->sendToOpenPer($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('sendToClickPer', $this->tblMailing->sendToClickPer($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('PerformanceRevenueByISP', $this->tblIspReport->PerformanceRevenueByISP($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('BounceReportByISP', $this->tblIspReport->BounceReportByISP($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('group', $tblMailing);
                break;
				case 'tab4' :
                $table_name = $this->tblAdunit->getEmailtableName($nl_adunit_uuid); // get here talbe name of das email
                $tblSubscribers = array();
                $this->Dynamic->useTable = $table_name;
                try {
                    $this->Dynamic->virtualFields = array('ActiveSubscripbers' => "SELECT count('nl_id') as totalSubscribers FROM " . $table_name . " WHERE nl_isActive=1",
					'InactiveSubscribers' => "SELECT count('nl_id') as totalSubscribers FROM " . $table_name . " WHERE nl_isActive=0");
                    $totalSubscribersData = $this->Dynamic->find('first', array('fields' => array('ActiveSubscripbers', 'InactiveSubscribers')));
					} catch (Exception $e) {
                    $e->getMessage();
                    // if not found table then set active and inactive subscriber will be zero
                    $totalSubscribersData['Dynamic']['ActiveSubscripbers'] = 0;
                    $totalSubscribersData['Dynamic']['InactiveSubscribers'] = 0;
				}
                $optout = 0;
                if (!empty($totalSubscribersData['Dynamic']['InactiveSubscribers'])) {
                    (($totalSubscribersData['Dynamic']['InactiveSubscribers'] * 100) / ($totalSubscribersData['Dynamic']['ActiveSubscripbers'] == 0) ? 1 : $totalSubscribersData['Dynamic']['ActiveSubscripbers']);
				}
				
                $this->set('AudienceProfile', $this->Chart->AudienceProfile($nl_adunit_uuid, $DateRange, $duration, $user_id));
				
                $this->set('SubscribersBySource', $this->Chart->SubscribersBySource($nl_adunit_uuid, $DateRange, $duration, $user_id));
				
                $this->set('DailySubscribersBySource', $this->Chart->DailySubscribersBySource($nl_adunit_uuid, $DateRange, $duration, $user_id));
				
                $this->set('RevenueBySource', $this->Chart->RevenueBySource($nl_adunit_uuid, $DateRange, $duration, $user_id));
                $this->set('RevenueOpenClickBySource', $this->Chart->RevenueOpenClickBySource($nl_adunit_uuid, $DateRange, $duration, $user_id));
				
                $this->set('totalSubscribers', (!empty($totalSubscribersData['Dynamic']['ActiveSubscripbers'])) ? $totalSubscribersData['Dynamic']['ActiveSubscripbers'] : 0);
                $this->set('optout', $optout);
				
                $this->Dynamic->virtualFields = array();

                $this->set('group', $tblMailing);
                $totalQuality = $tblMailing['tblMailing']['total_unsubscribe'] + $tblMailing['tblMailing']['total_bounce'] + $tblMailing['tblMailing']['total_spam'];
                $totalQuality = ($totalQuality == 0 ? "1" : $totalQuality);
                $country = array();
                $DeviceType = array();
                $browsers = array();
                $osfamily = array();
                App::import('Vendor', 'src/Piwik');
                $piwik = new Piwik(Configure::read('Piwik.url'), Configure::read('Piwik.token'), $tblMailing['tblp']['tracksite_id'], Piwik::FORMAT_JSON);
				
				
                $piwik->setPeriod(Piwik::PERIOD_RANGE);
                $piwik->setRange($DateRange['start'], $DateRange['end']);
                
				if(!empty($tblMailing['tblMailing']['track_code']))
				{
					$getCountries = $piwik->getCountry("contentPiece==" . $tblMailing['tblMailing']['track_code']);
					
					if (!empty($getCountries)) {
						
						foreach ($getCountries as $getCountry) {
							if (array_key_exists($getCountry->label, $country)) {
								$country[$getCountry->label] += $getCountry->nb_visits;
								} else {
								$country[$getCountry->label] = $getCountry->nb_visits;
							}
						}
					}
					
					$getDeviceType = $piwik->getDeviceType("contentPiece==" . $tblMailing['tblMailing']['track_code']);
					if (!empty($getDeviceType)) {
						foreach ($getDeviceType as $getDevice) {
							if (array_key_exists($getDevice->label, $DeviceType)) {
								$DeviceType[$getDevice->label] += $getDevice->nb_visits;
								} else {
								$DeviceType[$getDevice->label] = $getDevice->nb_visits;
							}
						}
					}
					
					$getBrowsers = $piwik->getBrowsers("contentPiece==" . $tblMailing['tblMailing']['track_code']);
					if (!empty($getBrowsers)) {
						foreach ($getBrowsers as $getBrowser) {
							if (array_key_exists($getBrowser->label, $browsers)) {
								$browsers[$getBrowser->label] += $getBrowser->nb_visits;
								} else {
								$browsers[$getBrowser->label] = $getBrowser->nb_visits;
							}
						}
					}
					
					$getOSFamilies = $piwik->getOSFamilies("contentPiece==" . $tblMailing['tblMailing']['track_code']);
					if (!empty($getOSFamilies)) {
						foreach ($getOSFamilies as $getOSFamily) {
							if (array_key_exists($getOSFamily->label, $osfamily)) {
								$osfamily[$getOSFamily->label] += $getOSFamily->nb_visits;
								} else {
								$osfamily[$getOSFamily->label] = $getOSFamily->nb_visits;
							}
						}
					}
				}
                $this->set('country', $country);
                $this->set('DeviceType', $DeviceType);
                $this->set('browsers', $browsers);
                $this->set('osfamily', $osfamily);
                $this->set('totalQuality', $totalQuality);
                break;
				case 'tab5' :
                $adunitid = $this->tblAdunit->field('adunit_id', array('ad_uid' => $nl_adunit_uuid));
                $nlchild = array();
                $options1['conditions'] = array(
				'tblAdunit.owner_user_id' => $user_id,
				'tblAdunit.ad_parent_id' => $adunitid
                );
				
                $nlchilds = $this->tblAdunit->find('all', $options1);
                $totalRevenue = 0;
                $totalCPM = 0;
                $totalCPC = 0;
                $totalCPL = 0;
                $totalSCR = 0;
                $revenueCPM = 0;
                $revenueCPC = 0;
                $revenueCPL = 0;
                $revenueSCR = 0;
                $mailsentCPM = 0;
                $mailsentCPC = 0;
                $mailsentCPL = 0;
                $mailsentSCR = 0;
                $activeLineItems = array();
                $activeLineTypes = array();
                $revenueByTypes = array();
                $revenueByAdunits = array();
                //print_r($nlchilds);
                $countm = 0;
				
                if (count($nlchilds) > 0) {
                    foreach ($nlchilds as $nlchild) {
                        $optionnl['joins'] = array(
						array('table' => 'tbl_line_items',
						'alias' => 'tblLineItems',
						'type' => 'INNER',
						'conditions' => array(
						'tblLineItems.li_id = tblMappedAdunit.ma_l_id')
						),
						array('table' => 'tbl_adunits',
						'categoryalias' => 'tba',
						'type' => 'INNER',
						'conditions' => array(
						'tba.ad_dfp_id = tblMappedAdunit.ma_ad_id'
                        )));
						
                        $optionnl['conditions'] = array('tblMappedAdunit.ma_ad_id' => $nlchild['tblAdunit']['ad_dfp_id'], 'tblMappedAdunit.ma_isActive' => '1');
						
                        $optionnl['fields'] = array('tblMappedAdunit.*', 'tblLineItems.*', 'tba.adunit_name');
                        $mappedUnits = $this->tblMappedAdunit->find('all', $optionnl);
                        //echo "<pre>";
                        //print_r($mappedUnits);
                        //echo "</pre>";
                        foreach ($mappedUnits as $mappedUnit) {
                            $totalRevenue += $mappedUnit['tblMappedAdunit']['ma_revenue'];
                            $activeLineItems[] = $mappedUnit['tblLineItems']['li_id'];
                            $activeLineTypes[] = $mappedUnit['tblLineItems']['li_cs_id'];
                            $revenueByTypes[$countm]['type'] = $mappedUnit['tblLineItems']['li_cs_id'];
                            $revenueByTypes[$countm]['revenue'] = $mappedUnit['tblMappedAdunit']['ma_revenue'];
                            $revenueByTypes[$countm]['name'] = $mappedUnit['tblLineItems']['li_name'];
                            //$revenueByTypes[$countm]['sent'] = $mappedUnit['tblLineItems']['m_sent'];
                            $revenueByAdunits[$countm]['name'] = $mappedUnit['tba']['adunit_name'];
                            $revenueByAdunits[$countm]['revenue'] = $mappedUnit['tblMappedAdunit']['ma_revenue'];
                            $countm++;
						}
					}
					} else {
                    $ad_dfp_id = $this->tblAdunit->field('ad_dfp_id', array('ad_uid' => $nl_adunit_uuid));
                    $optionnl['joins'] = array(
					array('table' => 'tbl_line_items',
					'alias' => 'tblLineItems',
					'type' => 'INNER',
					'conditions' => array(
					'tblLineItems.li_id = tblMappedAdunit.ma_l_id')
					),
					array('table' => 'tbl_adunits',
					'alias' => 'tba',
					'type' => 'INNER',
					'conditions' => array(
					'tba.ad_dfp_id = tblMappedAdunit.ma_ad_id'
                    )));
					
                    $optionnl['conditions'] = array('tblMappedAdunit.ma_ad_id' => $ad_dfp_id, 'tblMappedAdunit.ma_isActive' => '1');
					
                    $optionnl['fields'] = array('tblMappedAdunit.*', 'tblLineItems.*', 'tba.adunit_name');
                    $mappedUnits = $this->tblMappedAdunit->find('all', $optionnl);
                    //echo "<pre>";
                    //print_r($mappedUnits);
                    //echo "</pre>";
                    foreach ($mappedUnits as $mappedUnit) {
                        $totalRevenue += $mappedUnit['tblMappedAdunit']['ma_revenue'];
                        $activeLineItems[] = $mappedUnit['tblLineItems']['li_id'];
                        $activeLineTypes[] = $mappedUnit['tblLineItems']['li_cs_id'];
                        $revenueByTypes[$countm]['type'] = $mappedUnit['tblLineItems']['li_cs_id'];
                        $revenueByTypes[$countm]['revenue'] = $mappedUnit['tblMappedAdunit']['ma_revenue'];
                        $revenueByTypes[$countm]['name'] = $mappedUnit['tblLineItems']['li_name'];
                        $revenueByAdunits[$countm]['name'] = $mappedUnit['tba']['adunit_name'];
                        $revenueByAdunits[$countm]['revenue'] = $mappedUnit['tblMappedAdunit']['ma_revenue'];
                        $countm++;
					}
				}
                $orderby = "revenue";
				
                if (count($revenueByTypes) > 0) {
                    $sortArray = array();
                    foreach ($revenueByTypes as $revenueByType) {
                        foreach ($revenueByType as $key => $value) {
                            if (!isset($sortArray[$key])) {
                                $sortArray[$key] = array();
							}
                            $sortArray[$key][] = $value;
						}
					}
                    array_multisort($sortArray[$orderby], SORT_DESC, $revenueByTypes);
				}
				
                if (count($revenueByAdunits) > 0) {
                    $sortArray1 = array();
                    foreach ($revenueByAdunits as $revenueByAdunit) {
						
                        foreach ($revenueByAdunit as $key => $value) {
                            if (!isset($sortArray1[$key])) {
                                $sortArray1[$key] = array();
							}
                            $sortArray1[$key][] = $value;
						}
					}
                    array_multisort($sortArray1[$orderby], SORT_DESC, $revenueByAdunits);
				}
				
				
                foreach ($revenueByTypes as $revenueByType) {
                    switch ($revenueByType['type']) {
                        case 1 :
						$revenueCPC += $revenueByType['revenue'];
						break;
                        case 2 :
						$revenueCPL += $revenueByType['revenue'];
						break;
                        case 3 :
						$revenueCPL += $revenueByType['revenue'];
						break;
                        case 4 :
						$revenueCPL += $revenueByType['revenue'];
						break;
                        case 5 :
						$revenueCPM += $revenueByType['revenue'];
						break;
                        case 6 :
						$revenueCPL += $revenueByType['revenue'];
						break;
					}
				}
				
                //$activeLineTypes = array_unique($activeLineTypes);
                foreach ($activeLineTypes as $activeLineType) {
                    switch ($activeLineType) {
                        case 1 :
						$totalCPC++;
						break;
                        case 2 :
						$totalCPL++;
						break;
                        case 3 :
						$totalCPL++;
						break;
                        case 4 :
						$totalCPL++;
						break;
                        case 5 :
						$totalCPM++;
						break;
                        case 6 :
						$totalCPL++;
						break;
					}
				}
				
                $this->set('totalRevenue', $totalRevenue);
                $this->set('totalmonthCPM', $totalCPM);
                $this->set('nlchilds', $nlchilds);
                $this->set('totalCPC', $totalCPC);
                $this->set('totalCPL', $totalCPL);
                $this->set('totalSCR', $totalSCR);
                $this->set('revenueCPL', $revenueCPL);
                $this->set('revenueCPM', $revenueCPM);
                $this->set('revenueCPC', $revenueCPC);
                $this->set('revenueSCR', $revenueSCR);
                $this->set('revenueByTypes', $revenueByTypes);
                $this->set('revenueByAdunits', $revenueByAdunits);
                $this->set('activeLines', count(array_unique($activeLineItems)));
                $m_revenue = $sent = $delivered = $opens = $totalopens = $clicks = $totalclicks = $unsubscribe = $bounce = $spam = 0;
				
                $this->set('group', $tblMailing);
				
                $tabl5data = self::tabl5data();
                $this->set('tabl5data', $tabl5data);
                break;
				case 'tab6' :
                $tabl6data = self::tabl6data();
                break;
			}
			
			$adunit_name = $this->tblAdunit->field('adunit_name', array('ad_uid' => $nl_adunit_uuid));
			$this->set('tab', $tab);
			$this->set('adunit_name', $adunit_name);
			$this->set('title_for_layout', $adunit_name);
			$this->set('nl_adunit_uuid', $nl_adunit_uuid);
			$this->set('duration', $duration);
			$this->set('adunit_list', $this->tblAdunit->get_adunit_list($ids, $user_id));
		}
		
		function view_stats_leadgen($nl_adunit_uuid, $duration = 'month') {

			$options['joins'] = array(
            array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblp.profileID = User.tblProfileID')
			));
			$options['fields'] = array('tblp.tracksite_id');
			$options['conditions'] = array('User.id' => $this->Authake->getUserId());
			
			$tracksiteArr = $this->User->find('first', $options);
			$tracksite_id = $tracksiteArr['tblp']['tracksite_id'];
			$DateRange = $this->Commonfunction->getDuration($duration);
			
			//print_r($DateRange);
			$ad_dfp_id = $this->tblAdunit->field('ad_dfp_id', array('ad_uid' => $nl_adunit_uuid));
			$options1['joins'] = array(
            array('table' => 'tbl_adunits',
			'alias' => 'tba',
			'type' => 'INNER',
			'conditions' => array(
			'tba.ad_dfp_id = tblReportDay.rd_ad_unit_id',
			'tba.owner_user_id' => array($this->Authake->getUserId())
			)));
			
			$options1['conditions'] = array('and' => array(
			array('tblReportDay.rd_date <= ' => $DateRange['end'],
			'tblReportDay.rd_date >= ' => $DateRange['start'],
			'tblReportDay.rd_ad_unit_id ' => $ad_dfp_id
			)));
			
			$options1['fields'] = array('SUM(rd_imp) as rd_imp,SUM(rd_clicks) as rd_clicks ,SUM(rd_leads) as rd_leads,SUM(rd_delivered) as rd_delivered,SUM(rd_accepted) as rd_accepted,SUM(rd_ar_rejects) as rd_ar_rejects, rd_date as rd_date, SUM(rd_leads) - SUM(rd_delivered) as rd_3party , SUM(rd_delivered) - SUM(rd_accepted) as rd_client_rejects');
			$options1['group'] = 'tblReportDay.rd_date';
			$options1['order'] = array('tblReportDay.rd_date' => 'DESC');
			
			//print_r($options);
			
			$tblReportDay = $this->tblReportDay->find('all', $options1);
			//print_r($tblReportDay);
			
			$imp = 0;
			$clicks = 0;
			$leads = 0;
			$delivered = 0;
			$accepted = 0;
			$ar_rejects = 0;
			$third_party_rejects = 0;
			$clients_rejects = 0;
			foreach ($tblReportDay as $item) {
				$imp += $item[0]['rd_imp'];
				$clicks += $item[0]['rd_clicks'];
				$leads += $item[0]['rd_leads'];
				$delivered += $item[0]['rd_delivered'];
				$accepted += $item[0]['rd_accepted'];
				$ar_rejects += $item[0]['rd_ar_rejects'];
			}
			$third_party_rejects = $leads - $delivered;
			$clients_rejects = $delivered - $accepted;
			
			$totalRejects = $ar_rejects + $third_party_rejects + $clients_rejects;
			$totalRejects = ($totalRejects == 0 ? "1" : $totalRejects);
			
			$this->set('imp', $imp);
			$this->set('clicks', $clicks);
			$this->set('leads', $leads);
			$this->set('delivered', $delivered);
			$this->set('accepted', $accepted);
			$this->set('ar_rejects', $ar_rejects);
			$this->set('third_party_rejects', $third_party_rejects);
			$this->set('clients_rejects', $clients_rejects);
			$this->set('totalRejects', $totalRejects);
			$this->set('duration', $duration);
			$this->set('group', $tblReportDay);
			
			
			
			$optionsR['joins'] = array(
            array('table' => 'tbl_adunits',
			'alias' => 'tba',
			'type' => 'INNER',
			'conditions' => array(
			'tba.ad_dfp_id = tblReportDay.rd_ad_unit_id',
			'tba.owner_user_id' => array($this->Authake->getUserId())
			)),
            array('table' => 'tbl_line_items',
			'alias' => 'tbl',
			'type' => 'INNER',
			'conditions' => array(
			'tbl.li_dfp_id = tblReportDay.rd_line_item_id'))
			);
			
			$optionsR['conditions'] = array('and' => array(
			array('tblReportDay.rd_date <= ' => $DateRange['end'],
			'tblReportDay.rd_date >= ' => $DateRange['start'],
			'tblReportDay.rd_ad_unit_id ' => $ad_dfp_id
			)));
			$optionsR['group'] = 'tblReportDay.rd_line_item_id';
			$optionsR['fields'] = array('tbl.li_id');
			
			$tblReportDayAsPerRange = $this->tblReportDay->find('list', $optionsR);
			$tblReportDayAsPerRange = array_values($tblReportDayAsPerRange);
			$lineitemids = implode(",", $tblReportDayAsPerRange);
			
			$totalRevenue = 0;
			$totalCPM = 0;
			$totalCPC = 0;
			$totalCPL = 0;
			$totalSCR = 0;
			$revenueCPM = 0;
			$revenueCPC = 0;
			$revenueCPL = 0;
			$revenueSCR = 0;
			$mailsentCPM = 0;
			$mailsentCPC = 0;
			$mailsentCPL = 0;
			$mailsentSCR = 0;
			$activeLineItems = array();
			$activeLineTypes = array();
			$revenueByTypes = array();
			$revenueByAdunits = array();
			$countm = 0;
	
			$optionnl['joins'] = array(
            array('table' => 'tbl_line_items',
			'alias' => 'tblLineItems',
			'type' => 'INNER',
			'conditions' => array(
			'tblLineItems.li_id = tblMappedAdunit.ma_l_id')
            ),
            array('table' => 'tbl_adunits',
			'alias' => 'tba',
			'type' => 'INNER',
			'conditions' => array(
			'tba.ad_dfp_id = tblMappedAdunit.ma_ad_id'
			)),
            array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tba.publisher_id = tblp.user_id')));
			
			$optionnl['conditions'] = array('tblMappedAdunit.ma_ad_id' => $ad_dfp_id, 'tblMappedAdunit.ma_isActive' => '1', 'tblMappedAdunit.ma_l_id' => $tblReportDayAsPerRange);
			
			$optionnl['fields'] = array('tblMappedAdunit.*', 'tblLineItems.*', 'tba.adunit_name', 'tblp.tracksite_id');
			$mappedUnits = $this->tblMappedAdunit->find('all', $optionnl);
			//echo count($mappedUnits);
			//print_r($optionnl);
			//echo "<pre>";
			//print_r($mappedUnits);
			//echo "</pre>";
			
			$country = array();
			$DeviceType = array();
			$browsers = array();
			$osfamily = array();
			
			App::import('Vendor', 'src/Piwik');
			
			foreach ($mappedUnits as $mappedUnit) {
				try {
					$piwik = new Piwik(Configure::read('Piwik.url'), Configure::read('Piwik.token'), $mappedUnit['tblp']['tracksite_id'], Piwik::FORMAT_JSON);
					$piwik->setPeriod(Piwik::PERIOD_RANGE);
					$piwik->setRange($DateRange['start'], $DateRange['end']);
					$track_code = $mappedUnit['tblMappedAdunit']['ma_uid'];
					$getCountries = $piwik->getCountry("contentPiece==" . $track_code);
					foreach ($getCountries as $getCountry) {
						if (array_key_exists($getCountry->label, $country)) {
							$country[$getCountry->label] += $getCountry->nb_visits;
							} else {
							$country[$getCountry->label] = $getCountry->nb_visits;
						}
					}
					
					$getDeviceType = $piwik->getDeviceType("contentPiece==" . $track_code);
					foreach ($getDeviceType as $getDevice) {
						if (array_key_exists($getDevice->label, $DeviceType)) {
							$DeviceType[$getDevice->label] += $getDevice->nb_visits;
							} else {
							$DeviceType[$getDevice->label] = $getDevice->nb_visits;
						}
					}
					
					$getBrowsers = $piwik->getBrowsers("contentPiece==" . $track_code);
					foreach ($getBrowsers as $getBrowser) {
						if (array_key_exists($getBrowser->label, $browsers)) {
							$browsers[$getBrowser->label] += $getBrowser->nb_visits;
							} else {
							$browsers[$getBrowser->label] = $getBrowser->nb_visits;
						}
					}
					
					$getOSFamilies = $piwik->getOSFamilies("contentPiece==" . $track_code);
					foreach ($getOSFamilies as $getOSFamily) {
						if (array_key_exists($getOSFamily->label, $osfamily)) {
							$osfamily[$getOSFamily->label] += $getOSFamily->nb_visits;
							} else {
							$osfamily[$getOSFamily->label] = $getOSFamily->nb_visits;
						}
					}
					} catch (Exception $ex) {
					
				}
				
				$totalRevenue += $mappedUnit['tblMappedAdunit']['ma_revenue'];
				$activeLineItems[] = $mappedUnit['tblLineItems']['li_id'];
				$activeLineTypes[] = $mappedUnit['tblLineItems']['li_cs_id'];
				$revenueByTypes[$countm]['type'] = $mappedUnit['tblLineItems']['li_cs_id'];
				$revenueByTypes[$countm]['revenue'] = $mappedUnit['tblMappedAdunit']['ma_revenue'];
				$revenueByTypes[$countm]['name'] = $mappedUnit['tblLineItems']['li_name'];
				$revenueByAdunits[$countm]['name'] = $mappedUnit['tba']['adunit_name'];
				$revenueByAdunits[$countm]['revenue'] = $mappedUnit['tblMappedAdunit']['ma_revenue'];
				$countm++;
			}
			
			$orderby = "revenue";
			
			if (count($revenueByTypes) > 0) {
				$sortArray = array();
				foreach ($revenueByTypes as $revenueByType) {
					foreach ($revenueByType as $key => $value) {
						if (!isset($sortArray[$key])) {
							$sortArray[$key] = array();
						}
						$sortArray[$key][] = $value;
					}
				}
				array_multisort($sortArray[$orderby], SORT_DESC, $revenueByTypes);
			}
			
			if (count($revenueByAdunits) > 0) {
				$sortArray1 = array();
				foreach ($revenueByAdunits as $revenueByAdunit) {
					foreach ($revenueByAdunit as $key => $value) {
						if (!isset($sortArray1[$key])) {
							$sortArray1[$key] = array();
						}
						$sortArray1[$key][] = $value;
					}
				}
				array_multisort($sortArray1[$orderby], SORT_DESC, $revenueByAdunits);
			}
			
			
			foreach ($revenueByTypes as $revenueByType) {
				switch ($revenueByType['type']) {
					case 1 :
                    $revenueCPC += $revenueByType['revenue'];
                    break;
					case 2 :
                    $revenueCPL += $revenueByType['revenue'];
                    break;
					case 3 :
                    $revenueCPL += $revenueByType['revenue'];
                    break;
					case 4 :
                    $revenueCPL += $revenueByType['revenue'];
                    break;
					case 5 :
                    $revenueCPM += $revenueByType['revenue'];
                    break;
					case 6 :
                    $revenueCPL += $revenueByType['revenue'];
                    break;
				}
			}
			
			//$activeLineTypes = array_unique($activeLineTypes);
			foreach ($activeLineTypes as $activeLineType) {
				switch ($activeLineType) {
					case 1 :
                    $totalCPC++;
                    break;
					case 2 :
                    $totalCPL++;
                    break;
					case 3 :
                    $totalCPL++;
                    break;
					case 4 :
                    $totalCPL++;
                    break;
					case 5 :
                    $totalCPM++;
                    break;
					case 6 :
                    $totalCPL++;
                    break;
				}
			}

			$adunit_name = $this->tblAdunit->field('adunit_name', array('ad_uid' => $nl_adunit_uuid));
			$this->set('totalRevenue', $totalRevenue);
			$this->set('totalCPM', $totalCPM);
			$this->set('totalCPC', $totalCPC);
			$this->set('totalCPL', $totalCPL);
			$this->set('totalSCR', $totalSCR);
			$this->set('revenueCPL', $revenueCPL);
			$this->set('revenueCPM', $revenueCPM);
			$this->set('revenueCPC', $revenueCPC);
			$this->set('revenueSCR', $revenueSCR);
			$this->set('revenueByTypes', $revenueByTypes);
			$this->set('revenueByAdunits', $revenueByAdunits);
			$this->set('activeLines', count(array_unique($activeLineItems)));
			$this->set('adunit_name', $adunit_name);
			
			$this->set('country', $country);
			$this->set('DeviceType', $DeviceType);
			$this->set('browsers', $browsers);
			$this->set('osfamily', $osfamily);
		}
		
		public function view_user_profile($nl_adunit_uuid, $duration = null) {
			ini_set('memory_limit', '256M');
			$listid = null;
			
			$adunit_name = $this->tblAdunit->field('adunit_name', array('ad_uid' => $nl_adunit_uuid));
			
			$adunit_pubid = $this->tblAdunit->field('publisher_id', array('ad_uid' => $nl_adunit_uuid));
			
			$ad_dfp_id = $this->tblAdunit->field('ad_dfp_id', array('ad_uid' => $nl_adunit_uuid));
			
			$table_name = $adunit_pubid . "_" . $nl_adunit_uuid;
			$this->Dynamic->useTable = $table_name;
			$options['fields'] = array('Dynamic.nl_email');
			$options['limit'] = '7000';
			$resutls = $this->Dynamic->find('all', $options);
			
			
			$this->set('adunit_name', $adunit_name);
			
			$emails = array();
			foreach ($resutls as $email) {
				$emails[] = MD5($email['Dynamic']['nl_email']);
			}
			
			//print_r($emails);
			
			$userProfiles = $this->requestAction('/couchDB/viewKMAProfile', array('emailArray' => $emails));
			
			$items = $userProfiles;
			//print_r($items);
			
			$this->tblKmaResponse->virtualFields['sum'] = 'COUNT(*)';
			
			
			$MARITALS = array_filter($items, function($item) {
				return $item['doc']['KMA_MARITAL_STAT'] == 'S';
			});
			$MARITALM = array_filter($items, function($item) {
				return $item['doc']['KMA_MARITAL_STAT'] == 'M';
			});
			$MARITALNA = array_filter($items, function($item) {
				return $item['doc']['KMA_MARITAL_STAT'] == '';
			});
			$tblKmaMaritalStats = array();
			$tblKmaMaritalStats['Single'] = count($MARITALS);
			$tblKmaMaritalStats['Married'] = count($MARITALM);
			$tblKmaMaritalStats['N/A'] = count($MARITALNA);
			
			$AgeAGF = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'A' && $item['doc']['GENDER'] == 'F';
			});
			$AgeAGM = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'A' && $item['doc']['GENDER'] == 'M';
			});
			$AgeAGNA = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'A' && $item['doc']['GENDER'] == '';
			});
			$AgeBGF = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'B' && $item['doc']['GENDER'] == 'F';
			});
			$AgeBGM = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'B' && $item['doc']['GENDER'] == 'M';
			});
			$AgeBGNA = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'B' && $item['doc']['GENDER'] == '';
			});
			$AgeCGF = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'C' && $item['doc']['GENDER'] == 'F';
			});
			$AgeCGM = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'C' && $item['doc']['GENDER'] == 'M';
			});
			$AgeCGNA = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'C' && $item['doc']['GENDER'] == '';
			});
			$AgeDGF = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'D' && $item['doc']['GENDER'] == 'F';
			});
			$AgeDGM = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'D' && $item['doc']['GENDER'] == 'M';
			});
			$AgeDGNA = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'D' && $item['doc']['GENDER'] == '';
			});
			$AgeEGF = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'E' && $item['doc']['GENDER'] == 'F';
			});
			$AgeEGM = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'E' && $item['doc']['GENDER'] == 'M';
			});
			$AgeEGNA = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'E' && $item['doc']['GENDER'] == '';
			});
			$AgeFGF = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'F' && $item['doc']['GENDER'] == 'F';
			});
			$AgeFGM = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'F' && $item['doc']['GENDER'] == 'M';
			});
			$AgeFGNA = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == 'F' && $item['doc']['GENDER'] == '';
			});
			$AgeF = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == '' && $item['doc']['GENDER'] == 'F';
			});
			$AgeM = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == '' && $item['doc']['GENDER'] == 'M';
			});
			$AgeNA = array_filter($items, function($item) {
				return $item['doc']['KMA_AGEGRP'] == '' && $item['doc']['GENDER'] == '';
			});
			$tblKmaAgeGroup = array();
			$tblKmaAgeGroup['A']['F'] = count($AgeAGF);
			$tblKmaAgeGroup['A']['M'] = count($AgeAGM);
			$tblKmaAgeGroup['A'][''] = count($AgeAGNA);
			$tblKmaAgeGroup['B']['F'] = count($AgeBGF);
			$tblKmaAgeGroup['B']['M'] = count($AgeBGM);
			$tblKmaAgeGroup['B'][''] = count($AgeBGNA);
			$tblKmaAgeGroup['C']['F'] = count($AgeCGF);
			$tblKmaAgeGroup['C']['M'] = count($AgeCGM);
			$tblKmaAgeGroup['C'][''] = count($AgeCGNA);
			$tblKmaAgeGroup['D']['F'] = count($AgeDGF);
			$tblKmaAgeGroup['D']['M'] = count($AgeDGM);
			$tblKmaAgeGroup['D'][''] = count($AgeDGNA);
			$tblKmaAgeGroup['E']['F'] = count($AgeEGF);
			$tblKmaAgeGroup['E']['M'] = count($AgeEGM);
			$tblKmaAgeGroup['E'][''] = count($AgeEGNA);
			$tblKmaAgeGroup['F']['F'] = count($AgeFGF);
			$tblKmaAgeGroup['F']['M'] = count($AgeFGM);
			$tblKmaAgeGroup['F'][''] = count($AgeFGNA);
			$tblKmaAgeGroup['']['M'] = count($AgeM);
			$tblKmaAgeGroup['']['F'] = count($AgeF);
			$tblKmaAgeGroup[''][''] = count($AgeNA);
			
			$KMA_EDUCA = array_filter($items, function($item) {
				return $item['doc']['KMA_EDUC'] == 'A';
			});
			$KMA_EDUCB = array_filter($items, function($item) {
				return $item['doc']['KMA_EDUC'] == 'B';
			});
			$KMA_EDUCC = array_filter($items, function($item) {
				return $item['doc']['KMA_EDUC'] == 'C';
			});
			$KMA_EDUCD = array_filter($items, function($item) {
				return $item['doc']['KMA_EDUC'] == 'D';
			});
			$KMA_EDUCNA = array_filter($items, function($item) {
				return $item['doc']['KMA_EDUC'] == '';
			});
			$tblKmaEducation = array();
			$tblKmaEducation['Some High School'] = count($KMA_EDUCA);
			$tblKmaEducation['High School Grad'] = count($KMA_EDUCB);
			$tblKmaEducation['Some College'] = count($KMA_EDUCC);
			$tblKmaEducation['College Graduate'] = count($KMA_EDUCD);
			$tblKmaEducation['N/A'] = count($KMA_EDUCNA);
			
			$OCCUPA = array_filter($items, function($item) {
				return $item['doc']['KMA_OCCUP'] == 'A';
			});
			$OCCUPB = array_filter($items, function($item) {
				return $item['doc']['KMA_OCCUP'] == 'B';
			});
			$OCCUPC = array_filter($items, function($item) {
				return $item['doc']['KMA_OCCUP'] == 'C';
			});
			$OCCUPD = array_filter($items, function($item) {
				return $item['doc']['KMA_OCCUP'] == 'D';
			});
			$OCCUPE = array_filter($items, function($item) {
				return $item['doc']['KMA_OCCUP'] == 'E';
			});
			$OCCUPF = array_filter($items, function($item) {
				return $item['doc']['KMA_OCCUP'] == 'F';
			});
			$OCCUPG = array_filter($items, function($item) {
				return $item['doc']['KMA_OCCUP'] == 'G';
			});
			$OCCUPH = array_filter($items, function($item) {
				return $item['doc']['KMA_OCCUP'] == 'H';
			});
			$OCCUPNA = array_filter($items, function($item) {
				return $item['doc']['KMA_OCCUP'] == '';
			});
			$tblKmaOccup = array();
			$tblKmaOccup['Prof/Tech'] = count($OCCUPA);
			$tblKmaOccup['Admin / Manager'] = count($OCCUPB);
			$tblKmaOccup['Blue Collar'] = count($OCCUPC);
			$tblKmaOccup['Clerical/Service'] = count($OCCUPD);
			$tblKmaOccup['Homemaker'] = count($OCCUPE);
			$tblKmaOccup['Retired'] = count($OCCUPF);
			$tblKmaOccup['Business Owner'] = count($OCCUPG);
			$tblKmaOccup['Sales/Marketing'] = count($OCCUPH);
			$tblKmaOccup['N/A'] = count($OCCUPNA);
			
			
			$KMA_HHINCA = array_filter($items, function($item) {
				return $item['doc']['KMA_HHINC'] == 'A';
			});
			$KMA_HHINCB = array_filter($items, function($item) {
				return $item['doc']['KMA_HHINC'] == 'B';
			});
			$KMA_HHINCC = array_filter($items, function($item) {
				return $item['doc']['KMA_HHINC'] == 'C';
			});
			$KMA_HHINCD = array_filter($items, function($item) {
				return $item['doc']['KMA_HHINC'] == 'D';
			});
			$KMA_HHINCE = array_filter($items, function($item) {
				return $item['doc']['KMA_HHINC'] == 'E';
			});
			$KMA_HHINCF = array_filter($items, function($item) {
				return $item['doc']['KMA_HHINC'] == 'F';
			});
			$KMA_HHINCNA = array_filter($items, function($item) {
				return $item['doc']['KMA_HHINC'] == '';
			});
			
			$tblKmahhinc = array();
			$tblKmahhinc['Under 30 k'] = count($KMA_HHINCA);
			$tblKmahhinc['30k to 50k'] = count($KMA_HHINCB);
			$tblKmahhinc['50k to 75k'] = count($KMA_HHINCC);
			$tblKmahhinc['75k to 100k'] = count($KMA_HHINCD);
			$tblKmahhinc['100k to 150k'] = count($KMA_HHINCE);
			$tblKmahhinc['150k+'] = count($KMA_HHINCF);
			$tblKmahhinc['N/A'] = count($KMA_HHINCNA);
			
			
			$KMA_NETWORTHA = array_filter($items, function($item) {
				return $item['doc']['KMA_NETWORTH'] == 'A';
			});
			$KMA_NETWORTHB = array_filter($items, function($item) {
				return $item['doc']['KMA_NETWORTH'] == 'B';
			});
			$KMA_NETWORTHC = array_filter($items, function($item) {
				return $item['doc']['KMA_NETWORTH'] == 'C';
			});
			$KMA_NETWORTHD = array_filter($items, function($item) {
				return $item['doc']['KMA_NETWORTH'] == 'D';
			});
			$KMA_NETWORTHNA = array_filter($items, function($item) {
				return $item['doc']['KMA_NETWORTH'] == '';
			});
			
			$tblKmaNetWorth = array();
			$tblKmaNetWorth['Under 100k'] = count($KMA_NETWORTHA);
			$tblKmaNetWorth['100k to 250k'] = count($KMA_NETWORTHB);
			$tblKmaNetWorth['250k to 500k'] = count($KMA_NETWORTHC);
			$tblKmaNetWorth['500k+'] = count($KMA_NETWORTHD);
			$tblKmaNetWorth['N/A'] = count($KMA_NETWORTHNA);
			
			
			
			$KMA_OWNRENTO = array_filter($items, function($item) {
				return $item['doc']['KMA_OWNRENT'] == 'O';
			});
			$KMA_OWNRENTR = array_filter($items, function($item) {
				return $item['doc']['KMA_OWNRENT'] == 'R';
			});
			$KMA_OWNRENTNA = array_filter($items, function($item) {
				return $item['doc']['KMA_OWNRENT'] == '';
			});
			
			$tblKmaOwnRent = array();
			$tblKmaOwnRent['Owner'] = count($KMA_OWNRENTO);
			$tblKmaOwnRent['Renter'] = count($KMA_OWNRENTR);
			$tblKmaOwnRent['N/A'] = count($KMA_OWNRENTNA);
			
			
			$KMA_HSGS = array_filter($items, function($item) {
				return $item['doc']['KMA_HSG'] == 'S';
			});
			$KMA_HSGM = array_filter($items, function($item) {
				return $item['doc']['KMA_HSG'] == 'M';
			});
			$KMA_HSGT = array_filter($items, function($item) {
				return $item['doc']['KMA_HSG'] == 'T';
			});
			$KMA_HSGNA = array_filter($items, function($item) {
				return $item['doc']['KMA_HSG'] == '';
			});
			
			$tblKmaHsg = array();
			$tblKmaHsg['Single Family Homes'] = count($KMA_HSGS);
			$tblKmaHsg['Apartments'] = count($KMA_HSGM);
			$tblKmaHsg['Mobile Homes'] = count($KMA_HSGT);
			$tblKmaHsg['N/A'] = count($KMA_HSGNA);
			
			
			//Pending
			$tblKmaLmos = array();
			
			
			$KMA_HOMEOFFICEArray = array_filter($items, function($item) {
				return $item['doc']['KMA_HOMEOFFICE'] == 'Y';
			});
			$KMA_HOMEOFFICE = count($KMA_HOMEOFFICEArray);
			
			
			$KMA_SOHOArray = array_filter($items, function($item) {
				return $item['doc']['KMA_SOHO'] == 'Y';
			});
			$KMA_SOHO = count($KMA_SOHOArray);
			
			
			$KMA_CONTEDArray = array_filter($items, function($item) {
				return $item['doc']['KMA_CONTED'] == 'Y';
			});
			$KMA_CONTED = count($KMA_CONTEDArray);
			
			
			$KMA_CIGAR_SMOKERArray = array_filter($items, function($item) {
				return $item['doc']['KMA_CIGAR_SMOKER'] == 'Y';
			});
			$KMA_CIGAR_SMOKER = count($KMA_CIGAR_SMOKERArray);
			
			
			$KMA_SMOKERArray = array_filter($items, function($item) {
				return $item['doc']['KMA_SMOKER'] == 'Y';
			});
			$KMA_SMOKER = count($KMA_SMOKERArray);
			
			
			$KMA_WTLOSSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_WTLOSS'] == 'Y';
			});
			$KMA_WTLOSS = count($KMA_WTLOSSArray);
			
			
			$KMA_HEALTHArray = array_filter($items, function($item) {
				return $item['doc']['KMA_HEALTH'] == 'Y';
			});
			$KMA_HEALTH = count($KMA_HEALTHArray);
			
			
			$KMA_EXERCISEArray = array_filter($items, function($item) {
				return $item['doc']['KMA_EXERCISE'] == 'Y';
			});
			$KMA_EXERCISE = count($KMA_EXERCISEArray);
			
			
			$KMA_INVESTMENTSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_INVESTMENTS'] == 'Y';
			});
			$KMA_INVESTMENTS = count($KMA_INVESTMENTSArray);
			
			
			$KMA_OPSEEKArray = array_filter($items, function($item) {
				return $item['doc']['KMA_OPSEEK'] == 'Y';
			});
			$KMA_OPSEEK = count($KMA_OPSEEKArray);
			
			
			$KMA_INVEST_HIGHENDArray = array_filter($items, function($item) {
				return $item['doc']['KMA_INVEST_HIGHEND'] == 'Y';
			});
			$KMA_INVEST_HIGHEND = count($KMA_INVEST_HIGHENDArray);
			
			
			$KMA_INVEST_LOWENDArray = array_filter($items, function($item) {
				return $item['doc']['KMA_INVEST_LOWEND'] == 'Y';
			});
			$KMA_INVEST_LOWEND = count($KMA_INVEST_LOWENDArray);
			
			
			$KMA_PETS_CATSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_PETS_CATS'] == 'Y';
			});
			$KMA_PETS_CATS = count($KMA_PETS_CATSArray);
			
			
			$KMA_PETS_DOGSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_PETS_DOGS'] == 'Y';
			});
			$KMA_PETS_DOGS = count($KMA_PETS_DOGSArray);
			
			
			$KMA_PETSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_PETS'] == 'Y';
			});
			$KMA_PETS = count($KMA_PETSArray);
			
			
			$KMA_SRPRODSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_SRPRODS'] == 'Y';
			});
			$KMA_SRPRODS = count($KMA_SRPRODSArray);
			
			
			$KMA_TRAVELArray = array_filter($items, function($item) {
				return $item['doc']['KMA_TRAVEL'] == 'Y';
			});
			$KMA_TRAVEL = count($KMA_TRAVELArray);
			
			
			$KMA_TRAVEL_HIGHENDArray = array_filter($items, function($item) {
				return $item['doc']['KMA_TRAVEL_HIGHEND'] == 'Y';
			});
			$KMA_TRAVEL_HIGHEND = count($KMA_TRAVEL_HIGHENDArray);
			
			
			$KMA_TRAVEL_LOWENDArray = array_filter($items, function($item) {
				return $item['doc']['KMA_TRAVEL_LOWEND'] == 'Y';
			});
			$KMA_TRAVEL_LOWEND = count($KMA_TRAVEL_LOWENDArray);
			
			
			$KMA_WOMFASHArray = array_filter($items, function($item) {
				return $item['doc']['KMA_WOMFASH'] == 'Y';
			});
			$KMA_WOMFASH = count($KMA_WOMFASHArray);
			
			
			$KMA_WOMFASH_HIGHENDArray = array_filter($items, function($item) {
				return $item['doc']['KMA_WOMFASH_HIGHEND'] == 'Y';
			});
			$KMA_WOMFASH_HIGHEND = count($KMA_WOMFASH_HIGHENDArray);
			
			
			$KMA_WOMFASH_LOWENDArray = array_filter($items, function($item) {
				return $item['doc']['KMA_WOMFASH_LOWEND'] == 'Y';
			});
			$KMA_WOMFASH_LOWEND = count($KMA_WOMFASH_LOWENDArray);
			
			
			$KMA_MENFASHArray = array_filter($items, function($item) {
				return $item['doc']['KMA_MENFASH'] == 'Y';
			});
			$KMA_MENFASH = count($KMA_MENFASHArray);
			
			
			$KMA_TECHNOLOGYArray = array_filter($items, function($item) {
				return $item['doc']['KMA_TECHNOLOGY'] == 'Y';
			});
			$KMA_TECHNOLOGY = count($KMA_TECHNOLOGYArray);
			
			
			$KMA_DEALSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_DEALS'] == 'Y';
			});
			$KMA_DEALS = count($KMA_DEALSArray);
			
			
			$KMA_FOODWINEArray = array_filter($items, function($item) {
				return $item['doc']['KMA_FOODWINE'] == 'Y';
			});
			$KMA_FOODWINE = count($KMA_FOODWINEArray);
			
			
			$KMA_GAMERSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_GAMERS'] == 'Y';
			});
			$KMA_GAMERS = count($KMA_GAMERSArray);
			
			
			$KMA_DECORArray = array_filter($items, function($item) {
				return $item['doc']['KMA_DECOR'] == 'Y';
			});
			$KMA_DECOR = count($KMA_DECORArray);
			
			
			$KMA_HOMEGARDArray = array_filter($items, function($item) {
				return $item['doc']['KMA_HOMEGARD'] == 'Y';
			});
			$KMA_HOMEGARD = count($KMA_HOMEGARDArray);
			
			
			$KMA_KIDSPRODArray = array_filter($items, function($item) {
				return $item['doc']['KMA_KIDSPROD'] == 'Y';
			});
			$KMA_KIDSPROD = count($KMA_KIDSPRODArray);
			
			
			$KMA_MOTORCYCLEArray = array_filter($items, function($item) {
				return $item['doc']['KMA_MOTORCYCLE'] == 'Y';
			});
			$KMA_MOTORCYCLE = count($KMA_MOTORCYCLEArray);
			
			
			$KMA_CURREVENTArray = array_filter($items, function($item) {
				return $item['doc']['KMA_CURREVENT'] == 'Y';
			});
			$KMA_CURREVENT = count($KMA_CURREVENTArray);
			
			
			$KMA_POL_LEFTArray = array_filter($items, function($item) {
				return $item['doc']['KMA_POL_LEFT'] == 'Y';
			});
			$KMA_POL_LEFT = count($KMA_POL_LEFTArray);
			
			
			$KMA_POL_RIGHTArray = array_filter($items, function($item) {
				return $item['doc']['KMA_POL_RIGHT'] == 'Y';
			});
			$KMA_POL_RIGHT = count($KMA_POL_RIGHTArray);
			
			
			$KMA_POL_INDArray = array_filter($items, function($item) {
				return $item['doc']['KMA_POL_IND'] == 'Y';
			});
			$KMA_POL_IND = count($KMA_POL_INDArray);
			
			
			$KMA_SWEEPSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_SWEEPS'] == 'Y';
			});
			$KMA_SWEEPS = count($KMA_SWEEPSArray);
			
			
			$KMA_DONORSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_DONORS'] == 'Y';
			});
			$KMA_DONORS = count($KMA_DONORSArray);
			
			$KMA_CRAFTS_COLLArray = array_filter($items, function($item) {
				return $item['doc']['KMA_CRAFTS_COLL'] == 'Y';
			});
			$KMA_CRAFTS_COLL = count($KMA_CRAFTS_COLLArray);
			
			$KMA_AUTOMOTIVEArray = array_filter($items, function($item) {
				return $item['doc']['KMA_AUTOMOTIVE'] == 'Y';
			});
			$KMA_AUTOMOTIVE = count($KMA_AUTOMOTIVEArray);
			
			$KMA_OUTDOORSArray = array_filter($items, function($item) {
				return $item['doc']['KMA_OUTDOORS'] == 'Y';
			});
			$KMA_OUTDOORS = count($KMA_OUTDOORSArray);
			
			
			//print_r($tblKmaAgeGroup);
			$this->set('tblKmaMaritalStats', $tblKmaMaritalStats);
			$this->set('tblKmaAgeGroup', $tblKmaAgeGroup);
			$this->set('tblKmaEducation', $tblKmaEducation);
			$this->set('tblKmaOccup', $tblKmaOccup);
			$this->set('tblKmahhinc', $tblKmahhinc);
			$this->set('tblKmaNetWorth', $tblKmaNetWorth);
			$this->set('tblKmaOwnRent', $tblKmaOwnRent);
			$this->set('tblKmaHsg', $tblKmaHsg);
			$this->set('tblKmaLmos', $tblKmaLmos);
			$this->set('KMA_OUTDOORS', $KMA_OUTDOORS);
			$this->set('KMA_AUTOMOTIVE', $KMA_AUTOMOTIVE);
			$this->set('KMA_CRAFTS_COLL', $KMA_CRAFTS_COLL);
			$this->set('KMA_DONORS', $KMA_DONORS);
			$this->set('KMA_SWEEPS', $KMA_SWEEPS);
			$this->set('KMA_POL_IND', $KMA_POL_IND);
			$this->set('KMA_POL_RIGHT', $KMA_POL_RIGHT);
			$this->set('KMA_POL_LEFT', $KMA_POL_LEFT);
			$this->set('KMA_CURREVENT', $KMA_CURREVENT);
			$this->set('KMA_MOTORCYCLE', $KMA_MOTORCYCLE);
			$this->set('KMA_KIDSPROD', $KMA_KIDSPROD);
			$this->set('KMA_HOMEGARD', $KMA_HOMEGARD);
			$this->set('KMA_DECOR', $KMA_DECOR);
			$this->set('KMA_GAMERS', $KMA_GAMERS);
			$this->set('KMA_FOODWINE', $KMA_FOODWINE);
			$this->set('KMA_DEALS', $KMA_DEALS);
			$this->set('KMA_TECHNOLOGY', $KMA_TECHNOLOGY);
			$this->set('KMA_MENFASH', $KMA_MENFASH);
			$this->set('KMA_WOMFASH_LOWEND', $KMA_WOMFASH_LOWEND);
			$this->set('KMA_WOMFASH_HIGHEND', $KMA_WOMFASH_HIGHEND);
			$this->set('KMA_WOMFASH', $KMA_WOMFASH);
			$this->set('KMA_TRAVEL_LOWEND', $KMA_TRAVEL_LOWEND);
			$this->set('KMA_TRAVEL_HIGHEND', $KMA_TRAVEL_HIGHEND);
			$this->set('KMA_TRAVEL', $KMA_TRAVEL);
			$this->set('KMA_SRPRODS', $KMA_SRPRODS);
			$this->set('KMA_PETS', $KMA_PETS);
			$this->set('KMA_PETS_DOGS', $KMA_PETS_DOGS);
			$this->set('KMA_PETS_CATS', $KMA_PETS_CATS);
			$this->set('KMA_INVEST_LOWEND', $KMA_INVEST_LOWEND);
			$this->set('KMA_INVEST_HIGHEND', $KMA_INVEST_HIGHEND);
			$this->set('KMA_OPSEEK', $KMA_OPSEEK);
			$this->set('KMA_INVESTMENTS', $KMA_INVESTMENTS);
			$this->set('KMA_EXERCISE', $KMA_EXERCISE);
			$this->set('KMA_HEALTH', $KMA_HEALTH);
			$this->set('KMA_WTLOSS', $KMA_WTLOSS);
			$this->set('KMA_SMOKER', $KMA_SMOKER);
			$this->set('KMA_CIGAR_SMOKER', $KMA_CIGAR_SMOKER);
			$this->set('KMA_CONTED', $KMA_CONTED);
			$this->set('KMA_SOHO', $KMA_SOHO);
			$this->set('KMA_HOMEOFFICE', $KMA_HOMEOFFICE);
			$this->set('duration', $duration);
		}
		
		/*
			public function view_user_profile($nl_adunit_uuid, $duration = null) {
			$listid = null;
			
			$options['joins'] = array(
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblp.profileID = User.tblProfileID')
			));
			$options['fields'] = array('tblp.tracksite_id');
			$options['conditions'] = array('User.id' => $this->Authake->getUserId());
			
			$tracksiteArr = $this->User->find('first', $options);
			$tracksite_id = $tracksiteArr['tblp']['tracksite_id'];
			
			if($duration == null)
			$duration = "month";
			
			if($listid == null)
			$listid = "all";
			
			switch($duration)
			{
			case "day" :
			$DateRange = $this->rangeDay(date("Y-m-d"));
			break;
			case "week" :
			$DateRange = $this->rangeWeek(date("Y-m-d"));
			break;
			case "month" :
			$DateRange = $this->rangeMonth(date("Y-m-d"));
			break;
			case "year" :
			$DateRange = $this->rangeYear(date("Y-m-d"));
			break;
			default :
			$DateRange = $this->rangeWeek(date("Y-m-d"));
			break;
			}
			$adunit_name = $this->tblAdunit->field('adunit_name', array('ad_uid' =>$nl_adunit_uuid));
			$ad_dfp_id = $this->tblAdunit->field('ad_dfp_id', array('ad_uid' =>$nl_adunit_uuid));
			
			$this->tblKmaResponse->virtualFields['sum'] = 'COUNT(*)';
			
			$tblKmaMaritalStats = $this->tblKmaResponse->find('list', array(
			'fields' => array('tblKmaResponse.KMA_MARITAL_STAT', 'tblKmaResponse.sum'),
			'group' => 'tblKmaResponse.KMA_MARITAL_STAT','conditions' => array('not' => array('tblKmaResponse.KMA_MARITAL_STAT' => '','tblKmaResponse.kma_adunit_id' => ''))));
			
			$tblKmaAgeGroup = $this->tblKmaResponse->find('list', array(
			'fields' => array('tblKmaResponse.GENDER','tblKmaResponse.sum','tblKmaResponse.KMA_AGEGRP'),
			'group' => 'tblKmaResponse.KMA_AGEGRP,tblKmaResponse.GENDER','conditions' => array('not' => array('tblKmaResponse.KMA_AGEGRP' => '','tblKmaResponse.kma_adunit_id' => ''))));
			//echo "<pre>";
			//print_r($tblKmaAgeGroup);
			//echo "</pre>";
			$tblKmaGender = $this->tblKmaResponse->find('list', array(
			'fields' => array('tblKmaResponse.GENDER', 'tblKmaResponse.sum'),
			'group' => 'tblKmaResponse.GENDER','conditions' => array('not' => array('tblKmaResponse.GENDER' => '','tblKmaResponse.kma_adunit_id' => ''))));
			
			
			$tblKmaEducation = $this->tblKmaResponse->find('list', array(
			'fields' => array('tblKmaResponse.KMA_EDUC', 'tblKmaResponse.sum'),
			'group' => 'tblKmaResponse.KMA_EDUC','conditions' => array('not' => array('tblKmaResponse.KMA_EDUC' => '','tblKmaResponse.kma_adunit_id' => ''))));
			
			
			$tblKmaOccup = $this->tblKmaResponse->find('list', array(
			'fields' => array('tblKmaResponse.KMA_OCCUP', 'tblKmaResponse.sum'),
			'group' => 'tblKmaResponse.KMA_OCCUP','conditions' => array('not' => array('tblKmaResponse.KMA_OCCUP' => '','tblKmaResponse.kma_adunit_id' => ''))));
			
			
			$tblKmahhinc = $this->tblKmaResponse->find('list', array(
			'fields' => array('tblKmaResponse.KMA_HHINC', 'tblKmaResponse.sum'),
			'group' => 'tblKmaResponse.KMA_HHINC','conditions' => array('not' => array('tblKmaResponse.KMA_HHINC' => '','tblKmaResponse.kma_adunit_id' => ''))));
			
			
			$tblKmaNetWorth = $this->tblKmaResponse->find('list', array(
			'fields' => array('tblKmaResponse.KMA_NETWORTH', 'tblKmaResponse.sum'),
			'group' => 'tblKmaResponse.KMA_NETWORTH','conditions' => array('not' => array('tblKmaResponse.KMA_NETWORTH' => '','tblKmaResponse.kma_adunit_id' => ''))));
			
			
			$tblKmaOwnRent = $this->tblKmaResponse->find('list', array(
			'fields' => array('tblKmaResponse.KMA_OWNRENT', 'tblKmaResponse.sum'),
			'group' => 'tblKmaResponse.KMA_OWNRENT','conditions' => array('not' => array('tblKmaResponse.KMA_OWNRENT' => '','tblKmaResponse.kma_adunit_id' => ''))));
			
			
			$tblKmaHsg = $this->tblKmaResponse->find('list', array(
			'fields' => array('tblKmaResponse.KMA_HSG', 'tblKmaResponse.sum'),
			'group' => 'tblKmaResponse.KMA_HSG','conditions' => array('not' => array('tblKmaResponse.KMA_HSG' => '','tblKmaResponse.kma_adunit_id' => ''))));
			
			
			$tblKmaLmos = $this->tblKmaResponse->find('list', array(
			'fields' => array('tblKmaResponse.KMA_LMOS', 'tblKmaResponse.sum'),
			'group' => 'tblKmaResponse.KMA_LMOS','conditions' => array('not' => array('tblKmaResponse.KMA_LMOS' => '','tblKmaResponse.kma_adunit_id' => '')), 'order' => array('tblKmaResponse.KMA_LMOS')));
			
			$KMA_HOMEOFFICE = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_HOMEOFFICE' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_SOHO = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_SOHO' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_CONTED = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_CONTED' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_CIGAR_SMOKER = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_CIGAR_SMOKER' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_SMOKER = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_SMOKER' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_WTLOSS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_WTLOSS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_HEALTH = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_HEALTH' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_EXERCISE = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_EXERCISE' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_INVESTMENTS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_INVESTMENTS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_OPSEEK = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_OPSEEK' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_INVEST_HIGHEND = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_INVEST_HIGHEND' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_INVEST_LOWEND = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_INVEST_LOWEND' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_PETS_CATS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_PETS_CATS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_PETS_DOGS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_PETS_DOGS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_PETS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_PETS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_SRPRODS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_SRPRODS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_TRAVEL = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_TRAVEL' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_TRAVEL_HIGHEND = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_TRAVEL_HIGHEND' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_TRAVEL_LOWEND = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_TRAVEL_LOWEND' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_WOMFASH = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_WOMFASH' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_WOMFASH_HIGHEND = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_WOMFASH_HIGHEND' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_WOMFASH_LOWEND = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_WOMFASH_LOWEND' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_MENFASH = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_MENFASH' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_TECHNOLOGY = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_TECHNOLOGY' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_DEALS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_DEALS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_FOODWINE = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_FOODWINE' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_GAMERS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_GAMERS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_DECOR = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_DECOR' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_HOMEGARD = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_HOMEGARD' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_KIDSPROD = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_KIDSPROD' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_MOTORCYCLE = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_MOTORCYCLE' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_CURREVENT = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_CURREVENT' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_POL_LEFT = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_POL_LEFT' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_POL_RIGHT = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_POL_RIGHT' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_POL_IND = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_POL_IND' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_SWEEPS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_SWEEPS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_DONORS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_DONORS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_CRAFTS_COLL = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_CRAFTS_COLL' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_AUTOMOTIVE = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_AUTOMOTIVE' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			$KMA_OUTDOORS = $this->tblKmaResponse->find('count', array('conditions' => array('tblKmaResponse.KMA_OUTDOORS' => 'Y', 'not' => array('tblKmaResponse.kma_adunit_id' => ''))));
			
			//print_r($tblKmaAgeGroup);
			$this->set('tblKmaMaritalStats', $tblKmaMaritalStats);
			$this->set('tblKmaAgeGroup', $tblKmaAgeGroup);
			$this->set('tblKmaGender', $tblKmaGender);
			$this->set('tblKmaEducation', $tblKmaEducation);
			$this->set('tblKmaOccup', $tblKmaOccup);
			$this->set('tblKmahhinc', $tblKmahhinc);
			$this->set('tblKmaNetWorth', $tblKmaNetWorth);
			$this->set('tblKmaOwnRent', $tblKmaOwnRent);
			$this->set('tblKmaHsg', $tblKmaHsg);
			$this->set('tblKmaLmos', $tblKmaLmos);
			$this->set('KMA_OUTDOORS', $KMA_OUTDOORS);
			$this->set('KMA_AUTOMOTIVE', $KMA_AUTOMOTIVE);
			$this->set('KMA_CRAFTS_COLL', $KMA_CRAFTS_COLL);
			$this->set('KMA_DONORS', $KMA_DONORS);
			$this->set('KMA_SWEEPS', $KMA_SWEEPS);
			$this->set('KMA_POL_IND', $KMA_POL_IND);
			$this->set('KMA_POL_RIGHT', $KMA_POL_RIGHT);
			$this->set('KMA_POL_LEFT', $KMA_POL_LEFT);
			$this->set('KMA_CURREVENT', $KMA_CURREVENT);
			$this->set('KMA_MOTORCYCLE', $KMA_MOTORCYCLE);
			$this->set('KMA_KIDSPROD', $KMA_KIDSPROD);
			$this->set('KMA_HOMEGARD', $KMA_HOMEGARD);
			$this->set('KMA_DECOR', $KMA_DECOR);
			$this->set('KMA_GAMERS', $KMA_GAMERS);
			$this->set('KMA_FOODWINE', $KMA_FOODWINE);
			$this->set('KMA_DEALS', $KMA_DEALS);
			$this->set('KMA_TECHNOLOGY', $KMA_TECHNOLOGY);
			$this->set('KMA_MENFASH', $KMA_MENFASH);
			$this->set('KMA_WOMFASH_LOWEND', $KMA_WOMFASH_LOWEND);
			$this->set('KMA_WOMFASH_HIGHEND', $KMA_WOMFASH_HIGHEND);
			$this->set('KMA_WOMFASH', $KMA_WOMFASH);
			$this->set('KMA_TRAVEL_LOWEND', $KMA_TRAVEL_LOWEND);
			$this->set('KMA_TRAVEL_HIGHEND', $KMA_TRAVEL_HIGHEND);
			$this->set('KMA_TRAVEL', $KMA_TRAVEL);
			$this->set('KMA_SRPRODS', $KMA_SRPRODS);
			$this->set('KMA_PETS', $KMA_PETS);
			$this->set('KMA_PETS_DOGS', $KMA_PETS_DOGS);
			$this->set('KMA_PETS_CATS', $KMA_PETS_CATS);
			$this->set('KMA_INVEST_LOWEND', $KMA_INVEST_LOWEND);
			$this->set('KMA_INVEST_HIGHEND', $KMA_INVEST_HIGHEND);
			$this->set('KMA_OPSEEK', $KMA_OPSEEK);
			$this->set('KMA_INVESTMENTS', $KMA_INVESTMENTS);
			$this->set('KMA_EXERCISE', $KMA_EXERCISE);
			$this->set('KMA_HEALTH', $KMA_HEALTH);
			$this->set('KMA_WTLOSS', $KMA_WTLOSS);
			$this->set('KMA_SMOKER', $KMA_SMOKER);
			$this->set('KMA_CIGAR_SMOKER', $KMA_CIGAR_SMOKER);
			$this->set('KMA_CONTED', $KMA_CONTED);
			$this->set('KMA_SOHO', $KMA_SOHO);
			$this->set('KMA_HOMEOFFICE', $KMA_HOMEOFFICE);
			$this->set('duration', $duration);
			$this->set('adunit_name', $adunit_name);
		} */
		
		public function add_coreg_placement() {
			
		}
		
		function rangeMonth($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('first day of this month', $dt));
			$res['end'] = date('Y-m-d', strtotime('last day of this month', $dt));
			return $res;
		}
		
		function rangeWeek($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('-7 Days', $dt));
			$res['end'] = $datestr;
			return $res;
		}
		
		function rangeYear($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('-360 Days', $dt));
			$res['end'] = $datestr;
			return $res;
		}
		
		function rangeDay($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = $datestr;
			$res['end'] = $datestr;
			return $res;
		}
		
		private function getPercentage($field = 1, $total = 1, $div = 1000) {
			return ($total == 0) ? 0 : ($field * $div / $total);
		}
		
		private function tabl5data($nl_adunit_uuid = null, $DateRange = array()) {
			
		}
		
		private function __TotalLiveCampaignThisMonth($nl_adunit_uuid, $DateRange, $duration) {
			$options['joins'] = array(
            array('table' => 'tbl_adunits',
			'alias' => 'tblAdunit',
			'type' => 'INNER',
			'conditions' => array(
			'tblReportDay.rd_ad_unit_id = tblAdunit.ad_dfp_id')
			));
		}
		
		private function tbl_adunits($nl_adunit_uuid = null, $DateRange = array(), $duration = null) {
			
		}
		
		private function __RevenueOpenClickBySource($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
			$subscribers = array();
			$this->Dynamic->useTable = $this->tblAdunit->getEmailtableName($nl_adunit_uuid); // get here talbe name of das email
			// Dropped / Bounced Subscribers 
			$options = array();
			$options['joins'] = array(
            array('table' => 'das_digitaladvertising.tbl_mailings',
			'alias' => 'tblMailing',
			'type' => 'INNER',
			'conditions' => array(
			'Dynamic.nl_adunit_uuid = tblMailing.m_ad_uid')),
            array('table' => 'tbl_revenue_by_source',
			'alias' => 'tblRevenuBySource',
			'type' => 'INNER',
			'conditions' => array(
			'tblMailing.m_id = tblRevenuBySource.m_id')));
			
			$this->Dynamic->virtualFields = array('totalopens' => 'count(tblMailing.m_open)', 'totalclicks' => 'count(tblMailing.m_clicks)');
			$options['conditions'] = array('Dynamic.nl_isactive = 1');
			$options['group'] = array('tblRevenuBySource.source_name');
			$options['fields'] = array('tblRevenuBySource.source_name', 'totalopens', 'totalclicks');
			return $this->Dynamic->find('all', $options);
		}
		
		function tabl6data() {
			
		}
		
	}