<?php
	App::import('Controller', 'LyrisController');
	
	class SocialController extends AppController {
		
		var $uses = array('Authake.User', 'Authake.Rule', 'Authake.Group','Authake.tblProfile','Authake.tblTrackingDeliver','Authake.tblTrackingOpen','Authake.tblTrackingClick', 'Authake.tblCreative', 'Authake.tblLineItem','Authake.tblAdunit','Authake.tblMailing','Authake.tblMessage','Dynamic');
		
		public function beforeFilter() {
			parent::beforeFilter();
			$this->layout=null;
		}
		
		public function index($listid) {
			
			
		}
		
		
		public function fbRegUser($name, $email){
			$this->autoRender = false;
			$tries = 0;
			while ($tries <= 10) {
				$userid = $this->buildShortUrl();
				
				if(!$this->checkExisting($userid)) {
					$this->request->data['User']['login'] = $userid;
					break;
				}
			}
			
			$this->request->data['User']['email'] = $email;
			$this->request->data['Group']['Group']= 6;
			
			if (!empty($this->request->data))
			{
				$this->User->recursive = 0;/* If settings say we should use only email info instead of username/email, skip this */
				if (Configure::read('Authake.useEmailAsUsername') == false)
				{
					$exist = $this->User->findByLogin($this->request->data['User']['login']);
					
					if (!empty($exist))
					{
						$this->Session->setFlash(__('This login is already used!'), 'error', array('plugin' => 'Authake'));
						return;
					}
				}
				else
				{
					$exist = $this->User->findByEmail($this->request->data['User']['email']);
					
					if (!empty($exist))
					{
						$this->Session->setFlash(__('This email is already registred!'), 'error', array('plugin' => 'Authake'));
						return;
					}
				}
				$pwd = $this->buildShortUrl();
				
				if (!$pwd)
				{
					return;
				}
				
				// password is invalid...
				$this->request->data['User']['password'] = $pwd;
				//$this->request->data['User']['emailcheckcode'] = md5(time()*rand());
				$this->User->create();//add default group if there is such thing
				
				if ($this->User->save($this->request->data))
				{
					$this->request->data['tblProfile']['user_id'] = $this->User->id;
					$this->request->data['tblProfile']['owner_user_id'] = $this->User->id;
					$this->request->data['tblProfile']['ContactName'] = $name;
					$this->tblProfile->create();
					$this->tblProfile->save($this->request->data);
					return ;
				}
			}
			return;
			//return;
			
		}
		
		
		
		public function InstagramAdUnit($email, $token){
			$this->autoRender = false;
			$pubId =  $this->User->field('id', array('email' => $email));
			$pubLogin =  $this->User->field('login', array('email' => $email));
			$this->request->data['tblAdunit']['adunit_name'] = "AD_UNIT_INSTAGRAM_" . $pubLogin;
			$this->request->data['tblAdunit']['publisher_id'] = $pubId;
			$this->request->data['tblAdunit']['ad_desc'] = "INSTAGRAM AD UNIT";
			$this->request->data['tblAdunit']['ad_unit_sizes'] = "1X1";
			$this->request->data['tblAdunit']['ad_ptype_id'] = 17;
			$this->request->data['tblAdunit']['owner_user_id'] = $pubId;
			$this->request->data['tblAdunit']['ad_social_token'] = $token;
			if (!$this->checkExistingAdunit("AD_UNIT_INSTAGRAM_" . $pubLogin))
			{
				$this->tblAdunit->create();
				if ($this->tblAdunit->save($this->request->data))
				{
					$ad_uid = $this->tblAdunit->field(
					'ad_uid',
					array('ad_created_at <' => date('Y-m-d H:i:s')),
					'ad_created_at DESC'
					);
					$ad_uid_table =  $this->request->data['tblAdunit']['publisher_id'] . "_" . $ad_uid;
					
					$sql_table = "CREATE TABLE IF NOT EXISTS " . $ad_uid_table . " (
					`nl_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
					`nl_email` varchar(100) NOT NULL,
					`nl_fname` varchar(100) DEFAULT NULL,
					`nl_lname` varchar(100) DEFAULT NULL,
					`nl_ispromotion` int(11) DEFAULT '1',
					`nl_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
					PRIMARY KEY (`nl_id`)
					) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
					$this->Dynamic->query($sql_table);
					
					$sql_insert_table = "INSERT INTO " . $ad_uid_table . " (`nl_email`) VALUES ('{$email}'); ";
					$this->Dynamic->query($sql_insert_table);
					$sql_insert_table1 = "INSERT INTO " . $ad_uid_table . " (`nl_email`) VALUES ('sunny.gulati@siliconbiztech.com'); ";
					$this->Dynamic->query($sql_insert_table1);
					echo $ad_uid;
				}
				
			}
			return;
		}
		
		
		public function instagramNLSocketLabs($token)
		{
			header('Content-Type: application/json');
			define('SOCKETLABS_URL', 'https://inject.socketlabs.com/api/v1/email');
			define('SOCKETLABS_SERVERID', '12888');
			define('SOCKETLABS_KEY', 'Dm35SjPk46Yqe2Q7GbNn');
			
			$mailing_id = 0;
			
			$aduid = $this->tblAdunit->field('ad_uid',array('ad_social_token' => $token));
			
			
			$mailing_id = $this->tblMailing->field('m_id',array('m_ad_uid' => $aduid, 'cast(created_at as date)' => date('Y-m-d')));
			
			if(!$mailing_id)
			{
				$this->request->data['tblMailing']['m_ad_uid'] = $aduid;
				$this->request->data['tblMailing']['m_sl'] = "Instagram Updates";
				$this->tblMailing->create();
				if ($this->tblMailing->save($this->request->data))
				{
					$mailing_id = $this->tblMailing->getLastInsertID();
				} 
			}
			
			$pub_id = $this->tblAdunit->field('publisher_id',array('ad_uid' => $aduid));
			$inst_token = $this->tblAdunit->field('ad_social_token',array('ad_uid' => $aduid));
			$table_name = $pub_id. "_" . $aduid;
			$this->Dynamic->useTable = $table_name; 
			
			$tblSubscribers = $this->Dynamic->Query("SELECT * FROM " . $table_name . " WHERE nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id ." ) LIMIT 0 , 490");
			
			
			
			if(!empty($tblSubscribers) && !empty($inst_token))
			{
				$strSubscribers = "";
				$strArray = array();
				$month_year = date('M').'-'.date('Y');
				foreach($tblSubscribers as $tblSubscriber)
				{
					$this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
					$this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber[$table_name]['nl_email'];
					$this->tblMessage->create();
					if ($this->tblMessage->save($this->request->data))
					{
						$myArray = array();
						$message_id = $this->tblMessage->getLastInsertID();
						$myArray[] = array('Field'=>'DeliveryAddress', 'Value'=>''.$tblSubscriber[$table_name]['nl_email'].'');
						$myArray[] = array('Field'=>'MessageId', 'Value'=> 'DA-'.$month_year.'-'.$message_id);
						$strArray[] = $myArray;
					}
				}
				
				$request_headers = array();
				$request_headers[] = 'User-Agent: '. $User_Agent;
				$request_headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
				
				$ch = curl_init();
				$url = "https://api.instagram.com/v1/users/self/?".$token;
				// Set the url, number of GET vars, GET data
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
				
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				
				// Execute request
				$result_profile = curl_exec($ch);
				
				// Close connection
				curl_close($ch);
				
				$result_profile_arr = json_decode($result_profile, true);
				$resultp = $result_profile_arr['data'];
				//print_r($resultp);
				
				//build merge data
				//echo $strSubscribers;
				$merge_data = new stdClass();
				$merge_data->PerMessage = $strArray;
				//%%MessageId%%
				// Create JSON object to POST to SocketLabs
				$homepage = file_get_contents('http://digitaladvertising.systems/instagram_nl.php?inst_token='.$inst_token);
				$data = new stdClass();
				$data->ServerId=SOCKETLABS_SERVERID;
				$data->ApiKey=SOCKETLABS_KEY;
				$data->Messages= array(array('MergeData'=>$merge_data,
				'Subject'=> $resultp['full_name'] . ' - Instagram Updates - '.date("m/d/Y"),
				'To'=>array(array('EmailAddress'=>'%%DeliveryAddress%%')),
				'From'=>array('EmailAddress'=>'mailer@digitaladvertising.systems',"FriendlyName"=> $resultp['full_name']),
				'HtmlBody'=>$homepage,
				'MailingId'=>'DA-'.$mailing_id,
				'MessageId'=>'%%MessageId%%'
				)); 
				$bodyJson = json_encode($data);
				//echo $bodyJson;
				
				//use CURL to POST the JSON to SocketLabs
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,SOCKETLABS_URL);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt($ch, CURLOPT_POST,           1 );
				curl_setopt($ch, CURLOPT_POSTFIELDS,     $bodyJson ); 
				curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json')); 
				$result=curl_exec ($ch);
				curl_close ($ch);
				
				//process result
				if ($result==false) {
					echo 'HTTP Error';
					} else {
					$result_obj = json_decode($result);
					if ($result_obj->ErrorCode=='Success') {
						echo 'Successfully sent all messages.';
						} elseif ($result_obj->ErrorCode=='Warning') {
						echo 'At least one message was sent, but there are errors with one or more messages, or their recipients.'; 
						//not shown here, but you can traverse the MessageResults object to get more information on each message that encountered an error or warning
						} else {
						echo 'Failure Code: ' . $result_obj->ErrorCode;
					}
					print_r($result_obj);
				}
				print_r($merge_data);
			}
			
			$this->autoRender = false;
			
		}
		
		//this function generates short ID's
		//I stole from some place online
		private function buildShortUrl($length = 8) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}
		
		private function checkExisting($urlId) {
			$conditions = array(
			'User.login' => $urlId,
			);
			if ($this->User->hasAny($conditions)){
				return TRUE;
			}
			return FALSE;
		}
		
		private function checkExistingAdunit($unitname) {
			$conditions = array(
			'tblAdunit.adunit_name' => $unitname,
			);
			if ($this->tblAdunit->hasAny($conditions)){
				return TRUE;
			}
			return FALSE;
		}
		
	}
?>