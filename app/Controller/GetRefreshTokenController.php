<?php 
	
	class GetRefreshTokenController extends AppController {
		
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {
		
		Configure::write('debug', 1);
			$this->autoRender = false;
			
			  // form posted
        App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
      
        App::import('Vendor', 'src/ExampleUtils');
			
			
			try {
				// Get the client ID and secret from the auth.ini file. If you do not have a
				// client ID or secret, please create one of type "installed application" in
				// the Google API console: https://code.google.com/apis/console#access
				// and set it in the auth.ini file.
				$user = new DfpUser();
				$user->LogAll();
				//print_r($user);
				// Get the OAuth2 credential.
				$oauth2Info = $this->GetOAuth2Credential($user);
				// Enter the refresh token into your auth.ini file.
				print_r("Your refresh token is: %s\n\n", $oauth2Info['refresh_token']);
				print_r("In your auth.ini file, edit the refresh_token line to be:\n"
				. "refresh_token = \"%s\"\n", $oauth2Info['refresh_token']);
				} catch (OAuth2Exception $e) {
				ExampleUtils::CheckForOAuth2Errors($e);
				} catch (ValidationException $e) {
				ExampleUtils::CheckForOAuth2Errors($e);
				} catch (Exception $e) {
				print_r("An error has occurred: %s\n", $e->getMessage());
			}
			
		}
		
		/**
			* Gets an OAuth2 credential.
			* @param string $user the user that contains the client ID and secret
			* @return array the user's OAuth 2 credentials
		*/
		function GetOAuth2Credential($user) {
			
			// form posted
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
			App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201505/StatementBuilder');
			App::import('Vendor', 'src/ExampleUtils');
			
			$redirectUri = "http://digitaladvertising.systems/GetRefreshToken";
			$offline = TRUE;
			// Get the authorization URL for the OAuth2 token.
			// No redirect URL is being used since this is an installed application. A web
			// application would pass in a redirect URL back to the application,
			// ensuring it's one that has been configured in the API console.
			// Passing true for the second parameter ($offline) will provide us a refresh
			// token which can used be refresh the access token when it expires.
			$OAuth2Handler = $user->GetOAuth2Handler();
			$authorizationUrl = $OAuth2Handler->GetAuthorizationUrl(
			$user->GetOAuth2Info(), $redirectUri, $offline);
			// In a web application you would redirect the user to the authorization URL
			// and after approving the token they would be redirected back to the
			// redirect URL, with the URL parameter "code" added. For desktop
			// or server applications, spawn a browser to the URL and then have the user
			// enter the authorization code that is displayed.
			printf("Log in to your DFP account and open the following URL:\n%s\n\n",
			$authorizationUrl);
			print "After approving the token enter the authorization code here: ";
			//$stdin = fopen('php://stdin', 'r');
			//$code = trim(fgets($stdin));
                  	$code = '4/wDwibyqml3fydQKW2HYlW1NEK9fUMKFK9ijU_d86dpk#';
			//fclose($stdin);
			print "\n";
			// Get the access token using the authorization code. Ensure you use the same
			// redirect URL used when requesting authorization.
			$user->SetOAuth2Info(
			$OAuth2Handler->GetAccessToken(
            $user->GetOAuth2Info(), $code, $redirectUri));
			// The access token expires but the refresh token obtained for offline use
			// doesn't, and should be stored for later use.
			print_r($user->GetOAuth2Info());
			return $user->GetOAuth2Info();
		}
		
	}
	
	
