<?php
	
	class CouchDBController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblLineItem');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {
			
			//$couch_dsn = "http://localhost:5984/";
			### AUTHENTICATED DSN
			$couch_dsn = "http://admin:C0uchADm!n@ec2-54-161-239-191.compute-1.amazonaws.com/";
			
			$couch_db = "digitaladvertising";
			
			
			App::import('Vendor', 'couchdb/couch');
			App::import('Vendor', 'couchdb/couchClient');
			App::import('Vendor', 'couchdb/couchDocument');
			
			$client = new couchClient($couch_dsn,$couch_db);
			
			
			/*
				// Update Signed Offers SIgned Update
				try {
				$doc = $client->getDoc('testing');
				} catch (Exception $e) {
				echo "ERROR: ".$e->getMessage()." (".$e->getCode().")<br>\n";
				}
				
				$offer = array();
				$offer["order_name"] = "QualityHealth May IO 3 2016";
				$offer["li_name"] = "Vision Care Coreg";
				$offer["li_id"] = "165";
				$offer["offer_taken_at"] = date("Y-m-d H:i:s"); 
				$doc->OffersRegistered[] = $offer;
				try {
				$response = $client->storeDoc($doc);
				} catch (Exception $e) {
				echo "Something weird happened: ".$e->getMessage()." (errcode=".$e->getCode().")\n";
				exit(1);
				}
			*/
			
			
			
			// Update Shown Offers
			
			try {
				$doc = $client->getDoc('testing');
				} catch (Exception $e) {
				echo "ERROR: ".$e->getMessage()." (".$e->getCode().")<br>\n";
			}
			
			$offer = array();
			$offer["order_name"] = "QualityHealth May IO 3 2016";
			$offer["li_name"] = "Vision Care Coreg";
			$offer["li_id"] = "165";
			$offer["offer_shown_at"] = date("Y-m-d H:i:s");
			$doc->OffersShown[] = $offer;
			try {
				$response = $client->storeDoc($doc);
				} catch (Exception $e) {
				echo "Something weird happened: ".$e->getMessage()." (errcode=".$e->getCode().")\n";
				exit(1);
			}
			
			
			/*
				$view_fn="function(doc) { if (doc.Type == 'EmailProfile')  emit(null, doc); }";
				$design_doc = new stdClass();
				$design_doc->_id = '_design/GetEmailProfiles';
				$design_doc->language = 'javascript';
				$design_doc->views = array ( 'all'=> array ('map' => $view_fn ) );
			$client->storeDoc($design_doc);*/
			
			
			
			/*
				$campaign_id = 7; // Campaign id. Change accordingly 
				$publisher = 'Test Publisher';
				
				$md5email = md5('belomorov@yahoo.com');//Change accordingly . 
				
				$data['campaign_id']=$campaign_id;
				$data['publisher']=$publisher;
				$data['md5email']=$md5email;
				
				
				$headers = array(
				'Accept: application/json',
				'SMART_SESSION_ID: 7ccd0eb975032cad8c41dd80587f515daaa',
				'SMART_TOKEN: 564cdcae13d08aa',
				'SMART_API_REQUEST_TYPE: REST',
				);
				// call the API 
				$response = $this->createApiCall('http://kmadigital.com/api/index.php/api/campaign/', 'POST', $headers,$data);
				$response = json_decode($response, true);
				if ($response['status'] == 'SUCCESS')
				{
				$doc = new stdClass();
				$doc->_id = MD5("chocodigital@gmail.com");
				$doc->Type = "EmailProfile";
				$doc->Email = "chocodigital@gmail.com";
				$data = (object)$response['data'];
				foreach ($data as $key => $value)
				{
				$doc->$key = $value;
				}
				}
				try {
				$response = $client->storeDoc($doc);
				} catch (Exception $e) {
				echo "Something weird happened: ".$e->getMessage()." (errcode=".$e->getCode().")\n";
				exit(1);
				}
			*/
			//$doc = $client->getDoc('some_doc');
			//print_r($doc);
			$this->autoRender = false;
		}
		
		
		public static function createApiCall($url, $method, $headers, $data = array())
        {
            if ($method == 'PUT')
            {
                $headers[] = 'X-HTTP-Method-Override: PUT';
			}
			
            $headers = self::mergeArray(array("Cache-Control: no-cache"), $headers);
            $handle = curl_init();
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($handle, CURLOPT_FRESH_CONNECT, true);
			
            switch($method)
            {
                case 'GET':
				break;
                case 'POST':
				curl_setopt($handle, CURLOPT_POST, true);
				curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));
				break;
                case 'PUT':
				curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));
				break;
                case 'DELETE':
				curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			}
            $response = curl_exec($handle);
            return $response;
		}
        
        
        public static function mergeArray($a,$b)
		{
			$args=func_get_args();
			$res=array_shift($args);
			while(!empty($args))
			{
				$next=array_shift($args);
				foreach($next as $k => $v)
				{
					if(is_integer($k))
					isset($res[$k]) ? $res[]=$v : $res[$k]=$v;
					elseif(is_array($v) && isset($res[$k]) && is_array($res[$k]))
					$res[$k]=self::mergeArray($res[$k],$v);
					else
					$res[$k]=$v;
				}
			}
			return $res;
		}
		
		public function userProfile(){
			$couch_dsn = "http://admin:C0uchADm!n@ec2-54-161-239-191.compute-1.amazonaws.com/";
			$couch_db = "digitaladvertising";
			App::import('Vendor', 'couchdb/couch');
			App::import('Vendor', 'couchdb/couchClient');
			App::import('Vendor', 'couchdb/couchDocument');
			$client = new couchClient($couch_dsn,$couch_db);
			$email = $this->request->param('email');
			$campaign_id = 7; // Campaign id. Change accordingly 
			$publisher = 'Test Publisher';
			$response_return = "";
			$md5email = md5($email);
			try {
				$response_return = $client->getDoc($md5email);
				
				} catch (Exception $e) {
				$data['campaign_id']=$campaign_id;
				$data['publisher']=$publisher;
				$data['md5email']=$md5email;
				
				
				$headers = array(
				'Accept: application/json',
				'SMART_SESSION_ID: 7ccd0eb975032cad8c41dd80587f515daaa',
				'SMART_TOKEN: 564cdcae13d08aa',
				'SMART_API_REQUEST_TYPE: REST',
				);
				// call the API 
				$response = $this->createApiCall('http://kmadigital.com/api/index.php/api/campaign/', 'POST', $headers,$data);
				$response = json_decode($response, true);
				if ($response['status'] == 'SUCCESS')
				{
					$doc = new stdClass();
					$doc->_id = $md5email;
					$doc->Type = "EmailProfile";
					$doc->Email = $email;
					$data = (object)$response['data'];
					foreach ($data as $key => $value)
					{
						$doc->$key = $value;
					}
					try {
						$resp = $client->storeDoc($doc);
						$response_return = $client->getDoc($md5email);
						} catch (Exception $e) {
						echo "Something weird happened: ".$e->getMessage()." (errcode=".$e->getCode().")\n";
						exit(1);
					}
					} else {
					$response = $client->copyDoc('blankUserProfile',$md5email);
					$getcopy = $client->getDoc($md5email);
					$getcopy->Email = $email;
					$responseget = $client->storeDoc($getcopy);
					$response_return = $client->getDoc($md5email);
				}
			}
			return $response_return;
		}
		
		public function saveViewOffers()
		{
			$couch_dsn = "http://admin:C0uchADm!n@ec2-54-161-239-191.compute-1.amazonaws.com/";
			$couch_db = "digitaladvertising";
			App::import('Vendor', 'couchdb/couch');
			App::import('Vendor', 'couchdb/couchClient');
			App::import('Vendor', 'couchdb/couchDocument');
			$client = new couchClient($couch_dsn,$couch_db);
			$email = $this->request->param('email');
			$md5email = md5($email);
			try {
				$doc = $client->getDoc($md5email);
				$offer = array();
				$offer["order_name"] = $this->request->param('order_name');
				$offer["li_name"] = $this->request->param('li_name');
				$offer["ad_unit_name"] =  $this->request->param('ad_name');;
				$offer["li_category"] = $this->request->param('category_name');
				$offer["li_id"] = $this->request->param('li_id');
				$offer["mapped_ad_uid"] = $this->request->param('ma_uid');
				$offer["offer_shown_at"] = date("Y-m-d H:i:s"); 
				$doc->OffersShown[] = $offer;
				try {
					$response = $client->storeDoc($doc);
					} catch (Exception $e) {
					echo "Something weird happened: ".$e->getMessage()." (errcode=".$e->getCode().")\n";
					exit(1);
				}
				} catch (Exception $e) {
				echo "ERROR: ".$e->getMessage()." (".$e->getCode().")<br>\n";
			}
		}
		
		
		public function saveSignedOffers()
		{
			$couch_dsn = "http://admin:C0uchADm!n@ec2-54-161-239-191.compute-1.amazonaws.com/";
			$couch_db = "digitaladvertising";
			App::import('Vendor', 'couchdb/couch');
			App::import('Vendor', 'couchdb/couchClient');
			App::import('Vendor', 'couchdb/couchDocument');
			$client = new couchClient($couch_dsn,$couch_db);
			$email = $this->request->param('email');
			$md5email = md5($email);
			try {
				$doc = $client->getDoc($md5email);
				$offer = array();
				$offer["order_name"] = $this->request->param('order_name');
				$offer["li_name"] = $this->request->param('li_name');
				$offer["ad_unit_name"] =  $this->request->param('ad_name');;
				$offer["li_category"] = $this->request->param('category_name');
				$offer["li_id"] = $this->request->param('li_id');
				$offer["mapped_ad_uid"] = $this->request->param('ma_uid');
				$offer["offer_taken_at"] = date("Y-m-d H:i:s"); 
				$doc->OffersRegistered[] = $offer;
				try {
					$response = $client->storeDoc($doc);
					} catch (Exception $e) {
					echo "Something weird happened: ".$e->getMessage()." (errcode=".$e->getCode().")\n";
					exit(1);
				}
				} catch (Exception $e) {
				echo "ERROR: ".$e->getMessage()." (".$e->getCode().")<br>\n";
			}
		}
		
		
		public function viewKMAProfile() {
			
			//$couch_dsn = "http://localhost:5984/";
			### AUTHENTICATED DSN
			$couch_dsn = "http://admin:C0uchADm!n@ec2-54-161-239-191.compute-1.amazonaws.com/";
			
			$couch_db = "digitaladvertising";
			
			App::import('Vendor', 'couchdb/couch');
			App::import('Vendor', 'couchdb/couchClient');
			App::import('Vendor', 'couchdb/couchDocument');
			
			$client = new couchClient($couch_dsn,$couch_db);
			
			$arr = $this->request->param('emailArray');
			
			$view = $client->asArray()->include_docs(true)->keys($arr)->getAllDocs();
			//print_r($view);
			//$view = $client->asArray()->include_docs(false)->keys($arr)->getView('GetEmailProfiles','kma_deals');
			//$view = $client->asArray()->include_docs(false)->keys($arr)->getView('GetEmailProfiles','kma_profile');
			$i = 0;
			$items = $view['rows'];
			
			foreach($items as $key => $value){
				if(!isset($value['doc']) || (isset($value['doc']['match_kma']) && $value['doc']['match_kma'] != 'Y')) {
					unset($items[$key]);
				}
			}
			return $items;
			$this->autoRender = false;
		}
	}
?>	