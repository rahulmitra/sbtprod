<?php

class SendgridController extends AppController {

    var $uses = array('Authake.Group', 'Authake.User', 'Authake.tblAdunit', 'Authake.tblNlsubscribers', 'Dynamic', 'Authake.tblMailing', 'Authake.AutoResponder', 'Authake.tblProfile', 'Authake.tblMappedAdunit', 'Authake.tblMessage', 'UserLevelEmailPerformance');
    var $components = array('RequestHandler', 'Authake.Filter', 'Session'); // var $layout = 'authake';
    var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc')); //var $scaffold;

    public function beforeFilter() {
        parent::beforeFilter();

        // Change layout for Ajax requests
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
    }

    public function index() {
        
    }

    public function autorespnder($mailing_id, $email_id) {
        App::import('Vendor', 'src/html2text');
        App::import('Vendor', array('file' => 'autoload'));

        $m_li_crtid = $this->tblMailing->field(
                'm_li_crtid', array('m_id' => $mailing_id)
        );

        $options_ar['conditions'] = array('AutoResponder.id' => $m_li_crtid);
        $autoResponder = $this->AutoResponder->find('first', $options_ar);

        $sendgrid = new SendGrid('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');
        $strArray = array();
        $this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
        $this->request->data['tblMessage']['msg_email_id'] = $email_id;
        $this->tblMessage->create();
        if ($this->tblMessage->save($this->request->data)) {
            $message_id = $this->tblMessage->getLastInsertID();
            $strArray[] = $email_id;
        }


        $sponsoredBody = $autoResponder['AutoResponder']['emailbody'] . "<br/><p>" . $autoResponder['AutoResponder']['footer'] . "</p>";
        $h2t = & new html2text($sponsoredBody);
        $textVersion = $h2t->get_text();

        $email = new SendGrid\Email();
        $email
                ->setFrom('' . $autoResponder['AutoResponder']['from_email'] . '')
                ->setFromName('' . $autoResponder['AutoResponder']['from_name'] . '')
                ->setSubject('' . $autoResponder['AutoResponder']['subjectline'] . '')
                ->setSmtpapiTos($strArray)
                ->setText($textVersion)
                ->setHtml($sponsoredBody)
                ->addUniqueArg("MailingID", $mailing_id)
        ;

        try {
            $res = $sendgrid->send($email);
            var_dump($res);
        } catch (\SendGrid\Exception $e) {
            echo $e->getCode();
            foreach ($e->getErrors() as $er) {
                echo $er;
            }
        }
        $this->autoRender = false;
    }

    public function sendgridEmail() {

        App::import('Vendor', 'src/html2text');
        App::import('Vendor', array('file' => 'autoload'));

        $sendgrid = new SendGrid('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');



        $email = new SendGrid\Email();
        $email
                ->addTo('mohd.kamil@siliconbiztech.com')
                ->setFrom('sunny.gulati@siliconbiztech.com')
                ->setSubject('Subject goes here')
                ->setText('Hello World!')
                ->setHtml('<strong>Hello World! <a href="http://www.google.com">www.google.com</a></strong>')
        ;

        try {
            $res = $sendgrid->send($email);
            var_dump($res);
        } catch (\SendGrid\Exception $e) {
            echo $e->getCode();
            foreach ($e->getErrors() as $er) {
                echo $er;
            }
        }
        $this->autoRender = false;
    }

    public function DedicatedEmail() {
        Configure::write('debug',2);
        header('Content-Type: application/json');
        App::import('Vendor', 'src/html2text');
        App::import('Vendor', array('file' => 'autoload'));
        
        $mailing_id = $this->request->param('mailing_id');
        $esp_id = $this->request->param('esp_id');
        $SubjectLine = ''; //$this->request->param('SubjectLine');
        $physical_address = "";

        $raa = rand(pow(10, 4), pow(10, 3) - 1);

        $m_ad_uid = $this->tblMailing->field(
                'm_ad_uid', array('m_id' => $mailing_id)
        );

        $options_ad['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tma',
                'type' => 'INNER',
                'conditions' => array(
                    'tma.ma_ad_id = tblAdunit.ad_dfp_id'
                )),
            array('table' => 'tbl_esp_settings',
                'alias' => 'tes',
                'type' => 'INNER',
                'conditions' => array(
                    'tes.es_ad_id = tblAdunit.adunit_id'
                )),
            array('table' => 'tbl_email_service_providers',
                'alias' => 'tesp',
                'type' => 'INNER',
                'conditions' => array(
                    'tes.es_esp_id = tesp.esp_id'
                ))
        );

        $options_ad['conditions'] = array('tma.ma_uid' => $m_ad_uid, 'tes.es_esp_id' => $esp_id);
        $options_ad['fields'] = array('tblAdunit.ad_uid', 'tblAdunit.adunit_id', 'tblAdunit.publisher_id', 'tblAdunit.automatic_address_footer', 'tblAdunit.mailling_address_footer', 'tblAdunit.sender_email', 'tes.es_from_email', 'tes.es_private_key', 'tes.es_site_domain', 'tes.es_tracking_domain', 'tes.es_limit', 'tesp.esp_mail_tag', 'tesp.esp_unsubscribe_tag', 'tma.ma_ad_id', 'tma.ma_l_id');
        $adunitSettings = $this->tblAdunit->find('first', $options_ad);
        //print_r("<pre>");print_r($adunitSettings);print_r("</pre>");exit();
        $aduid = $adunitSettings['tblAdunit']['ad_uid'];

        $pub_id = $adunitSettings['tblAdunit']['publisher_id'];

        $mail_tag = $adunitSettings['tesp']['esp_mail_tag'];

        $unsub_tag = $adunitSettings['tesp']['esp_unsubscribe_tag'];

        $aduidDFP = $adunitSettings['tma']['ma_ad_id'];

        $ma_l_id = $adunitSettings['tma']['ma_l_id'];

        $fromEmail = $adunitSettings['tes']['es_from_email'];
        $fromName = $adunitSettings['tblAdunit']['sender_email'];
        $SENDGRID_API_KEY = $adunitSettings['tes']['es_private_key'];
        $sendgrid = new SendGrid($SENDGRID_API_KEY);
        $email = new SendGrid\Email();
        $siteName = $adunitSettings['tblAdunit']['sender_email'];
        $domain = $adunitSettings['tes']['es_site_domain'];
        $isTest = 0;
        $limit = $adunitSettings['tes']['es_limit'];
        $tracking_domain = $adunitSettings['tes']['es_tracking_domain'];
        $automatic_address_footer = $adunitSettings['tblAdunit']['automatic_address_footer'];
        $physical_address = $adunitSettings['tblAdunit']['mailling_address_footer'];

        $table_name = $pub_id . "_" . $aduid;
        $this->Dynamic->useTable = '241_d780e550493611e6b91e123aa499f9d5';//$table_name;

        $m_li_crtid = $this->tblMailing->field(
                'm_li_crtid', array('m_id' => $mailing_id)
        );

        $user_id = $this->User->field(
                'tblProfileID', array('id' => $pub_id)
        );

        $tracksite_id = $this->tblProfile->field(
                'tracksite_id', array('profileID' => $user_id)
        );


        echo $table_name = $pub_id . "_" . $aduid;
        $this->Dynamic->useTable = '241_d780e550493611e6b91e123aa499f9d5';//$table_name;
        //'241_e1dc4db46f7b11e6b91e123aa499f9d5';//
        if ($isTest == "1") {
            $tblSubscribers = array();
            $tblSubscribers[0]['tbl_email_segments']['es_email'] = "mohsin.kabir@siliconbiztech.com";
            $tblSubscribers[0]['tbl_email_segments']['es_fname'] = "Mohsin";
            $tblSubscribers[0]['tbl_email_segments']['es_lname'] = "Kabir";
            
//            $tblSubscribers[1]['tbl_email_segments']['es_email'] = "mohsintech@gmail.com";
//            $tblSubscribers[1]['tbl_email_segments']['es_fname'] = "Mohsin";
//            $tblSubscribers[1]['tbl_email_segments']['es_lname'] = "Kabir";
//            
//            $tblSubscribers[2]['tbl_email_segments']['es_email'] = "sandeep.bansal@siliconbiztech.com";
//            $tblSubscribers[2]['tbl_email_segments']['es_fname'] = "Sandeep";
//            $tblSubscribers[2]['tbl_email_segments']['es_lname'] = "Bansal";
//            
//            $tblSubscribers[3]['tbl_email_segments']['es_email'] = "ankita.bajaj@siliconbiztech.com";
//            $tblSubscribers[3]['tbl_email_segments']['es_fname'] = "Ankita";
//            $tblSubscribers[3]['tbl_email_segments']['es_lname'] = "Bajaj";
            
            $tblSubscribers[4]['tbl_email_segments']['es_email'] = "sunny.gulati@siliconbiztech.com";
            $tblSubscribers[4]['tbl_email_segments']['es_fname'] = "Sunny";
            $tblSubscribers[4]['tbl_email_segments']['es_lname'] = "Gulati";
            //$tblSubscribers = $this->Dynamic->Query("SELECT * FROM tbl_email_segments WHERE es_ml_id=" . $mailing_id ." and es_istest=1 and es_esp_id=5 LIMIT 0 , 3");
        } else {
            echo "SELECT * FROM tbl_email_segments WHERE es_ml_id=" . $mailing_id . " and es_esp_id=5 and es_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id . " ) LIMIT 0 , 1";
            $tblSubscribers = $this->Dynamic->query("SELECT * FROM tbl_email_segments WHERE es_ml_id=" . $mailing_id . " and es_esp_id=5 and es_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id . " ) LIMIT 0 ,500");
        }
        print_r($tblSubscribers);

        //$finalurl = $url . "?idsite=" . $tracksite_id . "&cid=" . $m_li_crtid . "&mid=" . $mailing_id . "&uid=" . $m_ad_uid . "&redirect=" . urlencode($redirect_uri); 

        $ulliItems = '';
        $fullhtml = '';
        $firstTitle = '';
        $i = 0;

        if (!empty($tblSubscribers)) {
            $strSubscribers = "";
            $strArray = array();
            $month_year = date('M') . '-' . date('Y');
            foreach ($tblSubscribers as $tblSubscriber) {
                $this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
                $this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber['tbl_email_segments']['es_email'];
                $this->tblMessage->create();
                if ($this->tblMessage->save($this->request->data)) {
                    $message_id = $this->tblMessage->getLastInsertID();
                    //$strArray[] = array('email'=>''.$tblSubscriber['tbl_email_segments']['es_email'].'', 'name'=>$tblSubscriber['tbl_email_segments']['es_fname'], 'last'=>preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $tblSubscriber['tbl_email_segments']['es_lname']));
                    $strArray[] = $tblSubscriber['tbl_email_segments']['es_email'];
                    $strName[] = $tblSubscriber['tbl_email_segments']['es_fname'];
                }
            }
            //print_r($strArray);

            $options2['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'Active', 'tbc.creatives_type' => '2', 'tbc.cr_id' => $m_li_crtid);
            $options2['joins'] = array(
                array('table' => 'tbl_line_items',
                    'alias' => 'tli',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tli.li_id = tblMappedAdunit.ma_l_id')),
                array('table' => 'tbl_creatives',
                    'alias' => 'tbc',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tbc.cr_lid = tli.li_dfp_id'
            )));
            $options2['fields'] = array('tbc.*', 'tblMappedAdunit.*');
            $sponsoredCreative = $this->tblMappedAdunit->find('first', $options2);
            //print_r($sponsoredCreative);
            $SubjectLine = strip_tags($sponsoredCreative['tbc']['cr_header']);

            //print_r($SubjectLine);
            // print_r($sponsoredCreative);
            $sponsoredRedirect = $sponsoredCreative['tbc']['cr_redirect_url'];
            $sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=" . $tracksite_id . "&rec=1&c_n=creative_" . $sponsoredCreative['tbc']['cr_id'] . "-mailing_" . $mailing_id . "&c_p=" . $sponsoredCreative['tblMappedAdunit']['ma_uid'] . "&send_image=0";
            $message = $sponsoredCreative['tbc']['cr_body'];
            //$message =$tblMailing['tblCreative']['cr_body'];
            $URLs = array();
            $content = explode("\n", $message);
            for ($i = 0; count($content) > $i; $i++) {

                if (preg_match('/href=/', $content[$i])) {
                    list($Gone, $Keep) = explode("href=\"", trim($content[$i]));

                    list($Keep, $Gone) = explode("\"", $Keep);

                    $decodeurl = urlencode($Keep);
                    $impressionurl = "http://tracking.digitaladvertising.systems/leadTrack.php?idsite=" . $tracksite_id . "&rec=1&c_n=creative_" . $sponsoredCreative['tbc']['cr_id'] . "-mailing_" . $mailing_id . "&c_p=" .$sponsoredCreative['tblMappedAdunit']['ma_uid']  . "&c_i=lead&email=|EMAIL|&redirect=" . $decodeurl;
                    $message =  strtr($message, array("$Keep" => $impressionurl));
                }
            }
            $message.="<br /><br /> " . $automatic_address_footer . "<br />" . $physical_address . "<br /><br /><img src='http://tracking.digitaladvertising.systems/piwik.php?idsite=" . $tracksite_id . "&rec=1&c_n=" . $ma_l_id . "&c_p=" . $m_ad_uid . "-" . $mailing_id . "&send_image=0' width='1' height='1'>";
            $message.="<br /><br /> Please click here to <a href='" . $unsub_tag. "'>Unsubscribe</a>";
            $h2t = & new html2text($message);
            $textVersion = $h2t->get_text();

            try {

                $email
                        ->setFrom('' . $fromEmail . '')
                        ->setFromName($fromName)
                        ->setSubject('' . $SubjectLine . '')
                        ->setSmtpapiTos($strArray)
                        ->setText($textVersion)
                        ->setHtml($message)
                        ->addUniqueArg("MailingID", $mailing_id)
                        ->setSubstitutions(array
                            (
                            '%%first_name%%' => $strName,
                                )
                        )
                ;

                $res = $sendgrid->send($email);
                var_dump($res);
            } catch (\SendGrid\Exception $e) {
                $emails = array();
                foreach ($tblSubscribers as $tblSubscriber) {
                    $emails[] = $tblSubscriber['tbl_email_segments']['es_email'];
                }
                $condition = array('tblMessage.msg_email_id in' => $emails, 'msg_m_id' => $mailing_id);
                //$this->tblMessage->deleteAll($condition,false);
                echo $e->getCode();
                foreach ($e->getErrors() as $er) {
                    echo $er;
                }
            }
        }

        $this->autoRender = false;
    }

    public function TriggerSeries() {
        header('Content-Type: application/json');
        App::import('Vendor', 'src/html2text');
        App::import('Vendor', array('file' => 'autoload'));

        $mailing_id = $this->request->param('mailing_id');
        $esp_id = $this->request->param('esp_id');
        $SubjectLine = $this->request->param('SubjectLine');
        $physical_address = "";
        $tblSubscribers = $this->request->param('tblSubscribers');
        $fromName = "Rory Olson";

        $raa = rand(pow(10, 4), pow(10, 3) - 1);

        $m_ad_uid = $this->tblMailing->field(
                'm_ad_uid', array('m_id' => $mailing_id)
        );

        $options_ad['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tma',
                'type' => 'INNER',
                'conditions' => array(
                    'tma.ma_ad_id = tblAdunit.ad_dfp_id'
                )),
            array('table' => 'tbl_esp_settings',
                'alias' => 'tes',
                'type' => 'INNER',
                'conditions' => array(
                    'tes.es_ad_id = tblAdunit.adunit_id'
                )),
            array('table' => 'tbl_email_service_providers',
                'alias' => 'tesp',
                'type' => 'INNER',
                'conditions' => array(
                    'tes.es_esp_id = tesp.esp_id'
                ))
        );

        $options_ad['conditions'] = array('tma.ma_uid' => $m_ad_uid, 'tes.es_esp_id' => $esp_id);
        $options_ad['fields'] = array('tblAdunit.ad_uid', 'tblAdunit.adunit_id', 'tblAdunit.publisher_id', 'tblAdunit.sender_email', 'tes.es_from_email', 'tes.es_private_key', 'tes.es_site_domain', 'tes.es_tracking_domain', 'tes.es_limit', 'tesp.esp_mail_tag', 'tesp.esp_unsubscribe_tag', 'tma.ma_ad_id', 'tma.ma_l_id');
        $adunitSettings = $this->tblAdunit->find('first', $options_ad);

        $aduid = $adunitSettings['tblAdunit']['ad_uid'];

        $pub_id = $adunitSettings['tblAdunit']['publisher_id'];

        $mail_tag = $adunitSettings['tesp']['esp_mail_tag'];

        $unsub_tag = $adunitSettings['tesp']['esp_unsubscribe_tag'];

        $aduidDFP = $adunitSettings['tma']['ma_ad_id'];

        $ma_l_id = $adunitSettings['tma']['ma_l_id'];

        $fromEmail = $adunitSettings['tes']['es_from_email'];
        $SENDGRID_API_KEY = $adunitSettings['tes']['es_private_key'];
        $sendgrid = new SendGrid($SENDGRID_API_KEY);

        //$sendgrid = new SendGrid('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');
        $email = new SendGrid\Email();
        $siteName = $adunitSettings['tblAdunit']['sender_email'];
        $domain = $adunitSettings['tes']['es_site_domain'];
        $isTest = 1;
        $limit = $adunitSettings['tes']['es_limit'];
        $tracking_domain = $adunitSettings['tes']['es_tracking_domain'];


        $table_name = $pub_id . "_" . $aduid;
        $this->Dynamic->useTable = $table_name;

        $m_li_crtid = $this->tblMailing->field(
                'm_li_crtid', array('m_id' => $mailing_id)
        );

        $user_id = $this->User->field(
                'tblProfileID', array('id' => $pub_id)
        );

        $tracksite_id = $this->tblProfile->field(
                'tracksite_id', array('profileID' => $user_id)
        );



        $table_name = $pub_id . "_" . $aduid;
        $this->Dynamic->useTable = $table_name;



        //$finalurl = $url . "?idsite=" . $tracksite_id . "&cid=" . $m_li_crtid . "&mid=" . $mailing_id . "&uid=" . $m_ad_uid . "&redirect=" . urlencode($redirect_uri); 

        $ulliItems = '';
        $fullhtml = '';
        $firstTitle = '';
        $i = 0;

        if (!empty($tblSubscribers)) {
            $strSubscribers = "";
            $strArray = array();
            $month_year = date('M') . '-' . date('Y');
            foreach ($tblSubscribers as $tblSubscriber) {
                $this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
                $this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber[$table_name]['nl_email'];
                $this->tblMessage->create();
                if ($this->tblMessage->save($this->request->data)) {
                    $message_id = $this->tblMessage->getLastInsertID();
                    //$strArray[] = array('email'=>''.$tblSubscriber['tbl_email_segments']['es_email'].'', 'name'=>$tblSubscriber['tbl_email_segments']['es_fname'], 'last'=>preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $tblSubscriber['tbl_email_segments']['es_lname']));
                    $strArray[] = $tblSubscriber[$table_name]['nl_email'];
                    $strName[] = $tblSubscriber[$table_name]['nl_fname'];
                }
            }
            //print_r($strArray);

            $options2['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'Active', 'tbc.cr_id' => $m_li_crtid);
            $options2['joins'] = array(
                array('table' => 'tbl_line_items',
                    'alias' => 'tli',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tli.li_id = tblMappedAdunit.ma_l_id')),
                array('table' => 'tbl_creatives',
                    'alias' => 'tbc',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tbc.cr_lid = tli.li_dfp_id'
            )));
            $options2['fields'] = array('tbc.*', 'tblMappedAdunit.*');
            $sponsoredCreative = $this->tblMappedAdunit->find('first', $options2);
            //print_r($sponsoredCreative);
            $sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
            $sponsoredRedirect = $sponsoredCreative['tbc']['cr_redirect_url'];
            $sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=" . $tracksite_id . "&rec=1&c_n=creative_" . $sponsoredCreative['tbc']['cr_id'] . "-mailing_" . $mailing_id . "&c_p=" . $sponsoredCreative['tblMappedAdunit']['ma_uid'] . "&send_image=0";
            $sponsoredBody = $sponsoredCreative['tbc']['cr_body'] . "<br /><br /> " . $physical_address . "<br /><br /><img src='http://tracking.digitaladvertising.systems/piwik.php?idsite=" . $tracksite_id . "&rec=1&c_n=" . $ma_l_id . "&c_p=" . $m_ad_uid . "-" . $mailing_id . "&send_image=0' width='1' height='1'>";

            $h2t = & new html2text($sponsoredBody);
            $textVersion = $h2t->get_text();

            try {

                $email
                        ->setFrom('' . $fromEmail . '')
                        ->setFromName($fromName)
                        ->setReplyTo('' . $fromEmail . '')
                        ->setSubject('' . $SubjectLine . '')
                        ->setSmtpapiTos($strArray)
                        ->setText($textVersion)
                        ->setHtml($sponsoredBody)
                        ->addUniqueArg("MailingID", $mailing_id)
                        ->setSubstitutions(array
                            (
                            '%%first_name%%' => $strName,
                                )
                        )
                ;
                //print_r($email);
                $res = $sendgrid->send($email);
                //var_dump($res);
            } catch (\SendGrid\Exception $e) {
                echo $e->getCode();
                foreach ($e->getErrors() as $er) {
                    echo $er;
                }
            }
        }

        $this->autoRender = false;
    }

    public function webhook() {
        $data = file_get_contents("php://input");
        $events = json_decode($data, true);

        foreach ($events as $event) {
            if (empty($event['MailingID'])) {
                
            } else {
                $strResp = serialize($event);
                //mail("sunny.gulati@siliconbiztech.com","sendgrid23",$strResp);
                switch ($event['event']) {
                    case 'processed' :
                        //mail("sunny.gulati@siliconbiztech.com","processed",$strResp);
                        break;

                    case 'deferred' :
                        //mail("sunny.gulati@siliconbiztech.com","deferred",$strResp);
                        break;

                    case 'delivered' :
                        $conditions = array(
                            'UserLevelEmailPerformance.EmailAddress' => $event['email'],
                            'UserLevelEmailPerformance.ListId' => $event['MailingID']
                        );
                        if ($this->UserLevelEmailPerformance->hasAny($conditions)) {
                            //do something
                            $this->UserLevelEmailPerformance->updateAll(
                                    array('UserLevelEmailPerformance.Sent' => '1', 'UserLevelEmailPerformance.MailingDate' => "'" . date("Y-m-d H:i:s", $event['timestamp']) . "'"), array('UserLevelEmailPerformance.ListId' => $event['MailingID']
                                , 'UserLevelEmailPerformance.EmailAddress' => $event['email'])
                            );
                        } else {
                            $this->request->data['UserLevelEmailPerformance']['ListId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['MessageId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $event['sg_message_id'];
                            $this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $event['email'];
                            $this->request->data['UserLevelEmailPerformance']['MailingDate'] = date("Y-m-d H:i:s", $event['timestamp']);
                            $this->request->data['UserLevelEmailPerformance']['Sent'] = 1;
                            $this->request->data['UserLevelEmailPerformance']['esp_id'] = 5;
                            $this->UserLevelEmailPerformance->create();
                            $this->UserLevelEmailPerformance->save($this->request->data);
                        }
                        break;

                    case 'open' :
                        $conditions = array(
                            'UserLevelEmailPerformance.EmailAddress' => $event['email'],
                            'UserLevelEmailPerformance.ListId' => $event['MailingID']
                        );
                        if ($this->UserLevelEmailPerformance->hasAny($conditions)) {
                            $this->UserLevelEmailPerformance->incrementOpenCount($event['MailingID'], $event['email'], $DateTime);
                        } else {
                            $this->request->data['UserLevelEmailPerformance']['ListId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['MessageId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $event['sg_message_id'];
                            $this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $event['email'];
                            $this->request->data['UserLevelEmailPerformance']['OpenedAt'] = date("Y-m-d H:i:s", $event['timestamp']);
                            $this->request->data['UserLevelEmailPerformance']['localIp'] = $event['ip'];
                            $this->request->data['UserLevelEmailPerformance']['Opened'] = 1;
                            $this->request->data['UserLevelEmailPerformance']['esp_id'] = 5;
                            $this->UserLevelEmailPerformance->create();
                            $this->UserLevelEmailPerformance->save($this->request->data);
                        }
                        break;

                    case 'click' :
                        $conditions = array(
                            'UserLevelEmailPerformance.EmailAddress' => $event['email'],
                            'UserLevelEmailPerformance.ListId' => $event['MailingID']
                        );
                        if ($this->UserLevelEmailPerformance->hasAny($conditions)) {
                            $this->UserLevelEmailPerformance->incrementClickCount($event['MailingID'], $event['email'], $event['timestamp']);
                        } else {
                            $this->request->data['UserLevelEmailPerformance']['ListId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['MessageId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $event['sg_message_id'];
                            $this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $event['email'];
                            $this->request->data['UserLevelEmailPerformance']['ClickedAt'] = date("Y-m-d H:i:s", $event['timestamp']);
                            $this->request->data['UserLevelEmailPerformance']['localIp'] = $event['ip'];
                            $this->request->data['UserLevelEmailPerformance']['Clicked'] = 1;
                            $this->request->data['UserLevelEmailPerformance']['esp_id'] = 5;
                            $this->UserLevelEmailPerformance->create();
                            $this->UserLevelEmailPerformance->save($this->request->data);
                        }
                        break;

                    case 'bounce' :
                        $conditions = array(
                            'UserLevelEmailPerformance.EmailAddress' => $event['email'],
                            'UserLevelEmailPerformance.ListId' => $event['MailingID']
                        );
                        if ($this->UserLevelEmailPerformance->hasAny($conditions)) {
                            $this->UserLevelEmailPerformance->updateAll(
                                    array('UserLevelEmailPerformance.Bounced' => '1', 'UserLevelEmailPerformance.BouncedAt' => "'" . date("Y-m-d H:i:s", $event['timestamp']) . "'",
                                'UserLevelEmailPerformance.BounceReason' => "'" . str_replace("'", "", $event['status']) . "#" . str_replace("'", "", $event['reason']) . "'"), array('UserLevelEmailPerformance.ListId' => $event['MailingID']
                                , 'UserLevelEmailPerformance.EmailAddress' => $event['email'])
                            );
                        } else {
                            $this->request->data['UserLevelEmailPerformance']['ListId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['MessageId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $event['email'];
                            $this->request->data['UserLevelEmailPerformance']['BouncedAt'] = date("Y-m-d H:i:s", $event['timestamp']);
                            $this->request->data['UserLevelEmailPerformance']['localIp'] = '';
                            $this->request->data['UserLevelEmailPerformance']['Bounced'] = 1;
                            $this->request->data['UserLevelEmailPerformance']['esp_id'] = 3;
                            $this->request->data['UserLevelEmailPerformance']['BounceReason'] = "'" . str_replace("'", "", $event['status']) . "#" . str_replace("'", "", $event['reason']) . "'";
                            $this->UserLevelEmailPerformance->create();
                            $this->UserLevelEmailPerformance->save($this->request->data);
                        }
                        break;

                    case 'dropped' :
                        //mail("sunny.gulati@siliconbiztech.com","dropped",$strResp);
                        break;

                    case 'spamreport' :
                        $conditions = array(
                            'UserLevelEmailPerformance.EmailAddress' => $event['email'],
                            'UserLevelEmailPerformance.ListId' => $event['MailingID']
                        );
                        if ($this->UserLevelEmailPerformance->hasAny($conditions)) {
                            $this->UserLevelEmailPerformance->updateAll(
                                    array('UserLevelEmailPerformance.Unsubscribed' => '1', 'UserLevelEmailPerformance.SpamComplaint' => '1', 'UserLevelEmailPerformance.UnsubscribedAt' => "'" . date("Y-m-d H:i:s", $event['timestamp']) . "'"), array('UserLevelEmailPerformance.ListId' => $event['MailingID']
                                , 'UserLevelEmailPerformance.EmailAddress' => $event['email'])
                            );
                            $m_ad_uid = $this->tblMailing->field(
                                    'm_ad_uid', array('m_id' => split("-", $event['MailingID'])[1])
                            );
                            $aduidDFP = $this->tblMappedAdunit->field(
                                    'ma_ad_id', array('ma_uid' => $m_ad_uid)
                            );
                            $aduid = $this->tblAdunit->field(
                                    'ad_uid', array('ad_dfp_id' => $aduidDFP)
                            );
                            $pub_id = $this->tblAdunit->field(
                                    'publisher_id', array('ad_uid' => $aduid)
                            );
                            $table_name = $pub_id . "_" . $aduid;
                            $this->Dynamic->useTable = $table_name;
                            $this->Dynamic->Query("Update " . $table_name . "  SET  nl_isActive=0 WHERE nl_email='" . $event['email'] . "'");
                        } else {
                            $this->request->data['UserLevelEmailPerformance']['ListId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['MessageId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $event['sg_message_id'];
                            $this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $event['email'];
                            $this->request->data['UserLevelEmailPerformance']['UnsubscribedAt'] = date("Y-m-d H:i:s", $event['timestamp']);
                            $this->request->data['UserLevelEmailPerformance']['localIp'] = '';
                            $this->request->data['UserLevelEmailPerformance']['Unsubscribed'] = 1;
                            $this->request->data['UserLevelEmailPerformance']['SpamComplaint'] = 1;
                            $this->request->data['UserLevelEmailPerformance']['esp_id'] = 5;
                            $this->UserLevelEmailPerformance->create();
                            $this->UserLevelEmailPerformance->save($this->request->data);
                            $m_ad_uid = $this->tblMailing->field(
                                    'm_ad_uid', array('m_id' => split("-", $event['MailingID'])[1])
                            );
                            $aduidDFP = $this->tblMappedAdunit->field(
                                    'ma_ad_id', array('ma_uid' => $m_ad_uid)
                            );
                            $aduid = $this->tblAdunit->field(
                                    'ad_uid', array('ad_dfp_id' => $aduidDFP)
                            );
                            $pub_id = $this->tblAdunit->field(
                                    'publisher_id', array('ad_uid' => $aduid)
                            );
                            $table_name = $pub_id . "_" . $aduid;
                            $this->Dynamic->useTable = $table_name;
                            $this->Dynamic->Query("Update " . $table_name . "  SET  nl_isActive=0 WHERE nl_email='" . $event['email'] . "'");
                        }
                        break;

                    case 'unsubscribe' :
                        $conditions = array(
                            'UserLevelEmailPerformance.EmailAddress' => $event['email'],
                            'UserLevelEmailPerformance.ListId' => $event['MailingID']
                        );
                        if ($this->UserLevelEmailPerformance->hasAny($conditions)) {
                            $this->UserLevelEmailPerformance->updateAll(
                                    array('UserLevelEmailPerformance.Unsubscribed' => '1', 'UserLevelEmailPerformance.UnsubscribedAt' => "'" . date("Y-m-d H:i:s", $event['timestamp']) . "'"), array('UserLevelEmailPerformance.ListId' => $event['MailingID']
                                , 'UserLevelEmailPerformance.EmailAddress' => $event['email'])
                            );
                            $m_ad_uid = $this->tblMailing->field(
                                    'm_ad_uid', array('m_id' => split("-", $event['MailingID'])[1])
                            );
                            $aduidDFP = $this->tblMappedAdunit->field(
                                    'ma_ad_id', array('ma_uid' => $m_ad_uid)
                            );
                            $aduid = $this->tblAdunit->field(
                                    'ad_uid', array('ad_dfp_id' => $aduidDFP)
                            );
                            $pub_id = $this->tblAdunit->field(
                                    'publisher_id', array('ad_uid' => $aduid)
                            );
                            $table_name = $pub_id . "_" . $aduid;
                            $this->Dynamic->useTable = $table_name;
                            $this->Dynamic->Query("Update " . $table_name . "  SET  nl_isActive=0 WHERE nl_email='" . $event['email'] . "'");
                        } else {
                            $this->request->data['UserLevelEmailPerformance']['ListId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['MessageId'] = $event['MailingID'];
                            $this->request->data['UserLevelEmailPerformance']['ExMessageId'] = $event['sg_message_id'];
                            $this->request->data['UserLevelEmailPerformance']['EmailAddress'] = $event['email'];
                            $this->request->data['UserLevelEmailPerformance']['UnsubscribedAt'] = date("Y-m-d H:i:s", $event['timestamp']);
                            $this->request->data['UserLevelEmailPerformance']['localIp'] = '';
                            $this->request->data['UserLevelEmailPerformance']['Unsubscribed'] = 1;
                            $this->request->data['UserLevelEmailPerformance']['esp_id'] = 5;
                            $this->UserLevelEmailPerformance->create();
                            $this->UserLevelEmailPerformance->save($this->request->data);
                            $m_ad_uid = $this->tblMailing->field(
                                    'm_ad_uid', array('m_id' => split("-", $event['MailingID'])[1])
                            );
                            $aduidDFP = $this->tblMappedAdunit->field(
                                    'ma_ad_id', array('ma_uid' => $m_ad_uid)
                            );
                            $aduid = $this->tblAdunit->field(
                                    'ad_uid', array('ad_dfp_id' => $aduidDFP)
                            );
                            $pub_id = $this->tblAdunit->field(
                                    'publisher_id', array('ad_uid' => $aduid)
                            );
                            $table_name = $pub_id . "_" . $aduid;
                            $this->Dynamic->useTable = $table_name;
                            $this->Dynamic->Query("Update " . $table_name . "  SET  nl_isActive=0 WHERE nl_email='" . $event['email'] . "'");
                        }
                        break;

                    case 'group_unsubscribe' :
                        //mail("sunny.gulati@siliconbiztech.com","group_unsubscribe",$strResp);
                        break;

                    case 'group_resubscribe' :
                        //mail("sunny.gulati@siliconbiztech.com","group_resubscribe",$strResp);
                        break;

                    default:
                        break;
                }
            }
        }
        $this->autoRender = false;
    }

}