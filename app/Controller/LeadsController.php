<?php

/**
 * Contact Controller
 * @author James Fairhurst <info@jamesfairhurst.co.uk>
 */
class LeadsController extends AppController {

    /**
     * Components$this->loadModel('Authake.tblOrder');
     */
    var $uses = array('Authake.Group', 'Authake.User', 'Authake.tblProfile', 'Authake.tblOrder', 'OrderLeads', 'Authake.tblLineItem', 'Authake.Rule', 'Authake.tblUserConnect');
    var $components = array('RequestHandler', 'Authake.Filter', 'Session', 'Commonfunction', 'Paginator'); // var $layout = 'authake';
    var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc')); //var $scaffold;

    /**
     * Before Filter callback
     */

    public function beforeFilter() {
        parent::beforeFilter();
        // Change layout for Ajax requests
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
    }

    /**
     * Main index action
     */
    private function getAllOrderIds() {
        $userId = $this->Authake->getUserId();
        $options['conditions'] = array('tblOrder.isActive' => 1);
        if (!empty($user_id)) {
            $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        $options['conditions']['NOT'] = array('tblOrder.dfp_order_id' => '');
        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
        )));
        $options['order'] = array('tblOrder.created_at' => 'DESC');
        $options['fields'] = array('tblOrder.dfp_order_id', 'tblOrder.dfp_order_id');
        return $this->tblOrder->find('list', $options);
    }

   public function index() {
        // form posted
        //Configure::write('debug', 2);
        ini_set('memory_limit', '-1');
        $this->loadModel('Authake.tblCostStructure');
        $this->set('title_for_layout', 'View Leads');
        $userId = $this->Authake->getUserId();
        $options['conditions'] = array('tblOrder.isActive' => 1);
        if (!empty($user_id)) {
            $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        $options['conditions']['NOT'] = array('tblOrder.dfp_order_id' => '');
        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
        )));
        $options['order'] = array('tblOrder.created_at' => 'DESC');
        $options['fields'] = array('tblOrder.dfp_order_id', 'tblOrder.order_name');
        $orderlist = $this->tblOrder->find('list', $options);
        $option['all'] = "All";
        
        $this->set('orderlist', $option + $orderlist);
        $companylist = $this->tblProfile->getListConnectedProfiles(); //('tblProfile.dfp_company_id')
        $this->set('companylist', $option + $companylist);
        // get all line item

        $tblLineItems = $this->tblLineItem->getListLineItems();

        $this->set('tblLineItems', $option + $tblLineItems);
        $costStructure = $this->tblCostStructure->find('list', array('order' => array('tblCostStructure.cs_name' => 'asc'), 'fields' => array('tblCostStructure.cs_id', 'tblCostStructure.cs_name')));
        $this->set('costStructure', $costStructure);
        if (!empty($_REQUEST['search'])) {
            $post_data = $this->Session->read('LeadSearch');
        } else {
            $this->Session->delete('LeadSearch');
        }
        if (!empty($_POST['leadPost']) && ($_POST['leadPost'] == 'Submit')) {
            $this->Session->write('LeadSearch', $this->request->data);
            $post_data =$this->request->data;
        }
        
        if (!empty($post_data)) {
            $start_date = $end_date = '';
            if (!empty($post_data['Lead']['daterange'])) {
                $daterange = explode('-', $post_data['Lead']['daterange']);
                $start_date = $this->Commonfunction->edit_datepickerFormat($daterange[0]);
                $end_date = $this->Commonfunction->edit_datepickerFormat($daterange[1]);
            }
            $options = array(); // clear options here 
            // condition here 

            if (!empty($post_data['Lead']['od_isRejected'])) {
                $options['conditions']['OR'][] = array('OrderLeads.od_isRejected' => 0);
            }
            if (!empty($post_data['Lead']['od_return'])) {
                $options['conditions']['OR'][] = array('OrderLeads.od_isreturn' => 1);
            }
            if (!empty($post_data['Lead']['od_isTest'])) {

                $options['conditions']['OR'][] = array('OrderLeads.od_isTest' => 1);
            }
            if (!empty($post_data['Lead']['line_item'])) {
                if (!in_array("all", $post_data['Lead']['line_item']))
                    $options['conditions'][] = array('OrderLeads.od_lid' => $post_data['Lead']['line_item']);
            }
            if (!empty($post_data['Lead']['email'])) {
                $options['conditions'][] = array('OrderLeads.od_email like' => '%' . $post_data['Lead']['email'] . '%');
            }
            if (!empty($post_data['Lead']['daterange'])) {
                $options['conditions'][] = array('DATE(OrderLeads.od_create) between ? and ?' => array($start_date, $end_date));
            }
            if (!empty($post_data['Lead']['cust_structure'])) {
                $options['conditions'][] = array('tblLineItem.li_cs_id' => $post_data['Lead']['cust_structure']);
            }


            /* $this->OrderLeads->bindModel(array('hasMany' => array('tblLineItem' => 
              array('foreignKey' => 'od_lid','conditions'=>array(),'fields'=>array('tblLineItem.camp_name', 'dependent' => false))))); */

            $options['joins'] = array(
                array('table' => 'das_digitaladvertising.tbl_creatives',
                    'alias' => 'Creative',
                    'type' => 'INNER',
                    'conditions' => array(
                        'OrderLeads.od_cid = Creative.cr_id',
                    )),
//                array('table' => 'das_digitaladvertising.tbl_orders',
//                    'alias' => 'tblOrder',
//                    'type' => 'INNER',
//                    'conditions' => array(
//                        'OrderLeads.od_lid = tblOrder.dfp_order_id',
//                    )),
                array('table' => 'das_digitaladvertising.tbl_line_items',
                    'alias' => 'tblLineItem',
                    'type' => 'INNER',
                    'conditions' => array(
                        'OrderLeads.od_lid = tblLineItem.li_dfp_id',
                    ))
            );
            $options['fields'] = array(
                'tblLineItem.li_name', 'OrderLeads.od_country_name', 'OrderLeads.od_map_id', 'OrderLeads.od_lid', 'OrderLeads.od_email', 'OrderLeads.od_create',
                'OrderLeads.fp_browser', 'OrderLeads.fp_os', 'OrderLeads.od_isDuplicate', 'OrderLeads.od_ipaddress', 'OrderLeads.od_isRejected',
                'OrderLeads.od_isTest', 'Creative.cr_name', 'Creative.cr_header');
            //$options['group'] = array('OrderLeads.od_id');
            $ordersRecords = array();
            if (!empty($post_data['Lead']['company_id'][0]) && !empty($post_data['Lead']['order_id'][0])) {
                if (!empty($post_data['Lead']['order_id'])) {
                    if (in_array("all", $post_data['Lead']['order_id'])) {
                        $orders = self::getAllOrderIds();
                    } else {

                        $orders = $post_data['Lead']['order_id'];
                    }
                } else {
                    $orders = self::getAllOrderIds();
                }
                $order_data = array();
                //  pr($orders);
                //   exit();
                foreach ($orders as $order_id) {
                    // if get all then continue 
                    if ($order_id == "all")
                        continue;

                    $tble = $this->OrderLeads->useTable = "order_" . $order_id;
                    $tble = $this->OrderLeads->table = "order_" . $order_id;
                    $order_ids = $OrderLeads = array();
                    //$options['limit'] = '30';
                    $options['order'] = array('OrderLeads.od_create' => 'DESC');
                    $this->Paginator->settings = $options;
                    $OrderLeads = $this->Paginator->paginate('OrderLeads');
                   // pr($this->OrderLeads->getDataSource()->getLog(false,false));exit();
//                    $results = $this->OrderLeads->find('all', array(
//                        'contain' => array('OrderLeads')
//                    ));
                    //       pr($OrderLeads);

                    if (!empty($OrderLeads)) {
                        foreach ($OrderLeads as $orderlead) {
                            $mapids[] = $orderlead['OrderLeads']['od_map_id'];
                            $ordersRecords[] = $orderlead;
                        }

                        $mappedAdunit = ClassRegistry::init('MappedAdunit');
                        $mappedOptions['joins'] = array(
                            array('table' => 'tbl_flights',
                                'alias' => 'Flight',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'MappedAdunit.ma_fl_id = Flight.fl_id',
                                )),
//             
                        );
                        $mapids = array_unique($mapids);
                        $mappedOptions['conditions'] = array('MappedAdunit.ma_uid' => $mapids);
                        $mappedOptions['fields'] = array('Flight.fl_name', 'MappedAdunit.ma_uid');
                        $flights = $mappedAdunit->find('all', $mappedOptions);
                        
                       // $this->find('list', $options);		
                       // $log = $this->getDataSource()->getLog(false, false);
                        //debug($log);exit();

                        foreach ($OrderLeads as $i => $orderlead) {
                            foreach ($flights as $flight) {
                                if ($orderlead['OrderLeads']['od_map_id'] == $flight['MappedAdunit']['ma_uid']) {
                                    $ordersRecords[$i]['Flight']['name'] = $flight['Flight']['fl_name'];
                                }
                            }
                        }
                    }
                }

                $this->set('ordersRecords', $ordersRecords);
            } else {

                $this->Session->setFlash(__('Please select company name and order'), 'danger');
                $this->redirect(array('controller' => 'leads', 'action' => 'index'));
            }
            
            $this->set('post_data', $post_data);
           $this->set('comp_id',  $post_data['comp_id']);
        }
    }
    /*public function index() {
        // form posted
        Configure::write('debug', 1);
        ini_set('memory_limit', '-1');
        $this->loadModel('Authake.tblCostStructure');
        $this->set('title_for_layout', 'View Leads');
        $userId = $this->Authake->getUserId();
        $options['conditions'] = array('tblOrder.isActive' => 1);
        if (!empty($user_id)) {
            $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        $options['conditions']['NOT'] = array('tblOrder.dfp_order_id' => '');
        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
        )));
        $options['order'] = array('tblOrder.created_at' => 'DESC');
        $options['fields'] = array('tblOrder.dfp_order_id', 'tblOrder.order_name');
        $orderlist = $this->tblOrder->find('list', $options);
        $option['all'] = "All";
        $this->set('orderlist', $option + $orderlist);
        $companylist = $this->tblProfile->getListConnectedProfiles(); //('tblProfile.dfp_company_id')
        $this->set('companylist', $option + $companylist);
        // get all line item

        $tblLineItems = $this->tblLineItem->getListLineItems();

        $this->set('tblLineItems', $option + $tblLineItems);
        $costStructure = $this->tblCostStructure->find('list', array('order' => array('tblCostStructure.cs_name' => 'asc'), 'fields' => array('tblCostStructure.cs_id', 'tblCostStructure.cs_name')));
        $this->set('costStructure', $costStructure);
        if (!empty($_REQUEST['search'])) {
            $post_data = $this->Session->read('LeadSearch');
        } else {
            $this->Session->delete('LeadSearch');
        }
        if (!empty($_POST['leadPost']) && ($_POST['leadPost'] == 'Submit')) {
            $this->Session->write('LeadSearch', $this->request->data);
            $post_data =$this->request->data;
        }
        if (!empty($post_data)) {
            $start_date = $end_date = '';
            if (!empty($post_data['Lead']['daterange'])) {
                $daterange = explode('-', $post_data['Lead']['daterange']);
                $start_date = $this->Commonfunction->edit_datepickerFormat($daterange[0]);
                $end_date = $this->Commonfunction->edit_datepickerFormat($daterange[1]);
            }
            $options = array(); // clear options here 
            // condition here 

            if (!empty($post_data['Lead']['od_isRejected'])) {
                $options['conditions']['OR'][] = array('OrderLeads.od_isRejected' => 0);
            }
            if (!empty($post_data['Lead']['od_return'])) {
                $options['conditions']['OR'][] = array('OrderLeads.od_isreturn' => 1);
            }
            if (!empty($post_data['Lead']['od_isTest'])) {

                $options['conditions']['OR'][] = array('OrderLeads.od_isTest' => 1);
            }
            if (!empty($post_data['Lead']['line_item'])) {
                if (!in_array("all", $post_data['Lead']['line_item']))
                    $options['conditions'][] = array('OrderLeads.od_lid' => $post_data['Lead']['line_item']);
            }
            if (!empty($post_data['Lead']['email'])) {
                $options['conditions'][] = array('OrderLeads.od_email like' => '%' . $post_data['Lead']['email'] . '%');
            }
            if (!empty($post_data['Lead']['daterange'])) {
                $options['conditions'][] = array('DATE(OrderLeads.od_create) between ? and ?' => array($start_date, $end_date));
            }
            if (!empty($post_data['Lead']['cust_structure'])) {
                $options['conditions'][] = array('tblLineItem.li_cs_id' => $post_data['Lead']['cust_structure']);
            }


            // $this->OrderLeads->bindModel(array('hasMany' => array('tblLineItem' => 
             // array('foreignKey' => 'od_lid','conditions'=>array(),'fields'=>array('tblLineItem.camp_name', 'dependent' => false)))));

            $options['joins'] = array(
                array('table' => 'das_digitaladvertising.tbl_creatives',
                    'alias' => 'Creative',
                    'type' => 'INNER',
                    'conditions' => array(
                        'OrderLeads.od_cid = Creative.cr_id',
                    )),
//                array('table' => 'das_digitaladvertising.tbl_orders',
//                    'alias' => 'tblOrder',
//                    'type' => 'INNER',
//                    'conditions' => array(
//                        'OrderLeads.od_lid = tblOrder.dfp_order_id',
//                    )),
                array('table' => 'das_digitaladvertising.tbl_line_items',
                    'alias' => 'tblLineItem',
                    'type' => 'INNER',
                    'conditions' => array(
                        'OrderLeads.od_lid = tblLineItem.li_dfp_id',
                    ))
            );
            $options['fields'] = array(
                'tblLineItem.li_name', 'OrderLeads.od_country_name', 'OrderLeads.od_map_id','OrderLeads.od_lid', 'OrderLeads.od_email', 'OrderLeads.od_create',
                'OrderLeads.fp_browser', 'OrderLeads.fp_os', 'OrderLeads.od_isDuplicate', 'OrderLeads.od_ipaddress', 'OrderLeads.od_isRejected',
                'OrderLeads.od_isTest', 'Creative.cr_name', 'Creative.cr_header');
            //$options['group'] = array('OrderLeads.od_id');
            $ordersRecords = array();
            if (!empty($post_data['Lead']['company_id'][0]) && !empty($post_data['Lead']['order_id'][0])) {
                if (!empty($post_data['Lead']['order_id'])) {
                    if (in_array("all", $post_data['Lead']['order_id'])) {
                        $orders = self::getAllOrderIds();
                    } else {

                        $orders = $post_data['Lead']['order_id'];
                    }
                } else {
                    $orders = self::getAllOrderIds();
                }
                $order_data = array();
                //  pr($orders);
                //   exit();
                foreach ($orders as $order_id) {
                    // if get all then continue 
                    if ($order_id == "all")
                        continue;
                    if(isset($this->request->params['named']['sort'])&&$this->request->params['named']['sort'] != ''){     
                            $sortField  =   $this->request->params['named']['sort'];        
                    }else{      
                        $sortField  =   'OrderLeads.od_id';     
                    }       
                    
                    $tble = $this->OrderLeads->useTable = "order_" . $order_id;
                    $tble = $this->OrderLeads->table = "order_" . $order_id;
                    $order_ids = $OrderLeads = array();
                    $options['limit'] = 20;
                    $options['order'] = array($sortField => 'DESC');
                    $this->Paginator->settings = $options;
                    $OrderLeads = $this->Paginator->paginate('OrderLeads');
                    // pr($this->OrderLeads->getDataSource()->getLog(false,false));exit();
//                    $results = $this->OrderLeads->find('all', array(
//                        'contain' => array('OrderLeads')
//                    ));
                    //       pr($OrderLeads);

                    if (!empty($OrderLeads)) {
                        foreach ($OrderLeads as $orderlead) {
                            $mapids[] = $orderlead['OrderLeads']['od_map_id'];
                            $ordersRecords[] = $orderlead;
                        }

                        $mappedAdunit = ClassRegistry::init('MappedAdunit');
                        $mappedOptions['joins'] = array(
                            array('table' => 'tbl_flights',
                                'alias' => 'Flight',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'MappedAdunit.ma_fl_id = Flight.fl_id',
                                )),
//             
                        );
                        $mapids = array_unique($mapids);
                        $mappedOptions['conditions'] = array('MappedAdunit.ma_uid' => $mapids);
                        $mappedOptions['fields'] = array('Flight.fl_name', 'MappedAdunit.ma_uid');
                        $flights = $mappedAdunit->find('all', $mappedOptions);

                        foreach ($OrderLeads as $i => $orderlead) {
                            foreach ($flights as $flight) {
                                if ($orderlead['OrderLeads']['od_map_id'] == $flight['MappedAdunit']['ma_uid']) {
                                    $ordersRecords[$i]['Flight']['name'] = $flight['Flight']['fl_name'];
                                }
                            }
                        }
                    }
                }

                $this->set('ordersRecords', $ordersRecords);
            } else {

                $this->Session->setFlash(__('Please select company name and order'), 'danger');
                $this->redirect(array('controller' => 'leads', 'action' => 'index'));
            }
            $this->set('post_data', $post_data);
        }
    }*/

    private function lineitemName($order_ids = array()) {

        $orderByList = array();
        $lineitems = $this->tblLineItem->find('all', array('fields' => array('tblLineItem.li_name', 'tblLineItem.*'), 'conditions' => array('tblLineItem.li_order_id' => $order_ids)));

        if (!empty($lineitems)) {
            foreach ($lineitems as $lineitem) {
                $orderByList[$lineitem['tblLineItem']['li_order_id']][] = $lineitem['tblLineItem']['li_name'];
            }
        }
        return $orderByList;
    }

    public function reConciliation() {

        $this->set('title_for_layout', 'Leads Reconciliation');
        // load model of order 

        $userId = $this->Authake->getUserId();
        $options['conditions'] = array('tblOrder.isActive' => 1);
        if (!empty($user_id)) {
            $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
        )));
        $options['order'] = array('tblOrder.created_at' => 'DESC');
        $options['fields'] = array('tblOrder.dfp_order_id', 'tblOrder.order_name');
        $orderlist = $this->tblOrder->find('list', $options);
        $this->set('orderlist', $orderlist);
        if (!empty($this->request->data)) {
            if ($this->request->data['Lead']['csv_file']['error'] == 0) {
                $_upload_csv = self::_upload_csv($this->request->data['Lead']['csv_file']);
            }
            if (!empty($_upload_csv) and ! empty($this->request->data['Lead']['order_id']) and ! empty($this->request->data['Lead']['line_id'])) {
                $destination = Configure::read('Path.csvPath') . $_upload_csv;
                //
                $fh = fopen($destination, 'r') or die("can't open file");
                $emailvalid = array();
                $emailinvalid = array();
                while (($data = fgetcsv($fh) ) !== FALSE) {
                    if (!filter_var($data[0], FILTER_VALIDATE_EMAIL) === false) {
                        $emailvalid[] = $data[0];
                    } else {
                        $emailinvalid[] = $data[0];
                    }
                }
                // update order  od_isreturn when check email 
                $updated_emails = $notupdated_email = array();
                if (!empty($emailvalid)) {
                    foreach ($emailvalid as $email) {
                        $this->OrderLeads->useTable = "order_" . $this->request->data['Lead']['order_id']; //das_email
                        $OrderLeads = $this->OrderLeads->find('first', array('fields' => array('OrderLeads.od_id'), 'conditions' => array('OrderLeads.od_email like' => $email, 'OrderLeads.od_lid' => $this->request->data['Lead']['order_id'])));
                        if (!empty($OrderLeads)) {
                            $this->OrderLeads->updateAll(array('od_isreturn' => 1), array('OrderLeads.od_id' => $OrderLeads['OrderLeads']['od_id']));
                            $updated_emails[] = $email;
                        } else {
                            $notupdated_email[] = $email;
                        }
                    }
                }
                $message = '';
                if (!empty($updated_emails)) {
                    $message .=' Upload emails:- ' . count($updated_emails);
                }
                if (!empty($notupdated_email)) {
                    $message .='  Not Upload emails:- ' . count($notupdated_email);
                }
                if (!empty($emailinvalid)) {
                    $message .='  Invalid emails:- ' . count($emailinvalid);
                }


                $this->Session->setFlash(__($message), 'success');
                unlink($destination); //delete after read file 
                fclose($fh);
            } else {
                $this->Session->setFlash(__('Please select order list, line item and csv file'), 'error');
            }
        }
    }

    private function _upload_csv($csv = array()) {
        if ($csv['error'] > 0) {
            return null;
        } else {
            $existing_csv = array();
            if ($csv['error'] > 0) {
                return $existing_csv['Lead']['csv_file'];
            } else {
                $destination = Configure::read('Path.csvPath');
                if (!file_exists($destination)) {
                    $createStructure = mkdir($destination, 0777, true);
                }
                $ext = explode('.', $csv['name']);
                $get_ext = array_pop($ext);
                $name = basename($csv['name'], '.' . $get_ext);


                $csv_name = self::clean($name) . '_' . time() . '.' . $get_ext;
                //$image_name = time() . '_' . time() . '.' . array_pop($ext);
                move_uploaded_file($csv['tmp_name'], $destination . $csv_name);
                if (!empty($existing_csv)) {
                    unlink($destination . $existing_csv['Lead']['csv_file']);
                }
                //move_uploaded_file($filename, $destination);
                return $csv_name;
            }
        }
    }

    private function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function order_line_item($order_id = null) {
        $this->autoRender = false;
        $options['joins'] = array(
            array('table' => 'tbl_cost_structures',
                'alias' => 'tcs',
                'type' => 'INNER',
                'conditions' => array(
                    'tcs.cs_id = tblLineItem.li_cs_id'
                )),
            array('table' => 'tbl_lineitem_status',
                'alias' => 'tls',
                'type' => 'INNER',
                'conditions' => array(
                    'tls.ls_id = tblLineItem.li_status_id'
                ))
        );

        $options['fields'] = array('tblLineItem.li_id', 'tblLineItem.li_name');

        $options['conditions'] = array('tblLineItem.li_order_id' => $order_id);
        $tblLineItems = $this->tblLineItem->find("list", $options);
        $line_items[0]['id'] = "0";
        $line_items[0]['text'] = "Select (or start typing) the line item";

        if (!empty($tblLineItems)) {
            $count = 1;
            foreach ($tblLineItems as $key => $value) {

                $line_items[$count]['id'] = $key;
                $line_items[$count]['text'] = $value;
                $count++;
            }
        }
        return json_encode($line_items);
    }

    function samplecsv() {
        //$this->autoRender = false;
        header('Content-Type: application/csv');
        $destination = Configure::read('Path.url_csvPath') . 'sample.csv';
        header('Content-Disposition: attachment; filename=' . $destination);
        header('Pragma: no-cache');
        readfile("/");
        die;
    }

     function getAllOrderList() {
       
        $this->autoRender = false;
        $user_id = array();
        if (!empty($this->request->data['user_id'])) {
            $user_id = explode(',', $this->request->data['user_id']);
        }

        $userId = $this->Authake->getUserId();
        $options['conditions'] = array('tblOrder.isActive' => 1);
        if (!in_array('all', $user_id) and ! empty($user_id)) {
            $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
        }
        $options['conditions']['NOT'] = array('tblOrder.dfp_order_id' => '');
        $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($userId)
        )));

        $options['fields'] = array('tblOrder.*', 'tblp.CompanyName');

        $options['order'] = array(
            'tblOrder.created_at' => 'DESC');

        $options['fields'] = array('tblOrder.dfp_order_id', 'tblOrder.order_name');

        $orders = $this->tblOrder->find('list', $options);
        //$log = $this->tblOrder->getDataSource()->getLog(false, false);
		//debug($log);
        $order_list = array();

        // $order_list[0]['id'] = "all";
        //  $order_list[0]['text'] = "All";
        if (!empty($orders)) {
            $count = 0;
            foreach ($orders as $key => $value) {

                $order_list[$count]['id'] = $key;
                $order_list[$count]['text'] = $value;
                $count++;
            }
        }
        //return json_encode($log);
        return json_encode($order_list);
    }

    function getAllLineItemByOrder() {
        $this->autoRender = false;
        $order_id = array();
        if (!empty($this->request->data['order_id'])) {
            $order_id = explode(',', $this->request->data['order_id']);
        }

        $options = array();
        $options['joins'] = array(
            array('table' => 'tbl_cost_structures',
                'alias' => 'tcs',
                'type' => 'INNER',
                'conditions' => array(
                    'tcs.cs_id = tblLineItem.li_cs_id'
                )),
            array('table' => 'tbl_lineitem_status',
                'alias' => 'tls',
                'type' => 'INNER',
                'conditions' => array(
                    'tls.ls_id = tblLineItem.li_status_id'
                ))
        );

        if (!in_array('all', $order_id) and ! empty($order_id))
            $options['conditions'] = array('tblLineItem.li_order_id' => $order_id);
        $options['fields'] = array('tblLineItem.li_dfp_id', 'tblLineItem.li_name');
        $tblLineItems = $this->tblLineItem->find("list", $options);
        $line_items[0]['id'] = "all";
        $line_items[0]['text'] = "All";

        if (!empty($tblLineItems)) {
            $count = 1;
            foreach ($tblLineItems as $key => $value) {
                $line_items[$count]['id'] = $key;
                $line_items[$count]['text'] = $value;
                $count++;
            }
        }
        return json_encode($line_items);
    }

    // CHECK AND CREATE DYAMIC FIELD OR TABLE
    function ordertabledb() {
        $tablestructure = array(
            'od_lid' => " ADD `od_lid` int NOT NULL DEFAULT 0 AFTER `od_create`",
            'od_cid' => " ADD  `od_cid` int NOT NULL DEFAULT 0 AFTER `od_create`",
            'od_ad_id' => " ADD  `od_ad_id` int NOT NULL DEFAULT 0 AFTER `od_create`",
            'od_mailing_id' => " ADD  `od_mailing_id` int DEFAULT 0 AFTER `od_create`",
            'od_map_id' => " ADD  `od_map_id` char(36) NOT NULL AFTER `od_create`",
            'od_country_name' => " ADD `od_country_name` varchar(200) NOT NULL AFTER `od_create`",
            'od_source' => " ADD  `od_source` varchar(200) NOT NULL  AFTER `od_create`",
            'od_email' => " ADD  `od_email` varchar(100) NOT NULL AFTER `od_create`",
            'od_phone' => " ADD  `od_phone` varchar(20) NOT NULL  AFTER `od_create`",
            'od_fname' => " ADD  `od_fname` varchar(100) DEFAULT NULL  AFTER `od_create`",
            'od_lname' => " ADD  `od_lname` varchar(100) DEFAULT NULL  AFTER `od_create`",
            'od_site' => " ADD  `od_site` varchar(100) DEFAULT NULL  AFTER `od_create`",
            'device_type' => " ADD  `device_type` varchar(100) DEFAULT NULL  AFTER `od_create`",
            'od_create' => " ADD  `od_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `od_create`",
            'fp_browser' => " ADD  `fp_browser` varchar(200) DEFAULT NULL  AFTER `od_create`",
            'fp_connection' => " ADD  `fp_connection` varchar(200) DEFAULT NULL  AFTER `od_create`",
            'fp_display' => " ADD  `fp_display` varchar(200) DEFAULT NULL  AFTER `od_create`",
            'fp_flash' => " ADD  `fp_flash` varchar(100) DEFAULT NULL  AFTER `od_create`",
            'fp_language' => " ADD  `fp_language` varchar(200) DEFAULT NULL  AFTER `od_create`",
            'fp_os' => " ADD  `fp_os` varchar(200) DEFAULT NULL  AFTER `od_create`",
            'fp_timezone' => " ADD  `fp_timezone` varchar(100) DEFAULT NULL  AFTER `od_create`",
            'fp_useragent' => " ADD  `fp_useragent` varchar(500) DEFAULT NULL  AFTER `od_create`",
            'od_isDuplicate' => " ADD  `od_isDuplicate` tinyint(1) DEFAULT 0 AFTER `od_create`",
            'od_isTest' => " ADD  `od_isTest` tinyint(1) DEFAULT 0 AFTER `od_create`",
            'od_ipaddress' => " ADD  `od_ipaddress` int(10) unsigned DEFAULT NULL  AFTER `od_create`",
            'od_isreturn' => " ADD  `od_isreturn` tinyint(2) DEFAULT 0 AFTER `od_create`",
            'od_isRejected' => " ADD  `od_isRejected` tinyint(1) DEFAULT 0 AFTER `od_create`"
        );

        $orders = $this->OrderLeads->query('SHOW TABLES');
        $sql = "";
        $addField = array();
        if (!empty($orders)) {
            foreach ($orders as $order) {
                $tablename = $order['TABLE_NAMES']['Tables_in_das_order_leads'];
                $isordertable = strstr($order['TABLE_NAMES']['Tables_in_das_order_leads'], 'orderj_');
                if (empty($isordertable))
                    continue;
                try {
                    $results = $this->OrderLeads->query("DESCRIBE " . $tablename);
                } catch (Exception $e) {
                    // CREATE A NEW TABLE 
                    $sql_table = "CREATE TABLE IF NOT EXISTS " . $tablename . " (
									`od_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
									`od_lid` int NOT NULL DEFAULT 0,
									`od_cid` int NOT NULL DEFAULT 0,
									`od_ad_id` int NOT NULL DEFAULT 0,
									`od_mailing_id` int DEFAULT 0,
									`od_map_id` char(36) NOT NULL,
									`od_country_name` varchar(200) NOT NULL,
									`od_source` varchar(200) NOT NULL,
									`od_email` varchar(100) NOT NULL,
									`od_phone` varchar(20) NOT NULL,
									`od_fname` varchar(100) DEFAULT NULL,
									`od_lname` varchar(100) DEFAULT NULL,
									`od_site` varchar(100) DEFAULT NULL,
									`device_type` varchar(100) DEFAULT NULL,
									`od_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
									`fp_browser` varchar(200) DEFAULT NULL,
									`fp_connection` varchar(200) DEFAULT NULL,
									`fp_display` varchar(200) DEFAULT NULL,
									`fp_flash` varchar(100) DEFAULT NULL,
									`fp_language` varchar(200) DEFAULT NULL,
									`fp_os` varchar(200) DEFAULT NULL,
									`fp_timezone` varchar(100) DEFAULT NULL,
									`fp_useragent` varchar(500) DEFAULT NULL,
									`od_isDuplicate` tinyint(1) DEFAULT '0',
									`od_isTest` tinyint(1) DEFAULT '0',
									`od_ipaddress` int(10) unsigned DEFAULT NULL,
									`od_isreturn` tinyint(2) DEFAULT '0',
									`od_isRejected` tinyint(1) DEFAULT '0',
									PRIMARY KEY (`od_id`)
									) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
                    $this->OrderLeads->query($sql_table);
                }
                $addField = array();
                if (!empty($results)) {
                    foreach ($results as $result) {
                        $addField[$result['COLUMNS']['Field']] = $result['COLUMNS']['Field'];
                    }
                    $result = array_diff_key($tablestructure, $addField);
                    if (!empty($result))
                        $sql .= "ALTER TABLE " . $tablename . "  " . implode(',', $result) . ";";
                }
            }
            if (!empty($result)) {
                $results = $this->OrderLeads->query($sql);
            }
        }
        pr($sql);
        die;
    }

    // here create das email 
    function das_email() {
        $this->loadModel('Authake.tblAdunit');
        $this->loadModel('Dynamic');
        $adunits = $this->tblAdunit->find('all', array('fields' => array('tblAdunit.publisher_id', 'tblAdunit.ad_uid')));
        if (!empty($adunits)) {
            // define table array here 
            $tablestructure = array(
                'nl_adunit_uuid' => " ADD `nl_adunit_uuid` char(50) NOT NULL AFTER `nl_id`",
                'nl_email' => " ADD  `nl_email` varchar(100) NOT NULL AFTER `nl_id`",
                'nl_fname' => " ADD  `nl_fname` varchar(100) DEFAULT NULL AFTER `nl_id`",
                'nl_lname' => " ADD  `nl_lname` varchar(100) NOT NULL AFTER `nl_id`",
                'nl_isActive' => " ADD  `nl_isActive` tinyint(1) NOT NULL AFTER `nl_id`",
                'nl_source' => " ADD  `nl_source` varchar(100) NOT NULL AFTER `nl_id`",
                'nl_ispromotion' => " ADD  `nl_lname` varchar(100) NOT NULL AFTER `nl_id`",
                'nl_create' => " ADD  `nl_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `nl_id`",
            );
        }
        $sql = "";
        $addField = array();
        if (!empty($adunits)) {
            foreach ($adunits as $adunits) {
                $tablename = $adunits['tblAdunit']['publisher_id'] . '_' . $adunits['tblAdunit']['ad_uid'];
                try {
                    $results = $this->Dynamic->query("DESCRIBE " . $tablename);
                    $addField = array();
                    if (!empty($results)) {
                        foreach ($results as $result) {
                            $addField[$result['COLUMNS']['Field']] = $result['COLUMNS']['Field'];
                        }

                        $result = array_diff_key($tablestructure, $addField);

                        if (!empty($result))
                            $sql .= " ALTER TABLE " . $tablename . "  " . implode(',', $result) . ";";
                    }
                } catch (Exception $e) {
                    // create a table if no found in das email db 
                    $sqltable = "CREATE TABLE IF NOT EXISTS " . $tablename . " (
							`nl_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
							`nl_adunit_uuid` char(50) NOT NULL,
							`nl_email` varchar(100) NOT NULL,
							`nl_fname` varchar(100) DEFAULT NULL,
							`nl_lname` varchar(100) DEFAULT NULL,
							`nl_ispromotion` int(11) DEFAULT '1',
							`nl_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
							PRIMARY KEY (`nl_id`)
							) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
                    $this->Dynamic->query($sqltable);
                }
            }
            if (!empty($sql)) {
                $this->Dynamic->query($sql);
            }
        }
        pr($sql);
        die;
    }

}

//ALTER TABLE  `order_386190511` ADD  `request` TEXT NULL AFTER  `od_isRejected` ,
//ADD  `response` TEXT NULL AFTER  `request` ,
//ADD  `data` TEXT NULL AFTER  `response` ;