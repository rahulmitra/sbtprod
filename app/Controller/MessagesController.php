<?php
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class MessagesController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblAdunit','Authake.tblOrder','Authake.tblMailing','Authake.tblMappedAdunits','Authake.tblNlsubscribers','Authake.tblSocialMessage','Authake.tblMappedAdunit','DasDynamic');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index($listid = 'all',$duration = 'month') {	
			
			
			
			//echo date('Y-m-d  H:i:s');
			//$rep = $this->tblMailing->query("select NOW()");
			//print_r($rep);
			
			
			switch($duration)
			{ 
				case "day" :
				$range = $this->rangeDay(date("Y-m-d"));
				break;
				case "week" :
				$range = $this->rangeWeek(date("Y-m-d"));
				break;
				case "month" :
				$range = $this->rangeMonth(date("Y-m-d"));
				break;
				case "year" :
				$range = $this->rangeYear(date("Y-m-d"));
				break;
				default :
				$range = $this->rangeWeek(date("Y-m-d"));
				break;
			} 
			
			
			
			$options['joins'] = array(
			array('table' => 'tbl_mapped_adunits',
			'alias' => 'tma',
			'type' => 'INNER',
			'conditions' => array(
			'tma.ma_uid = tblMailing.m_ad_uid')),
			array('table' => 'tbl_adunits',
			'alias' => 'tba', 
			'type' => 'INNER',
			'conditions' => array(
			'tba.ad_dfp_id = tma.ma_ad_id',
			'tba.owner_user_id' => array($this->Authake->getUserId())
			)));
			
			
			if($listid == "all")
			{
				$options['conditions'] = array( 'DATE(tblMailing.m_schedule_date) between ? and ?'=> array($range['start'], $range['end']));
				 
			} else {
				$options['conditions'] = array( 'DATE(tblMailing.m_schedule_date) between ? and ?'=> array($range['start'], $range['end']),'tba.ad_uid ' => $listid);
				
			}
			$options['group'] = array('tblMailing.m_id');
			$options['fields'] = array('tba.*', 'tblMailing.*');
			$options['order'] = array('tblMailing.m_schedule_date'=>'DESC');
			
			//print_r($options);
			
			$tblMailing = $this->tblMailing->find('all', $options);
			$sent = $delivered =$opens = $clicks = 0;
			foreach ($tblMailing as $item) {
				$sent += $item['tblMailing']['m_sent'];
				$delivered += $item['tblMailing']['m_delivered'];
				$opens += $item['tblMailing']['m_u_open'];
				$clicks += $item['tblMailing']['m_u_clicks'];
			}
			
			
			$this->set('sent', $sent);
			$this->set('delivered', $delivered);
			$this->set('opens', $opens);
			$this->set('clicks', $clicks);
			$this->set('duration', $duration);
			
			//pr($tblMailing);die;
			
			//$selectQuery = "SELECT *, (select count(*) from tbl_messages where msg_m_id=m_id) as sent, (select count(*) from tbl_tracking_delivers where td_mailing_id=m_id) as delivered , (select count(*) from tbl_tracking_opens where to_mailing_id=m_id) as opens , (select count(*) from tbl_tracking_clicks where tc_mailing_id=m_id) as clicks FROM `tbl_mailings` where m_ad_uid in (SELECT ad_uid FROM `tbl_adunits` WHERE publisher_id =15)";
			//$response = $this->DasDynamic->query($selectQuery);
			//print_r($response);
			$this->set('group', $tblMailing);
			
		}
		
		
		
		public function view_message_stats($messageid){
			
			$options['joins'] = array(
			array('table' => 'tbl_mapped_adunits',
			'alias' => 'tma',
			'type' => 'INNER',
			'conditions' => array(
			'tma.ma_uid = tblMailing.m_ad_uid')),
			array('table' => 'tbl_adunits',
			'alias' => 'tba', 
			'type' => 'INNER',
			'conditions' => array(
			'tba.ad_dfp_id = tma.ma_ad_id',
			'tba.owner_user_id' => array($this->Authake->getUserId())
			)),
			array('table' => 'tbl_profiles',
			'alias' => 'tp',
			'type' => 'INNER',
			'conditions' => array(
			'tp.user_id = tba.publisher_id'))
			);
			
			
			$options['conditions'] = array('tblMailing.m_id ' => $messageid
			);
			
			$options['fields'] = array('tba.*', 'tblMailing.*', 'tp.*');
			
			$tblMailing = $this->tblMailing->find('first', $options);
			//pr($tblMailing);die;
			$range['start']=date('Y').'-01-01';
			$range['end']=date('Y-m-d');
			App::import('Vendor', 'src/Piwik');
			//$tblMailing['tba']['tracksite_id']
			$piwik = new Piwik(Configure::read('Piwik.url'), Configure::read('Piwik.token'), 203, Piwik::FORMAT_JSON);
			$track_code = $tblMailing['tblMailing']['m_ad_uid'] . "-" . $tblMailing['tblMailing']['m_id'];
			//$track_code = '768b1c46195411e6b91e123aa499f9d5-938';
			$piwik->setPeriod(Piwik::PERIOD_RANGE);
			$piwik->setRange($range['start'], $range['end']);
			$getCountries = $piwik->getCountry("contentPiece==".$track_code);
			$country = $DeviceType=$browsers =$osfamily =array();
			 if(!empty($getCountries)){
					foreach($getCountries as $getCountry) {
					if (array_key_exists($getCountry->label, $country)) {
						$country[$getCountry->label] += $getCountry->nb_visits;
						} else {
						$country[$getCountry->label] = $getCountry->nb_visits;
					}
				}
			}
			$id = $tblMailing['tblMailing']['m_ad_uid'] . "-" . $tblMailing['tblMailing']['m_id'];
			
			if($tblMailing['tblMailing']['m_ad_uid'] == "c4d4fcce243c11e6b91e123aa499f9d5")
			$id = $tblMailing['tblMailing']['m_ad_uid'];
			
			$this->set('sent', $tblMailing['tblMailing']['m_sent']);
			$this->set('tblMailing', $tblMailing);
			$this->set('delivered', $tblMailing['tblMailing']['m_delivered']);
			$this->set('opens', $tblMailing['tblMailing']['m_u_open']);
			$this->set('clicks', $tblMailing['tblMailing']['m_u_clicks']);
			$this->set('country', $country); 
			$this->set('id', $id); 
			
		}
		
		
		public function view_email_lists() {
			// form posted
			$this->set('title_for_layout','All Email Lists');
			$options['joins'] = array(
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.publisher_id = tblp.user_id',
			'tblAdunit.owner_user_id' => array($this->Authake->getUserId()))),
			array('table' => 'tbl_product_types',
			'alias' => 'tbpt',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.ad_ptype_id = tbpt.ptype_id'
			)),
			array('table' => 'tbl_media_types',
			'alias' => 'tbmt',
			'type' => 'INNER',
			'conditions' => array(
			'tbmt.mtype_id = tbpt.ptype_m_id',
			'tbmt.mtype_id = 2'
			)));
			
			$options['fields'] = array('tblAdunit.*', 'tblp.CompanyName', 'tbpt.ptype_name', 'tbmt.mtype_name');
			
			$tblAdunits = $this->tblAdunit->find('all', $options);
			//print_r($tblAdunits);
			$this->set('group', $tblAdunits);
		}
		
		
		function rangeMonth($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('first day of this month', $dt));
			$res['end'] = date('Y-m-d', strtotime('last day of this month', $dt));
			return $res;
		}
		
		function rangeWeek($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('-7 Days', $dt));
			$res['end'] = $datestr;
			return $res;
		}
		
		function rangeYear($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('-365 Days', $dt));
			$res['end'] = $datestr;
			return $res;
		}
		
		function rangeDay($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = $datestr;
			$res['end'] = $datestr;
			return $res;
		}
		
		public function create() {
			if (!empty($this->request->data))
			{
				
				$time=time();
				$ip=$_SERVER['REMOTE_ADDR'];
				$adUnitId = $this->request->data['tblMailing']['m_ad_uid'];
				$subject = $this->request->data['tblMailing']['m_sl'];
				
				
				$optionm['conditions'] = array('tblMappedAdunit.ma_uid' => $adUnitId,  'tblMappedAdunit.ma_isActive' => '1');
				$optionm['joins'] = array(
				array('table' => 'tbl_adunits',
				'alias' => 'tblAdunit',
				'type' => 'INNER',
				'conditions' => array(
				'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
				),
				array('table' => 'tbl_profiles',
				'alias' => 'tblp',
				'type' => 'INNER',
				'conditions' => array(
				'tblAdunit.publisher_id = tblp.user_id')));
				$optionm['fields'] = array('tblMappedAdunit.*', 'tblAdunit.*');
				$mappedUnits = $this->tblMappedAdunit->find('first',$optionm);
				//print_r($mappedUnits);
				
				$filename = "";
				foreach(glob("/home/stockprice/public_html/digitaladvertising.systems/app/webroot/upload/". $mappedUnits['tblMappedAdunit']['ma_uid'] . "/*.html") as $filename)
				{
					$filename =  substr(strrchr($filename, "/"), 1);
				}
				
				$html = file_get_contents('http://digitaladvertising.systems/upload/'. $mappedUnits['tblMappedAdunit']['ma_uid'] . '/' . $filename);
				
				//echo $html;
				
				App::import('Vendor', 'src/html2text');
				$h2t = new html2text($html);
				$text = $h2t->get_text();
				
				$imageURL = "";
				try {
					App::import('Vendor', 'src/html2pdf/HTML2PDF');
					$html2pdf = new HTML2PDF('P', 'A4');
					$html2pdf->writeHTML($html);
					$file = $html2pdf->Output('temp.pdf','F');
					$im = new imagick('temp.pdf');
					$im->setImageFormat( "jpg" );
					$img_name = time().'.jpg';
					$im->cropImage(600,400, 0,0);
					$im->writeImage("/home/stockprice/public_html/digitaladvertising.systems/app/webroot/upload/". $mappedUnits['tblMappedAdunit']['ma_uid'] . "/" .$img_name);
					$im->clear();
					$im->destroy();
					$imageURL = "/upload/".$mappedUnits['tblMappedAdunit']['ma_uid']."/". $img_name;
					} catch(Exception $e) {
					$imageURL = "/img/no-preview.png";
				}
				
				//print_r($mappedUnits);
				$messageID = array(); 
				if(isset($mappedUnits['tblAdunit']['ad_esp_list_id']) && $mappedUnits['tblAdunit']['ad_esp'] == "1")
				{
					App::import('Vendor', 'src/lyrisapi');
					$lyriss = new lyrisapi("666767","Lyris@API");
					$messageID = $lyriss->messageAdd($mappedUnits['tblAdunit']['ad_esp_list_id'],"","",$subject,"HTML",$text,$html,"Lyris@API");
					
					if($messageID['status'] == "success"){
						$this->request->data['tblMailing']['m_ad_uid'] = $mappedUnits['tblMappedAdunit']['ma_uid'];
						$schedule = explode('/',$this->request->data['tblMailing']['m_schedule_date']);
						$schedule_date = $schedule[2] . "-". $schedule[0]."-".$schedule[1]; 
						$this->request->data['tblMailing']['m_sl'] = $subject;
						$this->request->data['tblMailing']['m_schedule_date'] = $schedule_date;
						$this->request->data['tblMailing']['m_esp_id'] = $messageID['messageid'];
						$this->request->data['tblMailing']['m_screenshot'] = $imageURL;
						$this->tblMailing->create();
						if ($this->tblMailing->save($this->request->data))
						{
							$alloweduid = $this->Authake->getUserId() . "," . $mappedUnits['tblAdunit']['publisher_id'];
							$this->request->data['tblSocialMessage']['message'] = trim(stripslashes(htmlspecialchars("Succesfully Uploaded the Creative on Lyris, Message Id :" . $messageID['messageid'] . "<br /> <img src='".$imageURL."' />")));
							$this->request->data['tblSocialMessage']['uid_fk'] = $this->Authake->getUserId();
							$this->request->data['tblSocialMessage']['ip'] = $ip;
							$this->request->data['tblSocialMessage']['allowed_uids'] = $alloweduid;
							$this->request->data['tblSocialMessage']['created'] = $time;
							$this->tblSocialMessage->create();
							if($this->tblSocialMessage->save($this->request->data))
							{
								$this->set('msg_id', $this->tblSocialMessage->getLastInsertID());
								$email = $this->Authake->getUserEmail();
								$this->set('email', $email);
								$this->set('time', $time);
								$this->set('message', "Succesfully Uploaded the Creative on Lyris, Message Id :" . $messageID['messageid'] . "<br /> <img src='".$imageURL."' />");
								$username = $this->User->field('ContactName', array('id' => $this->Authake->getUserId()));
								$this->set('username', $username);
							}
						}
					}
				} else if(isset($mappedUnits['tblAdunit']['ad_esp_list_id']) && $mappedUnits['tblAdunit']['ad_esp'] == "2")
				{
					$messageID['messageid'] = 'DASSOCEX-';
					$this->request->data['tblMailing']['m_ad_uid'] = $mappedUnits['tblMappedAdunit']['ma_uid'];
					$this->request->data['tblMailing']['m_sl'] = $subject;
					$this->request->data['tblMailing']['m_esp_id'] = $messageID['messageid'];
					$this->request->data['tblMailing']['m_screenshot'] = $imageURL;
					$this->tblMailing->create();
					if ($this->tblMailing->save($this->request->data))
					{
						$mailingID = $this->tblMailing->getLastInsertID();
						$this->tblMailing->updateAll(
						array('tblMailing.m_esp_id' => 'DASSOCEX-'.$mailingID ),
						array('tblMailing.m_id' => $mailingID));
						
						$alloweduid = $this->Authake->getUserId() . "," . $mappedUnits['tblAdunit']['publisher_id'];
						$this->request->data['tblSocialMessage']['message'] = trim(stripslashes(htmlspecialchars("Succesfully Added the Message in System, Please use the Newsletter Name <b><u>DASSOCEX-". $mailingID ."</u></b> in SocketLab <br /> <img src='".$imageURL."' />")));
						$this->request->data['tblSocialMessage']['uid_fk'] = $this->Authake->getUserId();
						$this->request->data['tblSocialMessage']['ip'] = $ip;
						$this->request->data['tblSocialMessage']['allowed_uids'] = $alloweduid;
						$this->request->data['tblSocialMessage']['created'] = $time;
						$this->tblSocialMessage->create();
						if($this->tblSocialMessage->save($this->request->data))
						{
							
							$this->set('msg_id', $this->tblSocialMessage->getLastInsertID());
							$email = $this->Authake->getUserEmail();
							$this->set('email', $email);
							$this->set('time', $time);
							$this->set('message', "Succesfully Uploaded the Creative on Lyris, Message Id :" . $messageID['messageid'] . "<br /> <img src='".$imageURL."' />");
							$username = $this->User->field('ContactName', array('id' => $this->Authake->getUserId()));
							$this->set('username', $username);
						}
					}
				}  else if(isset($mappedUnits['tblAdunit']['ad_esp_list_id']) && $mappedUnits['tblAdunit']['ad_esp'] == "0")
				{
					$messageID['messageid'] = 'DASSOCEX-';
					$this->request->data['tblMailing']['m_ad_uid'] = $mappedUnits['tblMappedAdunit']['ma_uid'];
					$this->request->data['tblMailing']['m_sl'] = $subject;
					$this->request->data['tblMailing']['m_esp_id'] = '';
					$this->request->data['tblMailing']['m_screenshot'] = $imageURL;
					$this->tblMailing->create();
					if ($this->tblMailing->save($this->request->data))
					{	
						
						$alloweduid = $this->Authake->getUserId() . "," . $mappedUnits['tblAdunit']['publisher_id'];
						$this->request->data['tblSocialMessage']['message'] = trim(stripslashes(htmlspecialchars("Succesfully Added the Message in System, <br /> <img src='".$imageURL."' />")));
						$this->request->data['tblSocialMessage']['uid_fk'] = $this->Authake->getUserId();
						$this->request->data['tblSocialMessage']['ip'] = $ip;
						$this->request->data['tblSocialMessage']['allowed_uids'] = $alloweduid;
						$this->request->data['tblSocialMessage']['created'] = $time;
						$this->tblSocialMessage->create();
						if($this->tblSocialMessage->save($this->request->data))
						{
							
							$this->set('msg_id', $this->tblSocialMessage->getLastInsertID());
							$email = $this->Authake->getUserEmail();
							$this->set('email', $email);
							$this->set('time', $time);
							$this->set('message', "Succesfully Added the Message in System : <br /> <img src='".$imageURL."' />");
							$username = $this->User->field('ContactName', array('id' => $this->Authake->getUserId()));
							$this->set('username', $username);
						}
					}
				}
			}
			
			$option_order['joins'] = array(
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblOrder.advertiser_id = tblp.dfp_company_id',
			'tblOrder.owner_user_id' => array($this->Authake->getUserId()))
			));
			$option_order['fields'] = array('tblOrder.*', 'tblp.CompanyName', 'tblp.user_id');	
			$orders = $this->tblOrder->find('all', $option_order);
			$company_name = array();
			$count = 0;
			foreach($orders as $order)
			{
				//$company_name .=  "{ id: ".$order['tblp']['user_id'].", text: '".$order['tblp']['CompanyName']."' },";
				if (!$this->checkCompanyExist($company_name, 'user_id', $order['tblOrder']['advertiser_user_id'])) {
					$company_name[$count]['user_id'] = $order['tblOrder']['advertiser_user_id'];
					$company_name[$count]['CompanyName'] = $order['tblp']['CompanyName'];
					$count++;
				}
			}
			$this->set('orders', $orders);
			$this->set('company_name', $company_name);
			//pr($this->request->data);die;
		}
		
		function checkCompanyExist($array, $key, $val) {
			foreach ($array as $item)
			if (isset($item[$key]) && $item[$key] == $val)
			return true;
			return false;
		}
		
		
		function view_message_stats_download($ma_uid=null,$fl_id=null){
			$this->loadModel('Authake.tblFlight');
			$options['joins'] = array(array('table' => 'tbl_mapped_adunits',
						'alias' => 'tma',
						'type' => 'INNER',
						'conditions' => array(
						'tma.ma_uid = tblMailing.m_ad_uid')),
					array('table' => 'tbl_adunits',
						'alias' => 'tba', 
						'type' => 'INNER',
						'conditions' => array(
						'tba.ad_dfp_id = tma.ma_ad_id',
						'tba.owner_user_id' => array($this->Authake->getUserId())
					)),
					array('table'=>'authake_users',
						'alias'=>'User',
						'type'=>'INNER',
						'conditions'=>array(
						'tba.publisher_id = User.id')
					),
					array('table'=>'tbl_profiles',
						'alias'=>'tblProfile',
						'type'=>'LEFT',
						'conditions'=>array(
						'User.tblProfileID = tblProfile.profileID')
					),
					array('table'=>'tbl_creatives',
						'alias'=>'tblCreative',
						'type'=>'INNER',
						'conditions'=>array(
						'tblMailing.m_li_crtid = tblCreative.cr_id')
					
					),
					array('table'=>'tbl_mapped_adunits',
						'alias'=>'tblMappedAdunit',
						'type'=>'INNER',
						'conditions'=>array(
						'tba.ad_dfp_id = tblMappedAdunit.ma_ad_id')
					));
					
				 		
				$options['conditions'] = array( 'tblMailing.m_ad_uid'=>$ma_uid);	
				$options['fields'] = array('tba.ad_uid','tba.adunit_name','tblMailing.m_delivered','tblMailing.m_li_crtid','tblMailing.m_ad_uid','tblMailing.m_id','User.tblProfileID','tblProfile.tracksite_id','tblCreative.*','tblMappedAdunit.ma_l_id');
				$tblMailing = $this->tblMailing->find('first', $options);		
				$flightStartDate  = $this->tblFlight->find('first',array('fields'=>array('tblFlight.fl_start'),'conditions'=>array('tblFlight.fl_id'=>$fl_id)));		
				  
				if(empty($tblMailing['tblCreative']['cr_body'])){
					$this->Session->setFlash(__('Sorry, Something went wrong!'), 'error');
					if($this->referer() =='/')
					$this->redirect(array('controller'=>'messages','action' => 'index/all'));
					$this->redirect($this->referer());
					
			}
			
			
			$message =$tblMailing['tblCreative']['cr_body'];
			$content = explode("\n", $tblMailing['tblCreative']['cr_body']);
			
			if($tblMailing['tblCreative']['creatives_type']==2){
				$filename =$tblMailing['tba']['adunit_name'].'-'.$flightStartDate['tblFlight']['fl_start'].'.html';
			 }else{
				 $filename =$tblMailing['tba']['adunit_name'].'-'.$flightStartDate['tblFlight']['fl_start'].'.txt';
				 $content ="Links:- \n\n" ;
				 $links = (!empty($tblMailing['tblCreative']['link']))?json_decode($tblMailing['tblCreative']['link'],true):array();
				 if(!empty($links)){
					 $serial =1;
					foreach($links as $link){
						 $content =   $content.$serial++.". "."http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tblMailing['tblProfile']['tracksite_id']."&rec=1&c_n=creative_".$tblMailing['tblMailing']['m_li_crtid']."-mailing_".$tblMailing['tblMailing']['m_id']."&c_p=".$tblMailing['tblMailing']['m_ad_uid']."&c_i=lead&email=|EMAIL|&redirect=".urlencode($link). "\n\n";
						
					
					}
				 }	
				 
				  $content = $content ."\n\nImpression Link \n\nhttp://tracking.digitaladvertising.systems/piwik.php?idsite=".$tblMailing['tblProfile']['tracksite_id']."&rec=1&c_n=".$tblMailing['tblMappedAdunit']['ma_l_id']."&c_p=".$tblMailing['tblMailing']['m_ad_uid']."-".$tblMailing['tblMailing']['m_id'] ."&send_image=0";
			 }
		 	header("Content-type: text/plain");
			header("Content-Disposition: attachment; filename=$filename");
			
			$URLs = array();
			for($i=0;count($content)>$i;$i++){
				 
				 if(preg_match('/href=/', $content[$i])){
					list($Gone,$Keep) = explode("href=\"", trim($content[$i]));
					 
					list($Keep,$Gone) = explode("\"", $Keep);
					 
					$decodeurl = urlencode($Keep);
				   $impressionurl ="http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tblMailing['tblProfile']['tracksite_id']."&rec=1&c_n=creative_".$tblMailing['tblMailing']['m_li_crtid']."-mailing_".$tblMailing['tblMailing']['m_id']."&c_p=".$tblMailing['tblMailing']['m_ad_uid']."&c_i=lead&email=|EMAIL|&redirect=".$decodeurl;
					 $message= strtr($message, array( "$Keep" =>  $impressionurl, ));
				}
			}

			  if($tblMailing['tblCreative']['creatives_type']==2){
				   echo $message."<img src='http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tblMailing['tblProfile']['tracksite_id']."&rec=1&c_n=".$tblMailing['tblMappedAdunit']['ma_l_id']."&c_p=".$tblMailing['tblMailing']['m_ad_uid']."-".$tblMailing['tblMailing']['m_id'] ."&send_image=0' width='1' height='1'>";
					die;	
			}	
			echo $content;die;	
			 
						
		}
		function view_message_stats_views($ma_uid=null){
			$this->autoRender = false;
			$options['joins'] = array(

			array('table' => 'tbl_mapped_adunits',
						'alias' => 'tma',
						'type' => 'INNER',
						'conditions' => array(
						'tma.ma_uid = tblMailing.m_ad_uid')),
					array('table' => 'tbl_adunits',
						'alias' => 'tba', 
						'type' => 'INNER',
						'conditions' => array(
						'tba.ad_dfp_id = tma.ma_ad_id',
						'tba.owner_user_id' => array($this->Authake->getUserId())
					)),
					array('table'=>'authake_users',
						'alias'=>'User',
						'type'=>'INNER',
						'conditions'=>array(
						'tba.publisher_id = User.id')
					),
					array('table'=>'tbl_profiles',
						'alias'=>'tblProfile',
						'type'=>'LEFT',
						'conditions'=>array(
						'User.tblProfileID = tblProfile.profileID')
					),
					array('table'=>'tbl_creatives',
						'alias'=>'tblCreative',
						'type'=>'INNER',
						'conditions'=>array(
						'tblMailing.m_li_crtid = tblCreative.cr_id')
					
					),
					array('table'=>'tbl_mapped_adunits',
						'alias'=>'tblMappedAdunit',
						'type'=>'INNER',
						'conditions'=>array(
						'tba.ad_dfp_id = tblMappedAdunit.ma_ad_id')
					));
					
				 		
				$options['conditions'] = array( 'tblMailing.m_ad_uid'=>$ma_uid);	
				$options['fields'] = array('tba.ad_uid','tba.adunit_name','tblMailing.m_delivered','tblMailing.m_li_crtid','tblMailing.m_ad_uid','tblMailing.m_id','User.tblProfileID','tblProfile.tracksite_id','tblCreative.*','tblMappedAdunit.ma_l_id');
				$tblMailing = $this->tblMailing->find('first', $options);		
				//pr($tblMailing);die; 
				if(empty($tblMailing['tblCreative']['cr_body'])){
					$this->Session->setFlash(__('Sorry, Something went wrong!'), 'error');
					if($this->referer() =='/')
					$this->redirect(array('controller'=>'messages','action' => 'index/all'));
					$this->redirect($this->referer());
					
			}
			$message =$tblMailing['tblCreative']['cr_body'];
			$content = explode("\n", $tblMailing['tblCreative']['cr_body']);
			
			$URLs = array();
			for($i=0;count($content)>$i;$i++){
				 
				 if(preg_match('/href=/', $content[$i])){
					list($Gone,$Keep) = explode("href=\"", trim($content[$i]));
					 
					list($Keep,$Gone) = explode("\"", $Keep);
					 
					$decodeurl = urlencode($Keep);
				   $impressionurl ="http://tracking.digitaladvertising.systems/leadTrack.php?idsite=".$tblMailing['tblProfile']['tracksite_id']."&rec=1&c_n=creative_".$tblMailing['tblMailing']['m_li_crtid']."-mailing_".$tblMailing['tblMailing']['m_id']."&c_p=".$tblMailing['tblMailing']['m_ad_uid']."&c_i=lead&email=|EMAIL|&redirect=".$decodeurl;
					 $message= strtr($message, array( "$Keep" =>  $impressionurl, ));
				}
			}

			   echo $message."<img src='http://tracking.digitaladvertising.systems/piwik.php?idsite=".$tblMailing['tblProfile']['tracksite_id']."&rec=1&c_n=".$tblMailing['tblMappedAdunit']['ma_l_id']."&c_p=".$tblMailing['tblMailing']['m_ad_uid']."-".$tblMailing['tblMailing']['m_id'] ."&send_image=0' width='1' height='1'>";
			 			
			 
						
		}
					
}							
