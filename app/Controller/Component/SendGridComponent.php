<?php

/**
 * Implementation of sendgrid with DAS
 * Sendgrid is platfor of Delivering your transactional and marketing email through one reliable platform.
 * @author Mohsin Kabir <mohsin.kabir@.siliconbiztech.com>
 * @tutorial package https://github.com/sendgrid/sendgrid-php
 * 
 */
App::import('Vendor', 'sendgrid', array('file' => '/sendgrid/sendgrid-php.php'));

class SendGridComponent extends Component {

    public function initialize(Controller $controller) {
        
    }

    /**
     * 
     * @param type $to array of emails
     * @param type $from 
     * @param type $subject
     * @param type $body
     * @param type $attachment
     */
    public function send($to, $from, $subject, $body, $file = array()) {
        $this->api_key = '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';
        $this->sg = new SendGrid($this->api_key);
        $from = new SendGrid\Email(null, $from);
        foreach ($to as $email) {
            $to = new SendGrid\Email(null, $email);
        }
        $content = new SendGrid\Content("text/html", $body);
        if (!empty($file)) {
            $attachment = new SendGrid\Attachment();
            $attachment->setContent(base64_encode(file_get_contents($file['path'])));
            $attachment->setType($file['type']);
            $attachment->setFilename($file['name']);
            $attachment->setDisposition("attachment");
            $attachment->setContentId($file['content_id']);
        }
        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        $mail->addAttachment($attachment);
        $response = $this->sg->client->mail()->send()->post($mail);
    }

}
