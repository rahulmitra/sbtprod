<?php

App::uses('Component', 'Controller');

class ReportingComponent extends Component {

    /**
     * description : 
     * on which pages it is used 
     * @todo - if any 
     * @author Niraj 
     * @created date 17-08-16
     * @param type $order_id  integers
     * @param type $li_dfp_id integers
     * @param type $range array
     * @param type $chartdays integers
     * @param type $AuthakeUserId  used for login user id
     * @return type array 
     */
    Public function tblReportByOrder($order_id = null, $li_dfp_id = null, $range = array(), $chartdays = 30, $AuthakeUserId = null) {
        //  here chartdays defaults is 30 days
        //get dyamic order 
        $OrderLeads = ClassRegistry::init('OrderLeads');
        $tblReportDay = ClassRegistry::init('Authake.tblReportDay');
        $OrderLeads->useTable = $tablename = "order_" . $order_id;
        try {
			if (!empty($range)) {
			$end_date2 =	$range['end_date'];
			$start_date2 = $range['start_date'];
			}
		
            $OrderLeads->virtualFields = array('totaltake' => "SELECT COUNT(*) FROM " . $OrderLeads->useTable . "  WHERE od_lid = $li_dfp_id",
                'totalAccepted' => "SELECT COUNT(*) FROM " . $OrderLeads->useTable . " WHERE od_lid = $li_dfp_id and od_isRejected!=1",
                'totalRejected' => "SELECT COUNT(*) FROM " . $OrderLeads->useTable . " WHERE od_lid = $li_dfp_id and od_isRejected = 1"
            );
            $ordersLeads = $OrderLeads->find('first', array('fields' => array('totaltake', 'totalAccepted', 'totalRejected')));
        } catch (Exception $e) {
            $e->getMessage();
        }

        // if there not found in sum 
        $ordersLeads['OrderLeads']['totaltake'] = (!empty($ordersLeads['OrderLeads']['totaltake'])) ? $ordersLeads['OrderLeads']['totaltake'] : 0;
        $ordersLeads['OrderLeads']['totalAccepted'] = (!empty($ordersLeads['OrderLeads']['totalAccepted'])) ? $ordersLeads['OrderLeads']['totalAccepted'] : 0;
        $ordersLeads['OrderLeads']['totalRejected'] = (!empty($ordersLeads['OrderLeads']['totalRejected'])) ? $ordersLeads['OrderLeads']['totalRejected'] : 0;
        $criteria['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.ad_dfp_id = tblReportDay.rd_ad_unit_id',
                    'tba.owner_user_id' => array($AuthakeUserId)
        )));

        $criteria['conditions'][] = array('tblReportDay.rd_line_item_id ' => $li_dfp_id);
        if (!empty($range)) {
            $criteria['conditions'][] = array('and' => array(
                    array('tblReportDay.rd_date <= ' => $range['end_date'],
                        'tblReportDay.rd_date >= ' => $range['start_date'],
            )));
        }
        $tblReportDay->virtualFields = array(
            "rd_imp" => "COALESCE(SUM(tblReportDay.rd_imp),0)",
            "rd_delivered" => "COALESCE(SUM(tblReportDay.rd_delivered),0)",
            "rd_accepted" => "COALESCE(SUM(tblReportDay.rd_accepted),0)",
            "rd_revenue" => "COALESCE(SUM(tblReportDay.rd_revenue),0)",
            "rd_leads" => "COALESCE(SUM(tblReportDay.rd_leads),0)"
        );
        $criteria['fields'] = array('rd_imp', 'rd_delivered', 'rd_accepted', 'rd_revenue', 'rd_leads');
        $data = $tblReportDay->find('first', $criteria);
        $tblReportDay->virtualFields = array();
        $end_date = date('Y-m-d');
        $start_date = date('Y-m-d', strtotime('-' . $chartdays . ' days'));
        // for take graph
        $data['tblReportDay']['takegraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_line_item_id' => $li_dfp_id, 'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_id', 'tblReportDay.rd_leads'), 'group' => array('tblReportDay.rd_date')));
        // for impression graph

        $data['tblReportDay']['impgraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_line_item_id' => $li_dfp_id, 'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_id', 'tblReportDay.rd_revenue'), 'group' => array('tblReportDay.rd_date')));
        // for impression graph
        $tblReportDay->virtualFields = array('eCPM' => '(tblReportDay.rd_revenue*1000)/tblReportDay.rd_imp');
        $data['tblReportDay']['eCPMgraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_line_item_id' => $li_dfp_id, 'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_id', 'eCPM'), 'group' => array('tblReportDay.rd_date')));
        $data['tblReportDay']['totaltake'] = $ordersLeads['OrderLeads']['totaltake'];
        $data['tblReportDay']['totalAccepted'] = $ordersLeads['OrderLeads']['totalAccepted'];
        $data['tblReportDay']['totalRejected'] = $ordersLeads['OrderLeads']['totalRejected'];
        $data['tblReportDay']['totalPending'] = ($ordersLeads['OrderLeads']['totaltake'] - ($ordersLeads['OrderLeads']['totalAccepted'] + $ordersLeads['OrderLeads']['totalRejected']));
        return $data['tblReportDay'];
    }
	
	
	
	 Public function tblReportByOrderAddunits($ad_dfp_id = null, $range = array(), $chartdays = 30, $costid = 0, $AuthakeUserId = null) {
        //  here chartdays defaults is 30 days
        //get dyamic order 
		$chartdays2 = 30;
        $tblReportDay = ClassRegistry::init('Authake.tblReportDay');
        try {
		
		$chartdays2 = 30;
		if (!empty($range)) {
		$end_date2 =	$range['end_date'];
        $start_date2 = $range['start_date'];
        }
		else{
		$end_date2 = date('Y-m-d H:i:s');
		$start_date2 = date('Y-m-d H:i:s', strtotime('-' . $chartdays2 . ' days'));
		}
        

		$criteria['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.ad_dfp_id = tblReportDay.rd_ad_unit_id',
                    'tba.publisher_id' => array($AuthakeUserId)
        )),
		  array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblReportDay.rd_line_item_id = tblLineItem.li_dfp_id'
                )
            ),
			
			array('table' => 'tbl_cost_structures',
                'alias' => 'tcs',
                'type' => 'LEFT',
                'conditions' => array(
                    'tcs.cs_id = tblLineItem.li_cs_id'
                )
            )
		
		);

        $criteria['conditions'][] = array('tblReportDay.rd_ad_unit_id' => $ad_dfp_id);
      
            $criteria['conditions'][] = array('and' => array(
                    array('tblReportDay.rd_date <= ' => $end_date2 ,
                        'tblReportDay.rd_date >= ' => $start_date2 ,
            )));
      
		// else{
		//$end_date2 = date('Y-m-d H:i:s');
       //$start_date2 = date('Y-m-d H:i:s', strtotime('-' . $chartdays2 . ' days'));
		
		// $criteria['conditions'][] = array('and' => array(
                   // array('tblReportDay.rd_date <= ' => $end_date2,
                        //'tblReportDay.rd_date >= ' => $start_date2,
            //)));
		//}
		if($costid > 0)  {
			$criteria['conditions'][] = array('tcs.cs_id' => $costid);
			}
		
        $tblReportDay->virtualFields = array(
            "rd_imp" => "COALESCE(SUM(tblReportDay.rd_imp),0)",
            "rd_delivered" => "COALESCE(SUM(tblReportDay.rd_delivered),0)",
            "rd_accepted" => "COALESCE(SUM(tblReportDay.rd_accepted),0)",
            "rd_revenue" => "COALESCE(SUM(tblReportDay.rd_revenue),0)",
            "rd_leads" => "COALESCE(SUM(tblReportDay.rd_leads),0)"
        );
        $criteria['fields'] = array('rd_imp', 'rd_delivered', 'rd_accepted', 'rd_revenue', 'rd_leads');
        $data = $tblReportDay->find('first', $criteria);
        $tblReportDay->virtualFields = array(
            "rd_leads" => "COALESCE(SUM(tblReportDay.rd_leads),0)"
		);
        $end_date = date('Y-m-d');
        $start_date = date('Y-m-d', strtotime('-' . $chartdays . ' days'));
        // for take graph
        $data['tblReportDay']['takegraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_ad_unit_id' => $ad_dfp_id, 'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_date', 'rd_leads'), 'group' => array('tblReportDay.rd_date')));
        
		// for impression graph
		$tblReportDay->virtualFields = array(
		 "rd_imp" => "COALESCE(SUM(tblReportDay.rd_imp),0)",
		  "rd_revenue" => "COALESCE(SUM(tblReportDay.rd_revenue),0)"
		);
		
        $data['tblReportDay']['impgraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_ad_unit_id' => $ad_dfp_id, 'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_date', 'rd_imp'), 'group' => array('tblReportDay.rd_date')));
       
		// for impression graph
        $tblReportDay->virtualFields = array('eCPM' => '(tblReportDay.rd_revenue*1000)/tblReportDay.rd_imp');
        $data['tblReportDay']['eCPMgraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_ad_unit_id' => $ad_dfp_id, 'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_id', 'eCPM'), 'group' => array('tblReportDay.rd_date')));
        
		//$data['tblReportDay']['startdate'] =  $start_date2;
		//$data['tblReportDay']['Endtdate'] =  $end_date2;
		} catch (Exception $e) {
            $e->getMessage();
        }
        return $data['tblReportDay'];
    }


    /**
     * description : 
     * on which pages it is used 
     * @todo - if any 
     * @author Niraj 
     * @created date 17-08-16
     * @param type $order_id  integers
     * @param type $li_dfp_id integers
     * @param type $range array
     * @param type $chartdays integers
     * @param type $AuthakeUserId  used for login user id
     * @return type array 
     */
    Public function flight_report($ad_id=null,$order_id = null, $li_dfp_id = null, $fl_id,$range = array(), $chartdays = 30, $AuthakeUserId = null) {
        //  here chartdays defaults is 30 days
        //get dyamic order 
       // $OrderLeads = ClassRegistry::init('OrderLeads');
        $tblReportDay = ClassRegistry::init('Authake.tblReportDay');
        $flightCreateObj= ClassRegistry::init('FlightsCreative');
        $creatives=$flightCreateObj->find('all',array('conditions'=>array('fl_id'=>$fl_id)));
        $createIds=array();
        foreach($creatives as $creative){
            $createIds[]= $creative['FlightsCreative']['cr_id'];
        }
		
		if (!empty($range)) {
		$end_date2 =	$range['end_date'];
        $start_date2 = $range['start_date'];
        }
		
		
//        $OrderLeads->useTable = $tablename = "order_" . $order_id;
//        try {
//            $OrderLeads->virtualFields = array('totaltake' => "SELECT COUNT(*) FROM " . $OrderLeads->useTable . "  WHERE od_lid = $li_dfp_id",
//                'totalAccepted' => "SELECT COUNT(*) FROM " . $OrderLeads->useTable . " WHERE od_lid = $li_dfp_id and od_isRejected!=1",
//                'totalRejected' => "SELECT COUNT(*) FROM " . $OrderLeads->useTable . " WHERE od_lid = $li_dfp_id and od_isRejected = 1"
//            );
//            $ordersLeads = $OrderLeads->find('first', array('fields' => array('totaltake', 'totalAccepted', 'totalRejected')));
//        } catch (Exception $e) {
//            $e->getMessage();
//        }

        // if there not found in sum 
       // $ordersLeads['OrderLeads']['totaltake'] = (!empty($ordersLeads['OrderLeads']['totaltake'])) ? $ordersLeads['OrderLeads']['totaltake'] : 0;
       // $ordersLeads['OrderLeads']['totalAccepted'] = (!empty($ordersLeads['OrderLeads']['totalAccepted'])) ? $ordersLeads['OrderLeads']['totalAccepted'] : 0;
      //  $ordersLeads['OrderLeads']['totalRejected'] = (!empty($ordersLeads['OrderLeads']['totalRejected'])) ? $ordersLeads['OrderLeads']['totalRejected'] : 0;
        $criteria['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.ad_dfp_id = tblReportDay.rd_ad_unit_id',
                    'tba.owner_user_id' => array($AuthakeUserId)
        )));

        $criteria['conditions'][] = array('tblReportDay.rd_line_item_id' => $li_dfp_id,'tblReportDay.rd_ad_unit_id ' => $ad_id,'tblReportDay.rd_crt_id'=>$createIds);
        if (!empty($range)) {
            $criteria['conditions'][] = array('and' => array(
                    array('tblReportDay.rd_date <= ' => $range['end_date'],
                        'tblReportDay.rd_date >= ' => $range['start_date'],
            )));
        }
        $tblReportDay->virtualFields = array(
            "rd_imp" => "COALESCE(SUM(tblReportDay.rd_imp),0)",
            "rd_delivered" => "COALESCE(SUM(tblReportDay.rd_delivered),0)",
            "rd_accepted" => "COALESCE(SUM(tblReportDay.rd_accepted),0)",
            "rd_revenue" => "COALESCE(SUM(tblReportDay.rd_revenue),0)",
            "rd_leads" => "COALESCE(SUM(tblReportDay.rd_leads),0)"
           // "rd_ar_rejects" => "COALESCE(SUM(tblReportDay.rd_ar_rejects),0)"
        );
        $criteria['fields'] = array('rd_imp', 'rd_delivered', 'rd_accepted', 'rd_revenue', 'rd_leads');
        $data = $tblReportDay->find('first', $criteria);
        //pr($tblReportDay->getDataSource()->getLog(false,false));
        $tblReportDay->virtualFields = array();
        $end_date = date('Y-m-d');
        $start_date = date('Y-m-d', strtotime('-' . $chartdays . ' days'));
        // for take graph
        $data['tblReportDay']['takegraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_line_item_id' => $li_dfp_id,'tblReportDay.rd_crt_id'=>$createIds, 'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_id', 'tblReportDay.rd_leads'), 'group' => array('tblReportDay.rd_date')));
        // for impression graph

        $data['tblReportDay']['impgraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_line_item_id' => $li_dfp_id, 'tblReportDay.rd_crt_id'=>$createIds,'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_id', 'tblReportDay.rd_revenue'), 'group' => array('tblReportDay.rd_date')));
        // for impression graph
        $tblReportDay->virtualFields = array('eCPM' => '(tblReportDay.rd_revenue*1000)/tblReportDay.rd_imp');
        $data['tblReportDay']['eCPMgraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_line_item_id' => $li_dfp_id,'tblReportDay.rd_crt_id'=>$createIds, 'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_id', 'eCPM'), 'group' => array('tblReportDay.rd_date')));
       // $data['tblReportDay']['totaltake'] = $ordersLeads['OrderLeads']['totaltake'];
     //   $data['tblReportDay']['totalAccepted'] = $ordersLeads['OrderLeads']['totalAccepted'];
     //   $data['tblReportDay']['totalRejected'] = $ordersLeads['OrderLeads']['totalRejected'];
     //   $data['tblReportDay']['totalPending'] = ($ordersLeads['OrderLeads']['totaltake'] - ($ordersLeads['OrderLeads']['totalAccepted'] + $ordersLeads['OrderLeads']['totalRejected']));
       // pr($data);
	   //print_r('<pre>');print_r($data['tblReportDay']);print_r('</pre>');
        return $data['tblReportDay'];
    }
	
	
	
	 Public function view_report($ad_id=null,$order_id = null, $li_dfp_id = null, $fl_id,$range = array(), $chartdays = 30, $AuthakeUserId = null) {
       
  
        $tblReportDay = ClassRegistry::init('Authake.tblReportDay');
        $flightCreateObj= ClassRegistry::init('FlightsCreative');
        $creatives=$flightCreateObj->find('all',array('conditions'=>array('fl_id'=>$fl_id)));
        $createIds=array();
        foreach($creatives as $creative){
            $createIds[]= $creative['FlightsCreative']['cr_id'];
        }
		$chartdays2 = 30;
		if (!empty($range)) {
		$end_date2 =	$range['end_date'];
        $start_date2 = $range['start_date'];
        }
		else{
		$end_date2 = date('Y-m-d H:i:s');
		$start_date2 = date('Y-m-d H:i:s', strtotime('-' . $chartdays2 . ' days'));
		}

        $criteria['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'INNER',
                'conditions' => array(
                    'tba.ad_dfp_id = tblReportDay.rd_ad_unit_id',
                    'tba.publisher_id' => array($AuthakeUserId)
        )));

        $criteria['conditions'][] = array('tblReportDay.rd_line_item_id' => $li_dfp_id,'tblReportDay.rd_ad_unit_id ' => $ad_id,'tblReportDay.rd_crt_id'=>$createIds);

            $criteria['conditions'][] = array('and' => array(
                    array('tblReportDay.rd_date <= ' => $end_date2,
                        'tblReportDay.rd_date >= ' => $start_date2,
            )));
   
        $tblReportDay->virtualFields = array(
            "rd_imp" => "COALESCE(SUM(tblReportDay.rd_imp),0)",
            "rd_delivered" => "COALESCE(SUM(tblReportDay.rd_delivered),0)",
            "rd_accepted" => "COALESCE(SUM(tblReportDay.rd_accepted),0)",
            "rd_revenue" => "COALESCE(SUM(tblReportDay.rd_revenue),0)",
            "rd_leads" => "COALESCE(SUM(tblReportDay.rd_leads),0)"
           // "rd_ar_rejects" => "COALESCE(SUM(tblReportDay.rd_ar_rejects),0)"
        );
        $criteria['fields'] = array('rd_imp', 'rd_delivered', 'rd_accepted', 'rd_revenue', 'rd_leads');
        $data = $tblReportDay->find('first', $criteria);
        //pr($tblReportDay->getDataSource()->getLog(false,false));
        $tblReportDay->virtualFields = array();
        $end_date = date('Y-m-d');
        $start_date = date('Y-m-d', strtotime('-' . $chartdays . ' days'));
        // for take graph
        $data['tblReportDay']['takegraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_line_item_id' => $li_dfp_id,'tblReportDay.rd_crt_id'=>$createIds, 'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_id', 'tblReportDay.rd_leads'), 'group' => array('tblReportDay.rd_date')));
        // for impression graph

        $data['tblReportDay']['impgraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_line_item_id' => $li_dfp_id, 'tblReportDay.rd_crt_id'=>$createIds,'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_id', 'tblReportDay.rd_revenue'), 'group' => array('tblReportDay.rd_date')));
        // for impression graph
        $tblReportDay->virtualFields = array('eCPM' => '(tblReportDay.rd_revenue*1000)/tblReportDay.rd_imp');
        $data['tblReportDay']['eCPMgraph'] = $tblReportDay->find('list', array('conditions' => array('tblReportDay.rd_line_item_id' => $li_dfp_id,'tblReportDay.rd_crt_id'=>$createIds, 'date(tblReportDay.rd_date)  between ? and ?' => array($start_date, $end_date)), 'fields' => array('tblReportDay.rd_id', 'eCPM'), 'group' => array('tblReportDay.rd_date')));
       // $data['tblReportDay']['totaltake'] = $ordersLeads['OrderLeads']['totaltake'];
     //   $data['tblReportDay']['totalAccepted'] = $ordersLeads['OrderLeads']['totalAccepted'];
     //   $data['tblReportDay']['totalRejected'] = $ordersLeads['OrderLeads']['totalRejected'];
     //   $data['tblReportDay']['totalPending'] = ($ordersLeads['OrderLeads']['totaltake'] - ($ordersLeads['OrderLeads']['totalAccepted'] + $ordersLeads['OrderLeads']['totalRejected']));
       // pr($data);
	   //print_r('<pre>');print_r($data['tblReportDay']);print_r('</pre>');
        return $data['tblReportDay'];
    }
	
	 Public function realtime_stats($order_id = null, $ma_uid = null, $range) {
	 
	     $OrderLeads = ClassRegistry::init('OrderLeads');
      
        $OrderLeads->useTable = $tablename = "order_" . $order_id;
        try {
            $OrderLeads->virtualFields = array('totaltake' => "SELECT COUNT(*) FROM " . $OrderLeads->useTable . "  WHERE od_map_id = '$ma_uid' and date(od_create) between  '" .$range['start_date']. "' and '" .$range['end_date']."' ",
                'totalAccepted' => "SELECT COUNT(*) FROM " . $OrderLeads->useTable . " WHERE od_map_id = '$ma_uid' and date(od_create) between '" .$range['start_date']. "' and '" .$range['end_date']. "' and od_isRejected!=1",
                'totalRejected' => "SELECT COUNT(*) FROM " . $OrderLeads->useTable . " WHERE od_map_id = '$ma_uid' and date(od_create) between '" .$range['start_date']. "' and '" .$range['end_date']. "' and od_isRejected = 1"
            );
			//print_r($OrderLeads);
            $ordersLeads = $OrderLeads->find('first', array('fields' => array('totaltake', 'totalAccepted', 'totalRejected')));
        } catch (Exception $e) {
            $e->getMessage();
        }

        // if there not found in sum 
        $ordersLeads['OrderLeads']['totaltake'] = (!empty($ordersLeads['OrderLeads']['totaltake'])) ? $ordersLeads['OrderLeads']['totaltake'] : 0;
        $ordersLeads['OrderLeads']['totalAccepted'] = (!empty($ordersLeads['OrderLeads']['totalAccepted'])) ? $ordersLeads['OrderLeads']['totalAccepted'] : 0;
        $ordersLeads['OrderLeads']['totalRejected'] = (!empty($ordersLeads['OrderLeads']['totalRejected'])) ? $ordersLeads['OrderLeads']['totalRejected'] : 0;
	  return $ordersLeads['OrderLeads'] ;
	 
	 }
	

}

?>
