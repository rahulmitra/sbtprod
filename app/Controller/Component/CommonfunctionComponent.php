<?php
class CommonfunctionComponent extends Component {

  /**
   * Constructor
   * saves the controller reference for later use
   * @param ComponentCollection $collection A ComponentCollection this component can use to lazy load its components
   * @param array $settings Array of configuration settings.
   */
	function getDuration($duration =null){
		switch($duration)
			{
				case "day" :
				return  $this->rangeDay(date("Y-m-d"));
				 
				case "week" :
				return  $this->rangeWeek(date("Y-m-d"));
				 
				case "month" :
				return  $this->rangeMonth(date("Y-m-d"));
				 
				case "year" :
				return $this->rangeYear(date("Y-m-d"));
				 
				default :
				return  $this->rangeWeek(date("Y-m-d"));
				 
			}
	}
	function rangeMonth($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('first day of this month', $dt));
			$res['end'] = date('Y-m-d', strtotime('last day of this month', $dt));
			return $res;
		}
		
		function rangeWeek($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('-7 Days', $dt));
			$res['end'] = $datestr;
			return $res;
		}
		
		function rangeYear($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('-365 Days', $dt));
			//$res['start'] = date('Y-m-d', strtotime(date('Y-01-01')));
			$res['end'] = $datestr;
			return $res;
		}
		
		function rangeDay($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = $datestr;
			$res['end'] = $datestr;
			return $res;
		}
		// currently used for ordercontroller
		
		function datediff($start_date=null ,$end_date=null){
			 
			$li_start_date = new DateTime($start_date);
			$li_end_date = new DateTime($end_date);
			$day=  $li_end_date->diff($li_start_date)->format("%a");
			return ($day==0)?1:$day;
		}
		function datepickerFormat($date = null){
			
			
			if(!empty($date)){
				$date_explode = explode('/',$date);
				$date =  $date_explode[2] . "-". $date_explode[0]."-".$date_explode[1];
				if(strtotime($date) == strtotime(date('Y-m-d'))){
					return date('Y-m-d',strtotime("+1 day"));
				}
				return $date; 
			}else{
				return date( 'm-d-Y');
			}
		}
		function edit_datepickerFormat($date = null){
			
			
			if(!empty($date)){
				$date_explode = explode('/',$date);
				return trim($date_explode[2]) . "-". trim($date_explode[0])."-".trim($date_explode[1]);
				  
			}else{
				return date( 'm-d-Y');
			}
		}
		
		function piwikReport($mappedUnits= array(),$range= array()){
			
			$country = array();
			$DeviceType = array();
			$browsers = array();
			$osfamily = array();
			App::import('Vendor', 'src/Piwik'); 
			foreach ($mappedUnits as $item) {
				try {
					$piwik = new Piwik(Configure::read('Piwik.url'), Configure::read('Piwik.token'), $item['tblp']['tracksite_id'], Piwik::FORMAT_JSON);
					$piwik->setPeriod(Piwik::PERIOD_RANGE);
					$piwik->setRange($range['start'], $range['end']);
					$track_code = $item['tblMappedAdunit']['ma_uid'];
					//echo $item['tblMappedAdunit']['ma_uid'].",";
					$getCountries = $piwik->getCountry("contentPiece==".$track_code, array('label,nb_visits'));
					if(!empty($getCountries))
					foreach($getCountries as $getCountry) {
						if (array_key_exists($getCountry->label, $country)) {
							$country[$getCountry->label] += $getCountry->nb_visits;
							} else {
							$country[$getCountry->label] = $getCountry->nb_visits;
						}
					}
					
					
					$getDeviceType = $piwik->getDeviceType("contentPiece==".$track_code, array('label,nb_visits'));
					if(!empty($getDeviceType))
					foreach($getDeviceType as $getDevice) {
						if (array_key_exists($getDevice->label, $DeviceType)) {
							$DeviceType[$getDevice->label] += $getDevice->nb_visits;
							} else {
							$DeviceType[$getDevice->label] = $getDevice->nb_visits;
						}
					}
					
					$getBrowsers = $piwik->getBrowsers("contentPiece==".$track_code, array('label,nb_visits'));
					if(!empty($getBrowsers))
					foreach($getBrowsers as $getBrowser) {
						if (array_key_exists($getBrowser->label, $browsers)) {
							$browsers[$getBrowser->label] += $getBrowser->nb_visits;
							} else {
							$browsers[$getBrowser->label] = $getBrowser->nb_visits;
						}
					}
					
					$getOSFamilies = $piwik->getOSFamilies("contentPiece==".$track_code, array('label,nb_visits'));
					if(!empty($getOSFamilies))
					foreach($getOSFamilies as $getOSFamily) {
						if (array_key_exists($getOSFamily->label, $osfamily)) {
							$osfamily[$getOSFamily->label] += $getOSFamily->nb_visits;
							} else {
							$osfamily[$getOSFamily->label] = $getOSFamily->nb_visits;
						}
					}
					
					} catch (Exception $ex){
					
				}
				
			}
			return array('country'=>$country,'DeviceType'=>$DeviceType,'browsers'=>$browsers,'osfamily'=>$osfamily);
			
		}
		function removespecail_text($input= null){
			return  preg_replace("/[^a-zA-Z]+/", "", $input);
		}
}
