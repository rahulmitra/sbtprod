<?php
/** Chart component  
 * @author Niraj on 19-08-16
 * @created date 17-08-16
 * 
 */
App::uses('Component', 'Controller');
class ChartComponent extends Component{
	 /**
     * description : 
     * on which pages it is used 
     * @todo - if any 
     * @author Niraj 
     * @created date 19-08-16
     * @param type $nl_adunit_uuid  integers
     * @param type $DateRange array
     * @param type $duration eg:day,week,month,year
     * @param type $AuthakeUserId  used for login user id
     * @return type array 
     */
	 
	 
     public function GrossRevenueTable($nl_adunit_uuid=null, $DateRange=array(), $duration='month',$AuthakeUserId=null) {
        $tblReportDay = ClassRegistry::init('Authake.tblReportDay');
        $tblFlight = ClassRegistry::init('Authake.tblFlight');
        $options2['joins'] = $options['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblReportDay.rd_ad_unit_id = tblAdunit.ad_dfp_id')
        ));
        if ($duration == 'year') {
            ///$options['group'] = array('month');
            $tblReportDay->virtualFields = array("total_rd_revenue" => "sum(tblReportDay.rd_revenue)", "dateformate" => "DATE_FORMAT(tblReportDay.rd_date,' %b %Y')");
        } else {
            /// $options['group'] = array('dateformate');
            $tblReportDay->virtualFields = array("total_rd_revenue" => "sum(tblReportDay.rd_revenue)", "dateformate" => "DATE_FORMAT(tblReportDay.rd_date,'%m/%d/%Y')");
        }
        $options['conditions'] = array('tblAdunit.ad_uid' => $nl_adunit_uuid, 'DATE(tblReportDay.rd_date) between ? and ?' => array($DateRange['start'], $DateRange['end'])); //'tblAdunit.ad_isactive' =>1);//,'tblMailing.status' =>1);
        $options2['conditions'] = array('tblAdunit.ad_uid' => $nl_adunit_uuid, 'DATE(tblReportDay.rd_date) between ? and ?' => array(date('Y-m-d'), date('Y-m-d'))); //'tblAdunit.ad_isactive' =>1);//,'tblMailing.status' =>1);

        $options2['fields'] = $options['fields'] = array('tblReportDay.total_rd_revenue'); //,'tblMailing.status' =>1);
        $tableRecords = array();
        $tablefield1 = $tblReportDay->find('first', $options);
        $tablefield2 = $tblReportDay->find('first', $options2);
        $tableRecords['total_rd_revenue'] = ($tablefield1['tblReportDay']['total_rd_revenue']) ? $tablefield1['tblReportDay']['total_rd_revenue'] : 0;
        $tableRecords['today_total_earnings'] = ($tablefield2['tblReportDay']['total_rd_revenue']) ? $tablefield2['tblReportDay']['total_rd_revenue'] : 0;
        $tableRecords['TotalLiveCampaignThisMonth'] = $tblFlight->totalLiveCampaignThisMonth($nl_adunit_uuid, $DateRange, $duration);
		
        $tableRecords['TotalMTD'] = self::MTD($nl_adunit_uuid, $DateRange, $duration);
        $tableRecords['TotaleCPM'] = $tblReportDay->eCPM($nl_adunit_uuid, $DateRange, $duration, $AuthakeUserId);
				
        return $tableRecords;
    }
    
     private function MTD($nl_adunit_uuid, $DateRange, $duration) {
		$tblAdunit = ClassRegistry::init('Authake.tblAdunit');
		$Dynamic = ClassRegistry::init('Dynamiccharts');
        $tblSubscribers = array();
        $table_name = $tblAdunit->getEmailtableName($nl_adunit_uuid); // get here talbe name of das email
        $Dynamic->createAdUnitDbTable($table_name); // Crerate Dynamic Db Table for adunit 
        if (!empty($table_name)) {
            $Dynamic->useTable = $table_name;
            $where = $fields = $groupby = '';
            if (!empty($DateRange)) {
                $where = ' and date(nl_create) BETWEEN "' . $DateRange['start'] . '" AND "' . $DateRange['end'] . '"' . $groupby;
            }
            $tblSubscribers = $Dynamic->Query("SELECT count('nl_id') as totalemail FROM " . $table_name . " WHERE nl_isActive=1" . $where);
            return (!empty($tblSubscribers)) ? $tblSubscribers[0][0]['totalemail'] : 0;
        }
        return 0;
    }
   /* Daily_New_Subscribers 
    * * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $AuthakeUserId  used for login user id
     * @return type
     */
    public function Daily_New_Subscribers($nl_adunit_uuid = null, $DateRange = array(), $duration = 'month',$AuthakeUserId=null) {
        $tblAdunit = ClassRegistry::init('Authake.tblAdunit');
        $Dynamic = ClassRegistry::init('Dynamiccharts');
        
        $table_name = $tblAdunit->getEmailtableName($nl_adunit_uuid); // get here talbe name of das email
        $tblSubscribers = array();
        if (!empty($table_name)) {
            $Dynamic->useTable = $table_name;
            $where = $fields = $groupby = '';
            if ($duration == 'year') {
                $groupby = ' group by month';
                $fields = "DATE_FORMAT(nl_create,'%b %Y') as dateformate ";
            } else {
                $groupby = ' group by date';
                $fields = "DATE_FORMAT(nl_create,'%m/%d/%Y') as dateformate ";
            }
            if (!empty($DateRange)) {
                $where = ' and date(nl_create) BETWEEN "' . $DateRange['start'] . '" AND "' . $DateRange['end'] . '"' . $groupby;
            }
            $tblSubscribers = $Dynamic->Query("SELECT count('nl_id') as totalemail,$fields ,month(nl_create) as month,date(nl_create) as date  FROM " . $table_name . " WHERE nl_isActive=1" . $where);
        }

        $filterData = array();
        if (!empty($tblSubscribers)) {
            foreach ($tblSubscribers as $tblSubscriber) {
                $filterData[] = $tblSubscriber[0];
            }
        }

        return $filterData;
    }
    
    public function AudienceProfile($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        // model initialization
        $tblAdunit = ClassRegistry::init('Authake.tblAdunit');
		$Dynamic = ClassRegistry::init('Dynamiccharts');
		//echo "dynimic";
		//$Dynamic->useDbConfig = 'DASEmail';
        $subscribers = array();
        $Dynamic->useTable = $tblAdunit->getEmailtableName($nl_adunit_uuid);
		// get here talbe name of das email
        //$options['conditions'] = array( 'Dynamiccharts.nl_create> (NOW() - INTERVAL 1 MONTH)' );
        $options = array();
        $subscribers['TotalSubcribers'] = $Dynamic->find('count', $options);
		$subscribers['TotalAvgMonth'] = $subscribers['TotalAvgDaily'] = $subscribers['opt_out_age'] = $subscribers['dropped_bounced_subscribers'] = $subscribers['active_engagement'] = 0;
        // avg monthly subscribe			  
		$Dynamic->virtualFields = array("avgmonth" => "count('Dynamiccharts.nl_id')", 'month' => 'month(Dynamiccharts.nl_create)');
        $options['fields'] = array('Dynamiccharts.avgmonth');
        $options['group'] = array('month');
        $monthWiseData = $Dynamic->find('all', $options);
        //get  avg 
        if (!empty($monthWiseData)) {
            $totalmonth = 0;
            $countmonth = 1;
            foreach ($monthWiseData as $monthdata) {
                $totalmonth += $monthdata['Dynamiccharts']['avgmonth'];
                $countmonth++;
            }
            $subscribers['TotalAvgMonth'] = $totalmonth / $countmonth;
        }
        // avg Daily subscribe			  


        $Dynamic->virtualFields = array("avgday" => "count('Dynamiccharts.nl_id')", 'date' => 'date(Dynamiccharts.nl_create)');
        $options['fields'] = array('Dynamiccharts.avgday');
        $options['group'] = array('date');
        $daysWiseData = $Dynamic->find('all', $options);
        if (!empty($daysWiseData)) {
            $totalday = 0;
            $countday = 1;
            foreach ($daysWiseData as $daydata) {
                $totalday += $daydata['Dynamiccharts']['avgday'];
                $countday++;
            }
            $subscribers['TotalAvgDaily'] = $totalday / $countmonth;
        }

        //get Opt-Outs %age
        $options = array();
        $Dynamic->virtualFields = array();
        $options['conditions'] = array('Dynamiccharts.nl_isactive = 0');
        $totalnlisactive = $Dynamic->find('count', $options);
        if (!empty($totalnlisactive) and ! empty($subscribers['TotalSubcribers'])) {
            $subscribers['opt_out_age'] = self::getPercentage($totalnlisactive, $subscribers['TotalSubcribers'], 100);
        }
        // Dropped / Bounced Subscribers 
        $options = array();
        $options['joins'] = array(
            array('table' => 'das_digitaladvertising.tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'Dynamiccharts.nl_adunit_uuid = tblMailing.m_ad_uid')));

        $Dynamic->virtualFields = array('total_m_bounce' => 'count(tblMailing.m_bounce)');
        $options['conditions'] = array('Dynamiccharts.nl_isactive = 1');
        $options['fields'] = array('total_m_bounce');
        $totalbounced = $Dynamic->find('first', $options);

        if (!empty($totalnlisactive['Dynamiccharts']['total_m_bounce']) and ! empty($subscribers['TotalSubcribers'])) {
            $subscribers['dropped_bounced_subscribers'] = self::getPercentage($totalnlisactive['Dynamiccharts']['total_m_bounce'], $subscribers['TotalSubcribers'], 100);
        }
        // is opend

        $Dynamic->virtualFields = array('total_m_open' => 'count(tblMailing.m_open)');
        $options['fields'] = array('total_m_open');

        $totalnlisactive = $Dynamic->find('first', $options);
        if (!empty($totalnlisactive['Dynamiccharts']['total_m_open']) and ! empty($subscribers['TotalSubcribers'])) {
            $subscribers['active_engagement'] = self::getPercentage($totalnlisactive['Dynamiccharts']['total_m_open'], $subscribers['TotalSubcribers'], 100);
        }
        // clear virtual fields
        $Dynamic->virtualFields = array();
        return $subscribers;
    } 
	private function getPercentage($field = 1, $total = 1, $div = 1000) {
        return ($total == 0) ? 0 : ($field * $div / $total);
    }
    
     /**
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    public function SubscribersBySource($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
         // model initialization
        $tblAdunit = ClassRegistry::init('Authake.tblAdunit');
		$Dynamic = ClassRegistry::init('Dynamiccharts');
		$subscribers = array();
        $Dynamic->useTable = $tblAdunit->getEmailtableName($nl_adunit_uuid); // get here talbe name of das email
        // Dropped / Bounced Subscribers 
        $options = array();

        $Dynamic->virtualFields = array('totalsource' => 'count(Dynamiccharts.nl_email)');
        $options['conditions'] = array('Dynamiccharts.nl_isactive = 1');
        $options['group'] = array('Dynamiccharts.nl_source');
        $options['fields'] = array('Dynamiccharts.totalsource', 'Dynamiccharts.nl_source');
        return $Dynamic->find('all', $options);
    }
    /**
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    
    public function DailySubscribersBySource($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        $subscribers = array();
         // model initialization
        $tblAdunit = ClassRegistry::init('Authake.tblAdunit');
		$Dynamic = ClassRegistry::init('Dynamiccharts');
		$Dynamic->virtualFields = array();
                
		$Dynamic->useTable = $tblAdunit->getEmailtableName($nl_adunit_uuid); // get here talbe name of das email
        // Dropped / Bounced Subscribers 
        $options = array();
        $options['joins'] = array(
            array('table' => 'das_digitaladvertising.tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'Dynamiccharts.nl_adunit_uuid = tblMailing.m_ad_uid')),
            array('table' => 'tbl_revenue_by_source',
                'alias' => 'tblRevenuBySource',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_id = tblRevenuBySource.m_id')));
        //DATE_FORMAT(tblIspReport.date,'%m/%d/%Y')
        $Dynamic->virtualFields = array('totalsource' => 'count(tblRevenuBySource.source_name)', 'date' => 'DATE_FORMAT(Dynamiccharts.nl_create,"%m/%d/%Y")');
        $options['conditions'] = array('Dynamiccharts.nl_isactive = 1');
        $options['group'] = array('tblRevenuBySource.id', 'tblRevenuBySource.source_name');
        $options['fields'] = array('tblRevenuBySource.source_name', 'totalsource', 'date');
        $records = $Dynamic->find('all', $options);
        $filterData = array();
        $source = array();
        if (!empty($records)) {
            foreach ($records as $data) {
                $index = $data['Dynamiccharts']['date'];
                $filterData[$index][] = array('date' => $data['Dynamiccharts']['date'], 'source' => $data['tblRevenuBySource']['source_name'], 'totalsource' => $data['Dynamiccharts']['totalsource']);
                // this is used for tototal souce
                $source[] = $data['tblRevenuBySource']['source_name'];
            }
        }
        return array('data' => $filterData, 'allsource' => array_unique($source));
    }
    /**
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    public function RevenueBySource($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        $subscribers = array();
         // model initialization
        $tblAdunit = ClassRegistry::init('Authake.tblAdunit');
		$Dynamic = ClassRegistry::init('Dynamiccharts');
		$Dynamic->virtualFields = array();
		
        $Dynamic->useTable = $tblAdunit->getEmailtableName($nl_adunit_uuid); // get here talbe name of das email
        // Dropped / Bounced Subscribers 
        $options = array();
        $options['joins'] = array(
            array('table' => 'das_digitaladvertising.tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'Dynamiccharts.nl_adunit_uuid = tblMailing.m_ad_uid')),
            array('table' => 'tbl_revenue_by_source',
                'alias' => 'tblRevenuBySource',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_id = tblRevenuBySource.m_id')));

        $Dynamic->virtualFields = array('totalsource' => 'count(tblRevenuBySource.source_name)');
        $options['conditions'] = array('Dynamiccharts.nl_isactive = 1');
        $options['group'] = array('tblRevenuBySource.source_name');
        $options['fields'] = array('tblRevenuBySource.source_name', 'totalsource');
        return $Dynamic->find('all', $options);
    }
    /**
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    
     public function RevenueOpenClickBySource($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        $subscribers = array();
         // model initialization
        $tblAdunit = ClassRegistry::init('Authake.tblAdunit');
		$Dynamic = ClassRegistry::init('Dynamiccharts');
		$Dynamic->virtualFields = array();
		 // get here talbe name of das email
        // Dropped / Bounced Subscribers 
        $options = array();
        $options['joins'] = array(
            array('table' => 'das_digitaladvertising.tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'Dynamiccharts.nl_adunit_uuid = tblMailing.m_ad_uid')),
            array('table' => 'tbl_revenue_by_source',
                'alias' => 'tblRevenuBySource',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_id = tblRevenuBySource.m_id')));

        $Dynamic->virtualFields = array('totalopens' => 'count(tblMailing.m_open)', 'totalclicks' => 'count(tblMailing.m_clicks)');
        $options['conditions'] = array('Dynamiccharts.nl_isactive = 1');
        $options['group'] = array('tblRevenuBySource.source_name');
        $options['fields'] = array('tblRevenuBySource.source_name', 'totalopens', 'totalclicks');
        return $Dynamic->find('all', $options);
    }

    
  }   
     
     
    
?>
