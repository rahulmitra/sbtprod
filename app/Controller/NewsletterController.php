<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class NewsletterController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblNlsubscribers','Authake.tblAdunit','Authake.tblMappedAdunit','Authake.tblMailing','Authake.tblOrderDailySummary','Dynamic');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {		
			$this->set('title_for_layout','All Orders');
			$userId = $this->Authake->getUserId();
			
			$options['conditions'] = array(
			'tblAdunit.ad_ptype_id' => '13',
			'tblAdunit.owner_user_id' => $userId,
			'tblAdunit.ad_parent_id' => 0
			);
			
			$nlproducts = $this->tblAdunit->find('all', $options);
			$subscribersCount = array();
			$nlChilds = array();
			foreach($nlproducts as $nllist) {
				$nl_adunit_uuid = $nllist['tblAdunit']['ad_uid'];
				$pub_id = $this->tblAdunit->field(
				'publisher_id',
				array('ad_uid' => $nl_adunit_uuid)
				);
				$table_name = $pub_id. "_" . $nl_adunit_uuid;
				$this->Dynamic->setSource($table_name); 
				$test = $this->Dynamic->find("all");
				
				$options1['conditions'] = array(
				'tblAdunit.ad_ptype_id' => '13',
				'tblAdunit.owner_user_id' => $userId,
				'tblAdunit.ad_parent_id' => $nllist['tblAdunit']['adunit_id']
				);
				
				$nlchilds[$nllist['tblAdunit']['ad_uid']] = $this->tblAdunit->find('all', $options1);
				$subscribersCount[$nllist['tblAdunit']['ad_uid']] = count($test);
			}
			
			
			//print_r($test);
			$this->set('group', $nlproducts);
			$this->set('subscribersCount', $subscribersCount);
			$this->set('nlchilds', $nlchilds);
			
			//print_r($d);
		}
		
		public function view_stats($nl_adunit_uuid, $duration = null) {
			$listid = null; 
			
			$options['joins'] = array(
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblp.profileID = User.tblProfileID')
			));
			$options['fields'] = array('tblp.tracksite_id');
			$options['conditions'] = array('User.id' => $this->Authake->getUserId());
			
			$tracksiteArr = $this->User->find('first', $options);
			$tracksite_id = $tracksiteArr['tblp']['tracksite_id'];
			
			
			App::import('Vendor', 'src/Piwik');
			$piwik = new Piwik(Configure::read('Piwik.url'), Configure::read('Piwik.token'), $tracksite_id, Piwik::FORMAT_JSON);
			
			$allSubscribers = $this->tblNlsubscribers->query("SELECT  DATE_FORMAT(nl_create, '%Y-%m-%d') as date, COUNT(*) as count FROM    tbl_nlsubscribers WHERE nl_adunit_uuid = '{$nl_adunit_uuid}' and nl_create BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() GROUP BY date");
			//print_r($allSubscribers);
			
			$nlSubscribersIsPromotion = $this->tblNlsubscribers->query("SELECT  DATE_FORMAT(nl_create, '%Y-%m-%d') as date, COUNT(*) as count FROM tbl_nlsubscribers WHERE nl_adunit_uuid = '{$nl_adunit_uuid}' and nl_ispromotion=1 and nl_create BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE() GROUP BY date");
			//print_r($nlSubscribersIsPromotion);
			//print_r($nlSubscribersIsPromotion);
			
			$piwik->setPeriod(Piwik::PERIOD_DAY);
			$piwik->setRange(date("Y-m-d", strtotime('-30 days')), date("Y-m-d"));
			$nb_pageviews = $piwik->getAction('','nb_pageviews');
			$nb_pageviews = (array)$nb_pageviews;
			$getEventCategory = $piwik->getEventAction();
			$getEventCategory = (array)$getEventCategory;
			
			$d = array();
			for($i = 0; $i < 30; $i++) 
			{
				$d[$i]['date'] = date("Y-m-d", strtotime('-'. $i .' days'));
				$d[$i]['day'] = date("d", strtotime('-'. $i .' days'));
				$d[$i]['month'] = date("m", strtotime('-'. $i .' days'));
				$d[$i]['year'] = date("Y", strtotime('-'. $i .' days'));
				$c_date = $d[$i]['date'];
				if(isset($nb_pageviews[$c_date]))
				$d[$i]['pageviews'] = $nb_pageviews[$c_date];
				else
				$d[$i]['pageviews'] = 0;
				$d[$i]['allSubscribers'] = 0;
				foreach ($allSubscribers as $data) {
					if($data[0]['date'] == $c_date)
					$d[$i]['allSubscribers'] = $data[0]['count'];
				}
				
				$d[$i]['IsPromotion'] = 0;
				foreach ($nlSubscribersIsPromotion as $data) {
					if($data[0]['date'] == $c_date)
					$d[$i]['IsPromotion'] = $data[0]['count'];
				}
				
				$d[$i]['popupviews'] = 0; 
				if(isset($getEventCategory[$c_date])){
					foreach ($getEventCategory[$c_date] as $key => $value) {
						if($value->label == "View")
						$d[$i]['popupviews'] = $value->nb_visits;
					}
				}
			}
			$this->set('popupdata', $d);
			
			
			if($duration == null)
			$duration = "month";
			
			if($listid == null)
			$listid = "all";
			
			switch($duration)
			{
				case "day" :
				$range = $this->rangeDay(date("Y-m-d"));
				break;
				case "week" :
				$range = $this->rangeWeek(date("Y-m-d"));
				break;
				case "month" :
				$range = $this->rangeMonth(date("Y-m-d"));
				break;
				case "year" :
				$range = $this->rangeYear(date("Y-m-d"));
				break;
				default :
				$range = $this->rangeWeek(date("Y-m-d"));
				break;
			} 
			
			//print_r($range);
			
			$options['joins'] = array(
			array('table' => 'tbl_mapped_adunits',
			'alias' => 'tma',
			'type' => 'INNER',
			'conditions' => array(
			'tma.ma_uid = tblMailing.m_ad_uid')),
			array('table' => 'tbl_adunits',
			'alias' => 'tba', 
			'type' => 'INNER',
			'conditions' => array(
			'tba.ad_dfp_id = tma.ma_ad_id',
			'tba.owner_user_id' => array($this->Authake->getUserId())
			)));
			
			
			$options['conditions'] = array('and' => array(
			array('tblMailing.m_schedule_date <= ' => $range['end'],
			'tblMailing.m_schedule_date >= ' => $range['start'],
			'tba.ad_uid ' => $nl_adunit_uuid
			)));
			
			$options['fields'] = array('tba.*', 'tblMailing.*');
			$options['order'] = array('tblMailing.m_schedule_date'=>'DESC');
			
			//print_r($options);
			
			$tblMailing = $this->tblMailing->find('all', $options);
			
			$sent = 0;
			$delivered = 0;
			$opens = 0;
			$clicks = 0;
			$unsubscribe = 0;
			$bounce = 0;
			foreach ($tblMailing as $item) {
				$sent += $item['tblMailing']['m_sent'];
				$delivered += $item['tblMailing']['m_delivered'];
				$opens += $item['tblMailing']['m_u_open'];
				$clicks += $item['tblMailing']['m_u_clicks'];
				$unsubscribe += $item['tblMailing']['m_unsubscribe'];
				$bounce += $item['tblMailing']['m_bounce'];
			}
			
			
			$this->set('sent', $sent);
			$this->set('delivered', $delivered);
			$this->set('opens', $opens);
			$this->set('clicks', $clicks);
			$this->set('duration', $duration);
			$this->set('unsubscribe', $unsubscribe);
			$this->set('bounce', $bounce);
			
			//print_r($tblMailing);
			
			//$selectQuery = "SELECT *, (select count(*) from tbl_messages where msg_m_id=m_id) as sent, (select count(*) from tbl_tracking_delivers where td_mailing_id=m_id) as delivered , (select count(*) from tbl_tracking_opens where to_mailing_id=m_id) as opens , (select count(*) from tbl_tracking_clicks where tc_mailing_id=m_id) as clicks FROM `tbl_mailings` where m_ad_uid in (SELECT ad_uid FROM `tbl_adunits` WHERE publisher_id =15)";
			//$response = $this->DasDynamic->query($selectQuery);
			//print_r($response);
			$this->set('group', $tblMailing);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $nl_adunit_uuid)
			);
			$table_name = $pub_id. "_" . $nl_adunit_uuid;
			$this->Dynamic->useTable = $table_name; 
			
			$test = $this->Dynamic->find('all');
			//print_r($test);
			$this->set('subscribers', $test);
		}
		
		
		public function view_stats_email($nl_adunit_uuid, $duration = null) {
			$listid = null; 
			
			$options['joins'] = array(
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblp.profileID = User.tblProfileID')
			));
			$options['fields'] = array('tblp.tracksite_id');
			$options['conditions'] = array('User.id' => $this->Authake->getUserId());
			
			$tracksiteArr = $this->User->find('first', $options);
			$tracksite_id = $tracksiteArr['tblp']['tracksite_id'];
			
			if($duration == null)
			$duration = "month";
			
			if($listid == null)
			$listid = "all";
			
			switch($duration)
			{
				case "day" :
				$range = $this->rangeDay(date("Y-m-d"));
				break;
				case "week" :
				$range = $this->rangeWeek(date("Y-m-d"));
				break;
				case "month" :
				$range = $this->rangeMonth(date("Y-m-d"));
				break;
				case "year" :
				$range = $this->rangeYear(date("Y-m-d"));
				break;
				default :
				$range = $this->rangeWeek(date("Y-m-d"));
				break;
			} 
			
			//print_r($range);
			
			$options['joins'] = array(
			array('table' => 'tbl_mapped_adunits',
			'alias' => 'tma',
			'type' => 'INNER',
			'conditions' => array(
			'tma.ma_uid = tblMailing.m_ad_uid')),
			array('table' => 'tbl_adunits',
			'alias' => 'tba', 
			'type' => 'INNER',
			'conditions' => array(
			'tba.ad_dfp_id = tma.ma_ad_id',
			'tba.owner_user_id' => array($this->Authake->getUserId())
			)));
			
			$options['conditions'] = array('and' => array(
			array('tblMailing.m_schedule_date <= ' => $range['end'],
			'tblMailing.m_schedule_date >= ' => $range['start'],
			'tba.ad_uid ' => $nl_adunit_uuid
			)));
			
			$options['fields'] = array('tba.*', 'tblMailing.*');
			$options['order'] = array('tblMailing.m_schedule_date'=>'DESC');
			
			//print_r($options);
			
			$tblMailing = $this->tblMailing->find('all', $options);
			
			
			$sent = 0;
			$delivered = 0;
			$opens = 0;
			$totalopens = 0;
			$clicks = 0;
			$totalclicks = 0;
			$unsubscribe = 0;
			$bounce = 0;
			$spam = 0;
			foreach ($tblMailing as $item) {
				$sent += $item['tblMailing']['m_sent'];
				$delivered += $item['tblMailing']['m_delivered'];
				$opens += $item['tblMailing']['m_u_open'];
				$totalopens += $item['tblMailing']['m_open'];
				$clicks += $item['tblMailing']['m_u_clicks'];
				$totalclicks += $item['tblMailing']['m_clicks'];
				$unsubscribe += $item['tblMailing']['m_unsubscribe'];
				$bounce += $item['tblMailing']['m_bounce'];
				$spam += $item['tblMailing']['m_spam'];
			}
			$totalQuality = $unsubscribe + $bounce + $spam;
			
			$totalQuality = ($totalQuality == 0 ? "1" : $totalQuality);
			
			$this->set('sent', $sent);
			$this->set('delivered', $delivered);
			$this->set('opens', $opens);
			$this->set('clicks', $clicks);
			$this->set('totalopens', $totalopens);
			$this->set('totalclicks', $totalclicks);
			$this->set('duration', $duration);
			$this->set('unsubscribe', $unsubscribe);
			$this->set('bounce', $bounce);
			$this->set('spam', $spam);
			$this->set('totalQuality', $totalQuality);
			
			
			//print_r($tblMailing);
			
			//$selectQuery = "SELECT *, (select count(*) from tbl_messages where msg_m_id=m_id) as sent, (select count(*) from tbl_tracking_delivers where td_mailing_id=m_id) as delivered , (select count(*) from tbl_tracking_opens where to_mailing_id=m_id) as opens , (select count(*) from tbl_tracking_clicks where tc_mailing_id=m_id) as clicks FROM `tbl_mailings` where m_ad_uid in (SELECT ad_uid FROM `tbl_adunits` WHERE publisher_id =15)";
			//$response = $this->DasDynamic->query($selectQuery);
			//print_r($response);
			$this->set('group', $tblMailing);
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $nl_adunit_uuid)
			);
			
			$subscribers = array();
			$table_name = $pub_id. "_" . $nl_adunit_uuid;
			$data_table = "SHOW TABLES LIKE '$table_name'";
			$tables = $this->Dynamic->query($data_table);
			if(count($tables) > 0) {
				$this->Dynamic->useTable = $table_name; 
				$subscribers = $this->Dynamic->find('all');
			}
			$this->set('subscribers', $subscribers);
			
			$adunitid = $this->tblAdunit->field('adunit_id', array('ad_uid' =>$nl_adunit_uuid));
			$nlchild = array();
			$options1['conditions'] = array(
			'tblAdunit.owner_user_id' => $this->Authake->getUserId(),
			'tblAdunit.ad_parent_id' => $adunitid
			);
			
			$nlchilds = $this->tblAdunit->find('all', $options1);
			$totalRevenue = 0;
			$totalCPM = 0;
			$totalCPC = 0;
			$totalCPL = 0;
			$totalSCR = 0;
			$revenueCPM = 0;
			$revenueCPC = 0;
			$revenueCPL = 0;
			$revenueSCR = 0;
			$mailsentCPM = 0;
			$mailsentCPC = 0;
			$mailsentCPL = 0;
			$mailsentSCR = 0;
			$activeLineItems = array();
			$activeLineTypes = array();
			$revenueByTypes = array();
			$revenueByAdunits = array();
			//print_r($nlchilds);
			$countm = 0;
			if(count($nlchilds) > 0) {
				foreach($nlchilds as $nlchild){
					$optionnl['joins'] = array(
					array('table' => 'tbl_line_items',
					'alias' => 'tblLineItems',
					'type' => 'INNER',
					'conditions' => array(
					'tblLineItems.li_id = tblMappedAdunit.ma_l_id')
					),
					array('table' => 'tbl_adunits',
					'alias' => 'tba', 
					'type' => 'INNER',
					'conditions' => array(
					'tba.ad_dfp_id = tblMappedAdunit.ma_ad_id'
					)));
					
					$optionnl['conditions'] = array('tblMappedAdunit.ma_ad_id' => $nlchild['tblAdunit']['ad_dfp_id'],  'tblMappedAdunit.ma_isActive' => '1');
					
					$optionnl['fields'] = array('tblMappedAdunit.*','tblLineItems.*', 'tba.adunit_name');
					$mappedUnits = $this->tblMappedAdunit->find('all',$optionnl);
					//echo "<pre>";
					//print_r($mappedUnits);
					//echo "</pre>";
					foreach($mappedUnits as $mappedUnit) {
						$totalRevenue += $mappedUnit['tblMappedAdunit']['ma_revenue'];
						$activeLineItems[] = $mappedUnit['tblLineItems']['li_id'];
						$activeLineTypes[] = $mappedUnit['tblLineItems']['li_cs_id'];
						$revenueByTypes[$countm]['type'] = $mappedUnit['tblLineItems']['li_cs_id'];
						$revenueByTypes[$countm]['revenue'] = $mappedUnit['tblMappedAdunit']['ma_revenue'];
						$revenueByTypes[$countm]['name'] = $mappedUnit['tblLineItems']['li_name'];
						//$revenueByTypes[$countm]['sent'] = $mappedUnit['tblLineItems']['m_sent'];
						$revenueByAdunits[$countm]['name'] = $mappedUnit['tba']['adunit_name'];
						$revenueByAdunits[$countm]['revenue'] = $mappedUnit['tblMappedAdunit']['ma_revenue'];
						$countm++;
					}
				}
				} else {
				$ad_dfp_id = $this->tblAdunit->field('ad_dfp_id', array('ad_uid' =>$nl_adunit_uuid));
				$optionnl['joins'] = array(
				array('table' => 'tbl_line_items',
				'alias' => 'tblLineItems',
				'type' => 'INNER',
				'conditions' => array(
				'tblLineItems.li_id = tblMappedAdunit.ma_l_id')
				),
				array('table' => 'tbl_adunits',
				'alias' => 'tba', 
				'type' => 'INNER',
				'conditions' => array(
				'tba.ad_dfp_id = tblMappedAdunit.ma_ad_id'
				)));
				
				$optionnl['conditions'] = array('tblMappedAdunit.ma_ad_id' => $ad_dfp_id,  'tblMappedAdunit.ma_isActive' => '1');
				
				$optionnl['fields'] = array('tblMappedAdunit.*','tblLineItems.*', 'tba.adunit_name');
				$mappedUnits = $this->tblMappedAdunit->find('all',$optionnl);
				//echo "<pre>";
				//print_r($mappedUnits);
				//echo "</pre>";
				foreach($mappedUnits as $mappedUnit) {
					$totalRevenue += $mappedUnit['tblMappedAdunit']['ma_revenue'];
					$activeLineItems[] = $mappedUnit['tblLineItems']['li_id'];
					$activeLineTypes[] = $mappedUnit['tblLineItems']['li_cs_id'];
					$revenueByTypes[$countm]['type'] = $mappedUnit['tblLineItems']['li_cs_id'];
					$revenueByTypes[$countm]['revenue'] = $mappedUnit['tblMappedAdunit']['ma_revenue'];
					$revenueByTypes[$countm]['name'] = $mappedUnit['tblLineItems']['li_name'];
					$revenueByAdunits[$countm]['name'] = $mappedUnit['tba']['adunit_name'];
					$revenueByAdunits[$countm]['revenue'] = $mappedUnit['tblMappedAdunit']['ma_revenue'];
					$countm++;
				}
			}
			$orderby = "revenue";
			
			if(count($revenueByTypes) > 0) {
				$sortArray = array(); 
				foreach($revenueByTypes as $revenueByType){ 
					foreach($revenueByType as $key=>$value){ 
						if(!isset($sortArray[$key])){ 
							$sortArray[$key] = array(); 
						} 
						$sortArray[$key][] = $value; 
					} 
				} 
				array_multisort($sortArray[$orderby],SORT_DESC,$revenueByTypes); 
			}
			
			if(count($revenueByAdunits) > 0) {
				$sortArray1 = array(); 
				foreach($revenueByAdunits as $revenueByAdunit){ 
					foreach($revenueByAdunit as $key=>$value){ 
						if(!isset($sortArray1[$key])){ 
							$sortArray1[$key] = array(); 
						} 
						$sortArray1[$key][] = $value; 
					} 
				} 
				array_multisort($sortArray1[$orderby],SORT_DESC,$revenueByAdunits); 
			}
			
			
			foreach($revenueByTypes as $revenueByType){
				switch($revenueByType['type'])
				{
					case 1 :
					$revenueCPC += $revenueByType['revenue'];
					break;
					case 2 :
					$revenueCPL += $revenueByType['revenue'];
					break;
					case 3 :
					$revenueCPL += $revenueByType['revenue'];
					break;
					case 4 :
					$revenueCPL += $revenueByType['revenue'];
					break;
					case 5 :
					$revenueCPM += $revenueByType['revenue'];
					break;
					case 6 :
					$revenueCPL += $revenueByType['revenue'];
					break;
				}
			}
			
			//$activeLineTypes = array_unique($activeLineTypes);
			foreach($activeLineTypes as $activeLineType){
				switch($activeLineType)
				{
					case 1 :
					$totalCPC++;
					break;
					case 2 :
					$totalCPL++;
					break;
					case 3 :
					$totalCPL++;
					break;
					case 4 :
					$totalCPL++;
					break;
					case 5 :
					$totalCPM++;
					break;
					case 6 :
					$totalCPL++;
					break;
				}
			}
			$this->set('totalRevenue', $totalRevenue);
			$this->set('totalCPM', $totalCPM);
			$this->set('nlchilds', $nlchilds);
			$this->set('totalCPC', $totalCPC);
			$this->set('totalCPL', $totalCPL);
			$this->set('totalSCR', $totalSCR);
			$this->set('revenueCPL', $revenueCPL);
			$this->set('revenueCPM', $revenueCPM);
			$this->set('revenueCPC', $revenueCPC);
			$this->set('revenueSCR', $revenueSCR);
			$this->set('revenueByTypes', $revenueByTypes);
			$this->set('revenueByAdunits', $revenueByAdunits);
			$this->set('activeLines', count(array_unique($activeLineItems)));
		}
		
		
		public function view_message_stats($listid){
			
			$options['joins'] = array(
			array('table' => 'tbl_mapped_adunits',
			'alias' => 'tma',
			'type' => 'INNER',
			'conditions' => array(
			'tma.ma_uid = tblMailing.m_ad_uid')),
			array('table' => 'tbl_adunits',
			'alias' => 'tba', 
			'type' => 'INNER',
			'conditions' => array(
			'tba.ad_dfp_id = tma.ma_ad_id',
			'tba.owner_user_id' => array($this->Authake->getUserId())
			)),
			array('table' => 'tbl_profiles',
			'alias' => 'tp',
			'type' => 'INNER',
			'conditions' => array(
			'tp.user_id = tba.publisher_id'))
			);
			
			
			$options['conditions'] = array('tblMailing.m_id ' => $listid
			);
			
			$options['fields'] = array('tba.*', 'tblMailing.*', 'tp.*');
			
			$tblMailing = $this->tblMailing->find('first', $options);
			//print_r($tblMailing);
			
			
			$sent = $tblMailing['tblMailing']['m_sent'];
			$delivered = $tblMailing['tblMailing']['m_delivered'];
			$opens = $tblMailing['tblMailing']['m_u_open'];
			$clicks = $tblMailing['tblMailing']['m_u_clicks'];
			
			$this->set('sent', $sent);
			$this->set('tblMailing', $tblMailing);
			$this->set('delivered', $delivered);
			$this->set('opens', $opens);
			$this->set('clicks', $clicks);
			
		}
		
		public function add() {		
			$isError = false;
			//$Model = ClassRegistry::init(array('class' => 'Wf128fe', 'table' => 'wf_128fe'));
			//$details = $Model->find('all');
			if($this->params['url']['aduid'])
			$this->request->data['tblNlsubscribers']['nl_adunit_uuid'] = $this->params['url']['aduid'];
			else
			$isError = true;
			if($this->params['url']['nl_email'])
			$this->request->data['tblNlsubscribers']['nl_email'] = $this->params['url']['nl_email'];
			else
			$isError = true;
			if(!$isError) {
				$this->tblNlsubscribers->create();
				$this->tblNlsubscribers->save($this->request->data);
			}
			$this->autoRender = false;
		}
		
		public function view_subscribers($nl_adunit_uuid) {
			$this->System->add_css(array('/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css'),'head');
			$this->set('title_for_layout','All Orders');
			
			$pub_id = $this->tblAdunit->field(
			'publisher_id',
			array('ad_uid' => $nl_adunit_uuid)
			);
			$table_name = $pub_id. "_" . $nl_adunit_uuid;
			$this->Dynamic->useTable = $table_name; 
			
			$test = $this->Dynamic->find('all');
			//print_r($test);
			$this->set('group', $test);
		}
		
		
		public function upload_subscribers($nl_adunit_uuid) {
			$this->set('uniqueID', $nl_adunit_uuid);
		}
		
		public function upload_subscribers_list($nl_adunit_uuid) {
			$this->layout = null;
			App::import('Vendor', 'UploadHandler', array('file' => 'fileupload/UploadHandler.php'));
			$custom_dir = $_SERVER['DOCUMENT_ROOT'] . '/files/';
			$options = array(
			'upload_dir' => $custom_dir,        
			'accept_file_types' => '/\.(csv)$/i'                     
			);
			$upload_handler = new UploadHandler($options);
			print_r($upload_handler);
			//$this->render('/Elements/ajaxreturn'); // This View is declared at /Elements/ajaxreturn.ctp
		}
		
		public function posting_instructions($nl_adunit_uuid) {
			$this->set('uniqueID', $nl_adunit_uuid);
		}
		
		public function view_list_stats($nl_adunit_uuid) {
			$this->set('uniqueID', $nl_adunit_uuid);
		}
		
		public function newsletter_script($nl_adunit_uuid){
			$optionm['joins'] = array(
			array('table' => 'tbl_adunits',
			'alias' => 'tblAdunit',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
			),
			array('table' => 'tbl_profiles',
			'alias' => 'tblp',
			'type' => 'INNER',
			'conditions' => array(
			'tblAdunit.publisher_id = tblp.user_id')));
			
			$optionm['conditions'] = array('tblAdunit.ad_uid' => $nl_adunit_uuid,  'tblMappedAdunit.ma_isActive' => '1');
			
			$optionm['fields'] = array('tblMappedAdunit.*', 'tblAdunit.*','tblp.*');
			$mappedUnits = $this->tblMappedAdunit->find('first',$optionm);
			$this->set('mappedUnits', $mappedUnits);
			
		}
		
		function rangeMonth($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('first day of this month', $dt));
			$res['end'] = date('Y-m-d', strtotime('last day of this month', $dt));
			return $res;
		}
		
		function rangeWeek($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('-7 Days', $dt));
			$res['end'] = $datestr;
			return $res;
		}
		
		function rangeYear($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = date('Y-m-d', strtotime('-360 Days', $dt));
			$res['end'] = $datestr;
			return $res;
		}
		
		function rangeDay($datestr) {
			date_default_timezone_set(date_default_timezone_get());
			$dt = strtotime($datestr);
			$res['start'] = $datestr;
			$res['end'] = $datestr;
			return $res;
		}
		
		
	}																							
