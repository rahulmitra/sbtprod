<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class OrderLeadsController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblAdunit','Authake.tblNlsubscribers','Authake.tblMappedAdunit','OrderLeads');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {		
			
		}
		
		public function add() {		
			$isError = false;
			
			if(!isset($this->params['url']['od_dfpid']))
			$isError = true;
			
			if(!isset($this->params['url']['od_email']))
			$isError = true;
			
			//print_r($this->request->data);
			if(!$isError) {
				$isDuplicate = 0;
				$od_isRejected = 0;
				$table_name = "order_" . $this->params['url']['od_dfpid'];
				$this->OrderLeads->useTable = $table_name; 
				$options['conditions'] = array('od_email' => $this->params['url']['od_email']);
				$resutls = $this->OrderLeads->find('all', $options);
				$count = count($resutls);
				if($count > 0)
				{
					$isDuplicate = 1;
					$od_isRejected = 1;
				}
				else {
					$options1['conditions'] = array('INET_NTOA(od_ipaddress)' => $this->params['url']['od_ipaddress'],'fp_browser' => $this->params['url']['fp_browser'],'fp_connection' => $this->params['url']['fp_connection'],'fp_display' => $this->params['url']['fp_display'],'fp_flash' => $this->params['url']['fp_flash'],'fp_language' => $this->params['url']['fp_language'],'fp_os' => $this->params['url']['fp_os'],'fp_timezone' => $this->params['url']['fp_timezone'],'fp_useragent' => $this->params['url']['fp_useragent']);
					$results = $this->OrderLeads->find('all', $options1);	
					$count1 = count($results);
					if($count1 > 0)
					$od_isRejected = 1;
				}
				
				
				$insertQuery = ("INSERT INTO " . $table_name . "(od_lid,od_cid,od_ad_id,od_email,od_site,fp_browser,fp_connection,fp_display,fp_flash,fp_language,fp_os,fp_timezone,fp_useragent,od_isDuplicate,od_isRejected,od_ipaddress) VALUES ({$this->params['url']['od_lid']},{$this->params['url']['od_cid']},{$this->params['url']['od_ad_id']},'{$this->params['url']['od_email']}','{$this->params['url']['od_site']}','{$this->params['url']['fp_browser']}','{$this->params['url']['fp_connection']}','{$this->params['url']['fp_display']}','{$this->params['url']['fp_flash']}','{$this->params['url']['fp_language']}','{$this->params['url']['fp_os']}','{$this->params['url']['fp_timezone']}','{$this->params['url']['fp_useragent']}',{$isDuplicate},{$od_isRejected}, INET_ATON('{$this->params['url']['od_ipaddress']}'))");
				$this->OrderLeads->query($insertQuery);
				} else {
				echo "Something Went Wrong";
			}
			$this->autoRender = false;
		}
		
		
		public function addOrderLeads() {		
			$isError = false;
			
			if(!isset($this->params['url']['od_cid']))
			$isError = true;
			
			if(!isset($this->params['url']['od_email']))
			$isError = true;
			
			if(!isset($this->params['url']['od_mid']))
			$isError = true;
			
			$response = "FAILURE";
			//print_r($this->request->data);
			if(!$isError) {
				
				$options2['conditions'] = array('tblMappedAdunit.ma_uid' => $this->params['url']['od_mid']);
				$options2['joins'] = array(
				array('table' => 'tbl_line_items',
				'alias' => 'tli',
				'type' => 'INNER',
				'conditions' => array(
				'tli.li_id = tblMappedAdunit.ma_l_id')));
				$options2['fields'] = array('tblMappedAdunit.*','tli.*');
				$tblMappedAdunit = $this->tblMappedAdunit->find('first', $options2);
				///print_r($tblMappedAdunit);
				//$this->params['url']['od_lid'] = $tblMappedAdunit['tli']['li_dfp_id'];
				//$this->params['url']['od_ad_id'] = $tblMappedAdunit['tblMappedAdunit']['ma_ad_id'];
				$isDuplicate = 0;
				$od_isRejected = 0;
				$table_name = "order_" . $tblMappedAdunit['tli']['li_order_id'];
				$this->OrderLeads->useTable = $table_name; 
				$options['conditions'] = array('od_email' => $this->params['url']['od_email']);
				$resutls = $this->OrderLeads->find('all', $options);
				$count = count($resutls);
				if($count > 0)
				{
					$isDuplicate = 1;
					$od_isRejected = 1;
				}
				else {
					$options1['conditions'] = array('INET_NTOA(od_ipaddress)' => $this->params['url']['od_ipaddress'],'fp_browser' => $this->params['url']['fp_browser'],'fp_connection' => $this->params['url']['fp_connection'],'fp_display' => $this->params['url']['fp_display'],'fp_flash' => $this->params['url']['fp_flash'],'fp_language' => $this->params['url']['fp_language'],'fp_os' => $this->params['url']['fp_os'],'fp_timezone' => $this->params['url']['fp_timezone'],'fp_useragent' => $this->params['url']['fp_useragent']);
					$results = $this->OrderLeads->find('all', $options1);	
					$count1 = count($results);
					if($count1 > 0)
					$od_isRejected = 1;
					$response = "SUCCESS";
				}
				
				
				$insertQuery = ("INSERT INTO " . $table_name . "(od_lid,od_cid,od_ad_id,od_map_id,od_email,od_site,fp_browser,fp_connection,fp_display,fp_flash,fp_language,fp_os,fp_timezone,fp_useragent,od_isDuplicate,od_isRejected,od_ipaddress,od_mailing_id) VALUES ({$tblMappedAdunit['tli']['li_dfp_id']},{$this->params['url']['od_cid']},{$tblMappedAdunit['tblMappedAdunit']['ma_ad_id']},'{$tblMappedAdunit['tblMappedAdunit']['ma_uid']}','{$this->params['url']['od_email']}','{$this->params['url']['od_site']}','{$this->params['url']['fp_browser']}','{$this->params['url']['fp_connection']}','{$this->params['url']['fp_display']}','{$this->params['url']['fp_flash']}','{$this->params['url']['fp_language']}','{$this->params['url']['fp_os']}','{$this->params['url']['fp_timezone']}','{$this->params['url']['fp_useragent']}',{$isDuplicate},{$od_isRejected}, INET_ATON('{$this->params['url']['od_ipaddress']}'),'{$this->params['url']['od_mailing_id']}')");
				$this->OrderLeads->query($insertQuery);
			}
			
			if($od_isRejected == 0){
				$email = $this->params['url']['od_email'];
				$leadid = $this->OrderLeads->getLastInsertID();
				if($this->params['url']['od_mid'] == "7006f1ba01be11e6b91e123aa499f9d5") {
					$this->httpGet("http://services.digitaladvertising.systems/data/data-post.aspx?lineitem=".$tblMappedAdunit['tli']['li_dfp_id']."&leadid=".$leadid."&method=realtime-api&email=".$email."&source=DAS-Sandpiper");
					
					$this->requestAction('/socketlab/stocksearningAutoresponder', array('email' => $email));
				}
				
				if($this->params['url']['od_mid'] == "b2ea6034123811e6b91e123aa499f9d5") {
					$this->httpGet("http://services.digitaladvertising.systems/data/data-post.aspx?lineitem=".$tblMappedAdunit['tli']['li_dfp_id']."&leadid=".$leadid."&method=realtime-api&email=".$email."&source=DAS-PMP");
					
					$this->requestAction('/socketlab/stocksearningAutoresponder', array('email' => $email));
				}
				
				if($this->params['url']['od_mid'] == "c9b081d9061f11e6b91e123aa499f9d5") {
					$this->httpGet("http://services.digitaladvertising.systems/data/data-post.aspx?lineitem=".$tblMappedAdunit['tli']['li_dfp_id']."&leadid=".$leadid."&method=realtime-api&email=".$email."&source=DAS-Dmt");
					
					$this->requestAction('/socketlab/stocksearningAutoresponder', array('email' => $email));
				}
				
				if($this->params['url']['od_mid'] == "c493f83f0cac11e6b91e123aa499f9d5") {
					$this->httpGet("http://services.digitaladvertising.systems/data/data-post.aspx?lineitem=".$tblMappedAdunit['tli']['li_dfp_id']."&leadid=".$leadid."&method=realtime-api&email=".$email."&source=DAS-FFP");
					
					$this->requestAction('/socketlab/stocksearningAutoresponder', array('email' => $email));
				}
				
				if($this->params['url']['od_mid'] == "c479cbaf17c011e6b91e123aa499f9d5") {
					$this->httpGet("http://services.digitaladvertising.systems/data/data-post.aspx?lineitem=".$tblMappedAdunit['tli']['li_dfp_id']."&leadid=".$leadid."&method=realtime-api&email=".$email."&source=DAS-OTC");
					
					$this->requestAction('/socketlab/stocksearningAutoresponder', array('email' => $email));
				}
			}
			$this->autoRender = false;
			echo $response;
		}
		
		function httpGet($url)
		{
			$ch = curl_init();  
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			$output=curl_exec($ch);
			curl_close($ch);
			return $output;
		}
	}													