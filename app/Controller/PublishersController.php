<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class PublishersController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblProfile', 'Authake.Rule');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {		
			// form posted
			// add css
			$this->System->add_css(array('/plugins/select2/select2.css','/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css'),'head');
			
			$this->set('title_for_layout','All Publishers');
			$options['conditions'] = array(
			'Ugroup.group_id' => '4' );
			
			$options['joins'] = array(
			array('table' => 'authake_groups_users',
			'alias' => 'Ugroup',
			'type' => 'INNER',
			'conditions' => array(
			'User.id = Ugroup.user_id')
			)
            );
			
			$options['fields'] = array('User.*');
			
			$test = $this->User->find('all', $options);
			
			$this->set('group', $test);
		}
		
		public function add() {		
			// form posted
			App::import('Vendor', 'src/Piwik');
			$this->set('title_for_layout','Add Publishers');
			
			//print_r($this->Authake->getGroupIds());
			if (!empty($this->request->data))
			{// only an admin can make an admin
				if ($this->request->data['Group']['Group'] == "1" and !in_array(1, $this->Authake->getGroupIds()))
				{
					$this->Session->setFlash(__('You cannot add a user in administrators group'), 'warning');
					$this->redirect(array('action'=>'index'));
				}
				
				$p = $this->request->data['User']['password'];
				$this->request->data['User']['password'] = md5($p);
				$this->User->create();
				$this->tblProfile->create();
				if ($this->User->save($this->request->data))
				{
					$piwik = new Piwik(Configure::read('Piwik.url'), Configure::read('Piwik.token'), 1, Piwik::FORMAT_JSON);
					$response = $piwik->addSite($this->request->data['tblProfile']['CompanyName'],$this->request->data['tblProfile']['CompanyURL']);
					$this->request->data['tblProfile']['user_id'] = $this->User->getLastInsertID();
					$this->request->data['tblProfile']['tracksite_id'] = $response;
					$this->tblProfile->save($this->request->data);
					$this->Session->setFlash(__('The Publisher has been saved'), 'success');
					$this->redirect(array('action'=>'index'));
				}
				else
				{
					$this->Session->setFlash(__('The User could not be saved. Please, try again.'), 'error');
				}
			}
			
			$this->request->data['User']['password'] = '';
			$groups = $this->User->Group->find('list');
			$this->set(compact('groups'));
			
		}
		
		public function addSite() {
			
			App::import('Vendor', 'src/Piwik');
			$piwik = new Piwik(Configure::read('Piwik.url'), Configure::read('Piwik.token'), 1, Piwik::FORMAT_JSON);
			echo "site id = ".$response = $piwik->addSite('Delhi Best Deals','http://www.delhibestdeals.com/');
					
			//echo 'Unique visitors yesterday: ' . $piwik->getUniqueVisitors();
		
		}

	}						
