<?php
	
	/**
		* Contact Controller
		* @author James Fairhurst <info@jamesfairhurst.co.uk>
	*/
	
	class NlsubscribersController extends AppController {
		/**
			* Components
		*/
		var $uses = array('Authake.Group','Authake.User','Authake.tblAdunit','Authake.tblNlsubscribers','Dynamic');
		var $components = array('RequestHandler','Authake.Filter','Session');// var $layout = 'authake';
		var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc'));//var $scaffold;
		
		/**
			* Before Filter callback
		*/
		public function beforeFilter() {
			parent::beforeFilter();
			
			// Change layout for Ajax requests
			if ($this->request->is('ajax')) {
				$this->layout = 'ajax';
			}
		}
		
		/**
			* Main index action
		*/
		public function index() {		
			
		}
		
		public function add() {		
			$isError = false;
			
			if(!$this->params['url']['aduid'])
				$isError = true;
			
			if(!$this->params['url']['nl_email'])
				$isError = true;
			
			//print_r($this->request->data);
			
			if(!$isError) {
				$pub_id = $this->tblAdunit->field(
				'publisher_id',
				array('ad_uid' => $this->params['url']['aduid'])
				);
				$table_name = $pub_id. "_" . $this->params['url']['aduid'];
				$this->Dynamic->useTable = $table_name; 
				$email_id = $this->params['url']['nl_email'] ;
				$checkQuery = ("SELECT * FROM " . $table_name . " WHERE nl_email ='$email_id' ");
				$check = $this->Dynamic->query($checkQuery);
				if(!$check){
				date_default_timezone_set('US/Eastern');
				$date = date('Y-m-d H:i:s');
				
				$insertQuery = ("INSERT INTO " . $table_name . "(nl_email, nl_fname, nl_lname, nl_create) VALUES ('{$this->params['url']['nl_email']}', '{$this->params['url']['nl_fname']}', '{$this->params['url']['nl_lname']}', '$date')");
				$this->Dynamic->query($insertQuery);
				}else{
				echo "Sorry!, this email address exist";
				}
			} else {
				echo "Something Went Wrong";
			}
			$this->autoRender = false;
		}
		
	}									