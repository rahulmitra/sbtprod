<?php

App::import('Controller', 'EspController');

class CronsController extends AppController {

    var $uses = array('Authake.tblMessage', 'Authake.User', 'Authake.tblTrackingDeliver', 'Authake.tblTrackingOpen', 'Dynamic', 'Authake.tblTrackingClick', 'Authake.tblMappedAdunit', 'Authake.tblProfile', 'Authake.tblMailing', 'Authake.tblCreative', 'Authake.tblLineItem', 'Authake.tblOrder', 'Authake.tblReportDay', 'UserLevelEmailPerformance', 'tblEspSettings');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout = null;
    }

    public function DMTFeedMailgun($m_ad_uid) {
        $this->requestAction('/mailgun/rssFeedTemplate1', array('url_feed' => 'http://dailymoneytimes.com/feed/', 'url' => 'http://digitaladvertising.systems/newsletter_create/dmtFeedTemplate/', 'siteName' => 'Daily Money Times', 'siteURL' => 'http://dailymoneytimes.com/', 'fromEmail' => 'newsletter@dmt-nl1.mg.das0.net', 'MAILGUNAPP_API_KEY' => 'key-4eb27ee2e9dd487f1095e7191a68b969', 'physical_address' => '<br>1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br>', 'domain' => 'dmt-nl1.mg.das0.net', 'm_ad_uid' => $m_ad_uid, 'isTest' => '0'));
        $this->autoRender = false;
        return;
    }

    public function dedicatedEmail($mailing_id, $esp_id) {
        switch ($esp_id) {
            case 6:
                $this->requestAction('/mailgun/DedicatedEmail', array('esp_id' => 6, 'mailing_id' => $mailing_id, 'SubjectLine' => 'Growing your money tree'));
                break;
            case 3:
                $this->requestAction('/mandrill/DedicatedEmail', array('esp_id' => 3, 'mailing_id' => $mailing_id, 'SubjectLine' => 'Growing your money tree'));
                break;
            case 5:
                //$this->requestAction('/sendgrid/DedicatedEmail', array('esp_id' => 5, 'mailing_id' => $mailing_id, 'SubjectLine' => 'Growing your money tree'));
                $this->requestAction('/sendgrid/DedicatedEmail', array('esp_id' => 5, 'mailing_id' => $mailing_id));
                break;
            Default :
                die();
                break;
        }
        $this->autoRender = false;
        return;
    }

    public function triggerCamp($mailing_id, $esp_id, $email) {
        switch ($esp_id) {
            case 3:
                $this->requestAction('/mandrill/DedicatedEmailTrigger', array('email' => $email, 'mailing_id' => $mailing_id));
                break;
            case 6:
                $this->requestAction('/mailgun/DedicatedEmailTrigger', array('email' => $email, 'mailing_id' => $mailing_id));
                break;
            Default :
                die();
                break;
        }
        $this->autoRender = false;
        return;
    }

    public function rssFeedTemplate1NL($m_ad_uid, $esp_id) {
        if ($m_ad_uid == "3d684b400cab11e6b91e123aa499f9d5") {
            $url_feed = "http://dailymoneytimes.com/feed/";
            $url_nl = "http://digitaladvertising.systems/newsletter_create/ffpFeedTemplate/";
            $rssfeed = $this->getFeed($url_feed);
            $SubjectLine = $rssfeed[0]['title'];
        }
        if ($m_ad_uid == "3f2d62e3d17811e589213417ebe5d44c") {
            $url_feed = "http://dailymoneytimes.com/feed/";
            $url_nl = "http://digitaladvertising.systems/newsletter_create/dmtFeedTemplate/";
            $rssfeed = $this->getFeed($url_feed);
            $SubjectLine = $rssfeed[0]['title'];
        }
        if ($m_ad_uid == "cb302d98129911e6b91e123aa499f9d5") {
            $url_feed = "http://www.politicsandmyportfolio.com/feed/";
            $url_nl = "http://digitaladvertising.systems/newsletter_create/politicsandmyportfolio/";
            $rssfeed = $this->getFeed($url_feed);
            $SubjectLine = $rssfeed[0]['title'];
        }
        if ($m_ad_uid == "6cc6c60dfd0b11e5b91e123aa499f9d5") {
            $url_nl = "http://digitaladvertising.systems/newsletter_create/OTCNewsletter/";
            $SubjectLine = "OTC Rockstar Top Market Movers on : " . date("m") . "/" . date("d") . "/" . date("Y");
        }

        switch ($esp_id) {
            case 6:
                $this->requestAction('/mailgun/rssFeedTemplateLive', array('url_nl' => $url_nl, 'SubjectLine' => $SubjectLine, 'esp_id' => 6, 'm_ad_uid' => $m_ad_uid));
                break;
            case 3:
                $this->requestAction('/mandrill/rssFeedTemplateLive', array('url_nl' => $url_nl, 'SubjectLine' => $SubjectLine, 'esp_id' => 3, 'm_ad_uid' => $m_ad_uid));
                break;
            case 2:
                $this->requestAction('/socketlab/rssFeedTemplateLive', array('url_nl' => $url_nl, 'SubjectLine' => $SubjectLine, 'esp_id' => 2, 'm_ad_uid' => $m_ad_uid));
                break;
            Default :
                die();
                break;
        }
        $this->autoRender = false;
        return;
    }

    public function FFPFeedMailgun($m_ad_uid) {
        $this->requestAction('/mailgun/rssFeedTemplate1', array('url_feed' => 'http://dailymoneytimes.com/feed/', 'url' => 'http://digitaladvertising.systems/newsletter_create/ffpFeedTemplate/', 'siteName' => 'Financial Freedom Post', 'siteURL' => 'http://financialfreedompost.com/', 'fromEmail' => 'newsletter@ffp-nl1.mg.das0.net', 'MAILGUNAPP_API_KEY' => 'key-4eb27ee2e9dd487f1095e7191a68b969', 'physical_address' => '<br>1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br>', 'domain' => 'ffp-nl1.mg.das0.net', 'm_ad_uid' => $m_ad_uid, 'isTest' => '0'));
        $this->autoRender = false;
        return;
    }

    public function FFPFeedMandrill($m_ad_uid) {
        $this->requestAction('/mandrill/rssFeedTemplate1', array('m_ad_uid' => $m_ad_uid, 'url_feed' => 'http://dailymoneytimes.com/feed/', 'url' => 'http://digitaladvertising.systems/newsletter_create/ffpFeedTemplate/', 'siteName' => 'Financial Freedom Post', 'siteURL' => 'http://financialfreedompost.com/', 'fromEmail' => 'newsletter@ffp-nl1.mnd.das0.net', 'MANDRILLAPP_API_KEY' => 'UVRlnjaTpI2KefEWXiF_Ng', 'domain' => 'ffp-nl1.mnd.das0.net', 'physical_address' => '<br>1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br>', 'isTest' => '1'));
        $this->autoRender = false;
    }

    public function testLead() {
        $this->Dynamic->useTable = "360_79c7ca8d236411e6b91e123aa499f9d5";
        $db = $this->Dynamic->getDataSource();

        $data = array(
            'Dynamic' => array(
                'nl_email' => 'sunnysss@siliconbt.com',
                'nl_fname' => 'sunny',
                'nl_lname' => 'gulati',
                'nl_source' => 'source',
                'nl_ip_address' => $db->expression("INET_ATON('103.248.93.180')")
        ));
        /*
          $this->Dynamic->set(array(
          'nl_email' => $_POST['email'],
          'nl_fname' => $fname,
          'nl_lname' => $lname,
          'nl_source' => $source,
          'nl_ip_address' => $location,
          'money' => 'INET_ATON('{$ipaddress}')'
          )); */
        $this->Dynamic->create();
        $this->Dynamic->save($data);
        $lastid = $this->Dynamic->getLastInsertId();
        $resp = "test";
        $this->Dynamic->updateAll(
                array('nl_pos_response' => "'" . $resp . "'"), array('nl_id' => $lastid)
        );
        $this->autoRender = false;
        return;
    }

    public function PMPFeedMailgun($m_ad_uid) {
        $this->requestAction('/mailgun/rssFeedTemplate1', array('url_feed' => 'http://www.politicsandmyportfolio.com/feed/', 'url' => 'http://digitaladvertising.systems/newsletter_create/politicsandmyportfolio/', 'siteName' => 'Politics & MyPortfolio', 'siteURL' => 'http://politicsandmyportfolio.com/', 'fromEmail' => 'newsletter@pmp-nl1.mg.das0.net', 'MAILGUNAPP_API_KEY' => 'key-4eb27ee2e9dd487f1095e7191a68b969', 'physical_address' => '<br>P.O. Box 8808<br>New Fairfield<br>CT - 06812<br>', 'domain' => 'pmp-nl1.mg.das0.net', 'm_ad_uid' => $m_ad_uid, 'isTest' => '1'));
        $this->autoRender = false;
        return;
    }

    public function PMPFeedMandrill($m_ad_uid) {
        $this->requestAction('/mandrill/rssFeedTemplate1', array('m_ad_uid' => $m_ad_uid, 'url_feed' => 'http://dailymoneytimes.com/feed/', 'url' => 'http://digitaladvertising.systems/newsletter_create/ffpFeedTemplate/', 'siteName' => 'Financial Freedom Post', 'siteURL' => 'http://financialfreedompost.com/', 'fromEmail' => 'newsletter@ffp-nl1.mnd.das0.net', 'MANDRILLAPP_API_KEY' => 'UVRlnjaTpI2KefEWXiF_Ng', 'domain' => 'ffp-nl1.mnd.das0.net', 'physical_address' => '<br>1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br>', 'isTest' => '1'));
        $this->autoRender = false;
    }

    public function OTCFeedMailgun($m_ad_uid) {
        $this->requestAction('/mailgun/OTCTemplate', array('url' => 'http://dailymoneytimes.com/feed/', 'siteName' => 'Daily Money Times', 'siteURL' => 'http://dailymoneytimes.com/', 'fromEmail' => 'newsletter@das0.net', 'MAILGUNAPP_API_KEY' => 'key-4eb27ee2e9dd487f1095e7191a68b969', 'physical_address' => '<br>1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br>', 'domain' => 'das0.net', 'm_ad_uid' => $m_ad_uid, 'isTest' => '1'));
        $this->autoRender = false;
        return;
    }

    public function mailVine($auid) {

        $this->requestAction('/esp/VineNLSocketLabs', array('aduid' => $auid));

        $this->autoRender = false;


        return;
    }

    public function mailInstagram($auid) {

        //$this->requestAction('/esp/instagramNLSocketLabs', array('aduid' => $auid));

        $this->autoRender = false;


        return;
    }

    public function updateCreativesData() {
        App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
        App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
        App::import('Vendor', 'src/ExampleUtils');
        $lineItems = $this->tblLineItem->find('all');

        foreach ($lineItems as $user) {
            // Set the ID of the line item to fetch all LICAs for.
            $lineItemId = $user['tblLineItem']['li_dfp_id'];
            try {
                // Get DfpUser from credentials in "../auth.ini"
                // relative to the DfpUser.php file's directory.
                $user = new DfpUser();
                // Log SOAP XML request and response.
                $user->LogDefaults();
                // Get the LineItemCreativeAssociationService.
                $lineItemCreativeAssociationService = $user->GetService(
                        'LineItemCreativeAssociationService', 'v201608');
                // Create a statement to select all LICAs for a given line item.
                $statementBuilder = new StatementBuilder();
                $statementBuilder->Where('lineItemId = :lineItemId')
                        ->OrderBy('lineItemId ASC, creativeId ASC')
                        ->Limit(StatementBuilder::SUGGESTED_PAGE_LIMIT)
                        ->WithBindVariableValue('lineItemId', $lineItemId);
                // Default for total result set size.
                $totalResultSetSize = 0;
                do {
                    // Get LICAs by statement.
                    $page = $lineItemCreativeAssociationService->getLineItemCreativeAssociationsByStatement($statementBuilder->ToStatement());
                    //print_r($page->results);
                    // Display results.
                    if (isset($page->results)) {
                        $totalResultSetSize = $page->totalResultSetSize;
                        $i = $page->startIndex;
                        foreach ($page->results as $lica) {
                            if (isset($lica->creativeSetId)) {
                                printf("%d) LICA with line item ID %d, and creative set ID %d was "
                                        . "found.\n", $i++, $lica->lineItemId, $lica->creativeSetId);
                            } else {
                                // Get the CreativeService.
                                $creativeService = $user->GetService('CreativeService', 'v201608');
                                // Set the ID of the creative to get.
                                $creativeId = $lica->creativeId;
                                $lineItemId = $lica->lineItemId;
                                $status = $lica->status;
                                $impressionsDelivered = $lica->stats->stats->impressionsDelivered;
                                $clicksDelivered = $lica->stats->stats->clicksDelivered;
                                $creativeName = '';
                                $creativeSnippet = '';
                                $creativePreviewURL = '';
                                $cid = '';
                                // Create a statement to select all creatives.
                                $statementBuilder = new StatementBuilder();
                                $statementBuilder->Where("id = :creativeId")->WithBindVariableValue("creativeId", $creativeId);
                                $statementBuilder->OrderBy('id ASC')->Limit(StatementBuilder::SUGGESTED_PAGE_LIMIT);
                                // Default for total result set size.
                                $totalResultSetSize = 0;
                                do {
                                    // Get creatives by statement.
                                    $page = $creativeService->getCreativesByStatement(
                                            $statementBuilder->ToStatement());
                                    // Display results.
                                    if (isset($page->results)) {
                                        $totalResultSetSize = $page->totalResultSetSize;
                                        $i = $page->startIndex;
                                        foreach ($page->results as $creative) {
                                            $creativeName = $creative->name;
                                            //$creativeSnippet = $creative->htmlSnippet;
                                            $creativePreviewURL = $creative->previewUrl;
                                            $cid = $creative->id;
                                            //print_r($creative);
                                        }
                                    }
                                    $statementBuilder->IncreaseOffsetBy(StatementBuilder::SUGGESTED_PAGE_LIMIT);
                                } while ($statementBuilder->GetOffset() < $totalResultSetSize);

                                $options = array();
                                $options['conditions'] = array('tblCreative.cr_dfp_id' => $creativeId);
                                //$tblCreatives = $this->tblCreative->find('all', $options)[0];
                                if (!isset($this->tblCreative->find('all', $options)[0])) {
                                    $this->request->data['tblCreative']['cr_lid'] = $lineItemId;
                                    $this->request->data['tblCreative']['cr_dfp_id'] = $cid;
                                    $this->request->data['tblCreative']['cr_status'] = $status;
                                    $this->request->data['tblCreative']['cr_imp'] = $impressionsDelivered;
                                    $this->request->data['tblCreative']['cr_click'] = $clicksDelivered;
                                    $this->request->data['tblCreative']['cr_name'] = $creativeName;
                                    $this->request->data['tblCreative']['cr_preview_url'] = $creativePreviewURL;
                                    $this->tblCreative->create();
                                    $this->tblCreative->save($this->request->data);
                                } else {
                                    $tblCreatives = $this->tblCreative->find('all', $options)[0];
                                    $this->request->data['tblCreative']['cr_id'] = $tblCreatives['tblCreative']['cr_id'];
                                    $this->request->data['tblCreative']['cr_lid'] = $lineItemId;
                                    $this->request->data['tblCreative']['cr_dfp_id'] = $cid;
                                    $this->request->data['tblCreative']['cr_status'] = $status;
                                    $this->request->data['tblCreative']['cr_imp'] = $impressionsDelivered;
                                    $this->request->data['tblCreative']['cr_click'] = $clicksDelivered;
                                    $this->request->data['tblCreative']['cr_name'] = $creativeName;
                                    $this->request->data['tblCreative']['cr_preview_url'] = $creativePreviewURL;
                                    $this->tblCreative->save($this->request->data);
                                }
                            }
                        }
                    }
                    $statementBuilder->IncreaseOffsetBy(StatementBuilder::SUGGESTED_PAGE_LIMIT);
                } while ($statementBuilder->GetOffset() < $totalResultSetSize);
                //printf("Number of results found: %d\n", $totalResultSetSize);
            } catch (OAuth2Exception $e) {
                ExampleUtils::CheckForOAuth2Errors($e);
            } catch (ValidationException $e) {
                ExampleUtils::CheckForOAuth2Errors($e);
            } catch (Exception $e) {
                printf("%s\n", $e->getMessage());
            }
        }

        $this->autoRender = false;


        return;
    }

    public function lyrisstats($listid) {
        App::import('Vendor', 'src/lyrisapi');
        $lyriss = new lyrisapi("666767", "Lyris@API");
        $messages = $lyriss->messageQueryListData($listid, null, null, "Lyris@API");
        $results = array();
        $count = 1;
        foreach ($messages as $message) {
            if ($message['sent'] == 'yes' && strpos($message['subject'], 'PROOF') === false) {
                //print_r($message);
                $stats = $lyriss->messageQueryStats($message['mid'], $listid);
                $results[$count]['mid'] = $message['mid'];
                $results[$count]['subject'] = $message['subject'];
                $results[$count]['sent_time_tz'] = $message['sent_time_tz'];
                $results[$count]['sent'] = $stats['stats-sent'];
                $results[$count]['opens'] = $stats['stats-opens'];
                $results[$count]['opens-unique'] = $stats['stats-opens-unique'];
                $results[$count]['spam'] = $stats['stats-spam'];
                $results[$count]['clicks-unique'] = $stats['stats-clicks-unique'];
                $results[$count]['bounces'] = $stats['stats-bounces'];
                $results[$count]['unsubscribes'] = $stats['stats-unsubscribes'];
                $count++;
            }
        }

        //fputcsv($output, array('mid','subject','sent_time_tz','sent','opens','opens-unique','spam','clicks-unique','bounces','unsubscribes'));


        $filePath = $_SERVER['DOCUMENT_ROOT'] . '/' . $listid . '.csv';

        $exists = file_exists($filePath) && filesize($filePath) > 0;

        $myfile = fopen($filePath, 'a+');

        fputcsv($myfile, array('mid', 'subject', 'sent_time_tz', 'sent', 'opens', 'opens-unique', 'spam', 'clicks-unique', 'bounces', 'unsubscribes'));

        if (!$exists) {
            foreach ($results as $product) {
                fputcsv($myfile, $product);
            }
        }

        //fputcsv($myfile, $posts_meta);

        $this->autoRender = false;
        return;
    }

    public function switchPanel() {
        $gid = $this->Session->read('Authake.group_ids');
        $newgid = array();
        if ($gid[0] == 3)
            $newgid[] = 4;
        if ($gid[0] == 4)
            $newgid[] = 3;

        $this->Session->write('Authake.group_ids', $newgid);
        $this->autoRender = false;
        return;
    }

    public function lyrisUpdateStats() {
        App::import('Vendor', 'src/lyrisapi');
        $lyriss = new lyrisapi("666767", "Lyris@API");
        $stats = $lyriss->messageQueryStats("1793463", "223235");
        $results = array();
        $results['sent'] = $stats['stats-sent'];
        $results['opens'] = $stats['stats-opens'];
        $results['opens-unique'] = $stats['stats-opens-unique'];
        $results['spam'] = $stats['stats-spam'];
        $results['clicks-unique'] = $stats['stats-clicks-unique'];
        $results['bounces'] = $stats['stats-bounces'];
        $results['unsubscribes'] = $stats['stats-unsubscribes'];
        $results['screenshot'] = "/upload/fbba4477849d11e5858d3417ebe5d44c/1446934337.jpg";
        $serialized_array = serialize($results);
        $this->autoRender = false;
        return;
    }

    public function lineItemsOverallUpdatefromDFP() {

        $orders = $this->tblOrder->find("all");
        //print_r($orders);
        foreach ($orders as $order) {
            if (!empty($order['tblOrder']['dfp_order_id'])) {
                //echo $order['tblOrder']['dfp_order_id'];
                $options['conditions'] = array('tblLineItem.li_order_id' => $order['tblOrder']['dfp_order_id']);
                $test = $this->tblLineItem->find("all", $options);
                $errors = array_filter($test);
                if (!empty($errors)) {

                    $orders = '';
                    $i = 1;
                    foreach ($test as $user) {
                        $orders .= ':orderId' . $i . ',';
                        $i++;
                    }
                    $orders = rtrim($orders, ',');

                    App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
                    App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
                    App::import('Vendor', 'src/ExampleUtils');

                    try {
                        // Get DfpUser from credentials in "../auth.ini"
                        // relative to the DfpUser.php file's directory.
                        $user = new DfpUser();
                        // Log SOAP XML request and response.
                        $user->LogDefaults();
                        // Get the LineItemService.
                        $lineItemService = $user->GetService('LineItemService', 'v201608');
                        // Create a statement to select all line items.
                        $statementBuilder = new StatementBuilder();
                        $statementBuilder->Where("id in ({$orders})");
                        $count = 1;
                        foreach ($test as $user) {
                            $statementBuilder->WithBindVariableValue('orderId' . $count, "{$user['tblLineItem']['li_dfp_id']}");
                            $count++;
                        }
                        $statementBuilder->OrderBy('id ASC')
                                ->Limit(StatementBuilder::SUGGESTED_PAGE_LIMIT);
                        // Default for total result set size.
                        $totalResultSetSize = 0;
                        do {
                            // Get line items by statement.
                            $page = $lineItemService->getLineItemsByStatement(
                                    $statementBuilder->ToStatement());
                            // Display results.
                            if (isset($page->results)) {
                                $results = (array) $page->results;
                                $this->set('results', $results);
                                $totalResultSetSize = $page->totalResultSetSize;
                                $i = $page->startIndex;
                            }
                            $statementBuilder->IncreaseOffsetBy(StatementBuilder::SUGGESTED_PAGE_LIMIT);
                        } while ($statementBuilder->GetOffset() < $totalResultSetSize);
                        //printf("Number of results found: %d\n", $totalResultSetSize);
                    } catch (OAuth2Exception $e) {
                        ExampleUtils::CheckForOAuth2Errors($e);
                    } catch (ValidationException $e) {
                        ExampleUtils::CheckForOAuth2Errors($e);
                    } catch (Exception $e) {
                        printf("%s\n", $e->getMessage());
                    }

                    //print_r($results);

                    $i = 0;
                    foreach ($results as $user):
                        $user = array($user);
                        if ($user[0]->id != "") {
                            $imp = $user[0]->stats ? $user[0]->stats->impressionsDelivered : 0;
                            $clicks = $user[0]->stats ? $user[0]->stats->clicksDelivered : 0;
                            $id = $user[0]->id;
                            $this->tblLineItem->updateAll(array("li_imp" => $imp, "li_clicks" => $clicks, "li_accepted" => $clicks), array("li_dfp_id" => $id));
                            echo "<br />" . $id;
                        } endforeach;
                }
            }
        }
        $this->autoRender = false;
        return;
    }

    public function lineItemsDayWiseUpdatefromDFP() {

        App::import('Vendor', 'src/Google/Api/Ads/Dfp/Lib/DfpUser');
        App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/ReportDownloader');
        App::import('Vendor', 'src/Google/Api/Ads/Dfp/Util/v201608/StatementBuilder');
        App::import('Vendor', 'src/ExampleUtils');


        $orders = $this->tblOrder->find("first", array('conditions' => array('tblOrder.isDFPProcessed' => 0), 'limit' => 1));

        //print_r($orders);

        if (!empty($orders['tblOrder']['dfp_order_id'])) {
            // Set the ID of the order to run the report for.
            $orderId = (int) $orders['tblOrder']['dfp_order_id'];
            try {
                // Get DfpUser from credentials in "../auth.ini"
                // relative to the DfpUser.php file's directory.
                $user = new DfpUser();
                // Log SOAP XML request and response.
                $user->LogDefaults();
                // Get the ReportService.
                $reportService = $user->GetService('ReportService', 'v201608');
                // Create report query.
                $reportQuery = new ReportQuery();
                $reportQuery->dimensions = array('ORDER_ID', 'DATE', 'LINE_ITEM_ID', 'AD_UNIT_ID');
                //$reportQuery->dimensionAttributes = array('LINE_ITEM_LIFETIME_IMPRESSIONS', 'LINE_ITEM_LIFETIME_CLICKS');
                $reportQuery->columns = array('AD_SERVER_IMPRESSIONS', 'AD_SERVER_CLICKS',
                    'AD_SERVER_CTR');
                // Create statement to filter for an order.
                $statementBuilder = new StatementBuilder();
                $statementBuilder->Where('order_id = :orderId')->WithBindVariableValue(
                        'orderId', $orderId);
                // Set the filter statement.
                $reportQuery->statement = $statementBuilder->ToStatement();
                // Set the start and end dates or choose a dynamic date range type.
                $reportQuery->dateRangeType = 'REACH_LIFETIME';
                // Create report job.
                $reportJob = new ReportJob();
                $reportJob->reportQuery = $reportQuery;
                // Run report job.
                $reportJob = $reportService->runReportJob($reportJob);
                // Create report downloader.
                $reportDownloader = new ReportDownloader($reportService, $reportJob->id);
                // Wait for the report to be ready.
                $reportDownloader->waitForReportReady();
                // Change to your file location.

                $rootPath = $_SERVER['DOCUMENT_ROOT'] . "/app/webroot/dfpreports";

                $filePath = sprintf('%s.csv.gz', tempnam($rootPath, 'delivery-report-'));
                $reportDownloader->downloadReport('CSV_DUMP', $filePath);

                //$csv = array();
                $report = gzopen($filePath, 'r');
                $fields = fgetcsv($report);
                $field_count = count($fields);
                while (($row = fgetcsv($report)) !== false) {
                    $row = array_pad($row, $field_count, null); // make sure enough columns are present
                    $item = array_combine($fields, $row); // map each column to an associative array
                    $conditions = array(
                        'tblReportDay.rd_order_id' => $item['Dimension.ORDER_ID'],
                        'tblReportDay.rd_line_item_id' => $item['Dimension.LINE_ITEM_ID'],
                        'tblReportDay.rd_ad_unit_id' => $item['Dimension.AD_UNIT_ID'],
                        'tblReportDay.rd_date' => $item['Dimension.DATE']
                    );
                    if ($this->tblReportDay->hasAny($conditions)) {
                        
                    } else {
                        $this->request->data['tblReportDay']['rd_order_id'] = $item['Dimension.ORDER_ID'];
                        $this->request->data['tblReportDay']['rd_line_item_id'] = $item['Dimension.LINE_ITEM_ID'];
                        $this->request->data['tblReportDay']['rd_ad_unit_id'] = $item['Dimension.AD_UNIT_ID'];
                        $this->request->data['tblReportDay']['rd_date'] = $item['Dimension.DATE'];
                        $this->request->data['tblReportDay']['rd_imp'] = $item['Column.AD_SERVER_IMPRESSIONS'];
                        $this->request->data['tblReportDay']['rd_clicks'] = $item['Column.AD_SERVER_CLICKS'];
                        $this->request->data['tblReportDay']['rd_ctr'] = $item['Column.AD_SERVER_CTR'];
                        $this->tblReportDay->create();
                        $this->tblReportDay->save($this->request->data);
                    }
                }
                //fclose($report);
                //print_r($csv);
                //printf("Downloading report to %s ...\n", $filePath);
                // Download the report.
                //$reportDownloader->downloadReport('CSV_DUMP', $filePath);
                //printf("done.\n");
            } catch (OAuth2Exception $e) {
                ExampleUtils::CheckForOAuth2Errors($e);
            } catch (ValidationException $e) {
                ExampleUtils::CheckForOAuth2Errors($e);
            } catch (Exception $e) {
                printf("%s\n", $e->getMessage());
            }
            $this->tblOrder->updateAll(array("isDFPProcessed" => 1), array("dfp_order_id" => $orderId));
        }

        $this->autoRender = false;
        return;
    }

    public function DedicatedEmailTrigger() {
        
    }

    public function ordersDFPProcessedReset() {
        $this->tblOrder->updateAll(array("isDFPProcessed" => 0));
        $this->autoRender = false;
        return;
    }

    public function wssTriggerPixel($mid, $email) {
        App::import('Vendor', 'src/html2text');
        App::import('Vendor', array('file' => 'autoload'));
        $mailing_id = 0;

        $fromEmail = "ts@wss-ts1.mg.das0.net";
        $MAILGUNAPP_API_KEY = "key-4eb27ee2e9dd487f1095e7191a68b969";
        $mg = new Mailgun\Mailgun($MAILGUNAPP_API_KEY);
        $siteName = "Wall Street Survivor";
        $domain = "wss-ts1.mg.das0.net";
        $batchMsg = $mg->BatchMessage($domain);

        $mailing_id = $mid;

        $m_ad_uid = $this->tblMailing->field(
                'm_ad_uid', array('m_id' => $mailing_id)
        );

        $m_cr_id = $this->tblMailing->field(
                'm_li_crtid', array('m_id' => $mailing_id)
        );

        if ($m_cr_id > 133)
            die();


        $ma_l_id = $this->tblMappedAdunit->field(
                'ma_l_id', array('ma_uid' => $m_ad_uid)
        );

        $aduidDFP = $this->tblMappedAdunit->field(
                'ma_ad_id', array('ma_uid' => $m_ad_uid)
        );

        $aduid = $this->tblAdunit->field(
                'ad_uid', array('ad_dfp_id' => $aduidDFP)
        );

        $pub_id = $this->tblAdunit->field(
                'publisher_id', array('ad_uid' => $aduid)
        );

        $user_id = $this->User->field(
                'tblProfileID', array('id' => $pub_id)
        );

        $tracksite_id = $this->tblProfile->field(
                'tracksite_id', array('profileID' => $user_id)
        );

        $this->Dynamic->useTable = "347_e979f029cc3e11e586d33417ebe5d44c";
        $tblSubscribers = $this->Dynamic->Query("SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id . " and msg_email_id='" . $email . "'");

        if (empty($tblSubscribers)) {
            $tblSubscribers = array();
            $tblSubscribers[0]['wss']['nl_email'] = $email;

            $strSubscribers = "";
            $strArray = array();
            $month_year = date('M') . '-' . date('Y');
            foreach ($tblSubscribers as $tblSubscriber) {
                $this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
                $this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber['wss']['nl_email'];
                $this->tblMessage->create();
                if ($this->tblMessage->save($this->request->data)) {
                    $myArray = array();
                    $message_id = $this->tblMessage->getLastInsertID();
                    $strArray[] = array('email' => '' . $tblSubscriber['wss']['nl_email'] . '', 'name' => '', 'type' => 'to');
                    $batchMsg->addToRecipient($tblSubscriber['wss']['nl_email'], array("first" => "", "last" => ""));
                    //$strArray[] = $myArray;
                }
            }

            $options2['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => 'text/html', 'tbc.cr_id' => $m_cr_id);
            $options2['joins'] = array(
                array('table' => 'tbl_line_items',
                    'alias' => 'tli',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tli.li_id = tblMappedAdunit.ma_l_id')),
                array('table' => 'tbl_creatives',
                    'alias' => 'tbc',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tbc.cr_lid = tli.li_dfp_id'
            )));
            $options2['fields'] = array('tbc.*', 'tblMappedAdunit.*');
            $sponsoredCreative = $this->tblMappedAdunit->find('first', $options2);
            $sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
            $sponsoredRedirect = $sponsoredCreative['tbc']['cr_redirect_url'];
            $sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=" . $tracksite_id . "&rec=1&c_n=creative_" . $sponsoredCreative['tbc']['cr_id'] . "-mailing_" . $mailing_id . "&c_p=" . $sponsoredCreative['tblMappedAdunit']['ma_uid'] . "&send_image=0";
            $sponsoredBody = $sponsoredCreative['tbc']['cr_body'] . " <img src='" . $sponsoredImpression . "' width='1' height='1'  style='display:none;' />";

            $h2t = & new html2text($sponsoredBody);
            $textVersion = $h2t->get_text();

            try {
                $batchMsg->setFromAddress($fromEmail, array("first" => $siteName));
                $batchMsg->setSubject($sponsoredTitle);
                $batchMsg->setHtmlBody($sponsoredBody);
                $batchMsg->setTextBody($textVersion);
                $batchMsg->addCampaignId('DASSOCEX-' . $mailing_id);
                $batchMsg->addCustomData("MailingId", 'DASSOCEX-' . $mailing_id);
                //print_r($batchMsg);
                $result = $batchMsg->finalize();

                $this->tblMailing->updateAll(
                        array('tblMailing.m_esp_id' => "'DASSOCEX-" . $mailing_id . "'", 'tblMailing.m_sent_date' => "'" . date('Y-m-d H:i:s') . "'", 'tblMailing.m_schedule_date' => "'" . date('Y-m-d H:i:s') . "'",
                    'tblMailing.m_sl' => "'" . $sponsoredTitle . "'"), array('tblMailing.m_id' => $mailing_id));
                echo 'Successfully sent all messages.';
                //print_r($batchMsg);
            } catch (Exception $e) {
                echo 'Message: ' . $e->getMessage();
            }
        }
        $this->autoRender = false;
        return;
    }

    public function campTriggerPixel($mid, $email) {
        App::import('Vendor', 'src/html2text');
        App::import('Vendor', array('file' => 'autoload'));
        $mailing_id = 0;

        $disclamer = "";


        if ($mid == 906 || $mid == 911) {
            $fromEmail = "newsletter@ffp-nl1.mg.das0.net";
            $MAILGUNAPP_API_KEY = "key-4eb27ee2e9dd487f1095e7191a68b969";
            $mg = new Mailgun\Mailgun($MAILGUNAPP_API_KEY);
            $siteName = "Financial Freedom Post";
            $domain = "ffp-nl1.mg.das0.net";
            $batchMsg = $mg->BatchMessage($domain);
            $disclamer = '<br>
				<br>
				<center>
				<p align="center" style="margin-top:0em;margin-bottom:0em;font-size:8px">
				<font face="Verdana, Arial, Helvetica, sans-serif"><span style="font-size:8pt;font-family:Arial">DISCLAIMER:<br>
				The content on this email is a combination of paid sponsorships and promoted articles. FinancialFreedomPost.com is not affiliated with nor does it endorse any trading system, newsletter or other similar service. FinancialFreedomPost.com does not guarantee or verify any performance claims made by such systems, newsletters or services. Trading involves a significant and substantial risk of loss and may not be suitable for everyone. You should only trade with money you can afford to lose. There is no guarantee that you will profit from your trading activity and it is possible that you may lose all of, or if trading on margin more than, your investment. Some of the results shown may be based on simulated performance. SIMULATED OR HYPOTHETICAL PERFORMANCE RESULTS HAVE CERTAIN INHERENT LIMITATIONS. UNLIKE THE RESULTS SHOWN IN AN ACTUAL PERFORMANCE RECORD, SUCH RESULTS DO NOT REPRESENT ACTUAL TRADING. ALSO, BECAUSE THE TRADES HAVE NOT ACTUALLY BEEN EXECUTED, THE RESULTS MY HAVE UNDER OR OVER-COMPENSATED FOR THE IMPACT, IF ANY, OF CERTAIN MARKET FACTORS, SUCH AS LACK OF LIQUIDITY. SIMULATED OR HYPOTHETICAL PROGRAMS IN GENERAL ARE ALSO SUBJECT TO THE FACT THAT THEY ARE DESIGNED WITH THE BENEFIT OF HINDSIGHT. NO REPRESENTATION IS BEING MADE THAT ANY ACCOUNT WILL OR IS LIKELY TO ACHIEVE PROFITS OR LOSSES SIMILAR TO THOSE SHOWN. Past performance is not necessarily indicative of future performance. This brief statement cannot disclose all the risks and other significant aspects of trading. You should carefully study trading and consider whether such activity is suitable for you in light of your circumstances and financial resources before you trade. 
				<br><br>
				This message is considered by regulation to be a commercial and advertising message. This is a permission-based message. You are receiving this email either because you opted-in to this subscription or because you have a prior existing relationship with FinancialFreedomPost.com or one of its subsidiaries, and previously provided your email address to us. This email fully complies with all laws and regulations. If you do not wish to receive this email, then we apologize for the inconvenience. You can immediately discontinue receiving this email by clicking on the un-subscribe or profile link and you will no longer receive this email. We will immediately redress any complaints you may have. If you have any questions, please send an email with your questions to newsletters@FinancialFreedomPost.com
				<br><br>
				FinancialFreedomPost.com does not recommend or promote any stock tickers, symbols or any such identifier.<br><br>
				1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br></span></font></p>
				<br/><br>To stop receiving these emails, you may <a href="%unsubscribe_url%" target="_blank">unsubscribe now</a>.
				</center>';
        }

        if ($mid == 908 || $mid == 910) {
            $fromEmail = "newsletter@dmt-nl1.mg.das0.net";
            $MAILGUNAPP_API_KEY = "key-4eb27ee2e9dd487f1095e7191a68b969";
            $mg = new Mailgun\Mailgun($MAILGUNAPP_API_KEY);
            $siteName = "Daily Money Times";
            $domain = "dmt-nl1.mg.das0.net";
            $batchMsg = $mg->BatchMessage($domain);
            $disclamer = '<br><br>
				<center>
				<p align="center" style="margin-top:0em;margin-bottom:0em;font-size:8px">
				<font face="Verdana, Arial, Helvetica, sans-serif"><span style="font-size:8pt;font-family:Arial">DISCLAIMER:<br>
				The content on this email is a combination of paid sponsorships and promoted articles. DailyMoneyTimes.com is not affiliated with nor does it endorse any trading system, newsletter or other similar service. DailyMoneyTimes.com does not guarantee or verify any performance claims made by such systems, newsletters or services. Trading involves a significant and substantial risk of loss and may not be suitable for everyone. You should only trade with money you can afford to lose. There is no guarantee that you will profit from your trading activity and it is possible that you may lose all of, or if trading on margin more than, your investment. Some of the results shown may be based on simulated performance. SIMULATED OR HYPOTHETICAL PERFORMANCE RESULTS HAVE CERTAIN INHERENT LIMITATIONS. UNLIKE THE RESULTS SHOWN IN AN ACTUAL PERFORMANCE RECORD, SUCH RESULTS DO NOT REPRESENT ACTUAL TRADING. ALSO, BECAUSE THE TRADES HAVE NOT ACTUALLY BEEN EXECUTED, THE RESULTS MY HAVE UNDER OR OVER-COMPENSATED FOR THE IMPACT, IF ANY, OF CERTAIN MARKET FACTORS, SUCH AS LACK OF LIQUIDITY. SIMULATED OR HYPOTHETICAL PROGRAMS IN GENERAL ARE ALSO SUBJECT TO THE FACT THAT THEY ARE DESIGNED WITH THE BENEFIT OF HINDSIGHT. NO REPRESENTATION IS BEING MADE THAT ANY ACCOUNT WILL OR IS LIKELY TO ACHIEVE PROFITS OR LOSSES SIMILAR TO THOSE SHOWN. Past performance is not necessarily indicative of future performance. This brief statement cannot disclose all the risks and other significant aspects of trading. You should carefully study trading and consider whether such activity is suitable for you in light of your circumstances and financial resources before you trade. 
				<br><br>
				This message is considered by regulation to be a commercial and advertising message. This is a permission-based message. You are receiving this email either because you opted-in to this subscription or because you have a prior existing relationship with DailyMoneyTimes.com or one of its subsidiaries, and previously provided your email address to us. This email fully complies with all laws and regulations. If you do not wish to receive this email, then we apologize for the inconvenience. You can immediately discontinue receiving this email by clicking on the un-subscribe or profile link and you will no longer receive this email. We will immediately redress any complaints you may have. If you have any questions, please send an email with your questions to newsletters@dailymoneytimes.com
				<br><br>
				DailyMoneyTimes.com does not recommend or promote any stock tickers, symbols or any such identifier.
				<br /><br />1001 Fischer BLVD,<br>P.O. Box 104,<br>Toms River NJ 08753<br></span></font></p>
				<br><br>To stop receiving these emails, you may <a href="%unsubscribe_url%" target="_blank">unsubscribe now</a>.
				</center>';
        }

        $mailing_id = $mid;

        $m_ad_uid = $this->tblMailing->field(
                'm_ad_uid', array('m_id' => $mailing_id)
        );

        $m_cr_id = $this->tblMailing->field(
                'm_li_crtid', array('m_id' => $mailing_id)
        );

        $ma_l_id = $this->tblMappedAdunit->field(
                'ma_l_id', array('ma_uid' => $m_ad_uid)
        );

        $aduidDFP = $this->tblMappedAdunit->field(
                'ma_ad_id', array('ma_uid' => $m_ad_uid)
        );

        $aduid = $this->tblAdunit->field(
                'ad_uid', array('ad_dfp_id' => $aduidDFP)
        );

        $pub_id = $this->tblAdunit->field(
                'publisher_id', array('ad_uid' => $aduid)
        );

        $user_id = $this->User->field(
                'tblProfileID', array('id' => $pub_id)
        );

        $tracksite_id = $this->tblProfile->field(
                'tracksite_id', array('profileID' => $user_id)
        );

        $table_name = $pub_id . "_" . $aduid;
        $this->Dynamic->useTable = $table_name;

        $tblSubscribers = $this->Dynamic->Query("SELECT `msg_email_id` FROM tbl_messages where msg_m_id=" . $mailing_id . " and msg_email_id='" . $email . "'");

        $isActive = $this->Dynamic->Query("SELECT `nl_isActive` FROM " . $table_name . " where nl_email='" . $email . "'");

        //print_r($isActive);

        if (empty($tblSubscribers) && $isActive[0][$table_name]['nl_isActive'] == 1) {
            $tblSubscribers = array();
            $tblSubscribers[0]['wss']['nl_email'] = $email;

            $strSubscribers = "";
            $strArray = array();
            $month_year = date('M') . '-' . date('Y');
            foreach ($tblSubscribers as $tblSubscriber) {
                $this->request->data['tblMessage']['msg_m_id'] = $mailing_id;
                $this->request->data['tblMessage']['msg_email_id'] = $tblSubscriber['wss']['nl_email'];
                $this->tblMessage->create();
                if ($this->tblMessage->save($this->request->data)) {
                    $myArray = array();
                    $message_id = $this->tblMessage->getLastInsertID();
                    $strArray[] = array('email' => '' . $tblSubscriber['wss']['nl_email'] . '', 'name' => '', 'type' => 'to');
                    $batchMsg->addToRecipient($tblSubscriber['wss']['nl_email'], array("first" => "", "last" => ""));
                    //$strArray[] = $myArray;
                }
            }

            $options2['conditions'] = array('tblMappedAdunit.ma_ad_id' => $aduidDFP, 'tbc.cr_status' => 'ACTIVE', 'tbc.cr_size' => 'text/html', 'tbc.cr_id' => $m_cr_id);
            $options2['joins'] = array(
                array('table' => 'tbl_line_items',
                    'alias' => 'tli',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tli.li_id = tblMappedAdunit.ma_l_id')),
                array('table' => 'tbl_creatives',
                    'alias' => 'tbc',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tbc.cr_lid = tli.li_dfp_id'
            )));
            $options2['fields'] = array('tbc.*', 'tblMappedAdunit.*');
            $sponsoredCreative = $this->tblMappedAdunit->find('first', $options2);
            $sponsoredTitle = $sponsoredCreative['tbc']['cr_header'];
            $sponsoredRedirect = $sponsoredCreative['tbc']['cr_redirect_url'];
            $sponsoredImpression = "http://tracking.digitaladvertising.systems/piwik.php?idsite=" . $tracksite_id . "&rec=1&c_n=creative_" . $sponsoredCreative['tbc']['cr_id'] . "-mailing_" . $mailing_id . "&c_p=" . $sponsoredCreative['tblMappedAdunit']['ma_uid'] . "&send_image=0";
            $sponsoredBody = $sponsoredCreative['tbc']['cr_body'] . " <img src='" . $sponsoredImpression . "' width='1' height='1'  style='display:none;' />" . $disclamer;

            $h2t = & new html2text($sponsoredBody);
            $textVersion = $h2t->get_text();

            try {
                $batchMsg->setFromAddress($fromEmail, array("first" => $siteName));
                $batchMsg->setSubject($sponsoredTitle);
                $batchMsg->setHtmlBody($sponsoredBody);
                $batchMsg->setTextBody($textVersion);
                $batchMsg->addCampaignId('DASSOCEX-' . $mailing_id);
                $batchMsg->addCustomData("MailingId", 'DASSOCEX-' . $mailing_id);
                //print_r($batchMsg);
                $result = $batchMsg->finalize();

                $this->tblMailing->updateAll(
                        array('tblMailing.m_esp_id' => "'DASSOCEX-" . $mailing_id . "'", 'tblMailing.m_sent_date' => "'" . date('Y-m-d H:i:s') . "'", 'tblMailing.m_schedule_date' => "'" . date('Y-m-d H:i:s') . "'",
                    'tblMailing.m_sl' => "'" . $sponsoredTitle . "'"), array('tblMailing.m_id' => $mailing_id));
                echo 'Successfully sent all messages.';
                //print_r($batchMsg);
            } catch (Exception $e) {
                echo 'Message: ' . $e->getMessage();
            }
        }
        $this->autoRender = false;
        return;
    }

    function getFeed($feed_url) {
        $rss = new DOMDocument();
        $rss->load($feed_url);
        $feed = array();
        foreach ($rss->getElementsByTagName('item') as $node) {
            $item = array(
                'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
                'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
                'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
                'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
            );
            array_push($feed, $item);
        }
        return $feed;
    }

    function wssRegistrationTrigger() {

        $this->Dynamic->useTable = "241_d780e550493611e6b91e123aa499f9d5";
        $tblSubscribers1 = $this->Dynamic->Query("SELECT `nl_email`, `nl_fname`, `nl_lname` FROM 241_d780e550493611e6b91e123aa499f9d5 WHERE nl_create  >= SUBDATE(DATE(CURDATE()),1) AND nl_create <SUBDATE(DATE(CURDATE()),0) and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=1017 )");
        $tblSubscribers2 = $this->Dynamic->Query("SELECT `nl_email`, `nl_fname`, `nl_lname` FROM 241_d780e550493611e6b91e123aa499f9d5 WHERE nl_create  >= SUBDATE(DATE(CURDATE()),2) AND nl_create <SUBDATE(DATE(CURDATE()),1) and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=1018 )");
        $tblSubscribers4 = $this->Dynamic->Query("SELECT `nl_email`, `nl_fname`, `nl_lname` FROM 241_d780e550493611e6b91e123aa499f9d5 WHERE nl_create  >= SUBDATE(DATE(CURDATE()),4) AND nl_create <SUBDATE(DATE(CURDATE()),3) and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=1019 )");
        $tblSubscribers5 = $this->Dynamic->Query("SELECT `nl_email`, `nl_fname`, `nl_lname` FROM 241_d780e550493611e6b91e123aa499f9d5 WHERE nl_create  >= SUBDATE(DATE(CURDATE()),5) AND nl_create <SUBDATE(DATE(CURDATE()),4) and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=1020 )");
        $tblSubscribers6 = $this->Dynamic->Query("SELECT `nl_email`, `nl_fname`, `nl_lname` FROM 241_d780e550493611e6b91e123aa499f9d5 WHERE nl_create  >= SUBDATE(DATE(CURDATE()),6) AND nl_create <SUBDATE(DATE(CURDATE()),5) and nl_email NOT IN ( SELECT `msg_email_id` FROM tbl_messages where msg_m_id=1021 )");


        if (!empty($tblSubscribers1)) {


            $this->requestAction('/sendgrid/TriggerSeries', array('tblSubscribers' => $tblSubscribers1, 'esp_id' => 5, 'mailing_id' => 1017, 'SubjectLine' => '%%first_name%% Be On Your Way to Something Big'));
        }
        if (!empty($tblSubscribers2)) {


            $this->requestAction('/sendgrid/TriggerSeries', array('tblSubscribers' => $tblSubscribers2, 'esp_id' => 5, 'mailing_id' => 1018, 'SubjectLine' => 'Did You Join a League Yet, %%first_name%%'));
        }
        if (!empty($tblSubscribers4)) {


            $this->requestAction('/sendgrid/TriggerSeries', array('tblSubscribers' => $tblSubscribers4, 'esp_id' => 5, 'mailing_id' => 1019, 'SubjectLine' => '%%first_name%% - Check Out Giants of Silicon Valley'));
        }
        if (!empty($tblSubscribers5)) {


            $this->requestAction('/sendgrid/TriggerSeries', array('tblSubscribers' => $tblSubscribers5, 'esp_id' => 5, 'mailing_id' => 1020, 'SubjectLine' => 'Almost a Week, Finished Your First Course, Right %%first_name%%?'));
        }
        if (!empty($tblSubscribers6)) {


            $this->requestAction('/sendgrid/TriggerSeries', array('tblSubscribers' => $tblSubscribers6, 'esp_id' => 5, 'mailing_id' => 1021, 'SubjectLine' => 'Final Call  %%first_name%%, Lock in Your Savings Today!'));
        }

        $this->autoRender = false;
    }

    function getRegistrationUsers() {

        header('Content-Type: application/csv');
        $destination = Configure::read('Path.url_csvPath') . 'RegistrationUsers.csv';
        header('Content-Disposition: attachment; filename=' . $destination);
        header('Pragma: no-cache');
        readfile("/");


        //header("Content-Type: text/csv; charset=utf-8");
        //header("Content-Disposition: attachment; filename={$fileName}");

        $this->Dynamic->useTable = "241_d780e550493611e6b91e123aa499f9d5";
        $tblSubscribers1 = $this->Dynamic->Query("SELECT `nl_email` FROM 241_d780e550493611e6b91e123aa499f9d5 WHERE nl_create  >= SUBDATE(DATE(CURDATE()),1) AND nl_create <SUBDATE(DATE(CURDATE()),0)");

        $tblSubscribers1 = json_decode(json_encode($tblSubscribers1), true);
        //print_r($tblSubscribers1);
        $i = 0;
        foreach ($tblSubscribers1 as $emailID) {
            //echo $emailID['241_d780e550493611e6b91e123aa499f9d5']['nl_email'];
            $prod[$i]['EmailAddress'] = $emailID['241_d780e550493611e6b91e123aa499f9d5']['nl_email'];
            $i++;
        }
        //ob_start();
        $fh = @fopen('php://output', 'w');

        $headerDisplayed = false;

        foreach ($prod as $data) {
            // Add a header row if it hasn't been added yet
            if (!$headerDisplayed) {
                // Use the keys from $data as the titles
                fputcsv($fh, array_keys($data));
                $headerDisplayed = true;
            }

            // Put the data into the stream
            fputcsv($fh, $data);
        }
        //$filename = "RegistrationUsers.csv"; // Trying to save file in server
        // Close the file
        fclose($fh);
        //file_put_contents($destination, $fh);
        //$this->autoRender = false;
        //return ob_get_clean();
    }

    function upload_csv() {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->Dynamic->useTable = "241_d780e550493611e6b91e123aa499f9d5";
        $data = $this->Dynamic->Query("SELECT `nl_email` FROM 241_d780e550493611e6b91e123aa499f9d5 WHERE nl_create  >= SUBDATE(DATE(CURDATE()),1) AND nl_create <SUBDATE(DATE(CURDATE()),0)");
        // $tblSubscribers1 = json_decode(json_encode($tblSubscribers1), true);
        $i = 0;
        foreach ($data as $emailID) {
            $results[$i]['email'] = $emailID['241_d780e550493611e6b91e123aa499f9d5']['nl_email'];
            $i++;
        }
        $name = time();
        $filePath = getcwd() . '/csv/' . $name . '.csv';
        $filePath = $_SERVER['DOCUMENT_ROOT'] . '/app/webroot/csv/' . $name . '.csv';
        $myfile = fopen($filePath, 'a+');

        foreach ($results as $product) {
            fputcsv($myfile, $product);
        }

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($filePath) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filePath));
        readfile($filePath);
        //$this->mail_attachment($name . '.csv', $filePath, 'mohsintech@gmail.com', 'no-reply@siliconbiz.com', 'no-reply@siliconbiz.com', 'New Registed Users', '');
    }

    function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $subject, $message) {
        echo $filename . '.......';
        $file = $path;
        $file_size = filesize($file);
        print_r($file);
        $handle = fopen($file, "r");
        $content = fread($handle, $file_size);
        fclose($handle);

        $content = chunk_split(base64_encode($content));
        $uid = md5(uniqid(time()));
        $name = basename($file);

        $eol = PHP_EOL;

        $header = "From: " . $from_name . " <" . $from_mail . ">\n";
        $header .= "Reply-To: " . $from_name . "\n";
        $header .= "MIME-Version: 1.0\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\n\n";
        $emessage = "--" . $uid . "\n";
        $emessage.= "Content-type:text/plain; charset=iso-8859-1\n";
        $emessage.= "Content-Transfer-Encoding: 8bit\n\n";
        $emessage .= $message . "\n\n";
        $emessage.= "--" . $uid . "\n";
        $emessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\n"; // use different content types here
        $emessage .= "Content-Transfer-Encoding: base64\n";
        $emessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\n\n";
        $emessage .= $content . "\n\n";
        $emessage .= "--" . $uid . "--";

//        $file = $path;
//        $file_size = filesize($file);
//        $handle = fopen($file, "r");
//        $content = fread($handle, $file_size);
//        fclose($handle);
//        $content = chunk_split(base64_encode($content));
//
//        $uid = md5(uniqid(time()));
//
//        $header = "From: " . $from_name . " <" . $from_mail . ">\r\n";
//        $header .= "MIME-Version: 1.0\r\n";
//        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
//        $header .= "This is a multi-part message in MIME format.\r\n";
//        $header .= "--" . $uid . "\r\n";
//        $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
//        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
//        $header .= $message . "\r\n\r\n";
//        $header .= "--" . $uid . "\r\n";
//        $header .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n";
//        $header .= "Content-Transfer-Encoding: base64\r\n";
//        $header .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
//        $header .= $content . "\r\n\r\n";
//        $header .= "--" . $uid . "--";
        // Messages for testing only, nobody will see them unless this script URL is visited manually
        if (mail($mailto, $subject, "", $header)) {
            echo "Message sent!";
        } else {
            echo "ERROR sending message.";
        }
    }

    private function array2csv($array) {
        if (count($array) == 0) {
            return null;
        }
        $lista = $array;
        ob_start();
        $fp = fopen('php://output', 'w');
        foreach ($array as $campos) {
            fputcsv($fp, $campos);
        }
        fclose($fp);
        return ob_get_clean();
    }

    public function sendCSVFile() {

        App::import('Vendor', 'src/html2text');
        App::import('Vendor', array('file' => 'autoload'));

        $sendgrid = new SendGrid('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');


        //$this->Dynamic->useTable = "241_d780e550493611e6b91e123aa499f9d5"; 
        //$tblSubscribers1 = $this->Dynamic->Query("SELECT `nl_email` FROM 241_d780e550493611e6b91e123aa499f9d5 WHERE nl_create  >= SUBDATE(DATE(CURDATE()),1) AND nl_create <SUBDATE(DATE(CURDATE()),0)");
        //$tblSubscribers1 = json_decode(json_encode($tblSubscribers1),true);
        //$data = $this->reqProducts();
        //prepareParams is csv templating and formatting function.
        // $products = $this->prepareParams($tblSubscribers1);
        //$products = $this->array2csv($tblSubscribers1);
        //$products = $this->getRegistrationUsers();
        //print_r($products);

        $filePath = $_SERVER['DOCUMENT_ROOT'] . '/app/webroot/csv';
        $fileName = "RegistrationUsers2.csv";
        $body = "Hello, you have an attached file.";

        $email = new SendGrid\Email();
        $email
                ->addTo('mohd.kamil@siliconbiztech.com')
                ->setFrom('sunny.gulati@siliconbiztech.com')
                ->setSubject($body)
                ->setText($body)
                ->setHtml('<strong>Hello World!</strong>')
                ->addAttachment('@' . $filePath . '/' . $fileName);

        try {
            $res = $sendgrid->send($email);
            var_dump($res);
        } catch (\SendGrid\Exception $e) {
            echo $e->getCode();
            foreach ($e->getErrors() as $er) {
                echo $er;
            }
        }

        $this->autoRender = false;
    }

    function wssleads() {
        $this->autoLayout = false;
        $this->autoRender = false;
    }

    function get_csv_data($file_name) {
        $this->autoLayout = false;
        $this->autoRender = false;
        $file = getcwd() . DS . 'files' . DS . 'flight' . DS . $file_name;
        $csv = array_map('str_getcsv', file($file));
        $csv_data = array();
        foreach ($csv as $i => $c) {
            $csv_data[$i] = $c[0];
        }
        return json_encode($csv_data);
    }

    function lead_post_testing(){
        $this->autoLayout=false;
        $this->autoRender=false;
        $email = $_REQUEST['email'];
        if(empty($email)){
            $response = "BADREQUEST";
        }else{
            $response= "SUCCESS";
        }
        return $response;
    }
}
