<?php

class CoregOffersController extends AppController {

    /**
     * Components
     */
    var $uses = array('Authake.Group', 'Authake.User', 'Authake.tblAdunit', 'Authake.tblLineItems', 'Authake.tblFlightAudiences', 'Authake.tblFlight',);
    var $components = array('RequestHandler', 'Authake.Filter', 'Session'); // var $layout = 'authake';
    var $paginate = array('limit' => 1000, 'order' => array('User.login' => 'asc')); //var $scaffold;

    /**
     * Before Filter callback
     */

    public function beforeFilter() {
        parent::beforeFilter();

        // Change layout for Ajax requests
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
    }

    public function index() {
        $this->autoRender = false;
        if ($_GET['email'] && $_GET['aduid']) {
            $email = $_GET['email'];
            $userProfile = $this->requestAction('/CouchDB/userProfile', array('email' => $email));
            //pr($userProfile);die;
            $options_ad['joins'] = array(
                array('table' => 'tbl_mapped_adunits',
                    'alias' => 'tma',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tma.ma_l_id = tblLineItems.li_id'
                    )),
                array('table' => 'tbl_adunits',
                    'alias' => 'tblAdunit',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tma.ma_ad_id = tblAdunit.ad_dfp_id'
                    )),
                array('table' => 'tbl_orders',
                    'alias' => 'to',
                    'type' => 'INNER',
                    'conditions' => array(
                        'to.dfp_order_id = tblLineItems.li_order_id'
                    )),
                array('table' => 'tbl_categories',
                    'alias' => 'tc',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tc.id = tblLineItems.li_category_id'
                    ))
            );

            $options_ad['conditions'] = array('tblAdunit.ad_uid' => $_GET['aduid']);
            $options_ad['fields'] = array('tblLineItems.li_name', 'tblLineItems.rank', 'tma.ma_fl_id', 'tma.ma_ecpm', 'tma.ma_uid', 'tblAdunit.adunit_name', 'tblLineItems.li_id', 'to.order_name', 'tc.category_name');
            $options_ad['order'] = 'tma.ma_ecpm desc';
            $this->tblLineItems->virtualFields['rank'] = 0;
            $tblLineItems = $this->tblLineItems->find('all', $options_ad);
            $i = 0;
            $format = 'Y-m-d H:i:s';
            //if Already Registered remove it
            foreach ($tblLineItems as $tblLineItem) {
                $isUnset = false;
                $isCategoryTaken = false;
                $count = 0;
                $count_category = 0;
                $li_categories = array();
                if (isset($userProfile->OffersRegistered) && is_array($userProfile->OffersRegistered)) {
                    foreach ($userProfile->OffersRegistered as $offer) {
                        if ($offer->li_id == $tblLineItem['tblLineItems']['li_id'] && strtotime($offer->offer_taken_at) > strtotime('-90 days')) {
                            $isUnset = true;
                        }
                    }
                }
                if (isset($userProfile->OffersShown) && is_array($userProfile->OffersShown)) {
                    foreach ($userProfile->OffersShown as $offer) {
                        if ($offer->li_id == $tblLineItem['tblLineItems']['li_id'] && strtotime($offer->offer_shown_at) > strtotime('-30 days')) {
                            $count++;
                        }
                    }
                }
                if (isset($userProfile->OffersShown) && is_array($userProfile->OffersShown)) {
                    foreach ($userProfile->OffersShown as $offer) {
                        if ($offer->li_category == $tblLineItem['tc']['category_name'] && strtotime($offer->offer_shown_at) > strtotime('-30 days')) {
                            $count_category++;
                        }
                    }
                }

                if ($count_category > 5) {
                    if (isset($userProfile->OffersRegistered) && is_array($userProfile->OffersRegistered)) {
                        foreach ($userProfile->OffersRegistered as $offer) {
                            if ($offer->li_category == $tblLineItem['tc']['category_name'] && strtotime($offer->offer_taken_at) > strtotime('-30 days')) {
                                $isCategoryTaken = true;
                            }
                        }
                    }
                }

                if ($count >= 3)
                    $tblLineItems[$i]['tblLineItems']['rank'] = $tblLineItems[$i]['tblLineItems']['rank'] - 1;

                if ($count_category > 5 && !$isCategoryTaken)
                    $tblLineItems[$i]['tblLineItems']['rank'] = $tblLineItems[$i]['tblLineItems']['rank'] - 1;

                $flightid = $tblLineItem['tma']['ma_fl_id'];
                // find here flilght fl_audience_segment
                $flights = $this->tblFlight->find('first', array('fields' => array('tblFlight.fl_audience_segment'), 'conditions' => array('fl_id' => $flightid)));

                if (!empty($flights['tblFlight']['fl_audience_segment'])) {
                    $fl_audience = json_decode($flights['tblFlight']['fl_audience_segment']);
                    //dummy data
                    $fl_audience = json_decode('{"condition": "OR","rules": [{"id": "KMA_AGEHOH","field": "KMA_AGEHOH","type": "integer","input": "select","operator": "less","value": "F"},{"id": "GENDER","field": "GENDER","type": "integer","input": "radio","operator": "equal","value": "F"},{"id": "KMA_EDUC","field": "KMA_EDUC","type": "integer","input": "select","operator": "equal","value": "D"},{"id": "KMA_HHINC","field": "KMA_HHINC","type": "integer","input": "select","operator": "equal","value": "C"},{"id": "KMA_OCCUP","field": "KMA_OCCUP","type": "integer","input": "select","operator": "equal","value": "D"},{"id": "SOURCE","field": "SOURCE","type": "string","input": "text","operator": "not_contains","value": "rrr"}]}');

                    $match = false; // if no match in multiple rule then unset 
                    foreach ($fl_audience->rules as $rules) {
                        if (count($fl_audience->rules) > 0) {
                            if ($fl_audience->condition == 'OR') {
                                // checking find in any on then unset lineitem								
                                switch ($rules->id) {
                                    case 'KMA_AGEHOH':
                                        if ((self::audience_age($userProfile->KMA_AGEHOH, $rules))) {
                                            $match = true;
                                            break;
                                        }
                                    case 'GENDER':
                                        if ((self::audience_gender($userProfile->GENDER, $rules))) {
                                            $match = true;
                                            break;
                                        }
                                    case 'KMA_EDUC':
                                        if ((self::audience_education($userProfile->KMA_EDUC, $rules))) {
                                            $match = true;
                                            break;
                                        }
                                    case 'KMA_HHINC':
                                        if ((self::audience_income($userProfile->KMA_HHINC, $rules))) {
                                            $match = true;
                                            break;
                                        }
                                    case 'KMA_OCCUP':
                                        if ((self::audience_occupation($userProfile->KMA_OCCUP, $rules))) {
                                            $match = true;
                                            break;
                                        }
                                    default:
                                        $match = false;
                                }
                                // match then break in Or condition 
                                if ($match) {
                                    break; // beak foreach loop
                                }
                            } else {
                                // AND condition of Aduance rule
                                switch ($rules->id) {
                                    case 'KMA_AGEHOH':
                                        if ((self::audience_age($userProfile->KMA_AGEHOH, $rules))) {
                                            $match = true;
                                            break;
                                        }
                                    case 'GENDER':
                                        if ((self::audience_gender($userProfile->GENDER, $rules))) {
                                            $match = true;
                                            break;
                                        }
                                    case 'KMA_EDUC':
                                        if ((self::audience_education($userProfile->KMA_EDUC, $rules))) {
                                            $match = true;
                                            break;
                                        }
                                    case 'KMA_HHINC':
                                        if ((self::audience_income($userProfile->KMA_HHINC, $rules))) {
                                            $match = true;
                                            break;
                                        }
                                    case 'KMA_OCCUP':
                                        if ((self::audience_occupation($userProfile->KMA_OCCUP, $rules))) {
                                            $match = true;
                                            break;
                                        }
                                    default:
                                        $match = false;
                                }
                                // match then break in Or condition 
                                if (!$match) {

                                    //unset here 
                                    unset($tblLineItems[$i]);
                                    break; // beak foreach loop
                                }
                            }
                        } else {
                            // if there are no rule then unset 
                            unset($tblLineItems[$i]);
                        }
                    }
                    if (!$match) {
                        // unsert here 
                        unset($tblLineItems[$i]);
                    }
                }
//                $options_fl['joins'] = array(
//                    array('table' => 'tbl_audience_segments',
//                        'alias' => 'tas',
//                        'type' => 'INNER',
//                        'conditions' => array(
//                            'tas.kma_id = tblFlightAudiences.fl_as_id'
//                        )),
//                    array('table' => 'tbl_audience_segments',
//                        'alias' => 'tasp',
//                        'type' => 'LEFT',
//                        'conditions' => array(
//                            'tasp.kma_id = tas.parent_id'
//                )));
//                $options_fl['conditions'] = array('tblFlightAudiences.fl_flight_id' => $flightid);
//                $options_fl['fields'] = array('tas.kma_datapoint', 'tasp.kma_datapoint');
//                $segments = $this->tblFlightAudiences->find('all', $options_fl);
//                echo "<pre>";
//                print_r($segments);
//                echo "</pre>";
//                foreach ($segments as $segment) {
//                    if ($segment['tas']['kma_datapoint'] != "" && $segment['tasp']['kma_datapoint'] != "") {
//                        if ($userProfile->$segment['tasp']['kma_datapoint'] == $segment['tas']['kma_datapoint'])
//                            $tblLineItems[$i]['tblLineItems']['rank'] = $tblLineItems[$i]['tblLineItems']['rank'] + 1;
//                        else
//                            unset($tblLineItems[$i]);
//                    } else {
//                        if ($userProfile->$segment['tas']['kma_datapoint'] == 'Y')
//                            $tblLineItems[$i]['tblLineItems']['rank'] = $tblLineItems[$i]['tblLineItems']['rank'] + 1;
//                        else
//                            unset($tblLineItems[$i]);
//                    }
//                }

                if ($isUnset)
                    unset($tblLineItems[$i]);

                $i++;
            }

            usort($tblLineItems, function($a, $b) {
                if ($a['tblLineItems']['rank'] == $b['tblLineItems']['rank']) {
                    if ($a['tma']['ma_ecpm'] == $b['tma']['ma_ecpm'])
                        return 0;
                    return $a['tma']['ma_ecpm'] < $b['tma']['ma_ecpm'] ? 1 : -1;
                }
                return $a['tblLineItems']['rank'] < $b['tblLineItems']['rank'] ? 1 : -1;
            });


            $j = 1;
            $tblLineItems = array_filter($tblLineItems);
            if (is_array($tblLineItems) && !empty($tblLineItems)) {
                foreach ($tblLineItems as $tblLineItem) {
                    //echo $j . ". " . $tblLineItem['tblLineItems']['li_name'] . " (<a  target='_blank' href='/CoregOffers/signedOffer/?email=".$email."&li_id=".$tblLineItem['tblLineItems']['li_id']."&aduid=".$_GET['aduid']."'>Sign UP</a>) Rank : ".$tblLineItem['tblLineItems']['rank']." - eCPM : ".$tblLineItem['tma']['ma_ecpm']."<br>";
                    echo $j . ". " . $tblLineItem['tblLineItems']['li_name'] . " (<a  target='_blank' href='/CoregOffers/signedOffer/?email=" . $email . "&li_id=" . $tblLineItem['tblLineItems']['li_id'] . "&aduid=" . $_GET['aduid'] . "'>Sign UP</a>)<br>";
                    $viewedOffers = $this->requestAction('/CouchDB/saveViewOffers', array('email' => $email, 'li_id' => $tblLineItem['tblLineItems']['li_id'], 'ma_uid' => $tblLineItem['tma']['ma_uid'], 'ad_name' => $tblLineItem['tblAdunit']['adunit_name'], 'order_name' => $tblLineItem['to']['order_name'], 'li_name' => $tblLineItem['tblLineItems']['li_name'], 'category_name' => $tblLineItem['tc']['category_name']));
                    $j++;

                    if ($j == 4)
                        break;
                }
            } else {
                echo "Sorry no more offers to display";
            }
        }
        //print_r($userProfile);
    }

    public function signedOffer() {
        $this->autoRender = false;
        $email = $_GET['email'];
        $userProfile = $this->requestAction('/CouchDB/userProfile', array('email' => $email));
        $options_ad['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tma',
                'type' => 'INNER',
                'conditions' => array(
                    'tma.ma_l_id = tblLineItems.li_id'
                )),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tma.ma_ad_id = tblAdunit.ad_dfp_id'
                )),
            array('table' => 'tbl_orders',
                'alias' => 'to',
                'type' => 'INNER',
                'conditions' => array(
                    'to.dfp_order_id = tblLineItems.li_order_id'
                )),
            array('table' => 'tbl_categories',
                'alias' => 'tc',
                'type' => 'INNER',
                'conditions' => array(
                    'tc.id = tblLineItems.li_category_id'
                ))
        );

        $options_ad['conditions'] = array('tma.ma_uid' => $_GET['ma_uid']);
        $options_ad['fields'] = array('tblLineItems.li_name', 'tblLineItems.rank', 'tma.ma_fl_id', 'tma.ma_ecpm', 'tma.ma_uid', 'tblAdunit.adunit_name', 'tblLineItems.li_id', 'to.order_name', 'tc.category_name');
        $options_ad['order'] = 'tma.ma_ecpm desc';
        $this->tblLineItems->virtualFields['rank'] = 0;
        $tblLineItem = $this->tblLineItems->find('first', $options_ad);

        $viewedOffers = $this->requestAction('/CouchDB/saveSignedOffers', array('email' => $_GET['email'], 'li_id' => $tblLineItem['tblLineItems']['li_id'], 'ma_uid' => $tblLineItem['tma']['ma_uid'], 'ad_name' => $tblLineItem['tblAdunit']['adunit_name'], 'order_name' => $tblLineItem['to']['order_name'], 'li_name' => $tblLineItem['tblLineItems']['li_name'], 'category_name' => $tblLineItem['tc']['category_name']));

        echo "Thanks for Signing Up";
    }

    public function coregLiveOffers() {
        $this->autoRender = false;
        if ($_GET['email'] && $_GET['aduid']) {
            $email = $_GET['email'];
            $userProfile = $this->requestAction('/CouchDB/userProfile', array('email' => $email));
            $options_ad['joins'] = array(
                array('table' => 'tbl_mapped_adunits',
                    'alias' => 'tma',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tma.ma_l_id = tblLineItems.li_id'
                    )),
                array('table' => 'tbl_flights',
                    'alias' => 'tfl',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tma.ma_fl_id = tfl.fl_id'
                    )),
                array('table' => 'tbl_adunits',
                    'alias' => 'tblAdunit',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tma.ma_ad_id = tblAdunit.ad_dfp_id'
                    )),
                array('table' => 'tbl_orders',
                    'alias' => 'to',
                    'type' => 'INNER',
                    'conditions' => array(
                        'to.dfp_order_id = tblLineItems.li_order_id'
                    )),
                array('table' => 'tbl_categories',
                    'alias' => 'tc',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tc.id = tblLineItems.li_category_id'
                    ))
            );

            $options_ad['conditions'] = array('tblAdunit.ad_uid' => $_GET['aduid'], 'tma.ma_isActive' => 1, 'to.status' => 1, 'tblLineItems.tbl_lineitem_status' => 1, 'tfl.status' => 1);
            $options_ad['fields'] = array('tblLineItems.li_name', 'tblLineItems.rank', 'tma.ma_fl_id', 'tma.ma_ecpm', 'tma.ma_uid', 'tblAdunit.adunit_name', 'tblLineItems.li_id', 'to.order_name', 'tc.category_name');
            $options_ad['order'] = 'tma.ma_ecpm desc';
            $this->tblLineItems->virtualFields['rank'] = 0;
            $tblLineItems = $this->tblLineItems->find('all', $options_ad);
            $i = 0;
            $format = 'Y-m-d H:i:s';
            //if Already Registered remove it
            foreach ($tblLineItems as $tblLineItem) {
                $isUnset = false;
                $isCategoryTaken = false;
                $count = 0;
                $count_category = 0;
                $li_categories = array();
                if (isset($userProfile->OffersRegistered) && is_array($userProfile->OffersRegistered)) {
                    foreach ($userProfile->OffersRegistered as $offer) {
                        if ($offer->li_id == $tblLineItem['tblLineItems']['li_id'] && strtotime($offer->offer_taken_at) > strtotime('-90 days')) {
                            $isUnset = true;
                        }
                    }
                }
                if (isset($userProfile->OffersShown) && is_array($userProfile->OffersShown)) {
                    foreach ($userProfile->OffersShown as $offer) {
                        if ($offer->li_id == $tblLineItem['tblLineItems']['li_id'] && strtotime($offer->offer_shown_at) > strtotime('-30 days')) {
                            $count++;
                        }
                    }
                }
                if (isset($userProfile->OffersShown) && is_array($userProfile->OffersShown)) {
                    foreach ($userProfile->OffersShown as $offer) {
                        if ($offer->li_category == $tblLineItem['tc']['category_name'] && strtotime($offer->offer_shown_at) > strtotime('-30 days')) {
                            $count_category++;
                        }
                    }
                }

                if ($count_category > 5) {
                    if (isset($userProfile->OffersRegistered) && is_array($userProfile->OffersRegistered)) {
                        foreach ($userProfile->OffersRegistered as $offer) {
                            if ($offer->li_category == $tblLineItem['tc']['category_name'] && strtotime($offer->offer_taken_at) > strtotime('-30 days')) {
                                $isCategoryTaken = true;
                            }
                        }
                    }
                }

                if ($count >= 3)
                    $tblLineItems[$i]['tblLineItems']['rank'] = $tblLineItems[$i]['tblLineItems']['rank'] - 1;

                if ($count_category > 5 && !$isCategoryTaken)
                    $tblLineItems[$i]['tblLineItems']['rank'] = $tblLineItems[$i]['tblLineItems']['rank'] - 1;

                $flightid = $tblLineItem['tma']['ma_fl_id'];
                $options_fl['joins'] = array(
                    array('table' => 'tbl_audience_segments',
                        'alias' => 'tas',
                        'type' => 'INNER',
                        'conditions' => array(
                            'tas.kma_id = tblFlightAudiences.fl_as_id'
                        )),
                    array('table' => 'tbl_audience_segments',
                        'alias' => 'tasp',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'tasp.kma_id = tas.parent_id'
                )));
                $options_fl['conditions'] = array('tblFlightAudiences.fl_flight_id' => $flightid);
                $options_fl['fields'] = array('tas.kma_datapoint', 'tasp.kma_datapoint');
                $segments = $this->tblFlightAudiences->find('all', $options_fl);
                foreach ($segments as $segment) {
                    if ($segment['tas']['kma_datapoint'] != "" && $segment['tasp']['kma_datapoint'] != "") {
                        if ($userProfile->$segment['tasp']['kma_datapoint'] == $segment['tas']['kma_datapoint'])
                            $tblLineItems[$i]['tblLineItems']['rank'] = $tblLineItems[$i]['tblLineItems']['rank'] + 1;
                        else
                            unset($tblLineItems[$i]);
                    } else {
                        if ($userProfile->$segment['tas']['kma_datapoint'] == 'Y')
                            $tblLineItems[$i]['tblLineItems']['rank'] = $tblLineItems[$i]['tblLineItems']['rank'] + 1;
                        else
                            unset($tblLineItems[$i]);
                    }
                }

                if ($isUnset)
                    unset($tblLineItems[$i]);

                $i++;
            }

            usort($tblLineItems, function($a, $b) {
                if ($a['tblLineItems']['rank'] == $b['tblLineItems']['rank']) {
                    if ($a['tma']['ma_ecpm'] == $b['tma']['ma_ecpm'])
                        return 0;
                    return $a['tma']['ma_ecpm'] < $b['tma']['ma_ecpm'] ? 1 : -1;
                }
                return $a['tblLineItems']['rank'] < $b['tblLineItems']['rank'] ? 1 : -1;
            });

            $tblLineItems = array_filter($tblLineItems);
            if (is_array($tblLineItems) && !empty($tblLineItems)) {
                foreach ($tblLineItems as $tblLineItem) {
                    $viewedOffers = $this->requestAction('/CouchDB/saveViewOffers', array('email' => $email, 'li_id' => $tblLineItem['tblLineItems']['li_id'], 'ma_uid' => $tblLineItem['tma']['ma_uid'], 'ad_name' => $tblLineItem['tblAdunit']['adunit_name'], 'order_name' => $tblLineItem['to']['order_name'], 'li_name' => $tblLineItem['tblLineItems']['li_name'], 'category_name' => $tblLineItem['tc']['category_name']));
                    echo '' . $tblLineItem['tma']['ma_uid'] . ',';
                }
            } else {
                echo "";
            }
        }

        if (!$_GET['email'] && $_GET['aduid']) {
            $options_ad['joins'] = array(
                array('table' => 'tbl_mapped_adunits',
                    'alias' => 'tma',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tma.ma_l_id = tblLineItems.li_id'
                    )),
                array('table' => 'tbl_adunits',
                    'alias' => 'tblAdunit',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tma.ma_ad_id = tblAdunit.ad_dfp_id'
                    )),
                array('table' => 'tbl_orders',
                    'alias' => 'to',
                    'type' => 'INNER',
                    'conditions' => array(
                        'to.dfp_order_id = tblLineItems.li_order_id'
                    )),
                array('table' => 'tbl_categories',
                    'alias' => 'tc',
                    'type' => 'INNER',
                    'conditions' => array(
                        'tc.id = tblLineItems.li_category_id'
                    ))
            );

            $options_ad['conditions'] = array('tblAdunit.ad_uid' => $_GET['aduid'], 'tma.ma_isActive' => 1);
            $options_ad['fields'] = array('tblLineItems.li_name', 'tblLineItems.rank', 'tma.ma_fl_id', 'tma.ma_ecpm', 'tma.ma_uid', 'tblAdunit.adunit_name', 'tblLineItems.li_id', 'to.order_name', 'tc.category_name');
            $options_ad['order'] = 'tma.ma_ecpm desc';
            $this->tblLineItems->virtualFields['rank'] = 0;
            $tblLineItems = $this->tblLineItems->find('all', $options_ad);
            $i = 0;
            $format = 'Y-m-d H:i:s';
            //if Already Registered remove it
            foreach ($tblLineItems as $tblLineItem) {
                echo '' . $tblLineItem['tma']['ma_uid'] . ',';
            }
        }
        //print_r($userProfile);
    }

    /**
     * Gender Audience Tageting
     * @param type $userProfile
     * @param type $rules
     * @return boolean
     */
    function audience_gender($gendervalue, $rules) {

        $response = false;
        if (($rules->id == 'GENDER') && $rules->value == $gendervalue) { // Checking GENDER Audience tageting 
            $response = false;
        }

        return $response;
    }

    /**
     * KMA_EDUC
     * @param type $userProfile
     * @param type $rules
     * @return boolean
     */
    function audience_education($kma_educ_value, $rules) {  // Checking Eduction Audience tageting 
        $response = false;
        if (($rules->id == 'KMA_EDUC') && $rules->value == $kma_educ_value) {
            $response = true;
        }
        return $response;
    }

    /*
     * KMA_HHINC
     * Checking income Audience tageting 
     */

    function audience_income($KMA_HHINC_value, $rules) {
        $response = false;
        if (($rules->id == 'KMA_HHINC') && $rules->value == $KMA_HHINC_value) {
            $response = true;
        }
        return $response;
    }

    /*
     * KMA_OCCUP
     * Checking occupation Audience tageting 
     */

    function audience_occupation($KMA_OCCUP_value, $rules) {
        $response = false;
        if (($rules->id == 'KMA_OCCUP') && $rules->value == $KMA_OCCUP_value) {
            $response = true;
        }
        return $response;
    }

    /*
     * KMA_AGEHOH
     * Checking occupation Audience tageting 
     *  'A':'18-34',
      'B':'35-44',
      'C':'45-54',
      'D':'55-64',
      'E':'65-74',
      'F':'75-plus'
     */

    function audience_age($agevalue, $rules) {
        $response = false;
        $range = array(
            'A' => array('min' => 18, 'max' => 34),
            'B' => array('min' => 35, 'max' => 44),
            'C' => array('min' => 45, 'max' => 54),
            'D' => array('min' => 55, 'max' => 64),
            'E' => array('min' => 65, 'max' => 74),
            'F' => array('min' => 75, 'max' => 300));


        if (($rules->id == 'KMA_AGEHOH')) {
            $operator = $rules->operator;
            if ($operator == 'less') { //17 < 18 
                if (($range[$rules->value]['min']) < $range[$agevalue]['min']) {
                    $response = true;
                }
            } elseif ($operator == 'less_or_equal') {
                if ($range[$rules->value]['min'] <= $range[$agevalue]['max']) {
                    $response = true;
                }
            } elseif ($operator == 'greater') {
                if ($range[$rules->value]['max'] > $range[$agevalue]['max']) {
                    $response = true;
                }
            } elseif ($operator == 'greater_or_equal') {
                if ($range[$rules->value]['max'] >= $range[$agevalue]['min']) {
                    $response = true;
                }
            }
        } else {
            $response = false;
        }

        return $response;
    }

    /**
     * KMA_NETWORTH
     * @param type $userProfile
     * @param type $rules
     * @return boolean
     */
    function audience_home_owner($KMA_NETWORTH_value, $rules) {
        $response = false;
        if (($rules->id == 'KMA_NETWORTH') && $rules->value == $KMA_NETWORTH_value) {
            $response = true;
        }
        return $response;
    }

    /** WEEKDAY */
    function audience_day($rules) {
        $response = false;
        $week_day = date('w');
        if (($rules->id == 'WEEKDAY') && $rules->value == $week_day) {
            $response = true;
        }
    }

    /**
     * Audience Tageting for  Interest  - key : INTEREST  
     * @param type $userProfile - user profile details on couchDB
     * @param type $rules - json format rules 
     * @return  true or false
     * @traget_audience KEYS 
     *  'KMA_HOMEOFFICE': 'Business - Home Office',
     * 	'KMA_SOHO': 'Business - Small Office/Home Office',
     *  'KMA_CONTED': 'Contiuing Education',
     *  'KMA_CIGAR_SMOKER': 'Smokers - Cigar',
     *  'KMA_SMOKER': 'Smokers',
     *  'KMA_WTLOSS': 'Health Conscious (Diet &amp; Weight Loss)',
     *  'KMA_HEALTH': 'Health Conscious (Healthy Living)',
     *  'KMA_EXERCISE': 'Health Conscious (Exercise) ',
     *  'KMA_INVESTMENTS': 'Investors (Investing &amp; Personal Finance) ',
     *  'KMA_OPSEEK': 'Business - Opportunity Seekers ',
     *  'KMA_INVEST_HIGHEND': 'Investors (High Net Worth) ',
     *  'KMA_INVEST_LOWEND': 'Investors (Active Investors)',
     *  'KMA_PETS_CATS': 'Pet Lovers - Cats ',
     *  'KMA_PETS_DOGS': 'Pet Lovers - Dogs ',
     *  'KMA_PETS': 'Pet Lovers',
     *  'KMA_SRPRODS': 'Products - Seniors',
     *  'KMA_TRAVEL': 'Travelers',
     *  'KMA_TRAVEL_HIGHEND': 'Travelers - Luxury',
     *  'KMA_TRAVEL_LOWEND': 'Travelers - Deal Hunters ',
     *  'KMA_WOMFASH': 'Fashion &amp; Apparel (Women\'s) ',
     *  'KMA_WOMFASH_HIGHEND': 'Fashion &amp; Apparel (Women\'s Brand Names) ',
     *  'KMA_WOMFASH_LOWEND': 'Fashion &amp; Apparel (Women\'s Deal Hunters)',
     *  'KMA_MENFASH': 'Fashion &amp; Apparel (Men\'s) ',
     *  'KMA_TECHNOLOGY': 'Electronics &amp; Technology',
     *  'KMA_DEALS': 'Deals &amp; Discounts',
     *  'KMA_FOODWINE': 'Food &amp; Wine',
     *  'KMA_GAMERS': 'Electronics &amp; Technology (Gamers) ',
     *  'KMA_DECOR': 'Home D?cor',
     *  'KMA_HOMEGARD': 'Home &amp; Garden',
     *  'KMA_KIDSPROD': 'Products - Children',
     *  'KMA_MOTORCYCLE': 'Auto - Motorcycle Enthusiasts',
     *  'KMA_CURREVENT': 'Magazines &amp; Books (Current Events) ',
     *  'KMA_POL_LEFT': 'Political (Left) ',
     *  'KMA_POL_RIGHT': 'Political (Right) ',
     *  'KMA_POL_IND': 'Political (Independent)',
     *  'KMA_SWEEPS': 'Sweepstakes Enthusiasts ',
     *  'KMA_DONORS': 'Donors',
     *  'KMA_CRAFTS_COLL': 'Arts, Crafts &amp; Collectibles',
     *  'KMA_AUTOMOTIVE': 'Auto - Enthusiasts',
     *  'KMA_OUTDOORS': 'Outdoors'

     */
    function audience_interest($userProfile, $rules) {
        $response = false;
        $user = (array) $userProfile;
        if ($rules->id == 'INTEREST') {
            if (empty($user[$rules->value])) {
                $response = false; // user has not interest with this criteria
            }
        }
        return $response;
    }

    /* get operator Current this is not used */

    private function GetOperatorCode($operatorname) {
        $operators = array('less' => '<', 'less_or_equal' => '<=', 'greater' => '>', 'greater_or_equal' => '>=', 'not_equal' => '!=', 'equal' => '==');
        if (array_key_exists($operatorname, $operators)) {
            return $operators[$operatorname];
        } else {
            return 0;
        }
    }

    function inline_offers() {
        //Configure::write('debug',2);
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->loadModel('tblCreative');
        $this->loadModel('FlightsCreative');
        //$adunit_id = '6e7b244c6eaa11e6b91e123aa499f9d5';//$_POST['auid'];
        $adunit_id = $_POST['auid'];
        if (empty($adunit_id))
            return;
        $options_ad['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tma',
                'type' => 'INNER',
                'conditions' => array(
                    'tma.ma_l_id = tblLineItems.li_id'
                )),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tma.ma_ad_id = tblAdunit.ad_dfp_id'
                )),
            array('table' => 'tbl_orders',
                'alias' => 'to',
                'type' => 'INNER',
                'conditions' => array(
                    'to.dfp_order_id = tblLineItems.li_order_id'
                )),
            array('table' => 'tbl_categories',
                'alias' => 'tc',
                'type' => 'INNER',
                'conditions' => array(
                    'tc.id = tblLineItems.li_category_id'
                ))
        );
        //pr($options_ad);
        $options_ad['conditions'] = array('tblAdunit.ad_uid' => $adunit_id);
        //$options_ad['fields'] = array('tblLineItems.li_name', 'tblLineItems.rank', 'tma.ma_fl_id', 'tma.ma_ecpm', 'tma.ma_uid', 'tblAdunit.adunit_name', 'tblLineItems.li_id', 'to.order_name', 'tc.category_name');
        $options_ad['fields'] = array('tblLineItems.*', 'tma.*', 'tblAdunit.*', 'to.*');
        $options_ad['order'] = 'tma.ma_ecpm desc';
        $tblLineItems = $this->tblLineItems->find('first', $options_ad);
        //get Creatives 
        //pr($tblLineItems);
        //pr($this->tblLineItems->getDataSource()->getLog());
        $creative = $this->tblCreative->find('first', array('conditions' => array('cr_lid' => $tblLineItems['tblLineItems']['li_dfp_id'])));
        //pr($creative);
        $options_create['joins'] = array(
            array('table' => 'tbl_flights',
                'alias' => 'tblFlights',
                'type' => 'INNER',
                'conditions' => array(
                    'tblFlights.fl_id = FlightsCreative.fl_id'
                )),
        );
        $options_create['conditions'] = array('FlightsCreative.cr_id' => $creative['tblCreative']['cr_id']);
        $flight = $this->FlightsCreative->find('first', $options_create);
        //pr($flight);
        $object = array('order_id' => $tblLineItems['to']['dfp_order_id'], 'od_lid' => $tblLineItems['tblLineItems']['li_dfp_id'], 'od_cid' => $creative['tblCreative']['cr_id'],
            'od_ad_id' => $tblLineItems['tblAdunit']['ad_dfp_id'], 'od_map_id' => $tblLineItems['tma']['ma_uid'], 'redirecturl' => $_POST['redirecturl']);
        $this->set('object', http_build_query($object));
        $this->set(compact('tblLineItems', 'flight', 'creative'));
        echo $this->render('/Offers/inline');
        exit();
    }

    function leads_post() {
        $this->autoLayout = false;
        $this->autoRender = false;
        if (!empty($_POST)) {
            $post_data = $_POST;
            $formData = json_decode($post_data['fData'], true);
            $fp = json_decode($post_data['fp'], true);
            $server = $_SERVER;
            $ipaddress = json_decode(str_replace(')', '', str_replace('(', '', file_get_contents('http://iplocation.siliconbiztech.com/?callback='))), true);
            $data = array(
                'od_lid' => $post_data['od_lid'],
                'od_cid' => $post_data['od_cid'],
                'od_ad_id' => $post_data['od_ad_id'],
                'od_map_id' => $post_data['od_map_id'],
                'od_email' => $formData['email'],
                'od_fname' => $formData['firstname'],
                'od_lname' => $formData['lastname'],
                'od_site' => $server['HTTP_ORIGIN'],
                'od_create' => date('Y-m-d H:i:s'),
                'od_isreturn' => 0,
                'od_isTest' => 1,
                'device_type' => '',
                'od_phone' => '',
                'od_source' => '',
                'od_country_name' => $ipaddress['country_code'],
                'fp_browser' => $fp['fp_browser'],
                'fp_connection' => $fp['fp_connection'],
                'fp_display' => $fp['fp_display'],
                'fp_flash' => $fp['fp_flash'],
                'fp_language' => $fp['fp_language'],
                'fp_os' => $fp['fp_os'],
                'fp_timezone' => $fp['fp_browser'],
                'fp_useragent' => $fp['fp_useragent'],
                'od_ipaddress' => $ipaddress['ip']);
            if (!empty($_POST['das_inline_value']) && ($_POST['das_inline_value'] == 1)) {
                $order = ClassRegistry::init('OrderLeads');
                $order->useDbConfig = 'DASOrderLeads';
                $order->useTable = 'order_' . $post_data['order_id'];
                $order->create();
                $order->save($data);
            }
            $query_data = http_build_query($formData);
            $redirect_url = $post_data['redirecturl'] . '?' . $query_data;
            echo $redirect_url;
        }
    }

}

?>		