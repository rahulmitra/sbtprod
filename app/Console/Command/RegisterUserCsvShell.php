<?php

App::uses('Component', 'Controller');
App::uses('AppShell', 'Console/Command');
App::import('Model', 'Dynamic');
App::import('Vendor', 'couchdb/couch');
App::import('Vendor', 'couchdb/couchClient');
App::import('Vendor', 'couchdb/couchDocument');
class RegisterUserCsvShell extends AppShell {

    function initialize() {
        $this->Dynamic = new Dynamic();
    }

    function main() {
        /** Getting all registered email */
        $this->Dynamic->useTable = "241_d780e550493611e6b91e123aa499f9d5";
        $data = $this->Dynamic->Query("SELECT `nl_email` FROM 241_d780e550493611e6b91e123aa499f9d5 WHERE nl_create  >= SUBDATE(DATE(CURDATE()),1) AND nl_create <SUBDATE(DATE(CURDATE()),0)");
        $i = 0;
        foreach ($data as $emailID) {
            $results[$i]['email'] = $emailID['241_d780e550493611e6b91e123aa499f9d5']['nl_email'];
            $i++;
        }
        $name = date('Y-m-d');
        $filePath = '/var/www/html/app/webroot/csv/' . $name . '.csv';
        $myfile = fopen($filePath, 'w');


        foreach ($results as $product) {
            fputcsv($myfile, $product);
        }
        fclose($myfile);
        $message = "Please download the Registered users (" . $name . ") CSV form here :" . "http://digitaladvertising.systems/csv/" . $name . '.csv';
        mail("Robert@wallstreetsurvivor.com , sandeep.bansal@siliconbiztech.com, mohsin.kabir@siliconbiztech.com ", 'Registered Users CSV: ' . date('Y-m-d'), $message);
        foreach ($data as $i=>$emailID) {
            print_r("<pre>");print_r($i);print_r("</pre>");
            $email  = $emailID['241_d780e550493611e6b91e123aa499f9d5']['nl_email'];
            if (!empty($email)) {
                $this->checkUserInCouchDb($email);
            }
        }
    }

    function checkUserInCouchDb($email) {
        $couch_dsn = "http://admin:C0uchADm!n@ec2-54-161-239-191.compute-1.amazonaws.com/";
        $couch_db = "digitaladvertising";
        $client = new couchClient($couch_dsn, $couch_db);
        //$email = 'bart@fullcontact.com';
        $response_return = "";
        $data['campaign_id'] = 7;
        $data['publisher'] = 'Test Publisher';
        $data['md5email'] = md5($email);

        try { // Update the existing data on coutch
            $response_return = $client->getDoc($data['md5email']);
            $response = $this->getFromFC($email, $response_return);
            $response = $this->getFromKMA($data, $response);
            try {
                $resp = $client->storeDoc($response);
            } catch (Exception $e) {
                echo "Something weird happened: " . $e->getMessage() . " (errcode=" . $e->getCode() . ")\n";
                exit(1);
            }
        } catch (Exception $ex) {  // save new profile on coutch 
            $doc = new stdClass();
            $doc->_id = $data['md5email'];
            $doc->Type = "EmailProfile";
            $doc->Email = $email;
            $response = $this->getFromFC($email, $doc);
            $response = $this->getFromKMA($data, $response);
            try {
                $resp = $client->storeDoc($response);
                //pr($resp);
            } catch (Exception $e) {
                pr($e);
                //exit(1);
            }
        }
    }

    /**
     * Fetch data from Full Contact
     * @param type $email
     * @param type $response_return
     * @return type
     */
    public function getFromFC($email, $response_return) {
        $URL = 'https://api.fullcontact.com/v2/person.json?email=' . $email . '&macromeasures=true';
        $headr = array();
        $headr[] = 'X-FullContact-APIKey:5b745db78eb012df';
        pr($email);
        pr(md5($email));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
        $response = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($response, true);
        if ($response['status'] == '200') {
            $data = json_decode(json_encode($response));

            foreach ($data as $key => $value) {
                $response_return->$key = $value;
            }
            $response_return->match_fc = 'Y';
        } else {
            $response_return->match_fc = 'N';
        }
        return $response_return;
    }

    /**
     * Fetch data from KMA
     * @param type $data
     * @param type $response_return
     * @return type
     */
    public function getFromKMA($data, $response_return) {
        $headers = array(
            'Accept: application/json',
            'SMART_SESSION_ID: 7ccd0eb975032cad8c41dd80587f515daaa',
            'SMART_TOKEN: 564cdcae13d08aa',
            'SMART_API_REQUEST_TYPE: REST',
        );
        // call the API 
        $responsekma = $this->createApiCall('http://kmadigital.com/api/index.php/api/campaign/', 'POST', $headers, $data);
        $responsekma = json_decode($responsekma, true);
        if ($responsekma['status'] == 'SUCCESS') {
            $data = (object) $responsekma['data'];
            foreach ($data as $key => $value) {
                $response_return->$key = $value;
            }
            $response_return->match_kma = 'Y';
        } else {
            $response_return->match_kma = 'N';
        }
        return $response_return;
    }

    public static function createApiCall($url, $method, $headers, $data = array()) {
        if ($method == 'PUT') {
            $headers[] = 'X-HTTP-Method-Override: PUT';
        }

        $headers = self::mergeArray(array("Cache-Control: no-cache"), $headers);
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_FRESH_CONNECT, true);

        switch ($method) {
            case 'GET':
                break;
            case 'POST':
                curl_setopt($handle, CURLOPT_POST, true);
                curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case 'PUT':
                curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case 'DELETE':
                curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
        }
        $response = curl_exec($handle);
        return $response;
    }

    public static function mergeArray($a, $b) {
        $args = func_get_args();
        $res = array_shift($args);
        while (!empty($args)) {
            $next = array_shift($args);
            foreach ($next as $k => $v) {
                if (is_integer($k))
                    isset($res[$k]) ? $res[] = $v : $res[$k] = $v;
                elseif (is_array($v) && isset($res[$k]) && is_array($res[$k]))
                    $res[$k] = self::mergeArray($res[$k], $v);
                else
                    $res[$k] = $v;
            }
        }
        return $res;
    }

    function help() {
        $this->out('Here comes the help message');
    }

}

?>