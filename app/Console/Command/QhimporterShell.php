<?php

App::import('Vendor', 'couchdb/couch');
App::import('Vendor', 'couchdb/couchClient');
App::import('Vendor', 'couchdb/couchDocument');
App::uses('AppShell', 'Console/Command');
App::import('Model', 'Dynamic');

class QhimporterShell extends AppShell {

    function initialize() {
        $this->Dynamic = new Dynamic();
        $couch_dsn = "http://admin:C0uchADm!n@ec2-54-161-239-191.compute-1.amazonaws.com/";
        $couch_db = "digitaladvertising";
        $this->client = new couchClient($couch_dsn, $couch_db);
    }

    function main() {
        $this->autoRender = false;
        $tblSubscribers = array();
        $this->Dynamic->useTable = "360_79c7ca8d236411e6b91e123aa499f9d5";
        $totalSubscribers = $this->Dynamic->Query("select nl_email from 360_79c7ca8d236411e6b91e123aa499f9d5");
        $j = 0;
        foreach ($totalSubscribers as $i => $totalSubscriber) {
            $fname = '';
            $lname = '';
            $age = '';
            $gender = '';
            $income = '';
            $homeowner = '';
            $email = $totalSubscriber['360_79c7ca8d236411e6b91e123aa499f9d5']['nl_email'];
            $md5email = MD5($email);
            try {
                $response_return = $this->client->getDoc($md5email);
                //pr($response_return);
                $response_return = json_decode(json_encode($response_return), true);

                if (!empty($response_return['match_fc']) && $response_return['match_fc'] == 'Y') {
                    $j++;
                    pr($response_return['Email']);
                    $socials = array();
                    foreach ($response_return['socialProfiles'] as $i => $photo) {
                        $socials[$i] = array(
                            'typeName' => !empty($photo['typeName']) ? $photo['typeName'] : '',
                            'url' => !empty($photo['url']) ? $photo['url'] : '',
                            'username' => !empty($photo['username']) ? $photo['username'] : ''
                        );
                    }
                    $photos = array();

                    foreach ($response_return['photos'] as $i => $photo) {
                        $photos[$i] = array(
                            'typeName' => !empty($photo['typeName']) ? $photo['typeName'] : '',
                            'url' => !empty($photo['url']) ? $photo['url'] : '');
                    }
                    $u_data[$i]['data'] = array(
                        'Email' => $response_return['Email'],
                        'fullName' => $response_return['contactInfo']['fullName'],
                        'familyName' => $response_return['contactInfo']['familyName'],
                        'givenName' => $response_return['contactInfo']['givenName'],
                        'gender' => !empty($response_return['demographics']['gender']) ? $response_return['demographics']['gender'] : '',
                        'photo' => $photos,
                        'socialProfiles' => $socials
                    );
                }

                //fputcsv($myfile, $posts_meta);
            } catch (Exception $e) {
                echo "exp aya" . $email;
            }
        }
        echo $j;
        //  pr($u_data);
        $name = time();
        $filePath = '/var/www/html/app/webroot/csv/qh/data.xml';
//        $xml = new SimpleXMLElement('<root/>');
//        array_walk($u_data, array($xml, 'addChild'));
//        $data = $xml->asXML();
//        // $myfile = fopen($filePath, 'a+');
//        pr($data);
//        file_put_contents($filePath, $data);

        $xml = $this->array_to_xml($u_data, new SimpleXMLElement('<root/>'))->asXML();

        echo "$xml\n";
        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML($xml);
        $dom->formatOutput = TRUE;
        $dom->saveXml();
        $dom->save($filePath);
    }

    function array_to_xml(array $arr, SimpleXMLElement $xml) {
        foreach ($arr as $k => $v) {

            $attrArr = array();
            $kArray = explode(' ', $k);
            $tag = array_shift($kArray);

            if (count($kArray) > 0) {
                foreach ($kArray as $attrValue) {
                    $attrArr[] = explode('=', $attrValue);
                }
            }

            if (is_array($v)) {
                if (is_numeric($k)) {
                    $this->array_to_xml($v, $xml);
                } else {
                    $child = $xml->addChild($tag);
                    if (isset($attrArr)) {
                        foreach ($attrArr as $attrArrV) {
                            $child->addAttribute($attrArrV[0], $attrArrV[1]);
                        }
                    }
                    $this->array_to_xml($v, $child);
                }
            } else {
                $child = $xml->addChild($tag, $v);
                if (isset($attrArr)) {
                    foreach ($attrArr as $attrArrV) {
                        $child->addAttribute($attrArrV[0], $attrArrV[1]);
                    }
                }
            }
        }

        return $xml;
    }

    function help() {
        $this->out('Here comes the help message');
    }

}

?>