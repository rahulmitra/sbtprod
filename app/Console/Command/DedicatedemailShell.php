<?php

App::uses('Component', 'Controller');
App::uses('AppShell', 'Console/Command');
App::import('Model', 'Dynamic');

class DedicatedemailShell extends AppShell {

    function initialize() {
       // $this->Dynamic = new Dynamic();
    }

    function main() {
        ini_set('memory_limit', '1256M');
        for ($i = 0; $i < 1; $i++) {
            $this->requestAction('/sendgrid/DedicatedEmail', array('esp_id' => 5, 'mailing_id' => 1029));
            sleep(10);
        }
        exit();
    }

}

?>