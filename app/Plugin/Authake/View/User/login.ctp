<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<?php echo $this->Form->create(null, array('url' => array('controller' => 'user', 'action'=>'login')));?>
	<!-- BEGIN LOGO -->
	<div class="logo" style="margin-top:0px;">
		<a href="/login">
			<img src="/img/das-logo.png" alt=""/>
		</a>
	</div>
	<!-- END LOGO -->
	<h3 style="margin-top:0px"  class="form-title">Sign In</h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		<span>
		Enter any username and password. </span>
	</div>
	<div class="form-group">
		<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
		<label class="control-label visible-ie8 visible-ie9">Username</label>
		<?php 
			echo $this->Form->input('login', array('label'=>'Login', 'size'=>'14', 'maxlength'=>'50', 'class'=>'form-control form-control-solid placeholder-no-fix'));
		?>
	</div>
	<div class="form-group">
		<label class="control-label visible-ie8 visible-ie9">Password</label>
		<?php 
			echo $this->Form->input('password', array('label'=>'Password', 'value' => '', 'size'=>'14', 'class'=>'form-control form-control-solid placeholder-no-fix'));
		?>
	</div>
	<div class="form-actions">
		<button type="submit" class="btn btn-success uppercase">Login</button>
		<label class="rememberme check">
		<input type="checkbox" name="remember" value="1"/>Remember </label>
		<a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
	</div>
	<div style="display:none;" class="login-options">
		<h4>Or login with</h4>
		<ul class="social-icons">
			<li>
				<a class="social-icon-color facebook" data-original-title="facebook" href="#"></a>
			</li>
			<li>
				<a class="social-icon-color twitter" data-original-title="Twitter" href="#"></a>
			</li>
			<li>
				<a class="social-icon-color googleplus" data-original-title="Goole Plus" href="#"></a>
			</li>
			<li>
				<a class="social-icon-color linkedin" data-original-title="Linkedin" href="#"></a>
			</li>
		</ul>
	</div>
</form>
<!-- END LOGIN FORM -->
<!-- BEGIN FORGOT PASSWORD FORM -->
<form class="forget-form" action="index.html" method="post">
	<h3>Forget Password ?</h3>
	<p>
		Enter your e-mail address below to reset your password.
	</p>
	<div class="form-group">
		<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
	</div>
	<div class="form-actions">
		<button type="button" id="back-btn" class="btn btn-default">Back</button>
		<button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
	</div>
</form>
<!-- END FORGOT PASSWORD FORM -->
</div>
<div class="copyright">
	2014-2015 © Digital Advertising Systems
</div>
<!-- END LOGIN -->					