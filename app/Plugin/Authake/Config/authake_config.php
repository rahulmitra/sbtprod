<?php
$config = array (
  'Authake' => 
  array (
    'useDefaultLayout' => '1',
    'baseUrl' => '/',
    'service' => 'Admin',
    'loginAction' => 
    array (
      'named' => 
      array (
      ),
      'pass' => 
      array (
      ),
      'controller' => 'pages',
      'action' => 'display',
      'plugin' => NULL,
    ),
    'loggedAction' => '/',
    'sessionTimeout' => '604800',
    'defaultDeniedAction' => 
    array (
      'plugin' => 'authake',
      'controller' => 'user',
      'action' => 'denied',
      'named' => 
      array (
      ),
      'pass' => 
      array (
      ),
    ),
    'rulesCacheTimeout' => '300',
    'systemEmail' => 'sunny.gulati@siliconbiztech.com',
    'systemReplyTo' => 'sunny.gulati@siliconbiztech.com',
    'passwordVerify' => '1',
    'registration' => '1',
    'defaultGroup' => '2',
    'useEmailAsUsername' => '1',
  ),
);