<?php
class tblAdunitsType extends AuthakeAppModel {
public $primaryKey = 'at_id';

public function getAdunitTypes(){
        return $this->find('all');
    }
public function getAdunitTypesList(){
       return $this->find('list',array('fields'=>array('tblAdunitsType.at_id','tblAdunitsType.at_name')));
        
    }

}
?>