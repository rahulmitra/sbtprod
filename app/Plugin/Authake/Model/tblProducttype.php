<?php
class tblProductType extends AuthakeAppModel {

	public function getProductTypeList(){
		$options['joins'] = array(
				array('table' => 'tbl_media_types',
				'alias' => 'mtypes',
				'type' => 'INNER',
				'conditions' => array(
				'tblProducttype.ptype_m_id = mtypes.mtype_id')
			));
			$this->virtualFields=array('name'=>'CONCAT(mtypes.mtype_name, "=>", tblProducttype.ptype_name)');
				 
			$options['fields'] = array('tblProducttype.ptype_id','name');
			return $this->find('list', $options);
	} 
	

	function getProducttype($parent_id = 0){
		$producttype = array();
		$producttype = $this->find('list',array('fields'=>array('ptype_id','ptype_name'),'conditions'=>array('ptype_m_id'=>$parent_id)));
		return $producttype;
	} 


}
?>