<?php

class tblFlight extends AuthakeAppModel {

    public $primaryKey = 'fl_id';

    /**
     * 
     * @param type $nl_adunit_uuid
     * @param type $range
     * @return type
     */
    public function totalLiveCampaignThisMonth($nl_adunit_uuid, $range) {
        $options['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tblMappedAdunit',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblFlight.fl_id = tblMappedAdunit.ma_fl_id')
        ));

        $options['conditions'] = array(
            'tblMappedAdunit.ma_uid' => $nl_adunit_uuid,
            'DATE(tblFlight.fl_start) >=' => date('Y-m-01'),
            'DATE(tblFlight.fl_end) <=' => date('Y-m-d'));
        $options['fields'] = array('tblFlight.*');
        $totalrecord = $this->find('count', $options);
        
        return (!empty($totalrecord)) ? $totalrecord : 0;
    }

    public function getFlightNameById($ids = array()) {
        $options['conditions'] = array('tblFlight.fl_id' => $ids);
        $options['fields'] = array('tblFlight.fl_id', 'tblFlight.fl_name');
        return $this->find('list', $options);
    }

}

?>