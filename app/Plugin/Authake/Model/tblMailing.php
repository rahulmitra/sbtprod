<?php
class tblMailing extends AuthakeAppModel {
	
	function getMaillingAdunitByAdunitId($user_id=array(), $nl_adunit_uuid= null ,$DateRange=array()){
		$options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblp.profileID = User.tblProfileID')
        ));
        $options['fields'] = array('tblp.tracksite_id');
        $options['conditions'] = array('User.id' => $user_id);
        $options['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tma',
                'type' => 'LEFT',
                'conditions' => array(
                    'tma.ma_uid = tblMailing.m_ad_uid')),
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'LEFT',
                'conditions' => array(
                    'tba.ad_dfp_id = tma.ma_ad_id',
                //     'tba.owner_user_id' => array($user_id)
                )),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'LEFT',
                'conditions' => array(
                    'tba.publisher_id = tblp.user_id')));

        $options['conditions'] = array('and' => array(
                array('tblMailing.m_schedule_date <= ' => $DateRange['end'],
                    'tblMailing.m_schedule_date >= ' => $DateRange['start'],
                    'tba.ad_uid ' => $nl_adunit_uuid
        )));
        $this->virtualFields=array(
        'total_revenue'=>'COALESCE(SUM(tblMailing.m_revenue),0)',
        'total_sent'=>'COALESCE(SUM(tblMailing.m_sent),0)',
        'total_delivered'=>'COALESCE(SUM(tblMailing.m_delivered),0)',
        'total_opens'=>'COALESCE(SUM(tblMailing.m_u_open),0)',
        'totalopens'=>'COALESCE(SUM(tblMailing.m_open),0)',
        
        'clicks'=>'COALESCE(SUM(tblMailing.m_u_clicks),0)',
        'totalclicks'=>'COALESCE(SUM(tblMailing.m_clicks),0)',
        'total_unsubscribe'=>'COALESCE(SUM(tblMailing.m_unsubscribe),0)',
        'total_bounce'=>'COALESCE(SUM(tblMailing.m_bounce),0)',
        'total_spam'=>'COALESCE(SUM(tblMailing.m_spam),0)',
        'track_code'=>'CONCAT(tblMailing.m_ad_uid, "-", tblMailing.m_id)'
        );
        $options['fields'] = array('tba.*', 'tblMailing.*', 'tblp.tracksite_id','total_revenue','total_sent','total_delivered','total_opens','totalopens','clicks','totalclicks','total_unsubscribe','total_bounce','total_spam');
        $options['fields'] = array('tba.*', 'tblMailing.*', 'tblp.tracksite_id');
        $options['order'] = array('tblMailing.m_schedule_date' => 'DESC');
        return  $this->find('first', $options);
		 
	}
	
	/**
     * revenueTrends join tbl_adunits and group by month 
     /**
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $AuthakeUserId  used for login user id
      
     * @re*/
	  public function sendToClickPer($nl_adunit_uuid = null, $DateRange = array(), $duration = 'month',$AuthakeUserId = null) {
		$options['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tma',
                'type' => 'LEFT',
                'conditions' => array(
                    'tma.ma_uid = tblMailing.m_ad_uid')),
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'LEFT',
                'conditions' => array(
                    'tba.ad_dfp_id = tma.ma_ad_id'
        )));

        $options['conditions'] = array('tba.ad_uid' => $nl_adunit_uuid, 'tblMailing.m_sent >' => 0, 'DATE(tblMailing.m_schedule_date) between ? and ?' => array($DateRange['start'], $DateRange['end']));

        $options['group'] = array('m_schedule_date');
        $this->virtualFields = array("send_to_open_per" => "sum(tblMailing.m_u_clicks) * 100 / sum(tblMailing.m_sent)", "dateformate" => "DATE_FORMAT(tblMailing.m_schedule_date,'%d %b %Y')");
        $options['fields'] = array('tblMailing.send_to_open_per', 'tblMailing.dateformate');
        return $this->find('all', $options);
    }
    /**
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $AuthakeUserId  used for login user id
      
     * @re*/
    public function sendToOpenPer($nl_adunit_uuid = null, $DateRange = array(), $duration = 'month',$AuthakeUserId = null) {
        $options['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tma',
                'type' => 'LEFT',
                'conditions' => array(
                    'tma.ma_uid = tblMailing.m_ad_uid')),
            array('table' => 'tbl_adunits',
                'alias' => 'tba',
                'type' => 'LEFT',
                'conditions' => array(
                    'tba.ad_dfp_id = tma.ma_ad_id'
        )));

        $options['conditions'] = array('tba.ad_uid' => $nl_adunit_uuid, 'tblMailing.m_sent >' => 0, 'DATE(tblMailing.m_schedule_date) between ? and ?' => array($DateRange['start'], $DateRange['end']));
        $options['group'] = array('m_schedule_date');
        $this->virtualFields = array("send_to_open_per" => "sum(tblMailing.m_u_open) * 100 / sum(tblMailing.m_sent)", "dateformate" => "DATE_FORMAT(tblMailing.m_schedule_date,'%d %b %Y')");
        $options['fields'] = array('tblMailing.send_to_open_per', 'tblMailing.dateformate');
        return $this->find('all', $options);
    }
    /**
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $AuthakeUserId  used for login user id
      
     * @re*/
    public function TotalEmailSentsTable($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        // remove tbe report virtual fields
        $this->virtualFields = array("total_m_delivered" => "sum(tblMailing.m_delivered)", "total_m_delivered" => "sum(tblMailing.m_delivered)", "total_m_opens" => "sum(tblMailing.m_open)", "total_m_u_open" => "sum(tblMailing.m_u_open)", "total_m_clicks" => "sum(tblMailing.m_clicks)", "total_m_u_clicks" => "sum(tblMailing.m_u_clicks)", "total_m_sents" => "sum(tblMailing.m_sent)");
        $options = array();
        $options['joins'] = array(
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tblMappedAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblMappedAdunit.ma_uid')),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_ad_id = tblAdunit.ad_dfp_id')),
        );

        $options['fields'] = array('tblMailing.total_m_delivered', 'tblMailing.total_m_delivered', 'tblMailing.total_m_u_open', 'tblMailing.total_m_opens', 'tblMailing.total_m_clicks', 'tblMailing.total_m_u_clicks', 'tblMailing.total_m_sents');
        ///*'tblAdunit.ad_uid' =>$nl_adunit_uuid,'tblAdunit.publisher_id' =>$user_id,/*
        $options['conditions'] = array('DATE(tblMailing.m_sent_date) between ? and ?' => array($DateRange['start'], $DateRange['end'])); //'tblAdunit.ad_isactive' =>1);//,'tblMailing.status' =>1);
        $m_sentdata = $this->find('first', $options);
        $m_sentdata['tblMailing']['sent_to_opens'] = self::getPercentage($m_sentdata['tblMailing']['total_m_opens'], $m_sentdata['tblMailing']['total_m_sents'], 1000);
        $m_sentdata['tblMailing']['sent_to_clicks'] = self::getPercentage($m_sentdata['tblMailing']['total_m_clicks'], $m_sentdata['tblMailing']['total_m_sents'], 1000);
        return $m_sentdata;
    }
    private function getPercentage($field = 1, $total = 1, $div = 1000) {
        return ($total == 0) ? 0 : ($field * $div / $total);
    }
	
	 public function getLastid(){
	 $options['fields'] = array('m_id');
	  $options['order'] = array('m_id' => 'DESC');
	 $m_sentid = $this->find('first', $options);
	
	 return $m_sentid ;
	 }
}
?>