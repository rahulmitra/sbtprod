<?php
class tblIspReport extends AuthakeAppModel {
	var $useTable = 'tbl_isp_report';
	//public $primaryKey = 'rd_id';
	
	/**
     * PerformanceRevenueByISP report 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $AuthakeUserId  used for login user id
      
     * @re*/
	Public function PerformanceRevenueByISP($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        // remove tbe report virtual fields

        $this->virtualFields = array("total_opens" => "sum(tblIspReport.open_count)", "total_clicks" => "sum(tblIspReport.click_count)", "total_bounces" => "sum(tblIspReport.bounce_count)");

        $options = array();
        $options['joins'] = array(
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.m_id = tblMailing.m_id')),
            array('table' => 'tbl_isp_list',
                'alias' => 'tblIspList',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.isp_master_id = tblIspList.id')),
        );
        $options['group'] = array('tblIspReport.isp_master_id');
        $options['fields'] = array('tblIspReport.total_opens', 'tblIspReport.total_clicks', 'tblIspReport.total_bounces', 'tblIspList.isp_name');
        ///*'tblMailing.m_ad_uid' =>$nl_adunit_uuid,'tblMailing.publisher_id' =>$user_id,/*
        $options['conditions'] = array('DATE(tblIspReport.date) between ? and ?' => array($DateRange['start'], $DateRange['end'])); //'tblAdunit.ad_isactive' =>1);//,'tblMailing.status' =>1);
        return $this->find('all', $options);
    }

	/**
     * sum(tblIspReport.revenue)*1000/sum(tblMaing.m_send) group by date , isp.id
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
	 public function ecpmbyISP($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
		$options = array();
        $options['joins'] = array(
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.m_id = tblMailing.m_id')),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblAdunit.ad_uid ')),
            array('table' => 'tbl_isp_list',
                'alias' => 'tblIspList',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.isp_master_id = tblIspList.id')),
        );
        if ($duration == 'year') {
            $options['group'] = array('month', 'tblIspList.id');
            $this->virtualFields = array("total_revenue" => "sum(tblIspReport.revenue)*1000)/sum(tblMaling.m_sent)", "dateformate" => "DATE_FORMAT(tblIspReport.date,' %b %Y')");
        } else {
            $options['group'] = array('dateformate', 'tblIspList.id');
            $this->virtualFields = array("total_revenue" => "sum(tblIspReport.revenue)*1000)/sum(tblMaling.m_sent)", "dateformate" => "DATE_FORMAT(tblIspReport.date,'%m/%d/%Y')");
        }
        $options['fields'] = array('tblIspReport.revenue', 'tblIspReport.dateformate', 'month(tblIspReport.date) as month', 'tblIspList.isp_name');
        ///*'tblMailing.m_ad_uid' =>$nl_adunit_uuid,'tblMailing.publisher_id' =>$user_id,/*
        $options['conditions'] = array('tblAdunit.ad_uid' => $nl_adunit_uuid, 'DATE(tblIspReport.date) between ? and ?' => array($DateRange['start'], $DateRange['end']));
        return $this->find('all', $options);
    }
      
      /** Earning by ISP (tbl_isp_report) (for MTD - show day wise, for YTD - show month wise) sum(rd_revenue) from tbl_report_day inner join tbl_mailings inner join tbl_isp_report inner join tbl_isp_master inner join tbl_mapped_adunits inner join tbl_adunit
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    public function EaringISP($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        // remove tbe report virtual fields

        $options = array();
        $options['joins'] = array(
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.m_id = tblMailing.m_id')),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblAdunit.ad_uid ')),
            array('table' => 'tbl_isp_list',
                'alias' => 'tblIspList',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.isp_master_id = tblIspList.id')),
        );
        if ($duration == 'year') {
            $options['group'] = array('month');
            $this->virtualFields = array("total_revenue" => "sum(tblIspReport.revenue)", "dateformate" => "DATE_FORMAT(tblIspReport.date,' %b %Y')");
        } else {
            $options['group'] = array('dateformate');
            $this->virtualFields = array("total_revenue" => "sum(tblIspReport.revenue)", "dateformate" => "DATE_FORMAT(tblIspReport.date,'%m/%d/%Y')");
        }


        $options['fields'] = array('tblIspReport.revenue', 'tblIspReport.dateformate', 'month(tblIspReport.date) as month', 'tblIspList.isp_name');
        ///*'tblMailing.m_ad_uid' =>$nl_adunit_uuid,'tblMailing.publisher_id' =>$user_id,/*
        $options['conditions'] = array('DATE(tblIspReport.date) between ? and ?' => array($DateRange['start'], $DateRange['end'])); //'tblAdunit.ad_isactive' =>1);//,'tblMailing.status' =>1);

        return $this->find('all', $options);
    } 
     
      /**
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
	 public function BounceReportByISP($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        // remove tbe report virtual fields

        $this->virtualFields = array("total_bounces" => "sum(tblIspReport.bounce_count)");
        $options = array();
        $options['joins'] = array(
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.m_id = tblMailing.m_id')),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblAdunit.ad_uid ')),
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tblMappedAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')),
            array('table' => 'tbl_isp_list',
                'alias' => 'tblIspList',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.isp_master_id = tblIspList.id')),
        );
        $options['group'] = array('tblIspReport.isp_master_id', 'tblMailing.m_id');
        $options['fields'] = array('tblIspReport.total_bounces', 'tblIspList.isp_name');
        ///*'tblMailing.m_ad_uid' =>$nl_adunit_uuid,'tblMailing.publisher_id' =>$user_id,/*
        $options['conditions'] = array('DATE(tblIspReport.date) between ? and ?' => array($DateRange['start'], $DateRange['end'])); //'tblAdunit.ad_isactive' =>1);//,'tblMailing.status' =>1);

        return $this->find('all', $options);
    }
    /**
     * Opens & Click % age Trends by ISP - 
     * percentage(isp Open), percentage(isp Click) from tbl_isp_report inner join tbl_isp_master inner join tbl_mailings inner join tbl_mapped_adunits inner join tbl_adunit
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    public function OpenClickTrendByIsp($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        // remove tbe report virtual fields

        $this->virtualFields = array("total_bounces" => "sum(tblIspReport.bounce_count)");
        $options = array();
        $options['joins'] = array(
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.m_id = tblMailing.m_id')),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblAdunit.ad_uid ')),
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tblMappedAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')),
            array('table' => 'tbl_isp_list',
                'alias' => 'tblIspList',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.isp_master_id = tblIspList.id')),
        );
        $options['group'] = array('tblIspReport.isp_master_id', 'tblMailing.m_id');
        
        if ($duration == 'year') {
				$options['group'] = array('month');
				$this->virtualFields = array( "dateformate" => "DATE_FORMAT(tblIspReport.date,' %b %Y')","OpenPercentage"=>"sum(tblIspReport.open_count)/(sum(tblIspReport.sent_count))*100","ClickPercentage"=>"(sum(tblIspReport.click_count)/sum(tblIspReport.sent_count))*100");
			} else {
				$options['group'] = array('dateformate');
				$this->virtualFields = array(  "dateformate" => "DATE_FORMAT(tblIspReport.date,'%m/%d/%Y')","OpenPercentage"=>"sum(tblIspReport.open_count)/(sum(tblIspReport.sent_count))*100","ClickPercentage"=>"(sum(tblIspReport.click_count)/sum(tblIspReport.sent_count))*100");
			}
			
			
        $options['fields'] = array('OpenPercentage','ClickPercentage','month(tblIspReport.date) as month','dateformate');
        $options['conditions'] = array('tblMailing.m_ad_uid' =>$nl_adunit_uuid,'DATE(tblIspReport.date) between ? and ?' => array($DateRange['start'], $DateRange['end'])); //'tblAdunit.ad_isactive' =>1);//,'tblMailing.status' =>1);
        return $this->find('all', $options);
    }
	   
}
?>
