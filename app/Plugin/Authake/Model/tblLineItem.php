<?php
class tblLineItem extends AuthakeAppModel {
	 public $primaryKey = 'li_id';
	
	public function nextId() {
		$result = $this->query("SHOW TABLE STATUS LIKE 'tbl_line_items';");
		return $result[0]['TABLES']['Auto_increment'];
	}
	
	public function getListLineItems(){
		  $options =array(); 
			 $options['joins'] = array(
			array('table' => 'tbl_cost_structures',
				'alias' => 'tcs',
				'type' => 'INNER',
				'conditions' => array(
				'tcs.cs_id = tblLineItem.li_cs_id'
			)),
			array('table' => 'tbl_lineitem_status',
				'alias' => 'tls',
				'type' => 'INNER',
				'conditions' => array(
				'tls.ls_id = tblLineItem.li_status_id'
			))
			);
			$options['fields'] = array('tblLineItem.li_dfp_id','tblLineItem.li_name');
			return $this->find("list",$options);
	}
	
	public function getListLineItemswithstatus(){
		  $options =array(); 
			 $options['joins'] = array(
			 array('table' => 'tbl_cost_structures',
				'alias' => 'tcs',
				'type' => 'INNER',
				'conditions' => array(
				'tcs.cs_id = tblLineItem.li_cs_id'
			)),
			array('table' => 'tbl_lineitem_status',
				'alias' => 'tls',
				'type' => 'INNER',
				'conditions' => array(
				'tls.ls_id = tblLineItem.li_status_id'
			))
			);
			$this->virtualFields = array(
			"li_id_status" => "CONCAT_WS('-', tblLineItem.li_id, tblLineItem.li_status_id)"
			);
			$options['conditions'] = array('NOT' => array('tblLineItem.li_dfp_id' => ''));
			$options['fields'] = array('li_id_status','tblLineItem.li_name');
			return $this->find("list",$options);
	}
	
	
	
	public function getLineItemDetail($lineItemId=null){
		$optionsd['joins'] = array(
			array('table' => 'tbl_cost_structures',
			'alias' => 'tcs',
			'type' => 'INNER',
			'conditions' => array(
			'tcs.cs_id = tblLineItem.li_cs_id'
			)),
			array('table' => 'tbl_orders',
			'alias' => 'tblOrder',
			'type' => 'INNER',
			'conditions' => array(
			'tblOrder.dfp_order_id = tblLineItem.li_order_id')
			),
			array('table' => 'tbl_lineitem_status',
			'alias' => 'tls',
			'type' => 'INNER',
			'conditions' => array(
			'tls.ls_id = tblLineItem.li_status_id'
			))
			);
			$optionsd['conditions'] = array('li_id' =>$lineItemId);
			$optionsd['fields'] = array('tblLineItem.*', 'tcs.*', 'tblOrder.order_name');
			return $this->find('first', $optionsd);
	} 
	/**
	 * 01-09-16
     * DasReporting function is used for shell programing get all count by  date and line item
     * @return array list 
     */ 
	public function DasReporting(){
		$criteria[] = array('tblOrder.isActive' => 1);
        $criteria['joins'] = array(
            array('table' => 'tbl_orders',
                'alias' => 'tblOrder',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.dfp_order_id = tblLineItem.li_order_id'
                )),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                )),
            array('table' => 'tbl_mapped_adunits',
                'alias' => 'tblMappedAdunit',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id = tblLineItem.li_id'
                )),
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblMappedAdunit.ma_uid'
                )),
            array('table' => 'tbl_flights',
                'alias' => 'tblFlight',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblMappedAdunit.ma_fl_id = tblFlight.fl_id')
            ),
            array('table' => 'tbl_adunits',
			'alias' => 'TblAdunit',
			'type' => 'INNER',
			'conditions' => array(
				'tblMappedAdunit.ma_ad_id = TblAdunit.ad_dfp_id'
			)) 
        );
        $this->virtualFields = array('li_total' => 'COALESCE(tblLineItem.li_total,0)');
        $criteria['fields'] = array( 'tblLineItem.li_id','tblFlight.fl_id','li_total', 'TblAdunit.adunit_id', 'TblAdunit.ad_ptype_id', 'TblAdunit.publisher_id', 'tblOrder.order_id','tblMailing.m_sent_date', 'COALESCE(SUM(tblMailing.m_open),0) as total_open', 'COALESCE(SUM(tblMailing.m_sent),0) as total_send', 'COALESCE(SUM(tblMailing.m_clicks),0) as total_clicks', 'COALESCE(SUM(tblMailing.m_revenue),0) as total_revenue', 'COALESCE(SUM(tblMailing.m_delivered),0) as total_deliverd', 'COALESCE(SUM(tblMailing.m_bounce),0) as total_bounce', 'COALESCE(SUM(tblMailing.m_spam),0) as total_compains', 'COALESCE(SUM(tblMappedAdunit.ma_imp),0) as total_impressions');
        $criteria['order'] = array('tblLineItem.li_name' => 'ASC');
        $criteria['group'] = array('date(tblMailing.m_sent_date)','tblLineItem.li_id');
        return $this->find("all", $criteria);
	}	
	
}
?>