<?php

class tblReportDay extends AuthakeAppModel {

    var $useTable = 'tbl_report_day';
    public $primaryKey = 'rd_id';

    /**
     * total Ecpm
     * (sum (tbl_report_day .rd_revenue) * 1000) / sum(tbl_mailings.m_sent) from tbl_report_day inner join tbl_mailings 
     * on rd_mailing_id=m_id inner join with tbl_adunits where rd_date as per selected time frame
     * @param type $nl_adunit_uuid
     * @param type $range
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    public function eCPM($nl_adunit_uuid = null, $range = array(), $duration = null, $user_id = null) {
        $options['joins'] = array(
            array('table' => 'tbl_mailings',
                'alias' => 'tblMaling',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMaling.m_id = tblReportDay.rd_mailing_id')),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    ' tblAdunit.ad_dfp_id = tblReportDay.rd_ad_unit_id ',
                    'tblAdunit.owner_user_id' => array($user_id)
        )));
        $this->virtualFields = array("total_rd_revenue" => "sum(tblReportDay.rd_revenue)", "total_m_sent" => "sum(tblMaling.m_sent)");
        $options['fields'] = array('tblReportDay.total_rd_revenue', 'total_m_sent');
        $options['conditions'] = array('tblAdunit.ad_uid' => $nl_adunit_uuid, 'tblAdunit.publisher_id' => $user_id, 'DATE(tblReportDay.rd_date) between ? and ?' => array($range['start'], $range['end']));
        $data = $this->find('first', $options);
        $total_m_sent = (!empty($data['tblReportDay']['total_m_sent'])) ? $data['tblReportDay']['total_m_sent'] : 0;
        $total_rd_revenue = (!empty($data['tblReportDay']['total_rd_revenue'])) ? $data['tblReportDay']['total_rd_revenue'] : 0;
        return (!empty($total_m_sent)) ? ($total_rd_revenue * 1000 / $total_m_sent) : 0;
    }

    /**
     * ecPM Trade group by date 
     * (sum (tbl_report_data.rd_revenue) * 1000) / sum(tbl_mailings.m_sent) from tbl_report_day inner join tbl_mailings 
     * on rd_mailing_id=m_id inner join with tbl_adunits where rd_date as per selected time frame Group by date 
     * @param type $nl_adunit_uuid
     * @param type $range
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    public function eCPMTrade($nl_adunit_uuid = null, $range = array(), $duration = null, $user_id = null) {
        $options['joins'] = array(
            array('table' => 'tbl_mailings',
                'alias' => 'tblMaling',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMaling.m_id = tblReportDay.rd_mailing_id')),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    ' tblAdunit.ad_dfp_id = tblReportDay.rd_ad_unit_id ',
                    'tblAdunit.owner_user_id' => array($user_id)
        )));

        if ($duration == 'year') {
            $options['group'] = array('month');
            $this->virtualFields = array("total_rd_revenue" => "(sum(tblReportDay.rd_revenue)*1000)/sum(tblMaling.m_sent)", "dateformate" => "DATE_FORMAT(tblReportDay.rd_date,' %b %Y')");
        } else {
            $options['group'] = array('dateformate');
            $this->virtualFields = array("total_rd_revenue" => "(sum(tblReportDay.rd_revenue)*1000)/sum(tblMaling.m_sent)", "dateformate" => "DATE_FORMAT(tblReportDay.rd_date,'%m/%d/%Y')");
        }

        $options['fields'] = array('tblReportDay.total_rd_revenue', 'tblReportDay.dateformate', 'month(tblReportDay.rd_date) as month');
        $options['conditions'] = array('tblAdunit.ad_uid' => $nl_adunit_uuid, 'tblAdunit.publisher_id' => $user_id, 'DATE(tblReportDay.rd_date) between ? and ?' => array($range['start'], $range['end'])); //'tblAdunit.ad_isactive' =>1);//,'tblMailing.status' =>1);

        return $this->find('all', $options);
    }
   
   /**
     * revenueTrends join tbl_adunits and group by month 
     /**
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @return type
     */
    public function revenueTrends($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {

        $options['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'LEFT',
                'conditions' => array(
                    'tblReportDay.rd_ad_unit_id = tblAdunit.ad_dfp_id')
            )
        );

        if ($duration == 'year') {
            $options['group'] = array('month');
            $this->virtualFields = array("total_rd_revenue" => "sum(tblReportDay.rd_revenue)", "dateformate" => "DATE_FORMAT(tblReportDay.rd_date,' %b %Y')");
        } else {
            $options['group'] = array('dateformate');
            $this->virtualFields = array("total_rd_revenue" => "sum(tblReportDay.rd_revenue)", "dateformate" => "DATE_FORMAT(tblReportDay.rd_date,'%m/%d/%Y')");
        }
        //'tblAdunit.ad_uid' =>$nl_adunit_uuid,
        $options['conditions'] = array('tblAdunit.ad_uid' => $nl_adunit_uuid, 'DATE(tblReportDay.rd_date) between ? and ?' => array($DateRange['start'], $DateRange['end'])); //'tblAdunit.ad_isactive' =>1);//,'tblMailing.status' =>1);

        $options['fields'] = array('tblReportDay.total_rd_revenue', 'tblReportDay.dateformate', 'month(tblReportDay.rd_date) as month'); //,'tblMailing.status' =>1);

        $revenueTrends=$this->find('all', $options);
        $data=array();
        if (!empty($revenueTrends)) {
            foreach ($revenueTrends as $revenue) {
                $data[] = array($revenue['tblReportDay']['dateformate'],$revenue['tblReportDay']['total_rd_revenue']);
            }
        }
        return $data;
    }
    /**
     * Revenue per cost structure 
     *  (tbl_report_day, line item pricing type main database)
     *  sum(rd_revenue) from tbl_report_day inner join tbl_line_items inner join tbl_cost_structures inner join adunit
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    public function RevenueByPriceFormat($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        if ($duration == 'year') {
            $this->virtualFields = array("total_rd_revenue" => "sum(tblReportDay.rd_revenue)");
        } else {
            $this->virtualFields = array("total_rd_revenue" => "sum(tblReportDay.rd_revenue)");
        }

        $options = array();
        $options['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblReportDay.rd_ad_unit_id = tblAdunit.ad_dfp_id')),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblReportDay.rd_line_item_id = tblLineItem.li_dfp_id')),
            array('table' => 'tbl_cost_structures',
                'alias' => 'tblCostStructure',
                'type' => 'INNER',
                'conditions' => array(
                    'tblLineItem.li_cs_id = tblCostStructure.cs_id'))
        );
        $options['group'] = array('tblCostStructure.cs_id');
        $options['fields'] = array('tblReportDay.total_rd_revenue', 'tblCostStructure.cs_name');
        $options['conditions'] = array('DATE(tblReportDay.rd_date) between ? and ?' => array($DateRange['start'], $DateRange['end']));
        $RevenueByPriceFormat = $this->find('all', $options);
        $data =array();
        if (!empty($RevenueByPriceFormat)) {
            foreach ($RevenueByPriceFormat as $priceformat) {
                $data[] = array($priceformat['tblCostStructure']['cs_name'], $priceformat['tblReportDay']['total_rd_revenue']);
            }
        }
        return $data;
    }
    
     /**
     * Revenue by Vertical ( category id)
     *  (tbl_report_day, line item category main database)
     * sum(rd_revenue) from tbl_report_day inner join tbl_line_items inner join adunit group by line item category
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    public function RevenueByVertical($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        if ($duration == 'year') {

            $this->virtualFields = array("total_rd_revenue" => "sum(tblReportDay.rd_revenue)");
        } else {

            $this->virtualFields = array("total_rd_revenue" => "sum(tblReportDay.rd_revenue)");
        }
        $options = array();
        $options['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblReportDay.rd_ad_unit_id = tblAdunit.ad_dfp_id')),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblReportDay.rd_line_item_id = tblLineItem.li_dfp_id')),
            array('table' => 'tbl_categories',
                'alias' => 'tblCategory',
                'type' => 'INNER',
                'conditions' => array(
                    'tblLineItem.li_category_id = tblCategory.id'))
        );

        $options['group'] = array('tblCategory.id');
        $options['fields'] = array('tblReportDay.total_rd_revenue', 'tblCategory.category_name');
        $options['conditions'] = array('tblAdunit.ad_uid' => $nl_adunit_uuid, 'DATE(tblReportDay.rd_date) between ? and ?' => array($DateRange['start'], $DateRange['end']));
        return $this->find('all', $options);
    }
     /**
     * Revenue by ISP (tbl_isp_report) - only of mails/newsletters adunits . bcz on coreg , system sending only auto-responder emails . There is no revenue for autoresponder 
     * sum(rd_revenue) from tbl_report_day inner join tbl_mailings inner join tbl_isp_report 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    public function RevenueByISP($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        $this->virtualFields = array();
        $options['conditions'] = array('tblMailing.m_ad_uid' =>$nl_adunit_uuid,'DATE(tblReportDay.rd_date) between ? and ?' => array($DateRange['start'], $DateRange['end'])); //'tblAdunit.ad_isactive' =>1);//,'tblMailing.status' =>1);
        
        $options = array();
        $options['joins'] = array(
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'tblReportDay.rd_mailing_id = tblMailing.m_id')),
            array('table' => 'tbl_isp_report',
                'alias' => 'tblIspReport',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_id = tblIspReport.m_id')),
            array('table' => 'tbl_isp_list',
                'alias' => 'tblIspList',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.isp_master_id = tblIspList.id')),
        ); 
         $this->virtualFields = array("total_rd_revenue" => "COALESCE(sum(tblReportDay.rd_revenue),0)");
         $options['fields'] = array('tblReportDay.rd_id', 'tblReportDay.total_rd_revenue', 'tblIspList.isp_name');
         $options['group'] = array('tblIspReport.isp_master_id');
         return   $this->find('all', $options);				   
    }
      
	/** Earning by ISP (tbl_isp_report) (for MTD - show day wise, for YTD - show month wise) sum(rd_revenue) from tbl_report_day inner join tbl_mailings inner join tbl_isp_report inner join tbl_isp_master inner join tbl_mapped_adunits inner join tbl_adunit
     * 
     * @param type $nl_adunit_uuid
     * @param type $DateRange
     * @param type $duration
     * @param type $user_id
     * @return type
     */
    public function EaringISP($nl_adunit_uuid = null, $DateRange = array(), $duration = null, $user_id = null) {
        // remove tbe report virtual fields

        $options = array();
        $options['joins'] = array(
        array('table' => 'tbl_isp_report',
                'alias' => 'tblIspReport',
                'type' => 'INNER',
                'conditions' => array(
                    'tblReportDay.rd_mailing_id = tblIspReport.m_id')),
            array('table' => 'tbl_mailings',
                'alias' => 'tblMailing',
                'type' => 'INNER',
                'conditions' => array(
                    'tblReportDay.rd_mailing_id = tblMailing.m_id')),
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMailing.m_ad_uid = tblAdunit.ad_uid ')),
            array('table' => 'tbl_isp_list',
                'alias' => 'tblIspList',
                'type' => 'INNER',
                'conditions' => array(
                    'tblIspReport.isp_master_id = tblIspList.id')),
        );
        if ($duration == 'year') {
            $options['group'] = array('month');
            $this->virtualFields = array("total_revenue" => "sum(tblReportDay.rd_revenue)", "dateformate" => "DATE_FORMAT(tblIspReport.date,' %b %Y')");
        } else {
            $options['group'] = array('dateformate');
            $this->virtualFields = array("total_revenue" => "sum(tblReportDay.rd_revenue)", "dateformate" => "DATE_FORMAT(tblIspReport.date,'%m/%d/%Y')");
        }
        $options['fields'] = array('total_revenue', 'dateformate', 'month(tblIspReport.date) as month');$options['conditions'] = array('tblMailing.m_ad_uid' =>$nl_adunit_uuid,'DATE(tblIspReport.date) between ? and ?' => array($DateRange['start'], $DateRange['end'])); 
        return  $this->find('all', $options);
        
    }
}

?>