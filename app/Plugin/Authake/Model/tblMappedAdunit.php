<?php

class tblMappedAdunit extends AuthakeAppModel {

    function get_total_active_flight($line_item_id = null) {

        $optionm['conditions'] = array(
            'tblMappedAdunit.ma_l_id' => $line_item_id,
            'tblMappedAdunit.ma_isActive' => '1',
            'tblMappedAdunit.ma_l_id' => $line_item_id,
            'tblFlight.status' => 1,
            'tblFlight.fl_end <=' => 'now');
        $optionm['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id = tblLineItem.li_id')
            ),
            array('table' => 'tbl_flights',
                'alias' => 'tblFlight',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_fl_id = tblFlight.fl_id')
            ),
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id')),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID')));
        return $this->find('count', $optionm);
    }

    function get_total_completed_flight($line_item_id = null) {
        $optionm['conditions'] = array(
            'tblMappedAdunit.ma_l_id' => $line_item_id, 
            
            'tblFlight.fl_end >' => 'now');
        $optionm['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = tblMappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_line_items',
                'alias' => 'tblLineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_l_id = tblLineItem.li_id')
            ),
            array('table' => 'tbl_flights',
                'alias' => 'tblFlight',
                'type' => 'INNER',
                'conditions' => array(
                    'tblMappedAdunit.ma_fl_id = tblFlight.fl_id')
            ),
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id')),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID')));

        return $this->find('count', $optionm);
    }

}

?>