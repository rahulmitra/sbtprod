<?php
class tblOrder extends AuthakeAppModel {
	/**
     * description : 
     * @author Niraj 
     * @created date 17-08-16     
     * @param type $reques_data used for Post data
     * @param type $AuthakeUserId  used for login user id
     * @return type JSON 
     */
	public function GetJsonOrderList($reques_data=array(),$AuthakeUserId=null){
	    $user_id=array();
	    if (!empty($reques_data['user_id'])) {
		$user_id = explode(',', $reques_data['user_id']);
	    }
	    
	    $options['conditions'] = array('tblOrder.isActive' => 1);
	    if (!in_array('all', $user_id) and ! empty($user_id)) {
		$options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $user_id);
	    }
	    $options['conditions']['NOT'] = array('tblOrder.dfp_order_id' => '');
	    $options['joins'] = array(
		array('table' => 'tbl_profiles',
		    'alias' => 'tblp',
		    'type' => 'INNER',
		    'conditions' => array(
			'tblOrder.advertiser_id = tblp.dfp_company_id',
			'tblOrder.owner_user_id' => array($AuthakeUserId)
	    )));
	    $options['fields'] = array('tblOrder.*', 'tblp.CompanyName');
	    $options['order'] = array(
		'tblOrder.created_at' => 'DESC');
	    $options['fields'] = array('tblOrder.dfp_order_id', 'tblOrder.order_name');
	    $orders = $this->find('list', $options);
	    $order_list = array();
	    if (!empty($orders)) {
		$count = 0;
		foreach ($orders as $key => $value) {
		    $order_list[$count]['id'] = $key;
		    $order_list[$count]['text'] = $value;
		    $count++;
		}
	    }
	    return json_encode($order_list);
	}
	/**
     * description : 
     * @author Niraj 
     * @created date 17-08-16     
     * @param type $reques_data used for Post data
     * @param type $AuthakeUserId  used for login user id
     * @return type Array 
     */
	public function GetOrderList($reques_data=array(),$AuthakeUserId=null){
	    
	    $options['conditions'] = array('tblOrder.isActive' => 1);
	     if (!empty($reques_data['company_id']) and $reques_data['company_id'][0] != 'all') {
		 $options['conditions'] = array('tblOrder.isActive' => 1, 'tblp.user_id' => $reques_data['company_id']);
	    }
	    $options['conditions']['NOT'] = array('tblOrder.dfp_order_id' => '');
	    $options['joins'] = array(
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblOrder.advertiser_id = tblp.dfp_company_id',
                    'tblOrder.owner_user_id' => array($AuthakeUserId))
		));
	    $options['order'] = array('tblOrder.created_at' => 'DESC');
	    $options['fields'] = array('tblOrder.dfp_order_id', 'tblOrder.order_name');	    
	    return $this->find('list', $options);	  
	}
}
?>