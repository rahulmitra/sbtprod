<?php

class tblAdunit extends AuthakeAppModel {

    public $primaryKey = 'adunit_id';

    public function getEmailtableName($nl_adunit_uuid = null) {
        $options['conditions'] = array('tblAdunit.ad_uid' => $nl_adunit_uuid/* ,'tblAdunit.ad_isactive'=> 1 */);
        $options['fields'] = array('tblAdunit.publisher_id');
        $tbladunit = $this->find('first', $options);
        if (!empty($tbladunit)) {
            return $tbladunit['tblAdunit']['publisher_id'] . "_" . $nl_adunit_uuid; //das_email
        }else{
			throw new NotFoundException('This Adunit "'.$nl_adunit_uuid.'" is Not availble in our database');
		}        
    }
    
     /**
     * get_adunit_list function is used for list of adunit
     * @param type $Profile_ids is used for profile ids
     * @param type $AuthakeUserId  used for login user id
     * @return type
     */ 
    public function get_adunit_list($Profile_ids = array(),$AuthakeUserId=null) {
        // form posted	
        $options['conditions'] = array(
            'tblAdunit.ad_parent_id' => 0,
            'or' => array(
                'tblAdunit.owner_user_id' => array($AuthakeUserId),
                'and' => array(
                    'tblAdunit.owner_user_id' => $Profile_ids,
                    'tblAdunit.ad_isPublicVisible' => 1)),
            'tblAdunit.ad_isactive ' => 1
        );

        $options['joins'] = array(
            array('table' => 'authake_users',
                'alias' => 'User',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = User.id'
                )),
            array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'User.tblProfileID = tblp.profileID'
                )),
            array('table' => 'tbl_product_types',
                'alias' => 'tbpt',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_ptype_id = tbpt.ptype_id'
                )),
            array('table' => 'tbl_media_types',
                'alias' => 'tbmt',
                'type' => 'INNER',
                'conditions' => array(
                    'tbmt.mtype_id = tbpt.ptype_m_id'
        )));
        $options['fields'] = array('tblAdunit.ad_uid', 'tblAdunit.adunit_name');
        return $this->find('list', $options);
    }  

}

?>
