<?php
class tblProfile extends AuthakeAppModel {
	public $primaryKey = 'profileID';
	//$this->loadModel('tblUserConnect');
	function getAllConnectedProfileList(){
		$options['joins'] = array(
			array('table' => 'authake_users',
			'alias' => 'User',
			'type' => 'INNER',
			'conditions' => array(
			'User.tblProfileID = tblProfile.profileID'
			)),
			array('table' => 'authake_groups_users',
			'alias' => 'Ugroup',
			'type' => 'INNER',
			'conditions' => array(
			'User.id = Ugroup.user_id'
			)));
			
			$options['fields'] = array('User.id','tblProfile.CompanyName');
			return $this->find('list', $options);
	}
		
	function getAllConnectedProfiles($owner_user_id =null){
		
		$loggedUserId = CakeSession::read('Authake.id');
		$usersconnected = ClassRegistry::init('tblUserConnect')->find('all', array('conditions' => array('OR' => array('requestedBy' => $loggedUserId, 'userId' => $loggedUserId), 'accepted' => '1')));
		
		$loggedProfileID = ClassRegistry::init('authake_user')->field('tblProfileID', array('id' =>$loggedUserId));
		
			//print_r($usersconnected);
			
			$connectedUser = array();
			foreach($usersconnected as $user)
			{
				$connectedUser[] = $user['tblUserConnect']['userId'];			
				$connectedUser[] = $user['tblUserConnect']['requestedBy'];		
			}
			
			$connectedUser = array_unique($connectedUser);
			
			$options['conditions'][] = array(
			'Ugroup.group_id' => '5', 
			'OR' => array(
			array('tblProfile.owner_user_id' => array($loggedUserId)),
			array('User.id' => $connectedUser),
			array('tblProfile.profileID' => $loggedProfileID)
			),
			'NOT'=>array('tblProfile.dfp_company_id'=>''));
			if(!empty($owner_user_id)){
				$options['conditions'][]= array('tblProfile.owner_user_id'=>$owner_user_id);
			}
			$options['joins'] = array(
			array('table' => 'authake_users',
			'alias' => 'User',
			'type' => 'INNER',
			'conditions' => array(
			'User.tblProfileID = tblProfile.profileID'
			)),
			array('table' => 'authake_groups_users',
			'alias' => 'Ugroup',
			'type' => 'INNER',
			'conditions' => array(
			'User.id = Ugroup.user_id'
			)));
			
			$options['fields'] = array('tblProfile.*','User.*');
			

			$test = $this->find('all', $options);
			return $test;		
	}
	function getListConnectedProfiles($key="User.id"){
		$loggedUserId = CakeSession::read('Authake.id');
		$usersconnected = ClassRegistry::init('tblUserConnect')->find('all', array('conditions' => array('OR' => array('requestedBy' => $loggedUserId, 'userId' => $loggedUserId), 'accepted' => '1')));
		$loggedProfileID = ClassRegistry::init('authake_user')->field('tblProfileID', array('id' =>$loggedUserId));
			//print_r($usersconnected);
		$connectedUser = array();
		foreach($usersconnected as $user){
				$connectedUser[] = $user['tblUserConnect']['userId'];			
				$connectedUser[] = $user['tblUserConnect']['requestedBy'];		
			}
			$connectedUser = array_unique($connectedUser);
			$options['conditions'] = array(
			'Ugroup.group_id' => '5',
			'OR' => array(
			array('tblProfile.owner_user_id' => array($loggedUserId)),
			array('User.id' => $connectedUser),
			array('tblProfile.profileID' => $loggedProfileID)
			),
			'NOT'=>array('tblProfile.dfp_company_id'=>''));
			$options['joins'] = array(
			array('table' => 'authake_users',
			'alias' => 'User',
			'type' => 'INNER',
			'conditions' => array(
			'User.tblProfileID = tblProfile.profileID'
			)),
			array('table' => 'authake_groups_users',
			'alias' => 'Ugroup',
			'type' => 'INNER',
			'conditions' => array(
			'User.id = Ugroup.user_id'
			)));
			$options['fields'] = array($key,'tblProfile.CompanyName');
			return $this->find('list', $options);					
	}
	
	function getAllConnectedProfileUserIds(){
		$loggedUserId = CakeSession::read('Authake.id');
		$usersconnected = ClassRegistry::init('tblUserConnect')->find('all', array('conditions' => array('OR' => array('requestedBy' => $loggedUserId, 'userId' => $loggedUserId), 'accepted' => '1')));
		
		$loggedProfileID = ClassRegistry::init('authake_user')->field('tblProfileID', array('id' =>$loggedUserId));
		
			//print_r($usersconnected);
			
			$connectedUser = array();
			foreach($usersconnected as $user)
			{
				$connectedUser[] = $user['tblUserConnect']['userId'];			
				$connectedUser[] = $user['tblUserConnect']['requestedBy'];		
			}
			
			$connectedUser = array_unique($connectedUser);
			
			$options['conditions'] = array(
			'Ugroup.group_id' => '5',
			'OR' => array(
			array('tblProfile.owner_user_id' => array($loggedUserId)),
			array('User.id' => $connectedUser),
			array('tblProfile.profileID' => $loggedProfileID)
			));
			
			$options['joins'] = array(
			array('table' => 'authake_users',
			'alias' => 'User',
			'type' => 'INNER',
			'conditions' => array(
			'User.tblProfileID = tblProfile.profileID'
			)),
			array('table' => 'authake_groups_users',
			'alias' => 'Ugroup',
			'type' => 'INNER',
			'conditions' => array(
			'User.id = Ugroup.user_id'
			)));
			
			$options['fields'] = array('User.id');
			$ids = $this->find('all', $options);
			$idsarray = array();
			foreach($ids as $id){
				$idsarray[] = $id['User']['id'];
			}
			
			return $idsarray;		
	}
	
	
	
	function getAllConnectedProfilesWall(){
		$loggedUserId = CakeSession::read('Authake.id');
		$usersconnected = ClassRegistry::init('tblUserConnect')->find('all', array('conditions' => array('OR' => array('requestedBy' => $loggedUserId, 'userId' => $loggedUserId), 'accepted' => '1')));
			
			//print_r($usersconnected);
			$loggedProfileID = ClassRegistry::init('authake_user')->field('tblProfileID', array('id' =>$loggedUserId));
			
			
			$connectedUser = array();
			foreach($usersconnected as $user)
			{
				$connectedUser[] = $user['tblUserConnect']['userId'];			
				$connectedUser[] = $user['tblUserConnect']['requestedBy'];			
			}
			
			$connectedUser = array_unique($connectedUser);
			
			$options['conditions'] = array(
			'Ugroup.group_id' => '5',
			'OR' => array(
			array('tblProfile.owner_user_id' => array($loggedUserId)),
			array('User.id' => $connectedUser),
			array('tblProfile.profileID' => $loggedProfileID)
			));
			
			$options['joins'] = array(
			array('table' => 'authake_users',
			'alias' => 'User',
			'type' => 'INNER',
			'conditions' => array(
			'User.tblProfileID = tblProfile.profileID'
			)),
			array('table' => 'authake_groups_users',
			'alias' => 'Ugroup',
			'type' => 'INNER',
			'conditions' => array(
			'User.id = Ugroup.user_id'
			)));
			
			$options['fields'] = array('tblProfile.*','User.*');
			

			$test = $this->find('all', $options);
			
			return $test;		
	}
}
?>