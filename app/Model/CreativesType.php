<?php
Class CreativesType extends AppModel {
	var $name = 'CreativesType';
	var $useTable = 'tbl_creatives_types';
	function GetCreativeType(){
		return  $this->find('list',array('fields'=>array('id','type'),'conditions'=>array('CreativesType.status'=>1)));
		
	} 
}
?>
