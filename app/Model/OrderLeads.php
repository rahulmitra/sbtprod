<?php
class OrderLeads extends AppModel {
	var $name = 'OrderLeads';
    var $useTable = false;
	public $useDbConfig = 'DASOrderLeads';
	
	public function getLastQuery() {
		$dbo = $this->getDatasource();
		$logs = $dbo->getLog();
		$lastLog = end($logs['log']);
		return $lastLog['query'];
	}
	
}
?>