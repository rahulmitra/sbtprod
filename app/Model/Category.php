<?php
Class Category extends AppModel {
	var $name = 'Category';
	var $useTable = 'tbl_categories';
	function GetCategory($parent_id = 0){
		$categorylist = array();
		$categorylist = $this->find('list',array('fields'=>array('id','category_name'),'conditions'=>array('Category.parent_id'=>$parent_id,'Category.status'=>1)));
		return $categorylist;
	} 
}
?>
