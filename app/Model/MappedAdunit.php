<?php

Class MappedAdunit extends AppModel {

    var $name = 'MappedAdunit';
    var $useTable = 'tbl_mapped_adunits';


function getDetails($field, $value, $limit = 4, $flight_id, $options = array()) {
    
    //pr($flight_id);
        $options_ad['joins'] = array(
            array('table' => 'tbl_adunits',
                'alias' => 'tblAdunit',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.ad_dfp_id = MappedAdunit.ma_ad_id')
            ),
            array('table' => 'tbl_line_items',
                'alias' => 'LineItem',
                'type' => 'INNER',
                'conditions' => array(
                    'MappedAdunit.ma_l_id = LineItem.li_id'
                )),
            array('table' => 'tbl_orders',
                'alias' => 'Order',
                'type' => 'INNER',
                'conditions' => array(
                    'Order.dfp_order_id = LineItem.li_order_id'
                )),
            array('table' => 'tbl_flights',
                'alias' => 'Flight',
                'type' => 'INNER',
                'conditions' => array(
                    'Flight.fl_id = MappedAdunit.ma_fl_id'
                )),
            array('table' => 'tbl_flights_creatives',
                'alias' => 'FlightCreative',
                'type' => 'INNER',
                'conditions' => array(
                    'FlightCreative.fl_id = MappedAdunit.ma_fl_id'
                )),
            array('table' => 'tbl_creatives',
                'alias' => 'Creative',
                'type' => 'INNER',
                'conditions' => array(
                    'Creative.cr_id = FlightCreative.cr_id'
                )),
             array('table' => 'tbl_profiles',
                'alias' => 'tblp',
                'type' => 'INNER',
                'conditions' => array(
                    'tblAdunit.publisher_id = tblp.user_id'))
        );

        /*if (!empty($options['test'])) {
            
        } else {
            $conditions = array(
                'MappedAdunit.ma_isActive' => 1,
                'Flight.status' => 1,
                'Order.status' => 1,
                'LineItem.tbl_lineitem_status' => 1,
            );
            //$conditions =array();
        }
        if ($field == 'ma_ad_id') {
            $conditions = am(array('MappedAdunit.ma_ad_id' => $value), $conditions);
        }*/
        $options_ad['conditions'] = array('MappedAdunit.ma_fl_id'=>$flight_id);
        $options_ad['fields'] = array('*');
        //$options_ad['limit'] = $limit;
        $options_ad['order'] = 'MappedAdunit.ma_id ASC';
        $mappedAdunitDetails = $this->find('first', $options_ad);
        //pr($this->getDataSource()->getLog(false));
        //exit;
        return $mappedAdunitDetails;
    }



}

?>
