<?php
	class Configuration extends AppModel {
		public $useDbConfig = 'authake';
		var $name = 'Configuration';  
		var $useTable = 'tbl_order_summary';
		
		function load()  
		{  
			$settings = $this->find('all');  
			foreach ($settings as $variable)  
			{  
				Configure::write  
                (  
				'Order.'.$variable['Configuration']['name'],  
				$variable['Configuration']['value']  
                );  
				Configure::write  
                (  
				'Spendy.'.$variable['Configuration']['name'],  
				$variable['Configuration']['y_spend']  
                );  
				Configure::write  
                (  
				'Ordery.'.$variable['Configuration']['name'],  
				$variable['Configuration']['y_value']  
                );  
				Configure::write  
                (  
				'Spend.'.$variable['Configuration']['name'],  
				$variable['Configuration']['spend']  
                );  
			}  
		}  
	}
?>