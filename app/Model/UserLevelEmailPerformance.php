<?php
	class UserLevelEmailPerformance extends AppModel {
		//var $name = 'Registrations_Data';
		
		public $useDbConfig = 'DASReport';
		var $name = 'UserLevelEmailPerformance';  
		var $useTable = 'UserLevelEmailPerformance';
		
		function incrementOpenCount($ListId, $email, $DateTime) {
			$count = $this->field('Opened', array('UserLevelEmailPerformance.ListId' => $ListId,'UserLevelEmailPerformance.EmailAddress' => $email));
			$count = $count + 1;
			$this->updateAll(
			array('UserLevelEmailPerformance.Opened' => $count),       
			array('UserLevelEmailPerformance.ListId' => $ListId,'UserLevelEmailPerformance.EmailAddress' => $email)
			);  
			$this->updateAll(array('UserLevelEmailPerformance.OpenedAt' => "'" . date('Y-m-d H:i:s') . "'"),
			array('UserLevelEmailPerformance.ListId' => $ListId,'UserLevelEmailPerformance.EmailAddress' => $email)
			); 
		}
		function incrementClickCount($ListId, $email, $DateTime) {
			$count = $this->field('Clicked', array('UserLevelEmailPerformance.ListId' => $ListId,'UserLevelEmailPerformance.EmailAddress' => $email));
			$count = $count + 1;
			//mail("sunny.gulati@siliconbiztech.com","Socketlab timestamp",$timestamp);
			$this->updateAll(
			array('UserLevelEmailPerformance.Clicked' => $count),       
			array('UserLevelEmailPerformance.ListId' => $ListId,'UserLevelEmailPerformance.EmailAddress' => $email)
			);  
			$this->updateAll(array('UserLevelEmailPerformance.ClickedAt' => "'" . date('Y-m-d H:i:s') . "'"),
			array('UserLevelEmailPerformance.ListId' => $ListId,'UserLevelEmailPerformance.EmailAddress' => $email)
			); 
		}
	}
?>