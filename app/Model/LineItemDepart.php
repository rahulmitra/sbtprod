<?php
Class LineItemDepart extends AppModel {
	var $name = 'LineItemDepart';
	var $useTable = 'tbl_line_item_dayparts';
	
	/**
     * getAllLineItemDepart function is used for list of daydepart of lineitem
     * @param type $line_item_id  used for return list of all these lineitems 
     * @return type array
     */ 
	function getAllLineItemDepart($line_item_id=null){
		$options['conditions'] = array('LineItemDepart.line_item_id' => $line_item_id);
        $options['fields'] = array('*');
        $data = $this->find('all', $options);
        $filterdata =array();
        if(!empty($data)){
			foreach($data as $value){
				$filterdata[$value['LineItemDepart']['week_day']][]=array('start_time'=>date('h:i a', strtotime($value['LineItemDepart']['start_time'])),'end_time'=>date('h:i a', strtotime($value['LineItemDepart']['end_time'])));
			}
		}
        return $filterdata; 
	} 
}
?>
