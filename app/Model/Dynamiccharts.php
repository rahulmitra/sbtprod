<?php

class Dynamiccharts extends AppModel {

    var $name = 'Dynamiccharts';
    var $useTable = false;
    public $useDbConfig = 'DASEmail';

    /**
     *  Check and create new table for adUnit 
     * @param type $tableName
     * @return type
     */
    public function createAdUnitDbTable($tableName) {
        $db = ConnectionManager::getDataSource('DASEmail');
        $tableNames = $db->listSources();
        if (array_search($tableName, $tableNames)) {
            return;
        } else {
           $sql_table = "CREATE TABLE IF NOT EXISTS " . $tableName . " (
                      `nl_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                      `nl_adunit_uuid` char(50) NOT NULL,
                      `nl_email` varchar(100) NOT NULL,
                      `nl_fname` varchar(100) DEFAULT NULL,
                      `nl_lname` varchar(100) DEFAULT NULL,
                      `nl_ispromotion` int(11) DEFAULT '1',
                      `nl_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      `nl_isActive` tinyint(1) DEFAULT '1',
                      `nl_source` varchar(100) DEFAULT NULL,
                      `nl_ip_address` varchar(100) DEFAULT NULL,
                      `nl_ipaddress` int(10) unsigned DEFAULT NULL,
                      `nl_pos_response` varchar(500) DEFAULT NULL,
                      `nl_timestamp` varchar(50) DEFAULT NULL,
                      PRIMARY KEY (`nl_id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=latin1;";
            $this->query($sql_table);
        }
    }

}

?>