<?php

class AdunitField extends AppModel {

    var $name = 'AdunitField';
    var $useTable = 'tbl_adunits_field_mapping';

    /**
     * Get all adunit fields 
     * @param type $adunit_id
     * @return type
     */
    public function getFields($adunit_id) {
        $adunit_fields = $this->find('all', array('conditions' => array('adunit_id' => $adunit_id)));
        return $adunit_fields;
    }
    
    public function getFieldsEmail($adunit_id = ''){
    	$options['fields']     = array('id','adunit_id','field_description','field_lable','type','field_name');
		$options['conditions'] = array('adunit_id'=> $adunit_id,'field_description LIKE'=>'%Email%');
        $result = $this->find('all', $options);
		return $result;
	}

}

?>