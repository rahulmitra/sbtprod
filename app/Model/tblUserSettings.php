<?php
	class tblUserSettings extends AppModel {
		public $useDbConfig = 'authake';
		var $name = 'tblUserSettings';  
		var $useTable = 'tbl_user_settings';
		
		function load()  
		{  
			$settings = $this->find('all');  
			foreach ($settings as $variable)  
			{  
				Configure::write  
                (  
				'User.'.$variable['tblUserSettings']['name'],  
				$variable['tblUserSettings']['value']  
                );  
			}  
		}  
	}
?>