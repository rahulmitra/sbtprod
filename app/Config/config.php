<?php

// This is used for tab and image Publisher Ad Unit 
$config['tab_ids'] = array('1' => 'images/1.png', '2' => 'images/2.png');
$config['button_option'] = array('S' => 'S', 'M' => 'M', 'L' => 'L', 'XL' => 'XL');
/* $config['addfields']=  array('First Name','Last Name','Email','Address 
  1' , 'Address 2', 'City', 'State','Zip Code','Country','Primary Phone Number', 'Date of Birth', 'Gender','Redirect Url','Phone Area Code','Phone Exchange','Phone 4 Digit');
 */
$config['addfields'] = array(
    'firstname' => array('label' => 'First Name', 'description' => 'First Name', 'texthelp' => 'Value must be shorter than 255 characters long', 'required' => true, 'defaultcheck' => 1, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'lastname' => array('label' => 'Last Name', 'description' => 'Last Name', 'texthelp' => 'Value must be shorter than 255 characters long', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'email' => array('label' => 'Email', 'description' => 'Email', 'Value must be shorter than 100 characters long', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'Address1' => array('label' => 'Address 1', 'description' => 'Address 1', 'texthelp' => 'Value must be shorter than 512 characters long', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'Address2' => array('label' => 'Address 2', 'description' => 'Address 2', 'texthelp' => 'Value must be shorter than 512 characters long', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'city' => array('label' => 'City', 'description' => 'City', 'texthelp' => 'Value must be shorter than 100 characters long', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''), 'state' => array('label' => 'State', 'description' => 'State', 'texthelp' => 'Value must be shorter than 100 characters long', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'zip_code' => array('label' => 'Zip Code', 'description' => 'Zip Code', 'texthelp' => 'Value must be shorter than 8 characters long', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'country' => array('label' => 'Country', 'description' => 'Country', 'texthelp' => 'Value must be shorter than 100 characters long', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'primary_phone_number' => array('label' => 'Primary Phone Number', 'description' => 'Primary Phone Number', 'texthelp' => 'Value must be shorter than 15 characters long', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => 'Put your disclaimer here'),
    'dob' => array('label' => 'Date of Birth', 'description' => 'Date of Birth', 'texthelp' => '', 'required' => true, 'defaultcheck' => 1, 'defaultoption' => '', 'type' => 1, 'placeholder' => 'eg:-mm/dd/yyyy', 'format' => array('1' => 'Date Textbox', '2' => 'Date Seperate Box', '3' => 'Date Seperate-Month Selection')),
    'gender' => array('label' => 'Gender', 'description' => 'Gender', 'texthelp' => '', 'required' => true, 'defaultcheck' => 1, 'defaultoption' => 'Male;Female', 'type' => 2, 'placeholder' => ''),
    'redirect_url' => array('label' => 'Redirect Url', 'description' => 'Redirect Url', 'texthelp' => 'Value must be shorter than 256 characters long', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'phone_area_code' => array('label' => 'Phone Area Code', 'description' => 'Phone Area Code', 'texthelp' => '', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'phone_exchange' => array('label' => 'Phone Exchange', 'description' => 'Phone Exchange', 'texthelp' => '', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
    'phone_4_digit' => array('label' => 'Phone 4 Digit', 'description' => 'Phone 4 Digit', 'texthelp' => '', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => '', 'type' => 1, 'placeholder' => ''),
);

$config['CollectedCollect'] = array(
    'firstname' => array('label' => 'First Name', 'texthelp' => 'Value must be shorter than 255 characters long', 'required' => true, 'defaultcheck' => 0, 'type' => 1, 'placeholder' => ''),
    'lastname' => array('label' => 'Last Name', 'texthelp' => 'Value must be shorter than 255 characters long', 'required' => false, 'defaultcheck' => 0, 'type' => 1, 'placeholder' => ''),
    'email' => array('label' => 'Email Address', 'texthelp' => 'Value must match user@domain.com <br>Value must be shorter than 255 characters long', 'required' => true, 'defaultcheck' => 1, 'type' => 1, 'placeholder' => ''),
    'phone' => array('label' => 'Phone Number', 'texthelp' => 'Must be in form: any Format<br>Must not be a toll free number (800,866,077,888,855,844,833,822) ', 'required' => true, 'defaultcheck' => 0, 'type' => 1, 'placeholder' => 'Put your disclaimer here'),
    'dob' => array('label' => 'Date of Birth', 'texthelp' => 'a) date textbox , b) date seperate box, c )  date seperate - month selection', 'required' => true, 'defaultcheck' => 0, 'type' => 1, 'placeholder' => 'eg:-mm/dd/yyyy', 'format' => array('1' => 'Date Textbox', '2' => 'Date Seperate Box', '3' => 'Date Seperate-Month Selection')),
    'gender' => array('label' => 'Gender', 'description' => 'Gender', 'texthelp' => '', 'required' => true, 'defaultcheck' => 0, 'defaultoption' => 'Male;Female', 'type' => 2, 'placeholder' => ''),
    'street_address' => array('label' => 'Street Address', 'texthelp' => 'Value must be shorter than 255 characters long', 'required' => false, 'defaultcheck' => 0, 'type' => 1, 'placeholder' => ''),
    'city' => array('label' => 'City', 'texthelp' => 'Value must be shorter than 255 characters long', 'required' => false, 'defaultcheck' => 0, 'type' => 1, 'placeholder' => ''),
    'state' => array('label' => 'State', 'texthelp' => 'Value must be shorter than 255 characters long', 'required' => false, 'defaultcheck' => 0, 'type' => 1, 'placeholder' => ''),
    'zip_code' => array('label' => 'Zip Code', 'texthelp' => 'Value must be shorter than 255 characters long', 'required' => false, 'defaultcheck' => 0, 'type' => 1, 'placeholder' => ''),
    'timestamp' => array('label' => 'TimeStamp', 'texthelp' => 'Value must be shorter than 255 characters long', 'required' => true, 'defaultcheck' => 1, 'type' => 4, 'placeholder' => ''),
    'ip_address' => array('label' => 'IP Address', 'texthelp' => '', 'required' => false, 'defaultcheck' => 1, 'type' => 4, 'placeholder' => ''),
    'lead_source' => array('label' => 'Lead Source', 'texthelp' => 'Value must be shorter than 255 characters long', 'required' => false, 'defaultcheck' => 1, 'type' => 4, 'placeholder' => ''));



$config['CollectFieldType'] = array('1' => 'Textbox', '2' => 'Radio Button', '3' => 'Dropdown', '4' => 'Hidden Field', '5' => 'Checkbox', '6' => 'Date');

$config['DateFormat'] = array(
    '1' => 'Date Textbox',
    '2' => 'Date Seperate Box',
    '3' => 'Date Seperate-Month Selection');

$config['dailyTopStocks'] = 'http://quote.stockquotes.finance/digitaladvertising_dailyTopStocks.php';
$config['wsm'] = 'http://quote.stockquotes.finance/digitaladvertising_wsm.php';
// image upload path // 
$config['Path']['creative_image'] = WWW_ROOT . 'upload' . DS . 'creatives' . DS;
$config['Path']['csvPath'] = WWW_ROOT . 'upload' . DS . 'csvLead' . DS;


$config['Path']['siteurl'] = '//digitaladvertising.systems/';//'http://sbtprod.localhost/';
$config['Path']['url_creative_image'] = $config['Path']['siteurl'] . 'upload' . DS . 'creatives' . DS;
$config['Path']['url_csvPath'] = $config['Path']['siteurl'] . 'upload' . DS . 'csvLead' . DS;
//Configure::write('Piwik.url','http://tracking.zupees.com');
$config['Piwik']['url'] = 'https://tracking.digitaladvertising.systems/index.php';
$config['Piwik']['token'] = '29e9d1b76ecf64c487d880cd316470c0';

$config['Php']['dateformte'] = 'm/d/Y';
$config['Mysql']['dateformte'] = '%m/%d/%Y';
$config['Mysql']['monthformte'] = '%b %Y';

$api_configuration = array(
    'url' => 'http://testAPI.localhost/'
);
$data_configuration = array(
    'flight' => array(
        'lead_post_status' => array(
            1 => 'Test Lead Pending',
            2 => 'Test Lead Submitted',
            3 => 'FLIGHT IS LIVE',
            4 => 'Closed'
        ),
        'delivery_mode' => array(
            '1' => 'Real Time Delivery',
            '2' => 'Batch Post',
            '3' => 'FTP Upload',
            '4' => 'HTTP GET/POST'
        ),
        'delivery_get_post' => array(
            '1' => 'GET', '2' => 'POST'
        )
    )
);

if ($_SERVER['SERVER_ADDR'] == '192.168.44.44') { // local configuration
    $config['Path']['siteurl'] = 'http://developer.das/';
    $config['Piwik']['url'] = 'https://tracking.das/index.php';
    $api_configuration = array(
        'url' => 'http://testAPI.localhost/'
    );
}
$config['api'] = $api_configuration;
$config['data'] = $data_configuration;
?>
