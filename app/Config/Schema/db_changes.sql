# Date : 02-08-16
# Author : Niraj

#26-08-16
ALTER TABLE `tbl_line_item_fields` ADD `field_format` VARCHAR(255) NOT NULL AFTER `fld_custom_texts`;

#25-08-16 Niraj

ALTER TABLE `tbl_creatives` CHANGE `cr_privacy_url` `cr_privacy_url` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

#16-08-16 Niraj

ALTER TABLE `tbl_adunits_field_mapping` CHANGE `type` `field_type` TINYINT(2) NULL;
ALTER TABLE `tbl_adunits_field_mapping` ADD `field_format` VARCHAR(255) NOT NULL AFTER `custom`;
ALTER TABLE `tbl_adunits_field_mapping` ADD `field_custom_text` VARCHAR(512) NOT NULL AFTER `custom`;
ALTER TABLE `tbl_adunits_field_mapping` ADD `field_name` VARCHAR(50) NOT NULL AFTER `custom`;

#12-8-16 kamil
ALTER TABLE `tbl_adunits_layout` ADD `script_included_button` TINYINT(2) NULL AFTER `ad_gen_layout_width`;

#05-08-16

ALTER TABLE `tbl_mailings` ADD `m_ad_uid` VARCHAR(1000) NOT NULL AFTER `created_at`;
#04-08-16

ALTER TABLE `tbl_line_items` ADD `li_subcategory_id` INT(11) NULL AFTER `li_category_id`;
ALTER TABLE `tbl_auto_responders` ADD `is_autoresp` TINYINT(2) NULL AFTER `chkARVal`; 

#02-08-16

ALTER TABLE tbl_line_items ADD status TINYINT(2) NULL COMMENT '1 for start, 2 for pause' AFTER chkARVal;
ALTER TABLE tbl_orders ADD status TINYINT(2) NULL COMMENT '1 for start, 2 for pause' AFTER isActive;
ALTER TABLE tbl_flights ADD status TINYINT(2) NULL COMMENT '1 for start, 2 for pause' AFTER fl_audience_segment;

ALTER TABLE `tbl_line_items` CHANGE `status` `tbl_lineitem_status` TINYINT(2) NULL DEFAULT NULL COMMENT '1 for play, 2 for stop';

#25/08/2016 Mohsin

--
-- Table structure for table `tbl_line_item_dayparts`
--

CREATE TABLE IF NOT EXISTS `tbl_line_item_dayparts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `line_item_id` int(11) NOT NULL,
  `week_day` int(4) NOT NULL,
  `start_time` varchar(100) DEFAULT NULL,
  `end_time` varchar(100) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE  `tbl_flights` ADD  `zip_targeting` VARCHAR( 100 ) NULL AFTER  `fl_audience_segment` ;
